#!/usr/bin/env python3
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

'''
merge old switches with new

old switches not present in new are discarded
new switches are added but commented out
old settings are transferred for switches set in old and present in new
comments/descriptions of switches are always updated
'''

import argparse
import warnings
import sys
import re
import os
from shutil import copyfile

class Switch :
    i = 0

    def __init__( self, line, number=False ) :
        m = re.search(r'(?P<used>#)?\s*(?P<name>\w+)\s*=\s*(?P<val>[^#]*)(?P<comment>#.*)?', line)
        if not m : raise Exception('')
        self.used    = m.group('used') != '#'
        self.name    = m.group('name')
        self.val     = m.group('val').strip()
        self.comment = m.group('comment').strip() if m.group('comment') else ''
        if number : 
            self.no   = Switch.i
            Switch.i += 1
        else : 
            self.no = None

    def __str__(self):
        keyval = '{0}{1.name} = {1.val}'.format('' if self.used else '#', self)
        return '{0:30}{1.comment}'.format(keyval,self)
        

def parse_switches( sfile, ordered=False ) :
    '''
    read in switch file

    ordered: keep track of order?
    '''
    switches = dict()
    with open( sfile, 'r' ) as inf:
        for line in inf:
            try:
                s = Switch( line.strip(), ordered )
                if s.name not in switches or s.used :
                    switches[s.name] = s
            except :
                pass
    return switches


def merge_switches( old, new ) :
    '''
    copy over set switches from old to new if found
    '''
    new_sorted = sorted(new.values(), key=lambda x: x.no)
    for switch in new_sorted :
        if switch.name in old and old[switch.name].used :
            switch.val  = old[switch.name].val
            switch.used = True
    return new_sorted


def print_switches( switches, fname, backup ) :
    if fname != '-' and os.path.exists(fname) and backup :
        copyfile( fname, '.'+fname+'.orig' )
    with (open( fname,'w') if fname != '-' else sys.stdout) as fout :
        for s in switches : fout.write( str(s)+'\n' )


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='merge old switch settings with new')
    parser.add_argument('old', help='current switches' )
    parser.add_argument('new', help='updated switches' )
    parser.add_argument('--output', default='-', help='output file [stdout])' )
    parser.add_argument('--backup', action='store_true', help='backup if overwriting file' )

    opts = parser.parse_args()

    current = parse_switches( opts.old )
    new     = parse_switches( opts.new, True )
    merged  = merge_switches( current, new )
    print_switches( merged, opts.output, opts.backup )

