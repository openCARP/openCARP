ARG MLIR_DEPS_IMAGE=docker.opencarp.org/opencarp/opencarp/mlir-deps:latest
FROM $MLIR_DEPS_IMAGE

# See the Dockerfile-mlir-amd and Dockerfile-mlir-nvidia dockerfiles for
# details

# In order to build openCARP for AMD, it is recommended to prevent the compiler
# from looking into the `/usr/include/thrust` directory for include paths (the
# AMD build needs to use rocthrust, located in `/opt/rocm/rocthrust`), so
# the normal thrust include path shouldn't be used

ARG name="opencarp-with-mlir-amd-nvidia"
ARG maintainer="loechner@unistra.fr"
ARG description="openCARP + LLVM + MLIR + NVIDIA + AMD"
ARG rocm_test_chipset=""

LABEL name=${name} \
      maintainer=${maintainer} \
      description=${description}

### AMD requirements ###

# Install ROCm and HIP
RUN apt update -y
RUN wget https://repo.radeon.com/amdgpu-install/22.10/ubuntu/focal/amdgpu-install_22.10.50100-1_all.deb
RUN apt install ./amdgpu-install_22.10.50100-1_all.deb -y
RUN amdgpu-install --usecase=rocm --no-dkms -y

# Install rocPRIM
RUN git clone https://github.com/ROCmSoftwarePlatform/rocPRIM.git
WORKDIR rocPRIM
RUN mkdir build && cmake -DBUILD_BENCHMARK=True -DCMAKE_CXX_COMPILER=hipcc -B build && make -j -C build && make install -C build
WORKDIR ..

# Install rocThrust
RUN git clone https://github.com/ROCmSoftwarePlatform/rocThrust.git
WORKDIR rocThrust
RUN mkdir build && cmake -DCMAKE_CXX_COMPILER=hipcc -B build && make -j -C build && make install -C build
WORKDIR ..

# Install libnuma1 (needed)
RUN apt install libnuma1 -y

### NVIDIA requirements ###

# Install CUDA
RUN apt update -y && apt install nvidia-cuda-toolkit -y

WORKDIR ${OPENCARP_DIR}/llvm-project
RUN mkdir build

# Build and install LLVM
WORKDIR ${OPENCARP_DIR}/llvm-project/build
# Enables NVIDIA and AMD Libraries in MLIR
RUN cmake -G Ninja ../llvm -DLLVM_ENABLE_PROJECTS="lld;mlir;clang;openmp" -DLLVM_TARGETS_TO_BUILD="host;NVPTX;AMDGPU" -DLLVM_ENABLE_RUNTIMES="compiler-rt;libcxx;libcxxabi" -DCMAKE_BUILD_TYPE=Release -DMLIR_ENABLE_BINDINGS_PYTHON=True -DMLIR_ENABLE_CUDA_RUNNER=True -DMLIR_ENABLE_ROCM_RUNNER=True -DBUILD_SHARED_LIBS=True -DLLVM_CCACHE_BUILD=True -DLLVM_USE_LINKER=gold -DROCM_TEST_CHIPSET=${rocm_test_chipset}
RUN ninja all
RUN cmake -DCMAKE_INSTALL_PREFIX=/usr/local -P cmake_install.cmake \
    && ldconfig

ENV PYTHONPATH="${OPENCARP_DIR}/llvm-project/build/tools/mlir/python_packages/mlir_core:${PYTHONPATH}"
ENV PATH="$PATH:${OPENCARP_DIR}/llvm-project/build/bin/"

WORKDIR ${OPENCARP_DIR}
