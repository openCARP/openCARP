ARG MLIR_DEPS_IMAGE=docker.opencarp.org/opencarp/opencarp/mlir-deps:latest
FROM $MLIR_DEPS_IMAGE

ARG name="opencarp-with-mlir-nvidia"
ARG maintainer="loechner@unistra.fr"
ARG description="openCARP + LLVM + MLIR + NVIDIA"

# Suggested CMake options for building openCARP (ptx version 64 is apparently
# the one that works with the base ubuntu image version):
# cmake -B _build # -GNinja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DENABLE_MLIR_CODEGEN=True -DUSE_OPENMP=ON -DMLIR_CUDA_PTX_FEATURE=ptx64 -DCUDA_GPU_FLAGS='--cuda-feature=+ptx64'

LABEL name=${name} \
      maintainer=${maintainer} \
      description=${description}

# Install CUDA
RUN apt update -y && apt install nvidia-cuda-toolkit -y

WORKDIR ${OPENCARP_DIR}/llvm-project
RUN mkdir build

# Build and install LLVM
WORKDIR ${OPENCARP_DIR}/llvm-project/build
# Enables CUDA Library in MLIR
RUN cmake -G Ninja ../llvm -DLLVM_ENABLE_PROJECTS="mlir;clang;openmp" -DLLVM_TARGETS_TO_BUILD="host;NVPTX" -DCMAKE_BUILD_TYPE=Release -DMLIR_ENABLE_BINDINGS_PYTHON=True -DMLIR_ENABLE_CUDA_RUNNER=True -DBUILD_SHARED_LIBS=True -DLLVM_CCACHE_BUILD=True -DLLVM_USE_LINKER=gold
RUN ninja all
RUN cmake -DCMAKE_INSTALL_PREFIX=/usr/local -P cmake_install.cmake \
    && ldconfig

ENV PYTHONPATH="${OPENCARP_DIR}/llvm-project/build/tools/mlir/python_packages/mlir_core:${PYTHONPATH}"
ENV PATH="$PATH:${OPENCARP_DIR}/llvm-project/build/bin/"

WORKDIR ${OPENCARP_DIR}
