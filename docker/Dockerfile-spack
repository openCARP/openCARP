FROM ubuntu:22.04

ARG name="opencarp-spack"
ARG maintainer="marie.houillon@kit.edu"
ARG description="environment ready for installing Spack"

LABEL name=${name} \
      maintainer=${maintainer} \
      description=${description}

ENV DEBIAN_FRONTEND=noninteractive

# Install packages for Spack
RUN apt-get -y update && apt-get install -y --no-install-recommends \
    build-essential \
    ca-certificates \
    curl \
    file \
    g++ \
    gcc \
    gfortran \
    git \
    gnupg2 \
    iproute2 \
    locales \
    make \
    python3 \
    python3-pip \
    python3-setuptools \
    tcl \
    unzip \
    wget \
 && ln -sf /usr/bin/python3 /usr/bin/python \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install hub
RUN wget https://github.com/github/hub/releases/download/v2.14.2/hub-linux-amd64-2.14.2.tgz
RUN tar -xvf hub-linux-amd64-2.14.2.tgz
RUN cp ./hub-linux-amd64-2.14.2/bin/hub /usr/local/bin/
RUN rm -rf hub-linux-amd64-2.14.2*

# Configure git
RUN git config --global user.email "info@opencarp.org"
RUN git config --global user.name "openCARP consortium"

