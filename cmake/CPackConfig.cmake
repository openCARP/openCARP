set(CPACK_PACKAGE_VENDOR ${PROJECT_NAME})
set(CPACK_PACKAGE_CONTACT "yung-lin.huang@universitaets-herzzentrum.de")

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

set(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${PROJECT_DESCRIPTION})

set(CPACK_PACKAGE_FILE_NAME "${PROJECT_NAME}-v${PROJECT_VERSION}")

set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")

if(DEFINED ENV{OPENCARP_DIR})
    set(OPENCARP_INSTALL_PREFIX $ENV{OPENCARP_DIR})
else()
    set(OPENCARP_INSTALL_PREFIX "/usr/local/lib/opencarp")
endif()

set(CPACK_PACKAGING_INSTALL_PREFIX ${OPENCARP_INSTALL_PREFIX})

# the following libraries are for packaging, they are duplicate when installed from source

# cpack base libraries
install(DIRECTORY ${OPENCARP_INSTALL_PREFIX}/lib
    DESTINATION . COMPONENT base
    USE_SOURCE_PERMISSIONS
    FILES_MATCHING
        PATTERN "libgcc_s*"
        PATTERN "libgomp*"
        PATTERN "libstdc++*"
        PATTERN "libgfortran*"
        PATTERN "libquadmath*"
        PATTERN "petsc" EXCLUDE
        PATTERN "openmpi" EXCLUDE
)

# cpack PETSc library
if(DEFINED ENV{PETSC_DIR})
    set(PETSC_DIR $ENV{PETSC_DIR})
else()
    set(PETSC_DIR "${OPENCARP_INSTALL_PREFIX}/lib/petsc")
endif()

if(IS_DIRECTORY ${PETSC_DIR}/bin)
    set(PETSC_DIRS_TO_INSTALL ${PETSC_DIR}/lib ${PETSC_DIR}/include ${PETSC_DIR}/bin)
else()
    set(PETSC_DIRS_TO_INSTALL ${PETSC_DIR}/lib ${PETSC_DIR}/include)
endif()

install(DIRECTORY ${PETSC_DIRS_TO_INSTALL}
    DESTINATION lib/petsc COMPONENT libpetsc
    USE_SOURCE_PERMISSIONS
    PATTERN "petsc/lib/petsc/bin" EXCLUDE
    PATTERN "petsc/conf" EXCLUDE
)

set(CPACK_COMPONENTS_ALL_IN_ONE_PACKAGE ON)
set(CPACK_COMPONENT_BASE_DISPLAY_NAME "openCARP base libraries")
set(CPACK_COMPONENT_BASE_DESCRIPTION  "openCARP base libraries")
set(CPACK_COMPONENT_CORE_DISPLAY_NAME "openCARP simulator")
set(CPACK_COMPONENT_CORE_DESCRIPTION  "openCARP core executable files: openCARP and bench")
set(CPACK_COMPONENT_TOOL_DISPLAY_NAME "openCARP tools")
set(CPACK_COMPONENT_TOOL_DESCRIPTION  "openCARP command-line tools: mesher, igbhead, igbextract, igbops, and igbapd")
set(CPACK_COMPONENT_LIBPETSC_DISPLAY_NAME "PETSc libraries")
set(CPACK_COMPONENT_LIBPETSC_DESCRIPTION  "PETSc libraries for openCARP")
set(CPACK_COMPONENT_LIBOPENMPI_DISPLAY_NAME "OpenMPI libraries")
set(CPACK_COMPONENT_LIBOPENMPI_DESCRIPTION  "OpenMPI libraries for openCARP")
set(CPACK_COMPONENTS_ALL base core tool libpetsc libopenmpi opencarp-src opencarp-autogen-src opencarp-autogen-limpet-src opencarp-scripts)

set(CPACK_COMPONENT_BASE_REQUIRED TRUE)
set(CPACK_COMPONENT_CORE_REQUIRED TRUE)
set(CPACK_COMPONENT_LIBPETSC_REQUIRED TRUE)
set(CPACK_COMPONENT_LIBOPENMPI_REQUIRED TRUE)
set(CPACK_COMPONENT_CORE_DEPENDS base libpetsc libopenmpi)
set(CPACK_COMPONENT_TOOL_DEPENDS base libpetsc libopenmpi)

# package generator: cmake --build _build --target package
set(CPACK_GENERATOR "TGZ;ZIP")

# package generator for DEB (Ubuntu): cd _build && cpack -G DEB
set(CPACK_DEB_COMPONENT_INSTALL ON)
set(CPACK_DEBIAN_PACKAGE_DEPENDS "ssh")
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/cmake/postinst")

# package generator for RPM (CentOS): cd _build && cpack -G RPM
set(CPACK_RPM_COMPONENT_INSTALL ON)
set(CPACK_RPM_PACKAGE_AUTOREQ " no")
set(CPACK_RPM_PACKAGE_REQUIRES "libgfortran, openssh-clients")
set(CPACK_RPM_POST_INSTALL_SCRIPT_FILE "${CMAKE_CURRENT_SOURCE_DIR}/cmake/postinst")

# package generator for PKG (Mac): cd _build && cpack -G productbuild
if(APPLE)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md ${CMAKE_BINARY_DIR}/LICENSE.txt)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/README.md ${CMAKE_BINARY_DIR}/README.txt)
    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_BINARY_DIR}/LICENSE.txt")
    set(CPACK_RESOURCE_FILE_README "${CMAKE_BINARY_DIR}/README.txt")

    set(CPACK_POSTFLIGHT_CORE_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/cmake/postinst")
    set(CPACK_POSTFLIGHT_TOOL_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/cmake/postinst")

    set(CPACK_SET_DESTDIR ON)
    set(CPACK_GENERATOR "productbuild")
endif(APPLE)

# source package generator: cmake --build build --target package_source
set(CPACK_SOURCE_GENERATOR "TGZ;ZIP")
set(CPACK_SOURCE_IGNORE_FILES
    /.git
    /.gitignore
    /*build*
    /.DS_Store
)

if(BUILD_EXTERNAL)
    # cpack carputils
    list(APPEND CPACK_COMPONENTS_ALL carputils)
    set(CPACK_COMPONENT_CARPUTILS_DEPENDS core tool meshtool)
    set(CPACK_COMPONENT_CARPUTILS_DISPLAY_NAME "carputils")
    set(CPACK_COMPONENT_CARPUTILS_DESCRIPTION  "carputils is a Python framework for generating and running openCARP examples.")
    string(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS ", python3-pip, python3-dev, python3-setuptools, python3-tk, gcc, git, pkg-config, libfreetype6-dev, libpng-dev, libjpeg-dev")
    string(APPEND CPACK_RPM_PACKAGE_REQUIRES ", python3-pip, python3-devel, python3-setuptools, python3-tkinter, gcc-c++, git, freetype-devel, libpng-devel, libjpeg-devel, diffutils, which")

# Don't integrate OpenMP for now, TODO
#    if(APPLE) # add libomp files for carputils c extension building
#        set(MACOS_OMP_PREFIX "/opt/local")
#
#        install(DIRECTORY ${MACOS_OMP_PREFIX}/lib/libomp ${MACOS_OMP_PREFIX}/include/libomp
#           DESTINATION lib COMPONENT libomp-macos
#            USE_SOURCE_PERMISSIONS
#        )
#
#        list(APPEND CPACK_COMPONENTS_ALL libomp-macos)
#        list(APPEND CPACK_COMPONENT_CARPUTILS_DEPENDS libomp-macos)
#    endif(APPLE)

    # cpack openCARP examples
    list(APPEND CPACK_COMPONENTS_ALL examples)
    set(CPACK_COMPONENT_EXAMPLES_DEPENDS carputils)
    set(CPACK_COMPONENT_EXAMPLES_DISPLAY_NAME "openCARP examples")
    set(CPACK_COMPONENT_EXAMPLES_DESCRIPTION  "These examples are intended to transfer basic user know-how regarding most openCARP features in an efficient way. The scripts are designed as mini-experiments, which can also serve as basic building blocks for more complex experiments.")

    # cpack meshtool
    list(APPEND CPACK_COMPONENTS_ALL meshtool)
    set(CPACK_COMPONENT_MESHTOOL_DEPENDS base)
    set(CPACK_COMPONENT_MESHTOOL_DISPLAY_NAME "Meshtool")
    set(CPACK_COMPONENT_MESHTOOL_DESCRIPTION  "Meshtool is a comand-line tool written in C++. It is designed to apply various manipulations to volumetric meshes.")

    # TODO: cpack meshalyzer
    # list(APPEND CPACK_COMPONENTS_ALL meshalyzer)

endif()

include(CPack)
