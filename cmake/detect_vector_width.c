// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file detect_vector_width.c
* @brief Detect vector width on X86 (SSE, AVX, AVX512), and on ARM/Aarch64 (SVE and Neon)
* @author Vincent Loechner
* @version
* @date 2025-03-03
*/

#include <stdio.h>

#if defined(__aarch64__) && defined(__ARM_FEATURE_SVE)
    #if __has_include(<arm_sve.h>)
        #include <arm_sve.h>
        #define SVE_SUPPORTED 1
    #endif
#endif

int main() {
    int vector_width = 64; // Default scalar width

    // Detect x86 SIMD (SSE, AVX, AVX-512)
    #if defined(__AVX512F__)
        vector_width = 512;
    #elif defined(__AVX2__)
        vector_width = 256;
    #elif defined(__AVX__)
        vector_width = 256;
    #elif defined(__SSE2__)
        vector_width = 128;

    // Detect ARM SIMD SVE, falls back to NEON if not found
    #elif SVE_SUPPORTED
        vector_width = svcntb() * 8; // Get max SVE vector width
    #elif defined(__ARM_NEON) || defined(__ARM_NEON__)
        vector_width = 128; // NEON (128-bit)
    #endif

    printf("%d\n", vector_width);
    return 0;
}
