function(get_ionic_models_for_opt MLIR_LIST MLIR_INLINE_LIST MLIR_NO_INLINE_LIST NO_INLINE_FLAG)

    set(NEW_MLIR_INLINE_LIST CACHE INTERNAL "")
    set(NEW_MLIR_NO_INLINE_LIST CACHE INTERNAL "")

    foreach(IMP INLINE NO_INLINE IN ZIP_LISTS MLIR_LIST MLIR_INLINE_LIST MLIR_NO_INLINE_LIST)
        if (INLINE)
            list(APPEND NEW_MLIR_INLINE_LIST ${IMP})
        elseif (NO_INLINE)
            list(APPEND NEW_MLIR_NO_INLINE_LIST ${IMP})
        elseif (NOT NO_INLINE_FLAG)
            list(APPEND NEW_MLIR_INLINE_LIST ${IMP})
        else()
            list(APPEND NEW_MLIR_NO_INLINE_LIST ${IMP})
        endif()
    endforeach()

    set(MLIR_INLINE_LIST "${NEW_MLIR_INLINE_LIST}" PARENT_SCOPE)
    set(MLIR_NO_INLINE_LIST "${NEW_MLIR_NO_INLINE_LIST}" PARENT_SCOPE)
endfunction()

macro(get_ionic_string_length MLIR_LIST MLIR_TOTAL MLIR_STR MAX_LENGTH)

    math(EXPR MLIR_TOTAL_REMINDER "${MLIR_TOTAL} %6 ")
    math(EXPR MLIR_TOTAL_NO_REMINDER "${MLIR_TOTAL} - ${MLIR_TOTAL_REMINDER}")
    math(EXPR MLIR_TOTAL_NO_REMINDER_MINUS_6 "${MLIR_TOTAL} - ${MLIR_TOTAL_REMINDER} - 6")

    set(MAXIMUM_LENGTH "${MAX_LENGTH}")
    set(MODEL_LIST ${MLIR_LIST})
    if (MLIR_TOTAL_NO_REMINDER GREATER_EQUAL 6)
        foreach(i RANGE 0 ${MLIR_TOTAL_NO_REMINDER_MINUS_6} 6)
            list(SUBLIST MODEL_LIST ${i} 6 NEW_LIST)
            string(REPLACE ";" " " NEW_LIST_STR "${NEW_LIST}")

            string(LENGTH ${NEW_LIST_STR} LIST_LENGTH)

            if (LIST_LENGTH GREATER MAXIMUM_LENGTH)
                set(MAXIMUM_LENGTH ${LIST_LENGTH})
            endif()
        endforeach()
    endif()

    # Remaining ionic models
    if (MLIR_TOTAL_REMINDER GREATER 0)
        list(SUBLIST MODEL_LIST ${MLIR_TOTAL_NO_REMINDER} ${MLIR_TOTAL_REMINDER} NEW_LIST)
        string(REPLACE ";" " " NEW_LIST_STR "${NEW_LIST}")
        string(LENGTH ${NEW_LIST_STR} LIST_LENGTH)

        if (LIST_LENGTH GREATER MAXIMUM_LENGTH)
            set(MAXIMUM_LENGTH ${LIST_LENGTH})
        endif()
    endif()

    # MLIR printing message
    string(REPLACE ";" " " NEW_LIST_STR "${MLIR_STR}")
    string(LENGTH ${NEW_LIST_STR} LIST_LENGTH)
    if (LIST_LENGTH GREATER MAXIMUM_LENGTH)
        set(MAXIMUM_LENGTH ${LIST_LENGTH})
    endif()

    # Add two more characters
    math(EXPR MAXIMUM_LENGTH "${MAXIMUM_LENGTH} + 2 ")

    if (MAXIMUM_LENGTH GREATER MAX_LENGTH)
        set(MAX_LENGTH "${MAXIMUM_LENGTH}")
    endif()
endmacro()

macro(center_string MLIR_STR MAXIMUM_LENGTH)
    string(LENGTH ${MLIR_STR} STR_LENGTH)
    math(EXPR NUM_ADDED_CHARS "(${MAXIMUM_LENGTH} - ${STR_LENGTH})/2")
    string(REPEAT " " ${NUM_ADDED_CHARS} ADDED_CHARS)
    message(STATUS "${ADDED_CHARS}${MLIR_STR}")
endmacro()

function(print_ionic_models MLIR_LIST MLIR_TOTAL MAXIMUM_LENGTH)

    math(EXPR MLIR_TOTAL_REMINDER "${MLIR_TOTAL} %6 ")
    math(EXPR MLIR_TOTAL_NO_REMINDER "${MLIR_TOTAL} - ${MLIR_TOTAL_REMINDER}")
    math(EXPR MLIR_TOTAL_NO_REMINDER_MINUS_6 "${MLIR_TOTAL} - ${MLIR_TOTAL_REMINDER} - 6")

    if (MLIR_TOTAL_NO_REMINDER GREATER_EQUAL 6)
        foreach(i RANGE 0 ${MLIR_TOTAL_NO_REMINDER_MINUS_6} 6)
            list(SUBLIST MLIR_LIST ${i} 6 NEW_LIST)
            string(REPLACE ";" " " NEW_LIST_STR "${NEW_LIST}")
            center_string("${NEW_LIST_STR}" "${MAXIMUM_LENGTH}")
        endforeach()
    endif()

    if (MLIR_TOTAL_REMINDER GREATER 0)
        # Remaining ionic models
        list(SUBLIST MLIR_LIST ${MLIR_TOTAL_NO_REMINDER} ${MLIR_TOTAL_REMINDER} NEW_LIST)
        string(REPLACE ";" " " NEW_LIST_STR "${NEW_LIST}")
        center_string("${NEW_LIST_STR}" "${MAXIMUM_LENGTH}")
    endif()

endfunction()

macro(get_specific_include_directories_from_target TARGET CXX_INCLUDES TYPE)
    set(NEW_CXX_INCLUDES "")
    # get properties from INCLUDE_DIRECTORIES
    get_target_property(INCLUDE_DIRECTORIES ${TARGET} ${TYPE})
    foreach(INCLUDE_DIR ${INCLUDE_DIRECTORIES})
        string(FIND ${INCLUDE_DIR} "INSTALL_INTERFACE:" INSTALL_INTERFACE)
        if (INSTALL_INTERFACE GREATER 0)
            continue()
        else()
            string(FIND ${INCLUDE_DIR} "BUILD_INTERFACE:" BUILD_INTERFACE)
            if (BUILD_INTERFACE GREATER 0)
                string(REPLACE "$<BUILD_INTERFACE:" "" NEW_INCLUDE_DIR ${INCLUDE_DIR})
                string(REPLACE ">" "" INCLUDE_DIR ${NEW_INCLUDE_DIR} )
            endif()
        endif()
        string(FIND ${CXX_INCLUDES} ${INCLUDE_DIR} INCLUDE_FOUND)
        if (INCLUDE_FOUND LESS 0)
            string(APPEND NEW_CXX_INCLUDES " -I ${INCLUDE_DIR}")
        endif()
    endforeach()
    set(CXX_INCLUDES "${CXX_INCLUDES}${NEW_CXX_INCLUDES}")
endmacro()

macro(get_include_directories_from_target TARGET CXX_INCLUDES)
    get_specific_include_directories_from_target(${TARGET} ${CXX_INCLUDES} INCLUDE_DIRECTORIES)
    get_specific_include_directories_from_target(${TARGET} ${CXX_INCLUDES} INTERFACE_INCLUDE_DIRECTORIES)
endmacro()

macro(get_include_directories INCLUDE_DIRS CXX_INCLUDES)
    # Get directories
    list(LENGTH INCLUDE_DIRS DIR_LENGTH)
    if (DIR_LENGTH LESS 2)
        set(CXX_INCLUDES "${CXX_INCLUDES} -I ${INCLUDE_DIRS}")
    else()
        foreach(INCLUDE_DIR IN LISTS INCLUDE_DIRS)
            set(CXX_INCLUDES "${CXX_INCLUDES} -I ${INCLUDE_DIR}")
        endforeach()
    endif()
endmacro()
