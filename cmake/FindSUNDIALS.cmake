# FindSUNDIALS
# ------------
#
# Find SUNDIALS, the SUite of Nonlinear and DIfferential/ALgebraic equation Solvers
#
# Currently only actually looks for cvode and nvecserial
#
# This module will define the following variables:
#
# ::
#
#   SUNDIALS_FOUND        - true if SUNDIALS was found on the system
#   SUNDIALS_INCLUDE_DIRS - Location of the SUNDIALS includes
#   SUNDIALS_LIBRARIES    - Required libraries
#   SUNDIALS_VERSION      - Full version string
#   SUNDIALS_DIR          - Location of the SUNDIALS installation
#
# This module will export the following targets:
#
# ``SUNDIALS::nvecserial``
# ``SUNDIALS::cvode``
#
# You can also set the following variables:
#
# ``SUNDIALS_ROOT`` or ``SUNDIALS_DIR`` (as an environment variable)
#   Specify the path to the SUNDIALS installation to use
#
# ``SUNDIALS_DEBUG``
#   Set to TRUE to get extra debugging output

include(FindPackageHandleStandardArgs)

if(NOT DEFINED ENV{SUNDIALS_DIR} AND NOT DEFINED SUNDIALS_ROOT)
  find_package(SUNDIALS CONFIG QUIET)
endif()

if (SUNDIALS_FOUND)
  if (TARGET SUNDIALS::nvecserial)
    return()
  else()
    message(STATUS "SUNDIALS found but not SUNDIALS::nvecserial")
  endif()
endif()

find_path(SUNDIALS_INCLUDE_DIR
  sundials_config.h
  HINTS
    "${SUNDIALS_ROOT}"
    ENV SUNDIALS_DIR
  PATH_SUFFIXES include include/sundials
  DOC "SUNDIALS Directory")

if (SUNDIALS_DEBUG)
  message(STATUS "[ ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE} ] "
    " SUNDIALS_INCLUDE_DIR = ${SUNDIALS_INCLUDE_DIR}"
    " SUNDIALS_ROOT = ${SUNDIALS_ROOT}")
endif()

set(SUNDIALS_INCLUDE_DIRS
  "${SUNDIALS_INCLUDE_DIR}"
  "${SUNDIALS_INCLUDE_DIR}/.."
  CACHE STRING "SUNDIALS include directories")

find_library(SUNDIALS_nvecserial_LIBRARY
  NAMES sundials_nvecserial
  HINTS
    "${SUNDIALS_INCLUDE_DIR}/.."
    "${SUNDIALS_INCLUDE_DIR}/../.."
  PATH_SUFFIXES lib lib64
  )

if (SUNDIALS_DEBUG)
  message(STATUS "[ ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE} ] "
    " SUNDIALS_nvecserial_LIBRARY = ${SUNDIALS_nvecserial_LIBRARY}")
endif()

if (NOT SUNDIALS_nvecserial_LIBRARY)
  message(WARNING "Sundials requested but SUNDIALS nvecserial not found.")
endif()
list(APPEND SUNDIALS_LIBRARIES "${SUNDIALS_nvecserial_LIBRARY}")
mark_as_advanced(SUNDIALS_nvecserial_LIBRARY)

set(SUNDIALS_COMPONENTS cvode)

foreach (LIB ${SUNDIALS_COMPONENTS})
  find_library(SUNDIALS_${LIB}_LIBRARY
    NAMES sundials_${LIB}
    HINTS
      "${SUNDIALS_INCLUDE_DIR}/.."
      "${SUNDIALS_INCLUDE_DIR}/../.."
    PATH_SUFFIXES lib lib64
    )

  if (SUNDIALS_DEBUG)
    message(STATUS "[ ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE} ] "
      " SUNDIALS_${LIB}_LIBRARY = ${SUNDIALS_${LIB}_LIBRARY}")
  endif()

  if (NOT SUNDIALS_${LIB}_LIBRARY)
    message(WARNING "Sundials requested but SUNDIALS ${LIB} not found.")
  endif()
  list(APPEND SUNDIALS_LIBRARIES "${SUNDIALS_${LIB}_LIBRARY}")
  mark_as_advanced(SUNDIALS_${LIB}_LIBRARY)
endforeach()

if (SUNDIALS_INCLUDE_DIR)
  file(READ "${SUNDIALS_INCLUDE_DIR}/sundials_config.h" SUNDIALS_CONFIG_FILE)
  string(FIND "${SUNDIALS_CONFIG_FILE}" "SUNDIALS_PACKAGE_VERSION" index)
  if("${index}" LESS 0)
    # Version >3
    set(SUNDIALS_VERSION_REGEX_PATTERN
      ".*#define SUNDIALS_VERSION \"([0-9]+)\\.([0-9]+)\\.([0-9]+)\".*")
  else()
    # Version <3
    set(SUNDIALS_VERSION_REGEX_PATTERN
      ".*#define SUNDIALS_PACKAGE_VERSION \"([0-9]+)\\.([0-9]+)\\.([0-9]+)\".*")
  endif()
  string(REGEX MATCH ${SUNDIALS_VERSION_REGEX_PATTERN} _ "${SUNDIALS_CONFIG_FILE}")
  set(SUNDIALS_VERSION_MAJOR ${CMAKE_MATCH_1} CACHE STRING "")
  set(SUNDIALS_VERSION_MINOR ${CMAKE_MATCH_2} CACHE STRING "")
  set(SUNDIALS_VERSION_PATCH ${CMAKE_MATCH_3} CACHE STRING "")
  set(SUNDIALS_VERSION "${SUNDIALS_VERSION_MAJOR}.${SUNDIALS_VERSION_MINOR}.${SUNDIALS_VERSION_PATCH}" CACHE STRING "SUNDIALS version")
endif()

if("${SUNDIALS_VERSION_MAJOR}" LESS 4)
  message(WARNING "SUNDIALS versions <4 are depricated and will not be supported in the next release")
endif()

if (SUNDIALS_DEBUG)
  message(STATUS "[ ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE} ] "
    " SUNDIALS_VERSION = ${SUNDIALS_VERSION}")
endif()

find_package_handle_standard_args(SUNDIALS
  REQUIRED_VARS SUNDIALS_LIBRARIES SUNDIALS_INCLUDE_DIR SUNDIALS_INCLUDE_DIRS
  VERSION_VAR SUNDIALS_VERSION
  )

set(SUNDIALS_LIBRARIES "${SUNDIALS_LIBRARIES}" CACHE STRING "SUNDIALS libraries")
mark_as_advanced(SUNDIALS_LIBRARIES SUNDIALS_INCLUDE_DIR SUNDIALS_INCLUDE_DIRS)

if (SUNDIALS_FOUND AND NOT TARGET SUNDIALS::SUNDIALS)
  add_library(SUNDIALS::nvecserial UNKNOWN IMPORTED)
  set_target_properties(SUNDIALS::nvecserial PROPERTIES
    IMPORTED_LOCATION "${SUNDIALS_nvecserial_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${SUNDIALS_INCLUDE_DIRS}")

  foreach (LIB ${SUNDIALS_COMPONENTS})  
    add_library(SUNDIALS::${LIB} UNKNOWN IMPORTED)
    set_target_properties(SUNDIALS::${LIB} PROPERTIES
      IMPORTED_LOCATION "${SUNDIALS_${LIB}_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${SUNDIALS_INCLUDE_DIRS}"
      INTERFACE_LINK_LIBRARIES SUNDIALS::nvecserial)
  endforeach()
endif()

# Set SUNDIALS_DIR based on SUNDIALS_INCLUDE_DIR if it has not been set yet
if(SUNDIALS_FOUND)
  if (NOT DEFINED CACHE{SUNDIALS_DIR} OR SUNDIALS_DIR STREQUAL "SUNDIALS_DIR-NOTFOUND")
    find_path(SUNDIALS_DIR
    include
    HINTS
      "${SUNDIALS_INCLUDE_DIR}/.."
      "${SUNDIALS_INCLUDE_DIR}/../.."
    DOC "SUNDIALS installation directory")
  endif()
endif()

