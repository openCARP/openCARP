## Instructions to sign the Contributor Agreement

To make the Contributor Agreement effective, please follow these steps:
* Read the [Contributor Agreement](https://opencarp.org/cla)
* Create an account on [our GitLab instance](http://git.opencarp.org)
* Fork off a [new branch](https://git.opencarp.org/openCARP/openCARP/-/branches/new) from the master of [the openCARP project](https://git.opencarp.org/openCARP/openCARP)
* Add a single file to the [contributors](contributors) folder named with your username and the extension ``.md``, e.g., ``JohnDoe.md``.
* Put the following in the file:

```
[date]

I hereby agree to the terms of the Contributor Agreement, version 1.2, with
MD5 checksum c66e0c1ecfef4f477d3636b89f118b65.

I furthermore declare that I am authorized and able to make this
agreement and sign this declaration.

Signed,

[your name]
https://git.opencarp.org/[your username]
```

Replace the bracketed text as follows:
* `[date]` with today's date, in the unambiguous numeric form YYYY-MM-DD.
* `[your name]` with your name.
* `[your username]` with your GitLab username.

You can confirm the MD5 checksum of the Contributor Agreement by running the
md5 program over CLA-1.0.md:

```
md5 CLA-1.2.md
MD5 (CLA-1.2.md) = c66e0c1ecfef4f477d3636b89f118b65
```

If the output is different from above, do not sign the Contributor Agreement
and let us know.

* Open a merge request to integrate your signed Contributor Agreement from
your cla-branch into the master branch.


That's it!
