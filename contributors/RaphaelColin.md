2023-01-25

I hereby agree to the terms of the Contributor Agreement, version 1.1, with
MD5 checksum a259dbbb2f90249a5998f19b62082fcf.

I furthermore declare that I am authorized and able to make this
agreement and sign this declaration.

Signed,

Raphaël Colin
https://git.opencarp.org/Raphael
