2024-11-14

I hereby agree to the terms of the Contributor Agreement, version 1.2, with
MD5 checksum c66e0c1ecfef4f477d3636b89f118b65.

I furthermore declare that I am authorized and able to make this
agreement and sign this declaration.

Signed,

Hermenegild Arevalo
https://git.opencarp.org/hermenegild
