stages:
- prepare
- build
- test
- release
- archive

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DOCKER_DRIVER: overlay2
  GIT_STRATEGY: clone
  GIT_DEPTH: 1
  # source code including submodules
  INCLSUBMODULES_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/openCARP-inclSubmodules/${CI_COMMIT_TAG}
  INCLSUBMODULES_RELEASE: openCARP-${CI_COMMIT_TAG}-inclSubmodules.zip
  # Docker image
  DEPS_DOCKERFILE: docker/Dockerfile-deps
  DEPS_TEST_IMAGE: docker.opencarp.org/opencarp/opencarp/deps:$CI_COMMIT_REF_SLUG
  DEPS_IMAGE: docker.opencarp.org/opencarp/opencarp/deps:latest
  DOCKERFILE: docker/Dockerfile
  TEST_IMAGE: docker.opencarp.org/opencarp/opencarp:$CI_COMMIT_REF_SLUG
  RELEASE_IMAGE: docker.opencarp.org/opencarp/opencarp:latest
  DOCKER_ARCHIVE: opencarp-docker-${CI_COMMIT_TAG}.tar.gz
  # doxygen documentation
  DOXYGEN_PATH: docs/doxygen/html
  DOXYGEN_DESTINATION: 'opencarp@opencarp-web.ibt.kit.edu:/var/www/doxygen/${CI_COMMIT_REF_SLUG}/'
  # pdf user manual
  PDF_MANUAL_MAKER_DOCKERFILE: docker/Dockerfile-manual-maker
  PDF_MANUAL_MAKER_IMAGE: docker.opencarp.org/opencarp/opencarp/manual-maker:latest
  PDF_MANUAL: opencarp-manual-latest.pdf
  PDF_MANUAL_RELEASE: opencarp-manual-${CI_COMMIT_TAG}.pdf
  PDF_MANUAL_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-manual/${CI_COMMIT_TAG}
  PDF_MANUAL_DESTINATION: 'opencarp@opencarp-web.ibt.kit.edu:/var/www/manual/'
  # online parameters page
  HTML_PARAM_PATH: shared
  HTML_PARAM_DESTINATION: 'opencarp@opencarp-web.ibt.kit.edu:/var/www/parameters/${CI_COMMIT_REF_SLUG}/'
  # online autotester report
  AUTOTESTER_REPORT_DESTINATION: 'opencarp@opencarp-web.ibt.kit.edu:/var/www/tests/${CI_COMMIT_REF_SLUG}/'
  # DataCite XML
  DATACITE_PATH: opencarp.xml
  DATACITE_RELEASE: opencarp-${CI_COMMIT_TAG}.xml
  DATACITE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-datacite/${CI_COMMIT_TAG}
  # AppImage
  APPIMAGE_DOCKERFILE: docker/Dockerfile-appimage
  APPIMAGE_IMAGE: docker.opencarp.org/opencarp/opencarp/appimage:latest
  BUILD_DIR: _build
  APPIMAGE: openCARP-latest-x86_64.AppImage
  APPIMAGE_RELEASE: openCARP-${CI_COMMIT_TAG}-x86_64.AppImage
  APPIMAGE_RELEASE_NAME: openCARP-${CI_COMMIT_TAG}-x86_64_AppImage
  APPIMAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-appimage/${CI_COMMIT_TAG}
  # deb package
  DEB_BUILD_DIR: _build_ubuntu
  DEB_RELEASE: opencarp-${CI_COMMIT_TAG}.deb
  DEB_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-linux/${CI_COMMIT_TAG}
  # Variables used for building Ubuntu 20.04 deb package
  DEB_BUILD_DIR_UBUNTU20: _build_ubuntu20
  DEB_RELEASE_UBUNTU20: opencarp-${CI_COMMIT_TAG}-ubuntu20.deb
  DEPS_UBUNTU20_IMAGE: docker.opencarp.org/opencarp/opencarp/deps-ubuntu20:latest
  # rpm package
  RPM_PACKAGER_DOCKERFILE: docker/Dockerfile-rpm-packager
  RPM_PACKAGER_IMAGE: docker.opencarp.org/opencarp/opencarp/rpm-packager:latest
  RPM_BUILD_DIR: _build_centos
  RPM_RELEASE: opencarp-${CI_COMMIT_TAG}.rpm
  RPM_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-linux/${CI_COMMIT_TAG}
  # pkg package
  PKG_BUILD_DIR: _build_mac
  PKG_X86_64_RELEASE: opencarp-${CI_COMMIT_TAG}-x86_64.pkg
  PKG_ARM64_RELEASE: opencarp-${CI_COMMIT_TAG}-arm64.pkg
  PKG_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/opencarp-macos/${CI_COMMIT_TAG}
  # Spack package
  SPACK_DOCKERFILE: docker/Dockerfile-spack
  SPACK_IMAGE: docker.opencarp.org/opencarp/opencarp/spack:latest
  # Ginkgo Docker image
  GINKGO_DOCKERFILE: docker/Dockerfile-ginkgo
  GINKGO_IMAGE: docker.opencarp.org/opencarp/opencarp/ginkgo:latest
  # Metadata YAML files
  CREATORS_LOCATIONS: |
    ${CI_PROJECT_URL}/raw/master/codemeta.json
    https://git.opencarp.org/openCARP/carputils/raw/master/codemeta.json
  CONTRIBUTORS_LOCATIONS: https://git.opencarp.org/openCARP/openCARP-CDE/raw/master/codemeta.json
  CODEMETA_LOCATION: codemeta.json
  CFF_PATH: CITATION.cff
  # Releases
  RELEASE_TAG: ${CI_COMMIT_TAG}
  RELEASE_API_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases
  RELEASE_ARCHIVE_URL: ${CI_PROJECT_URL}/-/archive/${CI_COMMIT_TAG}/openCARP-${CI_COMMIT_TAG}.tar.gz
  RELEASE_DESCRIPTION: |
    Find the changelog [here](${CI_PROJECT_URL}/blob/master/CHANGELOG.md). <br>
    Find openCARP docker images for each release in
    [openCARP docker container registry](${CI_PROJECT_URL}/container_registry).
  # BagPack
  BAG_PATH: opencarp-${CI_COMMIT_TAG}
  BAG_DESTINATION: 'opencarp@opencarp-web.ibt.kit.edu:/var/www/bags/'
  # RADAR
  RADAR_PATH: opencarp-${CI_COMMIT_TAG}
  RADAR_BACKLINK: ${CI_PROJECT_URL}/-/releases
  SMTP_SERVER: smarthost.kit.edu
  NOTIFICATION_EMAIL: info@opencarp.org
  # GRAV
  PIPELINE: opencarp
  PIPELINE_SOURCE: .
  GRAV_PATH: opencarp.org
  # MLIR codegen
  MLIR_DEPS_DOCKERFILE: docker/Dockerfile-mlir-deps
  MLIR_DEPS_TEST_IMAGE: docker.opencarp.org/opencarp/opencarp/mlir-deps:$CI_COMMIT_REF_SLUG
  MLIR_DEPS_IMAGE: docker.opencarp.org/opencarp/opencarp/mlir-deps:latest
  MLIR_DOCKERFILE: docker/Dockerfile-mlir
  MLIR_TEST_IMAGE: docker.opencarp.org/opencarp/opencarp/mlir:$CI_COMMIT_REF_SLUG
  MLIR_IMAGE: docker.opencarp.org/opencarp/opencarp/mlir:latest
  CPU_MAKE_DEBUG_BUILD_DIR: _build_cpu_make_debug
  CPU_MAKE_RELEASE_BUILD_DIR: _build_cpu_make_release
  CPU_NINJA_DEBUG_BUILD_DIR: _build_cpu_ninja_debug
  CPU_NINJA_RELEASE_BUILD_DIR: _build_cpu_ninja_release
  CPU_MAKE_DEBUG_DATALAYOUT_BUILD_DIR: _build_cpu_make_debug_datalayout
  CPU_MAKE_RELEASE_DATALAYOUT_BUILD_DIR: _build_cpu_make_release_datalayout
  CPU_NINJA_DEBUG_DATALAYOUT_BUILD_DIR: _build_cpu_ninja_debug_datalayout
  CPU_NINJA_RELEASE_DATALAYOUT_BUILD_DIR: _build_cpu_ninja_release_datalayout
  CUDA_MAKE_DEBUG_BUILD_DIR: _build_cuda_make_debug
  CUDA_MAKE_RELEASE_BUILD_DIR: _build_cuda_make_release
  CUDA_NINJA_DEBUG_BUILD_DIR: _build_cuda_ninja_debug
  CUDA_NINJA_RELEASE_BUILD_DIR: _build_cuda_ninja_release

include:
- local: .gitlab/ci/appimage.gitlab-ci.yml
- local: .gitlab/ci/archive.gitlab-ci.yml
- local: .gitlab/ci/autotester.gitlab-ci.yml
- local: .gitlab/ci/datacite.gitlab-ci.yml
- local: .gitlab/ci/docker.gitlab-ci.yml
- local: .gitlab/ci/doxygen.gitlab-ci.yml
- local: .gitlab/ci/deb.gitlab-ci.yml
- local: .gitlab/ci/imps.gitlab-ci.yml
- local: .gitlab/ci/markdown.gitlab-ci.yml
- local: .gitlab/ci/rpm.gitlab-ci.yml
- local: .gitlab/ci/release.gitlab-ci.yml
- local: .gitlab/ci/spack.gitlab-ci.yml
- local: .gitlab/ci/tag-dropdowns.gitlab-ci.yml
- local: .gitlab/ci/pdf.gitlab-ci.yml
- local: .gitlab/ci/pkg.gitlab-ci.yml
- local: .gitlab/ci/html.gitlab-ci.yml
- local: .gitlab/ci/version-badge.gitlab-ci.yml
- local: .gitlab/ci/xbat.gitlab-ci.yml
- local: .gitlab/ci/mlir.gitlab-ci.yml
- local: .gitlab/ci/ginkgo.gitlab-ci.yml
