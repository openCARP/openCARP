// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file sv_init.h
* @brief 
* @author Jorge Sanchez, Aurel Neic
* @version 
* @date 2019-03-28
*/

#ifndef sv_init_h
#define sv_init_h

#ifdef __cplusplus
# include "MULTI_ION_IF.h"
#endif // ifdef __cplusplus
#include <string.h>

namespace limpet {

int   read_sv(MULTI_IF *, int, const char *);
void  save_sv(MULTI_IF *, int, const char *);
void  open_trace(MULTI_IF *, int, int *, int *);
void  dump_trace(MULTI_IF *, limpet::Real);
void  close_trace(MULTI_IF *);
int   current_global_node(int); // Used in error recovery
float current_global_time(void);  // Used in error recovery
void  set_start_time(const float); // Used in error recovery.
int   should_print_bounds_exceeded_messages(void);
int   set_print_bounds_exceeded_messages(int);

}  // namespace limpet

#endif
