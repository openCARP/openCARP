// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include <unordered_map>
#include <string>
#include <sstream>

#include "target.h"

namespace limpet {

  namespace {
    /** @brief A hash map that links enum member values to string
     * representations. */
    static std::unordered_map<std::string, Target const> const map = { {"cpu",
      Target::CPU}, {"mlir-cpu", Target::MLIR_CPU}, {"mlir-rocm",
        Target::MLIR_ROCM}, {"mlir-cuda", Target::MLIR_CUDA},
        {"auto", Target::AUTO}};
  }

Target get_target_from_string(std::string const str) {
  auto t = map.find(str);
  if (t == map.end()) {
    return Target::UNKNOWN;
  }
  else {
    return t->second;
  }
}

std::string get_string_from_target(Target const target) {
    for (auto t: map) {
        if (t.second == target) {
            return t.first;
        }
    }
    return "unknown";
}

std::string get_target_list_string() {
  std::stringstream list;
  for (auto target : map) {
    list << target.first << ", ";
  }
  // remove last ', '
  return list.str().substr(0, list.str().length() - 2);
}

bool is_gpu(Target const target) {
  return target == Target::MLIR_CUDA || target == Target::MLIR_ROCM;
}

bool is_concrete(Target const target) {
  return target > Target::UNKNOWN && target < Target::N_TARGETS;
}

} // namespace limpet
