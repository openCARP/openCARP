// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
 * @file target.h
 * @brief Defines valid targets for an ionic model to run on and an allocator
 * for allocating memory on a specific target
 */

#ifndef TARGETS_H
#define TARGETS_H

#include <stdexcept>
#include <iostream>
#include <string>
#include <cstring>
#ifdef HAS_ROCM_MODEL
#include <hip/hip_runtime.h>
#endif
#ifdef HAS_CUDA_MODEL
#include <cuda_runtime.h>
#endif

namespace limpet
{

/** @brief enum that represents different targets to run ionic
 * models on.
 */
enum Target {
  AUTO = -2,       // !< special value for chosing the target automatically
  UNKNOWN = -1,    //!< special value to handle unknown targets
  CPU,        //!< baseline CPU model generated with the original opencarp code generator
  MLIR_CPU,   //!< vectorized CPU code generated with MLIR
  MLIR_ROCM,  //!< ROCM code for AMD GPUs generated with MLIR
  MLIR_CUDA,  //!< CUDA code for NVIDIA GPUs generated with MLIR
  N_TARGETS,  //!< a token to indicate the maximum number of targets
};

/** @brief Returns a value from the @ref Target enum from a given string
 *
 * @param str lower case string corresponding to an enum member
 *
 * @returns an enum value corresponding to the given string, or
 * @ref Target::UNKNOWN if the string cannot be matched
 */
Target get_target_from_string(std::string const str);

/** @brief Get a string representation of a given target
 *
 * @param target target to get the string of
 *
 * @returns a string representing the given enum value target.
 * returns "unknown" if the target is unknown.
 */
std::string get_string_from_target(Target const target);

/** @brief Returns a string containing the list of available targets
 *
 * @returns a string containing a comma-separated list of targets.
 */
std::string get_target_list_string();

/** @brief Checks if this is a GPU target
 *
 * @param target target to check
 * @returns true for GPU targets, false otherwise
 */
bool is_gpu(Target const target);

/** @brief Checks if @p target is a real, concrete target.
 *
 * @param target target to check
 *
 * @retval false if the target is not concrete (Target::UNKNOWN, Target::AUTO,
 * ...) or invalid
 * @retval true if the target is valid and concrete
 */
bool is_concrete(Target const target);

/**
 * @brief Allocator structure for dynamically allocating memory on multiple
 * targets.
 *
 * This tries to respect the standard C++ allocator trait as much as possible
 * but is not yet used like one.
 */
template <typename T>
struct TargetAllocator {
  typedef T value_type; //!< type to allocate

  /**
   * @brief Construct a TargetAllocator.
   *
   * @param target target this allocator will allocate on
   * @param always_managed if true, always use managed memory management for
   * GPU targets
   */
  TargetAllocator(Target target, bool always_managed = false) : _target(target), _always_managed(always_managed) {
    if (!is_concrete(target)) {
      throw std::invalid_argument("attempting to construct a TargetAllocator with an invalid target");
    }
  }

  /**
   * @brief Get the target for this allocator.
   *
   * @returns this allocator's target
   */
  Target get_target() const {
    return this->_target;
  }

  /**
   * @brief Set a new target for this allocator
   *
   * @param new_target new target
   */
  void set_target(Target new_target) {
    if (!is_concrete(new_target)) {
      throw std::invalid_argument("attempting to set TargetAllocator to an invalid target");
    }
    else {
      this->_target = new_target;
    }
  }

  /**
   * @brief Allocate memory for type T.
   *
   * @param n number of objects of type T to allocate (size of the array)
   *
   * @returns a pointer to the allocated memory
   *
   * @note the allocated memory depends on this allocator's target, so using
   * the returned pointer in the wrong context can result in an error (for
   * example, dereferencing a GPU device pointer in CPU code)
   */
  T* allocate(std::size_t n, bool do_zero = true) {
    using type = typename std::conditional<std::is_void<T>::value, char, T>::type;
    T* ptr = nullptr;
    std::size_t n_bytes = n * sizeof(type);
    switch (this->_target) {
      case Target::MLIR_CPU:
      case Target::CPU: {
        std::allocator<type> std_alloc;
        if(n)
        {
          ptr = std_alloc.allocate(n);
          // Explicitely cast the pointer to silence warning on setting a
          // dynamic class object to 0.
          // The memory allocated by this function should be initialized after,
          // for example by using a placement new
          if(do_zero)
            std::memset((void *) ptr, 0, n_bytes);
        }


        break;
                        }
      case Target::MLIR_ROCM:
#ifdef HAS_ROCM_MODEL
                        {
        T* hip_ptr;
        hipError_t error;
        // Force usage of HIP managed memory even if it's not supported.
        // This should still preserve the semantics of managed memory, but
        // is slower. It can be useful to allocate small shared data between CPU
        // and GPU.
        if (this->_always_managed) {
          error = hipMallocManaged(&hip_ptr, n_bytes);
        }
        else {
          int device;
          int managed_memory = 0;
          hipGetDevice(&device);
          hipDeviceGetAttribute(&managed_memory, hipDeviceAttributeManagedMemory, device);
          // Check for HIP managed memory and fallback to a normal hipMalloc if
          // it's not supported. It should still allow the CPU to write to GPU memory.
          if (managed_memory) {
            error = hipMallocManaged(&hip_ptr, n_bytes);
          }
          else {
            error = hipMalloc(&hip_ptr, n_bytes);
          }
        }
        if (error != hipSuccess) {
          throw std::runtime_error(hipGetErrorString(error));
        }
        error = hipMemset(hip_ptr, 0, n_bytes);
        if (error != hipSuccess) {
          throw std::runtime_error(hipGetErrorString(error));
        }
        ptr = hip_ptr;
                        }
#else
        throw std::invalid_argument("target MLIR_ROCM is unavailable");
#endif
        break;
      case Target::MLIR_CUDA:
#ifdef HAS_CUDA_MODEL
        {
          T* cuda_ptr;
          cudaError_t error;
          // Always use unified memory for CUDA
          error = cudaMallocManaged(&cuda_ptr, n_bytes);
          if (error != cudaSuccess) {
            throw std::runtime_error(cudaGetErrorString(error));
          }
          error = cudaMemset(cuda_ptr, 0, n_bytes);
          if (error != cudaSuccess) {
            throw std::runtime_error(cudaGetErrorString(error));
          }
          ptr = cuda_ptr;
        }
#else
        throw std::invalid_argument("target MLIR_CUDA is unavailable");
#endif
        break;
      default:
        throw std::invalid_argument("unknown allocation target");
    }
    return ptr;
  }

  // Size is not needed
  /**
   * @brief Deallocate memory pointed by @p ptr.
   *
   * @param ptr pointer to the memory to deallocate
   * @param n size of the memory to deallocate. This is set to 0 by default
   * because it isn't needed for the function to work, but is required to fit
   * with the allocator trait interface.
   */
  void deallocate(T* ptr, std::size_t n = 0) {
    using type = typename std::conditional<std::is_void<T>::value, char, T>::type;
    switch (this->_target) {
      case Target::MLIR_CPU:
      case Target::CPU: {
        std::allocator<type> std_alloc;
        std_alloc.deallocate((type*) ptr, n);
      }
        break;
      case Target::MLIR_ROCM:
#ifdef HAS_ROCM_MODEL
        {
          hipError_t error;
          error = hipFree(ptr);
          if (error != hipSuccess) {
            throw std::runtime_error(hipGetErrorString(error));
          }
        }
#else
        throw std::invalid_argument("target MLIR_ROCM is unavailable");
#endif
        break;
      case Target::MLIR_CUDA:
#ifdef HAS_CUDA_MODEL
        {
          cudaError_t error;
          error = cudaFree(ptr);
          if (error != cudaSuccess) {
            throw std::runtime_error(cudaGetErrorString(error));
          }
        }
#else
        throw std::invalid_argument("target MLIR_CUDA is unavailable");
#endif
        break;
      default:
        throw std::invalid_argument("unknown allocation target");
    }
  }

 private:
  Target _target; //!< target for this allocator
  bool _always_managed; //!< whether to always use managed memory for GPU
};

/**
 * @brief Utility function for allocating memory on a target. See
 * TargetAllocator.
 *
 * @param target target to allocate on
 * @param n size of the memory to allocate (size of the array of type T)
 * @param always_managed whether to use managed memory for GPU allocations
 */
template<typename T>
T* allocate_on_target(Target target, std::size_t n, bool always_managed = false, bool do_zero = true) {
  TargetAllocator<T> alloc(target, always_managed);
  return alloc.allocate(n, do_zero);
}
/**
 * @brief Utility function for deallocating memory on a target. See
 * TargetAllocator.
 *
 * @param target target to deallocate on (this should be the target on which
 * memory was allocated)
 * @param ptr pointer to memory to deallocate
 */

template<typename T>
void deallocate_on_target(Target target, T* ptr) {
  TargetAllocator<T> alloc(target);
  return alloc.deallocate(ptr);
}

}  // namespace limpet

#endif  // TARGETS_H
