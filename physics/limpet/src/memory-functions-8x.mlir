// Helper functions for to load and store values from the C/C++ structs of the ionic models

// Vector Length: 8
func.func private @load_gather_vector_8xf32(!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf32>) -> vector<8xf32>
func.func private @store_scatter_vector_8xf32(!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf32>) -> ()

func.func private @load_gather_vector_8xf64(!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> vector<8xf64>
func.func private @store_scatter_vector_8xf64(!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> ()

func.func @load_consecutive_elements_to_vector_8xf32(%base_address: memref<8xf32>, %offset : i32) -> vector<8xf32> {
    %offset_index = arith.index_cast %offset : i32 to index
    %vec = vector.load %base_address[%offset_index] : memref<8xf32>, vector<8xf32>
    return %vec: vector<8xf32>
}

func.func @store_consecutive_elements_from_vector_8xf32(%base_address: memref<8xf32>, %vec : vector<8xf32>, %offset : i32) {
    %offset_index = arith.index_cast %offset : i32 to index
    vector.store %vec, %base_address[%offset_index] : memref<8xf32>, vector<8xf32>
    return
}

func.func @load_struct_to_vector_8xf32(%base_address: !llvm.ptr, %initial_offset : i32, %struct_size : i32) -> vector<8xf32> {

    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i1
    %vector_mask = vector.constant_mask [8] : vector<8xi1>

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %struct_size : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %zero_f32 = arith.constant 0.0 : f32
    %pass_through = vector.broadcast %zero_f32 : f32 to vector<8xf32>
    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    %loaded_vec = func.call @load_gather_vector_8xf32(%val_ptr, %vec_index, %vector_mask, %pass_through) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf32>) -> vector<8xf32>
    return %loaded_vec : vector<8xf32>
}

func.func @store_struct_from_vector_8xf32(%base_address: !llvm.ptr, %stored_value : vector<8xf32>, %initial_offset : i32, %struct_size : i32) {

    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i1
    %vector_mask = vector.constant_mask [8] : vector<8xi1>

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %struct_size : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %zero_f32 = arith.constant 0.0 : f32
    %pass_through = vector.broadcast %zero_f32 : f32 to vector<8xf32>
    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    func.call @store_scatter_vector_8xf32(%val_ptr, %vec_index, %vector_mask, %stored_value) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf32>) -> ()
    return
}

func.func @load_consecutive_elements_to_vector_f64_from_type_8xf32(%base_address: memref<8xf32>, %offset: i32) -> vector<8xf64> {
    %offset_index = arith.index_cast %offset : i32 to index
    %vec_f32 = vector.load %base_address[%offset_index] : memref<8xf32>, vector<8xf32>
    %vec = arith.extf %vec_f32: vector<8xf32> to vector<8xf64>
    return %vec: vector<8xf64>
}

func.func @store_consecutive_elements_from_vector_f64_from_type_8xf32(%base_address: memref<8xf32>, %vec : vector<8xf64>, %offset: i32) {
    %offset_index = arith.index_cast %offset : i32 to index
    %vec_f32 = arith.truncf %vec: vector<8xf64> to vector<8xf32>
    vector.store %vec_f32, %base_address[%offset_index] : memref<8xf32>, vector<8xf32>
    return
}

func.func @load_struct_to_vector_f64_from_type_8xf32(%base_address: !llvm.ptr, %element_offset : i32, %struct_size : i32) -> vector<8xf64> {
    %vec = func.call @load_struct_to_vector_8xf32(%base_address, %element_offset, %struct_size) : (!llvm.ptr, i32, i32) -> vector<8xf32>
    %vec_f64 = llvm.fpext %vec : vector<8xf32> to vector<8xf64>
    return %vec_f64 : vector<8xf64>
}

func.func @store_struct_from_vector_f64_from_type_8xf32(%base_address: !llvm.ptr, %vec : vector<8xf64>, %element_offset : i32, %struct_size : i32) {
    %vec_f32 = llvm.fptrunc %vec : vector<8xf64> to vector<8xf32>
    func.call @store_struct_from_vector_8xf32(%base_address, %vec_f32, %element_offset, %struct_size) : (!llvm.ptr, vector<8xf32>, i32, i32) -> ()
    return
}

func.func @load_consecutive_elements_to_vector_8xf64(%base_address: memref<8xf64>, %offset : i32) -> vector<8xf64> {
    %offset_index = arith.index_cast %offset : i32 to index
    %vec = vector.load %base_address[%offset_index] : memref<8xf64>, vector<8xf64>
    return %vec: vector<8xf64>
}

func.func @store_consecutive_elements_from_vector_8xf64(%base_address: memref<8xf64>, %vec : vector<8xf64>, %offset : i32) {
    %offset_index = arith.index_cast %offset : i32 to index
    vector.store %vec, %base_address[%offset_index] : memref<8xf64>, vector<8xf64>
    return
}

func.func @load_single_value_struct_to_vector_8xf64(%base_address: !llvm.ptr, %offset : i32) -> vector<8xf64> {
    %val_ptr = llvm.getelementptr %base_address[%offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    %val = llvm.load %val_ptr : !llvm.ptr -> f64
    %vec = vector.broadcast %val : f64 to vector<8xf64>
    return %vec :  vector<8xf64>
}

func.func @load_struct_to_vector_8xf64(%base_address: !llvm.ptr, %initial_offset : i32, %struct_size : i32) -> vector<8xf64> {

    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i1
    %vector_mask = vector.constant_mask [8] : vector<8xi1>

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %struct_size : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %zero_f64 = arith.constant 0.0 : f64
    %pass_through = vector.broadcast %zero_f64 : f64 to vector<8xf64>
    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    %loaded_vec = func.call @load_gather_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %pass_through) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> vector<8xf64>
    return %loaded_vec : vector<8xf64>
}

func.func @store_struct_from_vector_8xf64(%base_address: !llvm.ptr, %stored_value : vector<8xf64>, %initial_offset : i32, %struct_size : i32) {

    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i1
    %vector_mask = vector.constant_mask [8] : vector<8xi1>

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %struct_size : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %zero_f64 = arith.constant 0.0 : f64
    %pass_through = vector.broadcast %zero_f64 : f64 to vector<8xf64>
    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    func.call @store_scatter_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %stored_value) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> ()
    return
}

func.func private @LUT_interpRow_mlir(memref<i8>, i32, !llvm.ptr, i32, i32)

func.func @LUT_interpRow_n_elements_vector_8xf64(%table: memref<i8>, %vec: vector<8xf64>,
                                            %n: i32, %row_ptr: !llvm.ptr,
                                            %lut_numelements: i32) {
    %vs = arith.constant 8 : i32
    
    %zero = arith.constant 0 : i32
    %eight = arith.constant 8 : i32
    
    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    // vector.print %vec : vector<8xf64>

    %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args (%offset = %zero) -> i32 {
        %index_int = arith.index_cast %iv : index to i32
        %extracted_value = vector.extractelement %vec[%index_int : i32] : vector<8xf64>
        %extracted_value_ptr = llvm.getelementptr %row_ptr[%offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
        llvm.store %extracted_value, %extracted_value_ptr : f64, !llvm.ptr
        %new_offset = llvm.add %offset, %eight : i32
        scf.yield %new_offset : i32
    }
    
    
    
    // func.call @store_struct_from_vector_8xf64(%row_ptr, %vec, %zero, %eight) : (!llvm.ptr, vector<8xf64>, i32, i32) -> ()
    
    
    func.call @LUT_interpRow_mlir(%table, %zero, %row_ptr, %lut_numelements, %vs)
        : (memref<i8>, i32, !llvm.ptr, i32, i32) -> ()
    return
}

func.func @load_conditional_struct_to_vector_8xf64(%base_address : !llvm.ptr, %initial_offset: i32,
                                                       %state_struct_size: i32,
                                                       %vector_mask : vector<8xi1>,
                                                       %pass_through: vector<8xf64>) -> vector<8xf64> {
    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %three = arith.constant 3 : i32
    %state_struct_size_double = llvm.lshr %state_struct_size, %three : i32

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %state_struct_size_double : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    %speculative_load = func.call @load_gather_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %pass_through) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> vector<8xf64>
    return %speculative_load : vector<8xf64>
}

func.func @store_conditional_vector_to_struct_8xf64(%base_address : !llvm.ptr,
                                                      %initial_offset : i32, %state_struct_size: i32,
                                                      %vector_mask : vector<8xi1>,
                                                      %stored_value: vector<8xf64>) {
    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %three = arith.constant 3 : i32
    %state_struct_size_double = llvm.lshr %state_struct_size, %three : i32

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %state_struct_size_double : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    func.call @store_scatter_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %stored_value) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> ()
    return
}

func.func @load_consecutive_conditional_struct_to_vector_8xf64(%base_address : !llvm.ptr, %initial_offset: i32,
                                                       %state_struct_size: i32,
                                                       %vector_mask : vector<8xi1>,
                                                       %pass_through: vector<8xf64>) -> vector<8xf64> {
    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i32

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %one : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    %speculative_load = func.call @load_gather_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %pass_through) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> vector<8xf64>
    return %speculative_load : vector<8xf64>
}

func.func @store_consecutive_conditional_vector_to_struct_8xf64(%base_address : !llvm.ptr,
                                                      %initial_offset : i32, %state_struct_size: i32,
                                                      %vector_mask : vector<8xi1>,
                                                      %stored_value: vector<8xf64>) {
    %zero_index = arith.constant 0 : index
    %one_index = arith.constant 1 : index
    %last_index = arith.constant 8 : index

    %zero_offset = arith.constant 0 : i32
    %one = arith.constant 1 : i32

    // Create vector
    %vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
    %vec_index, %offset = scf.for %iv = %zero_index to %last_index step %one_index
     iter_args(%vec = %vec_index_init, %offset = %zero_offset) -> (vector<8xi32>, i32) {
        %index_i32 = arith.index_cast %iv : index to i32
        %newvec = vector.insertelement %offset, %vec[%index_i32 : i32] : vector<8xi32>
        %new_offset = llvm.add %offset, %one : i32
        scf.yield %newvec, %new_offset : vector<8xi32>, i32
    }

    %val_ptr = llvm.getelementptr %base_address[%initial_offset] : (!llvm.ptr, i32) -> !llvm.ptr, i8
    func.call @store_scatter_vector_8xf64(%val_ptr, %vec_index, %vector_mask, %stored_value) : (!llvm.ptr, vector<8xi32>, vector<8xi1>, vector<8xf64>) -> ()
    return
}
