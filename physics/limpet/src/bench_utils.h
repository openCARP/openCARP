// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _BENCH_UTILS_H
#define _BENCH_UTILS_H

#include "cmdline.h"
#include "restitute.h"

namespace limpet {

enum TimerIDs {
  CON_TM_IDX,   // IO timer for console output
  SVD_TM_IDX,   // IO timer for state variable output
  STA_TM_IDX,   // IO timer for state saves (checkpointing)
  SSV_TM_IDX,   // IO timer for single state vector saves
  STM_TM_IDX,   // timer for stimulus trigger
  LIGHT_TM_IDX, // timer for illumination trigger
  TRACE_TM_IDX,  // timer for trace output
  CLAMP_TM_IDX,  // timer for trace output
  DOPPLE_TM_IDX,
  RES_SAVE_TM_IDX,  // timer for saving state vectors during restitution
  N_TIMERS      // number of IO Timers we are going to use
};

#define TIME_OUT     1.     // output period for data
#define NUMSEG       1          // number of nodes or nodes

// STATE VARIABLE DUMPS
#define DT_SV_DUMP 1.           // output interval for sv dumps

// typedefs
struct IOCtrl {
  char w2file;    //!< write to file
  char wbin;      //!< write to file in binary format
  char wsplt;     //!< split -> each vector goes into separate file
  char w2stdout;  //!< turn on/off output to stdout
  char first;     //!< first line of output
};

struct GVEC_DUMP {
  opencarp::FILE_SPEC hdls[NUM_IMP_DATA_TYPES+1];   //!< array of file handles to gvec output files
  char*     fn[NUM_IMP_DATA_TYPES+1];   //!< array to store file names
  int       dtype[NUM_IMP_DATA_TYPES+1]; //!< data type
  int       n_dumps;    //!< keep track of number of dumped time slices
};

// record statistics on the duration of events
#define us_RESOLUTION 1
struct event_timing {
  bool   init;   //!< initialization flag
  double mn;     //!< minimum duration of event
  double mx;     //!< maximum duration of event
  double avg;    //!< average duration of event
  double tot;    //!< total duration of all events
  int    count;  //!< number of events counted so far
};

enum timingIDs {
  SETUP_IDX,        //!< timing for setup phase
  INIT_IDX,         //!< timing for initialization
  LOOP_IDX,         //!< timing for main loop (including IO and ODE solve)
  ODE_IDX,          //!< timing for ODE solve
  N_TIMINGS         //!< number of benchmark timings we use
};

//!data type IDs
typedef enum {
  dtype_Gatetype,
  dtype_Real,
  dtype_Float,
  dtype_Char,
  dtype_Integer,
  dtype_Double,
  dtype_Short,
  dtype_GlobalData_t,
  dtype_long,
  dtype_bool
} DataType;

//!strings for each enum type
static const char * const data_type_names[] = {
  "Gatetype",
  "Real",
  "float",
  "char",
  "int",
  "double",
  "short",
  "GlobalData_t",
  "long",
  "bool"
};

// Gatetype is defined in LIMPET only and typically defined as float
// if defined otherwise, the size has to be overruled within LIMPET
// in this case the const qualifier has to be removed.
#define GATETYPE_SIZE      4
#define GLOBALDATA_T_SIZE  8
//!strings for each enum type
static const int data_type_sizes[] = {
  GATETYPE_SIZE,
  sizeof(limpet::Real),
  sizeof(float),
  sizeof(char),
  sizeof(int),
  sizeof(double),
  sizeof(short),
  GLOBALDATA_T_SIZE,
  sizeof(long),
  sizeof(bool)
};


int  write_dump_header(GVEC_DUMP *gvd, SV_DUMP *svd, const char *ExpID);
void open_globalvec_dump(FILE **fhdls, GVEC_DUMP *gvd, MULTI_IF *pMIIF, char *base_name, IOCtrl *io);
void globalvec_dump(FILE **fhdls, GVEC_DUMP *gvd, MULTI_IF *pMIIF, opencarp::timer_manager *tm, IOCtrl *io, int numNode);
void close_globalvec_dump(FILE **fhdls, GVEC_DUMP *gvd, IOCtrl *io);
void dump_all(MULTI_IF *, int, char *, char *, double, double, char *);
void update_timing(event_timing *t, double event_duration);
void initialize_timings(event_timing *t);
double getCellVal(opencarp::sf_vec* v, int ind);
void initial_SVs(MULTI_IF *miif, char *SVs, char *imp, char *plgins, int num);
void print_param_help(IonType* im, IonTypeList& plugs);
float determine_duration(struct gengetopt_args_info *p, TrgList *stim_lst);
void determine_stim_list(char *stl, TrgList *trg, bool DIAs);

}  // namespace limpet

#endif
