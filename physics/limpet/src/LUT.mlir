// Vector Size: 8
func.func private @LUT_print(f64)

func.func @compute_LUT_if_else_8xf64(%tab1: memref<8xf64>, %tab2: memref<8xf64>, %x : f64, %res: memref<8xf64>,%lut_elements : i32, %tab1_index : i32, %tab2_index : i32, %res_index : i32) {
	
	%1 = arith.constant dense<1.000000e+00> : vector<8xf64>
	%2 = vector.broadcast %x : f64 to vector<8xf64>
	%3 = arith.subf %1, %2 : vector<8xf64>
	
	%eight = arith.constant 8 : i32
	%4 = arith.remui %lut_elements, %eight : i32
	%5 = arith.cmpi "sge", %lut_elements, %eight : i32
	%6 = arith.subi %lut_elements, %4 : i32
	
	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%eight_index = arith.constant 8 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %eight_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {

			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
			
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<8xf64>, vector<8xf64>
        		
        		%tab2_full_index = arith.addi %tab2_index, %iv_int : i32
			%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
			
        		%77 = vector.load %tab2[%tab2_full_offset] : memref<8xf64>, vector<8xf64>
         
        		%66 = arith.mulf %3, %55 : vector<8xf64>
        		%8 = arith.mulf %2, %77 : vector<8xf64>
        		%9 = arith.addf %66, %8 : vector<8xf64>
        		

        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %9, %res[%res_full_offset] : memref<8xf64>, vector<8xf64>
        		
			%new_offset = llvm.add %offset_loop, %eight : i32
			scf.yield %new_offset : i32
		}
	}
	

	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32
	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<8xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 8 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<8xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<8xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<8xi32>, i32
		}
	
		
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index	
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64> into vector<8xf64>
		
		%tab2_full_index = arith.addi %tab2_index, %6 : i32
		%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
		
		%77 = vector.gather %tab2[%tab2_full_offset][%vec_index], %11, %1 : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64> into vector<8xf64>
         	
		%66 = arith.mulf %3, %55 : vector<8xf64>
		%8 = arith.mulf %2, %77 : vector<8xf64>
		%9 = arith.addf %66, %8 : vector<8xf64>
		
		
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		vector.scatter %res[%res_full_offset][%vec_index], %11, %9 : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64>
		
	}
	
	
	return
}

func.func @compute_LUT_if_then_8xf64(%tab1: memref<8xf64>, %res: memref<8xf64>, %lut_elements : i32, %tab1_index : i32, %res_index : i32) {
	%1 = arith.constant dense<1.000000e+00> : vector<8xf64>
	%eight = arith.constant 8 : i32
	%4 = arith.remui %lut_elements, %eight : i32
	%5 = arith.cmpi "sge", %lut_elements, %eight : i32
	%6 = arith.subi %lut_elements, %4 : i32

	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%eight_index = arith.constant 8 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %eight_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {
			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<8xf64>, vector<8xf64>
        		

        		
        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %55, %res[%res_full_offset] : memref<8xf64>, vector<8xf64>

			%new_offset = llvm.add %offset_loop, %eight : i32
			scf.yield %new_offset : i32
		}
	}
	
	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32

	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<8xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 8 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<8xi32>) : vector<8xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<8xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<8xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<8xi32>, i32
		}
	
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64> into vector<8xf64>
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		
		vector.scatter %res[%res_full_offset][%vec_index], %11, %55 : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64>
	}
	return

}


func.func @compute_LUT_bound_8xf64(%idx_row : vector<8xf64>, %res : f64, %val_row : vector<8xf64>, %ma : f64, %mn : f64, %lut_ele : i32,%mn_ind : i32, %tab1 : memref<8xf64>, %res_row : memref<8xf64>) {
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	// computing LUT_Error	
	//%4 = vector.broadcast %res : f64 to vector<8xf64>
	//%5 = arith.mulf %idx_row, %4 : vector<8xf64>
	//%6 = arith.subf %val_row, %5 : vector<8xf64>
	//%7 = arith.divf %6, %4 : vector<8xf64>
	
	%res_f32 = arith.truncf %res : f64 to f32
	%f32_4 = vector.broadcast %res_f32 : f32 to vector<8xf32>
	%idx_row_f32 = arith.truncf %idx_row : vector<8xf64> to vector<8xf32>
	%55 = arith.mulf %idx_row_f32, %f32_4 : vector<8xf32>
	
	%4 = vector.broadcast %res : f64 to vector<8xf64>
	%5 = arith.extf %55 : vector<8xf32> to vector<8xf64>
	
	
	%6 = arith.subf %val_row, %5 : vector<8xf64>
	%7 = arith.divf %6, %4 : vector<8xf64>

	// computing LUT_out_of_bounds
	%8 = vector.broadcast %ma : f64 to vector<8xf64>
	%9 = vector.broadcast %mn : f64 to vector<8xf64>
	%11 = arith.cmpf olt, %val_row, %9 : vector<8xf64>
	%12 = arith.cmpf ogt, %val_row, %8 : vector<8xf64>
	%13 = arith.ori %11, %12 : vector<8xi1>
	
	%zero_index = arith.constant 0 : index
	%one_index = arith.constant 1 : index
	%last_index = arith.constant 8 : index


	scf.for %iv = %zero_index to %last_index step %one_index
	iter_args () -> () {
	
        	%index_int = arith.index_cast %iv : index to i32
        	%derr = vector.extractelement %7[%index_int : i32] : vector<8xf64>
        	%bound = vector.extractelement %13[%index_int : i32] : vector<8xi1>
        	
        	%x_idx = vector.extractelement %idx_row[%index_int : i32] : vector<8xf64>
        	%idx = arith.fptosi %x_idx : f64 to i32
        	

        	%one = arith.constant 1 : i32
        	%idx_1 = arith.addi %idx, %one : i32
        	
        	%l1 = arith.subi %idx, %mn_ind : i32
        	%l2 = arith.muli %l1, %lut_ele : i32
        	
        	%l3 = arith.muli %index_int, %lut_ele : i32       	 
        	
        	scf.if %bound {
        		func.call @compute_LUT_if_then_8xf64(%tab1, %res_row, %lut_ele, %l2, %l3) : (memref<8xf64>, memref<8xf64>, i32, i32, i32) -> ()
        	
        	}
        	else {
        		%l4 = arith.subi %idx_1, %mn_ind : i32
        		%l5 = arith.muli %l4, %lut_ele : i32
        
        		func.call @compute_LUT_if_else_8xf64(%tab1, %tab1, %derr, %res_row, %lut_ele, %l2, %l5, %l3) : (memref<8xf64>, memref<8xf64>, f64, memref<8xf64>, i32, i32, i32, i32) -> ()
        		
        		
        	}	
    	}
	
	return
}

func.func private @LUT_problem_mlir(memref<i8>, f64, i32)

func.func @compute_LUT_interpRow_mlir_8xf64(%step : f64, %mn : f64, %mx : f64, %t_res : f64, %lut_ele : i32, %mn_ind : f64, %tab1 : memref<8xf64>, %res : memref<8xf64>, %no_imp_fast : i1, %cell_id : i32, %mx_ind : f64, %name : memref<i8>) {

	
	%tab_mn_ind = arith.fptosi %mn_ind : f64 to i32

	%1 = vector.broadcast %step : f64 to vector<8xf64>
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	%2 = vector.load %res[%offset_index] : memref<8xf64>, vector<8xf64> // val
	%idx_3 = arith.mulf %2, %1 : vector<8xf64> // idx
	
	%idx_i32 = arith.fptosi %idx_3 : vector<8xf64> to vector<8xi32>
	%3 = arith.sitofp %idx_i32 : vector<8xi32> to vector<8xf64>


	scf.if %no_imp_fast {
		%p1 = vector.broadcast %mn_ind : f64 to vector<8xf64>
		%p2 = vector.broadcast %mx_ind : f64 to vector<8xf64>
		
		%p3 = arith.cmpf olt, %3, %p1 : vector<8xf64>
		%p4 = arith.cmpf ogt, %3, %p2 : vector<8xf64>
		
		%p_zero_index = arith.constant 0 : index
		%p_one_index = arith.constant 1 : index
		%p_last_index = arith.constant 8 : index


		scf.for %iv = %p_zero_index to %p_last_index step %p_one_index
		iter_args () -> () {
			%index_int = arith.index_cast %iv : index to i32
			%p5 = vector.extractelement %p3[%index_int : i32] : vector<8xi1>
			%p6 = vector.extractelement %p4[%index_int : i32] : vector<8xi1>
			
			scf.if %p5 {
				%index_i32 = arith.index_cast %iv : index to i32
				vector.insertelement %mn_ind, %3[%index_i32 : i32] : vector<8xf64>
				%p7 = vector.extractelement %2[%index_int : i32] : vector<8xf64>
				%p8 = arith.addi %cell_id, %index_int : i32
				func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> () 
			} 
			else  { 
				scf.if %p6 {
					%index_i32 = arith.index_cast %iv : index to i32
					vector.insertelement %mx_ind, %3[%index_i32 : i32] : vector<8xf64>
					%p7 = vector.extractelement %2[%index_int : i32] : vector<8xf64>
					%p8 = arith.addi %cell_id, %index_int : i32
					func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> ()  
				}
			}
		}
	}
	
	
	func.call @compute_LUT_bound_8xf64(%3, %t_res, %2, %mx, %mn, %lut_ele, %tab_mn_ind, %tab1, %res) : (vector<8xf64>, f64, vector<8xf64>, f64, f64, i32, i32, memref<8xf64>, memref<8xf64>) -> ()
	
	return
}

// Vector Size: 4

func.func @compute_LUT_if_else_4xf64(%tab1: memref<4xf64>, %tab2: memref<4xf64>, %x : f64, %res: memref<4xf64>,%lut_elements : i32, %tab1_index : i32, %tab2_index : i32, %res_index : i32) {
	
	%1 = arith.constant dense<1.000000e+00> : vector<4xf64>
	%2 = vector.broadcast %x : f64 to vector<4xf64>
	%3 = arith.subf %1, %2 : vector<4xf64>
	
	%four = arith.constant 4 : i32
	%4 = arith.remui %lut_elements, %four : i32
	%5 = arith.cmpi "sge", %lut_elements, %four : i32
	%6 = arith.subi %lut_elements, %4 : i32
	
	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%four_index = arith.constant 4 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %four_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {
		
			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
			
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<4xf64>, vector<4xf64>
        		
        		%tab2_full_index = arith.addi %tab2_index, %iv_int : i32
			%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
			
        		%77 = vector.load %tab2[%tab2_full_offset] : memref<4xf64>, vector<4xf64>
         
        		%66 = arith.mulf %3, %55 : vector<4xf64>
        		%8 = arith.mulf %2, %77 : vector<4xf64>
        		%9 = arith.addf %66, %8 : vector<4xf64>
        		

        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %9, %res[%res_full_offset] : memref<4xf64>, vector<4xf64>
        		
			%new_offset = llvm.add %offset_loop, %four : i32
			scf.yield %new_offset : i32
		}
	}
	

	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32
	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<4xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 4 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<4xi32>) : vector<4xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<4xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<4xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<4xi32>, i32
		}
	
		
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index	
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64> into vector<4xf64>
		
		%tab2_full_index = arith.addi %tab2_index, %6 : i32
		%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
		
		%77 = vector.gather %tab2[%tab2_full_offset][%vec_index], %11, %1 : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64> into vector<4xf64>
         	
		%66 = arith.mulf %3, %55 : vector<4xf64>
		%8 = arith.mulf %2, %77 : vector<4xf64>
		%9 = arith.addf %66, %8 : vector<4xf64>
		
		
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		vector.scatter %res[%res_full_offset][%vec_index], %11, %9 : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64>
		
	}
	
	
	return
}

func.func @compute_LUT_if_then_4xf64(%tab1: memref<4xf64>, %res: memref<4xf64>, %lut_elements : i32, %tab1_index : i32, %res_index : i32) {
	%1 = arith.constant dense<1.000000e+00> : vector<4xf64>
	%four = arith.constant 4 : i32
	%4 = arith.remui %lut_elements, %four : i32
	%5 = arith.cmpi "sge", %lut_elements, %four : i32
	%6 = arith.subi %lut_elements, %4 : i32

	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%four_index = arith.constant 4 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %four_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {
			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<4xf64>, vector<4xf64>
        		

        		
        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %55, %res[%res_full_offset] : memref<4xf64>, vector<4xf64>

			%new_offset = llvm.add %offset_loop, %four : i32
			scf.yield %new_offset : i32
		}
	}
	
	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32

	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<4xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 4 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<4xi32>) : vector<4xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<4xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<4xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<4xi32>, i32
		}
	
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64> into vector<4xf64>
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		
		vector.scatter %res[%res_full_offset][%vec_index], %11, %55 : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64>
	}
	return

}


func.func @compute_LUT_bound_4xf64(%idx_row : vector<4xf64>, %res : f64, %val_row : vector<4xf64>, %ma : f64, %mn : f64, %lut_ele : i32,%mn_ind : i32, %tab1 : memref<4xf64>, %res_row : memref<4xf64>) {
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	// computing LUT_Error	
	%res_f32 = arith.truncf %res : f64 to f32
	%f32_4 = vector.broadcast %res_f32 : f32 to vector<4xf32>
	%idx_row_f32 = arith.truncf %idx_row : vector<4xf64> to vector<4xf32>
	%55 = arith.mulf %idx_row_f32, %f32_4 : vector<4xf32>
	
	%4 = vector.broadcast %res : f64 to vector<4xf64>
	%5 = arith.extf %55 : vector<4xf32> to vector<4xf64>
	
	
	%6 = arith.subf %val_row, %5 : vector<4xf64>
	%7 = arith.divf %6, %4 : vector<4xf64>

	// computing LUT_out_of_bounds
	%8 = vector.broadcast %ma : f64 to vector<4xf64>
	%9 = vector.broadcast %mn : f64 to vector<4xf64>
	%11 = arith.cmpf olt, %val_row, %9 : vector<4xf64>
	%12 = arith.cmpf ogt, %val_row, %8 : vector<4xf64>
	%13 = arith.ori %11, %12 : vector<4xi1>
	
	%zero_index = arith.constant 0 : index
	%one_index = arith.constant 1 : index
	%last_index = arith.constant 4 : index


	scf.for %iv = %zero_index to %last_index step %one_index
	iter_args () -> () {
	
        	%index_int = arith.index_cast %iv : index to i32
        	%derr = vector.extractelement %7[%index_int : i32] : vector<4xf64>
        	%bound = vector.extractelement %13[%index_int : i32] : vector<4xi1>
        	
        	%x_idx = vector.extractelement %idx_row[%index_int : i32] : vector<4xf64>
        	%idx = arith.fptosi %x_idx : f64 to i32
        	

        	%one = arith.constant 1 : i32
        	%idx_1 = arith.addi %idx, %one : i32
        	
        	%l1 = arith.subi %idx, %mn_ind : i32
        	%l2 = arith.muli %l1, %lut_ele : i32
        	
        	%l3 = arith.muli %index_int, %lut_ele : i32       	 
        	
        	scf.if %bound {
        		func.call @compute_LUT_if_then_4xf64(%tab1, %res_row, %lut_ele, %l2, %l3) : (memref<4xf64>, memref<4xf64>, i32, i32, i32) -> ()
        	
        	}
        	else {
        		%l4 = arith.subi %idx_1, %mn_ind : i32
        		%l5 = arith.muli %l4, %lut_ele : i32
        
        		func.call @compute_LUT_if_else_4xf64(%tab1, %tab1, %derr, %res_row, %lut_ele, %l2, %l5, %l3) : (memref<4xf64>, memref<4xf64>, f64, memref<4xf64>, i32, i32, i32, i32) -> ()
        		
        		
        	}	
    	}
	
	return
}


func.func @compute_LUT_interpRow_mlir_4xf64(%step : f64, %mn : f64, %mx : f64, %t_res : f64, %lut_ele : i32, %mn_ind : f64, %tab1 : memref<4xf64>, %res : memref<4xf64>, %no_imp_fast : i1, %cell_id : i32, %mx_ind : f64, %name : memref<i8>) {

	
	%tab_mn_ind = arith.fptosi %mn_ind : f64 to i32

	%1 = vector.broadcast %step : f64 to vector<4xf64>
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	%2 = vector.load %res[%offset_index] : memref<4xf64>, vector<4xf64> // val
	%idx_3 = arith.mulf %2, %1 : vector<4xf64> // idx
	
	%idx_i32 = arith.fptosi %idx_3 : vector<4xf64> to vector<4xi32>
	%3 = arith.sitofp %idx_i32 : vector<4xi32> to vector<4xf64>

	scf.if %no_imp_fast {
		%p1 = vector.broadcast %mn_ind : f64 to vector<4xf64>
		%p2 = vector.broadcast %mx_ind : f64 to vector<4xf64>
		
		%p3 = arith.cmpf olt, %3, %p1 : vector<4xf64>
		%p4 = arith.cmpf ogt, %3, %p2 : vector<4xf64>
		
		%p_zero_index = arith.constant 0 : index
		%p_one_index = arith.constant 1 : index
		%p_last_index = arith.constant 4 : index


		scf.for %iv = %p_zero_index to %p_last_index step %p_one_index
		iter_args () -> () {
			%index_int = arith.index_cast %iv : index to i32
			%p5 = vector.extractelement %p3[%index_int : i32] : vector<4xi1>
			%p6 = vector.extractelement %p4[%index_int : i32] : vector<4xi1>
			
			scf.if %p5 {
				%index_i32 = arith.index_cast %iv : index to i32
				vector.insertelement %mn_ind, %3[%index_i32 : i32] : vector<4xf64>
				%p7 = vector.extractelement %2[%index_int : i32] : vector<4xf64>
				%p8 = arith.addi %cell_id, %index_int : i32
				func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> () 
			} 
			else  { 
				scf.if %p6 {
					%index_i32 = arith.index_cast %iv : index to i32
					vector.insertelement %mx_ind, %3[%index_i32 : i32] : vector<4xf64>
					%p7 = vector.extractelement %2[%index_int : i32] : vector<4xf64>
					%p8 = arith.addi %cell_id, %index_int : i32
					func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> ()  
				}
			}
		}
	}
	
	
	func.call @compute_LUT_bound_4xf64(%3, %t_res, %2, %mx, %mn, %lut_ele, %tab_mn_ind, %tab1, %res) : (vector<4xf64>, f64, vector<4xf64>, f64, f64, i32, i32, memref<4xf64>, memref<4xf64>) -> ()
	
	return
}

// Vector Size: 2

func.func @compute_LUT_if_else_2xf64(%tab1: memref<2xf64>, %tab2: memref<2xf64>, %x : f64, %res: memref<2xf64>,%lut_elements : i32, %tab1_index : i32, %tab2_index : i32, %res_index : i32) {
	
	%1 = arith.constant dense<1.000000e+00> : vector<2xf64>
	%2 = vector.broadcast %x : f64 to vector<2xf64>
	%3 = arith.subf %1, %2 : vector<2xf64>
	
	%two = arith.constant 2 : i32
	%4 = arith.remui %lut_elements, %two : i32
	%5 = arith.cmpi "sge", %lut_elements, %two : i32
	%6 = arith.subi %lut_elements, %4 : i32
	
	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%two_index = arith.constant 2 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %two_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {
		
			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
			
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<2xf64>, vector<2xf64>
        		
        		%tab2_full_index = arith.addi %tab2_index, %iv_int : i32
			%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
			
        		%77 = vector.load %tab2[%tab2_full_offset] : memref<2xf64>, vector<2xf64>
         
        		%66 = arith.mulf %3, %55 : vector<2xf64>
        		%8 = arith.mulf %2, %77 : vector<2xf64>
        		%9 = arith.addf %66, %8 : vector<2xf64>
        		

        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %9, %res[%res_full_offset] : memref<2xf64>, vector<2xf64>
        		
			%new_offset = llvm.add %offset_loop, %two : i32
			scf.yield %new_offset : i32
		}
	}
	

	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32
	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<2xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 2 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<2xi32>) : vector<2xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<2xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<2xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<2xi32>, i32
		}
	
		
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index	
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64> into vector<2xf64>
		
		%tab2_full_index = arith.addi %tab2_index, %6 : i32
		%tab2_full_offset = arith.index_cast %tab2_full_index : i32 to index
		
		%77 = vector.gather %tab2[%tab2_full_offset][%vec_index], %11, %1 : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64> into vector<2xf64>
         	
		%66 = arith.mulf %3, %55 : vector<2xf64>
		%8 = arith.mulf %2, %77 : vector<2xf64>
		%9 = arith.addf %66, %8 : vector<2xf64>
		
		
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		vector.scatter %res[%res_full_offset][%vec_index], %11, %9 : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64>
		
	}
	
	
	return
}

func.func @compute_LUT_if_then_2xf64(%tab1: memref<2xf64>, %res: memref<2xf64>, %lut_elements : i32, %tab1_index : i32, %res_index : i32) {
	%1 = arith.constant dense<1.000000e+00> : vector<2xf64>
	%two = arith.constant 2 : i32
	%4 = arith.remui %lut_elements, %two : i32
	%5 = arith.cmpi "sge", %lut_elements, %two : i32
	%6 = arith.subi %lut_elements, %4 : i32

	scf.if %5 {
		%zero_index = arith.constant 0 : index
		%two_index = arith.constant 2 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.index_cast %6 : i32 to index

		%zero_offset = arith.constant 0 : i32

		%offset_loop = scf.for %iv = %zero_index to %last_index step %two_index
		iter_args(%offset_loop = %zero_offset) -> (i32) {
			
			%iv_int = arith.index_cast %iv : index to i32	
			
			%tab1_full_index = arith.addi %tab1_index, %iv_int : i32
			%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
        		%55 = vector.load %tab1[%tab1_full_offset] : memref<2xf64>, vector<2xf64>
        		

        		
        		%res_full_index = arith.addi %res_index, %iv_int : i32
			%res_full_offset = arith.index_cast %res_full_index : i32 to index
        		vector.store %55, %res[%res_full_offset] : memref<2xf64>, vector<2xf64>

			%new_offset = llvm.add %offset_loop, %two : i32
			scf.yield %new_offset : i32
		}
	}
	
	%zero = arith.constant 0 : i32
	%10 = arith.cmpi "sgt", %4, %zero : i32

	scf.if %10 {
		%mask_index = arith.index_cast %4 : i32 to index
		%11 = vector.create_mask %mask_index : vector<2xi1>

		%zero_index = arith.constant 0 : index
		%one_index = arith.constant 1 : index
		%last_index = arith.constant 2 : index

		%zero_offset = arith.constant 0 : i32
		%one = arith.constant 1: i32

		// Create vector
		%vec_index_init = llvm.mlir.constant(dense<0> : vector<2xi32>) : vector<2xi32>
		%vec_index, %offset_loop = scf.for %iv = %zero_index to %last_index step %one_index
		iter_args(%vec = %vec_index_init, %offset_loop = %zero_offset) -> (vector<2xi32>, i32) {
			%index_i32 = arith.index_cast %iv : index to i32
			%newvec = vector.insertelement %offset_loop, %vec[%index_i32 : i32] : vector<2xi32>
			%new_offset = llvm.add %offset_loop, %one : i32
			scf.yield %newvec, %new_offset : vector<2xi32>, i32
		}
	
		%tab1_full_index = arith.addi %tab1_index, %6 : i32
		%tab1_full_offset = arith.index_cast %tab1_full_index : i32 to index
		
		%55 = vector.gather %tab1[%tab1_full_offset][%vec_index], %11, %1 : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64> into vector<2xf64>
		
		%res_full_index = arith.addi %res_index, %6 : i32
		%res_full_offset = arith.index_cast %res_full_index : i32 to index
		
		vector.scatter %res[%res_full_offset][%vec_index], %11, %55 : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64>
	}
	return

}


func.func @compute_LUT_bound_2xf64(%idx_row : vector<2xf64>, %res : f64, %val_row : vector<2xf64>, %ma : f64, %mn : f64, %lut_ele : i32,%mn_ind : i32, %tab1 : memref<2xf64>, %res_row : memref<2xf64>) {
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	// computing LUT_Error	
	%res_f32 = arith.truncf %res : f64 to f32
	%f32_4 = vector.broadcast %res_f32 : f32 to vector<2xf32>
	%idx_row_f32 = arith.truncf %idx_row : vector<2xf64> to vector<2xf32>
	%55 = arith.mulf %idx_row_f32, %f32_4 : vector<2xf32>
	
	%4 = vector.broadcast %res : f64 to vector<2xf64>
	%5 = arith.extf %55 : vector<2xf32> to vector<2xf64>
	
	
	%6 = arith.subf %val_row, %5 : vector<2xf64>
	%7 = arith.divf %6, %4 : vector<2xf64>

	// computing LUT_out_of_bounds
	%8 = vector.broadcast %ma : f64 to vector<2xf64>
	%9 = vector.broadcast %mn : f64 to vector<2xf64>
	%11 = arith.cmpf olt, %val_row, %9 : vector<2xf64>
	%12 = arith.cmpf ogt, %val_row, %8 : vector<2xf64>
	%13 = arith.ori %11, %12 : vector<2xi1>
	
	%zero_index = arith.constant 0 : index
	%one_index = arith.constant 1 : index
	%last_index = arith.constant 2 : index


	scf.for %iv = %zero_index to %last_index step %one_index
	iter_args () -> () {
	
        	%index_int = arith.index_cast %iv : index to i32
        	%derr = vector.extractelement %7[%index_int : i32] : vector<2xf64>
        	%bound = vector.extractelement %13[%index_int : i32] : vector<2xi1>
        	
        	%x_idx = vector.extractelement %idx_row[%index_int : i32] : vector<2xf64>
        	%idx = arith.fptosi %x_idx : f64 to i32
        	

        	%one = arith.constant 1 : i32
        	%idx_1 = arith.addi %idx, %one : i32
        	
        	%l1 = arith.subi %idx, %mn_ind : i32
        	%l2 = arith.muli %l1, %lut_ele : i32
        	
        	%l3 = arith.muli %index_int, %lut_ele : i32       	 
        	
        	scf.if %bound {
        		func.call @compute_LUT_if_then_2xf64(%tab1, %res_row, %lut_ele, %l2, %l3) : (memref<2xf64>, memref<2xf64>, i32, i32, i32) -> ()
        	
        	}
        	else {
        		%l4 = arith.subi %idx_1, %mn_ind : i32
        		%l5 = arith.muli %l4, %lut_ele : i32
        
        		func.call @compute_LUT_if_else_2xf64(%tab1, %tab1, %derr, %res_row, %lut_ele, %l2, %l5, %l3) : (memref<2xf64>, memref<2xf64>, f64, memref<2xf64>, i32, i32, i32, i32) -> ()
        		
        		
        	}	
    	}
	
	return
}


func.func @compute_LUT_interpRow_mlir_2xf64(%step : f64, %mn : f64, %mx : f64, %t_res : f64, %lut_ele : i32, %mn_ind : f64, %tab1 : memref<2xf64>, %res : memref<2xf64>, %no_imp_fast : i1, %cell_id : i32, %mx_ind : f64, %name : memref<i8>) {

	
	%tab_mn_ind = arith.fptosi %mn_ind : f64 to i32

	%1 = vector.broadcast %step : f64 to vector<2xf64>
	%offset = arith.constant 0 : i32
	%offset_index = arith.index_cast %offset : i32 to index

	%2 = vector.load %res[%offset_index] : memref<2xf64>, vector<2xf64> // val
	%idx_3 = arith.mulf %2, %1 : vector<2xf64> // idx
	
	%idx_i32 = arith.fptosi %idx_3 : vector<2xf64> to vector<2xi32>
	%3 = arith.sitofp %idx_i32 : vector<2xi32> to vector<2xf64>

	scf.if %no_imp_fast {
		%p1 = vector.broadcast %mn_ind : f64 to vector<2xf64>
		%p2 = vector.broadcast %mx_ind : f64 to vector<2xf64>
		
		%p3 = arith.cmpf olt, %3, %p1 : vector<2xf64>
		%p4 = arith.cmpf ogt, %3, %p2 : vector<2xf64>
		
		%p_zero_index = arith.constant 0 : index
		%p_one_index = arith.constant 1 : index
		%p_last_index = arith.constant 2 : index


		scf.for %iv = %p_zero_index to %p_last_index step %p_one_index
		iter_args () -> () {
			%index_int = arith.index_cast %iv : index to i32
			%p5 = vector.extractelement %p3[%index_int : i32] : vector<2xi1>
			%p6 = vector.extractelement %p4[%index_int : i32] : vector<2xi1>
			
			scf.if %p5 {
				%index_i32 = arith.index_cast %iv : index to i32
				vector.insertelement %mn_ind, %3[%index_i32 : i32] : vector<2xf64>
				%p7 = vector.extractelement %2[%index_int : i32] : vector<2xf64>
				%p8 = arith.addi %cell_id, %index_int : i32
				func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> () 
			} 
			else  { 
				scf.if %p6 {
					%index_i32 = arith.index_cast %iv : index to i32
					vector.insertelement %mx_ind, %3[%index_i32 : i32] : vector<2xf64>
					%p7 = vector.extractelement %2[%index_int : i32] : vector<2xf64>
					%p8 = arith.addi %cell_id, %index_int : i32
					func.call @LUT_problem_mlir(%name, %p7, %p8) : (memref<i8>, f64, i32) -> ()  
				}
			}
		}
	}
	
	
	func.call @compute_LUT_bound_2xf64(%3, %t_res, %2, %mx, %mn, %lut_ele, %tab_mn_ind, %tab1, %res) : (vector<2xf64>, f64, vector<2xf64>, f64, f64, i32, i32, memref<2xf64>, memref<2xf64>) -> ()
	
	return
}









