// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "ion_type.h"

namespace limpet {

const std::string& IonType::get_name() const {
	return this->_name;
}

bool IonType::is_plugin() const {
	return this->_plugin;
}

bool IonType::operator==(const IonType& other) const {
	return this == &other;
}

bool IonType::operator!=(const IonType& other) const {
	return !(*this == other);
}

} // limpet namespace
