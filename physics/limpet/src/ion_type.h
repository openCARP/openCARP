// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef LIMPET_ION_TYPE_H
#define LIMPET_ION_TYPE_H

#include "limpet_types.h"
#include "target.h"
#include <string>
#include <vector>
#include <stdio.h>

namespace limpet {

template<class T>
constexpr inline T max(T a, T b ){ return a > b ? a : b; }
template<class T>
constexpr inline T min(T a, T b ){ return a < b ? a : b; }

/*
MODEL SPECIFIC DEFINITIONS

Each ionic model must have a model_id, a name and a data structure
containing all its adjustable parameters. This data structure is
defined in the header files of the respective ionic models.
*/

#include "ION_IF_datatypes.h"

class IonIfBase;

typedef float Gatetype;
typedef GlobalData_t (*SVgetfcn)(IonIfBase&,int,int);
typedef void (*SVputfcn)(IonIfBase&,int,int,GlobalData_t);
typedef char   IIF_Mask_t;

/**
 * @brief Abstract class representing an ionic model type.
 *
 * This class acts as an interface for ionic models. Each ionic model
 * generated from EasyML or some other language will define this class'
 * abstract functions and used data types.
 */
class IonType {
public:
  using params_type = void; //!< type of this model's parameter structure
  using state_type = void; //!< type of this model's state variable structure
  using private_type = void; //!< type of this model's private structure
  using private_type_vector = void; //! type of model's private structure but with vectorized fields
private:
	std::string _name; //!< name of the model
	bool _plugin; //!< whether this type can be used as a plugin

public:
	IonType(std::string name, bool plugin) : _name(std::move(name)), _plugin(plugin) {}

	/**
	 * @brief Gets the model name.
	 *
	 * @return the name of this model
	 */
	const std::string& get_name() const;

	/**
	 * @brief Returns whether this model is a plugin or not.
	 *
	 * @return `true` if this model is a plugin, or `false` otherwise
	 */
	bool is_plugin() const;

	/**
	 * @brief Gets the size in bytes needed to represent the parameters of this model.
	 *
	 * @return the size in bytes of the parameters of this model
	 */
	virtual size_t params_size() const = 0;

  /**
   * @brief Gets the vector size when using data layout optimization (DLO).
   *
   * @retval 1 if DLO isn't used (or if vector size is 1)
   * @returns the vector size otherwise
   */
  virtual size_t dlo_vector_size() const = 0;

  /**
   * @brief Gets data flags for this IMP's required data.
   *
   * @returns a bitmap of external variables used but unchanged by the IMP
   */
	virtual uint32_t reqdat() const = 0;

  /**
   * @brief Gets data flags for this IMP's modified data.
   *
   * @returns a bitmap of external variables modified by the IMP
   */
	virtual uint32_t moddat() const = 0;

	/**
	 * @brief Initializes the parameters in the given IMP.
   *
   * @param imp IMP to initialize the parameters of
	 */
	virtual void initialize_params(IonIfBase& imp) const = 0;

	/**
	 * @brief Contructs lookup tables.
   *
   * @param imp IMP structure to constructure the tables in
	 */
	virtual void construct_tables(IonIfBase& imp) const = 0;

	/**
	 * @brief Destroys the given IMP.
   *
   * This frees the lookup tables and state variables.
   *
   * @param imp IMP to destroy
	 */
	virtual void destroy(IonIfBase& imp) const = 0;

  /**
   * @brief Initializes the state variables of the given IMP.
   *
   * @param imp IMP to initialize the SVs of
   * @param data external variables data (can be written by this functions)
   *
   * @pre data is allocated and initialized
   */
	virtual void initialize_sv(IonIfBase& imp, GlobalData_t** data) const = 0;

	/**
	 * @brief Gets a supported target from the given target.
	 *
	 * If the given target is `Target::AUTO` the returned target will be the first available,
	 * else for other targets it either returns the same if supported or `Target::UNKNOWN`.
	 *
	 * @param target the given target
	 * @return the selected target, or `Target::UNKNOWN` if the selection failed
	 */
	virtual Target select_target(Target target) const = 0;

	/**
	 * @brief Performs computation for 1 time step.
   *
   * State and private variables of @p imp will be updated as well as external
   * variables (@p data) for nodes that are in the @p start to @p end range.
   *
   * @param target to run the computation on (see limpet::Target)
   * @param start index of the first node (cell) to compute
   * @param end index of the last node (cell) to compute
   * @param imp IMP holding all necessary data
   * @param data external variables
	 */
	virtual void compute(Target target, int start, int end, IonIfBase& imp, GlobalData_t** data) const = 0;

	/**
	 * @brief Returns whether the trace method is available or not.
	 *
	 * @return `true` if the trace method is available, or `false` otherwise
	 */
	virtual bool has_trace() const = 0;

  /**
   * @brief Write the values of traced variables to @p file.
   *
   * @param imp IMP holding model data
   * @param node node for which to trace the data
   * @param file file to write to
   * @param data external variables array
   */
	virtual void trace(IonIfBase& imp, int node, FILE* file, GlobalData_t** data) const = 0;

  /**
   * @brief Handles setting of this model's parameters.
   *
   * @param imp IMP holding this model's data
   * @param im_par string to be parsed for parameter initialization
   */
	virtual void tune(IonIfBase& imp, const char* im_par) const = 0;

	/**
	 * @brief Reads state variable values for one cell from a file.
	 *
	 * @retval 0 good
	 * @retval 1 temporary table allocated
	 * @retval >1 error
	 */
	virtual int read_svs(IonIfBase& imp, FILE* file) const = 0;

  /**
   * @brief Write state variable values for one cell to a file/
   *
   * @param imp IMP holding the model's data
   * @param out file to write to
   * @param node number for the cell we want to write the SVs of
   */
	virtual int write_svs(IonIfBase& imp, FILE* file, int node) const = 0;

  /**
   * @brief Get the offset and size of a state variable of the model,
   * as well as an access function.
   *
   * @param svname of the state variable
   * @param[out] off offset of the variable (or 0 if @p svname is "ALL_SV")
   * @param[out] sz size of the state variable (or size of the SV structure if
   * @p svname is "ALL_SV"), not the size of the whole sv array in case of DLO
   * e.g. if vector size is 8, sz is still 4 for a float and not 32
   *
   * @returns an access fonction that will return a state variable from an
   * offset and a cell number, always as GlobalData_t. (see SVgetfcn)
   */
	virtual SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const = 0;

	/**
	 * @brief Returns a list of SVs.
	 *
	 * @param list list of SVs
	 *
	 * @return number of SVs
	 * @post list is allocated and may be freed
	 */
	virtual int get_sv_list(char*** list) const = 0;

	/**
	 * @brief Determines the type of a SV.
	 *
	 * @param svname        name of the sv
	 * @param type          type of a sv
	 * @param type_name     name of the type
	 *
	 * @retval 0            the combination imp_id/svname was found
	 * @retval 1            the combination imp_id/svname was not found
	 */
	virtual int get_sv_type(const char* svname, int* type, char** type_name) const = 0;

	/**
	 * @brief Prints the parameters of this model.
	 */
	virtual void print_params() const = 0;

	/**
	 * @brief Prints the metadata of this model.
	 */
	virtual void print_metadata() const = 0;

  /**
   * @brief Generate an IonIf object from this type.
   *
   * @note The generated instance should always be used with this type only.
   *
   * @returns a child class of IonIfBase that is specific to this IonType
   */
  virtual IonIfBase* make_ion_if(Target target, int num_node,
      const std::vector<std::reference_wrapper<IonType>>& plugins) const = 0;

  /**
   * @brief Destroy an IonIf object.
   *
   * @param imp IMP to destroy
   *
   * @note This should only be used to destroy IMPS created with the
   * IonIfBase::make_ion_if function of the same model type
   */
  virtual void destroy_ion_if(IonIfBase *imp) const = 0;

	bool operator==(const IonType& other) const;

	bool operator!=(const IonType& other) const;
};

// Shorthand for lists of references to IonTypes
using IonTypeList = std::vector<std::reference_wrapper<IonType>>;

// TODO: replace with std::optional<std::reference_wrapper<IonType>>
// Using std::optional would be nice but requires C++17
IonType* get_ion_type(const std::string& name);

} // limpet namespace

#endif // LIMPET_ION_TYPE_H
