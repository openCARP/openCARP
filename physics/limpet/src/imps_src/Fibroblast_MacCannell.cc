// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: MacCannell KA, Bazzazi H, Chilton L, Shibukawa Y, Clark RB, Giles WR
*  Year: 2007
*  Title: A mathematical model of electrotonic interactions between ventricular myocytes and fibroblasts
*  Journal: Biophys J., 92(11), 4121-32
*  DOI: 10.1529/biophysj.106.101410
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Fibroblast_MacCannell.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Fibroblast_MacCannellIonType::Fibroblast_MacCannellIonType(bool plugin) : IonType(std::move(std::string("Fibroblast_MacCannell")), plugin) {}

size_t Fibroblast_MacCannellIonType::params_size() const {
  return sizeof(struct Fibroblast_MacCannell_Params);
}

size_t Fibroblast_MacCannellIonType::dlo_vector_size() const {

  return 1;
}

uint32_t Fibroblast_MacCannellIonType::reqdat() const {
  return Fibroblast_MacCannell_REQDAT;
}

uint32_t Fibroblast_MacCannellIonType::moddat() const {
  return Fibroblast_MacCannell_MODDAT;
}

void Fibroblast_MacCannellIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Fibroblast_MacCannellIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(FIBROBLAST_MACCANNELL_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(FIBROBLAST_MACCANNELL_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(FIBROBLAST_MACCANNELL_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Fibroblast_MacCannellIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CUDA_GENERATED
      compute_Fibroblast_MacCannell_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(FIBROBLAST_MACCANNELL_MLIR_ROCM_GENERATED)
      compute_Fibroblast_MacCannell_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(FIBROBLAST_MACCANNELL_MLIR_CPU_GENERATED)
      compute_Fibroblast_MacCannell_mlir_cpu(start, end, imp, data);
#   elif defined(FIBROBLAST_MACCANNELL_CPU_GENERATED)
      compute_Fibroblast_MacCannell_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Fibroblast_MacCannellIonType::compute."
#   endif
      break;
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Fibroblast_MacCannell_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Fibroblast_MacCannell_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Fibroblast_MacCannell_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef FIBROBLAST_MACCANNELL_CPU_GENERATED
    case Target::CPU:
      compute_Fibroblast_MacCannell_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define B (GlobalData_t)(-200.0)
#define Cm (GlobalData_t)(6.3)
#define F (GlobalData_t)(96487.0)
#define K_mK (GlobalData_t)(1.0)
#define K_mNa (GlobalData_t)(11.0)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(306.15)
#define V_rev (GlobalData_t)(-150.0)
#define Vol_i (GlobalData_t)(0.00137)
#define Vol_o (GlobalData_t)(0.0008)
#define r_Kv_init (GlobalData_t)(0.070448)
#define s_Kv_init (GlobalData_t)(0.970886)
#define K_mNa_15 (GlobalData_t)((pow(K_mNa,1.5)))



void Fibroblast_MacCannellIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Fibroblast_MacCannell_Params *p = imp.params();

  // Compute the regional constants
  {
    p->GK1 = 0.4822;
    p->GKv = 0.25;
    p->GbNa = 0.0095;
    p->Ggj = 2.;
    p->Ki_init = 140.045865;
    p->Ko = 5.4;
    p->Nai_init = 9.954213;
    p->Nao = 130.0;
    p->fb_myo_surf_ratio = (6.3/185.);
    p->fb_surf = 630.;
    p->maxINaK = 1.4;
    p->num_fb = 1.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  Ki_TAB,
  Nai_TAB,
  V_EK_TAB,
  Vmyo_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum Ki_TableIndex {
  E_K_idx,
  NROWS_Ki
};

enum Nai_TableIndex {
  E_Na_idx,
  Nai_15_idx,
  NROWS_Nai
};

enum V_EK_TableIndex {
  IK1_idx,
  NROWS_V_EK
};

enum Vmyo_TableIndex {
  NROWS_Vmyo
};



void Fibroblast_MacCannellIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e-3;
  cell_geom* region = &imp.cgeom();
  Fibroblast_MacCannell_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  
  // Create the Ki lookup table
  LUT* Ki_tab = &imp.tables()[Ki_TAB];
  LUT_alloc(Ki_tab, NROWS_Ki, 0.01, 500, 0.01, "Fibroblast_MacCannell Ki", imp.get_target());
  for (int __i=Ki_tab->mn_ind; __i<=Ki_tab->mx_ind; __i++) {
    double Ki = Ki_tab->res*__i;
    LUT_data_t* Ki_row = Ki_tab->tab[__i];
    Ki_row[E_K_idx] = (((R*T)/F)*(log((p->Ko/Ki))));
  }
  check_LUT(Ki_tab);
  
  
  // Create the Nai lookup table
  LUT* Nai_tab = &imp.tables()[Nai_TAB];
  LUT_alloc(Nai_tab, NROWS_Nai, 0.01, 1000, 0.01, "Fibroblast_MacCannell Nai", imp.get_target());
  for (int __i=Nai_tab->mn_ind; __i<=Nai_tab->mx_ind; __i++) {
    double Nai = Nai_tab->res*__i;
    LUT_data_t* Nai_row = Nai_tab->tab[__i];
    Nai_row[E_Na_idx] = (((R*T)/F)*(log((p->Nao/Nai))));
    Nai_row[Nai_15_idx] = (pow(Nai,1.5));
  }
  check_LUT(Nai_tab);
  
  
  // Create the V_EK lookup table
  LUT* V_EK_tab = &imp.tables()[V_EK_TAB];
  LUT_alloc(V_EK_tab, NROWS_V_EK, -1000, 1000, 0.005, "Fibroblast_MacCannell V_EK", imp.get_target());
  for (int __i=V_EK_tab->mn_ind; __i<=V_EK_tab->mx_ind; __i++) {
    double V_EK = V_EK_tab->res*__i;
    LUT_data_t* V_EK_row = V_EK_tab->tab[__i];
    double alpha_K1 = (0.1/(1.+(exp((0.06*(V_EK-(200.0)))))));
    double beta_K1 = (((3.*(exp((0.0002*(V_EK+100.0)))))+(exp((0.1*(V_EK-(10.0))))))/(1.0+(exp((-0.5*V_EK)))));
    V_EK_row[IK1_idx] = (((p->GK1*alpha_K1)*V_EK)/(alpha_K1+beta_K1));
  }
  check_LUT(V_EK_tab);
  

}



void Fibroblast_MacCannellIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e-3;
  cell_geom *region = &imp.cgeom();
  Fibroblast_MacCannell_Params *p = imp.params();

  Fibroblast_MacCannell_state *sv_base = (Fibroblast_MacCannell_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Fibroblast_MacCannell_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vmyo = Vmyo_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ki = p->Ki_init;
    sv->Nai = p->Nai_init;
    double V_fb_init = Vmyo;
    sv->r_Kv = r_Kv_init;
    sv->s_Kv = s_Kv_init;
    sv->V_fb = V_fb_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vmyo_ext[__i] = Vmyo;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef FIBROBLAST_MACCANNELL_CPU_GENERATED
extern "C" {
void compute_Fibroblast_MacCannell_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Fibroblast_MacCannellIonType::IonIfDerived& imp = static_cast<Fibroblast_MacCannellIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e-3;
  cell_geom *region = &imp.cgeom();
  Fibroblast_MacCannell_Params *p  = imp.params();
  Fibroblast_MacCannell_state *sv_base = (Fibroblast_MacCannell_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Fibroblast_MacCannellIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Fibroblast_MacCannell_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vmyo = Vmyo_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ki_row[NROWS_Ki];
    LUT_interpRow(&IF->tables()[Ki_TAB], sv->Ki, __i, Ki_row);
    LUT_data_t Nai_row[NROWS_Nai];
    LUT_interpRow(&IF->tables()[Nai_TAB], sv->Nai, __i, Nai_row);
    LUT_data_t Vmyo_row[NROWS_Vmyo];
    LUT_interpRow(&IF->tables()[Vmyo_TAB], Vmyo, __i, Vmyo_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t Igj = (Ggj_density*(Vmyo-(sv->V_fb)));
    Iion = (Iion+((Igj*p->num_fb)*p->fb_myo_surf_ratio));
    
    
    //Complete Forward Euler Update
    GlobalData_t IKv = (((p->GKv*sv->r_Kv)*sv->s_Kv)*(sv->V_fb-(Ki_row[E_K_idx])));
    GlobalData_t INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_row[Nai_15_idx]/(Nai_row[Nai_15_idx]+K_mNa_15)))*(sv->V_fb-(V_rev)))/(sv->V_fb-(B)));
    GlobalData_t IbNa = (p->GbNa*(sv->V_fb-(Nai_row[E_Na_idx])));
    GlobalData_t V_EK = (sv->V_fb-(Ki_row[E_K_idx]));
    LUT_data_t V_EK_row[NROWS_V_EK];
    LUT_interpRow(&IF->tables()[V_EK_TAB], V_EK, __i, V_EK_row);
    GlobalData_t diff_Nai = (Cm*(((-IbNa)-((3.*INaK)))/(Vol_i*F)));
    GlobalData_t Ifb = ((((IKv+V_EK_row[IK1_idx])+IbNa)+INaK)-(Igj));
    GlobalData_t diff_Ki = (Cm*((-((V_EK_row[IK1_idx]+IKv)-((2.*INaK))))/(Vol_i*F)));
    GlobalData_t d_V_fb_dt = (-Ifb);
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t V_fb_new = sv->V_fb+d_V_fb_dt*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t r_Kv_inf = (1./(1.+(exp(((-(sv->V_fb+20.))/11.)))));
    GlobalData_t s_Kv_inf = (1./(1.+(exp(((sv->V_fb+23.)/7.)))));
    GlobalData_t tau_r_Kv = (20.3+(138.0*(exp(((-((sv->V_fb+20.)/25.9))*((sv->V_fb+20.)/25.9))))));
    GlobalData_t tau_s_Kv = (1574.0+(5268.0*(exp(((-((sv->V_fb+23.)/22.7))*((sv->V_fb+23.)/22.7))))));
    GlobalData_t r_Kv_rush_larsen_B = (exp(((-dt)/tau_r_Kv)));
    GlobalData_t r_Kv_rush_larsen_C = (expm1(((-dt)/tau_r_Kv)));
    GlobalData_t s_Kv_rush_larsen_B = (exp(((-dt)/tau_s_Kv)));
    GlobalData_t s_Kv_rush_larsen_C = (expm1(((-dt)/tau_s_Kv)));
    GlobalData_t r_Kv_rush_larsen_A = ((-r_Kv_inf)*r_Kv_rush_larsen_C);
    GlobalData_t s_Kv_rush_larsen_A = ((-s_Kv_inf)*s_Kv_rush_larsen_C);
    GlobalData_t r_Kv_new = r_Kv_rush_larsen_A+r_Kv_rush_larsen_B*sv->r_Kv;
    GlobalData_t s_Kv_new = s_Kv_rush_larsen_A+s_Kv_rush_larsen_B*sv->s_Kv;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->V_fb = V_fb_new;
    sv->r_Kv = r_Kv_new;
    sv->s_Kv = s_Kv_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vmyo_ext[__i] = Vmyo;

  }

            }
}
#endif // FIBROBLAST_MACCANNELL_CPU_GENERATED

bool Fibroblast_MacCannellIonType::has_trace() const {
    return true;
}

void Fibroblast_MacCannellIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Fibroblast_MacCannell_trace_header.txt","wt");
    fprintf(theader->fd,
        "IK1\n"
        "INaK\n"
        "IbNa\n"
        "Igj\n"
        "sv->V_fb\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e-3;
  cell_geom *region = &imp.cgeom();
  Fibroblast_MacCannell_Params *p  = imp.params();

  Fibroblast_MacCannell_state *sv_base = (Fibroblast_MacCannell_state *)imp.sv_tab().data();

  Fibroblast_MacCannell_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Vmyo = Vmyo_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
  GlobalData_t Igj = (Ggj_density*(Vmyo-(sv->V_fb)));
  GlobalData_t Nai_15 = (pow(sv->Nai,1.5));
  GlobalData_t INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_15/(Nai_15+K_mNa_15)))*(sv->V_fb-(V_rev)))/(sv->V_fb-(B)));
  GlobalData_t IbNa = (p->GbNa*(sv->V_fb-(E_Na)));
  GlobalData_t V_EK = (sv->V_fb-(E_K));
  GlobalData_t alpha_K1 = (0.1/(1.+(exp((0.06*(V_EK-(200.0)))))));
  GlobalData_t beta_K1 = (((3.*(exp((0.0002*(V_EK+100.0)))))+(exp((0.1*(V_EK-(10.0))))))/(1.0+(exp((-0.5*V_EK)))));
  GlobalData_t IK1 = (((p->GK1*alpha_K1)*V_EK)/(alpha_K1+beta_K1));
  //Output the desired variables
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", Igj);
  fprintf(file, "%4.12f\t", sv->V_fb);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* Fibroblast_MacCannellIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Fibroblast_MacCannellIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        