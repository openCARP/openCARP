// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Luo CH, Rudy Y
*  Year: 1994
*  Title: A dynamic model of the cardiac ventricular action potential. I. Simulations of ionic currents and concentration changes
*  Journal: Circ Res., 74(6),1071-96
*  DOI: 10.1161/01.res.74.6.1071
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "LuoRudy94.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

LuoRudy94IonType::LuoRudy94IonType(bool plugin) : IonType(std::move(std::string("LuoRudy94")), plugin) {}

size_t LuoRudy94IonType::params_size() const {
  return sizeof(struct LuoRudy94_Params);
}

size_t LuoRudy94IonType::dlo_vector_size() const {

  return 1;
}

uint32_t LuoRudy94IonType::reqdat() const {
  return LuoRudy94_REQDAT;
}

uint32_t LuoRudy94IonType::moddat() const {
  return LuoRudy94_MODDAT;
}

void LuoRudy94IonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target LuoRudy94IonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef LUORUDY94_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(LUORUDY94_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(LUORUDY94_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(LUORUDY94_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef LUORUDY94_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef LUORUDY94_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef LUORUDY94_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef LUORUDY94_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void LuoRudy94IonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef LUORUDY94_MLIR_CUDA_GENERATED
      compute_LuoRudy94_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(LUORUDY94_MLIR_ROCM_GENERATED)
      compute_LuoRudy94_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(LUORUDY94_MLIR_CPU_GENERATED)
      compute_LuoRudy94_mlir_cpu(start, end, imp, data);
#   elif defined(LUORUDY94_CPU_GENERATED)
      compute_LuoRudy94_cpu(start, end, imp, data);
#   else
#     error "Could not generate method LuoRudy94IonType::compute."
#   endif
      break;
#   ifdef LUORUDY94_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_LuoRudy94_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef LUORUDY94_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_LuoRudy94_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef LUORUDY94_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_LuoRudy94_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef LUORUDY94_CPU_GENERATED
    case Target::CPU:
      compute_LuoRudy94_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define C1 (GlobalData_t)(.00025)
#define C2 (GlobalData_t)(0.0001)
#define CaNSRbar (GlobalData_t)(15.)
#define Cai_init (GlobalData_t)(0.079)
#define Cao (GlobalData_t)(1.8)
#define F (GlobalData_t)(96485.)
#define GCaT (GlobalData_t)(0.05)
#define GKp (GlobalData_t)(0.00552)
#define GNa (GlobalData_t)(16.)
#define GbNa (GlobalData_t)(0.004)
#define IbarNaK (GlobalData_t)(2.25)
#define IbarpCa (GlobalData_t)(1.15)
#define Iupbar (GlobalData_t)(0.00875)
#define Ki_init (GlobalData_t)(136.89149)
#define KmCa (GlobalData_t)(0.0006)
#define KmKo (GlobalData_t)(1.5)
#define KmNai (GlobalData_t)(10.)
#define KmpCa (GlobalData_t)(0.0005)
#define Kmup (GlobalData_t)(0.00092)
#define Ko (GlobalData_t)(4.5)
#define Nai_init (GlobalData_t)(12.236437)
#define Nao (GlobalData_t)(140.)
#define PCa (GlobalData_t)(0.00054)
#define PK (GlobalData_t)(0.000000193)
#define PNa (GlobalData_t)(0.000000675)
#define R (GlobalData_t)(8314.)
#define T (GlobalData_t)(310.)
#define V_init (GlobalData_t)(-88.654973)
#define VolJSR (GlobalData_t)(0.48)
#define VolMITO (GlobalData_t)(2.6)
#define VolMYO (GlobalData_t)(68.)
#define VolNSR (GlobalData_t)(5.52)
#define VolSR (GlobalData_t)(6.0)
#define a_Cab (GlobalData_t)(0.003016)
#define cmdnbar (GlobalData_t)(0.050)
#define csqnbar (GlobalData_t)(10.)
#define csqnth (GlobalData_t)(8.75)
#define fr_CLEFT (GlobalData_t)((0.12/0.88))
#define fr_JSR (GlobalData_t)(0.0048)
#define fr_MITO (GlobalData_t)(0.26)
#define fr_MYO (GlobalData_t)(0.68)
#define fr_NSR (GlobalData_t)(0.0552)
#define fr_SR (GlobalData_t)(0.06)
#define gam (GlobalData_t)(.15)
#define gitodv (GlobalData_t)(0.5)
#define gmaxrel (GlobalData_t)(150.)
#define grelbarjsrol (GlobalData_t)(4.)
#define jsr_init (GlobalData_t)(1.179991)
#define kmcmdn (GlobalData_t)(0.00238)
#define kmcsqn (GlobalData_t)(0.8)
#define kmtrpn (GlobalData_t)(0.0005)
#define len (GlobalData_t)(0.01)
#define nsr_init (GlobalData_t)(1.179991)
#define pi (GlobalData_t)(3.141592)
#define prnak (GlobalData_t)(0.01833)
#define rad (GlobalData_t)(0.0011)
#define tau_on (GlobalData_t)(0.5)
#define tcicr_init (GlobalData_t)(1000.)
#define tjsrol_init (GlobalData_t)(1000.)
#define trpnbar (GlobalData_t)(0.070)
#define zCa (GlobalData_t)(2.)
#define zK (GlobalData_t)(1.)
#define zNa (GlobalData_t)(1.)
#define Cao_uM (GlobalData_t)((Cao*1000.))
#define F_RT (GlobalData_t)((F/(R*T)))
#define GK1 (GlobalData_t)((0.75*(sqrt((Ko/5.4)))))
#define GKr (GlobalData_t)((0.02614*(sqrt((Ko/5.4)))))
#define Kleak (GlobalData_t)((Iupbar/CaNSRbar))
#define KmCa_uM (GlobalData_t)((KmCa*1000.))
#define KmpCa_uM (GlobalData_t)((KmpCa*1000.))
#define Kmup_uM (GlobalData_t)((Kmup*1000.))
#define Nao3 (GlobalData_t)(((Nao*Nao)*Nao))
#define Volcell (GlobalData_t)(((((1000.*pi)*rad)*rad)*len))
#define ageo (GlobalData_t)(((((2.*pi)*rad)*rad)+(((2.*pi)*rad)*len)))
#define sigma (GlobalData_t)((((exp((Nao/67.3)))-(1.))/7.))
#define tau_on_ol (GlobalData_t)(tau_on)
#define acap (GlobalData_t)((ageo*2.))
#define vcleft (GlobalData_t)((Volcell*fr_CLEFT))
#define vjsr (GlobalData_t)((Volcell*fr_JSR))
#define vmito (GlobalData_t)((Volcell*fr_MITO))
#define vmyo (GlobalData_t)((Volcell*fr_MYO))
#define vnsr (GlobalData_t)((Volcell*fr_NSR))
#define vsr (GlobalData_t)((Volcell*fr_SR))



void LuoRudy94IonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  LuoRudy94_Params *p = imp.params();

  // Compute the regional constants
  {
    p->GNaK = 1.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,
  VEK1_TAB,
  catotal_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum Cai_TableIndex {
  ECa_idx,
  ECaT_idx,
  IKs_temp_idx,
  IpCa_idx,
  Irup_idx,
  Nao3_cai_idx,
  cmdn_idx,
  fCa_idx,
  trpn_idx,
  NROWS_Cai
};

enum V_TableIndex {
  INaCa_A_idx,
  INaCa_C_idx,
  IbarCa_c_idx,
  IbarK_c_idx,
  IbarNa_c_idx,
  Kp_idx,
  b_rush_larsen_A_idx,
  b_rush_larsen_B_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  eterm_idx,
  ezca_term_idx,
  fNaK_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  g_rush_larsen_A_idx,
  g_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  r_idx,
  xr_rush_larsen_A_idx,
  xr_rush_larsen_B_idx,
  xs1_rush_larsen_A_idx,
  xs1_rush_larsen_B_idx,
  xs2_rush_larsen_A_idx,
  xs2_rush_larsen_B_idx,
  ydv_rush_larsen_A_idx,
  ydv_rush_larsen_B_idx,
  zdv_rush_larsen_A_idx,
  zdv_rush_larsen_B_idx,
  NROWS_V
};

enum VEK1_TableIndex {
  IK1_idx,
  NROWS_VEK1
};

enum catotal_TableIndex {
  CaIupdate_idx,
  NROWS_catotal
};



void LuoRudy94IonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  LuoRudy94_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  
  // Create the Cai lookup table
  LUT* Cai_tab = &imp.tables()[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-4, 10, 1e-4, "LuoRudy94 Cai", imp.get_target());
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[ECa_idx] = (((log((Cao_uM/Cai)))/F_RT)/2.);
    Cai_row[ECaT_idx] = (((log((Cao_uM/Cai)))/F_RT)/2.);
    Cai_row[IKs_temp_idx] = (0.433*(1.+(0.6/(1.+(pow((0.000038/(Cai*.001)),1.4))))));
    Cai_row[IpCa_idx] = ((IbarpCa*Cai)/(KmpCa_uM+Cai));
    Cai_row[Irup_idx] = ((Iupbar*Cai)/(Cai+Kmup_uM));
    Cai_row[Nao3_cai_idx] = ((Nao3*Cai)*0.001);
    Cai_row[cmdn_idx] = (cmdnbar*((1e-3*Cai)/((1e-3*Cai)+kmcmdn)));
    Cai_row[fCa_idx] = (KmCa_uM/(KmCa_uM+Cai));
    Cai_row[trpn_idx] = (trpnbar*((1e-3*Cai)/((1e-3*Cai)+kmtrpn)));
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &imp.tables()[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.01, "LuoRudy94 V", imp.get_target());
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[INaCa_A_idx] = (C1*(exp((((gam-(1.))*V)*F_RT))));
    V_row[INaCa_C_idx] = (C2*(exp((((gam-(1.))*V)*F_RT))));
    V_row[IbarCa_c_idx] = ((V==0.) ? ((((PCa*zCa)*zCa)*F)/zCa) : ((((((PCa*zCa)*zCa)*V)*F_RT)*F)/(expm1(((zCa*V)*F_RT)))));
    V_row[Kp_idx] = (1./(1.+(exp(((7.488-(V))/5.98)))));
    double a_h = ((V>=-40.) ? 1.0e-10 : (0.135*(exp(((80.+V)/-6.8)))));
    double a_j = ((V>=-40.) ? 1.0e-10 : ((((-1.2714e5*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    double a_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    double a_ydv = (0.015/(1.+(exp(((V+60.)/5.)))));
    double a_zdv = ((10.*(exp(((V-(40.))/25.))))/(1.+(exp(((V-(40.))/25.)))));
    double b_h = ((V>=-40.) ? (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_inf = ((V>-120.) ? (1./(1.+(exp(((-(V+14.))/10.8))))) : 0.);
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.))));
    double b_ydv = ((0.1*(exp(((V+25.)/5.))))/(1.+(exp(((V+25.)/5.)))));
    double b_zdv = ((10.*(exp(((-(V+90.))/25.))))/(1.+(exp(((-(V+90.))/25.)))));
    double d_inf = (1./(1.+(exp(((-(V+10.))/6.24)))));
    double em1term = (expm1((V*F_RT)));
    V_row[eterm_idx] = (exp((V*F_RT)));
    V_row[ezca_term_idx] = (exp(((zCa*V)*F_RT)));
    V_row[fNaK_idx] = (1./((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
    double f_inf = ((1./(1.+(exp(((V+32.)/8.)))))+(0.6/(1.+(exp(((50.-(V))/20.))))));
    double g_inf = ((V<=0.) ? (1./(1.+(exp(((V+60.)/5.6))))) : 0.);
    V_row[r_idx] = (1./(1.+(exp(((V+9.)/22.4)))));
    double tau_b = ((V<40.) ? (3.7+(6.1/(1.+(exp(((V+25.)/4.5)))))) : 3.7);
    double tau_f = (1./((0.0197*(exp(((-(0.0337*(V+10.)))*(0.0337*(V+10.))))))+0.02));
    double tau_g = ((V<=0.) ? ((-0.875*V)+12.) : 12.);
    double tau_xr = ((V==-14.2) ? (1./((0.00138/0.123)+((0.00061*(V+38.9))/((exp((0.145*(V+38.9))))-(1.))))) : ((V==-38.9) ? (1./(((0.00138*(V+14.2))/(1.-((exp((-0.123*(V+14.2)))))))+(0.00061/0.145))) : (1./(((0.00138*(V+14.2))/(1.-((exp((-0.123*(V+14.2)))))))+((0.00061*(V+38.9))/((exp((0.145*(V+38.9))))-(1.)))))));
    double tau_xs1 = ((V!=-30.) ? (1./(((7.19e-5*(V+30.))/(1.-((exp((-0.148*(V+30.)))))))+((1.31e-4*(V+30.))/((exp((0.0687*(V+30.))))-(1.))))) : (1./((7.19e-5/0.148)+(1.31e-4/0.0687))));
    double xr_inf = (1./(1.+(exp(((-(V+21.5))/7.5)))));
    double xs1_inf = (1./(1.+(exp(((1.5-(V))/16.7)))));
    V_row[IbarK_c_idx] = ((V==0.) ? ((((PK*zK)*zK)*F)*0.75) : (((((((PK*zK)*zK)*V)*F_RT)*F)*0.75)/em1term));
    V_row[IbarNa_c_idx] = ((V==0.) ? ((((PNa*zNa)*zNa)*F)*0.75) : (((((((PNa*zNa)*zNa)*V)*F_RT)*F)*0.75)/em1term));
    V_row[b_rush_larsen_B_idx] = (exp(((-dt)/tau_b)));
    double b_rush_larsen_C = (expm1(((-dt)/tau_b)));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)/tau_f)));
    double f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    V_row[g_rush_larsen_B_idx] = (exp(((-dt)/tau_g)));
    double g_rush_larsen_C = (expm1(((-dt)/tau_g)));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double tau_d = ((V!=-10.) ? ((d_inf*(1.-((exp(((-(V+10.))/6.24))))))/(0.035*(V+10.))) : ((d_inf/0.035)/6.24));
    double tau_xs2 = (tau_xs1*4.);
    V_row[xr_rush_larsen_B_idx] = (exp(((-dt)/tau_xr)));
    double xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    V_row[xs1_rush_larsen_B_idx] = (exp(((-dt)/tau_xs1)));
    double xs1_rush_larsen_C = (expm1(((-dt)/tau_xs1)));
    double xs2_inf = xs1_inf;
    V_row[ydv_rush_larsen_A_idx] = (((-a_ydv)/(a_ydv+b_ydv))*(expm1(((-dt)*(a_ydv+b_ydv)))));
    V_row[ydv_rush_larsen_B_idx] = (exp(((-dt)*(a_ydv+b_ydv))));
    V_row[zdv_rush_larsen_A_idx] = (((-a_zdv)/(a_zdv+b_zdv))*(expm1(((-dt)*(a_zdv+b_zdv)))));
    V_row[zdv_rush_larsen_B_idx] = (exp(((-dt)*(a_zdv+b_zdv))));
    V_row[b_rush_larsen_A_idx] = ((-b_inf)*b_rush_larsen_C);
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[f_rush_larsen_A_idx] = ((-f_inf)*f_rush_larsen_C);
    V_row[g_rush_larsen_A_idx] = ((-g_inf)*g_rush_larsen_C);
    V_row[xr_rush_larsen_A_idx] = ((-xr_inf)*xr_rush_larsen_C);
    V_row[xs1_rush_larsen_A_idx] = ((-xs1_inf)*xs1_rush_larsen_C);
    V_row[xs2_rush_larsen_B_idx] = (exp(((-dt)/tau_xs2)));
    double xs2_rush_larsen_C = (expm1(((-dt)/tau_xs2)));
    V_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    V_row[xs2_rush_larsen_A_idx] = ((-xs2_inf)*xs2_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the VEK1 lookup table
  LUT* VEK1_tab = &imp.tables()[VEK1_TAB];
  LUT_alloc(VEK1_tab, NROWS_VEK1, -800, 800, 0.01, "LuoRudy94 VEK1", imp.get_target());
  for (int __i=VEK1_tab->mn_ind; __i<=VEK1_tab->mx_ind; __i++) {
    double VEK1 = VEK1_tab->res*__i;
    LUT_data_t* VEK1_row = VEK1_tab->tab[__i];
    double aa_K1 = (1.02/(1.+(exp((0.2385*(VEK1-(59.215)))))));
    double bb_K1 = (((0.49124*(exp((0.08032*(VEK1+5.476)))))+(exp((0.06175*(VEK1-(594.31))))))/(1.+(exp((-0.5143*(VEK1+4.753))))));
    double K1_in = (aa_K1/(aa_K1+bb_K1));
    VEK1_row[IK1_idx] = ((GK1*K1_in)*VEK1);
  }
  check_LUT(VEK1_tab);
  
  
  // Create the catotal lookup table
  LUT* catotal_tab = &imp.tables()[catotal_TAB];
  LUT_alloc(catotal_tab, NROWS_catotal, 1e-4, 1, 1e-4, "LuoRudy94 catotal", imp.get_target());
  for (int __i=catotal_tab->mn_ind; __i<=catotal_tab->mx_ind; __i++) {
    double catotal = catotal_tab->res*__i;
    LUT_data_t* catotal_row = catotal_tab->tab[__i];
    double bmyo = ((((cmdnbar+trpnbar)+kmtrpn)+kmcmdn)-(catotal));
    double cmyo = ((((kmcmdn*kmtrpn)-((catotal*(kmtrpn+kmcmdn))))+(trpnbar*kmcmdn))+(cmdnbar*kmtrpn));
    double dmyo = (((-kmtrpn)*kmcmdn)*catotal);
    double fmyo = (sqrt(((bmyo*bmyo)-((3.*cmyo)))));
    catotal_row[CaIupdate_idx] = (((((2.*fmyo)/3.)*(cos(((acos((((((9.*bmyo)*cmyo)-((((2.*bmyo)*bmyo)*bmyo)))-((27.*dmyo)))/(2.*(pow(((bmyo*bmyo)-((3.*cmyo))),1.5))))))/3.))))-((bmyo/3.)))*1000.);
  }
  check_LUT(catotal_tab);
  

}



void LuoRudy94IonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  LuoRudy94_Params *p = imp.params();

  LuoRudy94_state *sv_base = (LuoRudy94_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    LuoRudy94_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    V = V_init;
    sv->jsr = jsr_init;
    sv->nsr = nsr_init;
    sv->tcicr = tcicr_init;
    sv->tjsrol = tjsrol_init;
    double ECa = (((log((Cao_uM/sv->Cai)))/F_RT)/2.);
    double ECaT = (((log((Cao_uM/sv->Cai)))/F_RT)/2.);
    double INaCa_A = (C1*(exp((((gam-(1.))*V)*F_RT))));
    double INaCa_C = (C2*(exp((((gam-(1.))*V)*F_RT))));
    double IbarCa_c = ((V==0.) ? ((((PCa*zCa)*zCa)*F)/zCa) : ((((((PCa*zCa)*zCa)*V)*F_RT)*F)/(expm1(((zCa*V)*F_RT)))));
    double IpCa = ((IbarpCa*sv->Cai)/(KmpCa_uM+sv->Cai));
    double Nao3_cai = ((Nao3*sv->Cai)*0.001);
    double a_h = ((V>=-40.) ? 1.0e-10 : (0.135*(exp(((80.+V)/-6.8)))));
    double a_j = ((V>=-40.) ? 1.0e-10 : ((((-1.2714e5*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    double a_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    double a_ydv = (0.015/(1.+(exp(((V+60.)/5.)))));
    double a_zdv = ((10.*(exp(((V-(40.))/25.))))/(1.+(exp(((V-(40.))/25.)))));
    double b_h = ((V>=-40.) ? (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_inf = ((V>-120.) ? (1./(1.+(exp(((-(V+14.))/10.8))))) : 0.);
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.))));
    double b_ydv = ((0.1*(exp(((V+25.)/5.))))/(1.+(exp(((V+25.)/5.)))));
    double b_zdv = ((10.*(exp(((-(V+90.))/25.))))/(1.+(exp(((-(V+90.))/25.)))));
    double d_inf = (1./(1.+(exp(((-(V+10.))/6.24)))));
    double em1term = (expm1((V*F_RT)));
    double eterm = (exp((V*F_RT)));
    double ezca_term = (exp(((zCa*V)*F_RT)));
    double fCa = (KmCa_uM/(KmCa_uM+sv->Cai));
    double f_inf = ((1./(1.+(exp(((V+32.)/8.)))))+(0.6/(1.+(exp(((50.-(V))/20.))))));
    double g_inf = ((V<=0.) ? (1./(1.+(exp(((V+60.)/5.6))))) : 0.);
    double xr_inf = (1./(1.+(exp(((-(V+21.5))/7.5)))));
    double xs1_inf = (1./(1.+(exp(((1.5-(V))/16.7)))));
    double ICab = (a_Cab*(V-(ECa)));
    double INaCa_B = ((((eterm*sv->Nai)*sv->Nai)*sv->Nai)*Cao);
    double IbarCa = (IbarCa_c*(((sv->Cai/1000.)*ezca_term)-((0.341*Cao))));
    double IbarK_c = ((V==0.) ? ((((PK*zK)*zK)*F)*0.75) : (((((((PK*zK)*zK)*V)*F_RT)*F)*0.75)/em1term));
    double IbarNa_c = ((V==0.) ? ((((PNa*zNa)*zNa)*F)*0.75) : (((((((PNa*zNa)*zNa)*V)*F_RT)*F)*0.75)/em1term));
    double b_init = b_inf;
    double d_init = d_inf;
    double f_init = f_inf;
    double g_init = g_inf;
    double h_init = (a_h/(a_h+b_h));
    double j_init = (a_j/(a_j+b_j));
    double m_init = (a_m/(a_m+b_m));
    double xr_init = xr_inf;
    double xs1_init = xs1_inf;
    double xs2_inf = xs1_inf;
    double ydv_init = (a_ydv/(a_ydv+b_ydv));
    double zdv_init = (a_zdv/(a_zdv+b_zdv));
    double INaCa = ((INaCa_A*(INaCa_B-(Nao3_cai)))/(1.+(INaCa_C*(INaCa_B+Nao3_cai))));
    double IbarK = (IbarK_c*((sv->Ki*eterm)-(Ko)));
    double IbarNa = (IbarNa_c*((sv->Nai*eterm)-(Nao)));
    sv->b = b_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->g = g_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->xr = xr_init;
    sv->xs1 = xs1_init;
    double xs2_init = xs2_inf;
    sv->ydv = ydv_init;
    sv->zdv = zdv_init;
    double ICa_T = ((((GCaT*sv->g)*sv->b)*sv->b)*(V-(ECaT)));
    double k_CaL = ((sv->d*sv->f)*fCa);
    sv->xs2 = xs2_init;
    double ICa = (IbarCa*k_CaL);
    double ICaK = (IbarK*k_CaL);
    double ICaNa = (IbarNa*k_CaL);
    double ICaL = ((ICa+ICaNa)+ICaK);
    double ICai = (((((ICaL+ICa_T)-(INaCa))-(INaCa))+IpCa)+ICab);
    double ICaIold_init = ICai;
    sv->ICaIold = ICaIold_init;
    double fdiff_ICai = ((ICai-(sv->ICaIold))/dt);
    double fdiff_ICaIold_init = fdiff_ICai;
    sv->fdiff_ICaIold = fdiff_ICaIold_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef LUORUDY94_CPU_GENERATED
extern "C" {
void compute_LuoRudy94_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  LuoRudy94IonType::IonIfDerived& imp = static_cast<LuoRudy94IonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  LuoRudy94_Params *p  = imp.params();
  LuoRudy94_state *sv_base = (LuoRudy94_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  LuoRudy94IonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    LuoRudy94_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables()[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables()[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t EK1 = ((1./F_RT)*(log((Ko/sv->Ki))));
    GlobalData_t EKs = ((1./F_RT)*(log(((Ko+(prnak*Nao))/(sv->Ki+(prnak*sv->Nai))))));
    GlobalData_t ENa = ((log((Nao/sv->Nai)))/F_RT);
    GlobalData_t Ileak = (Kleak*sv->nsr);
    GlobalData_t Itr = ((sv->nsr-(sv->jsr))/180.);
    GlobalData_t csqn = (csqnbar*(sv->jsr/(sv->jsr+kmcsqn)));
    GlobalData_t exp_tcicr = ((sv->tcicr<0.) ? 0. : (exp((((-sv->tcicr)+4.)/tau_on))));
    GlobalData_t exp_tjsrol = (exp(((-sv->tjsrol)/tau_on_ol)));
    GlobalData_t EKp = EK1;
    GlobalData_t ICa_T = ((((GCaT*sv->g)*sv->b)*sv->b)*(V-(Cai_row[ECaT_idx])));
    GlobalData_t ICab = (a_Cab*(V-(Cai_row[ECa_idx])));
    GlobalData_t IKr = (((sv->xr*GKr)*V_row[r_idx])*(V-(EK1)));
    GlobalData_t IKs = (((Cai_row[IKs_temp_idx]*sv->xs1)*sv->xs2)*(V-(EKs)));
    GlobalData_t INa = ((((((GNa*(V-(ENa)))*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
    GlobalData_t INaCa_B = ((((V_row[eterm_idx]*sv->Nai)*sv->Nai)*sv->Nai)*Cao);
    GlobalData_t INaK = ((((p->GNaK*IbarNaK)*V_row[fNaK_idx])*(1./(1.+((KmNai*KmNai)/(sv->Nai*sv->Nai)))))*(Ko/(Ko+KmKo)));
    GlobalData_t INab = ((V-(ENa))*GbNa);
    GlobalData_t IbarCa = (V_row[IbarCa_c_idx]*(((sv->Cai/1000.)*V_row[ezca_term_idx])-((0.341*Cao))));
    GlobalData_t Iup = (Cai_row[Irup_idx]-(Ileak));
    GlobalData_t VEK1 = (V-(EK1));
    LUT_data_t VEK1_row[NROWS_VEK1];
    LUT_interpRow(&IF->tables()[VEK1_TAB], VEK1, __i, VEK1_row);
    GlobalData_t k_CaL = ((sv->d*sv->f)*Cai_row[fCa_idx]);
    GlobalData_t ol_close = exp_tjsrol;
    GlobalData_t ol_open = (1.-(exp_tjsrol));
    GlobalData_t ryr_open = (1./(1.+exp_tcicr));
    GlobalData_t tjsrol_new = (((csqn>=csqnth)&&(sv->tjsrol>50.)) ? 0. : (sv->tjsrol+dt));
    GlobalData_t ICa = (IbarCa*k_CaL);
    GlobalData_t IKp = ((GKp*V_row[Kp_idx])*(V-(EKp)));
    GlobalData_t INaCa = ((V_row[INaCa_A_idx]*(INaCa_B-(Cai_row[Nao3_cai_idx])))/(1.+(V_row[INaCa_C_idx]*(INaCa_B+Cai_row[Nao3_cai_idx]))));
    GlobalData_t IbarK = (V_row[IbarK_c_idx]*((sv->Ki*V_row[eterm_idx])-(Ko)));
    GlobalData_t IbarNa = (V_row[IbarNa_c_idx]*((sv->Nai*V_row[eterm_idx])-(Nao)));
    GlobalData_t ireljsrol = (((grelbarjsrol*ol_open)*ol_close)*(sv->jsr-((1e-3*sv->Cai))));
    GlobalData_t ryr_close = (1.-(ryr_open));
    GlobalData_t ICaK = (IbarK*k_CaL);
    GlobalData_t ICaNa = (IbarNa*k_CaL);
    GlobalData_t ICaL = ((ICa+ICaNa)+ICaK);
    GlobalData_t INai = ((((INa+INab)+ICaNa)+(3.*INaK))+(3.*INaCa));
    GlobalData_t ICai = (((((ICaL+ICa_T)-(INaCa))-(INaCa))+Cai_row[IpCa_idx])+ICab);
    GlobalData_t IKi = ((((((IKr+IKs)+VEK1_row[IK1_idx])+IKp)+ICaK)-(INaK))-(INaK));
    GlobalData_t ICaIold_new = ICai;
    Iion = ((INai+IKi)+ICai);
    GlobalData_t fdiff_ICai = ((ICai-(sv->ICaIold))/dt);
    GlobalData_t magrel = (1./(1.+(exp(((ICai+5.)/0.9)))));
    GlobalData_t fdiff_ICaIold_new = fdiff_ICai;
    GlobalData_t irelcicr = ((((gmaxrel*magrel)*ryr_open)*ryr_close)*(sv->jsr-((1e-3*sv->Cai))));
    GlobalData_t tcicr_new = ((sv->tcicr<0.) ? (((V>-35.)&&(fdiff_ICai>sv->fdiff_ICaIold)) ? 0. : -1.) : (((-Iion)>10.) ? -1. : (sv->tcicr+dt)));
    GlobalData_t dCai = ((-(((((ICai*acap)/((vmyo*zCa)*F))+((Iup*vnsr)/vmyo))-(((irelcicr*vjsr)/vmyo)))-(((ireljsrol*vjsr)/vmyo))))*dt);
    GlobalData_t djsr = (((Itr-(irelcicr))-(ireljsrol))*dt);
    GlobalData_t bjsr = ((((csqnbar-(csqn))-(djsr))-(sv->jsr))+kmcsqn);
    GlobalData_t catotal = (((Cai_row[trpn_idx]+Cai_row[cmdn_idx])+dCai)+(sv->Cai*1e-3));
    LUT_data_t catotal_row[NROWS_catotal];
    LUT_interpRow(&IF->tables()[catotal_TAB], catotal, __i, catotal_row);
    GlobalData_t cjsr = (kmcsqn*((csqn+djsr)+sv->jsr));
    GlobalData_t jsr_new = (((sqrt(((bjsr*bjsr)+(4.*cjsr))))-(bjsr))/2.);
    GlobalData_t Cai_new = catotal_row[CaIupdate_idx];
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Ki = (((-IKi)*acap)/((vmyo*zK)*F));
    GlobalData_t diff_Nai = (((-INai)*acap)/((vmyo*zNa)*F));
    GlobalData_t diff_nsr = (Iup-(((Itr*vjsr)/vnsr)));
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t nsr_new = sv->nsr+diff_nsr*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t b_rush_larsen_B = V_row[b_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t g_rush_larsen_B = V_row[g_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t xr_rush_larsen_B = V_row[xr_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_B = V_row[xs1_rush_larsen_B_idx];
    GlobalData_t ydv_rush_larsen_A = V_row[ydv_rush_larsen_A_idx];
    GlobalData_t ydv_rush_larsen_B = V_row[ydv_rush_larsen_B_idx];
    GlobalData_t zdv_rush_larsen_A = V_row[zdv_rush_larsen_A_idx];
    GlobalData_t zdv_rush_larsen_B = V_row[zdv_rush_larsen_B_idx];
    GlobalData_t b_rush_larsen_A = V_row[b_rush_larsen_A_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t g_rush_larsen_A = V_row[g_rush_larsen_A_idx];
    GlobalData_t xr_rush_larsen_A = V_row[xr_rush_larsen_A_idx];
    GlobalData_t xs1_rush_larsen_A = V_row[xs1_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_B = V_row[xs2_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_A = V_row[xs2_rush_larsen_A_idx];
    GlobalData_t b_new = b_rush_larsen_A+b_rush_larsen_B*sv->b;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t g_new = g_rush_larsen_A+g_rush_larsen_B*sv->g;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs1_new = xs1_rush_larsen_A+xs1_rush_larsen_B*sv->xs1;
    GlobalData_t xs2_new = xs2_rush_larsen_A+xs2_rush_larsen_B*sv->xs2;
    GlobalData_t ydv_new = ydv_rush_larsen_A+ydv_rush_larsen_B*sv->ydv;
    GlobalData_t zdv_new = zdv_rush_larsen_A+zdv_rush_larsen_B*sv->zdv;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    sv->ICaIold = ICaIold_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->b = b_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->fdiff_ICaIold = fdiff_ICaIold_new;
    sv->g = g_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->jsr = jsr_new;
    sv->m = m_new;
    sv->nsr = nsr_new;
    sv->tcicr = tcicr_new;
    sv->tjsrol = tjsrol_new;
    sv->xr = xr_new;
    sv->xs1 = xs1_new;
    sv->xs2 = xs2_new;
    sv->ydv = ydv_new;
    sv->zdv = zdv_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // LUORUDY94_CPU_GENERATED

bool LuoRudy94IonType::has_trace() const {
    return false;
}

void LuoRudy94IonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* LuoRudy94IonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void LuoRudy94IonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        