// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Stefano Severi, Matteo Fantini, Lara A. Charawi, Dario DiFrancesco
*  Year: 2012
*  Title: An updated computational model of rabbit sinoatrial action potential to investigate the mechanisms of heart rate modulation
*  Journal: The Journal of Physiology, 590(18):4483-99
*  DOI: 10.1113/jphysiol.2012.229435
*  Comment: Original Severi model with modified isoprenaline formulation (changes affect ICaL, Casub and Nai) and the possibility to simulate with the adapted beta-adrenergic signalling cascade (Behar et al., 2016).
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Severi.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

SeveriIonType::SeveriIonType(bool plugin) : IonType(std::move(std::string("Severi")), plugin) {}

size_t SeveriIonType::params_size() const {
  return sizeof(struct Severi_Params);
}

size_t SeveriIonType::dlo_vector_size() const {

  return 1;
}

uint32_t SeveriIonType::reqdat() const {
  return Severi_REQDAT;
}

uint32_t SeveriIonType::moddat() const {
  return Severi_MODDAT;
}

void SeveriIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target SeveriIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef SEVERI_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(SEVERI_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(SEVERI_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(SEVERI_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef SEVERI_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef SEVERI_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef SEVERI_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef SEVERI_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void SeveriIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef SEVERI_MLIR_CUDA_GENERATED
      compute_Severi_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(SEVERI_MLIR_ROCM_GENERATED)
      compute_Severi_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(SEVERI_MLIR_CPU_GENERATED)
      compute_Severi_mlir_cpu(start, end, imp, data);
#   elif defined(SEVERI_CPU_GENERATED)
      compute_Severi_cpu(start, end, imp, data);
#   else
#     error "Could not generate method SeveriIonType::compute."
#   endif
      break;
#   ifdef SEVERI_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Severi_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef SEVERI_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Severi_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef SEVERI_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Severi_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef SEVERI_CPU_GENERATED
    case Target::CPU:
      compute_Severi_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define ATPi_max (GlobalData_t)(2.533)
#define A_init (GlobalData_t)(0.06)
#define BAPTA_10_mM (GlobalData_t)(0.)
#define C (GlobalData_t)(3.2e-5)
#define CM_tot (GlobalData_t)(0.045)
#define CQ_tot (GlobalData_t)(10.)
#define Ca_jsr_init (GlobalData_t)(0.316762674605)
#define Ca_nsr_init (GlobalData_t)(1.05386465080816)
#define Ca_sub_init (GlobalData_t)(1e-5)
#define Cai_init (GlobalData_t)(1e-5)
#define Cam_init (GlobalData_t)(5e-5)
#define EC50_SR (GlobalData_t)(0.45)
#define F (GlobalData_t)(96485.3415)
#define FN (GlobalData_t)(3.5)
#define Ff (GlobalData_t)(0.04)
#define Fg0 (GlobalData_t)(0.03)
#define Fgl (GlobalData_t)(4.4e9)
#define Fk0 (GlobalData_t)(350.)
#define Fk05 (GlobalData_t)(2.5e9)
#define Fk1 (GlobalData_t)(3000.)
#define Fkl (GlobalData_t)(60.)
#define GKACh_max (GlobalData_t)(0.00864)
#define HSR (GlobalData_t)(2.5)
#define I_init (GlobalData_t)(7.86181717518e-8)
#define K1ni (GlobalData_t)(395.3)
#define K1no (GlobalData_t)(1628.)
#define K2ni (GlobalData_t)(2.289)
#define K2no (GlobalData_t)(561.4)
#define K3ni (GlobalData_t)(26.44)
#define K3no (GlobalData_t)(4.663)
#define KATP_min (GlobalData_t)(6034.)
#define K_05ICaL1 (GlobalData_t)(0.7213)
#define K_05ICaL2 (GlobalData_t)(0.6535)
#define K_05IKs1 (GlobalData_t)(0.7109)
#define K_05IKs2 (GlobalData_t)(0.6955)
#define K_05INaK (GlobalData_t)(0.7109)
#define K_05RyR (GlobalData_t)(0.6753)
#define K_05if (GlobalData_t)((17.33/600.))
#define K_05iso (GlobalData_t)(123.71007691)
#define K_AC (GlobalData_t)(0.0735)
#define K_ACCa (GlobalData_t)(0.000024)
#define K_ACI (GlobalData_t)(0.016)
#define K_Ca (GlobalData_t)(0.0000913802)
#define K_Cam (GlobalData_t)(0.003)
#define K_ICaL1 (GlobalData_t)(0.4677)
#define K_ICaL2 (GlobalData_t)(27.4444)
#define K_IKs1 (GlobalData_t)(0.4333)
#define K_IKs2 (GlobalData_t)(34.2549)
#define K_INaK (GlobalData_t)(0.4333)
#define K_iso (GlobalData_t)(0.025)
#define K_up (GlobalData_t)(0.0006)
#define Kci (GlobalData_t)(0.0207)
#define Kcni (GlobalData_t)(26.44)
#define Kco (GlobalData_t)(3.663)
#define Ki (GlobalData_t)(140.)
#define Kif (GlobalData_t)(24.4)
#define Km_Kp (GlobalData_t)(1.4)
#define Km_Nap (GlobalData_t)(14.)
#define Km_f (GlobalData_t)(45.)
#define Km_fCa (GlobalData_t)(0.00035)
#define Ko (GlobalData_t)(5.4)
#define L_cell (GlobalData_t)(70.)
#define L_sub (GlobalData_t)(0.02)
#define MaxSR (GlobalData_t)(15.)
#define Mgi (GlobalData_t)(2.5)
#define MinSR (GlobalData_t)(1.)
#define Nai_init (GlobalData_t)(7.5)
#define Nao (GlobalData_t)(140.)
#define Nc (GlobalData_t)(2e13)
#define O_init (GlobalData_t)(1.7340201253e-7)
#define PI (GlobalData_t)(3.141592653589793238462643383279502884)
#define PKAtot (GlobalData_t)(1.)
#define PKItot (GlobalData_t)(0.3)
#define PLBp_init (GlobalData_t)(0.23)
#define PP1 (GlobalData_t)(0.89e-3)
#define P_Ca (GlobalData_t)(0.256)
#define P_up_basal (GlobalData_t)(0.012)
#define Q_mo (GlobalData_t)(0.006144)
#define Qci (GlobalData_t)(0.1369)
#define Qco (GlobalData_t)(0.)
#define Qn (GlobalData_t)(0.4315)
#define R (GlobalData_t)(8314.472)
#define RI_init (GlobalData_t)(0.211148145512825)
#define R_Ca_SR_release_init (GlobalData_t)(0.912317231017262)
#define R_cell (GlobalData_t)(4.)
#define RyR_max (GlobalData_t)(0.02)
#define RyR_min (GlobalData_t)(0.0127)
#define SL (GlobalData_t)(1.75e-3)
#define SL0 (GlobalData_t)(0.8e-3)
#define T (GlobalData_t)(310.)
#define TC_tot (GlobalData_t)(0.031)
#define TMC_tot (GlobalData_t)(0.062)
#define TT_init (GlobalData_t)(0.02)
#define T_Ca_dynamics (GlobalData_t)(6.928e3)
#define U_init (GlobalData_t)(0.06)
#define V_init (GlobalData_t)(-52.)
#define Vi_part (GlobalData_t)(0.46)
#define Vif05 (GlobalData_t)(-52.5)
#define Vjsr_part (GlobalData_t)(0.0012)
#define Vmyto (GlobalData_t)(633.4)
#define Vnsr_part (GlobalData_t)(0.0116)
#define a_init (GlobalData_t)(0.)
#define alpha_e (GlobalData_t)(0.341)
#define alpha_fCa (GlobalData_t)(0.01e-3)
#define alpha_m (GlobalData_t)(0.2)
#define cAMP_init (GlobalData_t)((19.73/600.))
#define cAMPb (GlobalData_t)((20./600.))
#define dL_init (GlobalData_t)(0.)
#define dT_init (GlobalData_t)(0.)
#define delta_m (GlobalData_t)(1e-5)
#define fBAPTA_init (GlobalData_t)(0.)
#define fBAPTA_sub_init (GlobalData_t)(0.)
#define fCMi_init (GlobalData_t)(0.0373817991524254)
#define fCMs_init (GlobalData_t)(0.054381370046)
#define fCQ_init (GlobalData_t)(0.299624275428735)
#define fCa_init (GlobalData_t)(0.697998543259722)
#define fL_init (GlobalData_t)(0.497133507285601)
#define fTC_init (GlobalData_t)(0.0180519400676086)
#define fTMC_init (GlobalData_t)(0.281244308217086)
#define fTMM_init (GlobalData_t)(0.501049376634)
#define fT_init (GlobalData_t)(0.)
#define h_init (GlobalData_t)(1.3676940140066e-5)
#define kATP (GlobalData_t)(6142.)
#define kATP05 (GlobalData_t)(6724.)
#define kPKA (GlobalData_t)((9000./600.))
#define kPKA_PLB (GlobalData_t)(1.5925)
#define kPKA_cAMP (GlobalData_t)((284.5/600.))
#define kPLBp (GlobalData_t)(52.25)
#define kPP1 (GlobalData_t)(23.575e3)
#define kPP1_PLB (GlobalData_t)(0.05967)
#define kbBAPTA (GlobalData_t)(0.11938)
#define kb_CM (GlobalData_t)(0.542)
#define kb_CQ (GlobalData_t)(0.445)
#define kb_TC (GlobalData_t)(0.446)
#define kb_TMC (GlobalData_t)(0.00751)
#define kb_TMM (GlobalData_t)(0.751)
#define kfBAPTA (GlobalData_t)(940.)
#define kf_CM (GlobalData_t)(227.7)
#define kf_CQ (GlobalData_t)(0.534)
#define kf_TC (GlobalData_t)(88.8)
#define kf_TMC (GlobalData_t)(227.7)
#define kf_TMM (GlobalData_t)(2.277)
#define kiCa (GlobalData_t)(0.500)
#define kim (GlobalData_t)(0.005)
#define koCa_max (GlobalData_t)(10.0)
#define kom (GlobalData_t)(0.06)
#define ks (GlobalData_t)(250e3)
#define m_init (GlobalData_t)(0.440131579215766)
#define nATP (GlobalData_t)(3.36)
#define nPKA (GlobalData_t)(5.)
#define nPLB (GlobalData_t)(1.)
#define nRyR (GlobalData_t)(9.773)
#define n_init (GlobalData_t)(0.)
#define nif (GlobalData_t)(9.281)
#define niso (GlobalData_t)(0.6064)
#define paF_init (GlobalData_t)(0.0990510403258968)
#define paS_init (GlobalData_t)(0.322999177802891)
#define phi_m (GlobalData_t)(154.226)
#define piy_init (GlobalData_t)(0.705410877258545)
#define q_init (GlobalData_t)(0.506139850982478)
#define r_init (GlobalData_t)(0.0144605370597924)
#define shift (GlobalData_t)(0.)
#define tau_dif_Ca (GlobalData_t)(0.04)
#define tau_tr (GlobalData_t)(40.)
#define y_init (GlobalData_t)(0.181334538702451)
#define RTONF (GlobalData_t)(((R*T)/F))
#define Vcell (GlobalData_t)(((PI*(R_cell*R_cell))*L_cell))
#define Vsub (GlobalData_t)(((((2.*PI)*L_sub)*(R_cell-((L_sub/2.))))*L_cell))
#define k34 (GlobalData_t)((Nao/(K3no+Nao)))
#define E_K (GlobalData_t)((RTONF*(log((Ko/Ki)))))
#define E_Ks (GlobalData_t)((RTONF*(log((Ko/Ki)))))
#define Vi (GlobalData_t)(((Vi_part*Vcell)-(Vsub)))
#define Vjsr (GlobalData_t)((Vjsr_part*Vcell))
#define Vnsr (GlobalData_t)((Vnsr_part*Vcell))



void SeveriIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Severi_Params *p = imp.params();

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->CCh_cas = 0.;
    p->Cao = 1.8;
    p->Cs_5_mM = 0.;
    p->GKr_max = 0.0021637;
    p->GNa_max = 0.0125;
    p->Gto_max = 0.002;
    p->INaK_max = 0.063;
    p->Iso_1_uM = 0.;
    p->Iso_cas = -1.0;
    p->Iva_3_uM = 0.;
    p->K_Iso_inc_cas_ICaL = 1.0;
    p->K_Iso_inc_cas_IKs = 1.0;
    p->K_Iso_inc_cas_INaK = 1.0;
    p->K_Iso_shift_cas_ICaL_dL_gate = 1.0;
    p->K_Iso_shift_cas_IKs_n_gate = 1.0;
    p->K_Iso_shift_cas_If_y_gate = 1.0;
    p->K_Iso_slope_cas_ICaL = 1.0;
    p->K_NaCa = 4.;
    p->K_j_SRCarel = 1.0;
    p->K_j_up = 1.0;
    p->K_k1 = 0.92;
    p->K_koCa = 1.0;
    p->P_CaL = 0.2;
    p->P_CaT = 0.02;
    p->GKs_max = ((p->Iso_1_uM>0.) ? (1.2*0.0016576) : 0.0016576);
    p->Gf_K_max = ((p->Iva_3_uM>=1.) ? (0.03*(1.-(0.66))) : 0.03);
    p->Gf_Na_max = ((p->Iva_3_uM>=1.) ? (0.03*(1.-(0.66))) : 0.03);
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void SeveriIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Severi_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+0.00009));
  double ACh_shift = ((p->ACh>0.) ? (-1.-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+0.00122423)))) : 0.);
  double IKACh_a_gate_alpha_a = ((((3.5988-(0.025641))/(1.+(0.0000012155/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.);
  double Iso_increase = ((p->Iso_1_uM>0.) ? 1.2 : 1.);
  double Iso_increase_ICaL = ((p->Iso_1_uM>0.) ? 1.23 : 1.);
  double Iso_shift = ((p->Iso_1_uM>0.) ? 7.5 : 0.);
  double Iso_shift_ICaL_dL_gate = ((p->Iso_1_uM>0.) ? -8. : 0.);
  double Iso_shift_IKs_n_gate = ((p->Iso_1_uM>0.) ? -14. : 0.);
  double Iso_slope = ((p->Iso_1_uM>0.) ? 0.69 : 1.);
  double b_up = ((p->Iso_1_uM>0.) ? -0.25 : ((p->ACh>0.) ? (((0.7*1e-6)*p->ACh)/(0.00009+(1e-6*p->ACh))) : 0.));
  double kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  double kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  double P_up = (P_up_basal*(1.-(b_up)));

}



void SeveriIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Severi_Params *p = imp.params();

  Severi_state *sv_base = (Severi_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+0.00009));
  double ACh_shift = ((p->ACh>0.) ? (-1.-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+0.00122423)))) : 0.);
  double IKACh_a_gate_alpha_a = ((((3.5988-(0.025641))/(1.+(0.0000012155/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.);
  double Iso_increase = ((p->Iso_1_uM>0.) ? 1.2 : 1.);
  double Iso_increase_ICaL = ((p->Iso_1_uM>0.) ? 1.23 : 1.);
  double Iso_shift = ((p->Iso_1_uM>0.) ? 7.5 : 0.);
  double Iso_shift_ICaL_dL_gate = ((p->Iso_1_uM>0.) ? -8. : 0.);
  double Iso_shift_IKs_n_gate = ((p->Iso_1_uM>0.) ? -14. : 0.);
  double Iso_slope = ((p->Iso_1_uM>0.) ? 0.69 : 1.);
  double b_up = ((p->Iso_1_uM>0.) ? -0.25 : ((p->ACh>0.) ? (((0.7*1e-6)*p->ACh)/(0.00009+(1e-6*p->ACh))) : 0.));
  double kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  double kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  double P_up = (P_up_basal*(1.-(b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Severi_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    Iion *= 1e3;
    
    
    // Initialize the rest of the nodal variables
    sv->A = A_init;
    sv->Ca_jsr = Ca_jsr_init;
    sv->Ca_nsr = Ca_nsr_init;
    sv->Ca_sub = Ca_sub_init;
    sv->Cai = Cai_init;
    sv->Cam = Cam_init;
    sv->I = I_init;
    sv->Nai = Nai_init;
    sv->O = O_init;
    sv->PLBp = PLBp_init;
    sv->RI = RI_init;
    sv->R_Ca_SR_release = R_Ca_SR_release_init;
    sv->TT = TT_init;
    sv->U = U_init;
    V = V_init;
    sv->a = a_init;
    sv->cAMP = cAMP_init;
    sv->dL = dL_init;
    sv->dT = dT_init;
    sv->fBAPTA = fBAPTA_init;
    sv->fBAPTA_sub = fBAPTA_sub_init;
    sv->fCMi = fCMi_init;
    sv->fCMs = fCMs_init;
    sv->fCQ = fCQ_init;
    sv->fCa = fCa_init;
    sv->fL = fL_init;
    sv->fT = fT_init;
    sv->fTC = fTC_init;
    sv->fTMC = fTMC_init;
    sv->fTMM = fTMM_init;
    sv->h = h_init;
    sv->m = m_init;
    sv->n = n_init;
    sv->paF = paF_init;
    sv->paS = paS_init;
    sv->piy = piy_init;
    sv->q = q_init;
    sv->r = r_init;
    sv->y = y_init;
    double ICaT = ((((((2.*p->P_CaT)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dT)*sv->fT);
    double ICs_on_Icontrol = ((p->Cs_5_mM>=1.) ? ((10.6015/5.)/((10.6015/5.)+(exp(((-0.71*V)/25.))))) : 1.);
    double IKACh = ((p->ACh>0.) ? (((GKACh_max*(V-(E_K)))*(1.+(exp(((V+20.)/20.)))))*sv->a) : 0.);
    double IKr = (((p->GKr_max*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    double Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
    double Nai_BAPTA = ((BAPTA_10_mM>0.) ? 7.5 : sv->Nai);
    double PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
    double k32 = (exp(((Qn*V)/(2.*RTONF))));
    double k41 = (exp((((-Qn)*V)/(2.*RTONF))));
    double var_do = ((1.+((p->Cao/Kco)*(1.+(exp(((Qco*V)/RTONF))))))+((Nao/K1no)*(1.+((Nao/K2no)*(1.+(Nao/K3no))))));
    double E_Na = (RTONF*(log((Nao/Nai_BAPTA))));
    double E_mh = (RTONF*(log(((Nao+(0.12*Ko))/(Nai_BAPTA+(0.12*Ki))))));
    double IfK = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_K_max)*(V-(E_K)))*ICs_on_Icontrol);
    double Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_ICaL*(-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    double Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_IKs*(-0.2152+((K_IKs1*(pow(PKA,10.0808)))/((pow(K_05IKs1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    double Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_INaK*(-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    double di = ((1.+((sv->Ca_sub/Kci)*((1.+(exp((((-Qci)*V)/RTONF))))+(Nai_BAPTA/Kcni))))+((Nai_BAPTA/K1ni)*(1.+((Nai_BAPTA/K2ni)*(1.+(Nai_BAPTA/K3ni))))));
    double k21 = (((p->Cao/Kco)*(exp(((Qco*V)/RTONF))))/var_do);
    double k23 = ((((((Nao/K1no)*Nao)/K2no)*(1.+(Nao/K3no)))*(exp((((-Qn)*V)/(2.*RTONF)))))/var_do);
    double k43 = (Nai_BAPTA/(K3ni+Nai_BAPTA));
    double IKs = ((((1.+Iso_inc_cas_IKs)*p->GKs_max)*(V-(E_Ks)))*(sv->n*sv->n));
    double INa = (((p->GNa_max*((sv->m*sv->m)*sv->m))*sv->h)*(V-(E_mh)));
    double INaK = ((((((1.+Iso_inc_cas_INaK)*Iso_increase)*p->INaK_max)*(1./(1.+(pow((Km_Kp/Ko),1.2)))))*(1./(1.+(pow((Km_Nap/Nai_BAPTA),1.3)))))*(1./(1.+(exp(((-((V-(E_Na))+110.))/20.))))));
    double IfNa = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_Na_max)*(V-(E_Na)))*ICs_on_Icontrol);
    double IsiCa = ((((((((((2.*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiK = ((((((((((0.000365*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Ki-((Ko*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiNa = ((((((((((0.0000185*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Nai_BAPTA-((Nao*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double k12 = (((sv->Ca_sub/Kci)*(exp((((-Qci)*V)/RTONF))))/di);
    double k14 = ((((((Nai_BAPTA/K1ni)*Nai_BAPTA)/K2ni)*(1.+(Nai_BAPTA/K3ni)))*(exp(((Qn*V)/(2.*RTONF)))))/di);
    double x1 = (((k41*k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    double ICaL = ((IsiCa+IsiK)+IsiNa);
    double If = (IfNa+IfK);
    double x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(k34+k32)));
    double x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    double x4 = (((k23*k34)*(k14+k12))+((k14*k21)*(k34+k32)));
    double INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    double Itot = (((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh);
    Iion = (-((-Itot)/C));
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    Iion *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef SEVERI_CPU_GENERATED
extern "C" {
void compute_Severi_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  SeveriIonType::IonIfDerived& imp = static_cast<SeveriIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Severi_Params *p  = imp.params();
  Severi_state *sv_base = (Severi_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  SeveriIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+0.00009));
  GlobalData_t ACh_shift = ((p->ACh>0.) ? (-1.-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+0.00122423)))) : 0.);
  GlobalData_t IKACh_a_gate_alpha_a = ((((3.5988-(0.025641))/(1.+(0.0000012155/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.);
  GlobalData_t Iso_increase = ((p->Iso_1_uM>0.) ? 1.2 : 1.);
  GlobalData_t Iso_increase_ICaL = ((p->Iso_1_uM>0.) ? 1.23 : 1.);
  GlobalData_t Iso_shift = ((p->Iso_1_uM>0.) ? 7.5 : 0.);
  GlobalData_t Iso_shift_ICaL_dL_gate = ((p->Iso_1_uM>0.) ? -8. : 0.);
  GlobalData_t Iso_shift_IKs_n_gate = ((p->Iso_1_uM>0.) ? -14. : 0.);
  GlobalData_t Iso_slope = ((p->Iso_1_uM>0.) ? 0.69 : 1.);
  GlobalData_t b_up = ((p->Iso_1_uM>0.) ? -0.25 : ((p->ACh>0.) ? (((0.7*1e-6)*p->ACh)/(0.00009+(1e-6*p->ACh))) : 0.));
  GlobalData_t kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  GlobalData_t kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  GlobalData_t P_up = (P_up_basal*(1.-(b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Severi_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    Iion *= 1e3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t ICaT = ((((((2.*p->P_CaT)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dT)*sv->fT);
    GlobalData_t ICs_on_Icontrol = ((p->Cs_5_mM>=1.) ? ((10.6015/5.)/((10.6015/5.)+(exp(((-0.71*V)/25.))))) : 1.);
    GlobalData_t IKACh = ((p->ACh>0.) ? (((GKACh_max*(V-(E_K)))*(1.+(exp(((V+20.)/20.)))))*sv->a) : 0.);
    GlobalData_t IKr = (((p->GKr_max*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    GlobalData_t Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
    GlobalData_t Nai_BAPTA = ((BAPTA_10_mM>0.) ? 7.5 : sv->Nai);
    GlobalData_t PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
    GlobalData_t k32 = (exp(((Qn*V)/(2.*RTONF))));
    GlobalData_t k41 = (exp((((-Qn)*V)/(2.*RTONF))));
    GlobalData_t var_do = ((1.+((p->Cao/Kco)*(1.+(exp(((Qco*V)/RTONF))))))+((Nao/K1no)*(1.+((Nao/K2no)*(1.+(Nao/K3no))))));
    GlobalData_t E_Na = (RTONF*(log((Nao/Nai_BAPTA))));
    GlobalData_t E_mh = (RTONF*(log(((Nao+(0.12*Ko))/(Nai_BAPTA+(0.12*Ki))))));
    GlobalData_t IfK = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_K_max)*(V-(E_K)))*ICs_on_Icontrol);
    GlobalData_t Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_ICaL*(-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    GlobalData_t Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_IKs*(-0.2152+((K_IKs1*(pow(PKA,10.0808)))/((pow(K_05IKs1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    GlobalData_t Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_INaK*(-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    GlobalData_t di = ((1.+((sv->Ca_sub/Kci)*((1.+(exp((((-Qci)*V)/RTONF))))+(Nai_BAPTA/Kcni))))+((Nai_BAPTA/K1ni)*(1.+((Nai_BAPTA/K2ni)*(1.+(Nai_BAPTA/K3ni))))));
    GlobalData_t k21 = (((p->Cao/Kco)*(exp(((Qco*V)/RTONF))))/var_do);
    GlobalData_t k23 = ((((((Nao/K1no)*Nao)/K2no)*(1.+(Nao/K3no)))*(exp((((-Qn)*V)/(2.*RTONF)))))/var_do);
    GlobalData_t k43 = (Nai_BAPTA/(K3ni+Nai_BAPTA));
    GlobalData_t IKs = ((((1.+Iso_inc_cas_IKs)*p->GKs_max)*(V-(E_Ks)))*(sv->n*sv->n));
    GlobalData_t INa = (((p->GNa_max*((sv->m*sv->m)*sv->m))*sv->h)*(V-(E_mh)));
    GlobalData_t INaK = ((((((1.+Iso_inc_cas_INaK)*Iso_increase)*p->INaK_max)*(1./(1.+(pow((Km_Kp/Ko),1.2)))))*(1./(1.+(pow((Km_Nap/Nai_BAPTA),1.3)))))*(1./(1.+(exp(((-((V-(E_Na))+110.))/20.))))));
    GlobalData_t IfNa = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_Na_max)*(V-(E_Na)))*ICs_on_Icontrol);
    GlobalData_t IsiCa = ((((((((((2.*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiK = ((((((((((0.000365*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Ki-((Ko*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiNa = ((((((((((0.0000185*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Nai_BAPTA-((Nao*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t k12 = (((sv->Ca_sub/Kci)*(exp((((-Qci)*V)/RTONF))))/di);
    GlobalData_t k14 = ((((((Nai_BAPTA/K1ni)*Nai_BAPTA)/K2ni)*(1.+(Nai_BAPTA/K3ni)))*(exp(((Qn*V)/(2.*RTONF)))))/di);
    GlobalData_t x1 = (((k41*k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    GlobalData_t ICaL = ((IsiCa+IsiK)+IsiNa);
    GlobalData_t If = (IfNa+IfK);
    GlobalData_t x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(k34+k32)));
    GlobalData_t x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    GlobalData_t x4 = (((k23*k34)*(k14+k12))+((k14*k21)*(k34+k32)));
    GlobalData_t INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    GlobalData_t Itot = (((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh);
    Iion = (-((-Itot)/C));
    
    
    //Complete Forward Euler Update
    GlobalData_t ATPi = ((ATPi_max*(((kATP*(pow(((sv->cAMP*100.)/cAMPb),nATP)))/(kATP05+(pow(((sv->cAMP*100.)/cAMPb),nATP))))-(KATP_min)))/100.);
    GlobalData_t BAPTA = (((BAPTA_10_mM>0.)&&(t>T_Ca_dynamics)) ? 10. : 0.);
    GlobalData_t E0_m = (V+41.);
    GlobalData_t F_PLBp = ((sv->PLBp>0.23) ? ((3.3931*(pow(sv->PLBp,4.0695)))/((pow(0.2805,4.0695))+(pow(sv->PLBp,4.0695)))) : ((1.698*(pow(sv->PLBp,13.5842)))/((pow(0.2240,13.5842))+(pow(sv->PLBp,13.5842)))));
    GlobalData_t ICaL_fCa_gate_fCa_infinity = (Km_fCa/(Km_fCa+sv->Ca_sub));
    GlobalData_t ICaL_fL_gate_fL_infinity = (1./(1.+(exp(((V+37.4)/5.3)))));
    GlobalData_t ICaL_fL_gate_tau_fL = ((0.001*(44.3+(230.*(exp((-(((V+36.)/10.)*((V+36.)/10.))))))))*1000.);
    GlobalData_t ICaT_dT_gate_dT_infinity = (1./(1.+(exp(((-(V+38.3))/5.5)))));
    GlobalData_t ICaT_dT_gate_tau_dT = ((0.001/((1.068*(exp(((V+38.3)/30.))))+(1.068*(exp(((-(V+38.3))/30.))))))*1000.);
    GlobalData_t ICaT_fT_gate_fT_infinity = (1./(1.+(exp(((V+58.7)/3.8)))));
    GlobalData_t ICaT_fT_gate_tau_fT = ((1./((16.67*(exp(((-(V+75.))/83.3))))+(16.67*(exp(((V+75.)/15.38))))))*1000.);
    GlobalData_t IKACh_a_gate_beta_a = ((10.*(exp((0.0133*(V+40.)))))/1000.);
    GlobalData_t IKr_pa_gate_pa_infinity = (1./(1.+(exp(((-(V+14.8))/8.5)))));
    GlobalData_t IKr_pa_gate_tau_paF = ((1./((30.*(exp((V/10.))))+(exp(((-V)/12.)))))*1000.);
    GlobalData_t IKr_pa_gate_tau_paS = ((0.846554/((4.2*(exp((V/17.))))+(0.15*(exp(((-V)/21.6))))))*1000.);
    GlobalData_t IKr_pi_gate_pi_infinity = (1./(1.+(exp(((V+28.6)/17.1)))));
    GlobalData_t IKr_pi_gate_tau_pi = ((1./((100.*(exp(((-V)/54.645))))+(656.*(exp((V/106.157))))))*1000.);
    GlobalData_t INa_h_gate_alpha_h = ((20.*(exp((-0.125*(V+75.)))))/1000.);
    GlobalData_t INa_h_gate_beta_h = ((2000./((320.*(exp((-0.1*(V+75.)))))+1.))/1000.);
    GlobalData_t INa_m_gate_beta_m = ((8000.*(exp((-0.056*(V+66.)))))/1000.);
    GlobalData_t If_y_gate_tau_y = ((0.7166529/((0.0708*(exp(((-(((V+5.)-(ACh_shift))-(Iso_shift)))/20.2791))))+(10.6*(exp((((V-(ACh_shift))-(Iso_shift))/18.))))))*1000.);
    GlobalData_t Iso_shift_cas_ICaL_dL_gate = ((p->Iso_cas>-0.1) ? (-((((p->K_Iso_shift_cas_ICaL_dL_gate*K_ICaL2)*(pow(PKA,9.281)))/((pow(K_05ICaL2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
    GlobalData_t Iso_shift_cas_IKs_n_gate = ((p->Iso_cas>-0.1) ? (-((((p->K_Iso_shift_cas_IKs_n_gate*K_IKs2)*(pow(PKA,9.281)))/((pow(K_05IKs2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
    GlobalData_t Iso_shift_cas_If_y_gate = ((p->Iso_cas>-0.1) ? (((p->K_Iso_shift_cas_If_y_gate*Kif)*((pow(sv->cAMP,nif))/((pow(K_05if,nif))+(pow(sv->cAMP,nif)))))-(18.76)) : 0.);
    GlobalData_t Iso_slope_cas_ICaL = ((p->Iso_cas>-0.1) ? (p->K_Iso_slope_cas_ICaL*((-1.199*PKA)+1.8526)) : 1.);
    GlobalData_t Ito_q_gate_q_infinity = (1./(1.+(exp(((V+49.)/13.)))));
    GlobalData_t Ito_q_gate_tau_q = (((0.001*0.6)*((65.17/((0.57*(exp((-0.08*(V+44.)))))+(0.065*(exp((0.1*(V+45.93)))))))+10.1))*1000.);
    GlobalData_t Ito_r_gate_r_infinity = (1./(1.+(exp(((-(V-(19.3)))/15.)))));
    GlobalData_t Ito_r_gate_tau_r = ((((0.001*0.66)*1.4)*((15.59/((1.037*(exp((0.09*(V+30.61)))))+(0.369*(exp((-0.12*(V+23.84)))))))+2.98))*1000.);
    GlobalData_t Nxb = ((((SL-(SL0))/2.)*Nc)*(sv->TT+sv->U));
    GlobalData_t adVm = ((V==-41.8) ? -41.80001 : ((V==0.) ? 0. : ((V==-6.8) ? -6.80001 : V)));
    GlobalData_t bdVm = ((V==-1.8) ? -1.80001 : V);
    GlobalData_t delta_fCMi = (((kf_CM*sv->Cai)*(1.-(sv->fCMi)))-((kb_CM*sv->fCMi)));
    GlobalData_t delta_fCMs = (((kf_CM*sv->Ca_sub)*(1.-(sv->fCMs)))-((kb_CM*sv->fCMs)));
    GlobalData_t delta_fCQ = (((kf_CQ*sv->Ca_jsr)*(1.-(sv->fCQ)))-((kb_CQ*sv->fCQ)));
    GlobalData_t delta_fTC = (((kf_TC*sv->Cai)*(1.-(sv->fTC)))-((kb_TC*sv->fTC)));
    GlobalData_t delta_fTMC = (((kf_TMC*sv->Cai)*(1.-((sv->fTMC+sv->fTMM))))-((kb_TMC*sv->fTMC)));
    GlobalData_t delta_fTMM = (((kf_TMM*Mgi)*(1.-((sv->fTMC+sv->fTMM))))-((kb_TMM*sv->fTMM)));
    GlobalData_t diff_Nai = ((-1.*((((INa+IfNa)+IsiNa)+(3.*INaK))+(3.*INaCa)))/(((Vi*0.000000001)+(Vsub*0.000000001))*(F*1000.)));
    GlobalData_t j_Ca_dif = ((sv->Ca_sub-(sv->Cai))/tau_dif_Ca);
    GlobalData_t j_NaCam = (Q_mo*(sv->Cam/(K_Cam+sv->Cam)));
    GlobalData_t j_SRCarel = (p->K_j_SRCarel*((ks*sv->O)*(sv->Ca_jsr-(sv->Ca_sub))));
    GlobalData_t j_tr = ((sv->Ca_nsr-(sv->Ca_jsr))/tau_tr);
    GlobalData_t j_uni = (((((P_Ca*2.)*phi_m)/RTONF)*(((alpha_m*sv->Cam)*(exp(((-2.*phi_m)/RTONF))))-((alpha_e*sv->Cai))))/((exp(((-2.*phi_m)/RTONF)))-(1.)));
    GlobalData_t k1 = (p->K_k1*(K_ACI+(K_AC/(1.+(exp(((K_Ca-((kb_CM*(sv->fCMi/(kf_CM*(1.-(sv->fCMi)))))))/K_ACCa)))))));
    GlobalData_t k2 = ((1.1*237.9851)*((pow((sv->cAMP*600.),5.101))/((pow(20.1077,6.101))+(pow((sv->cAMP*600.),6.101)))));
    GlobalData_t k3 = (kPKA*((pow(sv->cAMP,(nPKA-(1.))))/((pow(kPKA_cAMP,nPKA))+(pow(sv->cAMP,nPKA)))));
    GlobalData_t k4 = ((kPLBp*(pow(PKA,nPLB)))/((pow(kPKA_PLB,nPLB))+(pow(PKA,nPLB))));
    GlobalData_t k5 = (kPP1*((PP1*sv->PLBp)/(kPP1_PLB+sv->PLBp)));
    GlobalData_t kCaSR = (MaxSR-(((MaxSR-(MinSR))/(1.+(pow((EC50_SR/sv->Ca_jsr),HSR))))));
    GlobalData_t koCa = ((p->Iso_cas>-0.1) ? (p->K_koCa*(koCa_max*((RyR_min-(((RyR_max*(pow(PKA,nRyR)))/((pow(K_05RyR,nRyR))+(pow(PKA,nRyR))))))+1.))) : 10.);
    GlobalData_t ICaL_dL_gate_alpha_dL = ((((-0.02839*(((adVm+41.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/((exp(((-(((adVm+41.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/2.5)))-(1.)))-(((0.0849*(((adVm+6.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/((exp(((-(((adVm+6.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/4.8)))-(1.)))))/1000.);
    GlobalData_t ICaL_dL_gate_beta_dL = (((0.01143*(((bdVm+1.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/((exp(((((bdVm+1.8)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate))/2.5)))-(1.)))/1000.);
    GlobalData_t ICaL_dL_gate_dL_infinity = (1./(1.+(exp(((-(((V+20.3)-(Iso_shift_ICaL_dL_gate))-(Iso_shift_cas_ICaL_dL_gate)))/((Iso_slope*Iso_slope_cas_ICaL)*4.2))))));
    GlobalData_t ICaL_fCa_gate_tau_fCa = ((0.001*ICaL_fCa_gate_fCa_infinity)/alpha_fCa);
    GlobalData_t IKACh_a_gate_a_infinity = (IKACh_a_gate_alpha_a/(IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a));
    GlobalData_t IKACh_a_gate_tau_a = (1./(IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a));
    GlobalData_t IKs_n_gate_alpha_n = ((28./(1.+(exp(((-(((V-(40.))-(Iso_shift_IKs_n_gate))-(Iso_shift_cas_IKs_n_gate)))/3.)))))/1000.);
    GlobalData_t IKs_n_gate_beta_n = ((exp(((-((((V-(Iso_shift_IKs_n_gate))-(Iso_shift_cas_IKs_n_gate))-(shift))-(5.)))/25.)))/1000.);
    GlobalData_t IKs_n_gate_n_infinity = ((14./(1.+(exp(((-(((V-(40.))-(Iso_shift_IKs_n_gate))-(Iso_shift_cas_IKs_n_gate)))/12.)))))/((14./(1.+(exp(((-((V-(40.))-(Iso_shift_IKs_n_gate)))/12.)))))+(exp(((-(V-(Iso_shift_IKs_n_gate)))/45.)))));
    GlobalData_t INa_m_gate_alpha_m = ((((fabs(E0_m))<delta_m) ? 2000. : ((200.*E0_m)/(1.-((exp((-0.1*E0_m)))))))/1000.);
    GlobalData_t If_y_gate_y_infinity = (1./(1.+(exp((((((V-(Vif05))-(ACh_shift))-(Iso_shift))-(Iso_shift_cas_If_y_gate))/9.)))));
    GlobalData_t KCa = (Fk0+((Fk1*(pow(Nxb,FN)))/((pow(Fk05,FN))+(pow(Nxb,FN)))));
    GlobalData_t diff_Ca_jsr = (j_tr-((j_SRCarel+(CQ_tot*delta_fCQ))));
    GlobalData_t diff_Ca_sub = ((((j_SRCarel*Vjsr)/Vsub)-((((((IsiCa+ICaT)-((2.*INaCa)))/(((2.*(F*1000.))*Vsub)*0.000000001))+j_Ca_dif)+(CM_tot*delta_fCMs))))-((((kfBAPTA*sv->Ca_sub)*(BAPTA-(sv->fBAPTA_sub)))-((kbBAPTA*sv->fBAPTA_sub)))));
    GlobalData_t diff_Cam = ((Vmyto/Vi)*(j_uni-(j_NaCam)));
    GlobalData_t diff_PLBp = ((k4-(k5))/60000.);
    GlobalData_t diff_cAMP = ((((((kiso-(kcch))*ATPi)+(k1*ATPi))-((k2*sv->cAMP)))-((k3*sv->cAMP)))/60000.);
    GlobalData_t diff_dT = ((ICaT_dT_gate_dT_infinity-(sv->dT))/ICaT_dT_gate_tau_dT);
    GlobalData_t diff_fBAPTA = (((kfBAPTA*sv->Cai)*(BAPTA-(sv->fBAPTA)))-((kbBAPTA*sv->fBAPTA)));
    GlobalData_t diff_fBAPTA_sub = (((kfBAPTA*sv->Ca_sub)*(BAPTA-(sv->fBAPTA_sub)))-((kbBAPTA*sv->fBAPTA_sub)));
    GlobalData_t diff_fCMi = delta_fCMi;
    GlobalData_t diff_fCMs = delta_fCMs;
    GlobalData_t diff_fCQ = delta_fCQ;
    GlobalData_t diff_fL = ((ICaL_fL_gate_fL_infinity-(sv->fL))/ICaL_fL_gate_tau_fL);
    GlobalData_t diff_fT = ((ICaT_fT_gate_fT_infinity-(sv->fT))/ICaT_fT_gate_tau_fT);
    GlobalData_t diff_fTC = delta_fTC;
    GlobalData_t diff_fTMC = delta_fTMC;
    GlobalData_t diff_fTMM = delta_fTMM;
    GlobalData_t diff_h = ((INa_h_gate_alpha_h*(1.00000-(sv->h)))-((INa_h_gate_beta_h*sv->h)));
    GlobalData_t diff_paF = ((IKr_pa_gate_pa_infinity-(sv->paF))/IKr_pa_gate_tau_paF);
    GlobalData_t diff_paS = ((IKr_pa_gate_pa_infinity-(sv->paS))/IKr_pa_gate_tau_paS);
    GlobalData_t diff_piy = ((IKr_pi_gate_pi_infinity-(sv->piy))/IKr_pi_gate_tau_pi);
    GlobalData_t diff_q = ((Ito_q_gate_q_infinity-(sv->q))/Ito_q_gate_tau_q);
    GlobalData_t diff_r = ((Ito_r_gate_r_infinity-(sv->r))/Ito_r_gate_tau_r);
    GlobalData_t j_up = ((p->Iso_cas>-0.1) ? (p->K_j_up*(((0.9*P_up)*F_PLBp)/(1.+(K_up/sv->Cai)))) : (P_up/(1.+(K_up/sv->Cai))));
    GlobalData_t kiSRCa = (kiCa*kCaSR);
    GlobalData_t koSRCa = (koCa/kCaSR);
    GlobalData_t ICaL_dL_gate_tau_dL = (0.001/(ICaL_dL_gate_alpha_dL+ICaL_dL_gate_beta_dL));
    GlobalData_t IKs_n_gate_tau_n = (1./(IKs_n_gate_alpha_n+IKs_n_gate_beta_n));
    GlobalData_t diff_Ca_nsr = (j_up-(((j_tr*Vjsr)/Vnsr)));
    GlobalData_t diff_Cai = (((((j_Ca_dif*Vsub)-((j_up*Vnsr)))/Vi)-((((CM_tot*delta_fCMi)+(TC_tot*delta_fTC))+(TMC_tot*delta_fTMC))))-((((kfBAPTA*sv->Cai)*(BAPTA-(sv->fBAPTA)))-((kbBAPTA*sv->fBAPTA)))));
    GlobalData_t diff_I = ((((kiSRCa*sv->Ca_sub)*sv->O)-((kim*sv->I)))-(((kom*sv->I)-(((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->RI)))));
    GlobalData_t diff_O = ((((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->R_Ca_SR_release)-((kom*sv->O)))-((((kiSRCa*sv->Ca_sub)*sv->O)-((kim*sv->I)))));
    GlobalData_t diff_RI = (((kom*sv->I)-(((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->RI)))-(((kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_Ca_SR_release)))));
    GlobalData_t diff_R_Ca_SR_release = (((kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_Ca_SR_release)))-((((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->R_Ca_SR_release)-((kom*sv->O)))));
    GlobalData_t diff_a = ((IKACh_a_gate_a_infinity-(sv->a))/IKACh_a_gate_tau_a);
    GlobalData_t diff_fCa = ((ICaL_fCa_gate_fCa_infinity-(sv->fCa))/ICaL_fCa_gate_tau_fCa);
    GlobalData_t diff_m = ((INa_m_gate_alpha_m*(1.00000-(sv->m)))-((INa_m_gate_beta_m*sv->m)));
    GlobalData_t diff_y = ((If_y_gate_y_infinity-(sv->y))/If_y_gate_tau_y);
    GlobalData_t k_l = (Fkl/KCa);
    GlobalData_t diff_A = ((((Fkl*sv->Cai)*(((1.-(sv->A))-(sv->TT))-(sv->U)))-((sv->A*(Ff+k_l))))+(sv->TT*Fg0));
    GlobalData_t diff_TT = (((Ff*sv->A)-((sv->TT*(Fg0+k_l))))+((Fkl*sv->Cai)*sv->U));
    GlobalData_t diff_U = ((k_l*sv->TT)-(((Fg0+(Fkl*sv->Cai))*sv->U)));
    GlobalData_t diff_dL = ((ICaL_dL_gate_dL_infinity-(sv->dL))/ICaL_dL_gate_tau_dL);
    GlobalData_t diff_n = ((IKs_n_gate_n_infinity-(sv->n))/IKs_n_gate_tau_n);
    GlobalData_t A_new = sv->A+diff_A*dt;
    GlobalData_t Ca_jsr_new = sv->Ca_jsr+diff_Ca_jsr*dt;
    GlobalData_t Ca_nsr_new = sv->Ca_nsr+diff_Ca_nsr*dt;
    GlobalData_t Ca_sub_new = sv->Ca_sub+diff_Ca_sub*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Cam_new = sv->Cam+diff_Cam*dt;
    GlobalData_t I_new = sv->I+diff_I*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t PLBp_new = sv->PLBp+diff_PLBp*dt;
    GlobalData_t RI_new = sv->RI+diff_RI*dt;
    GlobalData_t R_Ca_SR_release_new = sv->R_Ca_SR_release+diff_R_Ca_SR_release*dt;
    GlobalData_t TT_new = sv->TT+diff_TT*dt;
    GlobalData_t U_new = sv->U+diff_U*dt;
    GlobalData_t a_new = sv->a+diff_a*dt;
    GlobalData_t cAMP_new = sv->cAMP+diff_cAMP*dt;
    GlobalData_t dL_new = sv->dL+diff_dL*dt;
    GlobalData_t dT_new = sv->dT+diff_dT*dt;
    GlobalData_t fBAPTA_new = sv->fBAPTA+diff_fBAPTA*dt;
    GlobalData_t fBAPTA_sub_new = sv->fBAPTA_sub+diff_fBAPTA_sub*dt;
    GlobalData_t fCMi_new = sv->fCMi+diff_fCMi*dt;
    GlobalData_t fCMs_new = sv->fCMs+diff_fCMs*dt;
    GlobalData_t fCQ_new = sv->fCQ+diff_fCQ*dt;
    GlobalData_t fCa_new = sv->fCa+diff_fCa*dt;
    GlobalData_t fL_new = sv->fL+diff_fL*dt;
    GlobalData_t fT_new = sv->fT+diff_fT*dt;
    GlobalData_t fTC_new = sv->fTC+diff_fTC*dt;
    GlobalData_t fTMC_new = sv->fTMC+diff_fTMC*dt;
    GlobalData_t fTMM_new = sv->fTMM+diff_fTMM*dt;
    GlobalData_t h_new = sv->h+diff_h*dt;
    GlobalData_t m_new = sv->m+diff_m*dt;
    GlobalData_t n_new = sv->n+diff_n*dt;
    GlobalData_t paF_new = sv->paF+diff_paF*dt;
    GlobalData_t paS_new = sv->paS+diff_paS*dt;
    GlobalData_t piy_new = sv->piy+diff_piy*dt;
    GlobalData_t q_new = sv->q+diff_q*dt;
    GlobalData_t r_new = sv->r+diff_r*dt;
    GlobalData_t y_new = sv->y+diff_y*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->A = A_new;
    sv->Ca_jsr = Ca_jsr_new;
    sv->Ca_nsr = Ca_nsr_new;
    sv->Ca_sub = Ca_sub_new;
    sv->Cai = Cai_new;
    sv->Cam = Cam_new;
    sv->I = I_new;
    Iion = Iion;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->PLBp = PLBp_new;
    sv->RI = RI_new;
    sv->R_Ca_SR_release = R_Ca_SR_release_new;
    sv->TT = TT_new;
    sv->U = U_new;
    sv->a = a_new;
    sv->cAMP = cAMP_new;
    sv->dL = dL_new;
    sv->dT = dT_new;
    sv->fBAPTA = fBAPTA_new;
    sv->fBAPTA_sub = fBAPTA_sub_new;
    sv->fCMi = fCMi_new;
    sv->fCMs = fCMs_new;
    sv->fCQ = fCQ_new;
    sv->fCa = fCa_new;
    sv->fL = fL_new;
    sv->fT = fT_new;
    sv->fTC = fTC_new;
    sv->fTMC = fTMC_new;
    sv->fTMM = fTMM_new;
    sv->h = h_new;
    sv->m = m_new;
    sv->n = n_new;
    sv->paF = paF_new;
    sv->paS = paS_new;
    sv->piy = piy_new;
    sv->q = q_new;
    sv->r = r_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    Iion *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // SEVERI_CPU_GENERATED

bool SeveriIonType::has_trace() const {
    return true;
}

void SeveriIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Severi_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->A\n"
        "ATPi\n"
        "sv->Ca_jsr\n"
        "sv->Ca_nsr\n"
        "sv->Ca_sub\n"
        "sv->Cai\n"
        "sv->Cam\n"
        "F_PLBp\n"
        "sv->I\n"
        "ICaL\n"
        "ICaT\n"
        "IKACh\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "If\n"
        "If_y_gate_y_infinity\n"
        "Iion\n"
        "Iso_inc_cas_ICaL\n"
        "Iso_inc_cas_IKs\n"
        "Iso_inc_cas_INaK\n"
        "Iso_shift_cas_ICaL_dL_gate\n"
        "Iso_shift_cas_IKs_n_gate\n"
        "Iso_shift_cas_If_y_gate\n"
        "Iso_slope_cas_ICaL\n"
        "Ito\n"
        "Itot\n"
        "sv->Nai\n"
        "sv->O\n"
        "PKA\n"
        "sv->PLBp\n"
        "sv->RI\n"
        "sv->R_Ca_SR_release\n"
        "sv->TT\n"
        "sv->U\n"
        "V\n"
        "sv->a\n"
        "sv->cAMP\n"
        "sv->dL\n"
        "sv->dT\n"
        "di\n"
        "diff_Ca_jsr\n"
        "diff_Ca_nsr\n"
        "diff_Ca_sub\n"
        "diff_Cai\n"
        "diff_Cam\n"
        "diff_I\n"
        "diff_Nai\n"
        "diff_O\n"
        "diff_PLBp\n"
        "diff_RI\n"
        "diff_R_Ca_SR_release\n"
        "diff_cAMP\n"
        "diff_fBAPTA\n"
        "diff_fBAPTA_sub\n"
        "diff_fCMi\n"
        "diff_fCMs\n"
        "diff_fCQ\n"
        "diff_fTC\n"
        "diff_fTMC\n"
        "diff_fTMM\n"
        "diff_paF\n"
        "diff_paS\n"
        "diff_piy\n"
        "sv->fBAPTA\n"
        "sv->fBAPTA_sub\n"
        "sv->fCMi\n"
        "sv->fCMs\n"
        "sv->fCQ\n"
        "sv->fCa\n"
        "sv->fL\n"
        "sv->fT\n"
        "sv->fTC\n"
        "sv->fTMC\n"
        "sv->fTMM\n"
        "sv->h\n"
        "j_Ca_dif\n"
        "j_NaCam\n"
        "j_SRCarel\n"
        "j_uni\n"
        "j_up\n"
        "k1\n"
        "k12\n"
        "k14\n"
        "k2\n"
        "k21\n"
        "k23\n"
        "k3\n"
        "k32\n"
        "k34\n"
        "k4\n"
        "k41\n"
        "k43\n"
        "k5\n"
        "kCaSR\n"
        "kcch\n"
        "kiSRCa\n"
        "kiso\n"
        "koCa\n"
        "koSRCa\n"
        "sv->m\n"
        "sv->n\n"
        "sv->paF\n"
        "sv->paS\n"
        "sv->piy\n"
        "sv->q\n"
        "sv->r\n"
        "var_do\n"
        "x1\n"
        "x2\n"
        "x3\n"
        "x4\n"
        "sv->y\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Severi_Params *p  = imp.params();

  Severi_state *sv_base = (Severi_state *)imp.sv_tab().data();

  Severi_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+0.00009));
  GlobalData_t ACh_shift = ((p->ACh>0.) ? (-1.-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+0.00122423)))) : 0.);
  GlobalData_t IKACh_a_gate_alpha_a = ((((3.5988-(0.025641))/(1.+(0.0000012155/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.);
  GlobalData_t Iso_increase = ((p->Iso_1_uM>0.) ? 1.2 : 1.);
  GlobalData_t Iso_increase_ICaL = ((p->Iso_1_uM>0.) ? 1.23 : 1.);
  GlobalData_t Iso_shift = ((p->Iso_1_uM>0.) ? 7.5 : 0.);
  GlobalData_t Iso_shift_ICaL_dL_gate = ((p->Iso_1_uM>0.) ? -8. : 0.);
  GlobalData_t Iso_shift_IKs_n_gate = ((p->Iso_1_uM>0.) ? -14. : 0.);
  GlobalData_t Iso_slope = ((p->Iso_1_uM>0.) ? 0.69 : 1.);
  GlobalData_t b_up = ((p->Iso_1_uM>0.) ? -0.25 : ((p->ACh>0.) ? (((0.7*1e-6)*p->ACh)/(0.00009+(1e-6*p->ACh))) : 0.));
  GlobalData_t kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  GlobalData_t kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  GlobalData_t P_up = (P_up_basal*(1.-(b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  Iion *= 1e3;
  
  
  GlobalData_t ATPi = ((ATPi_max*(((kATP*(pow(((sv->cAMP*100.)/cAMPb),nATP)))/(kATP05+(pow(((sv->cAMP*100.)/cAMPb),nATP))))-(KATP_min)))/100.);
  GlobalData_t BAPTA = (((BAPTA_10_mM>0.)&&(t>T_Ca_dynamics)) ? 10. : 0.);
  GlobalData_t F_PLBp = ((sv->PLBp>0.23) ? ((3.3931*(pow(sv->PLBp,4.0695)))/((pow(0.2805,4.0695))+(pow(sv->PLBp,4.0695)))) : ((1.698*(pow(sv->PLBp,13.5842)))/((pow(0.2240,13.5842))+(pow(sv->PLBp,13.5842)))));
  GlobalData_t ICaT = ((((((2.*p->P_CaT)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dT)*sv->fT);
  GlobalData_t ICs_on_Icontrol = ((p->Cs_5_mM>=1.) ? ((10.6015/5.)/((10.6015/5.)+(exp(((-0.71*V)/25.))))) : 1.);
  GlobalData_t IKACh = ((p->ACh>0.) ? (((GKACh_max*(V-(E_K)))*(1.+(exp(((V+20.)/20.)))))*sv->a) : 0.);
  GlobalData_t IKr = (((p->GKr_max*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
  GlobalData_t IKr_pa_gate_pa_infinity = (1./(1.+(exp(((-(V+14.8))/8.5)))));
  GlobalData_t IKr_pa_gate_tau_paF = ((1./((30.*(exp((V/10.))))+(exp(((-V)/12.)))))*1000.);
  GlobalData_t IKr_pa_gate_tau_paS = ((0.846554/((4.2*(exp((V/17.))))+(0.15*(exp(((-V)/21.6))))))*1000.);
  GlobalData_t IKr_pi_gate_pi_infinity = (1./(1.+(exp(((V+28.6)/17.1)))));
  GlobalData_t IKr_pi_gate_tau_pi = ((1./((100.*(exp(((-V)/54.645))))+(656.*(exp((V/106.157))))))*1000.);
  GlobalData_t Iso_shift_cas_If_y_gate = ((p->Iso_cas>-0.1) ? (((p->K_Iso_shift_cas_If_y_gate*Kif)*((pow(sv->cAMP,nif))/((pow(K_05if,nif))+(pow(sv->cAMP,nif)))))-(18.76)) : 0.);
  GlobalData_t Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
  GlobalData_t Nai_BAPTA = ((BAPTA_10_mM>0.) ? 7.5 : sv->Nai);
  GlobalData_t PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
  GlobalData_t delta_fCMi = (((kf_CM*sv->Cai)*(1.-(sv->fCMi)))-((kb_CM*sv->fCMi)));
  GlobalData_t delta_fCMs = (((kf_CM*sv->Ca_sub)*(1.-(sv->fCMs)))-((kb_CM*sv->fCMs)));
  GlobalData_t delta_fCQ = (((kf_CQ*sv->Ca_jsr)*(1.-(sv->fCQ)))-((kb_CQ*sv->fCQ)));
  GlobalData_t delta_fTC = (((kf_TC*sv->Cai)*(1.-(sv->fTC)))-((kb_TC*sv->fTC)));
  GlobalData_t delta_fTMC = (((kf_TMC*sv->Cai)*(1.-((sv->fTMC+sv->fTMM))))-((kb_TMC*sv->fTMC)));
  GlobalData_t delta_fTMM = (((kf_TMM*Mgi)*(1.-((sv->fTMC+sv->fTMM))))-((kb_TMM*sv->fTMM)));
  GlobalData_t j_Ca_dif = ((sv->Ca_sub-(sv->Cai))/tau_dif_Ca);
  GlobalData_t j_NaCam = (Q_mo*(sv->Cam/(K_Cam+sv->Cam)));
  GlobalData_t j_SRCarel = (p->K_j_SRCarel*((ks*sv->O)*(sv->Ca_jsr-(sv->Ca_sub))));
  GlobalData_t j_tr = ((sv->Ca_nsr-(sv->Ca_jsr))/tau_tr);
  GlobalData_t j_uni = (((((P_Ca*2.)*phi_m)/RTONF)*(((alpha_m*sv->Cam)*(exp(((-2.*phi_m)/RTONF))))-((alpha_e*sv->Cai))))/((exp(((-2.*phi_m)/RTONF)))-(1.)));
  GlobalData_t k1 = (p->K_k1*(K_ACI+(K_AC/(1.+(exp(((K_Ca-((kb_CM*(sv->fCMi/(kf_CM*(1.-(sv->fCMi)))))))/K_ACCa)))))));
  GlobalData_t k2 = ((1.1*237.9851)*((pow((sv->cAMP*600.),5.101))/((pow(20.1077,6.101))+(pow((sv->cAMP*600.),6.101)))));
  GlobalData_t k3 = (kPKA*((pow(sv->cAMP,(nPKA-(1.))))/((pow(kPKA_cAMP,nPKA))+(pow(sv->cAMP,nPKA)))));
  GlobalData_t k32 = (exp(((Qn*V)/(2.*RTONF))));
  GlobalData_t k41 = (exp((((-Qn)*V)/(2.*RTONF))));
  GlobalData_t k5 = (kPP1*((PP1*sv->PLBp)/(kPP1_PLB+sv->PLBp)));
  GlobalData_t kCaSR = (MaxSR-(((MaxSR-(MinSR))/(1.+(pow((EC50_SR/sv->Ca_jsr),HSR))))));
  GlobalData_t var_do = ((1.+((p->Cao/Kco)*(1.+(exp(((Qco*V)/RTONF))))))+((Nao/K1no)*(1.+((Nao/K2no)*(1.+(Nao/K3no))))));
  GlobalData_t E_Na = (RTONF*(log((Nao/Nai_BAPTA))));
  GlobalData_t E_mh = (RTONF*(log(((Nao+(0.12*Ko))/(Nai_BAPTA+(0.12*Ki))))));
  GlobalData_t IfK = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_K_max)*(V-(E_K)))*ICs_on_Icontrol);
  GlobalData_t If_y_gate_y_infinity = (1./(1.+(exp((((((V-(Vif05))-(ACh_shift))-(Iso_shift))-(Iso_shift_cas_If_y_gate))/9.)))));
  GlobalData_t Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_ICaL*(-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
  GlobalData_t Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_IKs*(-0.2152+((K_IKs1*(pow(PKA,10.0808)))/((pow(K_05IKs1,10.0808))+(pow(PKA,10.0808)))))) : 0.);
  GlobalData_t Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (p->K_Iso_inc_cas_INaK*(-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808)))))) : 0.);
  GlobalData_t Iso_shift_cas_ICaL_dL_gate = ((p->Iso_cas>-0.1) ? (-((((p->K_Iso_shift_cas_ICaL_dL_gate*K_ICaL2)*(pow(PKA,9.281)))/((pow(K_05ICaL2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
  GlobalData_t Iso_shift_cas_IKs_n_gate = ((p->Iso_cas>-0.1) ? (-((((p->K_Iso_shift_cas_IKs_n_gate*K_IKs2)*(pow(PKA,9.281)))/((pow(K_05IKs2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
  GlobalData_t Iso_slope_cas_ICaL = ((p->Iso_cas>-0.1) ? (p->K_Iso_slope_cas_ICaL*((-1.199*PKA)+1.8526)) : 1.);
  GlobalData_t di = ((1.+((sv->Ca_sub/Kci)*((1.+(exp((((-Qci)*V)/RTONF))))+(Nai_BAPTA/Kcni))))+((Nai_BAPTA/K1ni)*(1.+((Nai_BAPTA/K2ni)*(1.+(Nai_BAPTA/K3ni))))));
  GlobalData_t diff_Ca_jsr = (j_tr-((j_SRCarel+(CQ_tot*delta_fCQ))));
  GlobalData_t diff_Cam = ((Vmyto/Vi)*(j_uni-(j_NaCam)));
  GlobalData_t diff_cAMP = ((((((kiso-(kcch))*ATPi)+(k1*ATPi))-((k2*sv->cAMP)))-((k3*sv->cAMP)))/60000.);
  GlobalData_t diff_fBAPTA = (((kfBAPTA*sv->Cai)*(BAPTA-(sv->fBAPTA)))-((kbBAPTA*sv->fBAPTA)));
  GlobalData_t diff_fBAPTA_sub = (((kfBAPTA*sv->Ca_sub)*(BAPTA-(sv->fBAPTA_sub)))-((kbBAPTA*sv->fBAPTA_sub)));
  GlobalData_t diff_fCMi = delta_fCMi;
  GlobalData_t diff_fCMs = delta_fCMs;
  GlobalData_t diff_fCQ = delta_fCQ;
  GlobalData_t diff_fTC = delta_fTC;
  GlobalData_t diff_fTMC = delta_fTMC;
  GlobalData_t diff_fTMM = delta_fTMM;
  GlobalData_t diff_paF = ((IKr_pa_gate_pa_infinity-(sv->paF))/IKr_pa_gate_tau_paF);
  GlobalData_t diff_paS = ((IKr_pa_gate_pa_infinity-(sv->paS))/IKr_pa_gate_tau_paS);
  GlobalData_t diff_piy = ((IKr_pi_gate_pi_infinity-(sv->piy))/IKr_pi_gate_tau_pi);
  GlobalData_t j_up = ((p->Iso_cas>-0.1) ? (p->K_j_up*(((0.9*P_up)*F_PLBp)/(1.+(K_up/sv->Cai)))) : (P_up/(1.+(K_up/sv->Cai))));
  GlobalData_t k21 = (((p->Cao/Kco)*(exp(((Qco*V)/RTONF))))/var_do);
  GlobalData_t k23 = ((((((Nao/K1no)*Nao)/K2no)*(1.+(Nao/K3no)))*(exp((((-Qn)*V)/(2.*RTONF)))))/var_do);
  GlobalData_t k4 = ((kPLBp*(pow(PKA,nPLB)))/((pow(kPKA_PLB,nPLB))+(pow(PKA,nPLB))));
  GlobalData_t k43 = (Nai_BAPTA/(K3ni+Nai_BAPTA));
  GlobalData_t kiSRCa = (kiCa*kCaSR);
  GlobalData_t koCa = ((p->Iso_cas>-0.1) ? (p->K_koCa*(koCa_max*((RyR_min-(((RyR_max*(pow(PKA,nRyR)))/((pow(K_05RyR,nRyR))+(pow(PKA,nRyR))))))+1.))) : 10.);
  GlobalData_t IKs = ((((1.+Iso_inc_cas_IKs)*p->GKs_max)*(V-(E_Ks)))*(sv->n*sv->n));
  GlobalData_t INa = (((p->GNa_max*((sv->m*sv->m)*sv->m))*sv->h)*(V-(E_mh)));
  GlobalData_t INaK = ((((((1.+Iso_inc_cas_INaK)*Iso_increase)*p->INaK_max)*(1./(1.+(pow((Km_Kp/Ko),1.2)))))*(1./(1.+(pow((Km_Nap/Nai_BAPTA),1.3)))))*(1./(1.+(exp(((-((V-(E_Na))+110.))/20.))))));
  GlobalData_t IfNa = ((((((sv->y*sv->y)*Ko)/(Ko+Km_f))*p->Gf_Na_max)*(V-(E_Na)))*ICs_on_Icontrol);
  GlobalData_t IsiCa = ((((((((((2.*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp((((-1.*V)*2.)/RTONF)))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t IsiK = ((((((((((0.000365*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Ki-((Ko*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t IsiNa = ((((((((((0.0000185*Iso_increase_ICaL)*(1.-(ACh_block)))*(1.+Iso_inc_cas_ICaL))*p->P_CaL)*V)/(RTONF*(1.-((exp(((-1.*V)/RTONF)))))))*(Nai_BAPTA-((Nao*(exp(((-1.*V)/RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t diff_Ca_nsr = (j_up-(((j_tr*Vjsr)/Vnsr)));
  GlobalData_t diff_Cai = (((((j_Ca_dif*Vsub)-((j_up*Vnsr)))/Vi)-((((CM_tot*delta_fCMi)+(TC_tot*delta_fTC))+(TMC_tot*delta_fTMC))))-((((kfBAPTA*sv->Cai)*(BAPTA-(sv->fBAPTA)))-((kbBAPTA*sv->fBAPTA)))));
  GlobalData_t diff_PLBp = ((k4-(k5))/60000.);
  GlobalData_t k12 = (((sv->Ca_sub/Kci)*(exp((((-Qci)*V)/RTONF))))/di);
  GlobalData_t k14 = ((((((Nai_BAPTA/K1ni)*Nai_BAPTA)/K2ni)*(1.+(Nai_BAPTA/K3ni)))*(exp(((Qn*V)/(2.*RTONF)))))/di);
  GlobalData_t koSRCa = (koCa/kCaSR);
  GlobalData_t x1 = (((k41*k34)*(k23+k21))+((k21*k32)*(k43+k41)));
  GlobalData_t ICaL = ((IsiCa+IsiK)+IsiNa);
  GlobalData_t If = (IfNa+IfK);
  GlobalData_t diff_I = ((((kiSRCa*sv->Ca_sub)*sv->O)-((kim*sv->I)))-(((kom*sv->I)-(((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->RI)))));
  GlobalData_t diff_O = ((((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->R_Ca_SR_release)-((kom*sv->O)))-((((kiSRCa*sv->Ca_sub)*sv->O)-((kim*sv->I)))));
  GlobalData_t diff_RI = (((kom*sv->I)-(((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->RI)))-(((kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_Ca_SR_release)))));
  GlobalData_t diff_R_Ca_SR_release = (((kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_Ca_SR_release)))-((((koSRCa*(sv->Ca_sub*sv->Ca_sub))*sv->R_Ca_SR_release)-((kom*sv->O)))));
  GlobalData_t x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(k34+k32)));
  GlobalData_t x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
  GlobalData_t x4 = (((k23*k34)*(k14+k12))+((k14*k21)*(k34+k32)));
  GlobalData_t INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
  GlobalData_t Itot = (((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh);
  GlobalData_t diff_Ca_sub = ((((j_SRCarel*Vjsr)/Vsub)-((((((IsiCa+ICaT)-((2.*INaCa)))/(((2.*(F*1000.))*Vsub)*0.000000001))+j_Ca_dif)+(CM_tot*delta_fCMs))))-((((kfBAPTA*sv->Ca_sub)*(BAPTA-(sv->fBAPTA_sub)))-((kbBAPTA*sv->fBAPTA_sub)))));
  GlobalData_t diff_Nai = ((-1.*((((INa+IfNa)+IsiNa)+(3.*INaK))+(3.*INaCa)))/(((Vi*0.000000001)+(Vsub*0.000000001))*(F*1000.)));
  Iion = (-((-Itot)/C));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->A);
  fprintf(file, "%4.12f\t", ATPi);
  fprintf(file, "%4.12f\t", sv->Ca_jsr);
  fprintf(file, "%4.12f\t", sv->Ca_nsr);
  fprintf(file, "%4.12f\t", sv->Ca_sub);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->Cam);
  fprintf(file, "%4.12f\t", F_PLBp);
  fprintf(file, "%4.12f\t", sv->I);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaT);
  fprintf(file, "%4.12f\t", IKACh);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", If);
  fprintf(file, "%4.12f\t", If_y_gate_y_infinity);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", Iso_inc_cas_ICaL);
  fprintf(file, "%4.12f\t", Iso_inc_cas_IKs);
  fprintf(file, "%4.12f\t", Iso_inc_cas_INaK);
  fprintf(file, "%4.12f\t", Iso_shift_cas_ICaL_dL_gate);
  fprintf(file, "%4.12f\t", Iso_shift_cas_IKs_n_gate);
  fprintf(file, "%4.12f\t", Iso_shift_cas_If_y_gate);
  fprintf(file, "%4.12f\t", Iso_slope_cas_ICaL);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Itot);
  fprintf(file, "%4.12f\t", sv->Nai);
  fprintf(file, "%4.12f\t", sv->O);
  fprintf(file, "%4.12f\t", PKA);
  fprintf(file, "%4.12f\t", sv->PLBp);
  fprintf(file, "%4.12f\t", sv->RI);
  fprintf(file, "%4.12f\t", sv->R_Ca_SR_release);
  fprintf(file, "%4.12f\t", sv->TT);
  fprintf(file, "%4.12f\t", sv->U);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->a);
  fprintf(file, "%4.12f\t", sv->cAMP);
  fprintf(file, "%4.12f\t", sv->dL);
  fprintf(file, "%4.12f\t", sv->dT);
  fprintf(file, "%4.12f\t", di);
  fprintf(file, "%4.12f\t", diff_Ca_jsr);
  fprintf(file, "%4.12f\t", diff_Ca_nsr);
  fprintf(file, "%4.12f\t", diff_Ca_sub);
  fprintf(file, "%4.12f\t", diff_Cai);
  fprintf(file, "%4.12f\t", diff_Cam);
  fprintf(file, "%4.12f\t", diff_I);
  fprintf(file, "%4.12f\t", diff_Nai);
  fprintf(file, "%4.12f\t", diff_O);
  fprintf(file, "%4.12f\t", diff_PLBp);
  fprintf(file, "%4.12f\t", diff_RI);
  fprintf(file, "%4.12f\t", diff_R_Ca_SR_release);
  fprintf(file, "%4.12f\t", diff_cAMP);
  fprintf(file, "%4.12f\t", diff_fBAPTA);
  fprintf(file, "%4.12f\t", diff_fBAPTA_sub);
  fprintf(file, "%4.12f\t", diff_fCMi);
  fprintf(file, "%4.12f\t", diff_fCMs);
  fprintf(file, "%4.12f\t", diff_fCQ);
  fprintf(file, "%4.12f\t", diff_fTC);
  fprintf(file, "%4.12f\t", diff_fTMC);
  fprintf(file, "%4.12f\t", diff_fTMM);
  fprintf(file, "%4.12f\t", diff_paF);
  fprintf(file, "%4.12f\t", diff_paS);
  fprintf(file, "%4.12f\t", diff_piy);
  fprintf(file, "%4.12f\t", sv->fBAPTA);
  fprintf(file, "%4.12f\t", sv->fBAPTA_sub);
  fprintf(file, "%4.12f\t", sv->fCMi);
  fprintf(file, "%4.12f\t", sv->fCMs);
  fprintf(file, "%4.12f\t", sv->fCQ);
  fprintf(file, "%4.12f\t", sv->fCa);
  fprintf(file, "%4.12f\t", sv->fL);
  fprintf(file, "%4.12f\t", sv->fT);
  fprintf(file, "%4.12f\t", sv->fTC);
  fprintf(file, "%4.12f\t", sv->fTMC);
  fprintf(file, "%4.12f\t", sv->fTMM);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", j_Ca_dif);
  fprintf(file, "%4.12f\t", j_NaCam);
  fprintf(file, "%4.12f\t", j_SRCarel);
  fprintf(file, "%4.12f\t", j_uni);
  fprintf(file, "%4.12f\t", j_up);
  fprintf(file, "%4.12f\t", k1);
  fprintf(file, "%4.12f\t", k12);
  fprintf(file, "%4.12f\t", k14);
  fprintf(file, "%4.12f\t", k2);
  fprintf(file, "%4.12f\t", k21);
  fprintf(file, "%4.12f\t", k23);
  fprintf(file, "%4.12f\t", k3);
  fprintf(file, "%4.12f\t", k32);
  fprintf(file, "%4.12f\t", k34);
  fprintf(file, "%4.12f\t", k4);
  fprintf(file, "%4.12f\t", k41);
  fprintf(file, "%4.12f\t", k43);
  fprintf(file, "%4.12f\t", k5);
  fprintf(file, "%4.12f\t", kCaSR);
  fprintf(file, "%4.12f\t", kcch);
  fprintf(file, "%4.12f\t", kiSRCa);
  fprintf(file, "%4.12f\t", kiso);
  fprintf(file, "%4.12f\t", koCa);
  fprintf(file, "%4.12f\t", koSRCa);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->n);
  fprintf(file, "%4.12f\t", sv->paF);
  fprintf(file, "%4.12f\t", sv->paS);
  fprintf(file, "%4.12f\t", sv->piy);
  fprintf(file, "%4.12f\t", sv->q);
  fprintf(file, "%4.12f\t", sv->r);
  fprintf(file, "%4.12f\t", var_do);
  fprintf(file, "%4.12f\t", x1);
  fprintf(file, "%4.12f\t", x2);
  fprintf(file, "%4.12f\t", x3);
  fprintf(file, "%4.12f\t", x4);
  fprintf(file, "%4.12f\t", sv->y);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  Iion *= 1e-3;
  
  

}
IonIfBase* SeveriIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void SeveriIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        