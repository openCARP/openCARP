// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: K. H. W. J. ten Tusscher and A. V. Panfilov
*  Year: 2006
*  Title: Alternans and spiral breakup in a human ventricular tissue model
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 291(3), 1088-1100
*  DOI: 10.1152/ajpheart.00109.2006
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __TENTUSSCHERPANFILOV_H__
#define __TENTUSSCHERPANFILOV_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(TENTUSSCHERPANFILOV_CPU_GENERATED)    || defined(TENTUSSCHERPANFILOV_MLIR_CPU_GENERATED)    || defined(TENTUSSCHERPANFILOV_MLIR_ROCM_GENERATED)    || defined(TENTUSSCHERPANFILOV_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define TENTUSSCHERPANFILOV_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define TENTUSSCHERPANFILOV_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define TENTUSSCHERPANFILOV_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define TENTUSSCHERPANFILOV_CPU_GENERATED
#endif

namespace limpet {

#define tenTusscherPanfilov_REQDAT Vm_DATA_FLAG
#define tenTusscherPanfilov_MODDAT Iion_DATA_FLAG

struct tenTusscherPanfilov_Params {
    GlobalData_t Bufc;
    GlobalData_t Bufsr;
    GlobalData_t Bufss;
    GlobalData_t CAPACITANCE;
    GlobalData_t Cao;
    GlobalData_t D_CaL_off;
    GlobalData_t Fconst;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GbCa;
    GlobalData_t GbNa;
    GlobalData_t GpCa;
    GlobalData_t GpK;
    GlobalData_t Gto;
    GlobalData_t Kbufc;
    GlobalData_t Kbufsr;
    GlobalData_t Kbufss;
    GlobalData_t KmCa;
    GlobalData_t KmK;
    GlobalData_t KmNa;
    GlobalData_t KmNai;
    GlobalData_t Ko;
    GlobalData_t KpCa;
    GlobalData_t Kup;
    GlobalData_t Nao;
    GlobalData_t Rconst;
    GlobalData_t T;
    GlobalData_t Vc;
    GlobalData_t Vleak;
    GlobalData_t Vmaxup;
    GlobalData_t Vrel;
    GlobalData_t Vsr;
    GlobalData_t Vss;
    GlobalData_t cell_type;
    GlobalData_t knaca;
    GlobalData_t knak;
    GlobalData_t ksat;
    GlobalData_t n;
    GlobalData_t pKNa;
    GlobalData_t scl_tau_f;
    GlobalData_t vHalfXs;
    GlobalData_t xr2_off;
    char* flags;

};
static const char* tenTusscherPanfilov_flags = "EPI|MCELL|ENDO";


struct tenTusscherPanfilov_state {
    GlobalData_t CaSR;
    GlobalData_t CaSS;
    GlobalData_t Cai;
    Gatetype D;
    Gatetype F;
    Gatetype F2;
    Gatetype FCaSS;
    GlobalData_t GCaL;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t Gto;
    Gatetype H;
    Gatetype J;
    GlobalData_t Ki;
    Gatetype M;
    GlobalData_t Nai;
    Gatetype R;
    GlobalData_t R_;
    Gatetype S;
    Gatetype Xr1;
    Gatetype Xr2;
    Gatetype Xs;

};

class tenTusscherPanfilovIonType : public IonType {
public:
    using IonIfDerived = IonIf<tenTusscherPanfilovIonType>;
    using params_type = tenTusscherPanfilov_Params;
    using state_type = tenTusscherPanfilov_state;

    tenTusscherPanfilovIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_tenTusscherPanfilov(int, int, IonIfBase&, GlobalData_t**);
#ifdef TENTUSSCHERPANFILOV_CPU_GENERATED
void compute_tenTusscherPanfilov_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef TENTUSSCHERPANFILOV_MLIR_CPU_GENERATED
void compute_tenTusscherPanfilov_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef TENTUSSCHERPANFILOV_MLIR_ROCM_GENERATED
void compute_tenTusscherPanfilov_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef TENTUSSCHERPANFILOV_MLIR_CUDA_GENERATED
void compute_tenTusscherPanfilov_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
