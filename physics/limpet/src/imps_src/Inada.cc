// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Inada S, Shibata Md PhD N, Iwata PhD M, Haraguchi PhD R, Ashihara Md PhD T, Ikeda Md PhD T, Mitsui PhD K, Dobrzynski PhD H, Boyett PhD MR, Nakazawa PhD K.
*  Year: 2017
*  Title: Simulation of ventricular rate control during atrial fibrillation using ionic channel blockers
*  Journal: J Arrhythm. 33(4):302-309
*  DOI: 10.1016/j.joa.2016.12.002
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Inada.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

InadaIonType::InadaIonType(bool plugin) : IonType(std::move(std::string("Inada")), plugin) {}

size_t InadaIonType::params_size() const {
  return sizeof(struct Inada_Params);
}

size_t InadaIonType::dlo_vector_size() const {

  return 1;
}

uint32_t InadaIonType::reqdat() const {
  return Inada_REQDAT;
}

uint32_t InadaIonType::moddat() const {
  return Inada_MODDAT;
}

void InadaIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target InadaIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef INADA_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(INADA_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(INADA_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(INADA_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef INADA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef INADA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef INADA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef INADA_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void InadaIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef INADA_MLIR_CUDA_GENERATED
      compute_Inada_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(INADA_MLIR_ROCM_GENERATED)
      compute_Inada_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(INADA_MLIR_CPU_GENERATED)
      compute_Inada_mlir_cpu(start, end, imp, data);
#   elif defined(INADA_CPU_GENERATED)
      compute_Inada_cpu(start, end, imp, data);
#   else
#     error "Could not generate method InadaIonType::compute."
#   endif
      break;
#   ifdef INADA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Inada_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef INADA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Inada_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef INADA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Inada_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef INADA_CPU_GENERATED
    case Target::CPU:
      compute_Inada_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define AN (GlobalData_t)(0.)
#define Cae (GlobalData_t)(2.0e-3)
#define Cm_spec (GlobalData_t)(1e-6)
#define E_st (GlobalData_t)(-37.40e-3)
#define F (GlobalData_t)(96485.3415)
#define Ke (GlobalData_t)(5.4e-3)
#define Ki (GlobalData_t)(0.14)
#define N (GlobalData_t)(1.)
#define NH (GlobalData_t)(2.)
#define Nae (GlobalData_t)(140.0e-3)
#define Nai (GlobalData_t)(0.008)
#define R (GlobalData_t)(8.314472)
#define T (GlobalData_t)(310.)
#define TINY (GlobalData_t)(1e-6)
#define act_shift_f (GlobalData_t)(0.)
#define iJive (GlobalData_t)(1e6)
#define tJive (GlobalData_t)(1e3)
#define E_K (GlobalData_t)((((R*T)/F)*(log((Ke/Ki)))))
#define E_Na (GlobalData_t)((((R*T)/F)*(log((Nae/Nai)))))
#define FONRT (GlobalData_t)((F/(R*T)))
#define RTONF (GlobalData_t)(((R*T)/F))
#define d_o (GlobalData_t)(((1.0+((Cae/3.663e-3)*(1.0+(exp(0.0)))))+((Nae/1628.0e-3)*(1.0+((Nae/561.4e-3)*(1.0+(Nae/4.663e-3)))))))
#define k34 (GlobalData_t)((Nae/(4.663e-3+Nae)))
#define k43 (GlobalData_t)((Nai/(26.44e-3+Nai)))
#define k21 (GlobalData_t)((((Cae/3.663e-3)*(exp(0.0)))/d_o))



void InadaIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Inada_Params *p = imp.params();

  // Compute the regional constants
  {
    p->cell_type = N;
    if (0) ;
    else if (flag_set(p->flags, "AN")) p->cell_type = AN;
    else if (flag_set(p->flags, "N")) p->cell_type = N;
    else if (flag_set(p->flags, "NH")) p->cell_type = NH;
    p->Cm = ((p->cell_type==AN) ? 40e-12 : ((p->cell_type==N) ? 29e-12 : 40e-12));
    p->E_CaL = ((p->cell_type==AN) ? 62.10e-3 : ((p->cell_type==N) ? 62.0e-3 : 62.10e-3));
    p->E_b = ((p->cell_type==AN) ? -52.5e-3 : ((p->cell_type==N) ? -22.5e-3 : -40.0e-3));
    p->GCaL = ((p->cell_type==AN) ? 18.5e-9 : ((p->cell_type==N) ? 9.0e-9 : 21.0e-9));
    p->GK1 = ((p->cell_type==AN) ? 12.5e-9 : ((p->cell_type==N) ? 0.0 : 15.0e-9));
    p->GKr = ((p->cell_type==AN) ? 1.50e-9 : ((p->cell_type==N) ? 3.50e-9 : 2.0e-9));
    p->Gb = ((p->cell_type==AN) ? 1.8e-9 : ((p->cell_type==N) ? 1.2e-9 : 2.0e-9));
    p->Gf = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 1.0e-9 : 0.0));
    p->Gst = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.100e-9 : 0.0));
    p->Gto = ((p->cell_type==AN) ? 20.0e-9 : ((p->cell_type==N) ? 0.0 : 14.0e-9));
    p->kNaCa = ((p->cell_type==AN) ? (1.1832e-9*5.0) : ((p->cell_type==N) ? (1.1832e-9*1.8125) : (1.1832e-9*5.0)));
    p->p_max = ((p->cell_type==AN) ? (9.84e-11*0.25) : ((p->cell_type==N) ? (7.134e-11*2.0) : (9.84e-11*2.0)));
    p->p_rel = ((p->cell_type==AN) ? 1805.6 : ((p->cell_type==N) ? 1500.0 : 1805.6));
  }
  // Compute the regional initialization
  {
    p->GNa = ((p->cell_type==AN) ? 0.5e-12 : ((p->cell_type==N) ? 0.0 : 0.5e-12));
  }

}


// Define the parameters for the lookup tables
enum Tables {
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum V_TableIndex {
  IK1_idx,
  Ib_idx,
  Ip_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f1_rush_larsen_A_idx,
  f1_rush_larsen_B_idx,
  f2_rush_larsen_A_idx,
  f2_rush_larsen_B_idx,
  h1_rush_larsen_A_idx,
  h1_rush_larsen_B_idx,
  h2_rush_larsen_A_idx,
  h2_rush_larsen_B_idx,
  k23_idx,
  k32_idx,
  k41_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  paf_rush_larsen_A_idx,
  paf_rush_larsen_B_idx,
  pas_rush_larsen_A_idx,
  pas_rush_larsen_B_idx,
  pi_rush_larsen_A_idx,
  pi_rush_larsen_B_idx,
  qa_rush_larsen_A_idx,
  qa_rush_larsen_B_idx,
  qi_rush_larsen_A_idx,
  qi_rush_larsen_B_idx,
  vm_idx,
  x1_idx,
  x_rush_larsen_A_idx,
  x_rush_larsen_B_idx,
  y_rush_larsen_A_idx,
  y_rush_larsen_B_idx,
  yf_rush_larsen_A_idx,
  yf_rush_larsen_B_idx,
  ys_rush_larsen_A_idx,
  ys_rush_larsen_B_idx,
  NROWS_V
};



void InadaIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Inada_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double Cai_init = ((p->cell_type==AN) ? 1.0817e-7 : ((p->cell_type==N) ? 3.6327e-7 : 1.3399e-7));
  double Carel_init = ((p->cell_type==AN) ? 0.00040019 : ((p->cell_type==N) ? 8.1847e-5 : 0.00044752));
  double Casub_init = ((p->cell_type==AN) ? 5.86e-8 : ((p->cell_type==N) ? 2.3000e-7 : 7.1220e-8));
  double Caup_init = ((p->cell_type==AN) ? 0.00095955 : ((p->cell_type==N) ? 0.0011463 : 0.0011631));
  double V_init = ((p->cell_type==AN) ? -70.219 : ((p->cell_type==N) ? -62.132 : -68.668));
  double Volcell = ((((p->Cm-(20.0e-12))/45.0e-12)*(7.147123e-12-(2.19911e-12)))+2.19911e-12);
  double act_shift_CaL = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? -15.0 : 0.0));
  double d_init = ((p->cell_type==AN) ? 3.9525e-5 : ((p->cell_type==N) ? 0.00015340 : 4.9961e-5));
  double f1_init = ((p->cell_type==AN) ? 0.99854 : ((p->cell_type==N) ? 0.68093 : 0.99814));
  double f2_init = ((p->cell_type==AN) ? 0.99111 : ((p->cell_type==N) ? 0.44202 : 0.98513));
  double f_cmi_init = ((p->cell_type==AN) ? 0.043662 : ((p->cell_type==N) ? 0.13393 : 0.053550));
  double f_cms_init = ((p->cell_type==AN) ? 0.024102 : ((p->cell_type==N) ? 0.089149 : 0.029155));
  double f_cq_init = ((p->cell_type==AN) ? 0.32345 : ((p->cell_type==N) ? 0.086939 : 0.34828));
  double f_csl_init = ((p->cell_type==AN) ? 3.0851e-5 : ((p->cell_type==N) ? 4.6743e-5 : 4.4471e-5));
  double f_tc_init = ((p->cell_type==AN) ? 0.021203 : ((p->cell_type==N) ? 0.068561 : 0.026148));
  double f_tmc_init = ((p->cell_type==AN) ? 0.33949 : ((p->cell_type==N) ? 0.61949 : 0.39299));
  double f_tmm_init = ((p->cell_type==AN) ? 0.58343 : ((p->cell_type==N) ? 0.33603 : 0.53615));
  double h1_init = ((p->cell_type==AN) ? 0.72733 : ((p->cell_type==N) ? 0. : 0.64629));
  double h2_init = ((p->cell_type==AN) ? 0.63777 : ((p->cell_type==N) ? 0. : 0.56383));
  double inact_shift_CaL = ((p->cell_type==AN) ? -5.0 : ((p->cell_type==N) ? -5.0 : -5.0));
  double m_init = ((p->cell_type==AN) ? 0.012459 : ((p->cell_type==N) ? 0. : 0.015215));
  double paf_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.60607 : 0.093329));
  double pas_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.12876 : 0.067693));
  double pi_init = ((p->cell_type==AN) ? 0.986780 : ((p->cell_type==N) ? 0.97752 : 0.13435));
  double qa_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.19362 : 0.0));
  double qi_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.48850 : 0.0));
  double slope_factor_act_CaL = ((p->cell_type==AN) ? -6.61 : ((p->cell_type==N) ? -5.0 : -6.61));
  double x_init = ((p->cell_type==AN) ? 0.0087042 : ((p->cell_type==N) ? 0. : 0.0095587));
  double y_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.038225 : 0.0));
  double yf_init = ((p->cell_type==AN) ? 0.8847000 : ((p->cell_type==N) ? 0. : 0.87080));
  double ys_init = ((p->cell_type==AN) ? 0.2026300 : ((p->cell_type==N) ? 0.2026300 : 0.13435));
  double Volrel = (0.0012*Volcell);
  double Volsub = (0.01*Volcell);
  double Volup = (0.0116*Volcell);
  double Voli = ((0.46*Volcell)-(Volsub));
  
  // Create the V lookup table
  LUT* V_tab = &imp.tables()[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -2000, 2000, 0.05, "Inada V", imp.get_target());
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    double a_m = (((fabs((V+44.4)))>TINY) ? ((-0.46*(V+44.4))/(expm1(((V+44.4)/-12.673)))) : (-0.46*-12.673));
    double b_m = (18.4*(exp(((V+44.4)/-12.673))));
    V_row[vm_idx] = (V*0.001);
    double GK1_ = (p->GK1*(0.5+(0.5/(1.0+(exp((((V_row[vm_idx]*1.0e+3)-(-30.0))/5.0)))))));
    V_row[Ib_idx] = (p->Gb*(V_row[vm_idx]-(p->E_b)));
    V_row[Ip_idx] = ((((p->p_max*(pow(((Nai*1.0e+3)/(5.64+(Nai*1.0e+3))),3.0)))*(pow(((Ke*1.0e+3)/(0.621+(Ke*1.0e+3))),2.0)))*1.6)/(1.5+(exp(((-((V_row[vm_idx]*1.0e+3)+60.0))/40.0)))));
    double aa_d = (((((V_row[vm_idx]*1.0e+3)+35.0)>TINY) ? ((-26.12*((V_row[vm_idx]*1.0e+3)+35.0))/((exp((-0.4*((V_row[vm_idx]*1.0e+3)+35.0))))-(1.0))) : (-26.12/-0.4))+(((V_row[vm_idx]*1.0e+3)>TINY) ? (((-78.11*V_row[vm_idx])*1.0e+3)/((exp(((-0.208*V_row[vm_idx])*1.0e+3)))-(1.0))) : (-78.11/-0.208)));
    double aa_h1 = (44.900*(exp((((V_row[vm_idx]*1.0e+3)+66.9)/-5.570))));
    double aa_pi = (92.01*(exp(((-0.0183*V_row[vm_idx])*1.0e+3))));
    double aa_qa = (1.0/((0.15*(exp((((-V_row[vm_idx])*1.0e+3)/11.0))))+(0.20*(exp((((-V_row[vm_idx])*1.0e+3)/700.0))))));
    double aa_qi = (0.1504/((3100.0*(exp(((V_row[vm_idx]*1.0e+3)/13.0))))+(700.0*(exp(((V_row[vm_idx]*1.0e+3)/70.0))))));
    double bb_d = ((((V_row[vm_idx]*1.0e+3)-(5.0))>TINY) ? ((10.52*((V_row[vm_idx]*1.0e+3)-(5.0)))/((exp((0.4*((V_row[vm_idx]*1.0e+3)-(5.0)))))-(1.0))) : (10.52/0.4));
    double bb_h1 = (1491.000/(1.0+(323.300*(exp((((V_row[vm_idx]*1.0e+3)+94.6)/-12.900))))));
    double bb_pi = (603.6*(exp(((0.00942*V_row[vm_idx])*1.0e+3))));
    double bb_qa = (1.0/((16.0*(exp(((V_row[vm_idx]*1.0e+3)/8.0))))+(15.0*(exp(((V_row[vm_idx]*1.0e+3)/50.0))))));
    double bb_qi = ((0.1504/((95.0*(exp((((-V_row[vm_idx])*1.0e+3)/10.0))))+(50.0*(exp((((-V_row[vm_idx])*1.0e+3)/700.0))))))+(0.000229/(1.0+(exp((((-V_row[vm_idx])*1.0e+3)/5.0))))));
    double d_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)-((-3.2+act_shift_CaL)))/slope_factor_act_CaL)))));
    double f1_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)-((-24.0+inact_shift_CaL)))/6.31)))));
    V_row[k23_idx] = (((((Nae/1628.0e-3)*(Nae/561.4e-3))*(1.0+(Nae/4.663e-3)))*(exp((((-0.4315*V_row[vm_idx])*F)/((2.0*R)*T)))))/d_o);
    V_row[k32_idx] = (exp((((0.4315*V_row[vm_idx])*F)/((2.0*R)*T))));
    V_row[k41_idx] = (exp((((-0.4315*V_row[vm_idx])*F)/((2.0*R)*T))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double paf_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+10.22)/-8.50)))));
    double pi_inf = ((1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+4.90)/15.14)))))*(1.0-((0.3*(exp(((-(pow((V_row[vm_idx]*1.0e+3),2.0)))/500.0)))))));
    double qa_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)-(-49.10))/-8.98)))));
    double tau_f1 = ((0.010+(0.1539*(exp(((-(pow(((V_row[vm_idx]*1.0e+3)+40.0),2.0)))/185.67)))))*tJive);
    double tau_f2 = ((0.060+((0.48076*2.25)*(exp(((-(pow(((V_row[vm_idx]*1.0e+3)-(-40.0)),2.0)))/138.04)))))*tJive);
    double tau_h1 = (((0.03000/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+40.00)/6.000)))))+0.00035)*tJive);
    double tau_h2 = (((0.12000/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+60.00)/2.000)))))+0.00295)*tJive);
    double tau_paf = ((1.0/((17.0*(exp(((0.0398*V_row[vm_idx])*1.0e+3))))+(0.211*(exp(((-0.0510*V_row[vm_idx])*1.0e+3))))))*tJive);
    double tau_pas = ((0.33581+(0.90673*(exp(((-(pow(((V_row[vm_idx]*1.0e+3)+10.0),2.0)))/988.05)))))*tJive);
    double tau_x = ((0.596e-3+(3.118e-3/((1.037*(exp((0.09*((V_row[vm_idx]*1.0e+3)+30.61)))))+(0.396*(exp((-0.12*((V_row[vm_idx]*1.0e+3)+23.84))))))))*tJive);
    double tau_y = ((0.250+(2.000*(exp(((-(pow(((V_row[vm_idx]*1.0e+3)-(-70.00)),2.0)))/500.0)))))*tJive);
    double tau_yf = ((12.66e-3+(4.72716/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+154.5)/23.96))))))*tJive);
    double tau_ys = ((0.100+(4.000*(exp(((-(pow(((V_row[vm_idx]*1.0e+3)+65.0),2.0)))/500.0)))))*tJive);
    double x_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)-(7.44))/-16.4)))));
    double y_inf = (1.0/(1.0+(exp(((((V_row[vm_idx]*1.0e+3)-(-83.19))-(act_shift_f))/13.56)))));
    double yf_inf = (1.0/(1.0+(exp((((V_row[vm_idx]*1.0e+3)+33.8)/6.12)))));
    V_row[IK1_idx] = (((GK1_*(pow((Ke/(Ke+0.590e-3)),3.0)))*(V_row[vm_idx]-(-0.0819)))/(1.0+(exp((((1.393*((V_row[vm_idx]-(-0.0819))+0.0036))*F)/(R*T))))));
    V_row[f1_rush_larsen_B_idx] = (exp(((-dt)/tau_f1)));
    double f1_rush_larsen_C = (expm1(((-dt)/tau_f1)));
    double f2_inf = f1_inf;
    V_row[f2_rush_larsen_B_idx] = (exp(((-dt)/tau_f2)));
    double f2_rush_larsen_C = (expm1(((-dt)/tau_f2)));
    double h1_inf = (aa_h1/(aa_h1+bb_h1));
    V_row[h1_rush_larsen_B_idx] = (exp(((-dt)/tau_h1)));
    double h1_rush_larsen_C = (expm1(((-dt)/tau_h1)));
    V_row[h2_rush_larsen_B_idx] = (exp(((-dt)/tau_h2)));
    double h2_rush_larsen_C = (expm1(((-dt)/tau_h2)));
    V_row[paf_rush_larsen_B_idx] = (exp(((-dt)/tau_paf)));
    double paf_rush_larsen_C = (expm1(((-dt)/tau_paf)));
    double pas_inf = paf_inf;
    V_row[pas_rush_larsen_B_idx] = (exp(((-dt)/tau_pas)));
    double pas_rush_larsen_C = (expm1(((-dt)/tau_pas)));
    double qi_inf = (aa_qi/(aa_qi+bb_qi));
    double tau_d = (tJive/(aa_d+bb_d));
    double tau_pi = (tJive/(aa_pi+bb_pi));
    double tau_qa = ((1.0e-3/(aa_qa+bb_qa))*tJive);
    double tau_qi = ((tJive*1.0e-3)/(aa_qi+bb_qi));
    V_row[x1_idx] = (((k34*V_row[k41_idx])*(V_row[k23_idx]+k21))+((k21*V_row[k32_idx])*(k43+V_row[k41_idx])));
    V_row[x_rush_larsen_B_idx] = (exp(((-dt)/tau_x)));
    double x_rush_larsen_C = (expm1(((-dt)/tau_x)));
    V_row[y_rush_larsen_B_idx] = (exp(((-dt)/tau_y)));
    double y_rush_larsen_C = (expm1(((-dt)/tau_y)));
    V_row[yf_rush_larsen_B_idx] = (exp(((-dt)/tau_yf)));
    double yf_rush_larsen_C = (expm1(((-dt)/tau_yf)));
    double ys_inf = yf_inf;
    V_row[ys_rush_larsen_B_idx] = (exp(((-dt)/tau_ys)));
    double ys_rush_larsen_C = (expm1(((-dt)/tau_ys)));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[f1_rush_larsen_A_idx] = ((-f1_inf)*f1_rush_larsen_C);
    V_row[f2_rush_larsen_A_idx] = ((-f2_inf)*f2_rush_larsen_C);
    V_row[h1_rush_larsen_A_idx] = ((-h1_inf)*h1_rush_larsen_C);
    double h2_inf = h1_inf;
    V_row[paf_rush_larsen_A_idx] = ((-paf_inf)*paf_rush_larsen_C);
    V_row[pas_rush_larsen_A_idx] = ((-pas_inf)*pas_rush_larsen_C);
    V_row[pi_rush_larsen_B_idx] = (exp(((-dt)/tau_pi)));
    double pi_rush_larsen_C = (expm1(((-dt)/tau_pi)));
    V_row[qa_rush_larsen_B_idx] = (exp(((-dt)/tau_qa)));
    double qa_rush_larsen_C = (expm1(((-dt)/tau_qa)));
    V_row[qi_rush_larsen_B_idx] = (exp(((-dt)/tau_qi)));
    double qi_rush_larsen_C = (expm1(((-dt)/tau_qi)));
    V_row[x_rush_larsen_A_idx] = ((-x_inf)*x_rush_larsen_C);
    V_row[y_rush_larsen_A_idx] = ((-y_inf)*y_rush_larsen_C);
    V_row[yf_rush_larsen_A_idx] = ((-yf_inf)*yf_rush_larsen_C);
    V_row[ys_rush_larsen_A_idx] = ((-ys_inf)*ys_rush_larsen_C);
    V_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    V_row[h2_rush_larsen_A_idx] = ((-h2_inf)*h2_rush_larsen_C);
    V_row[pi_rush_larsen_A_idx] = ((-pi_inf)*pi_rush_larsen_C);
    V_row[qa_rush_larsen_A_idx] = ((-qa_inf)*qa_rush_larsen_C);
    V_row[qi_rush_larsen_A_idx] = ((-qi_inf)*qi_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}



void InadaIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Inada_Params *p = imp.params();

  Inada_state *sv_base = (Inada_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double Cai_init = ((p->cell_type==AN) ? 1.0817e-7 : ((p->cell_type==N) ? 3.6327e-7 : 1.3399e-7));
  double Carel_init = ((p->cell_type==AN) ? 0.00040019 : ((p->cell_type==N) ? 8.1847e-5 : 0.00044752));
  double Casub_init = ((p->cell_type==AN) ? 5.86e-8 : ((p->cell_type==N) ? 2.3000e-7 : 7.1220e-8));
  double Caup_init = ((p->cell_type==AN) ? 0.00095955 : ((p->cell_type==N) ? 0.0011463 : 0.0011631));
  double V_init = ((p->cell_type==AN) ? -70.219 : ((p->cell_type==N) ? -62.132 : -68.668));
  double Volcell = ((((p->Cm-(20.0e-12))/45.0e-12)*(7.147123e-12-(2.19911e-12)))+2.19911e-12);
  double act_shift_CaL = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? -15.0 : 0.0));
  double d_init = ((p->cell_type==AN) ? 3.9525e-5 : ((p->cell_type==N) ? 0.00015340 : 4.9961e-5));
  double f1_init = ((p->cell_type==AN) ? 0.99854 : ((p->cell_type==N) ? 0.68093 : 0.99814));
  double f2_init = ((p->cell_type==AN) ? 0.99111 : ((p->cell_type==N) ? 0.44202 : 0.98513));
  double f_cmi_init = ((p->cell_type==AN) ? 0.043662 : ((p->cell_type==N) ? 0.13393 : 0.053550));
  double f_cms_init = ((p->cell_type==AN) ? 0.024102 : ((p->cell_type==N) ? 0.089149 : 0.029155));
  double f_cq_init = ((p->cell_type==AN) ? 0.32345 : ((p->cell_type==N) ? 0.086939 : 0.34828));
  double f_csl_init = ((p->cell_type==AN) ? 3.0851e-5 : ((p->cell_type==N) ? 4.6743e-5 : 4.4471e-5));
  double f_tc_init = ((p->cell_type==AN) ? 0.021203 : ((p->cell_type==N) ? 0.068561 : 0.026148));
  double f_tmc_init = ((p->cell_type==AN) ? 0.33949 : ((p->cell_type==N) ? 0.61949 : 0.39299));
  double f_tmm_init = ((p->cell_type==AN) ? 0.58343 : ((p->cell_type==N) ? 0.33603 : 0.53615));
  double h1_init = ((p->cell_type==AN) ? 0.72733 : ((p->cell_type==N) ? 0. : 0.64629));
  double h2_init = ((p->cell_type==AN) ? 0.63777 : ((p->cell_type==N) ? 0. : 0.56383));
  double inact_shift_CaL = ((p->cell_type==AN) ? -5.0 : ((p->cell_type==N) ? -5.0 : -5.0));
  double m_init = ((p->cell_type==AN) ? 0.012459 : ((p->cell_type==N) ? 0. : 0.015215));
  double paf_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.60607 : 0.093329));
  double pas_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.12876 : 0.067693));
  double pi_init = ((p->cell_type==AN) ? 0.986780 : ((p->cell_type==N) ? 0.97752 : 0.13435));
  double qa_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.19362 : 0.0));
  double qi_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.48850 : 0.0));
  double slope_factor_act_CaL = ((p->cell_type==AN) ? -6.61 : ((p->cell_type==N) ? -5.0 : -6.61));
  double x_init = ((p->cell_type==AN) ? 0.0087042 : ((p->cell_type==N) ? 0. : 0.0095587));
  double y_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.038225 : 0.0));
  double yf_init = ((p->cell_type==AN) ? 0.8847000 : ((p->cell_type==N) ? 0. : 0.87080));
  double ys_init = ((p->cell_type==AN) ? 0.2026300 : ((p->cell_type==N) ? 0.2026300 : 0.13435));
  double Volrel = (0.0012*Volcell);
  double Volsub = (0.01*Volcell);
  double Volup = (0.0116*Volcell);
  double Voli = ((0.46*Volcell)-(Volsub));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Inada_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    sv->GNa = p->GNa;
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    sv->Carel = Carel_init;
    sv->Casub = Casub_init;
    sv->Caup = Caup_init;
    V = V_init;
    sv->d = d_init;
    sv->f1 = f1_init;
    sv->f2 = f2_init;
    sv->f_cmi = f_cmi_init;
    sv->f_cms = f_cms_init;
    sv->f_cq = f_cq_init;
    sv->f_csl = f_csl_init;
    sv->f_tc = f_tc_init;
    sv->f_tmc = f_tmc_init;
    sv->f_tmm = f_tmm_init;
    sv->h1 = h1_init;
    sv->h2 = h2_init;
    sv->m = m_init;
    sv->paf = paf_init;
    sv->pas = pas_init;
    sv->pi = pi_init;
    sv->qa = qa_init;
    sv->qi = qi_init;
    sv->x = x_init;
    sv->y = y_init;
    sv->yf = yf_init;
    sv->ys = ys_init;
    double INa = (((fabs((V*0.001)))>TINY) ? (((((((((((sv->GNa*sv->m)*sv->m)*sv->m)*((0.635*sv->h1)+(0.365*sv->h2)))*Nae)*(V*0.001))*F)*F)/(R*T))*((exp((((V*0.001)-(E_Na))*(F/(R*T)))))-(1.0)))/((exp((((V*0.001)*F)/(R*T))))-(1.0))) : ((((((((((sv->GNa*sv->m)*sv->m)*sv->m)*((0.635*sv->h1)+(0.365*sv->h2)))*Nae)*F)*F)/(R*T))*(((exp(((((V*0.001)-(E_Na))*F)/(R*T))))+((((V*0.001)*F)/(R*T))*(exp(((((V*0.001)-(E_Na))*F)/(R*T))))))-(1.0)))/((F/(R*T))*(exp((((V*0.001)*F)/(R*T)))))));
    double vm = (V*0.001);
    double GK1_ = (p->GK1*(0.5+(0.5/(1.0+(exp((((vm*1.0e+3)-(-30.0))/5.0)))))));
    double ICaL = (((p->GCaL*sv->d)*((0.675*sv->f1)+(0.325*sv->f2)))*(vm-(p->E_CaL)));
    double IKr = (((p->GKr*((0.90*sv->paf)+(0.10*sv->pas)))*sv->pi)*(vm-(E_K)));
    double Ib = (p->Gb*(vm-(p->E_b)));
    double If = ((p->Gf*sv->y)*(vm-(-0.030)));
    double Ip = ((((p->p_max*(pow(((Nai*1.0e+3)/(5.64+(Nai*1.0e+3))),3.0)))*(pow(((Ke*1.0e+3)/(0.621+(Ke*1.0e+3))),2.0)))*1.6)/(1.5+(exp(((-((vm*1.0e+3)+60.0))/40.0)))));
    double Ist = (((p->Gst*sv->qa)*sv->qi)*(vm-(E_st)));
    double Ito = (((p->Gto*sv->x)*((0.45*sv->yf)+(0.55*sv->ys)))*(vm-(E_K)));
    double d_i = ((1.0+((sv->Casub/0.0207e-3)*((1.0+(exp((((-0.1369*vm)*F)/(R*T)))))+(Nai/26.44e-3))))+((Nai/395.3e-3)*(1.0+((Nai/2.289e-3)*(1.0+(Nai/26.44e-3))))));
    double k23 = (((((Nae/1628.0e-3)*(Nae/561.4e-3))*(1.0+(Nae/4.663e-3)))*(exp((((-0.4315*vm)*F)/((2.0*R)*T)))))/d_o);
    double k32 = (exp((((0.4315*vm)*F)/((2.0*R)*T))));
    double k41 = (exp((((-0.4315*vm)*F)/((2.0*R)*T))));
    double IK1 = (((GK1_*(pow((Ke/(Ke+0.590e-3)),3.0)))*(vm-(-0.0819)))/(1.0+(exp((((1.393*((vm-(-0.0819))+0.0036))*F)/(R*T))))));
    double k12 = (((sv->Casub/0.0207e-3)*(exp((((-0.1369*vm)*F)/(R*T)))))/d_i);
    double k14 = (((((Nai/395.3e-3)*(Nai/2.289e-3))*(1.0+(Nai/26.44e-3)))*(exp((((0.4315*vm)*F)/((2.0*R)*T)))))/d_i);
    double x1 = (((k34*k41)*(k23+k21))+((k21*k32)*(k43+k41)));
    double x2 = (((k43*k32)*(k14+k12))+((k41*k12)*(k34+k32)));
    double x3 = (((k43*k14)*(k23+k21))+((k12*k23)*(k43+k41)));
    double x4 = (((k34*k23)*(k14+k12))+((k21*k14)*(k34+k32)));
    double INaCa = ((p->kNaCa*((k21*x2)-((k12*x1))))/(((x1+x2)+x3)+x4));
    Iion = (((((((((((INa+ICaL)+IKr)+Ito)+If)+Ist)+Ib)+IK1)+Ip)+INaCa)*iJive)/(p->Cm/Cm_spec));
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef INADA_CPU_GENERATED
extern "C" {
void compute_Inada_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  InadaIonType::IonIfDerived& imp = static_cast<InadaIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Inada_Params *p  = imp.params();
  Inada_state *sv_base = (Inada_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  InadaIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t Cai_init = ((p->cell_type==AN) ? 1.0817e-7 : ((p->cell_type==N) ? 3.6327e-7 : 1.3399e-7));
  GlobalData_t Carel_init = ((p->cell_type==AN) ? 0.00040019 : ((p->cell_type==N) ? 8.1847e-5 : 0.00044752));
  GlobalData_t Casub_init = ((p->cell_type==AN) ? 5.86e-8 : ((p->cell_type==N) ? 2.3000e-7 : 7.1220e-8));
  GlobalData_t Caup_init = ((p->cell_type==AN) ? 0.00095955 : ((p->cell_type==N) ? 0.0011463 : 0.0011631));
  GlobalData_t V_init = ((p->cell_type==AN) ? -70.219 : ((p->cell_type==N) ? -62.132 : -68.668));
  GlobalData_t Volcell = ((((p->Cm-(20.0e-12))/45.0e-12)*(7.147123e-12-(2.19911e-12)))+2.19911e-12);
  GlobalData_t act_shift_CaL = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? -15.0 : 0.0));
  GlobalData_t d_init = ((p->cell_type==AN) ? 3.9525e-5 : ((p->cell_type==N) ? 0.00015340 : 4.9961e-5));
  GlobalData_t f1_init = ((p->cell_type==AN) ? 0.99854 : ((p->cell_type==N) ? 0.68093 : 0.99814));
  GlobalData_t f2_init = ((p->cell_type==AN) ? 0.99111 : ((p->cell_type==N) ? 0.44202 : 0.98513));
  GlobalData_t f_cmi_init = ((p->cell_type==AN) ? 0.043662 : ((p->cell_type==N) ? 0.13393 : 0.053550));
  GlobalData_t f_cms_init = ((p->cell_type==AN) ? 0.024102 : ((p->cell_type==N) ? 0.089149 : 0.029155));
  GlobalData_t f_cq_init = ((p->cell_type==AN) ? 0.32345 : ((p->cell_type==N) ? 0.086939 : 0.34828));
  GlobalData_t f_csl_init = ((p->cell_type==AN) ? 3.0851e-5 : ((p->cell_type==N) ? 4.6743e-5 : 4.4471e-5));
  GlobalData_t f_tc_init = ((p->cell_type==AN) ? 0.021203 : ((p->cell_type==N) ? 0.068561 : 0.026148));
  GlobalData_t f_tmc_init = ((p->cell_type==AN) ? 0.33949 : ((p->cell_type==N) ? 0.61949 : 0.39299));
  GlobalData_t f_tmm_init = ((p->cell_type==AN) ? 0.58343 : ((p->cell_type==N) ? 0.33603 : 0.53615));
  GlobalData_t h1_init = ((p->cell_type==AN) ? 0.72733 : ((p->cell_type==N) ? 0. : 0.64629));
  GlobalData_t h2_init = ((p->cell_type==AN) ? 0.63777 : ((p->cell_type==N) ? 0. : 0.56383));
  GlobalData_t inact_shift_CaL = ((p->cell_type==AN) ? -5.0 : ((p->cell_type==N) ? -5.0 : -5.0));
  GlobalData_t m_init = ((p->cell_type==AN) ? 0.012459 : ((p->cell_type==N) ? 0. : 0.015215));
  GlobalData_t paf_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.60607 : 0.093329));
  GlobalData_t pas_init = ((p->cell_type==AN) ? 0.034731 : ((p->cell_type==N) ? 0.12876 : 0.067693));
  GlobalData_t pi_init = ((p->cell_type==AN) ? 0.986780 : ((p->cell_type==N) ? 0.97752 : 0.13435));
  GlobalData_t qa_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.19362 : 0.0));
  GlobalData_t qi_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.48850 : 0.0));
  GlobalData_t slope_factor_act_CaL = ((p->cell_type==AN) ? -6.61 : ((p->cell_type==N) ? -5.0 : -6.61));
  GlobalData_t x_init = ((p->cell_type==AN) ? 0.0087042 : ((p->cell_type==N) ? 0. : 0.0095587));
  GlobalData_t y_init = ((p->cell_type==AN) ? 0.0 : ((p->cell_type==N) ? 0.038225 : 0.0));
  GlobalData_t yf_init = ((p->cell_type==AN) ? 0.8847000 : ((p->cell_type==N) ? 0. : 0.87080));
  GlobalData_t ys_init = ((p->cell_type==AN) ? 0.2026300 : ((p->cell_type==N) ? 0.2026300 : 0.13435));
  GlobalData_t Volrel = (0.0012*Volcell);
  GlobalData_t Volsub = (0.01*Volcell);
  GlobalData_t Volup = (0.0116*Volcell);
  GlobalData_t Voli = ((0.46*Volcell)-(Volsub));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Inada_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables()[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t INa = (((fabs((V*0.001)))>TINY) ? (((((((((((sv->GNa*sv->m)*sv->m)*sv->m)*((0.635*sv->h1)+(0.365*sv->h2)))*Nae)*(V*0.001))*F)*F)/(R*T))*((exp((((V*0.001)-(E_Na))*(F/(R*T)))))-(1.0)))/((exp((((V*0.001)*F)/(R*T))))-(1.0))) : ((((((((((sv->GNa*sv->m)*sv->m)*sv->m)*((0.635*sv->h1)+(0.365*sv->h2)))*Nae)*F)*F)/(R*T))*(((exp(((((V*0.001)-(E_Na))*F)/(R*T))))+((((V*0.001)*F)/(R*T))*(exp(((((V*0.001)-(E_Na))*F)/(R*T))))))-(1.0)))/((F/(R*T))*(exp((((V*0.001)*F)/(R*T)))))));
    GlobalData_t ICaL = (((p->GCaL*sv->d)*((0.675*sv->f1)+(0.325*sv->f2)))*(V_row[vm_idx]-(p->E_CaL)));
    GlobalData_t IKr = (((p->GKr*((0.90*sv->paf)+(0.10*sv->pas)))*sv->pi)*(V_row[vm_idx]-(E_K)));
    GlobalData_t If = ((p->Gf*sv->y)*(V_row[vm_idx]-(-0.030)));
    GlobalData_t Ist = (((p->Gst*sv->qa)*sv->qi)*(V_row[vm_idx]-(E_st)));
    GlobalData_t Ito = (((p->Gto*sv->x)*((0.45*sv->yf)+(0.55*sv->ys)))*(V_row[vm_idx]-(E_K)));
    GlobalData_t d_i = ((1.0+((sv->Casub/0.0207e-3)*((1.0+(exp((((-0.1369*V_row[vm_idx])*F)/(R*T)))))+(Nai/26.44e-3))))+((Nai/395.3e-3)*(1.0+((Nai/2.289e-3)*(1.0+(Nai/26.44e-3))))));
    GlobalData_t k12 = (((sv->Casub/0.0207e-3)*(exp((((-0.1369*V_row[vm_idx])*F)/(R*T)))))/d_i);
    GlobalData_t k14 = (((((Nai/395.3e-3)*(Nai/2.289e-3))*(1.0+(Nai/26.44e-3)))*(exp((((0.4315*V_row[vm_idx])*F)/((2.0*R)*T)))))/d_i);
    GlobalData_t x2 = (((k43*V_row[k32_idx])*(k14+k12))+((V_row[k41_idx]*k12)*(k34+V_row[k32_idx])));
    GlobalData_t x3 = (((k43*k14)*(V_row[k23_idx]+k21))+((k12*V_row[k23_idx])*(k43+V_row[k41_idx])));
    GlobalData_t x4 = (((k34*V_row[k23_idx])*(k14+k12))+((k21*k14)*(k34+V_row[k32_idx])));
    GlobalData_t INaCa = ((p->kNaCa*((k21*x2)-((k12*V_row[x1_idx]))))/(((V_row[x1_idx]+x2)+x3)+x4));
    Iion = (((((((((((INa+ICaL)+IKr)+Ito)+If)+Ist)+V_row[Ib_idx])+V_row[IK1_idx])+V_row[Ip_idx])+INaCa)*iJive)/(p->Cm/Cm_spec));
    
    
    //Complete Forward Euler Update
    GlobalData_t dd_f_cmi = (((227.7e+6*sv->Cai)*(1.0-(sv->f_cmi)))-((0.542e+3*sv->f_cmi)));
    GlobalData_t dd_f_cms = (((227.7e+6*sv->Casub)*(1.0-(sv->f_cms)))-((0.542e+3*sv->f_cms)));
    GlobalData_t dd_f_cq = (((0.534e+6*sv->Carel)*(1.0-(sv->f_cq)))-((0.445e+3*sv->f_cq)));
    GlobalData_t dd_f_csl = (((((115.0*1000.0)*sv->Casub)*(1.0-(sv->f_csl)))-((1000.0*sv->f_csl)))*0.001);
    GlobalData_t dd_f_tc = (((88.8e+6*sv->Cai)*(1.0-(sv->f_tc)))-((0.446e+3*sv->f_tc)));
    GlobalData_t dd_f_tmc = (((227.7e+6*sv->Cai)*((1.0-(sv->f_tmc))-(sv->f_tmm)))-((0.00751e+3*sv->f_tmc)));
    GlobalData_t dd_f_tmm = (((2.277e+6*2.5e-3)*((1.0-(sv->f_tmc))-(sv->f_tmm)))-((0.751e+3*sv->f_tmm)));
    GlobalData_t j_ca = ((sv->Casub-(sv->Cai))/0.04e-3);
    GlobalData_t j_rel = ((p->p_rel*(sv->Carel-(sv->Casub)))/(1.0+(pow((0.0012e-3/sv->Casub),2.0))));
    GlobalData_t j_tr = ((sv->Caup-(sv->Carel))/60.0e-3);
    GlobalData_t j_up = (0.005/(1.0+(0.0006e-3/sv->Cai)));
    GlobalData_t diff_Cai = (((((j_ca*Volsub)-((j_up*Volup)))/Voli)-((((0.045e-3*dd_f_cmi)+(0.031e-3*dd_f_tc))+(0.062e-3*dd_f_tmc))))/tJive);
    GlobalData_t diff_Carel = (((j_tr-(j_rel))-((10.0e-3*dd_f_cq)))/tJive);
    GlobalData_t diff_Casub = ((((((((-(ICaL-((2.0*INaCa))))/(2.0*F))+(j_rel*Volrel))/Volsub)-(j_ca))-((0.045e-3*dd_f_cms)))-(((0.031/1.2)*dd_f_csl)))/tJive);
    GlobalData_t diff_Caup = ((j_up-(((j_tr*Volrel)/Volup)))/tJive);
    GlobalData_t diff_f_cmi = (dd_f_cmi/tJive);
    GlobalData_t diff_f_cms = (dd_f_cms/tJive);
    GlobalData_t diff_f_cq = (dd_f_cq/tJive);
    GlobalData_t diff_f_csl = (dd_f_csl/tJive);
    GlobalData_t diff_f_tc = (dd_f_tc/tJive);
    GlobalData_t diff_f_tmc = (dd_f_tmc/tJive);
    GlobalData_t diff_f_tmm = (dd_f_tmm/tJive);
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Carel_new = sv->Carel+diff_Carel*dt;
    GlobalData_t Casub_new = sv->Casub+diff_Casub*dt;
    GlobalData_t Caup_new = sv->Caup+diff_Caup*dt;
    GlobalData_t f_cmi_new = sv->f_cmi+diff_f_cmi*dt;
    GlobalData_t f_cms_new = sv->f_cms+diff_f_cms*dt;
    GlobalData_t f_cq_new = sv->f_cq+diff_f_cq*dt;
    GlobalData_t f_csl_new = sv->f_csl+diff_f_csl*dt;
    GlobalData_t f_tc_new = sv->f_tc+diff_f_tc*dt;
    GlobalData_t f_tmc_new = sv->f_tmc+diff_f_tmc*dt;
    GlobalData_t f_tmm_new = sv->f_tmm+diff_f_tmm*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t f1_rush_larsen_B = V_row[f1_rush_larsen_B_idx];
    GlobalData_t f2_rush_larsen_B = V_row[f2_rush_larsen_B_idx];
    GlobalData_t h1_rush_larsen_B = V_row[h1_rush_larsen_B_idx];
    GlobalData_t h2_rush_larsen_B = V_row[h2_rush_larsen_B_idx];
    GlobalData_t paf_rush_larsen_B = V_row[paf_rush_larsen_B_idx];
    GlobalData_t pas_rush_larsen_B = V_row[pas_rush_larsen_B_idx];
    GlobalData_t x_rush_larsen_B = V_row[x_rush_larsen_B_idx];
    GlobalData_t y_rush_larsen_B = V_row[y_rush_larsen_B_idx];
    GlobalData_t yf_rush_larsen_B = V_row[yf_rush_larsen_B_idx];
    GlobalData_t ys_rush_larsen_B = V_row[ys_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f1_rush_larsen_A = V_row[f1_rush_larsen_A_idx];
    GlobalData_t f2_rush_larsen_A = V_row[f2_rush_larsen_A_idx];
    GlobalData_t h1_rush_larsen_A = V_row[h1_rush_larsen_A_idx];
    GlobalData_t paf_rush_larsen_A = V_row[paf_rush_larsen_A_idx];
    GlobalData_t pas_rush_larsen_A = V_row[pas_rush_larsen_A_idx];
    GlobalData_t pi_rush_larsen_B = V_row[pi_rush_larsen_B_idx];
    GlobalData_t qa_rush_larsen_B = V_row[qa_rush_larsen_B_idx];
    GlobalData_t qi_rush_larsen_B = V_row[qi_rush_larsen_B_idx];
    GlobalData_t x_rush_larsen_A = V_row[x_rush_larsen_A_idx];
    GlobalData_t y_rush_larsen_A = V_row[y_rush_larsen_A_idx];
    GlobalData_t yf_rush_larsen_A = V_row[yf_rush_larsen_A_idx];
    GlobalData_t ys_rush_larsen_A = V_row[ys_rush_larsen_A_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t h2_rush_larsen_A = V_row[h2_rush_larsen_A_idx];
    GlobalData_t pi_rush_larsen_A = V_row[pi_rush_larsen_A_idx];
    GlobalData_t qa_rush_larsen_A = V_row[qa_rush_larsen_A_idx];
    GlobalData_t qi_rush_larsen_A = V_row[qi_rush_larsen_A_idx];
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f1_new = f1_rush_larsen_A+f1_rush_larsen_B*sv->f1;
    GlobalData_t f2_new = f2_rush_larsen_A+f2_rush_larsen_B*sv->f2;
    GlobalData_t h1_new = h1_rush_larsen_A+h1_rush_larsen_B*sv->h1;
    GlobalData_t h2_new = h2_rush_larsen_A+h2_rush_larsen_B*sv->h2;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t paf_new = paf_rush_larsen_A+paf_rush_larsen_B*sv->paf;
    GlobalData_t pas_new = pas_rush_larsen_A+pas_rush_larsen_B*sv->pas;
    GlobalData_t pi_new = pi_rush_larsen_A+pi_rush_larsen_B*sv->pi;
    GlobalData_t qa_new = qa_rush_larsen_A+qa_rush_larsen_B*sv->qa;
    GlobalData_t qi_new = qi_rush_larsen_A+qi_rush_larsen_B*sv->qi;
    GlobalData_t x_new = x_rush_larsen_A+x_rush_larsen_B*sv->x;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    GlobalData_t yf_new = yf_rush_larsen_A+yf_rush_larsen_B*sv->yf;
    GlobalData_t ys_new = ys_rush_larsen_A+ys_rush_larsen_B*sv->ys;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    sv->Carel = Carel_new;
    sv->Casub = Casub_new;
    sv->Caup = Caup_new;
    Iion = Iion;
    sv->d = d_new;
    sv->f1 = f1_new;
    sv->f2 = f2_new;
    sv->f_cmi = f_cmi_new;
    sv->f_cms = f_cms_new;
    sv->f_cq = f_cq_new;
    sv->f_csl = f_csl_new;
    sv->f_tc = f_tc_new;
    sv->f_tmc = f_tmc_new;
    sv->f_tmm = f_tmm_new;
    sv->h1 = h1_new;
    sv->h2 = h2_new;
    sv->m = m_new;
    sv->paf = paf_new;
    sv->pas = pas_new;
    sv->pi = pi_new;
    sv->qa = qa_new;
    sv->qi = qi_new;
    sv->x = x_new;
    sv->y = y_new;
    sv->yf = yf_new;
    sv->ys = ys_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // INADA_CPU_GENERATED

bool InadaIonType::has_trace() const {
    return false;
}

void InadaIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* InadaIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void InadaIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        