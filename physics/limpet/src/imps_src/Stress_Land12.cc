// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: An analysis of deformation-dependent electromechanical coupling in the mouse heart
*  Authors: Sander Land, Steven A Niederer, Jan Magnus Aronsen, Emil K S Espe, Lili Zhang, William E Louch, Ivar Sjaastad, Ole M Sejersted, Nicolas P Smith
*  Year: 2012
*  Journal: Journal of Physiology 2012;590:4553-69
*  DOI: 10.1113/jphysiol.2012.231928
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Stress_Land12.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Stress_Land12IonType::Stress_Land12IonType(bool plugin) : IonType(std::move(std::string("Stress_Land12")), plugin) {}

size_t Stress_Land12IonType::params_size() const {
  return sizeof(struct Stress_Land12_Params);
}

size_t Stress_Land12IonType::dlo_vector_size() const {

  return 1;
}

uint32_t Stress_Land12IonType::reqdat() const {
  return Stress_Land12_REQDAT;
}

uint32_t Stress_Land12IonType::moddat() const {
  return Stress_Land12_MODDAT;
}

void Stress_Land12IonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Stress_Land12IonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef STRESS_LAND12_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(STRESS_LAND12_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(STRESS_LAND12_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(STRESS_LAND12_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef STRESS_LAND12_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef STRESS_LAND12_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef STRESS_LAND12_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef STRESS_LAND12_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Stress_Land12IonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef STRESS_LAND12_MLIR_CUDA_GENERATED
      compute_Stress_Land12_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(STRESS_LAND12_MLIR_ROCM_GENERATED)
      compute_Stress_Land12_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(STRESS_LAND12_MLIR_CPU_GENERATED)
      compute_Stress_Land12_mlir_cpu(start, end, imp, data);
#   elif defined(STRESS_LAND12_CPU_GENERATED)
      compute_Stress_Land12_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Stress_Land12IonType::compute."
#   endif
      break;
#   ifdef STRESS_LAND12_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Stress_Land12_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND12_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Stress_Land12_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND12_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Stress_Land12_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND12_CPU_GENERATED
    case Target::CPU:
      compute_Stress_Land12_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Q_1_init (GlobalData_t)(0.)
#define Q_2_init (GlobalData_t)(0.)
#define TRPN_init (GlobalData_t)(0.0752)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define xb_init (GlobalData_t)(0.00046)



void Stress_Land12IonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Stress_Land12_Params *p = imp.params();

  // Compute the regional constants
  {
    p->A_1 = -29.;
    p->A_2 = 116.;
    p->Ca_50ref = 0.8;
    p->TRPN_50 = 0.35;
    p->T_ref = 120.;
    p->a = 0.35;
    p->alpha_1 = 0.1;
    p->alpha_2 = 0.5;
    p->beta_0 = 1.65;
    p->beta_1 = -1.5;
    p->k_TRPN = 0.1;
    p->k_xb = 0.1;
    p->n_TRPN = 2.;
    p->n_xb = 5.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Stress_Land12IonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Stress_Land12_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void Stress_Land12IonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land12_Params *p = imp.params();

  Stress_Land12_state *sv_base = (Stress_Land12_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Stress_Land12_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Q_1 = Q_1_init;
    sv->Q_2 = Q_2_init;
    sv->TRPN = TRPN_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->xb = xb_init;
    double Q = (sv->Q_1+sv->Q_2);
    double lambda = length;
    double lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    double lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m);
    double overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    double T_0 = ((p->T_ref*sv->xb)*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef STRESS_LAND12_CPU_GENERATED
extern "C" {
void compute_Stress_Land12_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Stress_Land12IonType::IonIfDerived& imp = static_cast<Stress_Land12IonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land12_Params *p  = imp.params();
  Stress_Land12_state *sv_base = (Stress_Land12_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Stress_Land12IonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __Cai_getfcn_exists = __Cai_SVgetfcn ? 1 : 0;
  int __Cai_putfcn_exists = __Cai_SVputfcn ? 1 : 0;
  
          char* __parent_table_address = (char*)imp.parent()->get_sv_address();
          int __parent_table_size = imp.parent()->get_sv_size() / 1;
      

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Stress_Land12_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Q = (sv->Q_1+sv->Q_2);
    GlobalData_t lambda = length;
    GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    GlobalData_t lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m);
    GlobalData_t overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    GlobalData_t T_0 = ((p->T_ref*sv->xb)*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    
    
    //Complete Forward Euler Update
    GlobalData_t Ca_50 = (p->Ca_50ref*(1.+(p->beta_1*(lambda_m-(1.)))));
    GlobalData_t dlambdadt = delta_sl;
    GlobalData_t permtot = (sqrt((pow((sv->TRPN/p->TRPN_50),p->n_xb))));
    GlobalData_t diff_Q_1 = ((p->A_1*dlambdadt)-((p->alpha_1*sv->Q_1)));
    GlobalData_t diff_Q_2 = ((p->A_2*dlambdadt)-((p->alpha_2*sv->Q_2)));
    GlobalData_t diff_TRPN = (p->k_TRPN*(((pow((Cai/Ca_50),p->n_TRPN))*(1.-(sv->TRPN)))-(sv->TRPN)));
    GlobalData_t diff_xb = (p->k_xb*((permtot*(1.-(sv->xb)))-(((1./permtot)*sv->xb))));
    GlobalData_t Q_1_new = sv->Q_1+diff_Q_1*dt;
    GlobalData_t Q_2_new = sv->Q_2+diff_Q_2*dt;
    GlobalData_t TRPN_new = sv->TRPN+diff_TRPN*dt;
    GlobalData_t xb_new = sv->xb+diff_xb*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Q_1 = Q_1_new;
    sv->Q_2 = Q_2_new;
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->xb = xb_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;

  }

            }
}
#endif // STRESS_LAND12_CPU_GENERATED

bool Stress_Land12IonType::has_trace() const {
    return true;
}

void Stress_Land12IonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Stress_Land12_trace_header.txt","wt");
    fprintf(theader->fd,
        "Tension\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land12_Params *p  = imp.params();

  Stress_Land12_state *sv_base = (Stress_Land12_state *)imp.sv_tab().data();

  Stress_Land12_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;
  //Initialize the external vars to their current values
  GlobalData_t Tension = Tension_ext[__i];
  GlobalData_t delta_sl = delta_sl_ext[__i];
  GlobalData_t length = length_ext[__i];
  GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Q = (sv->Q_1+sv->Q_2);
  GlobalData_t lambda = length;
  GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
  GlobalData_t lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m);
  GlobalData_t overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
  GlobalData_t T_0 = ((p->T_ref*sv->xb)*overlap);
  Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", Tension);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* Stress_Land12IonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Stress_Land12IonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        