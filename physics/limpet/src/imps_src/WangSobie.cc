// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Linda J. Wang and Eric A. Sobie
*  Year: 2008
*  Title: Mathematical model of the neonatal mouse ventricular action potential
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 294(6), 2565-2575
*  DOI: 10.1152/ajpheart.01376.2007
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "WangSobie.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

WangSobieIonType::WangSobieIonType(bool plugin) : IonType(std::move(std::string("WangSobie")), plugin) {}

size_t WangSobieIonType::params_size() const {
  return sizeof(struct WangSobie_Params);
}

size_t WangSobieIonType::dlo_vector_size() const {

  return 1;
}

uint32_t WangSobieIonType::reqdat() const {
  return WangSobie_REQDAT;
}

uint32_t WangSobieIonType::moddat() const {
  return WangSobie_MODDAT;
}

void WangSobieIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target WangSobieIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef WANGSOBIE_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(WANGSOBIE_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(WANGSOBIE_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(WANGSOBIE_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef WANGSOBIE_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef WANGSOBIE_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef WANGSOBIE_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef WANGSOBIE_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void WangSobieIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef WANGSOBIE_MLIR_CUDA_GENERATED
      compute_WangSobie_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(WANGSOBIE_MLIR_ROCM_GENERATED)
      compute_WangSobie_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(WANGSOBIE_MLIR_CPU_GENERATED)
      compute_WangSobie_mlir_cpu(start, end, imp, data);
#   elif defined(WANGSOBIE_CPU_GENERATED)
      compute_WangSobie_cpu(start, end, imp, data);
#   else
#     error "Could not generate method WangSobieIonType::compute."
#   endif
      break;
#   ifdef WANGSOBIE_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_WangSobie_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef WANGSOBIE_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_WangSobie_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef WANGSOBIE_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_WangSobie_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef WANGSOBIE_CPU_GENERATED
    case Target::CPU:
      compute_WangSobie_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Acap (GlobalData_t)(1.5410e-5)
#define C2_init (GlobalData_t)(1.6996e-4)
#define C3_init (GlobalData_t)(1.1754e-8)
#define C4_init (GlobalData_t)(6.1239e-10)
#define CMDN_tot (GlobalData_t)(25.)
#define CSQN_tot (GlobalData_t)(15000.)
#define C_K1_init (GlobalData_t)(1.1e-3)
#define C_K2_init (GlobalData_t)(8.3641e-4)
#define CaJSR_init (GlobalData_t)(705.5417)
#define CaNSR_init (GlobalData_t)(703.3063)
#define Cao (GlobalData_t)(1000.)
#define Cass_init (GlobalData_t)(0.1867)
#define Cli (GlobalData_t)(30000.)
#define Clo (GlobalData_t)(132000.)
#define Cm (GlobalData_t)(1.)
#define E_CaL (GlobalData_t)(63.)
#define E_CaT (GlobalData_t)(50.)
#define F (GlobalData_t)(96.5)
#define GCaL (GlobalData_t)(0.19019)
#define GCaT (GlobalData_t)(0.055)
#define GCab (GlobalData_t)(0.00025)
#define GK1 (GlobalData_t)(0.235)
#define GKr (GlobalData_t)(01.17)
#define GKs (GlobalData_t)(0.046)
#define GKss (GlobalData_t)(0.015)
#define GKto_f (GlobalData_t)(0.1017)
#define GKur (GlobalData_t)(0.0048)
#define GNa (GlobalData_t)(10.)
#define GNab (GlobalData_t)(0.0026)
#define HTRPN_Ca_init (GlobalData_t)(66.0407)
#define HTRPN_tot (GlobalData_t)(70.)
#define I1_init (GlobalData_t)(1.5450e-9)
#define I2_init (GlobalData_t)(6.4226e-8)
#define I3_init (GlobalData_t)(6.5201e-7)
#define ICaL_max (GlobalData_t)(7.)
#define I_K_init (GlobalData_t)(4.3522e-4)
#define K_mCa (GlobalData_t)(1380.)
#define K_mNa (GlobalData_t)(87500.)
#define Km_CMDN (GlobalData_t)(0.238)
#define Km_CSQN (GlobalData_t)(800.)
#define Km_Cl (GlobalData_t)(4.)
#define Km_Ko (GlobalData_t)(1500.)
#define Km_Nai (GlobalData_t)(21000.)
#define Km_pCa (GlobalData_t)(0.5)
#define Km_up (GlobalData_t)(0.5)
#define Ko (GlobalData_t)(5400.)
#define Kpc_half (GlobalData_t)(20.)
#define Kpc_max (GlobalData_t)(0.23324)
#define LTRPN_Ca_init (GlobalData_t)(8.9220)
#define LTRPN_tot (GlobalData_t)(35.)
#define Nao (GlobalData_t)(140000.)
#define O_K_init (GlobalData_t)(2.1e-3)
#define O_init (GlobalData_t)(4.4776e-12)
#define P_C2_init (GlobalData_t)(0.1003)
#define P_ClCa (GlobalData_t)(2.74e-7)
#define P_O1_init (GlobalData_t)(1.1e-3)
#define P_O2_init (GlobalData_t)(2.9799e-8)
#define P_RyR_init (GlobalData_t)(2.8603e-14)
#define R (GlobalData_t)(8.314)
#define T (GlobalData_t)(298.)
#define VJSR (GlobalData_t)(0.12e-8)
#define VNSR (GlobalData_t)(2.098e-7)
#define V_init (GlobalData_t)(-80.6475)
#define Vmyo (GlobalData_t)(2.2826e-6)
#define Vss (GlobalData_t)(3.0734e-8)
#define aKss (GlobalData_t)(0.8500)
#define eta (GlobalData_t)(0.35)
#define k_NaCa (GlobalData_t)(907.68)
#define k_minus_a (GlobalData_t)(0.07125)
#define k_minus_b (GlobalData_t)(0.965)
#define k_minus_c (GlobalData_t)(0.0008)
#define k_minus_htrpn (GlobalData_t)(3.2e-5)
#define k_minus_ltrpn (GlobalData_t)(0.0196)
#define k_plus_a (GlobalData_t)(0.00608)
#define k_plus_b (GlobalData_t)(0.00405)
#define k_plus_c (GlobalData_t)(0.009)
#define k_plus_htrpn (GlobalData_t)(0.00237)
#define k_plus_ltrpn (GlobalData_t)(0.0327)
#define k_sat (GlobalData_t)(0.1)
#define kb (GlobalData_t)(0.036778)
#define kf (GlobalData_t)(0.023761)
#define maxINaK (GlobalData_t)(0.88)
#define maxIpCa (GlobalData_t)(0.2)
#define n_ClCa (GlobalData_t)(3.)
#define tau_tr (GlobalData_t)(20.)
#define tau_xfer (GlobalData_t)(8.)
#define v1 (GlobalData_t)(0.45)
#define v2 (GlobalData_t)(2.088e-5)
#define v3 (GlobalData_t)(0.09)
#define sigma (GlobalData_t)(((1.0/7.)*((exp((Nao/67300.)))-(1.))))



void WangSobieIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  WangSobie_Params *p = imp.params();

  // Compute the regional constants
  {
    p->Cai_init = 0.2049;
    p->Ki_init = 1.3645e5;
    p->Nai_init = 2.1747e4;
  }
  // Compute the regional initialization
  {
    p->ICaL_slowdown = 1.;
  }

}


// Define the parameters for the lookup tables
enum Tables {
  V_TAB,
  V_EK_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum V_TableIndex {
  aa_a0_idx,
  aa_a1_idx,
  aa_i_idx,
  alpha_idx,
  ato_f_rush_larsen_A_idx,
  ato_f_rush_larsen_B_idx,
  aur_rush_larsen_A_idx,
  aur_rush_larsen_B_idx,
  b_rush_larsen_A_idx,
  b_rush_larsen_B_idx,
  bb_a0_idx,
  bb_a1_idx,
  bb_i_idx,
  f_NaK_idx,
  g_rush_larsen_A_idx,
  g_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  ito_f_rush_larsen_A_idx,
  ito_f_rush_larsen_B_idx,
  iur_rush_larsen_A_idx,
  iur_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  nKs_rush_larsen_A_idx,
  nKs_rush_larsen_B_idx,
  NROWS_V
};

enum V_EK_TableIndex {
  IK1_idx,
  NROWS_V_EK
};



void WangSobieIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  WangSobie_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  
  // Create the V lookup table
  LUT* V_tab = &imp.tables()[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.01, "WangSobie V", imp.get_target());
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[aa_a0_idx] = (0.022348*(exp((0.01176*V))));
    V_row[aa_a1_idx] = (0.013733*(exp((0.038198*V))));
    V_row[aa_i_idx] = (0.090821*(exp((0.023391*(V+5.)))));
    double aa_ito_f = ((0.000152*(exp(((-(V-(3.81)))/15.75))))/((0.0067083*(exp(((-(V+132.05))/15.75))))+1.));
    V_row[alpha_idx] = (((0.4*(exp(((V+12.)/10.))))*((1.+(0.7*(exp(((-((V+40.)*(V+40.)))/10.)))))-((0.75*(exp(((-((V+20.)*(V+20.)))/400.)))))))/(1.+(0.12*(exp(((V+12.)/10.))))));
    double alpha_ato_f = (0.18064*(exp((0.03577*(V+30.)))));
    double alpha_h = ((V<-40.) ? (0.135*(exp(((80.+V)/-6.8)))) : 0.);
    double alpha_j = ((V<-40.) ? (((-((127140.*(exp((0.2444*V))))+(3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    double alpha_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    double alpha_nKs = ((V==-26.5) ? (0.00000481333/0.128) : ((0.00000481333*(V+26.5))/(-(expm1((-0.128*(V+26.5)))))));
    double aur_infinity = (1./(1.+(exp(((-(V+22.5))/7.7)))));
    double b_infinity = (1./(1.+(exp(((-(V+48.))/6.1)))));
    V_row[bb_a0_idx] = (0.047002*(exp((-0.0631*V))));
    V_row[bb_a1_idx] = (0.0000689*(exp((-0.04178*V))));
    V_row[bb_i_idx] = (0.006497*(exp((-0.03268*(V+5.)))));
    double bb_ito_f = ((0.00095*(exp(((V+132.05)/15.75))))/((0.051335*(exp(((V+132.05)/15.75))))+1.));
    double beta_ato_f = (0.3956*(exp((-0.06237*(V+30.)))));
    double beta_h = ((V<-40.) ? ((3.56*(exp((0.079*V))))+(310000.*(exp((0.35*V))))) : (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))));
    double beta_j = ((V<-40.) ? ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))) : ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))));
    double beta_m = (0.08*(exp(((-V)/11.))));
    double beta_nKs = (0.0000953333*(exp((-0.038*(V+26.5)))));
    V_row[f_NaK_idx] = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    double g_infinity = (1./(1.+(exp(((V+66.)/6.6)))));
    double iur_infinity = (1./(1.+(exp(((V+45.2)/5.7)))));
    double tau_aur = ((0.493*(exp((-0.0629*V))))+2.058);
    double tau_b = (0.1+(5.4/(1.+(exp(((V+100.)/6.6))))));
    double tau_g = (8.+(32./(1.+(exp(((V+65.)/5.))))));
    double tau_ito_f = (1./(((0.000152*(exp(((-(V+13.5))/7.))))/((0.067083*(exp(((-(V+33.5))/7.))))+1.))+((0.00095*(exp(((V+33.5)/7.))))/((0.051335*(exp(((V+33.5)/7.))))+1.))));
    double tau_iur = (1200.-((170./(1.+(exp(((V+45.2)/5.7)))))));
    V_row[ato_f_rush_larsen_A_idx] = (((-alpha_ato_f)/(alpha_ato_f+beta_ato_f))*(expm1(((-dt)*(alpha_ato_f+beta_ato_f)))));
    V_row[ato_f_rush_larsen_B_idx] = (exp(((-dt)*(alpha_ato_f+beta_ato_f))));
    V_row[aur_rush_larsen_B_idx] = (exp(((-dt)/tau_aur)));
    double aur_rush_larsen_C = (expm1(((-dt)/tau_aur)));
    V_row[b_rush_larsen_B_idx] = (exp(((-dt)/tau_b)));
    double b_rush_larsen_C = (expm1(((-dt)/tau_b)));
    V_row[g_rush_larsen_B_idx] = (exp(((-dt)/tau_g)));
    double g_rush_larsen_C = (expm1(((-dt)/tau_g)));
    V_row[h_rush_larsen_A_idx] = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(alpha_h+beta_h))));
    double ito_f_infinity = (aa_ito_f/(aa_ito_f+bb_ito_f));
    V_row[ito_f_rush_larsen_B_idx] = (exp(((-dt)/tau_ito_f)));
    double ito_f_rush_larsen_C = (expm1(((-dt)/tau_ito_f)));
    V_row[iur_rush_larsen_B_idx] = (exp(((-dt)/tau_iur)));
    double iur_rush_larsen_C = (expm1(((-dt)/tau_iur)));
    V_row[j_rush_larsen_A_idx] = (((-alpha_j)/(alpha_j+beta_j))*(expm1(((-dt)*(alpha_j+beta_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(alpha_j+beta_j))));
    V_row[m_rush_larsen_A_idx] = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(alpha_m+beta_m))));
    V_row[nKs_rush_larsen_A_idx] = (((-alpha_nKs)/(alpha_nKs+beta_nKs))*(expm1(((-dt)*(alpha_nKs+beta_nKs)))));
    V_row[nKs_rush_larsen_B_idx] = (exp(((-dt)*(alpha_nKs+beta_nKs))));
    V_row[aur_rush_larsen_A_idx] = ((-aur_infinity)*aur_rush_larsen_C);
    V_row[b_rush_larsen_A_idx] = ((-b_infinity)*b_rush_larsen_C);
    V_row[g_rush_larsen_A_idx] = ((-g_infinity)*g_rush_larsen_C);
    V_row[ito_f_rush_larsen_A_idx] = ((-ito_f_infinity)*ito_f_rush_larsen_C);
    V_row[iur_rush_larsen_A_idx] = ((-iur_infinity)*iur_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the V_EK lookup table
  LUT* V_EK_tab = &imp.tables()[V_EK_TAB];
  LUT_alloc(V_EK_tab, NROWS_V_EK, -800, 800, 0.01, "WangSobie V_EK", imp.get_target());
  for (int __i=V_EK_tab->mn_ind; __i<=V_EK_tab->mx_ind; __i++) {
    double V_EK = V_EK_tab->res*__i;
    LUT_data_t* V_EK_row = V_EK_tab->tab[__i];
    V_EK_row[IK1_idx] = ((((GK1*Ko)/(Ko+210.))*V_EK)/(1.+(exp((0.0896*V_EK)))));
  }
  check_LUT(V_EK_tab);
  

}



void WangSobieIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  WangSobie_Params *p = imp.params();

  WangSobie_state *sv_base = (WangSobie_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    WangSobie_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    sv->ICaL_slowdown = p->ICaL_slowdown;
    // Initialize the rest of the nodal variables
    sv->C2 = C2_init;
    sv->C3 = C3_init;
    sv->C4 = C4_init;
    sv->C_K1 = C_K1_init;
    sv->C_K2 = C_K2_init;
    sv->CaJSR = CaJSR_init;
    sv->CaNSR = CaNSR_init;
    sv->Cai = p->Cai_init;
    sv->Cass = Cass_init;
    sv->HTRPN_Ca = HTRPN_Ca_init;
    sv->I1 = I1_init;
    sv->I2 = I2_init;
    sv->I3 = I3_init;
    sv->I_K = I_K_init;
    sv->Ki = p->Ki_init;
    sv->LTRPN_Ca = LTRPN_Ca_init;
    sv->Nai = p->Nai_init;
    sv->O = O_init;
    sv->O_K = O_K_init;
    sv->P_C2 = P_C2_init;
    sv->P_O1 = P_O1_init;
    sv->P_O2 = P_O2_init;
    sv->P_RyR = P_RyR_init;
    V = V_init;
    double E_CaN = (((R*T)/(2.*F))*(log((Cao/sv->Cai))));
    double E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    double E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    double ICaL = ((GCaL*sv->O)*(V-(E_CaL)));
    double INaCa = ((((k_NaCa/(((K_mNa*K_mNa)*K_mNa)+((Nao*Nao)*Nao)))/(K_mCa+Cao))/(1.+(k_sat*(exp(((((eta-(1.))*V)*F)/(R*T)))))))*((((exp((((eta*V)*F)/(R*T))))*((sv->Nai*sv->Nai)*sv->Nai))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*sv->Cai))));
    double IpCa = ((maxIpCa*(sv->Cai*sv->Cai))/((Km_pCa*Km_pCa)+(sv->Cai*sv->Cai)));
    double aa_ito_f = ((0.000152*(exp(((-(V-(3.81)))/15.75))))/((0.0067083*(exp(((-(V+132.05))/15.75))))+1.));
    double alpha_ato_f = (0.18064*(exp((0.03577*(V+30.)))));
    double alpha_h = ((V<-40.) ? (0.135*(exp(((80.+V)/-6.8)))) : 0.);
    double alpha_j = ((V<-40.) ? (((-((127140.*(exp((0.2444*V))))+(3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    double alpha_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    double alpha_nKs = ((V==-26.5) ? (0.00000481333/0.128) : ((0.00000481333*(V+26.5))/(-(expm1((-0.128*(V+26.5)))))));
    double aur_infinity = (1./(1.+(exp(((-(V+22.5))/7.7)))));
    double b_infinity = (1./(1.+(exp(((-(V+48.))/6.1)))));
    double bb_ito_f = ((0.00095*(exp(((V+132.05)/15.75))))/((0.051335*(exp(((V+132.05)/15.75))))+1.));
    double beta_ato_f = (0.3956*(exp((-0.06237*(V+30.)))));
    double beta_h = ((V<-40.) ? ((3.56*(exp((0.079*V))))+(310000.*(exp((0.35*V))))) : (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))));
    double beta_j = ((V<-40.) ? ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))) : ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))));
    double beta_m = (0.08*(exp(((-V)/11.))));
    double beta_nKs = (0.0000953333*(exp((-0.038*(V+26.5)))));
    double f_ClCa = (((((sv->Cass/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass));
    double f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    double g_infinity = (1./(1.+(exp(((V+66.)/6.6)))));
    double iur_infinity = (1./(1.+(exp(((V+45.2)/5.7)))));
    double IClCa = (((((P_ClCa*f_ClCa)*((V*F)*F))/(R*T))*((Clo*(exp(((V*F)/(R*T)))))-(Cli)))/(expm1(((V*F)/(R*T)))));
    double IKr = ((GKr*sv->O_K)*(V-(E_K)));
    double IKss = ((GKss*aKss)*(V-(E_K)));
    double INaK = ((((maxINaK*f_NaK)/(1.+(pow((Km_Nai/sv->Nai),1.5))))*Ko)/(Ko+Km_Ko));
    double IbCa = (GCab*(V-(E_CaN)));
    double IbNa = (GNab*(V-(E_Na)));
    double V_EK = (V-(E_K));
    double ato_f_init = (alpha_ato_f/(alpha_ato_f+beta_ato_f));
    double aur_init = aur_infinity;
    double b_init = b_infinity;
    double g_init = g_infinity;
    double h_init = (alpha_h/(alpha_h+beta_h));
    double ito_f_infinity = (aa_ito_f/(aa_ito_f+bb_ito_f));
    double iur_init = iur_infinity;
    double j_init = (alpha_j/(alpha_j+beta_j));
    double m_init = (alpha_m/(alpha_m+beta_m));
    double nKs_init = (alpha_nKs/(alpha_nKs+beta_nKs));
    double IK1 = ((((GK1*Ko)/(Ko+210.))*V_EK)/(1.+(exp((0.0896*V_EK)))));
    sv->ato_f = ato_f_init;
    sv->aur = aur_init;
    sv->b = b_init;
    sv->g = g_init;
    sv->h = h_init;
    double ito_f_init = ito_f_infinity;
    sv->iur = iur_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->nKs = nKs_init;
    double ICaT = (((GCaT*sv->b)*sv->g)*(V-(E_CaT)));
    double IKs = ((GKs*(sv->nKs*sv->nKs))*(V-(E_K)));
    double IKur = (((GKur*sv->aur)*sv->iur)*(V-(E_K)));
    double INa = ((((((GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    double ato_f_65 = (pow(sv->ato_f,6.5));
    sv->ito_f = ito_f_init;
    double Itof = (((GKto_f*ato_f_65)*sv->ito_f)*(V-(E_K)));
    Iion = ((((((((((((((ICaL+ICaT)+IpCa)+INaCa)+IbCa)+INa)+IbNa)+INaK)+Itof)+IK1)+IKs)+IKur)+IKss)+IKr)+IClCa);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef WANGSOBIE_CPU_GENERATED
extern "C" {
void compute_WangSobie_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  WangSobieIonType::IonIfDerived& imp = static_cast<WangSobieIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  WangSobie_Params *p  = imp.params();
  WangSobie_state *sv_base = (WangSobie_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  WangSobieIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    WangSobie_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables()[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t E_CaN = (((R*T)/(2.*F))*(log((Cao/sv->Cai))));
    GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    GlobalData_t ICaL = ((GCaL*sv->O)*(V-(E_CaL)));
    GlobalData_t ICaT = (((GCaT*sv->b)*sv->g)*(V-(E_CaT)));
    GlobalData_t INaCa = ((((k_NaCa/(((K_mNa*K_mNa)*K_mNa)+((Nao*Nao)*Nao)))/(K_mCa+Cao))/(1.+(k_sat*(exp(((((eta-(1.))*V)*F)/(R*T)))))))*((((exp((((eta*V)*F)/(R*T))))*((sv->Nai*sv->Nai)*sv->Nai))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*sv->Cai))));
    GlobalData_t IpCa = ((maxIpCa*(sv->Cai*sv->Cai))/((Km_pCa*Km_pCa)+(sv->Cai*sv->Cai)));
    GlobalData_t ato_f_65 = (pow(sv->ato_f,6.5));
    GlobalData_t f_ClCa = (((((sv->Cass/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass));
    GlobalData_t IClCa = (((((P_ClCa*f_ClCa)*((V*F)*F))/(R*T))*((Clo*(exp(((V*F)/(R*T)))))-(Cli)))/(expm1(((V*F)/(R*T)))));
    GlobalData_t IKr = ((GKr*sv->O_K)*(V-(E_K)));
    GlobalData_t IKs = ((GKs*(sv->nKs*sv->nKs))*(V-(E_K)));
    GlobalData_t IKss = ((GKss*aKss)*(V-(E_K)));
    GlobalData_t IKur = (((GKur*sv->aur)*sv->iur)*(V-(E_K)));
    GlobalData_t INa = ((((((GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    GlobalData_t INaK = ((((maxINaK*V_row[f_NaK_idx])/(1.+(pow((Km_Nai/sv->Nai),1.5))))*Ko)/(Ko+Km_Ko));
    GlobalData_t IbCa = (GCab*(V-(E_CaN)));
    GlobalData_t IbNa = (GNab*(V-(E_Na)));
    GlobalData_t Itof = (((GKto_f*ato_f_65)*sv->ito_f)*(V-(E_K)));
    GlobalData_t V_EK = (V-(E_K));
    LUT_data_t V_EK_row[NROWS_V_EK];
    LUT_interpRow(&IF->tables()[V_EK_TAB], V_EK, __i, V_EK_row);
    Iion = ((((((((((((((ICaL+ICaT)+IpCa)+INaCa)+IbCa)+INa)+IbNa)+INaK)+Itof)+V_EK_row[IK1_idx])+IKs)+IKur)+IKss)+IKr)+IClCa);
    
    
    //Complete Forward Euler Update
    GlobalData_t BJSR = (1./(1.+((CSQN_tot*Km_CSQN)/((Km_CSQN+sv->CaJSR)*(Km_CSQN+sv->CaJSR)))));
    GlobalData_t Bi = (1./(1.+((CMDN_tot*Km_CMDN)/((Km_CMDN+sv->Cai)*(Km_CMDN+sv->Cai)))));
    GlobalData_t Bss = (1./(1.+((CMDN_tot*Km_CMDN)/((Km_CMDN+sv->Cass)*(Km_CMDN+sv->Cass)))));
    GlobalData_t C1 = (1.-((((((((sv->O+sv->C2)+sv->C2)+sv->C3)+sv->C4)+sv->I1)+sv->I2)+sv->I3)));
    GlobalData_t C_K0 = (1.-((((sv->C_K1+sv->C_K2)+sv->O_K)+sv->I_K)));
    GlobalData_t J_leak = (v2*(sv->CaNSR-(sv->Cai)));
    GlobalData_t J_rel = (((v1*(sv->P_O1+sv->P_O2))*(sv->CaJSR-(sv->Cass)))*sv->P_RyR);
    GlobalData_t J_tr = ((sv->CaNSR-(sv->CaJSR))/tau_tr);
    GlobalData_t J_trpn = ((((k_plus_htrpn*sv->Cai)*(HTRPN_tot-(sv->HTRPN_Ca)))+((k_plus_ltrpn*sv->Cai)*(LTRPN_tot-(sv->LTRPN_Ca))))-(((k_minus_htrpn*sv->HTRPN_Ca)+(k_minus_ltrpn*sv->LTRPN_Ca))));
    GlobalData_t J_up = ((v3*(sv->Cai*sv->Cai))/((Km_up*Km_up)+(sv->Cai*sv->Cai)));
    GlobalData_t J_xfer = ((sv->Cass-(sv->Cai))/tau_xfer);
    GlobalData_t Kpcb = (0.0005/sv->ICaL_slowdown);
    GlobalData_t Kpcf = ((13.*(1.-((exp(((-((V+14.5)*(V+14.5)))/100.))))))/sv->ICaL_slowdown);
    GlobalData_t P_C1 = (1.-(((sv->P_C2+sv->P_O1)+sv->P_O2)));
    GlobalData_t beta = ((0.05*(exp(((-(V+12.))/13.))))/sv->ICaL_slowdown);
    GlobalData_t diff_HTRPN_Ca = (((k_plus_htrpn*sv->Cai)*(HTRPN_tot-(sv->HTRPN_Ca)))-((k_minus_htrpn*sv->HTRPN_Ca)));
    GlobalData_t diff_LTRPN_Ca = (((k_plus_ltrpn*sv->Cai)*(LTRPN_tot-(sv->LTRPN_Ca)))-((k_minus_ltrpn*sv->LTRPN_Ca)));
    GlobalData_t diff_Nai = ((((-(((INa+IbNa)+(3.*INaK))+(3.*INaCa)))*Acap)*Cm)/(Vmyo*F));
    GlobalData_t diff_P_C2 = ((k_plus_c*sv->P_O1)-((k_minus_c*sv->P_C2)));
    GlobalData_t diff_P_O2 = (((k_plus_b*((sv->Cass*sv->Cass)*sv->Cass))*sv->P_O1)-((k_minus_b*sv->P_O2)));
    GlobalData_t diff_P_RyR = ((-0.04*sv->P_RyR)-((((0.1*ICaL)/ICaL_max)*(exp(((-((V-(5.))*(V-(5.))))/648.))))));
    GlobalData_t gamma = (((Kpc_max*sv->Cass)/(Kpc_half+sv->Cass))/sv->ICaL_slowdown);
    GlobalData_t diff_C2 = ((((4.*V_row[alpha_idx])*C1)+((2.*beta)*sv->C3))-(((beta*sv->C2)+((3.*V_row[alpha_idx])*sv->C2))));
    GlobalData_t diff_C3 = ((((3.*V_row[alpha_idx])*sv->C2)+((3.*beta)*sv->C4))-((((2.*beta)*sv->C3)+((2.*V_row[alpha_idx])*sv->C3))));
    GlobalData_t diff_C4 = (((((((2.*V_row[alpha_idx])*sv->C3)+((4.*beta)*sv->O))+(0.01*((((4.*Kpcb)*beta)*sv->I1)-(((V_row[alpha_idx]*gamma)*sv->C4)))))+(0.002*(((4.*beta)*sv->I2)-((Kpcf*sv->C4)))))+(((4.*beta)*Kpcb)*sv->I3))-(((((3.*beta)*sv->C4)+(V_row[alpha_idx]*sv->C4))+((gamma*Kpcf)*sv->C4))));
    GlobalData_t diff_C_K1 = (((V_row[aa_a0_idx]*C_K0)+(kb*sv->C_K2))-(((V_row[bb_a0_idx]*sv->C_K1)+(kf*sv->C_K1))));
    GlobalData_t diff_C_K2 = (((kf*sv->C_K1)+(V_row[bb_a1_idx]*sv->O_K))-(((kb*sv->C_K2)+(V_row[aa_a1_idx]*sv->C_K2))));
    GlobalData_t diff_CaJSR = (BJSR*(J_xfer-(J_rel)));
    GlobalData_t diff_CaNSR = ((((J_up-(J_leak))*Vmyo)/VNSR)-(((J_tr*VJSR)/VNSR)));
    GlobalData_t diff_Cai = (Bi*((J_leak+J_xfer)-((J_up+J_trpn))));
    GlobalData_t diff_Cass = (Bss*(((J_rel*VJSR)/Vss)-((((J_xfer*Vmyo)/Vss)+((((((ICaL+IbCa)+IpCa)+ICaT)-((2.*ICaT)))*(Acap*Cm))/((2.*Vss)*F))))));
    GlobalData_t diff_I1 = ((((gamma*sv->O)+(0.001*((V_row[alpha_idx]*sv->I3)-((Kpcf*sv->I1)))))+(0.01*(((V_row[alpha_idx]*gamma)*sv->C4)-((((4.*beta)*Kpcf)*sv->I1)))))-((Kpcb*sv->I1)));
    GlobalData_t diff_I2 = ((((0.001*((Kpcf*sv->O)-((V_row[alpha_idx]*sv->I2))))+(Kpcb*sv->I3))+(0.002*((Kpcf*sv->C4)-(((4.*beta)*sv->I2)))))-((gamma*sv->I2)));
    GlobalData_t diff_I3 = ((((0.001*((Kpcf*sv->I1)-((V_row[alpha_idx]*sv->I3))))+(gamma*sv->I2))+((gamma*Kpcf)*sv->C4))-(((((4.*beta)*Kpcb)*sv->I3)+(Kpcb*sv->I3))));
    GlobalData_t diff_I_K = ((V_row[aa_i_idx]*sv->O_K)-((V_row[bb_i_idx]*sv->I_K)));
    GlobalData_t diff_Ki = ((((-((((((Itof+V_EK_row[IK1_idx])+IKs)+IKss)+IKur)+IKr)-((2.*INaK))))*Acap)*Cm)/(Vmyo*F));
    GlobalData_t diff_O = ((((V_row[alpha_idx]*sv->C4)+(Kpcb*sv->I1))+(0.001*((V_row[alpha_idx]*sv->I2)-((Kpcf*sv->O)))))-((((4.*beta)*sv->O)+(gamma*sv->O))));
    GlobalData_t diff_O_K = (((V_row[aa_a1_idx]*sv->C_K2)+(V_row[bb_i_idx]*sv->I_K))-(((V_row[bb_a1_idx]*sv->O_K)+(V_row[aa_i_idx]*sv->O_K))));
    GlobalData_t diff_P_O1 = (((((k_plus_a*(((sv->Cass*sv->Cass)*sv->Cass)*sv->Cass))*P_C1)+(k_minus_b*sv->P_O2))+(k_minus_c*sv->P_C2))-((((k_minus_a*sv->P_O1)+((k_plus_b*((sv->Cass*sv->Cass)*sv->Cass))*sv->P_O1))+(k_plus_c*sv->P_O1))));
    GlobalData_t C2_new = sv->C2+diff_C2*dt;
    GlobalData_t C3_new = sv->C3+diff_C3*dt;
    GlobalData_t C4_new = sv->C4+diff_C4*dt;
    GlobalData_t C_K1_new = sv->C_K1+diff_C_K1*dt;
    GlobalData_t C_K2_new = sv->C_K2+diff_C_K2*dt;
    GlobalData_t CaJSR_new = sv->CaJSR+diff_CaJSR*dt;
    GlobalData_t CaNSR_new = sv->CaNSR+diff_CaNSR*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Cass_new = sv->Cass+diff_Cass*dt;
    GlobalData_t HTRPN_Ca_new = sv->HTRPN_Ca+diff_HTRPN_Ca*dt;
    GlobalData_t I1_new = sv->I1+diff_I1*dt;
    GlobalData_t I2_new = sv->I2+diff_I2*dt;
    GlobalData_t I3_new = sv->I3+diff_I3*dt;
    GlobalData_t I_K_new = sv->I_K+diff_I_K*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t LTRPN_Ca_new = sv->LTRPN_Ca+diff_LTRPN_Ca*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t O_K_new = sv->O_K+diff_O_K*dt;
    GlobalData_t P_C2_new = sv->P_C2+diff_P_C2*dt;
    GlobalData_t P_O1_new = sv->P_O1+diff_P_O1*dt;
    GlobalData_t P_O2_new = sv->P_O2+diff_P_O2*dt;
    GlobalData_t P_RyR_new = sv->P_RyR+diff_P_RyR*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t ato_f_rush_larsen_A = V_row[ato_f_rush_larsen_A_idx];
    GlobalData_t ato_f_rush_larsen_B = V_row[ato_f_rush_larsen_B_idx];
    GlobalData_t aur_rush_larsen_B = V_row[aur_rush_larsen_B_idx];
    GlobalData_t b_rush_larsen_B = V_row[b_rush_larsen_B_idx];
    GlobalData_t g_rush_larsen_B = V_row[g_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t ito_f_rush_larsen_B = V_row[ito_f_rush_larsen_B_idx];
    GlobalData_t iur_rush_larsen_B = V_row[iur_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t nKs_rush_larsen_A = V_row[nKs_rush_larsen_A_idx];
    GlobalData_t nKs_rush_larsen_B = V_row[nKs_rush_larsen_B_idx];
    GlobalData_t aur_rush_larsen_A = V_row[aur_rush_larsen_A_idx];
    GlobalData_t b_rush_larsen_A = V_row[b_rush_larsen_A_idx];
    GlobalData_t g_rush_larsen_A = V_row[g_rush_larsen_A_idx];
    GlobalData_t ito_f_rush_larsen_A = V_row[ito_f_rush_larsen_A_idx];
    GlobalData_t iur_rush_larsen_A = V_row[iur_rush_larsen_A_idx];
    GlobalData_t ato_f_new = ato_f_rush_larsen_A+ato_f_rush_larsen_B*sv->ato_f;
    GlobalData_t aur_new = aur_rush_larsen_A+aur_rush_larsen_B*sv->aur;
    GlobalData_t b_new = b_rush_larsen_A+b_rush_larsen_B*sv->b;
    GlobalData_t g_new = g_rush_larsen_A+g_rush_larsen_B*sv->g;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t ito_f_new = ito_f_rush_larsen_A+ito_f_rush_larsen_B*sv->ito_f;
    GlobalData_t iur_new = iur_rush_larsen_A+iur_rush_larsen_B*sv->iur;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t nKs_new = nKs_rush_larsen_A+nKs_rush_larsen_B*sv->nKs;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->C2 = C2_new;
    sv->C3 = C3_new;
    sv->C4 = C4_new;
    sv->C_K1 = C_K1_new;
    sv->C_K2 = C_K2_new;
    sv->CaJSR = CaJSR_new;
    sv->CaNSR = CaNSR_new;
    sv->Cai = Cai_new;
    sv->Cass = Cass_new;
    sv->HTRPN_Ca = HTRPN_Ca_new;
    sv->I1 = I1_new;
    sv->I2 = I2_new;
    sv->I3 = I3_new;
    sv->I_K = I_K_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->LTRPN_Ca = LTRPN_Ca_new;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->O_K = O_K_new;
    sv->P_C2 = P_C2_new;
    sv->P_O1 = P_O1_new;
    sv->P_O2 = P_O2_new;
    sv->P_RyR = P_RyR_new;
    sv->ato_f = ato_f_new;
    sv->aur = aur_new;
    sv->b = b_new;
    sv->g = g_new;
    sv->h = h_new;
    sv->ito_f = ito_f_new;
    sv->iur = iur_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->nKs = nKs_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // WANGSOBIE_CPU_GENERATED

bool WangSobieIonType::has_trace() const {
    return true;
}

void WangSobieIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("WangSobie_trace_header.txt","wt");
    fprintf(theader->fd,
        "ICaL\n"
        "ICaT\n"
        "IClCa\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "IKss\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "IbNa\n"
        "Iion\n"
        "IpCa\n"
        "Itof\n"
        "sv->Ki\n"
        "Kpcf\n"
        "V\n"
        "alpha\n"
        "beta\n"
        "gamma\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  WangSobie_Params *p  = imp.params();

  WangSobie_state *sv_base = (WangSobie_state *)imp.sv_tab().data();

  WangSobie_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t E_CaN = (((R*T)/(2.*F))*(log((Cao/sv->Cai))));
  GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
  GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
  GlobalData_t ICaL = ((GCaL*sv->O)*(V-(E_CaL)));
  GlobalData_t ICaT = (((GCaT*sv->b)*sv->g)*(V-(E_CaT)));
  GlobalData_t INaCa = ((((k_NaCa/(((K_mNa*K_mNa)*K_mNa)+((Nao*Nao)*Nao)))/(K_mCa+Cao))/(1.+(k_sat*(exp(((((eta-(1.))*V)*F)/(R*T)))))))*((((exp((((eta*V)*F)/(R*T))))*((sv->Nai*sv->Nai)*sv->Nai))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*sv->Cai))));
  GlobalData_t IpCa = ((maxIpCa*(sv->Cai*sv->Cai))/((Km_pCa*Km_pCa)+(sv->Cai*sv->Cai)));
  GlobalData_t Kpcf = ((13.*(1.-((exp(((-((V+14.5)*(V+14.5)))/100.))))))/sv->ICaL_slowdown);
  GlobalData_t alpha = (((0.4*(exp(((V+12.)/10.))))*((1.+(0.7*(exp(((-((V+40.)*(V+40.)))/10.)))))-((0.75*(exp(((-((V+20.)*(V+20.)))/400.)))))))/(1.+(0.12*(exp(((V+12.)/10.))))));
  GlobalData_t ato_f_65 = (pow(sv->ato_f,6.5));
  GlobalData_t beta = ((0.05*(exp(((-(V+12.))/13.))))/sv->ICaL_slowdown);
  GlobalData_t f_ClCa = (((((sv->Cass/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass))*sv->Cass)/(Km_Cl+sv->Cass));
  GlobalData_t f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
  GlobalData_t gamma = (((Kpc_max*sv->Cass)/(Kpc_half+sv->Cass))/sv->ICaL_slowdown);
  GlobalData_t IClCa = (((((P_ClCa*f_ClCa)*((V*F)*F))/(R*T))*((Clo*(exp(((V*F)/(R*T)))))-(Cli)))/(expm1(((V*F)/(R*T)))));
  GlobalData_t IKr = ((GKr*sv->O_K)*(V-(E_K)));
  GlobalData_t IKs = ((GKs*(sv->nKs*sv->nKs))*(V-(E_K)));
  GlobalData_t IKss = ((GKss*aKss)*(V-(E_K)));
  GlobalData_t IKur = (((GKur*sv->aur)*sv->iur)*(V-(E_K)));
  GlobalData_t INa = ((((((GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
  GlobalData_t INaK = ((((maxINaK*f_NaK)/(1.+(pow((Km_Nai/sv->Nai),1.5))))*Ko)/(Ko+Km_Ko));
  GlobalData_t IbCa = (GCab*(V-(E_CaN)));
  GlobalData_t IbNa = (GNab*(V-(E_Na)));
  GlobalData_t Itof = (((GKto_f*ato_f_65)*sv->ito_f)*(V-(E_K)));
  GlobalData_t V_EK = (V-(E_K));
  GlobalData_t IK1 = ((((GK1*Ko)/(Ko+210.))*V_EK)/(1.+(exp((0.0896*V_EK)))));
  Iion = ((((((((((((((ICaL+ICaT)+IpCa)+INaCa)+IbCa)+INa)+IbNa)+INaK)+Itof)+IK1)+IKs)+IKur)+IKss)+IKr)+IClCa);
  //Output the desired variables
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaT);
  fprintf(file, "%4.12f\t", IClCa);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKss);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", Itof);
  fprintf(file, "%4.12f\t", sv->Ki);
  fprintf(file, "%4.12f\t", Kpcf);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", alpha);
  fprintf(file, "%4.12f\t", beta);
  fprintf(file, "%4.12f\t", gamma);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* WangSobieIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void WangSobieIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        