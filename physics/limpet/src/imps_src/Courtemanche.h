// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Courtemanche, M., Ramirez, R. J., and Nattel, S.
*  Year: 1998
*  Title: Ionic mechanisms underlying human atrial action potential properties: insights from a mathematical model
*  Journal: Am. J. Physiol. 275(1 Pt 2), H301-H321
*  DOI: 10.1152/ajpheart.1998.275.1.H301
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __COURTEMANCHE_H__
#define __COURTEMANCHE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(COURTEMANCHE_CPU_GENERATED)    || defined(COURTEMANCHE_MLIR_CPU_GENERATED)    || defined(COURTEMANCHE_MLIR_ROCM_GENERATED)    || defined(COURTEMANCHE_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define COURTEMANCHE_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define COURTEMANCHE_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define COURTEMANCHE_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define COURTEMANCHE_CPU_GENERATED
#endif

namespace limpet {

#define Courtemanche_REQDAT Vm_DATA_FLAG
#define Courtemanche_MODDAT Iion_DATA_FLAG

struct Courtemanche_Params {
    GlobalData_t ACh;
    GlobalData_t Cao;
    GlobalData_t Cm;
    GlobalData_t GACh;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GbCa;
    GlobalData_t GbNa;
    GlobalData_t Gto;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t factorGKur;
    GlobalData_t factorGrel;
    GlobalData_t factorGtr;
    GlobalData_t factorGup;
    GlobalData_t factorhGate;
    GlobalData_t factormGate;
    GlobalData_t factoroaGate;
    GlobalData_t factorxrGate;
    GlobalData_t maxCaup;
    GlobalData_t maxINaCa;
    GlobalData_t maxINaK;
    GlobalData_t maxIpCa;
    GlobalData_t maxIup;

};

struct Courtemanche_state {
    GlobalData_t Ca_rel;
    GlobalData_t Ca_up;
    GlobalData_t Cai;
    GlobalData_t Ki;
    Gatetype d;
    Gatetype f;
    Gatetype f_Ca;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype oa;
    Gatetype oi;
    Gatetype u;
    Gatetype ua;
    Gatetype ui;
    Gatetype v;
    Gatetype w;
    Gatetype xr;
    Gatetype xs;

};

class CourtemancheIonType : public IonType {
public:
    using IonIfDerived = IonIf<CourtemancheIonType>;
    using params_type = Courtemanche_Params;
    using state_type = Courtemanche_state;

    CourtemancheIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Courtemanche(int, int, IonIfBase&, GlobalData_t**);
#ifdef COURTEMANCHE_CPU_GENERATED
void compute_Courtemanche_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef COURTEMANCHE_MLIR_CPU_GENERATED
void compute_Courtemanche_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef COURTEMANCHE_MLIR_ROCM_GENERATED
void compute_Courtemanche_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef COURTEMANCHE_MLIR_CUDA_GENERATED
void compute_Courtemanche_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
