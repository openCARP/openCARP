// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: DeBruin, K.A., Krassowska, W
*  Year: 1998
*  Title: Electroporation and Shock-Induced Transmembrane Potential in a Cardiac Fiber During Defibrillation Strength Shocks
*  Journal: Annals of Biomedical Engineering 26, 584-596
*  DOI: 10.1114/1.101
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Electroporation_DeBruinKrassowska98.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Electroporation_DeBruinKrassowska98IonType::Electroporation_DeBruinKrassowska98IonType(bool plugin) : IonType(std::move(std::string("Electroporation_DeBruinKrassowska98")), plugin) {}

size_t Electroporation_DeBruinKrassowska98IonType::params_size() const {
  return sizeof(struct Electroporation_DeBruinKrassowska98_Params);
}

size_t Electroporation_DeBruinKrassowska98IonType::dlo_vector_size() const {

  return 1;
}

uint32_t Electroporation_DeBruinKrassowska98IonType::reqdat() const {
  return Electroporation_DeBruinKrassowska98_REQDAT;
}

uint32_t Electroporation_DeBruinKrassowska98IonType::moddat() const {
  return Electroporation_DeBruinKrassowska98_MODDAT;
}

void Electroporation_DeBruinKrassowska98IonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Electroporation_DeBruinKrassowska98IonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Electroporation_DeBruinKrassowska98IonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CUDA_GENERATED
      compute_Electroporation_DeBruinKrassowska98_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_ROCM_GENERATED)
      compute_Electroporation_DeBruinKrassowska98_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CPU_GENERATED)
      compute_Electroporation_DeBruinKrassowska98_mlir_cpu(start, end, imp, data);
#   elif defined(ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED)
      compute_Electroporation_DeBruinKrassowska98_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Electroporation_DeBruinKrassowska98IonType::compute."
#   endif
      break;
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Electroporation_DeBruinKrassowska98_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Electroporation_DeBruinKrassowska98_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Electroporation_DeBruinKrassowska98_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED
    case Target::CPU:
      compute_Electroporation_DeBruinKrassowska98_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define PI (GlobalData_t)(3.14159)
#define T (GlobalData_t)(310.0)
#define e (GlobalData_t)(1.6021765e-19)
#define k (GlobalData_t)(1.38066e-20)
#define nd_f (GlobalData_t)((e/(k*T)))



void Electroporation_DeBruinKrassowska98IonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Electroporation_DeBruinKrassowska98_Params *p = imp.params();

  // Compute the regional constants
  {
    p->N0 = 1.5e5;
    p->alpha = 200.0;
    p->beta = 6.25e-5;
    p->h = 5.0e-7;
    p->nn = 0.15;
    p->q = 2.46;
    p->sigma = 13.0;
    p->w0 = 5.25;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Electroporation_DeBruinKrassowska98IonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Electroporation_DeBruinKrassowska98_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double gp_f = (((PI*p->sigma)*p->h)*0.25);

}



void Electroporation_DeBruinKrassowska98IonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Electroporation_DeBruinKrassowska98_Params *p = imp.params();

  Electroporation_DeBruinKrassowska98_state *sv_base = (Electroporation_DeBruinKrassowska98_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double gp_f = (((PI*p->sigma)*p->h)*0.25);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Electroporation_DeBruinKrassowska98_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    double dvm_2 = (V*V);
    double n_init = (p->N0*(exp(((p->q*p->beta)*dvm_2))));
    sv->n = n_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED
extern "C" {
void compute_Electroporation_DeBruinKrassowska98_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Electroporation_DeBruinKrassowska98IonType::IonIfDerived& imp = static_cast<Electroporation_DeBruinKrassowska98IonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Electroporation_DeBruinKrassowska98_Params *p  = imp.params();
  Electroporation_DeBruinKrassowska98_state *sv_base = (Electroporation_DeBruinKrassowska98_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Electroporation_DeBruinKrassowska98IonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t gp_f = (((PI*p->sigma)*p->h)*0.25);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Electroporation_DeBruinKrassowska98_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t nd_vm = (V*nd_f);
    GlobalData_t nvm = (p->nn*nd_vm);
    GlobalData_t nvmm = (p->w0-(nvm));
    GlobalData_t nvmp = (p->w0+nvm);
    GlobalData_t gp = ((gp_f*((exp(nd_vm))-(1.)))/((((exp(nd_vm))*((p->w0*(exp(nvmm)))-(nvm)))/nvmm)-((((p->w0*(exp(nvmp)))+nvm)/nvmp))));
    Iion = (Iion+((gp*sv->n)*V));
    
    
    //Complete Forward Euler Update
    GlobalData_t dvm_2 = (V*V);
    GlobalData_t dN1 = (p->alpha*(exp((p->beta*dvm_2))));
    GlobalData_t dN2 = (((-dN1)/p->N0)*(exp((((-p->q)*p->beta)*dvm_2))));
    GlobalData_t diff_n = (dN1+(dN2*sv->n));
    GlobalData_t n_new = sv->n+diff_n*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->n = n_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // ELECTROPORATION_DEBRUINKRASSOWSKA98_CPU_GENERATED

bool Electroporation_DeBruinKrassowska98IonType::has_trace() const {
    return false;
}

void Electroporation_DeBruinKrassowska98IonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* Electroporation_DeBruinKrassowska98IonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Electroporation_DeBruinKrassowska98IonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        