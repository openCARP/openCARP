// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Inada S, Shibata Md PhD N, Iwata PhD M, Haraguchi PhD R, Ashihara Md PhD T, Ikeda Md PhD T, Mitsui PhD K, Dobrzynski PhD H, Boyett PhD MR, Nakazawa PhD K.
*  Year: 2017
*  Title: Simulation of ventricular rate control during atrial fibrillation using ionic channel blockers
*  Journal: J Arrhythm. 33(4):302-309
*  DOI: 10.1016/j.joa.2016.12.002
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __INADA_H__
#define __INADA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(INADA_CPU_GENERATED)    || defined(INADA_MLIR_CPU_GENERATED)    || defined(INADA_MLIR_ROCM_GENERATED)    || defined(INADA_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define INADA_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define INADA_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define INADA_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define INADA_CPU_GENERATED
#endif

namespace limpet {

#define Inada_REQDAT Vm_DATA_FLAG
#define Inada_MODDAT Iion_DATA_FLAG

struct Inada_Params {
    GlobalData_t Cm;
    GlobalData_t E_CaL;
    GlobalData_t E_b;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GNa;
    GlobalData_t Gb;
    GlobalData_t Gf;
    GlobalData_t Gst;
    GlobalData_t Gto;
    GlobalData_t cell_type;
    GlobalData_t kNaCa;
    GlobalData_t p_max;
    GlobalData_t p_rel;
    char* flags;

};
static const char* Inada_flags = "AN|N|NH";


struct Inada_state {
    GlobalData_t Cai;
    GlobalData_t Carel;
    GlobalData_t Casub;
    GlobalData_t Caup;
    GlobalData_t GNa;
    Gatetype d;
    Gatetype f1;
    Gatetype f2;
    GlobalData_t f_cmi;
    GlobalData_t f_cms;
    GlobalData_t f_cq;
    GlobalData_t f_csl;
    GlobalData_t f_tc;
    GlobalData_t f_tmc;
    GlobalData_t f_tmm;
    Gatetype h1;
    Gatetype h2;
    Gatetype m;
    Gatetype paf;
    Gatetype pas;
    Gatetype pi;
    Gatetype qa;
    Gatetype qi;
    Gatetype x;
    Gatetype y;
    Gatetype yf;
    Gatetype ys;

};

class InadaIonType : public IonType {
public:
    using IonIfDerived = IonIf<InadaIonType>;
    using params_type = Inada_Params;
    using state_type = Inada_state;

    InadaIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Inada(int, int, IonIfBase&, GlobalData_t**);
#ifdef INADA_CPU_GENERATED
void compute_Inada_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef INADA_MLIR_CPU_GENERATED
void compute_Inada_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef INADA_MLIR_ROCM_GENERATED
void compute_Inada_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef INADA_MLIR_CUDA_GENERATED
void compute_Inada_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
