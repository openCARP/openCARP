// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Jean-Pierre Drouhard, Fernand A. Roberge
*  Year: 1987
*  Title: Revised formulation of the Hodgkin-Huxley representation of the sodium current in cardiac cells
*  Journal: Computers and Biomedical Research, 20(4), 333-350
*  DOI: 10.1016/0010-4809(87)90048-6
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __DROUHARDROBERGE_H__
#define __DROUHARDROBERGE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(DROUHARDROBERGE_CPU_GENERATED)    || defined(DROUHARDROBERGE_MLIR_CPU_GENERATED)    || defined(DROUHARDROBERGE_MLIR_ROCM_GENERATED)    || defined(DROUHARDROBERGE_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define DROUHARDROBERGE_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define DROUHARDROBERGE_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define DROUHARDROBERGE_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define DROUHARDROBERGE_CPU_GENERATED
#endif

namespace limpet {

#define DrouhardRoberge_REQDAT Vm_DATA_FLAG
#define DrouhardRoberge_MODDAT Iion_DATA_FLAG

struct DrouhardRoberge_Params {
    GlobalData_t APDshorten;
    GlobalData_t GNa;
    GlobalData_t Gsi;

};

struct DrouhardRoberge_state {
    GlobalData_t Cai;
    Gatetype X;
    Gatetype d;
    Gatetype f;
    Gatetype h;
    Gatetype m;

};

class DrouhardRobergeIonType : public IonType {
public:
    using IonIfDerived = IonIf<DrouhardRobergeIonType>;
    using params_type = DrouhardRoberge_Params;
    using state_type = DrouhardRoberge_state;

    DrouhardRobergeIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_DrouhardRoberge(int, int, IonIfBase&, GlobalData_t**);
#ifdef DROUHARDROBERGE_CPU_GENERATED
void compute_DrouhardRoberge_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DROUHARDROBERGE_MLIR_CPU_GENERATED
void compute_DrouhardRoberge_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DROUHARDROBERGE_MLIR_ROCM_GENERATED
void compute_DrouhardRoberge_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DROUHARDROBERGE_MLIR_CUDA_GENERATED
void compute_DrouhardRoberge_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
