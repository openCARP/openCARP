// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Thomas R. Shannon, Fei Wang, Jose Puglisi, Christopher Weber, Donald M. Bers
*  Year: 2004
*  Title: A Mathematical Treatment of Integrated Ca Dynamics within the Ventricular Myocyte
*  Journal: Biophysical Journal, 87(5), 3351-3371
*  DOI: 10.1529/biophysj.104.047449
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __SHANNON_H__
#define __SHANNON_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(SHANNON_CPU_GENERATED)    || defined(SHANNON_MLIR_CPU_GENERATED)    || defined(SHANNON_MLIR_ROCM_GENERATED)    || defined(SHANNON_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define SHANNON_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define SHANNON_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define SHANNON_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define SHANNON_CPU_GENERATED
#endif

namespace limpet {

#define Shannon_REQDAT Vm_DATA_FLAG
#define Shannon_MODDAT Iion_DATA_FLAG

struct Shannon_Params {

};

struct Shannon_state {
    GlobalData_t Ca_Calmodulin;
    GlobalData_t Ca_Calsequestrin;
    GlobalData_t Ca_Myosin;
    GlobalData_t Ca_SL;
    GlobalData_t Ca_SLB_SL;
    GlobalData_t Ca_SLB_jct;
    GlobalData_t Ca_SLHigh_SL;
    GlobalData_t Ca_SLHigh_jct;
    GlobalData_t Ca_SR;
    GlobalData_t Ca_SRB;
    GlobalData_t Ca_TroponinC;
    GlobalData_t Ca_TroponinC_Ca_Mg;
    GlobalData_t Ca_jct;
    GlobalData_t Cai;
    GlobalData_t I;
    GlobalData_t MGMyosin;
    GlobalData_t MGTroponinC_Ca_Mg;
    GlobalData_t Na_SL;
    GlobalData_t Na_SL_buf;
    GlobalData_t Na_jct;
    GlobalData_t Na_jct_buf;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t R_cp0;
    Gatetype R_tos;
    Gatetype X_tof;
    Gatetype X_tos;
    Gatetype Xr;
    Gatetype Xs;
    Gatetype Y_tof;
    Gatetype Y_tos;
    Gatetype d;
    Gatetype f;
    GlobalData_t fCaB_SL;
    GlobalData_t fCaB_jct;
    Gatetype h;
    Gatetype j;
    Gatetype m;

};

    struct Shannon_Regional_Constants_cpu {
                    
    char dummy;

    };

    struct Shannon_Nodal_Req_cpu {
    
    GlobalData_t V;

    };

    struct Shannon_Private_cpu {
      using nodal_req_type = Shannon_Nodal_Req_cpu;
      using regional_constants_type = Shannon_Regional_Constants_cpu;
      IonIfBase *IF;
      int   node_number;
      void* cvode_mem;
      void* sunctx_ptr;
      bool  trace_init;
      regional_constants_type rc;
      nodal_req_type nr;
    };
    

//Printing out the Rosenbrock declarations
extern "C" {

void Shannon_rosenbrock_f_cpu(float*, float*, void*);

void Shannon_rosenbrock_jacobian_cpu(float**, float*, void*, int);

void rbStepX ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );
}

class ShannonIonType : public IonType {
public:
    using IonIfDerived = IonIf<ShannonIonType>;
    using params_type = Shannon_Params;
    using state_type = Shannon_state;

    using private_type = Shannon_Private_cpu;
    #ifdef SHANNON_MLIR_CPU_GENERATED
    using private_type_vector = Shannon_Private_mlir_cpu;
    #endif

    ShannonIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Shannon(int, int, IonIfBase&, GlobalData_t**);
#ifdef SHANNON_CPU_GENERATED
void compute_Shannon_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SHANNON_MLIR_CPU_GENERATED
void compute_Shannon_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SHANNON_MLIR_ROCM_GENERATED
void compute_Shannon_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SHANNON_MLIR_CUDA_GENERATED
void compute_Shannon_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
