// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Williams JC, Xu J, Lu Z, Klimas A, Chen X, Ambrosi CM, Cohen IS, Entcheva E.
*  Year: 2013
*  Title: Computational Optogenetics: Empirically-Derived Voltage- and Light-Sensitive Channelrhodopsin-2 Model
*  Journal: PLoS Comput Biol. 9(9):e1003220
*  DOI: 10.1371/journal.pcbi.1003220
*  Comment: Implementd by Boyle PM (pmjboyle@uw.edu), Williams JC, Ambrosi CM, Entcheva E, Trayanova NA descirbed in A comprehensive multiscale framework for simulating optogenetics in the heart
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ICHR2_WILLIAMSXU_H__
#define __ICHR2_WILLIAMSXU_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(ICHR2_WILLIAMSXU_CPU_GENERATED)    || defined(ICHR2_WILLIAMSXU_MLIR_CPU_GENERATED)    || defined(ICHR2_WILLIAMSXU_MLIR_ROCM_GENERATED)    || defined(ICHR2_WILLIAMSXU_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define ICHR2_WILLIAMSXU_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define ICHR2_WILLIAMSXU_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define ICHR2_WILLIAMSXU_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define ICHR2_WILLIAMSXU_CPU_GENERATED
#endif

namespace limpet {

#define IChR2_WilliamsXu_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG|illum_DATA_FLAG
#define IChR2_WilliamsXu_MODDAT Iion_DATA_FLAG

struct IChR2_WilliamsXu_Params {
    GlobalData_t Acell;
    GlobalData_t GChR2;
    GlobalData_t Gd2;
    GlobalData_t Temp;
    GlobalData_t attenuation;
    GlobalData_t c2;
    GlobalData_t e12_c1;
    GlobalData_t e12d;
    GlobalData_t e21_c1;
    GlobalData_t e21d;
    GlobalData_t gam;
    GlobalData_t lambda;
    GlobalData_t qeff1;
    GlobalData_t qeff2;
    GlobalData_t sigret;
    GlobalData_t tau_ChR2;
    GlobalData_t wloss;

};

struct IChR2_WilliamsXu_state {
    GlobalData_t C1;
    GlobalData_t C2;
    GlobalData_t GChR2;
    GlobalData_t O2;
    GlobalData_t attenuation;
    GlobalData_t pp;

};

class IChR2_WilliamsXuIonType : public IonType {
public:
    using IonIfDerived = IonIf<IChR2_WilliamsXuIonType>;
    using params_type = IChR2_WilliamsXu_Params;
    using state_type = IChR2_WilliamsXu_state;

    IChR2_WilliamsXuIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_IChR2_WilliamsXu(int, int, IonIfBase&, GlobalData_t**);
#ifdef ICHR2_WILLIAMSXU_CPU_GENERATED
void compute_IChR2_WilliamsXu_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ICHR2_WILLIAMSXU_MLIR_CPU_GENERATED
void compute_IChR2_WilliamsXu_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ICHR2_WILLIAMSXU_MLIR_ROCM_GENERATED
void compute_IChR2_WilliamsXu_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ICHR2_WILLIAMSXU_MLIR_CUDA_GENERATED
void compute_IChR2_WilliamsXu_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
