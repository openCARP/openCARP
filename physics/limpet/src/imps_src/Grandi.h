// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Grandi E, Pasqualini FS & Bers DM
*  Year: 2010
*  Title: A novel computationalmodel of the human ventricular action potential and Ca transient
*  Journal: JMol Cell Cardiol 48, 112-121
*  DOI: 10.1016/j.yjmcc.2009.09.019
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __GRANDI_H__
#define __GRANDI_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(GRANDI_CPU_GENERATED)    || defined(GRANDI_MLIR_CPU_GENERATED)    || defined(GRANDI_MLIR_ROCM_GENERATED)    || defined(GRANDI_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define GRANDI_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define GRANDI_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define GRANDI_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define GRANDI_CPU_GENERATED
#endif

namespace limpet {

#define Grandi_REQDAT Vm_DATA_FLAG
#define Grandi_MODDAT Iion_DATA_FLAG

struct Grandi_Params {
    GlobalData_t Cai_init;
    GlobalData_t Cao;
    GlobalData_t Cli;
    GlobalData_t Clo;
    GlobalData_t Ki_init;
    GlobalData_t Ko;
    GlobalData_t Mgi;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t cell_type;
    GlobalData_t markov_iks;
    char* flags;

};
static const char* Grandi_flags = "EPI|MCELL|ENDO";


struct Grandi_state {
    GlobalData_t C1;
    GlobalData_t C10;
    GlobalData_t C11;
    GlobalData_t C12;
    GlobalData_t C13;
    GlobalData_t C14;
    GlobalData_t C15;
    GlobalData_t C2;
    GlobalData_t C3;
    GlobalData_t C4;
    GlobalData_t C5;
    GlobalData_t C6;
    GlobalData_t C7;
    GlobalData_t C8;
    GlobalData_t C9;
    GlobalData_t CaM;
    GlobalData_t Ca_sr;
    GlobalData_t Cai;
    GlobalData_t Caj;
    GlobalData_t Casl;
    GlobalData_t Csqnb;
    GlobalData_t Ki;
    GlobalData_t Myoc;
    GlobalData_t Myom;
    GlobalData_t NaBj;
    GlobalData_t NaBsl;
    GlobalData_t Nai;
    GlobalData_t Naj;
    GlobalData_t Nasl;
    GlobalData_t O1;
    GlobalData_t RyRi;
    GlobalData_t RyRo;
    GlobalData_t RyRr;
    GlobalData_t SLHj;
    GlobalData_t SLHsl;
    GlobalData_t SLLj;
    GlobalData_t SLLsl;
    GlobalData_t SRB;
    GlobalData_t TnCHc;
    GlobalData_t TnCHm;
    GlobalData_t TnCL;
    Gatetype d;
    Gatetype f;
    GlobalData_t fcaBj;
    GlobalData_t fcaBsl;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype xkr;
    Gatetype xks;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;
    GlobalData_t __Cai_mM_local;

};

class GrandiIonType : public IonType {
public:
    using IonIfDerived = IonIf<GrandiIonType>;
    using params_type = Grandi_Params;
    using state_type = Grandi_state;

    GrandiIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Grandi(int, int, IonIfBase&, GlobalData_t**);
#ifdef GRANDI_CPU_GENERATED
void compute_Grandi_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GRANDI_MLIR_CPU_GENERATED
void compute_Grandi_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GRANDI_MLIR_ROCM_GENERATED
void compute_Grandi_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GRANDI_MLIR_CUDA_GENERATED
void compute_Grandi_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
