// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Steven Niederer, Peter Hunter, Nicholas Smith
*  Year: 2006
*  Title: A Quantitative Analysis of Cardiac Myocyte Relaxation: A Simulation Study
*  Journal: Biophysical Journal 2006;90:1697-1722
*  DOI: 10.1529/biophysj.105.069534
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Stress_Niederer.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Stress_NiedererIonType::Stress_NiedererIonType(bool plugin) : IonType(std::move(std::string("Stress_Niederer")), plugin) {}

size_t Stress_NiedererIonType::params_size() const {
  return sizeof(struct Stress_Niederer_Params);
}

size_t Stress_NiedererIonType::dlo_vector_size() const {

  return 1;
}

uint32_t Stress_NiedererIonType::reqdat() const {
  return Stress_Niederer_REQDAT;
}

uint32_t Stress_NiedererIonType::moddat() const {
  return Stress_Niederer_MODDAT;
}

void Stress_NiedererIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Stress_NiedererIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef STRESS_NIEDERER_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(STRESS_NIEDERER_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(STRESS_NIEDERER_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(STRESS_NIEDERER_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef STRESS_NIEDERER_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef STRESS_NIEDERER_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef STRESS_NIEDERER_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef STRESS_NIEDERER_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Stress_NiedererIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef STRESS_NIEDERER_MLIR_CUDA_GENERATED
      compute_Stress_Niederer_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(STRESS_NIEDERER_MLIR_ROCM_GENERATED)
      compute_Stress_Niederer_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(STRESS_NIEDERER_MLIR_CPU_GENERATED)
      compute_Stress_Niederer_mlir_cpu(start, end, imp, data);
#   elif defined(STRESS_NIEDERER_CPU_GENERATED)
      compute_Stress_Niederer_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Stress_NiedererIonType::compute."
#   endif
      break;
#   ifdef STRESS_NIEDERER_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Stress_Niederer_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_NIEDERER_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Stress_Niederer_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_NIEDERER_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Stress_Niederer_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_NIEDERER_CPU_GENERATED
    case Target::CPU:
      compute_Stress_Niederer_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Q_1_init (GlobalData_t)(0.)
#define Q_2_init (GlobalData_t)(0.)
#define Q_3_init (GlobalData_t)(0.)
#define TRPN_init (GlobalData_t)(0.067593139865)
#define dExtensionRatiodt (GlobalData_t)(0.)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define z_init (GlobalData_t)(0.014417937837)



void Stress_NiedererIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Stress_Niederer_Params *p = imp.params();

  // Compute the regional constants
  {
    p->A_1 = -29.;
    p->A_2 = 138.;
    p->A_3 = 129.;
    p->Ca_50ref = 1.05e-3;
    p->Ca_TRPN_Max = 70e-3;
    p->K_z = 0.15;
    p->T_ref = 56.2;
    p->a = 0.35;
    p->alpha_0 = 8e-3;
    p->alpha_1 = 0.03;
    p->alpha_2 = 0.13;
    p->alpha_3 = 0.625;
    p->alpha_r1 = 2e-3;
    p->alpha_r2 = 1.75e-3;
    p->beta_0 = 4.9;
    p->beta_1 = -4.;
    p->gamma_trpn = 2.;
    p->k_Ref_off = 0.2;
    p->k_on = 100.;
    p->n_Hill = 3.;
    p->n_Rel = 3.;
    p->z_p = 0.85;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Stress_NiedererIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Stress_Niederer_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  double K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));

}



void Stress_NiedererIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Niederer_Params *p = imp.params();

  Stress_Niederer_state *sv_base = (Stress_Niederer_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  double K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Stress_Niederer_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    Tension *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->Q_1 = Q_1_init;
    sv->Q_2 = Q_2_init;
    sv->Q_3 = Q_3_init;
    sv->TRPN = TRPN_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->z = z_init;
    //Change the units of external variables as appropriate.
    Tension *= 1e3;
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef STRESS_NIEDERER_CPU_GENERATED
extern "C" {
void compute_Stress_Niederer_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Stress_NiedererIonType::IonIfDerived& imp = static_cast<Stress_NiedererIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Niederer_Params *p  = imp.params();
  Stress_Niederer_state *sv_base = (Stress_Niederer_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Stress_NiedererIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  GlobalData_t K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __Cai_getfcn_exists = __Cai_SVgetfcn ? 1 : 0;
  int __Cai_putfcn_exists = __Cai_SVputfcn ? 1 : 0;
  
          char* __parent_table_address = (char*)imp.parent()->get_sv_address();
          int __parent_table_size = imp.parent()->get_sv_size() / 1;
      

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Stress_Niederer_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    Tension *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Q = ((sv->Q_1+sv->Q_2)+sv->Q_3);
    GlobalData_t lambda = length;
    GlobalData_t Ca_50 = (p->Ca_50ref*(1.+(p->beta_1*(lambda-(1.)))));
    GlobalData_t overlap = (1.+(p->beta_0*(lambda-(1.))));
    GlobalData_t Ca_TRPN_50 = ((Ca_50*p->Ca_TRPN_Max)/(Ca_50+((p->k_Ref_off/p->k_on)*(1.-((((1.+(p->beta_0*(lambda-(1.))))*0.5)/p->gamma_trpn))))));
    GlobalData_t z_max = (((p->alpha_0/(pow((Ca_TRPN_50/p->Ca_TRPN_Max),p->n_Hill)))-(K_2))/((p->alpha_r1+K_1)+(p->alpha_0/(pow((Ca_TRPN_50/p->Ca_TRPN_Max),p->n_Hill)))));
    GlobalData_t T_Base = ((p->T_ref*sv->z)/z_max);
    GlobalData_t T_0 = (T_Base*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    
    
    //Complete Forward Euler Update
    GlobalData_t Ca_b = (p->Ca_TRPN_Max-(sv->TRPN));
    GlobalData_t Cai_mM = (Cai/1000.);
    GlobalData_t beta_Tm = (p->alpha_r1+((p->alpha_r2*(pow(sv->z,(p->n_Rel-(1.)))))/((pow(sv->z,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
    GlobalData_t diff_Q_1 = ((p->A_1*dExtensionRatiodt)-((p->alpha_1*sv->Q_1)));
    GlobalData_t diff_Q_2 = ((p->A_2*dExtensionRatiodt)-((p->alpha_2*sv->Q_2)));
    GlobalData_t diff_Q_3 = ((p->A_3*dExtensionRatiodt)-((p->alpha_3*sv->Q_3)));
    GlobalData_t k_off = (((1.-((Tension/(p->gamma_trpn*p->T_ref))))>0.1) ? (p->k_Ref_off*(1.-((Tension/(p->gamma_trpn*p->T_ref))))) : (p->k_Ref_off*0.1));
    GlobalData_t J_TRPN = (((p->Ca_TRPN_Max-(sv->TRPN))*k_off)-(((Cai_mM*sv->TRPN)*p->k_on)));
    GlobalData_t alpha_Tm = (p->alpha_0*(pow((Ca_b/Ca_TRPN_50),p->n_Hill)));
    GlobalData_t diff_TRPN = J_TRPN;
    GlobalData_t diff_z = ((alpha_Tm*(1.-(sv->z)))-((beta_Tm*sv->z)));
    GlobalData_t Q_1_new = sv->Q_1+diff_Q_1*dt;
    GlobalData_t Q_2_new = sv->Q_2+diff_Q_2*dt;
    GlobalData_t Q_3_new = sv->Q_3+diff_Q_3*dt;
    GlobalData_t TRPN_new = sv->TRPN+diff_TRPN*dt;
    GlobalData_t z_new = sv->z+diff_z*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Q_1 = Q_1_new;
    sv->Q_2 = Q_2_new;
    sv->Q_3 = Q_3_new;
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->z = z_new;
    //Change the units of external variables as appropriate.
    Tension *= 1e3;
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;

  }

            }
}
#endif // STRESS_NIEDERER_CPU_GENERATED

bool Stress_NiedererIonType::has_trace() const {
    return true;
}

void Stress_NiedererIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Stress_Niederer_trace_header.txt","wt");
    fprintf(theader->fd,
        "Tension\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Niederer_Params *p  = imp.params();

  Stress_Niederer_state *sv_base = (Stress_Niederer_state *)imp.sv_tab().data();

  Stress_Niederer_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  GlobalData_t K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;
  //Initialize the external vars to their current values
  GlobalData_t Tension = Tension_ext[__i];
  GlobalData_t delta_sl = delta_sl_ext[__i];
  GlobalData_t length = length_ext[__i];
  GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
  //Change the units of external variables as appropriate.
  Tension *= 1e-3;
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", Tension);
  //Change the units of external variables as appropriate.
  Tension *= 1e3;
  
  

}
IonIfBase* Stress_NiedererIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Stress_NiedererIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        