// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Alan Fabbri, Matteo Fantini, Ronald Wilders, Stefano Severi
*  Year: 2017
*  Title: Computational analysis of the human sinus node action potential: model development and effects of mutations
*  Journal: The Journal of Physiology, 595.7 pp 2365-2396
*  DOI: 10.1113/JP273259
*  Comment: Original Fabbri model with modified isoprenaline formulation (changes affect ICaL and Casub) and the possibility to simulate with the adapted beta-adrenergic signalling cascade (Behar et al., 2016) and a new IKs current formulation (Verkerk and Wilders, 2023).
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Fabbri.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

FabbriIonType::FabbriIonType(bool plugin) : IonType(std::move(std::string("Fabbri")), plugin) {}

size_t FabbriIonType::params_size() const {
  return sizeof(struct Fabbri_Params);
}

size_t FabbriIonType::dlo_vector_size() const {

  return 1;
}

uint32_t FabbriIonType::reqdat() const {
  return Fabbri_REQDAT;
}

uint32_t FabbriIonType::moddat() const {
  return Fabbri_MODDAT;
}

void FabbriIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target FabbriIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef FABBRI_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(FABBRI_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(FABBRI_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(FABBRI_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef FABBRI_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef FABBRI_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef FABBRI_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef FABBRI_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void FabbriIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef FABBRI_MLIR_CUDA_GENERATED
      compute_Fabbri_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(FABBRI_MLIR_ROCM_GENERATED)
      compute_Fabbri_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(FABBRI_MLIR_CPU_GENERATED)
      compute_Fabbri_mlir_cpu(start, end, imp, data);
#   elif defined(FABBRI_CPU_GENERATED)
      compute_Fabbri_cpu(start, end, imp, data);
#   else
#     error "Could not generate method FabbriIonType::compute."
#   endif
      break;
#   ifdef FABBRI_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Fabbri_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef FABBRI_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Fabbri_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef FABBRI_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Fabbri_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef FABBRI_CPU_GENERATED
    case Target::CPU:
      compute_Fabbri_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define A2 (GlobalData_t)(0.07)
#define ATPi_max (GlobalData_t)(2.533)
#define Ca_jsr_init (GlobalData_t)(0.409551)
#define Ca_nsr_init (GlobalData_t)(0.435148)
#define Ca_sub_init (GlobalData_t)(6.226104e-5)
#define Cai_init (GlobalData_t)(9.15641e-6)
#define GNa_WT (GlobalData_t)(0.0223)
#define INa_WT_ratio (GlobalData_t)(0.5)
#define I_init (GlobalData_t)(4.595622e-10)
#define Iso_slope_cas_ICaL (GlobalData_t)(0.0)
#define KATP_min (GlobalData_t)(6034.)
#define K_05ICaL1 (GlobalData_t)(0.730287)
#define K_05ICaL2 (GlobalData_t)(0.661450)
#define K_05INaK (GlobalData_t)(0.719701)
#define K_05RyR (GlobalData_t)(0.682891)
#define K_05if (GlobalData_t)((17.8741/600.))
#define K_05iso (GlobalData_t)(58.57114132)
#define K_AC (GlobalData_t)(0.0735)
#define K_ACCa (GlobalData_t)(0.000024)
#define K_ACI (GlobalData_t)(0.016)
#define K_Ca (GlobalData_t)(0.000563995)
#define K_ICaL1 (GlobalData_t)(0.470657)
#define K_ICaL2 (GlobalData_t)(27.526226)
#define K_INaK (GlobalData_t)(0.435692)
#define K_iso (GlobalData_t)(0.007)
#define Ki_init (GlobalData_t)(140.)
#define Kif (GlobalData_t)(26.26)
#define Nai_init (GlobalData_t)(5.)
#define O_init (GlobalData_t)(6.181512e-9)
#define PKAtot (GlobalData_t)(1.)
#define PKItot (GlobalData_t)(0.3)
#define PLBp_init (GlobalData_t)(0.23)
#define PP1 (GlobalData_t)(0.89e-3)
#define RI_init (GlobalData_t)(0.069199)
#define R_1_init (GlobalData_t)(0.9308)
#define RyR_max (GlobalData_t)(0.02)
#define RyR_min (GlobalData_t)(0.0127)
#define V_R231C (GlobalData_t)(113.8797)
#define V_WT (GlobalData_t)(102.4597)
#define V_init (GlobalData_t)(-47.787168)
#define a_init (GlobalData_t)(0.00277)
#define cAMP_init (GlobalData_t)((19.73/600.))
#define cAMPb (GlobalData_t)((20./600.))
#define dL_init (GlobalData_t)(0.001921)
#define dT_init (GlobalData_t)(0.268909)
#define delta_m_WT (GlobalData_t)(1e-05)
#define fCMi_init (GlobalData_t)(0.217311)
#define fCMs_init (GlobalData_t)(0.158521)
#define fCQ_init (GlobalData_t)(0.138975)
#define fCa_init (GlobalData_t)(0.844449)
#define fL_init (GlobalData_t)(0.846702)
#define fTC_init (GlobalData_t)(0.017929)
#define fTMC_init (GlobalData_t)(0.259947)
#define fTMM_init (GlobalData_t)(0.653777)
#define fT_init (GlobalData_t)(0.020484)
#define g_ratio (GlobalData_t)(0.3153)
#define h_WT_init (GlobalData_t)(0.003058)
#define h_init (GlobalData_t)(0.003058)
#define kATP (GlobalData_t)(6142.)
#define kATP05 (GlobalData_t)(6724.)
#define kPKA (GlobalData_t)((9000./600.))
#define kPKA_PLB (GlobalData_t)(1.610336)
#define kPKA_cAMP (GlobalData_t)((284.5/600.))
#define kPLBp (GlobalData_t)(52.25)
#define kPP1 (GlobalData_t)(23.85e3)
#define kPP1_PLB (GlobalData_t)(0.07457)
#define k_R231C (GlobalData_t)(-34.9006)
#define k_WT (GlobalData_t)(55.2060)
#define koCa_max (GlobalData_t)(10000.0)
#define m_WT_init (GlobalData_t)(0.447724)
#define m_init (GlobalData_t)(0.447724)
#define nATP (GlobalData_t)(3.36)
#define nPKA (GlobalData_t)(5.)
#define nPLB (GlobalData_t)(1.)
#define nRyR (GlobalData_t)(9.773)
#define n_init (GlobalData_t)(0.1162)
#define nif (GlobalData_t)(9.281)
#define niso (GlobalData_t)(0.9264)
#define paF_init (GlobalData_t)(0.011068)
#define paS_init (GlobalData_t)(0.283185)
#define piy_init (GlobalData_t)(0.709051)
#define q_init (GlobalData_t)(0.430836)
#define r_Kur_init (GlobalData_t)(0.011845)
#define r_init (GlobalData_t)(0.014523)
#define s_Kur_init (GlobalData_t)(0.845304)
#define x_init (GlobalData_t)(0.0685782388661923)
#define y_init (GlobalData_t)(0.009508)



void FabbriIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Fabbri_Params *p = imp.params();

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->ACh_on = 0.0;
    p->C = 57.;
    p->CCh_cas = 0.0;
    p->CM_tot = 0.045;
    p->CQ_tot = 10.0;
    p->Ca_intracellular_fluxes_tau_dif_Ca = 5.469e-05;
    p->Ca_intracellular_fluxes_tau_tr = 0.04;
    p->Cao = 1.8;
    p->EC50_SK = 0.7;
    p->EC50_SR = 0.45;
    p->F = 9.64853414999999950e+04;
    p->GKACh = 0.00345;
    p->GKr_max = 0.00424;
    p->GKs_max = 6.5e-04;
    p->GKur_max = 1.539e-04;
    p->GNa_max = 0.0223;
    p->GSK = 0.;
    p->Gf_K_max = 0.00268;
    p->Gf_Na_max = 0.00159;
    p->Gto_max = 0.0035;
    p->HSR = 2.5;
    p->ICaL_fCa_gate_alpha_fCa = 0.0075;
    p->INaK_max = 0.08105;
    p->Iso_1_uM = 0.0;
    p->Iso_cas = -1.0;
    p->Iso_linear = 0.0;
    p->K1ni = 395.3;
    p->K1no = 1628.0;
    p->K2ni = 2.289;
    p->K2no = 561.4;
    p->K3ni = 26.44;
    p->K3no = 4.663;
    p->K_NaCa = 3.343;
    p->K_iso_increase = 1.0;
    p->K_iso_shift = 1.0;
    p->K_iso_shift_ninf = 1.0;
    p->K_iso_slope_dL = 1.0;
    p->K_j_SRCarel = 1.0;
    p->K_j_up = 1.0;
    p->K_k1 = 4.64625;
    p->K_up = 2.86113000000000003e-04;
    p->Kci = 0.0207;
    p->Kcni = 26.44;
    p->Kco = 3.663;
    p->Km_Kp = 1.4;
    p->Km_Nap = 14.0;
    p->Km_fCa = 0.000338;
    p->Ko = 5.4;
    p->L_cell = 67.0;
    p->L_sub = 0.02;
    p->MaxSR = 15.0;
    p->Mgi = 2.5;
    p->MinSR = 1.0;
    p->Nao = 140.0;
    p->P_CaL = 0.4578;
    p->P_CaT = 0.04132;
    p->P_up_basal = 5.0;
    p->Qci = 0.1369;
    p->Qco = 0.0;
    p->Qn = 0.4315;
    p->R231C_on = 0.;
    p->R_2 = 8314.472;
    p->R_cell = 3.9;
    p->T = 310.0;
    p->TC_tot = 0.031;
    p->TMC_tot = 0.062;
    p->VW_IKs = 0.;
    p->V_dL = -16.45085327;
    p->V_fL = -37.4;
    p->V_i_part = 0.46;
    p->V_jsr_part = 0.0012;
    p->V_nsr_part = 0.0116;
    GlobalData_t cAMP = cAMP_init;
    p->delta_m = 1e-05;
    p->dynamic_Ki_Nai = 0.;
    p->h_inf_shift = 0.0;
    p->h_inf_slope = 1.;
    p->h_tau_gain = 1.0;
    p->k_dL = 4.337;
    p->k_fL = 5.3;
    p->kb_CM = 542.0;
    p->kb_CQ = 445.0;
    p->kb_TC = 446.0;
    p->kb_TMC = 7.51;
    p->kb_TMM = 751.0;
    p->kf_CM = 1641986.0;
    p->kf_CQ = 175.4;
    p->kf_TC = 88800.0;
    p->kf_TMC = 227700.0;
    p->kf_TMM = 2277.0;
    p->kiCa = 500.0;
    p->kim = 5.0;
    p->kom = 660.0;
    p->ks = 1.48041085099999994e+08;
    p->m_inf_shift = 0.;
    p->m_inf_slope = 1.;
    p->m_tau_shift = 0.;
    p->n_SK = 2.2;
    p->n_inf_shift = 0.0;
    p->n_shift_slope = 1.;
    p->offset_fT = 0.0;
    p->ratio_INaL_INa = 0.;
    p->shift_fL = 0.0;
    p->slope_up = 5e-05;
    p->tau_y_a_shift = 0.;
    p->tau_y_b_shift = 0.;
    p->y_shift = 0.0;
    GlobalData_t PKA = (((cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-cAMP)*600.)*0.03395094))))+0.99221714));
    p->RTONF = ((p->R_2*p->T)/p->F);
    p->V_cell = (((1e-09*3.14159265358979312e+00)*(pow(p->R_cell,2.0)))*p->L_cell);
    p->V_sub = (((((1e-09*2.0)*3.14159265358979312e+00)*p->L_sub)*(p->R_cell-((p->L_sub/2.0))))*p->L_cell);
    p->k34 = (p->Nao/(p->K3no+p->Nao));
    p->V_i = ((p->V_i_part*p->V_cell)-(p->V_sub));
    p->V_jsr = (p->V_jsr_part*p->V_cell);
    p->V_nsr = (p->V_nsr_part*p->V_cell);
    p->koCa = ((p->Iso_cas>-0.1) ? (koCa_max*((RyR_min-(((RyR_max*(pow(PKA,nRyR)))/((pow(K_05RyR,nRyR))+(pow(PKA,nRyR))))))+1.)) : 10000.0);
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void FabbriIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Fabbri_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+9e-05));
  double ACh_shift = ((p->ACh>0.0) ? (-1.0-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+1.22422999999999998e-03)))) : 0.0);
  double Ca_intracellular_fluxes_b_up = ((p->Iso_1_uM>0.0) ? (-0.25*p->K_iso_shift) : ((p->ACh>0.0) ? ((0.7*p->ACh)/(9e-05+p->ACh)) : 0.0));
  double GKr = (p->GKr_max*(sqrt((p->Ko/5.4))));
  double GNa_L = (p->GNa_max*p->ratio_INaL_INa);
  double Iso_increase_1 = ((p->Iso_linear>0.0) ? (1.0+(0.23*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.23*p->K_iso_increase) : 1.0));
  double Iso_increase_2 = ((p->Iso_linear>0.0) ? (1.0+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.2*p->K_iso_increase) : 1.0));
  double Iso_shift_1 = ((p->Iso_1_uM>0.0) ? (-14.0*p->K_iso_shift) : 0.0);
  double Iso_shift_2 = ((p->Iso_1_uM>0.0) ? (7.5*p->K_iso_shift) : 0.0);
  double Iso_shift_dL = ((p->Iso_1_uM>0.0) ? (-8.0*p->K_iso_shift) : 0.0);
  double Iso_shift_ninf = ((p->Iso_1_uM>0.0) ? (14.568*p->K_iso_shift_ninf) : 0.0);
  double Iso_slope_dL = ((p->Iso_1_uM>0.0) ? (-27.0*p->K_iso_slope_dL) : 0.0);
  double alpha_a = ((((3.5988-(0.025641))/(1.0+(1.21550000000000005e-06/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.0);
  double kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  double kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  double P_up = (p->P_up_basal*(1.0-(Ca_intracellular_fluxes_b_up)));

}



void FabbriIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Fabbri_Params *p = imp.params();

  Fabbri_state *sv_base = (Fabbri_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+9e-05));
  double ACh_shift = ((p->ACh>0.0) ? (-1.0-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+1.22422999999999998e-03)))) : 0.0);
  double Ca_intracellular_fluxes_b_up = ((p->Iso_1_uM>0.0) ? (-0.25*p->K_iso_shift) : ((p->ACh>0.0) ? ((0.7*p->ACh)/(9e-05+p->ACh)) : 0.0));
  double GKr = (p->GKr_max*(sqrt((p->Ko/5.4))));
  double GNa_L = (p->GNa_max*p->ratio_INaL_INa);
  double Iso_increase_1 = ((p->Iso_linear>0.0) ? (1.0+(0.23*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.23*p->K_iso_increase) : 1.0));
  double Iso_increase_2 = ((p->Iso_linear>0.0) ? (1.0+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.2*p->K_iso_increase) : 1.0));
  double Iso_shift_1 = ((p->Iso_1_uM>0.0) ? (-14.0*p->K_iso_shift) : 0.0);
  double Iso_shift_2 = ((p->Iso_1_uM>0.0) ? (7.5*p->K_iso_shift) : 0.0);
  double Iso_shift_dL = ((p->Iso_1_uM>0.0) ? (-8.0*p->K_iso_shift) : 0.0);
  double Iso_shift_ninf = ((p->Iso_1_uM>0.0) ? (14.568*p->K_iso_shift_ninf) : 0.0);
  double Iso_slope_dL = ((p->Iso_1_uM>0.0) ? (-27.0*p->K_iso_slope_dL) : 0.0);
  double alpha_a = ((((3.5988-(0.025641))/(1.0+(1.21550000000000005e-06/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.0);
  double kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  double kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  double P_up = (p->P_up_basal*(1.0-(Ca_intracellular_fluxes_b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Fabbri_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_jsr = Ca_jsr_init;
    sv->Ca_nsr = Ca_nsr_init;
    sv->Ca_sub = Ca_sub_init;
    sv->Cai = Cai_init;
    sv->I = I_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->O = O_init;
    sv->PLBp = PLBp_init;
    sv->RI = RI_init;
    sv->R_1 = R_1_init;
    V = V_init;
    sv->a = a_init;
    sv->cAMP = cAMP_init;
    sv->dL = dL_init;
    sv->dT = dT_init;
    sv->fCMi = fCMi_init;
    sv->fCMs = fCMs_init;
    sv->fCQ = fCQ_init;
    sv->fCa = fCa_init;
    sv->fL = fL_init;
    sv->fT = fT_init;
    sv->fTC = fTC_init;
    sv->fTMC = fTMC_init;
    sv->fTMM = fTMM_init;
    sv->h = h_init;
    sv->h_WT = h_WT_init;
    sv->m = m_init;
    sv->m_WT = m_WT_init;
    sv->n = n_init;
    sv->paF = paF_init;
    sv->paS = paS_init;
    sv->piy = piy_init;
    sv->q = q_init;
    sv->r = r_init;
    sv->r_Kur = r_Kur_init;
    sv->s_Kur = s_Kur_init;
    sv->x = x_init;
    sv->y = y_init;
    double E_K = (p->RTONF*(log((p->Ko/sv->Ki))));
    double E_Ks = ((p->VW_IKs==0.) ? (p->RTONF*(log(((p->Ko+(0.12*p->Nao))/(sv->Ki+(0.12*sv->Nai)))))) : (p->RTONF*(log(((p->Ko+(0.0018*p->Nao))/(sv->Ki+(0.0018*sv->Nai)))))));
    double E_Na = (p->RTONF*(log((p->Nao/sv->Nai))));
    double E_mh = (p->RTONF*(log(((p->Nao+(0.12*p->Ko))/(sv->Nai+(0.12*sv->Ki))))));
    double ICaT = ((((((2.0*p->P_CaT)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dT)*sv->fT);
    double PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
    double R231C = ((p->R231C_on!=0.) ? ((g_ratio*(pow((1./(1.+(exp(((-(V-(V_R231C)))/k_R231C))))),2.)))/(pow((((1.-(A2))/(1.+(exp(((-(V-(V_WT)))/k_WT)))))+A2),2.))) : 1.);
    double di = ((1.0+((sv->Ca_sub/p->Kci)*((1.0+(exp((((-p->Qci)*V)/p->RTONF))))+(sv->Nai/p->Kcni))))+((sv->Nai/p->K1ni)*(1.0+((sv->Nai/p->K2ni)*(1.0+(sv->Nai/p->K3ni))))));
    double do_ = ((1.0+((p->Cao/p->Kco)*(1.0+(exp(((p->Qco*V)/p->RTONF))))))+((p->Nao/p->K1no)*(1.0+((p->Nao/p->K2no)*(1.0+(p->Nao/p->K3no))))));
    double k32 = (exp(((p->Qn*V)/(2.0*p->RTONF))));
    double k41 = (exp((((-p->Qn)*V)/(2.0*p->RTONF))));
    double k43 = (sv->Nai/(p->K3ni+sv->Nai));
    double IKACh = ((p->ACh>0.0) ? ((((p->ACh_on*p->GKACh)*(V-(E_K)))*(1.0+(exp(((V+20.0)/20.0)))))*sv->a) : 0.0);
    double IKr = (((GKr*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    double IKur = (((p->GKur_max*sv->r_Kur)*sv->s_Kur)*(V-(E_K)));
    double INa_ = (((p->GNa_max*(pow(sv->m,3.0)))*sv->h)*(V-(E_mh)));
    double INa_L = ((GNa_L*(pow(sv->m,3.0)))*(V-(E_mh)));
    double INa_WT = (((GNa_WT*(pow(sv->m_WT,3.0)))*sv->h_WT)*(V-(E_mh)));
    double ISK = ((p->GSK*(V-(E_K)))*sv->x);
    double IfK = ((sv->y*p->Gf_K_max)*(V-(E_K)));
    double IfNa = ((sv->y*p->Gf_Na_max)*(V-(E_Na)));
    double Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808))))) : 0.);
    double Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? ((p->VW_IKs==0.0) ? (-0.2152+((0.435692*(pow(PKA,10.0808)))/((pow(0.719701,10.0808))+(pow(PKA,10.0808))))) : (-0.2152+((0.494259*(pow(PKA,10.0808)))/((pow(0.736717,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    double Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808))))) : 0.);
    double Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
    double k12 = (((sv->Ca_sub/p->Kci)*(exp((((-p->Qci)*V)/p->RTONF))))/di);
    double k14 = ((((((sv->Nai/p->K1ni)*sv->Nai)/p->K2ni)*(1.0+(sv->Nai/p->K3ni)))*(exp(((p->Qn*V)/(2.0*p->RTONF)))))/di);
    double k21 = (((p->Cao/p->Kco)*(exp(((p->Qco*V)/p->RTONF))))/do_);
    double k23 = ((((((p->Nao/p->K1no)*p->Nao)/p->K2no)*(1.0+(p->Nao/p->K3no)))*(exp((((-p->Qn)*V)/(2.0*p->RTONF)))))/do_);
    double GKs = ((p->VW_IKs==0.) ? (p->GKs_max*((p->Iso_linear>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))) : ((0.2*p->GKs_max)*((p->Iso_linear>0.0) ? (1.0+(0.25*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.25*p->K_iso_increase) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))));
    double INa = (((1.-(INa_WT_ratio))*(INa_+INa_L))+(INa_WT_ratio*INa_WT));
    double INaK = ((((((1.0+Iso_inc_cas_INaK)*Iso_increase_2)*p->INaK_max)*(pow((1.0+(pow((p->Km_Kp/p->Ko),1.2))),-1.0)))*(pow((1.0+(pow((p->Km_Nap/sv->Nai),1.3))),-1.0)))*(pow((1.0+(exp(((-((V-(E_Na))+110.0))/20.0)))),-1.0)));
    double If = (IfNa+IfK);
    double IsiCa = ((((((((((2.0*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiK = ((((((((((0.000365*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Ki-((p->Ko*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiNa = ((((((((((1.85e-05*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Nai-((p->Nao*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double x1 = (((k41*p->k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    double x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(p->k34+k32)));
    double x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    double x4 = (((k23*p->k34)*(k14+k12))+((k14*k21)*(p->k34+k32)));
    double ICaL = ((IsiCa+IsiK)+IsiNa);
    double IKs = (((R231C*GKs)*(V-(E_Ks)))*(pow(sv->n,2.0)));
    double INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    double Itot = (((((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh)+IKur)+ISK);
    Iion = ((Itot*1000.)/p->C);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef FABBRI_CPU_GENERATED
extern "C" {
void compute_Fabbri_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  FabbriIonType::IonIfDerived& imp = static_cast<FabbriIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Fabbri_Params *p  = imp.params();
  Fabbri_state *sv_base = (Fabbri_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  FabbriIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+9e-05));
  GlobalData_t ACh_shift = ((p->ACh>0.0) ? (-1.0-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+1.22422999999999998e-03)))) : 0.0);
  GlobalData_t Ca_intracellular_fluxes_b_up = ((p->Iso_1_uM>0.0) ? (-0.25*p->K_iso_shift) : ((p->ACh>0.0) ? ((0.7*p->ACh)/(9e-05+p->ACh)) : 0.0));
  GlobalData_t GKr = (p->GKr_max*(sqrt((p->Ko/5.4))));
  GlobalData_t GNa_L = (p->GNa_max*p->ratio_INaL_INa);
  GlobalData_t Iso_increase_1 = ((p->Iso_linear>0.0) ? (1.0+(0.23*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.23*p->K_iso_increase) : 1.0));
  GlobalData_t Iso_increase_2 = ((p->Iso_linear>0.0) ? (1.0+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.2*p->K_iso_increase) : 1.0));
  GlobalData_t Iso_shift_1 = ((p->Iso_1_uM>0.0) ? (-14.0*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_2 = ((p->Iso_1_uM>0.0) ? (7.5*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_dL = ((p->Iso_1_uM>0.0) ? (-8.0*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_ninf = ((p->Iso_1_uM>0.0) ? (14.568*p->K_iso_shift_ninf) : 0.0);
  GlobalData_t Iso_slope_dL = ((p->Iso_1_uM>0.0) ? (-27.0*p->K_iso_slope_dL) : 0.0);
  GlobalData_t alpha_a = ((((3.5988-(0.025641))/(1.0+(1.21550000000000005e-06/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.0);
  GlobalData_t kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  GlobalData_t kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  GlobalData_t P_up = (p->P_up_basal*(1.0-(Ca_intracellular_fluxes_b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Fabbri_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_K = (p->RTONF*(log((p->Ko/sv->Ki))));
    GlobalData_t E_Ks = ((p->VW_IKs==0.) ? (p->RTONF*(log(((p->Ko+(0.12*p->Nao))/(sv->Ki+(0.12*sv->Nai)))))) : (p->RTONF*(log(((p->Ko+(0.0018*p->Nao))/(sv->Ki+(0.0018*sv->Nai)))))));
    GlobalData_t E_Na = (p->RTONF*(log((p->Nao/sv->Nai))));
    GlobalData_t E_mh = (p->RTONF*(log(((p->Nao+(0.12*p->Ko))/(sv->Nai+(0.12*sv->Ki))))));
    GlobalData_t ICaT = ((((((2.0*p->P_CaT)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dT)*sv->fT);
    GlobalData_t PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
    GlobalData_t R231C = ((p->R231C_on!=0.) ? ((g_ratio*(pow((1./(1.+(exp(((-(V-(V_R231C)))/k_R231C))))),2.)))/(pow((((1.-(A2))/(1.+(exp(((-(V-(V_WT)))/k_WT)))))+A2),2.))) : 1.);
    GlobalData_t di = ((1.0+((sv->Ca_sub/p->Kci)*((1.0+(exp((((-p->Qci)*V)/p->RTONF))))+(sv->Nai/p->Kcni))))+((sv->Nai/p->K1ni)*(1.0+((sv->Nai/p->K2ni)*(1.0+(sv->Nai/p->K3ni))))));
    GlobalData_t do_ = ((1.0+((p->Cao/p->Kco)*(1.0+(exp(((p->Qco*V)/p->RTONF))))))+((p->Nao/p->K1no)*(1.0+((p->Nao/p->K2no)*(1.0+(p->Nao/p->K3no))))));
    GlobalData_t k32 = (exp(((p->Qn*V)/(2.0*p->RTONF))));
    GlobalData_t k41 = (exp((((-p->Qn)*V)/(2.0*p->RTONF))));
    GlobalData_t k43 = (sv->Nai/(p->K3ni+sv->Nai));
    GlobalData_t IKACh = ((p->ACh>0.0) ? ((((p->ACh_on*p->GKACh)*(V-(E_K)))*(1.0+(exp(((V+20.0)/20.0)))))*sv->a) : 0.0);
    GlobalData_t IKr = (((GKr*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    GlobalData_t IKur = (((p->GKur_max*sv->r_Kur)*sv->s_Kur)*(V-(E_K)));
    GlobalData_t INa_ = (((p->GNa_max*(pow(sv->m,3.0)))*sv->h)*(V-(E_mh)));
    GlobalData_t INa_L = ((GNa_L*(pow(sv->m,3.0)))*(V-(E_mh)));
    GlobalData_t INa_WT = (((GNa_WT*(pow(sv->m_WT,3.0)))*sv->h_WT)*(V-(E_mh)));
    GlobalData_t ISK = ((p->GSK*(V-(E_K)))*sv->x);
    GlobalData_t IfK = ((sv->y*p->Gf_K_max)*(V-(E_K)));
    GlobalData_t IfNa = ((sv->y*p->Gf_Na_max)*(V-(E_Na)));
    GlobalData_t Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808))))) : 0.);
    GlobalData_t Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? ((p->VW_IKs==0.0) ? (-0.2152+((0.435692*(pow(PKA,10.0808)))/((pow(0.719701,10.0808))+(pow(PKA,10.0808))))) : (-0.2152+((0.494259*(pow(PKA,10.0808)))/((pow(0.736717,10.0808))+(pow(PKA,10.0808)))))) : 0.);
    GlobalData_t Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808))))) : 0.);
    GlobalData_t Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
    GlobalData_t k12 = (((sv->Ca_sub/p->Kci)*(exp((((-p->Qci)*V)/p->RTONF))))/di);
    GlobalData_t k14 = ((((((sv->Nai/p->K1ni)*sv->Nai)/p->K2ni)*(1.0+(sv->Nai/p->K3ni)))*(exp(((p->Qn*V)/(2.0*p->RTONF)))))/di);
    GlobalData_t k21 = (((p->Cao/p->Kco)*(exp(((p->Qco*V)/p->RTONF))))/do_);
    GlobalData_t k23 = ((((((p->Nao/p->K1no)*p->Nao)/p->K2no)*(1.0+(p->Nao/p->K3no)))*(exp((((-p->Qn)*V)/(2.0*p->RTONF)))))/do_);
    GlobalData_t GKs = ((p->VW_IKs==0.) ? (p->GKs_max*((p->Iso_linear>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))) : ((0.2*p->GKs_max)*((p->Iso_linear>0.0) ? (1.0+(0.25*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.25*p->K_iso_increase) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))));
    GlobalData_t INa = (((1.-(INa_WT_ratio))*(INa_+INa_L))+(INa_WT_ratio*INa_WT));
    GlobalData_t INaK = ((((((1.0+Iso_inc_cas_INaK)*Iso_increase_2)*p->INaK_max)*(pow((1.0+(pow((p->Km_Kp/p->Ko),1.2))),-1.0)))*(pow((1.0+(pow((p->Km_Nap/sv->Nai),1.3))),-1.0)))*(pow((1.0+(exp(((-((V-(E_Na))+110.0))/20.0)))),-1.0)));
    GlobalData_t If = (IfNa+IfK);
    GlobalData_t IsiCa = ((((((((((2.0*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiK = ((((((((((0.000365*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Ki-((p->Ko*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiNa = ((((((((((1.85e-05*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Nai-((p->Nao*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t x1 = (((k41*p->k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    GlobalData_t x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(p->k34+k32)));
    GlobalData_t x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    GlobalData_t x4 = (((k23*p->k34)*(k14+k12))+((k14*k21)*(p->k34+k32)));
    GlobalData_t ICaL = ((IsiCa+IsiK)+IsiNa);
    GlobalData_t IKs = (((R231C*GKs)*(V-(E_Ks)))*(pow(sv->n,2.0)));
    GlobalData_t INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    GlobalData_t Itot = (((((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh)+IKur)+ISK);
    Iion = ((Itot*1000.)/p->C);
    
    
    //Complete Forward Euler Update
    GlobalData_t ATPi = ((ATPi_max*(((kATP*(pow(((sv->cAMP*100.)/cAMPb),nATP)))/(kATP05+(pow(((sv->cAMP*100.)/cAMPb),nATP))))-(KATP_min)))/100.);
    GlobalData_t F_PLBp = ((sv->PLBp>0.23) ? ((3.3931*(pow(sv->PLBp,4.0695)))/((pow(0.2805,4.0695))+(pow(sv->PLBp,4.0695)))) : ((1.698*(pow(sv->PLBp,13.5842)))/((pow(0.2240,13.5842))+(pow(sv->PLBp,13.5842)))));
    GlobalData_t ISK_x_gate_tau_x = (1.0/(((0.047*sv->Ca_sub)*1e3)+(1.0/76.0)));
    GlobalData_t delta_fCMi = (((p->kf_CM*sv->Cai)*(1.0-(sv->fCMi)))-((p->kb_CM*sv->fCMi)));
    GlobalData_t delta_fCMs = (((p->kf_CM*sv->Ca_sub)*(1.0-(sv->fCMs)))-((p->kb_CM*sv->fCMs)));
    GlobalData_t delta_fCQ = (((p->kf_CQ*sv->Ca_jsr)*(1.0-(sv->fCQ)))-((p->kb_CQ*sv->fCQ)));
    GlobalData_t delta_fTC = (((p->kf_TC*sv->Cai)*(1.0-(sv->fTC)))-((p->kb_TC*sv->fTC)));
    GlobalData_t delta_fTMC = (((p->kf_TMC*sv->Cai)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMC*sv->fTMC)));
    GlobalData_t delta_fTMM = (((p->kf_TMM*p->Mgi)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMM*sv->fTMM)));
    GlobalData_t diff_Ki = ((p->dynamic_Ki_Nai==1.) ? (((-1.0*(((((((IKur+Ito)+IKr)+IKs)+IfK)+IsiK)+ISK)-((2.0*INaK))))/((p->V_i+p->V_sub)*p->F))*0.001) : 0.);
    GlobalData_t diff_Nai = ((p->dynamic_Ki_Nai==1.) ? (((-1.0*((((INa+IfNa)+IsiNa)+(3.0*INaK))+(3.0*INaCa)))/((p->V_i+p->V_sub)*p->F))*0.001) : 0.);
    GlobalData_t fCa_infinity = (p->Km_fCa/(p->Km_fCa+sv->Ca_sub));
    GlobalData_t j_Ca_dif = ((sv->Ca_sub-(sv->Cai))/p->Ca_intracellular_fluxes_tau_dif_Ca);
    GlobalData_t j_SRCarel = (((p->K_j_SRCarel*p->ks)*sv->O)*(sv->Ca_jsr-(sv->Ca_sub)));
    GlobalData_t j_tr = ((sv->Ca_nsr-(sv->Ca_jsr))/p->Ca_intracellular_fluxes_tau_tr);
    GlobalData_t k1 = (p->K_k1*(K_ACI+(K_AC/(1.0+(exp(((K_Ca-((p->kb_CM*(sv->fCMi/(p->kf_CM*(1.-(sv->fCMi)))))))/K_ACCa)))))));
    GlobalData_t k2 = ((1.1*237.9851)*((pow((sv->cAMP*600.),5.101))/((pow(20.1077,6.101))+(pow((sv->cAMP*600.),6.101)))));
    GlobalData_t k3 = (kPKA*((pow(sv->cAMP,(nPKA-(1.))))/((pow(kPKA_cAMP,nPKA))+(pow(sv->cAMP,nPKA)))));
    GlobalData_t k4 = ((kPLBp*(pow(PKA,nPLB)))/((pow(kPKA_PLB,nPLB))+(pow(PKA,nPLB))));
    GlobalData_t k5 = (kPP1*((PP1*sv->PLBp)/(kPP1_PLB+sv->PLBp)));
    GlobalData_t kCaSR = (p->MaxSR-(((p->MaxSR-(p->MinSR))/(1.0+(pow((p->EC50_SR/sv->Ca_jsr),p->HSR))))));
    GlobalData_t x_infinity = ((0.81*(pow((sv->Ca_sub*1e3),p->n_SK)))/((pow((sv->Ca_sub*1e3),p->n_SK))+(pow(p->EC50_SK,p->n_SK))));
    GlobalData_t ICaL_fCa_gate_tau_fCa = ((0.001*fCa_infinity)/p->ICaL_fCa_gate_alpha_fCa);
    GlobalData_t diff_Ca_jsr = ((j_tr-((j_SRCarel+(p->CQ_tot*delta_fCQ))))*0.001);
    GlobalData_t diff_Ca_sub = ((((j_SRCarel*p->V_jsr)/p->V_sub)-((((((IsiCa+ICaT)-((2.0*INaCa)))/((2.0*p->F)*p->V_sub))+j_Ca_dif)+(p->CM_tot*delta_fCMs))))*0.001);
    GlobalData_t diff_PLBp = ((k4-(k5))/60000.);
    GlobalData_t diff_cAMP = ((((((kiso-(kcch))*ATPi)+(k1*ATPi))-((k2*sv->cAMP)))-((k3*sv->cAMP)))/60000.);
    GlobalData_t diff_fCMi = (delta_fCMi*0.001);
    GlobalData_t diff_fCMs = (delta_fCMs*0.001);
    GlobalData_t diff_fCQ = (delta_fCQ*0.001);
    GlobalData_t diff_fTC = (delta_fTC*0.001);
    GlobalData_t diff_fTMC = (delta_fTMC*0.001);
    GlobalData_t diff_fTMM = (delta_fTMM*0.001);
    GlobalData_t diff_x = ((x_infinity-(sv->x))/ISK_x_gate_tau_x);
    GlobalData_t j_up = ((p->Iso_cas>-0.1) ? (p->K_j_up*(((0.9*P_up)*F_PLBp)/(1.0+(exp((((-sv->Cai)+p->K_up)/p->slope_up)))))) : (P_up/(1.0+(exp((((-sv->Cai)+p->K_up)/p->slope_up))))));
    GlobalData_t kiSRCa = (p->kiCa*kCaSR);
    GlobalData_t koSRCa = (p->koCa/kCaSR);
    GlobalData_t diff_Ca_nsr = ((j_up-(((j_tr*p->V_jsr)/p->V_nsr)))*0.001);
    GlobalData_t diff_Cai = (((((j_Ca_dif*p->V_sub)-((j_up*p->V_nsr)))/p->V_i)-((((p->CM_tot*delta_fCMi)+(p->TC_tot*delta_fTC))+(p->TMC_tot*delta_fTMC))))*0.001);
    GlobalData_t diff_I = (((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))-(((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))))*0.001);
    GlobalData_t diff_O = (((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))-((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))))*0.001);
    GlobalData_t diff_RI = ((((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))-(((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))))*0.001);
    GlobalData_t diff_R_1 = ((((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))-((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))))*0.001);
    GlobalData_t diff_fCa = (((fCa_infinity-(sv->fCa))/ICaL_fCa_gate_tau_fCa)*0.001);
    GlobalData_t Ca_jsr_new = sv->Ca_jsr+diff_Ca_jsr*dt;
    GlobalData_t Ca_nsr_new = sv->Ca_nsr+diff_Ca_nsr*dt;
    GlobalData_t Ca_sub_new = sv->Ca_sub+diff_Ca_sub*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t I_new = sv->I+diff_I*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t PLBp_new = sv->PLBp+diff_PLBp*dt;
    GlobalData_t RI_new = sv->RI+diff_RI*dt;
    GlobalData_t R_1_new = sv->R_1+diff_R_1*dt;
    GlobalData_t cAMP_new = sv->cAMP+diff_cAMP*dt;
    GlobalData_t fCMi_new = sv->fCMi+diff_fCMi*dt;
    GlobalData_t fCMs_new = sv->fCMs+diff_fCMs*dt;
    GlobalData_t fCQ_new = sv->fCQ+diff_fCQ*dt;
    GlobalData_t fCa_new = sv->fCa+diff_fCa*dt;
    GlobalData_t fTC_new = sv->fTC+diff_fTC*dt;
    GlobalData_t fTMC_new = sv->fTMC+diff_fTMC*dt;
    GlobalData_t fTMM_new = sv->fTMM+diff_fTMM*dt;
    GlobalData_t x_new = sv->x+diff_x*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t E0_m = ((V+41.0)-(p->m_tau_shift));
    GlobalData_t E0_m_WT = (V+41.0);
    GlobalData_t INa_h_WT_gate_alpha_h_WT = (20.0*(exp((-0.125*(V+75.0)))));
    GlobalData_t INa_h_WT_gate_beta_h_WT = (2000.0/((320.0*(exp((-0.1*(V+75.0)))))+1.0));
    GlobalData_t INa_h_gate_alpha_h = (20.0*(exp((-0.125*(V+75.0)))));
    GlobalData_t INa_h_gate_beta_h = (2000.0/((320.0*(exp((-0.1*(V+75.0)))))+1.0));
    GlobalData_t INa_m_WT_gate_beta_m_WT = (8000.0*(exp((-0.056*(V+66.0)))));
    GlobalData_t INa_m_gate_beta_m = (8000.0*(exp((-0.056*((V+66.0)-(p->m_tau_shift))))));
    GlobalData_t Iso_shift_cas_ICaL_dL_gate = ((p->Iso_cas>-0.1) ? (-(((K_ICaL2*(pow(PKA,9.281)))/((pow(K_05ICaL2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
    GlobalData_t Iso_shift_cas_IKs_n_gate = ((p->Iso_cas>-0.1) ? ((p->VW_IKs==0.0) ? (-((((24.4*1.411375)*(pow(PKA,9.281)))/((pow(0.704217,9.281))+(pow(PKA,9.281))))-(18.76))) : (-((((24.4*1.438835)*(pow(PKA,9.281)))/((pow(0.707399,9.281))+(pow(PKA,9.281))))-(18.76)))) : 0.);
    GlobalData_t Iso_shift_cas_If_y_gate = ((p->Iso_cas>-0.1) ? ((Kif*((pow(sv->cAMP,nif))/((pow(K_05if,nif))+(pow(sv->cAMP,nif)))))-(18.76)) : 0.);
    GlobalData_t beta_a = ((10.0*(exp((0.0133*(V+40.0)))))/1000.0);
    GlobalData_t dT_inf = (1.0/(1.0+(exp(((-(V+38.3))/5.5)))));
    GlobalData_t fL_inf = (1.0/(1.0+(exp((((V-(p->V_fL))+p->shift_fL)/p->k_fL)))));
    GlobalData_t fT_inf = (1.0/(1.0+(exp(((V+58.7)/3.8)))));
    GlobalData_t h_WT_inf = (1.0/(1.0+(exp(((V+69.804)/4.4565)))));
    GlobalData_t h_inf = (1.0/(1.0+(exp((((V+69.804)-(p->h_inf_shift))/(4.4565*p->h_inf_slope))))));
    GlobalData_t m_WT_inf = (1.0/(1.0+(exp(((-(V+42.0504))/8.3106)))));
    GlobalData_t m_inf = (1.0/(1.0+(exp(((-((V+42.0504)-(p->m_inf_shift)))/(8.3106*p->m_inf_slope))))));
    GlobalData_t pa_infinity = (1.0/(1.0+(exp(((-(V+10.0144))/7.6607)))));
    GlobalData_t piy_inf = (1.0/(1.0+(exp(((V+28.6)/17.1)))));
    GlobalData_t q_inf = (1.0/(1.0+(exp(((V+49.0)/13.0)))));
    GlobalData_t r_Kur_inf = (1.0/(1.0+(exp(((V+6.0)/-8.6)))));
    GlobalData_t r_inf = (1.0/(1.0+(exp(((-(V-(19.3)))/15.0)))));
    GlobalData_t s_Kur_inf = (1.0/(1.0+(exp(((V+7.5)/10.0)))));
    GlobalData_t tau_dT = ((0.001/((1.068*(exp(((V+38.3)/30.0))))+(1.068*(exp(((-(V+38.3))/30.0))))))*1000.0);
    GlobalData_t tau_fL = ((0.001*(44.3+(230.0*(exp((-(pow(((V+36.0)/10.0),2.0))))))))*1000.0);
    GlobalData_t tau_fT = (((1.0/((16.67*(exp(((-(V+75.0))/83.3))))+(16.67*(exp(((V+75.0)/15.38))))))+p->offset_fT)*1000.0);
    GlobalData_t tau_paF = ((1.0/((30.0*(exp((V/10.0))))+(exp(((-V)/12.0)))))*1000.0);
    GlobalData_t tau_paS = ((8.46553540000000049e-01/((4.2*(exp((V/17.0))))+(0.15*(exp(((-V)/21.6))))))*1000.0);
    GlobalData_t tau_piy = ((1.0/((100.0*(exp(((-V)/54.645))))+(656.0*(exp((V/106.157))))))*1000.0);
    GlobalData_t tau_q = (((0.001*0.6)*((65.17/((0.57*(exp((-0.08*(V+44.0)))))+(0.065*(exp((0.1*(V+45.93)))))))+10.1))*1000.0);
    GlobalData_t tau_r = ((((0.001*0.66)*1.4)*((15.59/((1.037*(exp((0.09*(V+30.61)))))+(0.369*(exp((-0.12*(V+23.84)))))))+2.98))*1000.0);
    GlobalData_t tau_r_Kur = (((0.009/(1.0+(exp(((V+5.0)/12.0)))))+0.0005)*1000.0);
    GlobalData_t tau_s_Kur = (((0.59/(1.0+(exp(((V+60.0)/10.0)))))+3.05)*1000.0);
    GlobalData_t IKs_n_gate_beta_n = ((p->VW_IKs==0.) ? (exp(((-(((V-(Iso_shift_1))-(Iso_shift_cas_IKs_n_gate))-(5.0)))/25.0))) : (1.93*(exp(((-V)/83.2)))));
    GlobalData_t INa_m_WT_gate_alpha_m_WT = (((fabs(E0_m_WT))<delta_m_WT) ? 2000.0 : ((200.0*E0_m_WT)/(-(expm1((-0.1*E0_m_WT))))));
    GlobalData_t INa_m_gate_alpha_m = (((fabs(E0_m))<p->delta_m) ? 2000.0 : ((200.0*E0_m)/(-(expm1((-0.1*E0_m))))));
    GlobalData_t a_rush_larsen_A = (((-alpha_a)/(alpha_a+beta_a))*(expm1(((-dt)*(alpha_a+beta_a)))));
    GlobalData_t a_rush_larsen_B = (exp(((-dt)*(alpha_a+beta_a))));
    GlobalData_t adVm = ((V==((-41.8-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate))) ? ((-41.80001-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)) : ((V==0.0) ? 0.0 : ((V==((-6.8-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate))) ? ((-6.80001-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)) : V)));
    GlobalData_t bdVm = ((V==((-1.8-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate))) ? ((-1.80001-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)) : V);
    GlobalData_t dL_inf = (1.0/(1.0+(exp(((-(((V-(p->V_dL))-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/((p->k_dL*(1.0+Iso_slope_cas_ICaL))*(1.0+(Iso_slope_dL/100.0))))))));
    GlobalData_t dT_rush_larsen_B = (exp(((-dt)/tau_dT)));
    GlobalData_t dT_rush_larsen_C = (expm1(((-dt)/tau_dT)));
    GlobalData_t fL_rush_larsen_B = (exp(((-dt)/tau_fL)));
    GlobalData_t fL_rush_larsen_C = (expm1(((-dt)/tau_fL)));
    GlobalData_t fT_rush_larsen_B = (exp(((-dt)/tau_fT)));
    GlobalData_t fT_rush_larsen_C = (expm1(((-dt)/tau_fT)));
    GlobalData_t n_inf = ((p->VW_IKs==0.) ? (sqrt((1.0/(1.0+(exp(((-((((V+0.6383)-(Iso_shift_1))-(Iso_shift_cas_IKs_n_gate))-(p->n_inf_shift)))/(10.7071*p->n_shift_slope)))))))) : (1./(1.+(exp(((-((((V+15.733)+Iso_shift_ninf)-(Iso_shift_cas_IKs_n_gate))-(p->n_inf_shift)))/(p->n_shift_slope*27.77)))))));
    GlobalData_t paF_inf = pa_infinity;
    GlobalData_t paF_rush_larsen_B = (exp(((-dt)/tau_paF)));
    GlobalData_t paF_rush_larsen_C = (expm1(((-dt)/tau_paF)));
    GlobalData_t paS_inf = pa_infinity;
    GlobalData_t paS_rush_larsen_B = (exp(((-dt)/tau_paS)));
    GlobalData_t paS_rush_larsen_C = (expm1(((-dt)/tau_paS)));
    GlobalData_t piy_rush_larsen_B = (exp(((-dt)/tau_piy)));
    GlobalData_t piy_rush_larsen_C = (expm1(((-dt)/tau_piy)));
    GlobalData_t q_rush_larsen_B = (exp(((-dt)/tau_q)));
    GlobalData_t q_rush_larsen_C = (expm1(((-dt)/tau_q)));
    GlobalData_t r_Kur_rush_larsen_B = (exp(((-dt)/tau_r_Kur)));
    GlobalData_t r_Kur_rush_larsen_C = (expm1(((-dt)/tau_r_Kur)));
    GlobalData_t r_rush_larsen_B = (exp(((-dt)/tau_r)));
    GlobalData_t r_rush_larsen_C = (expm1(((-dt)/tau_r)));
    GlobalData_t s_Kur_rush_larsen_B = (exp(((-dt)/tau_s_Kur)));
    GlobalData_t s_Kur_rush_larsen_C = (expm1(((-dt)/tau_s_Kur)));
    GlobalData_t tau_h = ((p->h_tau_gain/(INa_h_gate_alpha_h+INa_h_gate_beta_h))*1000.0);
    GlobalData_t tau_h_WT = ((1.0/(INa_h_WT_gate_alpha_h_WT+INa_h_WT_gate_beta_h_WT))*1000.0);
    GlobalData_t tau_y = (((1.0/(((0.36*(((((V+148.8)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->tau_y_a_shift)))/(expm1((0.066*(((((V+148.8)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->tau_y_a_shift))))))+((0.1*(((((V+87.3)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->tau_y_b_shift)))/(-(expm1((-0.2*(((((V+87.3)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->tau_y_b_shift)))))))))-(0.054))*1000.0);
    GlobalData_t y_inf = ((V<(-((((80.0-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift)))) ? (0.01329+(0.99921/(1.0+(exp(((((((V+97.134)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift))/8.1752)))))) : (0.0002501*(exp(((-((((V-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift)))/12.861)))));
    GlobalData_t ICaL_dL_gate_alpha_dL = (((-0.02839*(((adVm+41.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/(expm1(((-(((adVm+41.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/2.5))))-(((0.0849*(((adVm+6.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/(expm1(((-(((adVm+6.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/4.8))))));
    GlobalData_t ICaL_dL_gate_beta_dL = ((0.01143*(((bdVm+1.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/(expm1(((((bdVm+1.8)-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate))/2.5))));
    GlobalData_t IKs_n_gate_alpha_n = ((p->VW_IKs==0.) ? (28.0/(1.0+(exp(((-(((V-(40.0))-(Iso_shift_1))-(Iso_shift_cas_IKs_n_gate)))/3.0))))) : ((n_inf/(1.-(n_inf)))*IKs_n_gate_beta_n));
    GlobalData_t dT_rush_larsen_A = ((-dT_inf)*dT_rush_larsen_C);
    GlobalData_t fL_rush_larsen_A = ((-fL_inf)*fL_rush_larsen_C);
    GlobalData_t fT_rush_larsen_A = ((-fT_inf)*fT_rush_larsen_C);
    GlobalData_t h_WT_rush_larsen_B = (exp(((-dt)/tau_h_WT)));
    GlobalData_t h_WT_rush_larsen_C = (expm1(((-dt)/tau_h_WT)));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)/tau_h)));
    GlobalData_t h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    GlobalData_t paF_rush_larsen_A = ((-paF_inf)*paF_rush_larsen_C);
    GlobalData_t paS_rush_larsen_A = ((-paS_inf)*paS_rush_larsen_C);
    GlobalData_t piy_rush_larsen_A = ((-piy_inf)*piy_rush_larsen_C);
    GlobalData_t q_rush_larsen_A = ((-q_inf)*q_rush_larsen_C);
    GlobalData_t r_Kur_rush_larsen_A = ((-r_Kur_inf)*r_Kur_rush_larsen_C);
    GlobalData_t r_rush_larsen_A = ((-r_inf)*r_rush_larsen_C);
    GlobalData_t s_Kur_rush_larsen_A = ((-s_Kur_inf)*s_Kur_rush_larsen_C);
    GlobalData_t tau_m = ((1.0/(INa_m_gate_alpha_m+INa_m_gate_beta_m))*1000.0);
    GlobalData_t tau_m_WT = ((1.0/(INa_m_WT_gate_alpha_m_WT+INa_m_WT_gate_beta_m_WT))*1000.0);
    GlobalData_t y_rush_larsen_B = (exp(((-dt)/tau_y)));
    GlobalData_t y_rush_larsen_C = (expm1(((-dt)/tau_y)));
    GlobalData_t h_WT_rush_larsen_A = ((-h_WT_inf)*h_WT_rush_larsen_C);
    GlobalData_t h_rush_larsen_A = ((-h_inf)*h_rush_larsen_C);
    GlobalData_t m_WT_rush_larsen_B = (exp(((-dt)/tau_m_WT)));
    GlobalData_t m_WT_rush_larsen_C = (expm1(((-dt)/tau_m_WT)));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t tau_dL = ((0.001/(ICaL_dL_gate_alpha_dL+ICaL_dL_gate_beta_dL))*1000.0);
    GlobalData_t tau_n = ((1.0/(IKs_n_gate_alpha_n+IKs_n_gate_beta_n))*1000.0);
    GlobalData_t y_rush_larsen_A = ((-y_inf)*y_rush_larsen_C);
    GlobalData_t dL_rush_larsen_B = (exp(((-dt)/tau_dL)));
    GlobalData_t dL_rush_larsen_C = (expm1(((-dt)/tau_dL)));
    GlobalData_t m_WT_rush_larsen_A = ((-m_WT_inf)*m_WT_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_inf)*m_rush_larsen_C);
    GlobalData_t n_rush_larsen_B = (exp(((-dt)/tau_n)));
    GlobalData_t n_rush_larsen_C = (expm1(((-dt)/tau_n)));
    GlobalData_t dL_rush_larsen_A = ((-dL_inf)*dL_rush_larsen_C);
    GlobalData_t n_rush_larsen_A = ((-n_inf)*n_rush_larsen_C);
    GlobalData_t a_new = a_rush_larsen_A+a_rush_larsen_B*sv->a;
    GlobalData_t dL_new = dL_rush_larsen_A+dL_rush_larsen_B*sv->dL;
    GlobalData_t dT_new = dT_rush_larsen_A+dT_rush_larsen_B*sv->dT;
    GlobalData_t fL_new = fL_rush_larsen_A+fL_rush_larsen_B*sv->fL;
    GlobalData_t fT_new = fT_rush_larsen_A+fT_rush_larsen_B*sv->fT;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t h_WT_new = h_WT_rush_larsen_A+h_WT_rush_larsen_B*sv->h_WT;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t m_WT_new = m_WT_rush_larsen_A+m_WT_rush_larsen_B*sv->m_WT;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    GlobalData_t paF_new = paF_rush_larsen_A+paF_rush_larsen_B*sv->paF;
    GlobalData_t paS_new = paS_rush_larsen_A+paS_rush_larsen_B*sv->paS;
    GlobalData_t piy_new = piy_rush_larsen_A+piy_rush_larsen_B*sv->piy;
    GlobalData_t q_new = q_rush_larsen_A+q_rush_larsen_B*sv->q;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t r_Kur_new = r_Kur_rush_larsen_A+r_Kur_rush_larsen_B*sv->r_Kur;
    GlobalData_t s_Kur_new = s_Kur_rush_larsen_A+s_Kur_rush_larsen_B*sv->s_Kur;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_jsr = Ca_jsr_new;
    sv->Ca_nsr = Ca_nsr_new;
    sv->Ca_sub = Ca_sub_new;
    sv->Cai = Cai_new;
    sv->I = I_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->PLBp = PLBp_new;
    sv->RI = RI_new;
    sv->R_1 = R_1_new;
    sv->a = a_new;
    sv->cAMP = cAMP_new;
    sv->dL = dL_new;
    sv->dT = dT_new;
    sv->fCMi = fCMi_new;
    sv->fCMs = fCMs_new;
    sv->fCQ = fCQ_new;
    sv->fCa = fCa_new;
    sv->fL = fL_new;
    sv->fT = fT_new;
    sv->fTC = fTC_new;
    sv->fTMC = fTMC_new;
    sv->fTMM = fTMM_new;
    sv->h = h_new;
    sv->h_WT = h_WT_new;
    sv->m = m_new;
    sv->m_WT = m_WT_new;
    sv->n = n_new;
    sv->paF = paF_new;
    sv->paS = paS_new;
    sv->piy = piy_new;
    sv->q = q_new;
    sv->r = r_new;
    sv->r_Kur = r_Kur_new;
    sv->s_Kur = s_Kur_new;
    sv->x = x_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // FABBRI_CPU_GENERATED

bool FabbriIonType::has_trace() const {
    return true;
}

void FabbriIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Fabbri_trace_header.txt","wt");
    fprintf(theader->fd,
        "ATPi\n"
        "sv->Ca_jsr\n"
        "sv->Ca_nsr\n"
        "sv->Ca_sub\n"
        "sv->Cai\n"
        "F_PLBp\n"
        "sv->I\n"
        "ICaL\n"
        "ICaT\n"
        "IKACh\n"
        "IKr\n"
        "IKs\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "ISK\n"
        "If\n"
        "IfK\n"
        "IfNa\n"
        "Iion\n"
        "IsiCa\n"
        "IsiK\n"
        "IsiNa\n"
        "Iso_inc_cas_ICaL\n"
        "Iso_inc_cas_IKs\n"
        "Iso_inc_cas_INaK\n"
        "Iso_shift_cas_ICaL_dL_gate\n"
        "Iso_shift_cas_IKs_n_gate\n"
        "Iso_shift_cas_If_y_gate\n"
        "Iso_slope_cas_ICaL\n"
        "Ito\n"
        "Itot\n"
        "sv->Ki\n"
        "sv->Nai\n"
        "sv->O\n"
        "PKA\n"
        "sv->PLBp\n"
        "sv->RI\n"
        "sv->R_1\n"
        "V\n"
        "sv->a\n"
        "sv->cAMP\n"
        "sv->dL\n"
        "dL_inf\n"
        "sv->dT\n"
        "di\n"
        "diff_Ca_jsr\n"
        "diff_Ca_nsr\n"
        "diff_Ca_sub\n"
        "diff_Cai\n"
        "diff_I\n"
        "diff_Ki\n"
        "diff_Nai\n"
        "diff_O\n"
        "diff_PLBp\n"
        "diff_RI\n"
        "diff_R_1\n"
        "diff_cAMP\n"
        "diff_fCMi\n"
        "diff_fCMs\n"
        "diff_fCQ\n"
        "diff_fTC\n"
        "diff_fTMC\n"
        "diff_fTMM\n"
        "diff_paF\n"
        "diff_paS\n"
        "diff_piy\n"
        "do_\n"
        "sv->fCMi\n"
        "sv->fCMs\n"
        "sv->fCQ\n"
        "sv->fCa\n"
        "sv->fL\n"
        "sv->fT\n"
        "sv->fTC\n"
        "sv->fTMC\n"
        "sv->fTMM\n"
        "sv->h\n"
        "sv->h_WT\n"
        "j_Ca_dif\n"
        "j_SRCarel\n"
        "j_up\n"
        "k1\n"
        "k12\n"
        "k14\n"
        "k2\n"
        "k21\n"
        "k23\n"
        "k3\n"
        "k32\n"
        "p->k34\n"
        "k4\n"
        "k41\n"
        "k43\n"
        "k5\n"
        "kCaSR\n"
        "kcch\n"
        "kiSRCa\n"
        "kiso\n"
        "p->koCa\n"
        "koSRCa\n"
        "sv->m\n"
        "sv->m_WT\n"
        "sv->n\n"
        "sv->paF\n"
        "sv->paS\n"
        "sv->piy\n"
        "sv->q\n"
        "sv->r\n"
        "sv->r_Kur\n"
        "sv->s_Kur\n"
        "sv->x\n"
        "x1\n"
        "x2\n"
        "x3\n"
        "x4\n"
        "sv->y\n"
        "y_inf\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Fabbri_Params *p  = imp.params();

  Fabbri_state *sv_base = (Fabbri_state *)imp.sv_tab().data();

  Fabbri_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t ACh_block = (((0.31*1e-6)*p->ACh)/((1e-6*p->ACh)+9e-05));
  GlobalData_t ACh_shift = ((p->ACh>0.0) ? (-1.0-(((9.898*(pow((1e-6*p->ACh),0.618)))/((pow((1e-6*p->ACh),0.618))+1.22422999999999998e-03)))) : 0.0);
  GlobalData_t Ca_intracellular_fluxes_b_up = ((p->Iso_1_uM>0.0) ? (-0.25*p->K_iso_shift) : ((p->ACh>0.0) ? ((0.7*p->ACh)/(9e-05+p->ACh)) : 0.0));
  GlobalData_t GKr = (p->GKr_max*(sqrt((p->Ko/5.4))));
  GlobalData_t GNa_L = (p->GNa_max*p->ratio_INaL_INa);
  GlobalData_t Iso_increase_1 = ((p->Iso_linear>0.0) ? (1.0+(0.23*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.23*p->K_iso_increase) : 1.0));
  GlobalData_t Iso_increase_2 = ((p->Iso_linear>0.0) ? (1.0+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.2*p->K_iso_increase) : 1.0));
  GlobalData_t Iso_shift_1 = ((p->Iso_1_uM>0.0) ? (-14.0*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_2 = ((p->Iso_1_uM>0.0) ? (7.5*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_dL = ((p->Iso_1_uM>0.0) ? (-8.0*p->K_iso_shift) : 0.0);
  GlobalData_t Iso_shift_ninf = ((p->Iso_1_uM>0.0) ? (14.568*p->K_iso_shift_ninf) : 0.0);
  GlobalData_t Iso_slope_dL = ((p->Iso_1_uM>0.0) ? (-27.0*p->K_iso_slope_dL) : 0.0);
  GlobalData_t alpha_a = ((((3.5988-(0.025641))/(1.0+(1.21550000000000005e-06/(pow((1e-6*p->ACh),1.6951)))))+0.025641)/1000.0);
  GlobalData_t kcch = (0.0146*((pow(p->CCh_cas,1.4402))/((pow(51.7331,1.4402))+(pow(p->CCh_cas,1.4402)))));
  GlobalData_t kiso = (K_iso+(0.1181*((pow(p->Iso_cas,niso))/((pow(K_05iso,niso))+(pow(p->Iso_cas,niso))))));
  GlobalData_t P_up = (p->P_up_basal*(1.0-(Ca_intracellular_fluxes_b_up)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t ATPi = ((ATPi_max*(((kATP*(pow(((sv->cAMP*100.)/cAMPb),nATP)))/(kATP05+(pow(((sv->cAMP*100.)/cAMPb),nATP))))-(KATP_min)))/100.);
  GlobalData_t E_K = (p->RTONF*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Ks = ((p->VW_IKs==0.) ? (p->RTONF*(log(((p->Ko+(0.12*p->Nao))/(sv->Ki+(0.12*sv->Nai)))))) : (p->RTONF*(log(((p->Ko+(0.0018*p->Nao))/(sv->Ki+(0.0018*sv->Nai)))))));
  GlobalData_t E_Na = (p->RTONF*(log((p->Nao/sv->Nai))));
  GlobalData_t E_mh = (p->RTONF*(log(((p->Nao+(0.12*p->Ko))/(sv->Nai+(0.12*sv->Ki))))));
  GlobalData_t F_PLBp = ((sv->PLBp>0.23) ? ((3.3931*(pow(sv->PLBp,4.0695)))/((pow(0.2805,4.0695))+(pow(sv->PLBp,4.0695)))) : ((1.698*(pow(sv->PLBp,13.5842)))/((pow(0.2240,13.5842))+(pow(sv->PLBp,13.5842)))));
  GlobalData_t ICaT = ((((((2.0*p->P_CaT)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dT)*sv->fT);
  GlobalData_t Iso_shift_cas_If_y_gate = ((p->Iso_cas>-0.1) ? ((Kif*((pow(sv->cAMP,nif))/((pow(K_05if,nif))+(pow(sv->cAMP,nif)))))-(18.76)) : 0.);
  GlobalData_t PKA = (((sv->cAMP*600.)<25.87) ? ((-0.9483029*(exp((((-sv->cAMP)*600.)*0.06561479))))+0.97781646) : ((-0.45260509*(exp((((-sv->cAMP)*600.)*0.03395094))))+0.99221714));
  GlobalData_t R231C = ((p->R231C_on!=0.) ? ((g_ratio*(pow((1./(1.+(exp(((-(V-(V_R231C)))/k_R231C))))),2.)))/(pow((((1.-(A2))/(1.+(exp(((-(V-(V_WT)))/k_WT)))))+A2),2.))) : 1.);
  GlobalData_t delta_fCMi = (((p->kf_CM*sv->Cai)*(1.0-(sv->fCMi)))-((p->kb_CM*sv->fCMi)));
  GlobalData_t delta_fCMs = (((p->kf_CM*sv->Ca_sub)*(1.0-(sv->fCMs)))-((p->kb_CM*sv->fCMs)));
  GlobalData_t delta_fCQ = (((p->kf_CQ*sv->Ca_jsr)*(1.0-(sv->fCQ)))-((p->kb_CQ*sv->fCQ)));
  GlobalData_t delta_fTC = (((p->kf_TC*sv->Cai)*(1.0-(sv->fTC)))-((p->kb_TC*sv->fTC)));
  GlobalData_t delta_fTMC = (((p->kf_TMC*sv->Cai)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMC*sv->fTMC)));
  GlobalData_t delta_fTMM = (((p->kf_TMM*p->Mgi)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMM*sv->fTMM)));
  GlobalData_t di = ((1.0+((sv->Ca_sub/p->Kci)*((1.0+(exp((((-p->Qci)*V)/p->RTONF))))+(sv->Nai/p->Kcni))))+((sv->Nai/p->K1ni)*(1.0+((sv->Nai/p->K2ni)*(1.0+(sv->Nai/p->K3ni))))));
  GlobalData_t do_ = ((1.0+((p->Cao/p->Kco)*(1.0+(exp(((p->Qco*V)/p->RTONF))))))+((p->Nao/p->K1no)*(1.0+((p->Nao/p->K2no)*(1.0+(p->Nao/p->K3no))))));
  GlobalData_t j_Ca_dif = ((sv->Ca_sub-(sv->Cai))/p->Ca_intracellular_fluxes_tau_dif_Ca);
  GlobalData_t j_SRCarel = (((p->K_j_SRCarel*p->ks)*sv->O)*(sv->Ca_jsr-(sv->Ca_sub)));
  GlobalData_t j_tr = ((sv->Ca_nsr-(sv->Ca_jsr))/p->Ca_intracellular_fluxes_tau_tr);
  GlobalData_t k1 = (p->K_k1*(K_ACI+(K_AC/(1.0+(exp(((K_Ca-((p->kb_CM*(sv->fCMi/(p->kf_CM*(1.-(sv->fCMi)))))))/K_ACCa)))))));
  GlobalData_t k2 = ((1.1*237.9851)*((pow((sv->cAMP*600.),5.101))/((pow(20.1077,6.101))+(pow((sv->cAMP*600.),6.101)))));
  GlobalData_t k3 = (kPKA*((pow(sv->cAMP,(nPKA-(1.))))/((pow(kPKA_cAMP,nPKA))+(pow(sv->cAMP,nPKA)))));
  GlobalData_t k32 = (exp(((p->Qn*V)/(2.0*p->RTONF))));
  GlobalData_t k41 = (exp((((-p->Qn)*V)/(2.0*p->RTONF))));
  GlobalData_t k43 = (sv->Nai/(p->K3ni+sv->Nai));
  GlobalData_t k5 = (kPP1*((PP1*sv->PLBp)/(kPP1_PLB+sv->PLBp)));
  GlobalData_t kCaSR = (p->MaxSR-(((p->MaxSR-(p->MinSR))/(1.0+(pow((p->EC50_SR/sv->Ca_jsr),p->HSR))))));
  GlobalData_t pa_infinity = (1.0/(1.0+(exp(((-(V+10.0144))/7.6607)))));
  GlobalData_t piy_inf = (1.0/(1.0+(exp(((V+28.6)/17.1)))));
  GlobalData_t tau_paF = ((1.0/((30.0*(exp((V/10.0))))+(exp(((-V)/12.0)))))*1000.0);
  GlobalData_t tau_paS = ((8.46553540000000049e-01/((4.2*(exp((V/17.0))))+(0.15*(exp(((-V)/21.6))))))*1000.0);
  GlobalData_t tau_piy = ((1.0/((100.0*(exp(((-V)/54.645))))+(656.0*(exp((V/106.157))))))*1000.0);
  GlobalData_t IKACh = ((p->ACh>0.0) ? ((((p->ACh_on*p->GKACh)*(V-(E_K)))*(1.0+(exp(((V+20.0)/20.0)))))*sv->a) : 0.0);
  GlobalData_t IKr = (((GKr*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
  GlobalData_t IKur = (((p->GKur_max*sv->r_Kur)*sv->s_Kur)*(V-(E_K)));
  GlobalData_t INa_ = (((p->GNa_max*(pow(sv->m,3.0)))*sv->h)*(V-(E_mh)));
  GlobalData_t INa_L = ((GNa_L*(pow(sv->m,3.0)))*(V-(E_mh)));
  GlobalData_t INa_WT = (((GNa_WT*(pow(sv->m_WT,3.0)))*sv->h_WT)*(V-(E_mh)));
  GlobalData_t ISK = ((p->GSK*(V-(E_K)))*sv->x);
  GlobalData_t IfK = ((sv->y*p->Gf_K_max)*(V-(E_K)));
  GlobalData_t IfNa = ((sv->y*p->Gf_Na_max)*(V-(E_Na)));
  GlobalData_t Iso_inc_cas_ICaL = ((p->Iso_cas>-0.1) ? (-0.2152+((K_ICaL1*(pow(PKA,10.0808)))/((pow(K_05ICaL1,10.0808))+(pow(PKA,10.0808))))) : 0.);
  GlobalData_t Iso_inc_cas_IKs = ((p->Iso_cas>-0.1) ? ((p->VW_IKs==0.0) ? (-0.2152+((0.435692*(pow(PKA,10.0808)))/((pow(0.719701,10.0808))+(pow(PKA,10.0808))))) : (-0.2152+((0.494259*(pow(PKA,10.0808)))/((pow(0.736717,10.0808))+(pow(PKA,10.0808)))))) : 0.);
  GlobalData_t Iso_inc_cas_INaK = ((p->Iso_cas>-0.1) ? (-0.2152+((K_INaK*(pow(PKA,10.0808)))/((pow(K_05INaK,10.0808))+(pow(PKA,10.0808))))) : 0.);
  GlobalData_t Iso_shift_cas_ICaL_dL_gate = ((p->Iso_cas>-0.1) ? (-(((K_ICaL2*(pow(PKA,9.281)))/((pow(K_05ICaL2,9.281))+(pow(PKA,9.281))))-(18.76))) : 0.);
  GlobalData_t Iso_shift_cas_IKs_n_gate = ((p->Iso_cas>-0.1) ? ((p->VW_IKs==0.0) ? (-((((24.4*1.411375)*(pow(PKA,9.281)))/((pow(0.704217,9.281))+(pow(PKA,9.281))))-(18.76))) : (-((((24.4*1.438835)*(pow(PKA,9.281)))/((pow(0.707399,9.281))+(pow(PKA,9.281))))-(18.76)))) : 0.);
  GlobalData_t Ito = (((p->Gto_max*(V-(E_K)))*sv->q)*sv->r);
  GlobalData_t diff_Ca_jsr = ((j_tr-((j_SRCarel+(p->CQ_tot*delta_fCQ))))*0.001);
  GlobalData_t diff_cAMP = ((((((kiso-(kcch))*ATPi)+(k1*ATPi))-((k2*sv->cAMP)))-((k3*sv->cAMP)))/60000.);
  GlobalData_t diff_fCMi = (delta_fCMi*0.001);
  GlobalData_t diff_fCMs = (delta_fCMs*0.001);
  GlobalData_t diff_fCQ = (delta_fCQ*0.001);
  GlobalData_t diff_fTC = (delta_fTC*0.001);
  GlobalData_t diff_fTMC = (delta_fTMC*0.001);
  GlobalData_t diff_fTMM = (delta_fTMM*0.001);
  GlobalData_t diff_piy = ((piy_inf-(sv->piy))/tau_piy);
  GlobalData_t j_up = ((p->Iso_cas>-0.1) ? (p->K_j_up*(((0.9*P_up)*F_PLBp)/(1.0+(exp((((-sv->Cai)+p->K_up)/p->slope_up)))))) : (P_up/(1.0+(exp((((-sv->Cai)+p->K_up)/p->slope_up))))));
  GlobalData_t k12 = (((sv->Ca_sub/p->Kci)*(exp((((-p->Qci)*V)/p->RTONF))))/di);
  GlobalData_t k14 = ((((((sv->Nai/p->K1ni)*sv->Nai)/p->K2ni)*(1.0+(sv->Nai/p->K3ni)))*(exp(((p->Qn*V)/(2.0*p->RTONF)))))/di);
  GlobalData_t k21 = (((p->Cao/p->Kco)*(exp(((p->Qco*V)/p->RTONF))))/do_);
  GlobalData_t k23 = ((((((p->Nao/p->K1no)*p->Nao)/p->K2no)*(1.0+(p->Nao/p->K3no)))*(exp((((-p->Qn)*V)/(2.0*p->RTONF)))))/do_);
  GlobalData_t k4 = ((kPLBp*(pow(PKA,nPLB)))/((pow(kPKA_PLB,nPLB))+(pow(PKA,nPLB))));
  GlobalData_t kiSRCa = (p->kiCa*kCaSR);
  GlobalData_t koSRCa = (p->koCa/kCaSR);
  GlobalData_t paF_inf = pa_infinity;
  GlobalData_t paS_inf = pa_infinity;
  GlobalData_t y_inf = ((V<(-((((80.0-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift)))) ? (0.01329+(0.99921/(1.0+(exp(((((((V+97.134)-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift))/8.1752)))))) : (0.0002501*(exp(((-((((V-(ACh_shift))-(Iso_shift_2))-(Iso_shift_cas_If_y_gate))-(p->y_shift)))/12.861)))));
  GlobalData_t GKs = ((p->VW_IKs==0.) ? (p->GKs_max*((p->Iso_linear>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.+(0.2*p->K_iso_increase)) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))) : ((0.2*p->GKs_max)*((p->Iso_linear>0.0) ? (1.0+(0.25*p->K_iso_increase)) : ((p->Iso_1_uM>0.0) ? (1.25*p->K_iso_increase) : ((p->Iso_cas>-0.1) ? (1.0+Iso_inc_cas_IKs) : 1.0)))));
  GlobalData_t INa = (((1.-(INa_WT_ratio))*(INa_+INa_L))+(INa_WT_ratio*INa_WT));
  GlobalData_t INaK = ((((((1.0+Iso_inc_cas_INaK)*Iso_increase_2)*p->INaK_max)*(pow((1.0+(pow((p->Km_Kp/p->Ko),1.2))),-1.0)))*(pow((1.0+(pow((p->Km_Nap/sv->Nai),1.3))),-1.0)))*(pow((1.0+(exp(((-((V-(E_Na))+110.0))/20.0)))),-1.0)));
  GlobalData_t If = (IfNa+IfK);
  GlobalData_t IsiCa = ((((((((((2.0*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t IsiK = ((((((((((0.000365*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Ki-((p->Ko*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t IsiNa = ((((((((((1.85e-05*p->P_CaL)*Iso_increase_1)*(1.0-(ACh_block)))*(1.0+Iso_inc_cas_ICaL))*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Nai-((p->Nao*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
  GlobalData_t dL_inf = (1.0/(1.0+(exp(((-(((V-(p->V_dL))-(Iso_shift_dL))-(Iso_shift_cas_ICaL_dL_gate)))/((p->k_dL*(1.0+Iso_slope_cas_ICaL))*(1.0+(Iso_slope_dL/100.0))))))));
  GlobalData_t diff_Ca_nsr = ((j_up-(((j_tr*p->V_jsr)/p->V_nsr)))*0.001);
  GlobalData_t diff_Cai = (((((j_Ca_dif*p->V_sub)-((j_up*p->V_nsr)))/p->V_i)-((((p->CM_tot*delta_fCMi)+(p->TC_tot*delta_fTC))+(p->TMC_tot*delta_fTMC))))*0.001);
  GlobalData_t diff_I = (((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))-(((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))))*0.001);
  GlobalData_t diff_O = (((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))-((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))))*0.001);
  GlobalData_t diff_PLBp = ((k4-(k5))/60000.);
  GlobalData_t diff_RI = ((((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))-(((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))))*0.001);
  GlobalData_t diff_R_1 = ((((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))-((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))))*0.001);
  GlobalData_t diff_paF = ((paF_inf-(sv->paF))/tau_paF);
  GlobalData_t diff_paS = ((paS_inf-(sv->paS))/tau_paS);
  GlobalData_t x1 = (((k41*p->k34)*(k23+k21))+((k21*k32)*(k43+k41)));
  GlobalData_t x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(p->k34+k32)));
  GlobalData_t x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
  GlobalData_t x4 = (((k23*p->k34)*(k14+k12))+((k14*k21)*(p->k34+k32)));
  GlobalData_t ICaL = ((IsiCa+IsiK)+IsiNa);
  GlobalData_t IKs = (((R231C*GKs)*(V-(E_Ks)))*(pow(sv->n,2.0)));
  GlobalData_t INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
  GlobalData_t Itot = (((((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh)+IKur)+ISK);
  GlobalData_t diff_Ca_sub = ((((j_SRCarel*p->V_jsr)/p->V_sub)-((((((IsiCa+ICaT)-((2.0*INaCa)))/((2.0*p->F)*p->V_sub))+j_Ca_dif)+(p->CM_tot*delta_fCMs))))*0.001);
  GlobalData_t diff_Ki = ((p->dynamic_Ki_Nai==1.) ? (((-1.0*(((((((IKur+Ito)+IKr)+IKs)+IfK)+IsiK)+ISK)-((2.0*INaK))))/((p->V_i+p->V_sub)*p->F))*0.001) : 0.);
  GlobalData_t diff_Nai = ((p->dynamic_Ki_Nai==1.) ? (((-1.0*((((INa+IfNa)+IsiNa)+(3.0*INaK))+(3.0*INaCa)))/((p->V_i+p->V_sub)*p->F))*0.001) : 0.);
  Iion = ((Itot*1000.)/p->C);
  //Output the desired variables
  fprintf(file, "%4.12f\t", ATPi);
  fprintf(file, "%4.12f\t", sv->Ca_jsr);
  fprintf(file, "%4.12f\t", sv->Ca_nsr);
  fprintf(file, "%4.12f\t", sv->Ca_sub);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", F_PLBp);
  fprintf(file, "%4.12f\t", sv->I);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaT);
  fprintf(file, "%4.12f\t", IKACh);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", ISK);
  fprintf(file, "%4.12f\t", If);
  fprintf(file, "%4.12f\t", IfK);
  fprintf(file, "%4.12f\t", IfNa);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", IsiCa);
  fprintf(file, "%4.12f\t", IsiK);
  fprintf(file, "%4.12f\t", IsiNa);
  fprintf(file, "%4.12f\t", Iso_inc_cas_ICaL);
  fprintf(file, "%4.12f\t", Iso_inc_cas_IKs);
  fprintf(file, "%4.12f\t", Iso_inc_cas_INaK);
  fprintf(file, "%4.12f\t", Iso_shift_cas_ICaL_dL_gate);
  fprintf(file, "%4.12f\t", Iso_shift_cas_IKs_n_gate);
  fprintf(file, "%4.12f\t", Iso_shift_cas_If_y_gate);
  fprintf(file, "%4.12f\t", Iso_slope_cas_ICaL);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Itot);
  fprintf(file, "%4.12f\t", sv->Ki);
  fprintf(file, "%4.12f\t", sv->Nai);
  fprintf(file, "%4.12f\t", sv->O);
  fprintf(file, "%4.12f\t", PKA);
  fprintf(file, "%4.12f\t", sv->PLBp);
  fprintf(file, "%4.12f\t", sv->RI);
  fprintf(file, "%4.12f\t", sv->R_1);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->a);
  fprintf(file, "%4.12f\t", sv->cAMP);
  fprintf(file, "%4.12f\t", sv->dL);
  fprintf(file, "%4.12f\t", dL_inf);
  fprintf(file, "%4.12f\t", sv->dT);
  fprintf(file, "%4.12f\t", di);
  fprintf(file, "%4.12f\t", diff_Ca_jsr);
  fprintf(file, "%4.12f\t", diff_Ca_nsr);
  fprintf(file, "%4.12f\t", diff_Ca_sub);
  fprintf(file, "%4.12f\t", diff_Cai);
  fprintf(file, "%4.12f\t", diff_I);
  fprintf(file, "%4.12f\t", diff_Ki);
  fprintf(file, "%4.12f\t", diff_Nai);
  fprintf(file, "%4.12f\t", diff_O);
  fprintf(file, "%4.12f\t", diff_PLBp);
  fprintf(file, "%4.12f\t", diff_RI);
  fprintf(file, "%4.12f\t", diff_R_1);
  fprintf(file, "%4.12f\t", diff_cAMP);
  fprintf(file, "%4.12f\t", diff_fCMi);
  fprintf(file, "%4.12f\t", diff_fCMs);
  fprintf(file, "%4.12f\t", diff_fCQ);
  fprintf(file, "%4.12f\t", diff_fTC);
  fprintf(file, "%4.12f\t", diff_fTMC);
  fprintf(file, "%4.12f\t", diff_fTMM);
  fprintf(file, "%4.12f\t", diff_paF);
  fprintf(file, "%4.12f\t", diff_paS);
  fprintf(file, "%4.12f\t", diff_piy);
  fprintf(file, "%4.12f\t", do_);
  fprintf(file, "%4.12f\t", sv->fCMi);
  fprintf(file, "%4.12f\t", sv->fCMs);
  fprintf(file, "%4.12f\t", sv->fCQ);
  fprintf(file, "%4.12f\t", sv->fCa);
  fprintf(file, "%4.12f\t", sv->fL);
  fprintf(file, "%4.12f\t", sv->fT);
  fprintf(file, "%4.12f\t", sv->fTC);
  fprintf(file, "%4.12f\t", sv->fTMC);
  fprintf(file, "%4.12f\t", sv->fTMM);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->h_WT);
  fprintf(file, "%4.12f\t", j_Ca_dif);
  fprintf(file, "%4.12f\t", j_SRCarel);
  fprintf(file, "%4.12f\t", j_up);
  fprintf(file, "%4.12f\t", k1);
  fprintf(file, "%4.12f\t", k12);
  fprintf(file, "%4.12f\t", k14);
  fprintf(file, "%4.12f\t", k2);
  fprintf(file, "%4.12f\t", k21);
  fprintf(file, "%4.12f\t", k23);
  fprintf(file, "%4.12f\t", k3);
  fprintf(file, "%4.12f\t", k32);
  fprintf(file, "%4.12f\t", p->k34);
  fprintf(file, "%4.12f\t", k4);
  fprintf(file, "%4.12f\t", k41);
  fprintf(file, "%4.12f\t", k43);
  fprintf(file, "%4.12f\t", k5);
  fprintf(file, "%4.12f\t", kCaSR);
  fprintf(file, "%4.12f\t", kcch);
  fprintf(file, "%4.12f\t", kiSRCa);
  fprintf(file, "%4.12f\t", kiso);
  fprintf(file, "%4.12f\t", p->koCa);
  fprintf(file, "%4.12f\t", koSRCa);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->m_WT);
  fprintf(file, "%4.12f\t", sv->n);
  fprintf(file, "%4.12f\t", sv->paF);
  fprintf(file, "%4.12f\t", sv->paS);
  fprintf(file, "%4.12f\t", sv->piy);
  fprintf(file, "%4.12f\t", sv->q);
  fprintf(file, "%4.12f\t", sv->r);
  fprintf(file, "%4.12f\t", sv->r_Kur);
  fprintf(file, "%4.12f\t", sv->s_Kur);
  fprintf(file, "%4.12f\t", sv->x);
  fprintf(file, "%4.12f\t", x1);
  fprintf(file, "%4.12f\t", x2);
  fprintf(file, "%4.12f\t", x3);
  fprintf(file, "%4.12f\t", x4);
  fprintf(file, "%4.12f\t", sv->y);
  fprintf(file, "%4.12f\t", y_inf);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* FabbriIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void FabbriIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        