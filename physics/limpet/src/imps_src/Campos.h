// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Fernando O. Campos, Yohannes Shiferaw, Anton J. Prassl, Patrick M. Boyle, Edward J. Vigmond, Gernot Plank
*  Year: 2015
*  Title: Stochastic spontaneous calcium release events trigger premature ventricular complexes by overcoming electrotonic load
*  Journal: Cardiovascular Research, 107(1), 175-183
*  DOI: 10.1093/cvr/cvv149
*  Comment: Modified version of the Mahajan-Shiferaw rabbit ventricular myocyte model with spontaneous Ca release
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __CAMPOS_H__
#define __CAMPOS_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(CAMPOS_CPU_GENERATED)    || defined(CAMPOS_MLIR_CPU_GENERATED)    || defined(CAMPOS_MLIR_ROCM_GENERATED)    || defined(CAMPOS_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define CAMPOS_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define CAMPOS_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define CAMPOS_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define CAMPOS_CPU_GENERATED
#endif

namespace limpet {

#define Campos_REQDAT Vm_DATA_FLAG
#define Campos_MODDAT Iion_DATA_FLAG

struct Campos_Params {
    GlobalData_t CSR;
    GlobalData_t Cao;
    GlobalData_t INaCamax;
    GlobalData_t INaKmax;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t g_Ca;
    GlobalData_t g_K1;
    GlobalData_t g_Kr;
    GlobalData_t g_Ks;
    GlobalData_t g_Na;
    GlobalData_t g_RyR;
    GlobalData_t g_sp;
    GlobalData_t g_tof;
    GlobalData_t g_tos;
    GlobalData_t v_Na;
    GlobalData_t v_up;

};

struct Campos_state {
    GlobalData_t Cai;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t c1;
    GlobalData_t c2;
    GlobalData_t cj;
    GlobalData_t cjp;
    GlobalData_t cp;
    GlobalData_t cs;
    Gatetype h;
    GlobalData_t i1ba;
    GlobalData_t i1ca;
    GlobalData_t i2ba;
    GlobalData_t i2ca;
    Gatetype j;
    GlobalData_t j_rel;
    Gatetype m;
    GlobalData_t tropi;
    GlobalData_t trops;
    Gatetype xKr;
    Gatetype xs1;
    Gatetype xs2;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;

};

class CamposIonType : public IonType {
public:
    using IonIfDerived = IonIf<CamposIonType>;
    using params_type = Campos_Params;
    using state_type = Campos_state;

    CamposIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Campos(int, int, IonIfBase&, GlobalData_t**);
#ifdef CAMPOS_CPU_GENERATED
void compute_Campos_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef CAMPOS_MLIR_CPU_GENERATED
void compute_Campos_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef CAMPOS_MLIR_ROCM_GENERATED
void compute_Campos_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef CAMPOS_MLIR_CUDA_GENERATED
void compute_Campos_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
