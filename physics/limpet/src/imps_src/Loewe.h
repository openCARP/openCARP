// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Axel Loewe, Yannick Lutz, Alan Fabbri, Stefano Severi
*  Year: 2019
*  Title: Hypocalcemia-Induced Slowing of Human Sinus Node Pacemaking
*  Journal: Biophysical Journal, 117(12):2244-2254
*  DOI: 10.1016/j.bpj.2019.07.037
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __LOEWE_H__
#define __LOEWE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(LOEWE_CPU_GENERATED)    || defined(LOEWE_MLIR_CPU_GENERATED)    || defined(LOEWE_MLIR_ROCM_GENERATED)    || defined(LOEWE_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define LOEWE_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define LOEWE_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define LOEWE_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define LOEWE_CPU_GENERATED
#endif

namespace limpet {

#define Loewe_REQDAT Vm_DATA_FLAG
#define Loewe_MODDAT Iion_DATA_FLAG

struct Loewe_Params {
    GlobalData_t ACh;
    GlobalData_t ACh_block;
    GlobalData_t ACh_on;
    GlobalData_t ACh_shift;
    GlobalData_t C;
    GlobalData_t CM_tot;
    GlobalData_t CQ_tot;
    GlobalData_t Ca_intracellular_fluxes_b_up;
    GlobalData_t Ca_intracellular_fluxes_tau_dif_Ca;
    GlobalData_t Ca_intracellular_fluxes_tau_tr;
    GlobalData_t Cao;
    GlobalData_t EC50_SK;
    GlobalData_t EC50_SR;
    GlobalData_t F;
    GlobalData_t GKACh;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GKs_;
    GlobalData_t GKur;
    GlobalData_t GNa;
    GlobalData_t GNa_L;
    GlobalData_t GSK;
    GlobalData_t Gf_K;
    GlobalData_t Gf_Na;
    GlobalData_t Gto;
    GlobalData_t HSR;
    GlobalData_t ICaL_fCa_gate_alpha_fCa;
    GlobalData_t IKACh_a_gate_alpha_a;
    GlobalData_t INaK_max;
    GlobalData_t Iso_1_uM;
    GlobalData_t Iso_increase_1;
    GlobalData_t Iso_increase_2;
    GlobalData_t Iso_shift_1;
    GlobalData_t Iso_shift_2;
    GlobalData_t Iso_shift_dL;
    GlobalData_t Iso_slope_dL;
    GlobalData_t K1ni;
    GlobalData_t K1no;
    GlobalData_t K2ni;
    GlobalData_t K2no;
    GlobalData_t K3ni;
    GlobalData_t K3no;
    GlobalData_t K_NaCa;
    GlobalData_t K_up;
    GlobalData_t Kci;
    GlobalData_t Kcni;
    GlobalData_t Kco;
    GlobalData_t Km_Kp;
    GlobalData_t Km_Nap;
    GlobalData_t Km_fCa;
    GlobalData_t Ko;
    GlobalData_t L_cell;
    GlobalData_t L_sub;
    GlobalData_t MaxSR;
    GlobalData_t Mgi;
    GlobalData_t MinSR;
    GlobalData_t Nao;
    GlobalData_t P_CaL;
    GlobalData_t P_CaT;
    GlobalData_t P_up;
    GlobalData_t P_up_basal;
    GlobalData_t Qci;
    GlobalData_t Qco;
    GlobalData_t Qn;
    GlobalData_t RTONF;
    GlobalData_t R_2;
    GlobalData_t R_cell;
    GlobalData_t T;
    GlobalData_t TC_tot;
    GlobalData_t TMC_tot;
    GlobalData_t V_cell;
    GlobalData_t V_dL;
    GlobalData_t V_i;
    GlobalData_t V_i_part;
    GlobalData_t V_jsr;
    GlobalData_t V_jsr_part;
    GlobalData_t V_nsr;
    GlobalData_t V_nsr_part;
    GlobalData_t V_sub;
    GlobalData_t delta_m;
    GlobalData_t k34;
    GlobalData_t k_dL;
    GlobalData_t k_fL;
    GlobalData_t kb_CM;
    GlobalData_t kb_CQ;
    GlobalData_t kb_TC;
    GlobalData_t kb_TMC;
    GlobalData_t kb_TMM;
    GlobalData_t kf_CM;
    GlobalData_t kf_CQ;
    GlobalData_t kf_TC;
    GlobalData_t kf_TMC;
    GlobalData_t kf_TMM;
    GlobalData_t kiCa;
    GlobalData_t kim;
    GlobalData_t koCa;
    GlobalData_t kom;
    GlobalData_t ks;
    GlobalData_t n_SK;
    GlobalData_t offset_fT;
    GlobalData_t shift_fL;
    GlobalData_t slope_up;
    GlobalData_t y_shift;

};

struct Loewe_state {
    GlobalData_t Ca_jsr;
    GlobalData_t Ca_nsr;
    GlobalData_t Ca_sub;
    GlobalData_t Cai;
    GlobalData_t I;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t RI;
    GlobalData_t R_1;
    Gatetype a;
    Gatetype dL;
    Gatetype dT;
    GlobalData_t fCMi;
    GlobalData_t fCMs;
    GlobalData_t fCQ;
    GlobalData_t fCa;
    Gatetype fL;
    Gatetype fT;
    GlobalData_t fTC;
    GlobalData_t fTMC;
    GlobalData_t fTMM;
    Gatetype h;
    Gatetype m;
    Gatetype n;
    Gatetype paF;
    Gatetype paS;
    Gatetype piy;
    Gatetype q;
    Gatetype r;
    Gatetype r_Kur;
    Gatetype s_Kur;
    GlobalData_t x;
    Gatetype y;

};

class LoeweIonType : public IonType {
public:
    using IonIfDerived = IonIf<LoeweIonType>;
    using params_type = Loewe_Params;
    using state_type = Loewe_state;

    LoeweIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Loewe(int, int, IonIfBase&, GlobalData_t**);
#ifdef LOEWE_CPU_GENERATED
void compute_Loewe_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef LOEWE_MLIR_CPU_GENERATED
void compute_Loewe_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef LOEWE_MLIR_ROCM_GENERATED
void compute_Loewe_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef LOEWE_MLIR_CUDA_GENERATED
void compute_Loewe_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
