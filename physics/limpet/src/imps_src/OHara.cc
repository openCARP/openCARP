// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: O'Hara T, Virag L, Varro A, Rudy Y
*  Year: 2011
*  Title: Simulation of the Undiseased Human Cardiac Ventricular Action Potential: Model Formulation and Experimental Validation
*  Journal: PLOS Computational Biology, 7(5)
*  DOI: 10.1371/journal.pcbi.1002061
*  Comment: Sodium current can be changed by flag to the tenTusscherPanfilov formulation as done in Dutta et al. doi:10.1016/j.pbiomolbio.2017.02.007. Additionally, model modifications as suggested by Michael Clerx can be modifed by flags: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "OHara.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

OHaraIonType::OHaraIonType(bool plugin) : IonType(std::move(std::string("OHara")), plugin) {}

size_t OHaraIonType::params_size() const {
  return sizeof(struct OHara_Params);
}

size_t OHaraIonType::dlo_vector_size() const {

  return 1;
}

uint32_t OHaraIonType::reqdat() const {
  return OHara_REQDAT;
}

uint32_t OHaraIonType::moddat() const {
  return OHara_MODDAT;
}

void OHaraIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target OHaraIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef OHARA_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(OHARA_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(OHARA_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(OHARA_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef OHARA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef OHARA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef OHARA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef OHARA_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void OHaraIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef OHARA_MLIR_CUDA_GENERATED
      compute_OHara_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(OHARA_MLIR_ROCM_GENERATED)
      compute_OHara_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(OHARA_MLIR_CPU_GENERATED)
      compute_OHara_mlir_cpu(start, end, imp, data);
#   elif defined(OHARA_CPU_GENERATED)
      compute_OHara_cpu(start, end, imp, data);
#   else
#     error "Could not generate method OHaraIonType::compute."
#   endif
      break;
#   ifdef OHARA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_OHara_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef OHARA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_OHara_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef OHARA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_OHara_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef OHARA_CPU_GENERATED
    case Target::CPU:
      compute_OHara_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Aff (GlobalData_t)(0.6)
#define Ahf (GlobalData_t)(0.99)
#define BSLmax (GlobalData_t)(1.124)
#define BSRmax (GlobalData_t)(0.047)
#define CLERX (GlobalData_t)(1.)
#define CaMKo (GlobalData_t)(0.05)
#define CaMKt_init (GlobalData_t)(0.0)
#define Cai_init (GlobalData_t)(1.0e-4)
#define ENDO (GlobalData_t)(0.)
#define EPI (GlobalData_t)(1.)
#define F (GlobalData_t)(96485.0)
#define Jrelnp_init (GlobalData_t)(0.0)
#define Jrelp_init (GlobalData_t)(0.0)
#define KKo (GlobalData_t)(0.3582)
#define KNao0 (GlobalData_t)(27.78)
#define Khp (GlobalData_t)(1.698e-7)
#define Ki_init (GlobalData_t)(145.0)
#define Kki (GlobalData_t)(0.5)
#define KmBSL (GlobalData_t)(0.0087)
#define KmBSR (GlobalData_t)(0.00087)
#define KmCaAct_i (GlobalData_t)(150.0e-6)
#define KmCaAct_ss (GlobalData_t)(150.0e-6)
#define KmCaM (GlobalData_t)(0.0015)
#define KmCaMK (GlobalData_t)(0.15)
#define Kmgatp (GlobalData_t)(1.698e-7)
#define Kmn (GlobalData_t)(0.002)
#define Knai0 (GlobalData_t)(9.073)
#define Knap (GlobalData_t)(224.0)
#define Kxkur (GlobalData_t)(292.0)
#define L (GlobalData_t)(0.01)
#define MCELL (GlobalData_t)(2.)
#define MgADP (GlobalData_t)(0.05)
#define MgATP (GlobalData_t)(9.8)
#define Nai_init (GlobalData_t)(7.0)
#define ORIGINAL (GlobalData_t)(0.)
#define ORdINa (GlobalData_t)(1.)
#define PCab (GlobalData_t)(2.5e-8)
#define PKNa (GlobalData_t)(0.01833)
#define PNab (GlobalData_t)(3.75e-10)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(310.0)
#define TT2INa (GlobalData_t)(0.)
#define aCaMK (GlobalData_t)(0.05)
#define a_init (GlobalData_t)(0.0)
#define ap_init (GlobalData_t)(0.0)
#define bCaMK (GlobalData_t)(0.00068)
#define bt (GlobalData_t)(4.75)
#define cansr_init (GlobalData_t)(1.2)
#define csqnmax (GlobalData_t)(10.0)
#define d_init (GlobalData_t)(0.0)
#define delta (GlobalData_t)(-0.1550)
#define eP (GlobalData_t)(4.2)
#define fcaf_init (GlobalData_t)(1.0)
#define fcafp_init (GlobalData_t)(1.0)
#define fcas_init (GlobalData_t)(1.0)
#define ff_init (GlobalData_t)(1.0)
#define ffp_init (GlobalData_t)(1.0)
#define fs_init (GlobalData_t)(1.0)
#define hL_init (GlobalData_t)(1.0)
#define hLp_init (GlobalData_t)(1.0)
#define hf_init (GlobalData_t)(1.0)
#define hs_init (GlobalData_t)(1.0)
#define hsp_init (GlobalData_t)(1.0)
#define iF_init (GlobalData_t)(1.0)
#define iFp_init (GlobalData_t)(1.0)
#define iS_init (GlobalData_t)(1.0)
#define iSp_init (GlobalData_t)(1.0)
#define j_init (GlobalData_t)(1.0)
#define jca_init (GlobalData_t)(1.0)
#define jp_init (GlobalData_t)(1.0)
#define k1m (GlobalData_t)(182.4)
#define k1p (GlobalData_t)(949.5)
#define k2m (GlobalData_t)(39.4)
#define k2n (GlobalData_t)(1000.0)
#define k2p (GlobalData_t)(687.2)
#define k3m (GlobalData_t)(79300.0)
#define k3p (GlobalData_t)(1899.0)
#define k4m (GlobalData_t)(40.0)
#define k4p (GlobalData_t)(639.0)
#define kCaoff (GlobalData_t)(5.0e3)
#define kCaon (GlobalData_t)(1.5e6)
#define kasymm (GlobalData_t)(12.5)
#define kmcmdn (GlobalData_t)(0.00238)
#define kmcsqn (GlobalData_t)(0.8)
#define kmtrpn (GlobalData_t)(0.0005)
#define kna1 (GlobalData_t)(15.0)
#define kna2 (GlobalData_t)(5.0)
#define kna3 (GlobalData_t)(88.12)
#define mL_init (GlobalData_t)(0.0)
#define m_init (GlobalData_t)(0.0)
#define nca_init (GlobalData_t)(0.0)
#define qca (GlobalData_t)(0.1670)
#define qna (GlobalData_t)(0.5224)
#define rad (GlobalData_t)(0.0011)
#define scale_GNa (GlobalData_t)(0.37)
#define tau_hL (GlobalData_t)(200.0)
#define tau_jca (GlobalData_t)(75.0)
#define trpnmax (GlobalData_t)(0.07)
#define v_init (GlobalData_t)(-87.)
#define wca (GlobalData_t)(6.0e4)
#define wna (GlobalData_t)(6.0e4)
#define wnaca (GlobalData_t)(5.0e3)
#define xk1_init (GlobalData_t)(1.0)
#define xrf_init (GlobalData_t)(0.0)
#define xrs_init (GlobalData_t)(0.0)
#define xs1_init (GlobalData_t)(0.0)
#define xs2_init (GlobalData_t)(0.0)
#define zca (GlobalData_t)(2.0)
#define zk (GlobalData_t)(1.0)
#define zna (GlobalData_t)(1.0)
#define Afs (GlobalData_t)((1.0-(Aff)))
#define Ageo (GlobalData_t)(((((2.*3.14)*rad)*rad)+(((2.*3.14)*rad)*L)))
#define Ahs (GlobalData_t)((1.0-(Ahf)))
#define a2 (GlobalData_t)(k2p)
#define a4 (GlobalData_t)((((k4p*MgATP)/Kmgatp)/(1.0+(MgATP/Kmgatp))))
#define a_rel (GlobalData_t)((0.5*bt))
#define b1 (GlobalData_t)((k1m*MgADP))
#define btp (GlobalData_t)((1.25*bt))
#define k2_i (GlobalData_t)(kCaoff)
#define k2_ss (GlobalData_t)(kCaoff)
#define k5_i (GlobalData_t)(kCaoff)
#define k5_ss (GlobalData_t)(kCaoff)
#define tau_hLp (GlobalData_t)((3.0*tau_hL))
#define vcell (GlobalData_t)(((((1000.*3.14)*rad)*rad)*L))
#define Acap (GlobalData_t)((2.*Ageo))
#define a_relp (GlobalData_t)((0.5*btp))
#define vjsr (GlobalData_t)((0.0048*vcell))
#define vmyo (GlobalData_t)((0.68*vcell))
#define vnsr (GlobalData_t)((0.0552*vcell))
#define vss (GlobalData_t)((0.02*vcell))



void OHaraIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  OHara_Params *p = imp.params();

  // Compute the regional constants
  {
    p->Cao = 1.8;
    p->GpCa = 0.0005;
    p->INa_Type = ORdINa;
    if (0) ;
    else if (flag_set(p->flags, "TT2INa")) p->INa_Type = TT2INa;
    else if (flag_set(p->flags, "ORdINa")) p->INa_Type = ORdINa;
    p->Ko = 5.4;
    p->Nao = 140.0;
    p->celltype = ENDO;
    if (0) ;
    else if (flag_set(p->flags, "ENDO")) p->celltype = ENDO;
    else if (flag_set(p->flags, "EPI")) p->celltype = EPI;
    else if (flag_set(p->flags, "MCELL")) p->celltype = MCELL;
    p->factorICaK = 1.0;
    p->factorICaL = 1.0;
    p->factorICaNa = 1.0;
    p->factorINaCa = 1.;
    p->factorINaCass = 1.0;
    p->factorINaK = 1.;
    p->factorIbCa = 1.;
    p->factorIbNa = 1.;
    p->jrel_stiff_const = 0.005;
    p->modelformulation = ORIGINAL;
    if (0) ;
    else if (flag_set(p->flags, "ORIGINAL")) p->modelformulation = ORIGINAL;
    else if (flag_set(p->flags, "CLERX")) p->modelformulation = CLERX;
    p->scale_tau_m = 0.3;
    p->shift_m_inf = -8.0;
    p->vHalfXs = 11.60;
    p->GK1 = ((p->celltype==EPI) ? (0.1908*1.2) : ((p->celltype==MCELL) ? (0.1908*1.3) : 0.1908));
    p->GKb = ((p->celltype==EPI) ? (0.003*0.6) : 0.003);
    p->GKr = ((p->celltype==EPI) ? (0.046*1.3) : ((p->celltype==MCELL) ? (0.046*0.8) : 0.046));
    p->GKs = ((p->celltype==EPI) ? (0.0034*1.4) : 0.0034);
    p->GNa = ((p->INa_Type==TT2INa) ? 14.838 : (75.*scale_GNa));
    p->GNaCa = ((p->celltype==EPI) ? (0.0008*1.1) : ((p->celltype==MCELL) ? (0.0008*1.4) : 0.0008));
    p->GNaL = ((p->celltype==EPI) ? (0.0075*0.6) : 0.0075);
    p->Gto = ((p->celltype==EPI) ? (0.02*4.0) : ((p->celltype==MCELL) ? (0.02*4.0) : 0.02));
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  v_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum Cai_TableIndex {
  Bcai_idx,
  IpCa_idx,
  Jupnp_idx,
  Jupp_idx,
  KsCa_idx,
  allo_i_idx,
  NROWS_Cai
};

enum v_TableIndex {
  Afcaf_idx,
  Afcas_idx,
  AiF_idx,
  AiS_idx,
  Axrf_idx,
  Axrs_idx,
  Knai_idx,
  a3_idx,
  a_rush_larsen_A_idx,
  a_rush_larsen_B_idx,
  ap_rush_larsen_A_idx,
  ap_rush_larsen_B_idx,
  b2_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  exp_2vfrt_idx,
  exp_vfrt_idx,
  fcaf_rush_larsen_A_idx,
  fcaf_rush_larsen_B_idx,
  fcafp_rush_larsen_A_idx,
  fcafp_rush_larsen_B_idx,
  fcas_rush_larsen_A_idx,
  fcas_rush_larsen_B_idx,
  ff_rush_larsen_A_idx,
  ff_rush_larsen_B_idx,
  ffp_rush_larsen_A_idx,
  ffp_rush_larsen_B_idx,
  fs_rush_larsen_A_idx,
  fs_rush_larsen_B_idx,
  hL_rush_larsen_A_idx,
  hLp_rush_larsen_A_idx,
  hTT2_rush_larsen_A_idx,
  hTT2_rush_larsen_B_idx,
  hca_idx,
  hf_rush_larsen_A_idx,
  hf_rush_larsen_B_idx,
  hna_idx,
  hs_rush_larsen_A_idx,
  hs_rush_larsen_B_idx,
  hsp_rush_larsen_A_idx,
  hsp_rush_larsen_B_idx,
  iF_rush_larsen_A_idx,
  iF_rush_larsen_B_idx,
  iFp_rush_larsen_A_idx,
  iFp_rush_larsen_B_idx,
  iS_rush_larsen_A_idx,
  iS_rush_larsen_B_idx,
  iSp_rush_larsen_A_idx,
  iSp_rush_larsen_B_idx,
  jTT2_rush_larsen_A_idx,
  jTT2_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  jca_rush_larsen_A_idx,
  jp_rush_larsen_A_idx,
  jp_rush_larsen_B_idx,
  k3_i_idx,
  k3_ss_idx,
  k3pp_i_idx,
  k3pp_ss_idx,
  k8_i_idx,
  k8_ss_idx,
  mL_rush_larsen_A_idx,
  mL_rush_larsen_B_idx,
  mORd_rush_larsen_A_idx,
  mORd_rush_larsen_B_idx,
  mTT2_rush_larsen_A_idx,
  mTT2_rush_larsen_B_idx,
  rk1_idx,
  rkr_idx,
  vffrt_expm1_2vfrt_idx,
  vffrt_expm1_vfrt_idx,
  xk1_rush_larsen_A_idx,
  xk1_rush_larsen_B_idx,
  xkb_idx,
  xrf_rush_larsen_A_idx,
  xrf_rush_larsen_B_idx,
  xrs_rush_larsen_A_idx,
  xrs_rush_larsen_B_idx,
  xs1_rush_larsen_A_idx,
  xs1_rush_larsen_B_idx,
  xs2_rush_larsen_A_idx,
  xs2_rush_larsen_B_idx,
  NROWS_v
};



void OHaraIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  OHara_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double H = ((p->modelformulation==CLERX) ? 1.0e-4 : 1.0e-7);
  double PCa = ((p->celltype==EPI) ? (0.0001*1.2) : ((p->celltype==MCELL) ? (0.0001*2.5) : 0.0001));
  double Pnak = ((p->celltype==EPI) ? (30.*0.9) : ((p->celltype==MCELL) ? (30.*0.7) : 30.));
  double cmdnmax = ((p->celltype==EPI) ? (0.05*1.3) : 0.05);
  double h10_i = ((kasymm+1.0)+((p->Nao/kna1)*(1.0+(p->Nao/kna2))));
  double h10_ss = ((kasymm+1.0)+((p->Nao/kna1)*(1.+(p->Nao/kna2))));
  double hL_rush_larsen_B = (exp(((-dt)/tau_hL)));
  double hL_rush_larsen_C = (expm1(((-dt)/tau_hL)));
  double hLp_rush_larsen_B = (exp(((-dt)/tau_hLp)));
  double hLp_rush_larsen_C = (expm1(((-dt)/tau_hLp)));
  double jca_rush_larsen_B = (exp(((-dt)/tau_jca)));
  double jca_rush_larsen_C = (expm1(((-dt)/tau_jca)));
  double sqrt_Ko = (sqrt(p->Ko));
  double sqrt_Ko_54 = (sqrt((p->Ko/5.4)));
  double PCaK = (3.574e-4*PCa);
  double PCaNa = (0.00125*PCa);
  double PCap = (1.1*PCa);
  double h11_i = ((p->Nao*p->Nao)/((h10_i*kna1)*kna2));
  double h11_ss = ((p->Nao*p->Nao)/((h10_ss*kna1)*kna2));
  double h12_i = (1.0/h10_i);
  double h12_ss = (1.0/h10_ss);
  double PCaKp = (3.574e-4*PCap);
  double PCaNap = (0.00125*PCap);
  double k1_i = ((h12_i*p->Cao)*kCaon);
  double k1_ss = ((h12_ss*p->Cao)*kCaon);
  
  // Create the Cai lookup table
  LUT* Cai_tab = &imp.tables()[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-6, 1e-2, 1e-6, "OHara Cai", imp.get_target());
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[Bcai_idx] = (1.0/((1.0+(((cmdnmax*kmcmdn)/(kmcmdn+Cai))/(kmcmdn+Cai)))+(((trpnmax*kmtrpn)/(kmtrpn+Cai))/(kmtrpn+Cai))));
    Cai_row[IpCa_idx] = ((p->GpCa*Cai)/(0.0005+Cai));
    Cai_row[Jupnp_idx] = ((p->celltype==EPI) ? (((0.004375*Cai)/(Cai+0.00092))*1.3) : ((0.004375*Cai)/(Cai+0.00092)));
    Cai_row[Jupp_idx] = ((p->celltype==EPI) ? ((((2.75*0.004375)*Cai)/((Cai+0.00092)-(0.00017)))*1.3) : (((2.75*0.004375)*Cai)/((Cai+0.00092)-(0.00017))));
    Cai_row[KsCa_idx] = (1.0+(0.6/(1.0+(pow((3.8e-5/Cai),1.4)))));
    Cai_row[allo_i_idx] = (1.0/(1.0+((KmCaAct_i/Cai)*(KmCaAct_i/Cai))));
  }
  check_LUT(Cai_tab);
  
  
  // Create the v lookup table
  LUT* v_tab = &imp.tables()[v_TAB];
  LUT_alloc(v_tab, NROWS_v, -1000, 1000, 1e-2, "OHara v", imp.get_target());
  for (int __i=v_tab->mn_ind; __i<=v_tab->mx_ind; __i++) {
    double v = v_tab->res*__i;
    LUT_data_t* v_row = v_tab->tab[__i];
    v_row[Afcaf_idx] = (0.3+(0.6/(1.0+(exp(((v-(10.0))/10.0))))));
    v_row[AiF_idx] = (1.0/(1.0+(exp(((v-(213.6))/151.2)))));
    v_row[Axrf_idx] = (1.0/(1.0+(exp(((v+54.81)/38.21)))));
    double KNao = (KNao0*(exp(((((1.0-(delta))*v)*F)/((3.0*R)*T)))));
    v_row[Knai_idx] = (Knai0*(exp((((delta*v)*F)/((3.0*R)*T)))));
    double a_inf = (1.0/(1.0+(exp(((-(v-(14.34)))/14.82)))));
    double aa_hTT2 = ((v>=-40.) ? 0. : (0.057*(exp(((-(v+80.))/6.8)))));
    double aa_jTT2 = ((v>=-40.) ? 0. : ((((-2.5428e4*(exp((0.2444*v))))-((6.948e-6*(exp((-0.04391*v))))))*(v+37.78))/(1.+(exp((0.311*(v+79.23)))))));
    double aa_mTT2 = (1./(1.+(exp(((-60.-(v))/5.)))));
    double ap_inf = (1.0/(1.0+(exp(((-(v-(24.34)))/14.82)))));
    double bb_hTT2 = ((v>=-40.) ? (0.77/(0.13*(1.+(exp(((-(v+10.66))/11.1)))))) : ((2.7*(exp((0.079*v))))+(3.1e5*(exp((0.3485*v))))));
    double bb_jTT2 = ((v>=-40.) ? ((0.6*(exp((0.057*v))))/(1.+(exp((-0.1*(v+32.)))))) : ((0.02424*(exp((-0.01052*v))))/(1.+(exp((-0.1378*(v+40.14)))))));
    double bb_mTT2 = ((0.1/(1.+(exp(((v+35.)/5.)))))+(0.10/(1.+(exp(((v-(50.))/200.))))));
    double d_inf = (1.0/(1.0+(exp(((-(v+3.940))/4.230)))));
    double delta_epi = ((p->celltype==EPI) ? (1.0-((0.95/(1.0+(exp(((v+70.0)/5.0))))))) : 1.0);
    double dti_develop = (1.354+(1.0e-4/((exp(((v-(167.4))/15.89)))+(exp(((-(v-(12.23)))/0.2154))))));
    double dti_recover = (1.0-((0.5/(1.0+(exp(((v+70.0)/20.0)))))));
    double f_inf = (1.0/(1.0+(exp(((v+19.58)/3.696)))));
    double hL_inf = (1.0/(1.0+(exp(((v+87.61)/7.488)))));
    double hLp_inf = (1.0/(1.0+(exp(((v+93.81)/7.488)))));
    double hTT2_inf = (1./((1.+(exp(((v+71.55)/7.43))))*(1.+(exp(((v+71.55)/7.43))))));
    double h_inf = (1.0/(1.+(exp(((v+82.90)/6.086)))));
    v_row[hca_idx] = (exp((((qca*v)*F)/(R*T))));
    v_row[hna_idx] = (exp((((qna*v)*F)/(R*T))));
    double hsp_inf = (1.0/(1.+(exp(((v+89.1)/6.086)))));
    double i_inf = (1.0/(1.0+(exp(((v+43.94)/5.711)))));
    double mL_inf = (1.0/(1.0+(exp(((-(v+42.85))/5.264)))));
    double mORd_inf = (1.0/(1.0+(exp(((-((v+39.57)-(p->shift_m_inf)))/9.871)))));
    double mTT2_inf = (1./((1.+(exp(((-56.86-(v))/9.03))))*(1.+(exp(((-56.86-(v))/9.03))))));
    v_row[rk1_idx] = (1.0/(1.0+(exp((((v+105.8)-((2.6*p->Ko)))/9.493)))));
    v_row[rkr_idx] = ((1.0/(1.0+(exp(((v+55.0)/75.0)))))/(1.0+(exp(((v-(10.0))/30.0)))));
    double tau_a = (1.0515/((1.0/(1.2089*(1.0+(exp(((-(v-(18.4099)))/29.3814))))))+(3.5/(1.0+(exp(((v+100.0)/29.3814)))))));
    double tau_d = (0.6+(1.0/((exp((-0.05*(v+6.0))))+(exp((0.09*(v+14.0)))))));
    double tau_fcaf = (7.0+(1.0/((0.04*(exp(((-(v-(4.0)))/7.0))))+(0.04*(exp(((v-(4.0))/7.0)))))));
    double tau_fcas = (100.0+(1.0/((0.00012*(exp(((-v)/3.0))))+(0.00012*(exp((v/7.0)))))));
    double tau_ff = (7.0+(1.0/((0.0045*(exp(((-(v+20.0))/10.0))))+(0.0045*(exp(((v+20.0)/10.0)))))));
    double tau_fs = (1000.0+(1.0/((0.000035*(exp(((-(v+5.0))/4.0))))+(0.000035*(exp(((v+5.0)/6.0)))))));
    double tau_hf = (1.0/((1.432e-5*(exp(((-(v+1.196))/6.285))))+(6.149*(exp(((v+0.5096)/20.27))))));
    double tau_hs = (1.0/((0.009794*(exp(((-(v+17.95))/28.05))))+(0.3343*(exp(((v+5.730)/56.66))))));
    double tau_j = (2.038+(1.0/((0.02136*(exp(((-(v+100.6))/8.281))))+(0.3052*(exp(((v+0.9941)/38.45)))))));
    double tau_mORd = (p->scale_tau_m/((6.765*(exp(((v+11.64)/34.77))))+(8.552*(exp(((-(v+77.42))/5.955))))));
    double tau_xk1 = (122.2/((exp(((-(v+127.2))/20.36)))+(exp(((v+236.8)/69.33)))));
    double tau_xrf = (12.98+(1.0/((0.3652*(exp(((v-(31.66))/3.869))))+(4.123e-5*(exp(((-(v-(47.78)))/20.38)))))));
    double tau_xrs = (1.865+(1.0/((0.06629*(exp(((v-(34.70))/7.355))))+(1.128e-5*(exp(((-(v-(29.74)))/25.94)))))));
    double tau_xs1 = (817.3+(1.0/((2.326e-4*(exp(((v+48.28)/17.80))))+(0.001292*(exp(((-(v+210.0))/230.0)))))));
    double tau_xs2 = (1.0/((0.01*(exp(((v-(50.0))/20.0))))+(0.0193*(exp(((-(v+66.54))/31.0))))));
    double vffrt = (((v*F)*F)/(R*T));
    double vfrt = ((v*F)/(R*T));
    double xk1_inf = (1.0/(1.0+(exp(((-((v+(2.5538*p->Ko))+144.59))/((1.5692*p->Ko)+3.8115))))));
    v_row[xkb_idx] = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
    double xr_inf = (1.0/(1.0+(exp(((-(v+8.337))/6.789)))));
    double xs1_inf = (1.0/(1.0+(exp(((-(v+p->vHalfXs))/8.932)))));
    v_row[Afcas_idx] = (1.0-(v_row[Afcaf_idx]));
    v_row[AiS_idx] = (1.0-(v_row[AiF_idx]));
    v_row[Axrs_idx] = (1.0-(v_row[Axrf_idx]));
    double Nao_KNao_3 = (((p->Nao/KNao)*(p->Nao/KNao))*(p->Nao/KNao));
    double Nao_KNao_p1_3 = (((1.0+(p->Nao/KNao))*(1.0+(p->Nao/KNao)))*(1.0+(p->Nao/KNao)));
    v_row[a_rush_larsen_B_idx] = (exp(((-dt)/tau_a)));
    double a_rush_larsen_C = (expm1(((-dt)/tau_a)));
    v_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    v_row[exp_2vfrt_idx] = (exp((2.0*vfrt)));
    v_row[exp_vfrt_idx] = (exp(vfrt));
    double fca_inf = f_inf;
    v_row[fcaf_rush_larsen_B_idx] = (exp(((-dt)/tau_fcaf)));
    double fcaf_rush_larsen_C = (expm1(((-dt)/tau_fcaf)));
    v_row[fcas_rush_larsen_B_idx] = (exp(((-dt)/tau_fcas)));
    double fcas_rush_larsen_C = (expm1(((-dt)/tau_fcas)));
    double ff_inf = f_inf;
    v_row[ff_rush_larsen_B_idx] = (exp(((-dt)/tau_ff)));
    double ff_rush_larsen_C = (expm1(((-dt)/tau_ff)));
    double ffp_inf = f_inf;
    double fs_inf = f_inf;
    v_row[fs_rush_larsen_B_idx] = (exp(((-dt)/tau_fs)));
    double fs_rush_larsen_C = (expm1(((-dt)/tau_fs)));
    double h7_i = (1.0+((p->Nao/kna3)*(1.0+(1.0/v_row[hna_idx]))));
    double h7_ss = (1.0+((p->Nao/kna3)*(1.0+(1.0/v_row[hna_idx]))));
    v_row[hL_rush_larsen_A_idx] = ((-hL_inf)*hL_rush_larsen_C);
    v_row[hLp_rush_larsen_A_idx] = ((-hLp_inf)*hLp_rush_larsen_C);
    double hf_inf = h_inf;
    v_row[hf_rush_larsen_B_idx] = (exp(((-dt)/tau_hf)));
    double hf_rush_larsen_C = (expm1(((-dt)/tau_hf)));
    double hs_inf = h_inf;
    v_row[hs_rush_larsen_B_idx] = (exp(((-dt)/tau_hs)));
    double hs_rush_larsen_C = (expm1(((-dt)/tau_hs)));
    double iF_inf = i_inf;
    double iFp_inf = i_inf;
    double iS_inf = i_inf;
    double iSp_inf = i_inf;
    double jTT2_inf = hTT2_inf;
    double j_inf = h_inf;
    v_row[j_rush_larsen_B_idx] = (exp(((-dt)/tau_j)));
    double j_rush_larsen_C = (expm1(((-dt)/tau_j)));
    v_row[mORd_rush_larsen_B_idx] = (exp(((-dt)/tau_mORd)));
    double mORd_rush_larsen_C = (expm1(((-dt)/tau_mORd)));
    double tau_ap = tau_a;
    double tau_fcafp = (2.5*tau_fcaf);
    double tau_ffp = (2.5*tau_ff);
    double tau_hTT2 = (1.0/(aa_hTT2+bb_hTT2));
    double tau_hsp = (3.0*tau_hs);
    double tau_iF = ((4.562+(1./((0.3933*(exp(((-(v+100.0))/100.0))))+(0.08004*(exp(((v+50.0)/16.59)))))))*delta_epi);
    double tau_iS = ((23.62+(1./((0.001416*(exp(((-(v+96.52))/59.05))))+(1.780e-8*(exp(((v+114.1)/8.079)))))))*delta_epi);
    double tau_jTT2 = (1.0/(aa_jTT2+bb_jTT2));
    double tau_jp = (1.46*tau_j);
    double tau_mTT2 = (aa_mTT2*bb_mTT2);
    v_row[vffrt_expm1_2vfrt_idx] = ((v==0.) ? (F/2.) : (vffrt/(expm1((2.0*vfrt)))));
    v_row[vffrt_expm1_vfrt_idx] = ((v==0.) ? F : (vffrt/(expm1(vfrt))));
    v_row[xk1_rush_larsen_B_idx] = (exp(((-dt)/tau_xk1)));
    double xk1_rush_larsen_C = (expm1(((-dt)/tau_xk1)));
    double xrf_inf = xr_inf;
    v_row[xrf_rush_larsen_B_idx] = (exp(((-dt)/tau_xrf)));
    double xrf_rush_larsen_C = (expm1(((-dt)/tau_xrf)));
    double xrs_inf = xr_inf;
    v_row[xrs_rush_larsen_B_idx] = (exp(((-dt)/tau_xrs)));
    double xrs_rush_larsen_C = (expm1(((-dt)/tau_xrs)));
    v_row[xs1_rush_larsen_B_idx] = (exp(((-dt)/tau_xs1)));
    double xs1_rush_larsen_C = (expm1(((-dt)/tau_xs1)));
    double xs2_inf = xs1_inf;
    v_row[xs2_rush_larsen_B_idx] = (exp(((-dt)/tau_xs2)));
    double xs2_rush_larsen_C = (expm1(((-dt)/tau_xs2)));
    v_row[a3_idx] = (((k3p*(p->Ko/KKo))*(p->Ko/KKo))/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
    v_row[a_rush_larsen_A_idx] = ((-a_inf)*a_rush_larsen_C);
    v_row[ap_rush_larsen_B_idx] = (exp(((-dt)/tau_ap)));
    double ap_rush_larsen_C = (expm1(((-dt)/tau_ap)));
    v_row[b2_idx] = ((k2m*Nao_KNao_3)/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
    v_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    double fcaf_inf = fca_inf;
    double fcafp_inf = fca_inf;
    v_row[fcafp_rush_larsen_B_idx] = (exp(((-dt)/tau_fcafp)));
    double fcafp_rush_larsen_C = (expm1(((-dt)/tau_fcafp)));
    double fcas_inf = fca_inf;
    v_row[ff_rush_larsen_A_idx] = ((-ff_inf)*ff_rush_larsen_C);
    v_row[ffp_rush_larsen_B_idx] = (exp(((-dt)/tau_ffp)));
    double ffp_rush_larsen_C = (expm1(((-dt)/tau_ffp)));
    v_row[fs_rush_larsen_A_idx] = ((-fs_inf)*fs_rush_larsen_C);
    double h8_i = (p->Nao/((kna3*v_row[hna_idx])*h7_i));
    double h8_ss = (p->Nao/((kna3*v_row[hna_idx])*h7_ss));
    double h9_i = (1.0/h7_i);
    double h9_ss = (1.0/h7_ss);
    v_row[hTT2_rush_larsen_B_idx] = (exp(((-dt)/tau_hTT2)));
    double hTT2_rush_larsen_C = (expm1(((-dt)/tau_hTT2)));
    v_row[hf_rush_larsen_A_idx] = ((-hf_inf)*hf_rush_larsen_C);
    v_row[hs_rush_larsen_A_idx] = ((-hs_inf)*hs_rush_larsen_C);
    v_row[hsp_rush_larsen_B_idx] = (exp(((-dt)/tau_hsp)));
    double hsp_rush_larsen_C = (expm1(((-dt)/tau_hsp)));
    v_row[iF_rush_larsen_B_idx] = (exp(((-dt)/tau_iF)));
    double iF_rush_larsen_C = (expm1(((-dt)/tau_iF)));
    v_row[iS_rush_larsen_B_idx] = (exp(((-dt)/tau_iS)));
    double iS_rush_larsen_C = (expm1(((-dt)/tau_iS)));
    v_row[jTT2_rush_larsen_B_idx] = (exp(((-dt)/tau_jTT2)));
    double jTT2_rush_larsen_C = (expm1(((-dt)/tau_jTT2)));
    v_row[j_rush_larsen_A_idx] = ((-j_inf)*j_rush_larsen_C);
    double jca_inf = fca_inf;
    double jp_inf = j_inf;
    v_row[jp_rush_larsen_B_idx] = (exp(((-dt)/tau_jp)));
    double jp_rush_larsen_C = (expm1(((-dt)/tau_jp)));
    v_row[mORd_rush_larsen_A_idx] = ((-mORd_inf)*mORd_rush_larsen_C);
    v_row[mTT2_rush_larsen_B_idx] = (exp(((-dt)/tau_mTT2)));
    double mTT2_rush_larsen_C = (expm1(((-dt)/tau_mTT2)));
    double tau_iFp = ((dti_develop*dti_recover)*tau_iF);
    double tau_iSp = ((dti_develop*dti_recover)*tau_iS);
    double tau_m = ((p->INa_Type==TT2INa) ? tau_mTT2 : tau_mORd);
    v_row[xk1_rush_larsen_A_idx] = ((-xk1_inf)*xk1_rush_larsen_C);
    v_row[xrf_rush_larsen_A_idx] = ((-xrf_inf)*xrf_rush_larsen_C);
    v_row[xrs_rush_larsen_A_idx] = ((-xrs_inf)*xrs_rush_larsen_C);
    v_row[xs1_rush_larsen_A_idx] = ((-xs1_inf)*xs1_rush_larsen_C);
    v_row[xs2_rush_larsen_A_idx] = ((-xs2_inf)*xs2_rush_larsen_C);
    v_row[ap_rush_larsen_A_idx] = ((-ap_inf)*ap_rush_larsen_C);
    v_row[fcaf_rush_larsen_A_idx] = ((-fcaf_inf)*fcaf_rush_larsen_C);
    v_row[fcafp_rush_larsen_A_idx] = ((-fcafp_inf)*fcafp_rush_larsen_C);
    v_row[fcas_rush_larsen_A_idx] = ((-fcas_inf)*fcas_rush_larsen_C);
    v_row[ffp_rush_larsen_A_idx] = ((-ffp_inf)*ffp_rush_larsen_C);
    v_row[hTT2_rush_larsen_A_idx] = ((-hTT2_inf)*hTT2_rush_larsen_C);
    v_row[hsp_rush_larsen_A_idx] = ((-hsp_inf)*hsp_rush_larsen_C);
    v_row[iF_rush_larsen_A_idx] = ((-iF_inf)*iF_rush_larsen_C);
    v_row[iFp_rush_larsen_B_idx] = (exp(((-dt)/tau_iFp)));
    double iFp_rush_larsen_C = (expm1(((-dt)/tau_iFp)));
    v_row[iS_rush_larsen_A_idx] = ((-iS_inf)*iS_rush_larsen_C);
    v_row[iSp_rush_larsen_B_idx] = (exp(((-dt)/tau_iSp)));
    double iSp_rush_larsen_C = (expm1(((-dt)/tau_iSp)));
    v_row[jTT2_rush_larsen_A_idx] = ((-jTT2_inf)*jTT2_rush_larsen_C);
    v_row[jca_rush_larsen_A_idx] = ((-jca_inf)*jca_rush_larsen_C);
    v_row[jp_rush_larsen_A_idx] = ((-jp_inf)*jp_rush_larsen_C);
    double k3p_i = (h9_i*wca);
    double k3p_ss = (h9_ss*wca);
    v_row[k3pp_i_idx] = (h8_i*wnaca);
    v_row[k3pp_ss_idx] = (h8_ss*wnaca);
    v_row[k8_i_idx] = ((h8_i*h11_i)*wna);
    v_row[k8_ss_idx] = ((h8_ss*h11_ss)*wna);
    v_row[mTT2_rush_larsen_A_idx] = ((-mTT2_inf)*mTT2_rush_larsen_C);
    double tau_mL = tau_m;
    v_row[iFp_rush_larsen_A_idx] = ((-iFp_inf)*iFp_rush_larsen_C);
    v_row[iSp_rush_larsen_A_idx] = ((-iSp_inf)*iSp_rush_larsen_C);
    v_row[k3_i_idx] = (k3p_i+v_row[k3pp_i_idx]);
    v_row[k3_ss_idx] = (k3p_ss+v_row[k3pp_ss_idx]);
    v_row[mL_rush_larsen_B_idx] = (exp(((-dt)/tau_mL)));
    double mL_rush_larsen_C = (expm1(((-dt)/tau_mL)));
    v_row[mL_rush_larsen_A_idx] = ((-mL_inf)*mL_rush_larsen_C);
  }
  check_LUT(v_tab);
  

}



void OHaraIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  OHara_Params *p = imp.params();

  OHara_state *sv_base = (OHara_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double H = ((p->modelformulation==CLERX) ? 1.0e-4 : 1.0e-7);
  double PCa = ((p->celltype==EPI) ? (0.0001*1.2) : ((p->celltype==MCELL) ? (0.0001*2.5) : 0.0001));
  double Pnak = ((p->celltype==EPI) ? (30.*0.9) : ((p->celltype==MCELL) ? (30.*0.7) : 30.));
  double cmdnmax = ((p->celltype==EPI) ? (0.05*1.3) : 0.05);
  double h10_i = ((kasymm+1.0)+((p->Nao/kna1)*(1.0+(p->Nao/kna2))));
  double h10_ss = ((kasymm+1.0)+((p->Nao/kna1)*(1.+(p->Nao/kna2))));
  double hL_rush_larsen_B = (exp(((-dt)/tau_hL)));
  double hL_rush_larsen_C = (expm1(((-dt)/tau_hL)));
  double hLp_rush_larsen_B = (exp(((-dt)/tau_hLp)));
  double hLp_rush_larsen_C = (expm1(((-dt)/tau_hLp)));
  double jca_rush_larsen_B = (exp(((-dt)/tau_jca)));
  double jca_rush_larsen_C = (expm1(((-dt)/tau_jca)));
  double sqrt_Ko = (sqrt(p->Ko));
  double sqrt_Ko_54 = (sqrt((p->Ko/5.4)));
  double PCaK = (3.574e-4*PCa);
  double PCaNa = (0.00125*PCa);
  double PCap = (1.1*PCa);
  double h11_i = ((p->Nao*p->Nao)/((h10_i*kna1)*kna2));
  double h11_ss = ((p->Nao*p->Nao)/((h10_ss*kna1)*kna2));
  double h12_i = (1.0/h10_i);
  double h12_ss = (1.0/h10_ss);
  double PCaKp = (3.574e-4*PCap);
  double PCaNap = (0.00125*PCap);
  double k1_i = ((h12_i*p->Cao)*kCaon);
  double k1_ss = ((h12_ss*p->Cao)*kCaon);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    OHara_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t v = v_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->CaMKt = CaMKt_init;
    sv->Cai = Cai_init;
    sv->Jrelnp = Jrelnp_init;
    sv->Jrelp = Jrelp_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->a = a_init;
    sv->ap = ap_init;
    sv->cansr = cansr_init;
    sv->d = d_init;
    sv->fcaf = fcaf_init;
    sv->fcafp = fcafp_init;
    sv->fcas = fcas_init;
    sv->ff = ff_init;
    sv->ffp = ffp_init;
    sv->fs = fs_init;
    sv->hL = hL_init;
    sv->hLp = hLp_init;
    sv->hf = hf_init;
    sv->hs = hs_init;
    sv->hsp = hsp_init;
    sv->iF = iF_init;
    sv->iFp = iFp_init;
    sv->iS = iS_init;
    sv->iSp = iSp_init;
    sv->j = j_init;
    sv->jca = jca_init;
    sv->jp = jp_init;
    sv->mL = mL_init;
    sv->nca = nca_init;
    v = v_init;
    sv->xk1 = xk1_init;
    sv->xrf = xrf_init;
    sv->xrs = xrs_init;
    sv->xs1 = xs1_init;
    sv->xs2 = xs2_init;
    double Afcaf = (0.3+(0.6/(1.0+(exp(((v-(10.0))/10.0))))));
    double AiF = (1.0/(1.0+(exp(((v-(213.6))/151.2)))));
    double Axrf = (1.0/(1.0+(exp(((v+54.81)/38.21)))));
    double EK = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    double EKs = (((R*T)/F)*(log(((p->Ko+(PKNa*p->Nao))/(sv->Ki+(PKNa*sv->Nai))))));
    double ENa = (((R*T)/F)*(log((p->Nao/sv->Nai))));
    double IpCa = ((p->GpCa*sv->Cai)/(0.0005+sv->Cai));
    double KNao = (KNao0*(exp(((((1.0-(delta))*v)*F)/((3.0*R)*T)))));
    double Knai = (Knai0*(exp((((delta*v)*F)/((3.0*R)*T)))));
    double KsCa = (1.0+(0.6/(1.0+(pow((3.8e-5/sv->Cai),1.4)))));
    double P = (eP/(((1.0+(H/Khp))+(sv->Nai/Knap))+(sv->Ki/Kxkur)));
    double allo_i = (1.0/(1.0+((KmCaAct_i/sv->Cai)*(KmCaAct_i/sv->Cai))));
    double cajsr_init = sv->cansr;
    double cass_init = sv->Cai;
    double f = ((Aff*sv->ff)+(Afs*sv->fs));
    double fp = ((Aff*sv->ffp)+(Afs*sv->fs));
    double h = ((Ahf*sv->hf)+(Ahs*sv->hs));
    double h4_i = (1.0+((sv->Nai/kna1)*(1.+(sv->Nai/kna2))));
    double hTT2_inf = (1./((1.+(exp(((v+71.55)/7.43))))*(1.+(exp(((v+71.55)/7.43))))));
    double hca = (exp((((qca*v)*F)/(R*T))));
    double hna = (exp((((qna*v)*F)/(R*T))));
    double hp = ((Ahf*sv->hf)+(Ahs*sv->hsp));
    double kss_init = sv->Ki;
    double mORd_inf = (1.0/(1.0+(exp(((-((v+39.57)-(p->shift_m_inf)))/9.871)))));
    double mTT2_inf = (1./((1.+(exp(((-56.86-(v))/9.03))))*(1.+(exp(((-56.86-(v))/9.03))))));
    double nass_init = sv->Nai;
    double rk1 = (1.0/(1.0+(exp((((v+105.8)-((2.6*p->Ko)))/9.493)))));
    double rkr = ((1.0/(1.0+(exp(((v+55.0)/75.0)))))/(1.0+(exp(((v-(10.0))/30.0)))));
    double vffrt = (((v*F)*F)/(R*T));
    double vfrt = ((v*F)/(R*T));
    double xkb = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
    double Afcas = (1.0-(Afcaf));
    double AiS = (1.0-(AiF));
    double Axrs = (1.0-(Axrf));
    double IK1 = ((((p->GK1*sqrt_Ko)*rk1)*sv->xk1)*(v-(EK)));
    double IKb = ((p->GKb*xkb)*(v-(EK)));
    double IKs = ((((p->GKs*KsCa)*sv->xs1)*sv->xs2)*(v-(EKs)));
    double Nao_KNao_3 = (((p->Nao/KNao)*(p->Nao/KNao))*(p->Nao/KNao));
    double Nao_KNao_p1_3 = (((1.0+(p->Nao/KNao))*(1.0+(p->Nao/KNao)))*(1.0+(p->Nao/KNao)));
    double b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
    sv->cajsr = cajsr_init;
    sv->cass = cass_init;
    double exp_2vfrt = (exp((2.0*vfrt)));
    double exp_vfrt = (exp(vfrt));
    double h1_i = (1.+((sv->Nai/kna3)*(1.+hna)));
    double h5_i = ((sv->Nai*sv->Nai)/((h4_i*kna1)*kna2));
    double h6_i = (1.0/h4_i);
    double h7_i = (1.0+((p->Nao/kna3)*(1.0+(1.0/hna))));
    double h7_ss = (1.0+((p->Nao/kna3)*(1.0+(1.0/hna))));
    double hTT2_init = hTT2_inf;
    double jTT2_inf = hTT2_inf;
    sv->kss = kss_init;
    double mORd_init = mORd_inf;
    double mTT2_init = mTT2_inf;
    double nai_Knai_3 = (((sv->Nai/Knai)*(sv->Nai/Knai))*(sv->Nai/Knai));
    double nai_Knai_p1_3 = (((1.0+(sv->Nai/Knai))*(1.0+(sv->Nai/Knai)))*(1.0+(sv->Nai/Knai)));
    sv->nass = nass_init;
    double vffrt_expm1_2vfrt = ((v==0.) ? (F/2.) : (vffrt/(expm1((2.0*vfrt)))));
    double vffrt_expm1_vfrt = ((v==0.) ? F : (vffrt/(expm1(vfrt))));
    double CaMKb = ((CaMKo*(1.0-(sv->CaMKt)))/(1.0+(KmCaM/sv->cass)));
    double IbCa = ((((p->factorIbCa*PCab)*4.0)*vffrt_expm1_2vfrt)*((sv->Cai*exp_2vfrt)-((0.341*p->Cao))));
    double IbNa = (((p->factorIbNa*PNab)*vffrt_expm1_vfrt)*((sv->Nai*exp_vfrt)-(p->Nao)));
    double PhiCaK = (vffrt_expm1_vfrt*(((0.75*sv->kss)*exp_vfrt)-((0.75*p->Ko))));
    double PhiCaL = ((4.0*vffrt_expm1_2vfrt)*((sv->cass*exp_2vfrt)-((0.341*p->Cao))));
    double PhiCaNa = (vffrt_expm1_vfrt*(((0.75*sv->nass)*exp_vfrt)-((0.75*p->Nao))));
    double a1 = ((k1p*nai_Knai_3)/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
    double a3 = (((k3p*(p->Ko/KKo))*(p->Ko/KKo))/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
    double allo_ss = (1.0/(1.0+((KmCaAct_ss/sv->cass)*(KmCaAct_ss/sv->cass))));
    double b2 = ((k2m*Nao_KNao_3)/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
    double b4 = (((k4m*(sv->Ki/Kki))*(sv->Ki/Kki))/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
    double fca = ((Afcaf*sv->fcaf)+(Afcas*sv->fcas));
    double fcap = ((Afcaf*sv->fcafp)+(Afcas*sv->fcas));
    double h1_ss = (1.+((sv->nass/kna3)*(1.+hna)));
    double h2_i = ((sv->Nai*hna)/(kna3*h1_i));
    double h3_i = (1.0/h1_i);
    double h4_ss = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
    double h8_i = (p->Nao/((kna3*hna)*h7_i));
    double h8_ss = (p->Nao/((kna3*hna)*h7_ss));
    double h9_i = (1.0/h7_i);
    double h9_ss = (1.0/h7_ss);
    sv->hTT2 = hTT2_init;
    double i = ((AiF*sv->iF)+(AiS*sv->iS));
    double ip = ((AiF*sv->iFp)+(AiS*sv->iSp));
    double jTT2_init = jTT2_inf;
    double k6_i = ((h6_i*sv->Cai)*kCaon);
    sv->mORd = mORd_init;
    sv->mTT2 = mTT2_init;
    double xr = ((Axrf*sv->xrf)+(Axrs*sv->xrs));
    double CaMKa = (CaMKb+sv->CaMKt);
    double IKr = ((((p->GKr*sqrt_Ko_54)*xr)*rkr)*(v-(EK)));
    double h2_ss = ((sv->nass*hna)/(kna3*h1_ss));
    double h3_ss = (1.0/h1_ss);
    double h5_ss = ((sv->nass*sv->nass)/((h4_ss*kna1)*kna2));
    double h6_ss = (1.0/h4_ss);
    sv->jTT2 = jTT2_init;
    double k3p_i = (h9_i*wca);
    double k3p_ss = (h9_ss*wca);
    double k3pp_i = (h8_i*wnaca);
    double k3pp_ss = (h8_ss*wnaca);
    double k4p_i = ((h3_i*wca)/hca);
    double k4pp_i = (h2_i*wnaca);
    double k7_i = ((h5_i*h2_i)*wna);
    double k8_i = ((h8_i*h11_i)*wna);
    double k8_ss = ((h8_ss*h11_ss)*wna);
    double x1 = ((p->modelformulation==CLERX) ? (((((a4*a1)*a2)+((b1*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)) : (((((a4*a1)*a2)+((b2*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)));
    double x2 = (((((b2*b1)*b4)+((a1*a2)*a3))+((a3*b1)*b4))+((a2*a3)*b4));
    double x3 = (((((a2*a3)*a4)+((b3*b2)*b1))+((b2*b1)*a4))+((a3*a4)*b1));
    double x4 = (((((b4*b3)*b2)+((a3*a4)*a1))+((b2*a4)*a1))+((b3*b2)*a1));
    double E1 = (x1/(((x1+x2)+x3)+x4));
    double E2 = (x2/(((x1+x2)+x3)+x4));
    double E3 = (x3/(((x1+x2)+x3)+x4));
    double E4 = (x4/(((x1+x2)+x3)+x4));
    double fICaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
    double fINaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
    double fINap = (1.0/(1.0+(KmCaMK/CaMKa)));
    double fItop = (1.0/(1.0+(KmCaMK/CaMKa)));
    double k3_i = (k3p_i+k3pp_i);
    double k3_ss = (k3p_ss+k3pp_ss);
    double k4_i = (k4p_i+k4pp_i);
    double k4p_ss = ((h3_ss*wca)/hca);
    double k4pp_ss = (h2_ss*wnaca);
    double k6_ss = ((h6_ss*sv->cass)*kCaon);
    double k7_ss = ((h5_ss*h2_ss)*wna);
    double r = (((((a1*a2)*a3)*a4)-((((b1*b2)*b3)*b4)))/(((x1+x2)+x3)+x4));
    double ICaK = ((((((p->factorICaK*(1.0-(fICaLp)))*PCaK)*PhiCaK)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCaKp)*PhiCaK)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    double ICaL = ((((((p->factorICaL*(1.0-(fICaLp)))*PCa)*PhiCaL)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCap)*PhiCaL)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    double ICaNa = ((((((p->factorICaNa*(1.0-(fICaLp)))*PCaNa)*PhiCaNa)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCaNap)*PhiCaNa)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    double INa = ((p->INa_Type==TT2INa) ? ((((((p->GNa*sv->mTT2)*sv->mTT2)*sv->mTT2)*sv->hTT2)*sv->jTT2)*(v-(ENa))) : (((((p->GNa*(v-(ENa)))*sv->mORd)*sv->mORd)*sv->mORd)*((((1.0-(fINap))*h)*sv->j)+((fINap*hp)*sv->jp))));
    double INaL = (((p->GNaL*(v-(ENa)))*sv->mL)*(((1.0-(fINaLp))*sv->hL)+(fINaLp*sv->hLp)));
    double Ito = ((p->Gto*(v-(EK)))*((((1.0-(fItop))*sv->a)*i)+((fItop*sv->ap)*ip)));
    double JNaKK = ((p->modelformulation==CLERX) ? (-2.*r) : (2.*((E4*b1)-((E3*a1)))));
    double JNaKNa = ((p->modelformulation==CLERX) ? (3.*r) : (3.*((E1*a3)-((E2*b3)))));
    double k4_ss = (k4p_ss+k4pp_ss);
    double x1_i = (((k2_i*k4_i)*(k7_i+k6_i))+((k5_i*k7_i)*(k2_i+k3_i)));
    double x2_i = (((k1_i*k7_i)*(k4_i+k5_i))+((k4_i*k6_i)*(k1_i+k8_i)));
    double x3_i = (((k1_i*k3_i)*(k7_i+k6_i))+((k8_i*k6_i)*(k2_i+k3_i)));
    double x3_ss = (((k1_ss*k3_ss)*(k7_ss+k6_ss))+((k8_ss*k6_ss)*(k2_ss+k3_ss)));
    double x4_i = (((k2_i*k8_i)*(k4_i+k5_i))+((k3_i*k5_i)*(k1_i+k8_i)));
    double E1_i = (x1_i/(((x1_i+x2_i)+x3_i)+x4_i));
    double E2_i = (x2_i/(((x1_i+x2_i)+x3_i)+x4_i));
    double E3_i = (x3_i/(((x1_i+x2_i)+x3_i)+x4_i));
    double E4_i = (x4_i/(((x1_i+x2_i)+x3_i)+x4_i));
    double INaK = ((p->factorINaK*Pnak)*((zna*JNaKNa)+(zk*JNaKK)));
    double x1_ss = (((k2_ss*k4_ss)*(k7_ss+k6_ss))+((k5_ss*k7_ss)*(k2_ss+k3_ss)));
    double x2_ss = (((k1_ss*k7_ss)*(k4_ss+k5_ss))+((k4_ss*k6_ss)*(k1_ss+k8_ss)));
    double x4_ss = (((k2_ss*k8_ss)*(k4_ss+k5_ss))+((k3_ss*k5_ss)*(k1_ss+k8_ss)));
    double E1_ss = (x1_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    double E2_ss = (x2_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    double E3_ss = (x3_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    double E4_ss = (x4_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    double JncxCai = ((E2_i*k2_i)-((E1_i*k1_i)));
    double JncxNai = (((3.0*((E4_i*k7_i)-((E1_i*k8_i))))+(E3_i*k4pp_i))-((E2_i*k3pp_i)));
    double INaCa = ((((p->factorINaCa*0.8)*p->GNaCa)*allo_i)*((zna*JncxNai)+(zca*JncxCai)));
    double JncxCa_ss = ((E2_ss*k2_ss)-((E1_ss*k1_ss)));
    double JncxNa_ss = (((3.0*((E4_ss*k7_ss)-((E1_ss*k8_ss))))+(E3_ss*k4pp_ss))-((E2_ss*k3pp_ss)));
    double INaCa_ss = ((((p->factorINaCass*0.2)*p->GNaCa)*allo_ss)*((zna*JncxNa_ss)+(zca*JncxCa_ss)));
    Iion = (((((((((((((((INa+INaL)+Ito)+ICaL)+ICaNa)+ICaK)+IKr)+IKs)+IK1)+INaCa)+INaCa_ss)+INaK)+IbNa)+IKb)+IpCa)+IbCa);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    v_ext[__i] = v;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef OHARA_CPU_GENERATED
extern "C" {
void compute_OHara_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  OHaraIonType::IonIfDerived& imp = static_cast<OHaraIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  OHara_Params *p  = imp.params();
  OHara_state *sv_base = (OHara_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  OHaraIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t H = ((p->modelformulation==CLERX) ? 1.0e-4 : 1.0e-7);
  GlobalData_t PCa = ((p->celltype==EPI) ? (0.0001*1.2) : ((p->celltype==MCELL) ? (0.0001*2.5) : 0.0001));
  GlobalData_t Pnak = ((p->celltype==EPI) ? (30.*0.9) : ((p->celltype==MCELL) ? (30.*0.7) : 30.));
  GlobalData_t cmdnmax = ((p->celltype==EPI) ? (0.05*1.3) : 0.05);
  GlobalData_t h10_i = ((kasymm+1.0)+((p->Nao/kna1)*(1.0+(p->Nao/kna2))));
  GlobalData_t h10_ss = ((kasymm+1.0)+((p->Nao/kna1)*(1.+(p->Nao/kna2))));
  GlobalData_t hL_rush_larsen_B = (exp(((-dt)/tau_hL)));
  GlobalData_t hL_rush_larsen_C = (expm1(((-dt)/tau_hL)));
  GlobalData_t hLp_rush_larsen_B = (exp(((-dt)/tau_hLp)));
  GlobalData_t hLp_rush_larsen_C = (expm1(((-dt)/tau_hLp)));
  GlobalData_t jca_rush_larsen_B = (exp(((-dt)/tau_jca)));
  GlobalData_t jca_rush_larsen_C = (expm1(((-dt)/tau_jca)));
  GlobalData_t sqrt_Ko = (sqrt(p->Ko));
  GlobalData_t sqrt_Ko_54 = (sqrt((p->Ko/5.4)));
  GlobalData_t PCaK = (3.574e-4*PCa);
  GlobalData_t PCaNa = (0.00125*PCa);
  GlobalData_t PCap = (1.1*PCa);
  GlobalData_t h11_i = ((p->Nao*p->Nao)/((h10_i*kna1)*kna2));
  GlobalData_t h11_ss = ((p->Nao*p->Nao)/((h10_ss*kna1)*kna2));
  GlobalData_t h12_i = (1.0/h10_i);
  GlobalData_t h12_ss = (1.0/h10_ss);
  GlobalData_t PCaKp = (3.574e-4*PCap);
  GlobalData_t PCaNap = (0.00125*PCap);
  GlobalData_t k1_i = ((h12_i*p->Cao)*kCaon);
  GlobalData_t k1_ss = ((h12_ss*p->Cao)*kCaon);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    OHara_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t v = v_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables()[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t v_row[NROWS_v];
    LUT_interpRow(&IF->tables()[v_TAB], v, __i, v_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t CaMKb = ((CaMKo*(1.0-(sv->CaMKt)))/(1.0+(KmCaM/sv->cass)));
    GlobalData_t EK = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    GlobalData_t EKs = (((R*T)/F)*(log(((p->Ko+(PKNa*p->Nao))/(sv->Ki+(PKNa*sv->Nai))))));
    GlobalData_t ENa = (((R*T)/F)*(log((p->Nao/sv->Nai))));
    GlobalData_t P = (eP/(((1.0+(H/Khp))+(sv->Nai/Knap))+(sv->Ki/Kxkur)));
    GlobalData_t allo_ss = (1.0/(1.0+((KmCaAct_ss/sv->cass)*(KmCaAct_ss/sv->cass))));
    GlobalData_t f = ((Aff*sv->ff)+(Afs*sv->fs));
    GlobalData_t fp = ((Aff*sv->ffp)+(Afs*sv->fs));
    GlobalData_t h = ((Ahf*sv->hf)+(Ahs*sv->hs));
    GlobalData_t h4_i = (1.0+((sv->Nai/kna1)*(1.+(sv->Nai/kna2))));
    GlobalData_t h4_ss = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
    GlobalData_t hp = ((Ahf*sv->hf)+(Ahs*sv->hsp));
    GlobalData_t CaMKa = (CaMKb+sv->CaMKt);
    GlobalData_t IK1 = ((((p->GK1*sqrt_Ko)*v_row[rk1_idx])*sv->xk1)*(v-(EK)));
    GlobalData_t IKb = ((p->GKb*v_row[xkb_idx])*(v-(EK)));
    GlobalData_t IKs = ((((p->GKs*Cai_row[KsCa_idx])*sv->xs1)*sv->xs2)*(v-(EKs)));
    GlobalData_t b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
    GlobalData_t h1_i = (1.+((sv->Nai/kna3)*(1.+v_row[hna_idx])));
    GlobalData_t h1_ss = (1.+((sv->nass/kna3)*(1.+v_row[hna_idx])));
    GlobalData_t h5_i = ((sv->Nai*sv->Nai)/((h4_i*kna1)*kna2));
    GlobalData_t h5_ss = ((sv->nass*sv->nass)/((h4_ss*kna1)*kna2));
    GlobalData_t h6_i = (1.0/h4_i);
    GlobalData_t h6_ss = (1.0/h4_ss);
    GlobalData_t nai_Knai_3 = (((sv->Nai/v_row[Knai_idx])*(sv->Nai/v_row[Knai_idx]))*(sv->Nai/v_row[Knai_idx]));
    GlobalData_t nai_Knai_p1_3 = (((1.0+(sv->Nai/v_row[Knai_idx]))*(1.0+(sv->Nai/v_row[Knai_idx])))*(1.0+(sv->Nai/v_row[Knai_idx])));
    GlobalData_t IbCa = ((((p->factorIbCa*PCab)*4.0)*v_row[vffrt_expm1_2vfrt_idx])*((sv->Cai*v_row[exp_2vfrt_idx])-((0.341*p->Cao))));
    GlobalData_t IbNa = (((p->factorIbNa*PNab)*v_row[vffrt_expm1_vfrt_idx])*((sv->Nai*v_row[exp_vfrt_idx])-(p->Nao)));
    GlobalData_t PhiCaK = (v_row[vffrt_expm1_vfrt_idx]*(((0.75*sv->kss)*v_row[exp_vfrt_idx])-((0.75*p->Ko))));
    GlobalData_t PhiCaL = ((4.0*v_row[vffrt_expm1_2vfrt_idx])*((sv->cass*v_row[exp_2vfrt_idx])-((0.341*p->Cao))));
    GlobalData_t PhiCaNa = (v_row[vffrt_expm1_vfrt_idx]*(((0.75*sv->nass)*v_row[exp_vfrt_idx])-((0.75*p->Nao))));
    GlobalData_t a1 = ((k1p*nai_Knai_3)/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
    GlobalData_t b4 = (((k4m*(sv->Ki/Kki))*(sv->Ki/Kki))/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
    GlobalData_t fICaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t fINaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t fINap = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t fItop = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t fca = ((v_row[Afcaf_idx]*sv->fcaf)+(v_row[Afcas_idx]*sv->fcas));
    GlobalData_t fcap = ((v_row[Afcaf_idx]*sv->fcafp)+(v_row[Afcas_idx]*sv->fcas));
    GlobalData_t h2_i = ((sv->Nai*v_row[hna_idx])/(kna3*h1_i));
    GlobalData_t h2_ss = ((sv->nass*v_row[hna_idx])/(kna3*h1_ss));
    GlobalData_t h3_i = (1.0/h1_i);
    GlobalData_t h3_ss = (1.0/h1_ss);
    GlobalData_t i = ((v_row[AiF_idx]*sv->iF)+(v_row[AiS_idx]*sv->iS));
    GlobalData_t ip = ((v_row[AiF_idx]*sv->iFp)+(v_row[AiS_idx]*sv->iSp));
    GlobalData_t k6_i = ((h6_i*sv->Cai)*kCaon);
    GlobalData_t k6_ss = ((h6_ss*sv->cass)*kCaon);
    GlobalData_t xr = ((v_row[Axrf_idx]*sv->xrf)+(v_row[Axrs_idx]*sv->xrs));
    GlobalData_t ICaK = ((((((p->factorICaK*(1.0-(fICaLp)))*PCaK)*PhiCaK)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCaKp)*PhiCaK)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    GlobalData_t ICaL = ((((((p->factorICaL*(1.0-(fICaLp)))*PCa)*PhiCaL)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCap)*PhiCaL)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    GlobalData_t ICaNa = ((((((p->factorICaNa*(1.0-(fICaLp)))*PCaNa)*PhiCaNa)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCaNap)*PhiCaNa)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
    GlobalData_t IKr = ((((p->GKr*sqrt_Ko_54)*xr)*v_row[rkr_idx])*(v-(EK)));
    GlobalData_t INa = ((p->INa_Type==TT2INa) ? ((((((p->GNa*sv->mTT2)*sv->mTT2)*sv->mTT2)*sv->hTT2)*sv->jTT2)*(v-(ENa))) : (((((p->GNa*(v-(ENa)))*sv->mORd)*sv->mORd)*sv->mORd)*((((1.0-(fINap))*h)*sv->j)+((fINap*hp)*sv->jp))));
    GlobalData_t INaL = (((p->GNaL*(v-(ENa)))*sv->mL)*(((1.0-(fINaLp))*sv->hL)+(fINaLp*sv->hLp)));
    GlobalData_t Ito = ((p->Gto*(v-(EK)))*((((1.0-(fItop))*sv->a)*i)+((fItop*sv->ap)*ip)));
    GlobalData_t k4p_i = ((h3_i*wca)/v_row[hca_idx]);
    GlobalData_t k4p_ss = ((h3_ss*wca)/v_row[hca_idx]);
    GlobalData_t k4pp_i = (h2_i*wnaca);
    GlobalData_t k4pp_ss = (h2_ss*wnaca);
    GlobalData_t k7_i = ((h5_i*h2_i)*wna);
    GlobalData_t k7_ss = ((h5_ss*h2_ss)*wna);
    GlobalData_t x1 = ((p->modelformulation==CLERX) ? (((((a4*a1)*a2)+((b1*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)) : (((((a4*a1)*a2)+((v_row[b2_idx]*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)));
    GlobalData_t x2 = (((((v_row[b2_idx]*b1)*b4)+((a1*a2)*v_row[a3_idx]))+((v_row[a3_idx]*b1)*b4))+((a2*v_row[a3_idx])*b4));
    GlobalData_t x3 = (((((a2*v_row[a3_idx])*a4)+((b3*v_row[b2_idx])*b1))+((v_row[b2_idx]*b1)*a4))+((v_row[a3_idx]*a4)*b1));
    GlobalData_t x4 = (((((b4*b3)*v_row[b2_idx])+((v_row[a3_idx]*a4)*a1))+((v_row[b2_idx]*a4)*a1))+((b3*v_row[b2_idx])*a1));
    GlobalData_t E1 = (x1/(((x1+x2)+x3)+x4));
    GlobalData_t E2 = (x2/(((x1+x2)+x3)+x4));
    GlobalData_t E3 = (x3/(((x1+x2)+x3)+x4));
    GlobalData_t E4 = (x4/(((x1+x2)+x3)+x4));
    GlobalData_t k4_i = (k4p_i+k4pp_i);
    GlobalData_t k4_ss = (k4p_ss+k4pp_ss);
    GlobalData_t r = (((((a1*a2)*v_row[a3_idx])*a4)-((((b1*v_row[b2_idx])*b3)*b4)))/(((x1+x2)+x3)+x4));
    GlobalData_t JNaKK = ((p->modelformulation==CLERX) ? (-2.*r) : (2.*((E4*b1)-((E3*a1)))));
    GlobalData_t JNaKNa = ((p->modelformulation==CLERX) ? (3.*r) : (3.*((E1*v_row[a3_idx])-((E2*b3)))));
    GlobalData_t x1_i = (((k2_i*k4_i)*(k7_i+k6_i))+((k5_i*k7_i)*(k2_i+v_row[k3_i_idx])));
    GlobalData_t x1_ss = (((k2_ss*k4_ss)*(k7_ss+k6_ss))+((k5_ss*k7_ss)*(k2_ss+v_row[k3_ss_idx])));
    GlobalData_t x2_i = (((k1_i*k7_i)*(k4_i+k5_i))+((k4_i*k6_i)*(k1_i+v_row[k8_i_idx])));
    GlobalData_t x2_ss = (((k1_ss*k7_ss)*(k4_ss+k5_ss))+((k4_ss*k6_ss)*(k1_ss+v_row[k8_ss_idx])));
    GlobalData_t x3_i = (((k1_i*v_row[k3_i_idx])*(k7_i+k6_i))+((v_row[k8_i_idx]*k6_i)*(k2_i+v_row[k3_i_idx])));
    GlobalData_t x3_ss = (((k1_ss*v_row[k3_ss_idx])*(k7_ss+k6_ss))+((v_row[k8_ss_idx]*k6_ss)*(k2_ss+v_row[k3_ss_idx])));
    GlobalData_t x4_i = (((k2_i*v_row[k8_i_idx])*(k4_i+k5_i))+((v_row[k3_i_idx]*k5_i)*(k1_i+v_row[k8_i_idx])));
    GlobalData_t x4_ss = (((k2_ss*v_row[k8_ss_idx])*(k4_ss+k5_ss))+((v_row[k3_ss_idx]*k5_ss)*(k1_ss+v_row[k8_ss_idx])));
    GlobalData_t E1_i = (x1_i/(((x1_i+x2_i)+x3_i)+x4_i));
    GlobalData_t E1_ss = (x1_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    GlobalData_t E2_i = (x2_i/(((x1_i+x2_i)+x3_i)+x4_i));
    GlobalData_t E2_ss = (x2_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    GlobalData_t E3_i = (x3_i/(((x1_i+x2_i)+x3_i)+x4_i));
    GlobalData_t E3_ss = (x3_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    GlobalData_t E4_i = (x4_i/(((x1_i+x2_i)+x3_i)+x4_i));
    GlobalData_t E4_ss = (x4_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
    GlobalData_t INaK = ((p->factorINaK*Pnak)*((zna*JNaKNa)+(zk*JNaKK)));
    GlobalData_t JncxCa_ss = ((E2_ss*k2_ss)-((E1_ss*k1_ss)));
    GlobalData_t JncxCai = ((E2_i*k2_i)-((E1_i*k1_i)));
    GlobalData_t JncxNa_ss = (((3.0*((E4_ss*k7_ss)-((E1_ss*v_row[k8_ss_idx]))))+(E3_ss*k4pp_ss))-((E2_ss*v_row[k3pp_ss_idx])));
    GlobalData_t JncxNai = (((3.0*((E4_i*k7_i)-((E1_i*v_row[k8_i_idx]))))+(E3_i*k4pp_i))-((E2_i*v_row[k3pp_i_idx])));
    GlobalData_t INaCa = ((((p->factorINaCa*0.8)*p->GNaCa)*Cai_row[allo_i_idx])*((zna*JncxNai)+(zca*JncxCai)));
    GlobalData_t INaCa_ss = ((((p->factorINaCass*0.2)*p->GNaCa)*allo_ss)*((zna*JncxNa_ss)+(zca*JncxCa_ss)));
    Iion = (((((((((((((((INa+INaL)+Ito)+ICaL)+ICaNa)+ICaK)+IKr)+IKs)+IK1)+INaCa)+INaCa_ss)+INaK)+IbNa)+IKb)+Cai_row[IpCa_idx])+IbCa);
    
    
    //Complete Forward Euler Update
    GlobalData_t Bcajsr = (1.0/(1.0+(((csqnmax*kmcsqn)/(kmcsqn+sv->cajsr))/(kmcsqn+sv->cajsr))));
    GlobalData_t Bcass = (1.0/((1.0+(((BSRmax*KmBSR)/(KmBSR+sv->cass))/(KmBSR+sv->cass)))+(((BSLmax*KmBSL)/(KmBSL+sv->cass))/(KmBSL+sv->cass))));
    GlobalData_t Jdiff = ((sv->cass-(sv->Cai))/0.2);
    GlobalData_t JdiffK = ((sv->kss-(sv->Ki))/2.0);
    GlobalData_t JdiffNa = ((sv->nass-(sv->Nai))/2.0);
    GlobalData_t Jleak = ((0.0039375*sv->cansr)/15.0);
    GlobalData_t Jtr = ((sv->cansr-(sv->cajsr))/100.0);
    GlobalData_t diff_CaMKt = (((aCaMK*CaMKb)*(CaMKb+sv->CaMKt))-((bCaMK*sv->CaMKt)));
    GlobalData_t fJrelp = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t fJupp = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t km2n = sv->jca;
    GlobalData_t kmn_cass_4 = ((((1.0+(Kmn/sv->cass))*(1.0+(Kmn/sv->cass)))*(1.0+(Kmn/sv->cass)))*(1.0+(Kmn/sv->cass)));
    GlobalData_t Jrel_canidate = (((1.0-(fJrelp))*sv->Jrelnp)+(fJrelp*sv->Jrelp));
    GlobalData_t Jup = ((((1.0-(fJupp))*Cai_row[Jupnp_idx])+(fJupp*Cai_row[Jupp_idx]))-(Jleak));
    GlobalData_t anca = (1.0/((k2n/km2n)+kmn_cass_4));
    GlobalData_t diff_Ki = ((((-(((((Ito+IKr)+IKs)+IK1)+IKb)-((2.0*INaK))))*Acap)/(F*vmyo))+((JdiffK*vss)/vmyo));
    GlobalData_t diff_Nai = ((((-((((INa+INaL)+(3.0*INaCa))+(3.0*INaK))+IbNa))*Acap)/(F*vmyo))+((JdiffNa*vss)/vmyo));
    GlobalData_t diff_kss = ((((-ICaK)*Acap)/(F*vss))-(JdiffK));
    GlobalData_t diff_nass = ((((-(ICaNa+(3.0*INaCa_ss)))*Acap)/(F*vss))-(JdiffNa));
    GlobalData_t Jrel = (((Jrel_canidate*p->jrel_stiff_const)>sv->cajsr) ? (sv->cajsr/p->jrel_stiff_const) : Jrel_canidate);
    GlobalData_t diff_Cai = (Cai_row[Bcai_idx]*(((((-((Cai_row[IpCa_idx]+IbCa)-((2.0*INaCa))))*Acap)/((2.0*F)*vmyo))-(((Jup*vnsr)/vmyo)))+((Jdiff*vss)/vmyo)));
    GlobalData_t diff_cansr = (Jup-(((Jtr*vjsr)/vnsr)));
    GlobalData_t diff_nca = ((anca*k2n)-((sv->nca*km2n)));
    GlobalData_t diff_cajsr = (Bcajsr*(Jtr-(Jrel)));
    GlobalData_t diff_cass = (Bcass*(((((-(ICaL-((2.0*INaCa_ss))))*Acap)/((2.0*F)*vss))+((Jrel*vjsr)/vss))-(Jdiff)));
    GlobalData_t CaMKt_new = sv->CaMKt+diff_CaMKt*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t cajsr_new = sv->cajsr+diff_cajsr*dt;
    GlobalData_t cansr_new = sv->cansr+diff_cansr*dt;
    GlobalData_t cass_new = sv->cass+diff_cass*dt;
    GlobalData_t kss_new = sv->kss+diff_kss*dt;
    GlobalData_t nass_new = sv->nass+diff_nass*dt;
    GlobalData_t nca_new = sv->nca+diff_nca*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t jrel_cajsr_term = ((((((((1.5/sv->cajsr)*(1.5/sv->cajsr))*(1.5/sv->cajsr))*(1.5/sv->cajsr))*(1.5/sv->cajsr))*(1.5/sv->cajsr))*(1.5/sv->cajsr))*(1.5/sv->cajsr));
    GlobalData_t tau_rel_canidate = (bt/(1.0+(0.0123/sv->cajsr)));
    GlobalData_t tau_relp_canidate = (btp/(1.0+(0.0123/sv->cajsr)));
    GlobalData_t a_rush_larsen_B = v_row[a_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_B = v_row[d_rush_larsen_B_idx];
    GlobalData_t fcaf_rush_larsen_B = v_row[fcaf_rush_larsen_B_idx];
    GlobalData_t fcas_rush_larsen_B = v_row[fcas_rush_larsen_B_idx];
    GlobalData_t ff_rush_larsen_B = v_row[ff_rush_larsen_B_idx];
    GlobalData_t fs_rush_larsen_B = v_row[fs_rush_larsen_B_idx];
    GlobalData_t hL_rush_larsen_A = v_row[hL_rush_larsen_A_idx];
    GlobalData_t hLp_rush_larsen_A = v_row[hLp_rush_larsen_A_idx];
    GlobalData_t hf_rush_larsen_B = v_row[hf_rush_larsen_B_idx];
    GlobalData_t hs_rush_larsen_B = v_row[hs_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_B = v_row[j_rush_larsen_B_idx];
    GlobalData_t jrel_ical_term = ((-ICaL)/(1.0+jrel_cajsr_term));
    GlobalData_t mORd_rush_larsen_B = v_row[mORd_rush_larsen_B_idx];
    GlobalData_t tau_rel = ((tau_rel_canidate<0.001) ? 0.001 : tau_rel_canidate);
    GlobalData_t tau_relp = ((tau_relp_canidate<0.001) ? 0.001 : tau_relp_canidate);
    GlobalData_t xk1_rush_larsen_B = v_row[xk1_rush_larsen_B_idx];
    GlobalData_t xrf_rush_larsen_B = v_row[xrf_rush_larsen_B_idx];
    GlobalData_t xrs_rush_larsen_B = v_row[xrs_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_B = v_row[xs1_rush_larsen_B_idx];
    GlobalData_t xs2_rush_larsen_B = v_row[xs2_rush_larsen_B_idx];
    GlobalData_t Jrel_inf = ((p->celltype==MCELL) ? ((a_rel*jrel_ical_term)*1.7) : (a_rel*jrel_ical_term));
    GlobalData_t Jrel_infp = ((p->celltype==MCELL) ? ((a_relp*jrel_ical_term)*1.7) : (a_relp*jrel_ical_term));
    GlobalData_t a_rush_larsen_A = v_row[a_rush_larsen_A_idx];
    GlobalData_t ap_rush_larsen_B = v_row[ap_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = v_row[d_rush_larsen_A_idx];
    GlobalData_t fcafp_rush_larsen_B = v_row[fcafp_rush_larsen_B_idx];
    GlobalData_t ff_rush_larsen_A = v_row[ff_rush_larsen_A_idx];
    GlobalData_t ffp_rush_larsen_B = v_row[ffp_rush_larsen_B_idx];
    GlobalData_t fs_rush_larsen_A = v_row[fs_rush_larsen_A_idx];
    GlobalData_t hTT2_rush_larsen_B = v_row[hTT2_rush_larsen_B_idx];
    GlobalData_t hf_rush_larsen_A = v_row[hf_rush_larsen_A_idx];
    GlobalData_t hs_rush_larsen_A = v_row[hs_rush_larsen_A_idx];
    GlobalData_t hsp_rush_larsen_B = v_row[hsp_rush_larsen_B_idx];
    GlobalData_t iF_rush_larsen_B = v_row[iF_rush_larsen_B_idx];
    GlobalData_t iS_rush_larsen_B = v_row[iS_rush_larsen_B_idx];
    GlobalData_t jTT2_rush_larsen_B = v_row[jTT2_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = v_row[j_rush_larsen_A_idx];
    GlobalData_t jp_rush_larsen_B = v_row[jp_rush_larsen_B_idx];
    GlobalData_t mORd_rush_larsen_A = v_row[mORd_rush_larsen_A_idx];
    GlobalData_t mTT2_rush_larsen_B = v_row[mTT2_rush_larsen_B_idx];
    GlobalData_t tau_Jrelnp = tau_rel;
    GlobalData_t tau_Jrelp = tau_relp;
    GlobalData_t xk1_rush_larsen_A = v_row[xk1_rush_larsen_A_idx];
    GlobalData_t xrf_rush_larsen_A = v_row[xrf_rush_larsen_A_idx];
    GlobalData_t xrs_rush_larsen_A = v_row[xrs_rush_larsen_A_idx];
    GlobalData_t xs1_rush_larsen_A = v_row[xs1_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_A = v_row[xs2_rush_larsen_A_idx];
    GlobalData_t Jrelnp_inf = Jrel_inf;
    GlobalData_t Jrelnp_rush_larsen_B = (exp(((-dt)/tau_Jrelnp)));
    GlobalData_t Jrelnp_rush_larsen_C = (expm1(((-dt)/tau_Jrelnp)));
    GlobalData_t Jrelp_inf = Jrel_infp;
    GlobalData_t Jrelp_rush_larsen_B = (exp(((-dt)/tau_Jrelp)));
    GlobalData_t Jrelp_rush_larsen_C = (expm1(((-dt)/tau_Jrelp)));
    GlobalData_t ap_rush_larsen_A = v_row[ap_rush_larsen_A_idx];
    GlobalData_t fcaf_rush_larsen_A = v_row[fcaf_rush_larsen_A_idx];
    GlobalData_t fcafp_rush_larsen_A = v_row[fcafp_rush_larsen_A_idx];
    GlobalData_t fcas_rush_larsen_A = v_row[fcas_rush_larsen_A_idx];
    GlobalData_t ffp_rush_larsen_A = v_row[ffp_rush_larsen_A_idx];
    GlobalData_t hTT2_rush_larsen_A = v_row[hTT2_rush_larsen_A_idx];
    GlobalData_t hsp_rush_larsen_A = v_row[hsp_rush_larsen_A_idx];
    GlobalData_t iF_rush_larsen_A = v_row[iF_rush_larsen_A_idx];
    GlobalData_t iFp_rush_larsen_B = v_row[iFp_rush_larsen_B_idx];
    GlobalData_t iS_rush_larsen_A = v_row[iS_rush_larsen_A_idx];
    GlobalData_t iSp_rush_larsen_B = v_row[iSp_rush_larsen_B_idx];
    GlobalData_t jTT2_rush_larsen_A = v_row[jTT2_rush_larsen_A_idx];
    GlobalData_t jca_rush_larsen_A = v_row[jca_rush_larsen_A_idx];
    GlobalData_t jp_rush_larsen_A = v_row[jp_rush_larsen_A_idx];
    GlobalData_t mTT2_rush_larsen_A = v_row[mTT2_rush_larsen_A_idx];
    GlobalData_t Jrelnp_rush_larsen_A = ((-Jrelnp_inf)*Jrelnp_rush_larsen_C);
    GlobalData_t Jrelp_rush_larsen_A = ((-Jrelp_inf)*Jrelp_rush_larsen_C);
    GlobalData_t iFp_rush_larsen_A = v_row[iFp_rush_larsen_A_idx];
    GlobalData_t iSp_rush_larsen_A = v_row[iSp_rush_larsen_A_idx];
    GlobalData_t mL_rush_larsen_B = v_row[mL_rush_larsen_B_idx];
    GlobalData_t mL_rush_larsen_A = v_row[mL_rush_larsen_A_idx];
    GlobalData_t Jrelnp_new = Jrelnp_rush_larsen_A+Jrelnp_rush_larsen_B*sv->Jrelnp;
    GlobalData_t Jrelp_new = Jrelp_rush_larsen_A+Jrelp_rush_larsen_B*sv->Jrelp;
    GlobalData_t a_new = a_rush_larsen_A+a_rush_larsen_B*sv->a;
    GlobalData_t ap_new = ap_rush_larsen_A+ap_rush_larsen_B*sv->ap;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t fcaf_new = fcaf_rush_larsen_A+fcaf_rush_larsen_B*sv->fcaf;
    GlobalData_t fcafp_new = fcafp_rush_larsen_A+fcafp_rush_larsen_B*sv->fcafp;
    GlobalData_t fcas_new = fcas_rush_larsen_A+fcas_rush_larsen_B*sv->fcas;
    GlobalData_t ff_new = ff_rush_larsen_A+ff_rush_larsen_B*sv->ff;
    GlobalData_t ffp_new = ffp_rush_larsen_A+ffp_rush_larsen_B*sv->ffp;
    GlobalData_t fs_new = fs_rush_larsen_A+fs_rush_larsen_B*sv->fs;
    GlobalData_t hL_new = hL_rush_larsen_A+hL_rush_larsen_B*sv->hL;
    GlobalData_t hLp_new = hLp_rush_larsen_A+hLp_rush_larsen_B*sv->hLp;
    GlobalData_t hTT2_new = hTT2_rush_larsen_A+hTT2_rush_larsen_B*sv->hTT2;
    GlobalData_t hf_new = hf_rush_larsen_A+hf_rush_larsen_B*sv->hf;
    GlobalData_t hs_new = hs_rush_larsen_A+hs_rush_larsen_B*sv->hs;
    GlobalData_t hsp_new = hsp_rush_larsen_A+hsp_rush_larsen_B*sv->hsp;
    GlobalData_t iF_new = iF_rush_larsen_A+iF_rush_larsen_B*sv->iF;
    GlobalData_t iFp_new = iFp_rush_larsen_A+iFp_rush_larsen_B*sv->iFp;
    GlobalData_t iS_new = iS_rush_larsen_A+iS_rush_larsen_B*sv->iS;
    GlobalData_t iSp_new = iSp_rush_larsen_A+iSp_rush_larsen_B*sv->iSp;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t jTT2_new = jTT2_rush_larsen_A+jTT2_rush_larsen_B*sv->jTT2;
    GlobalData_t jca_new = jca_rush_larsen_A+jca_rush_larsen_B*sv->jca;
    GlobalData_t jp_new = jp_rush_larsen_A+jp_rush_larsen_B*sv->jp;
    GlobalData_t mL_new = mL_rush_larsen_A+mL_rush_larsen_B*sv->mL;
    GlobalData_t mORd_new = mORd_rush_larsen_A+mORd_rush_larsen_B*sv->mORd;
    GlobalData_t mTT2_new = mTT2_rush_larsen_A+mTT2_rush_larsen_B*sv->mTT2;
    GlobalData_t xk1_new = xk1_rush_larsen_A+xk1_rush_larsen_B*sv->xk1;
    GlobalData_t xrf_new = xrf_rush_larsen_A+xrf_rush_larsen_B*sv->xrf;
    GlobalData_t xrs_new = xrs_rush_larsen_A+xrs_rush_larsen_B*sv->xrs;
    GlobalData_t xs1_new = xs1_rush_larsen_A+xs1_rush_larsen_B*sv->xs1;
    GlobalData_t xs2_new = xs2_rush_larsen_A+xs2_rush_larsen_B*sv->xs2;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (CaMKt_new < 1e-9) { IIF_warn(__i, "CaMKt < 1e-9, clamping to 1e-9"); sv->CaMKt = 1e-9; }
    else {sv->CaMKt = CaMKt_new;}
    if (Cai_new < 1e-9) { IIF_warn(__i, "Cai < 1e-9, clamping to 1e-9"); sv->Cai = 1e-9; }
    else {sv->Cai = Cai_new;}
    Iion = Iion;
    sv->Jrelnp = Jrelnp_new;
    sv->Jrelp = Jrelp_new;
    if (Ki_new < 1e-9) { IIF_warn(__i, "Ki < 1e-9, clamping to 1e-9"); sv->Ki = 1e-9; }
    else {sv->Ki = Ki_new;}
    if (Nai_new < 1e-9) { IIF_warn(__i, "Nai < 1e-9, clamping to 1e-9"); sv->Nai = 1e-9; }
    else {sv->Nai = Nai_new;}
    if (a_new < 0) { IIF_warn(__i, "a < 0, clamping to 0"); sv->a = 0; }
    else if (a_new > 1) { IIF_warn(__i, "a > 1, clamping to 1"); sv->a = 1; }
    else {sv->a = a_new;}
    if (ap_new < 0) { IIF_warn(__i, "ap < 0, clamping to 0"); sv->ap = 0; }
    else if (ap_new > 1) { IIF_warn(__i, "ap > 1, clamping to 1"); sv->ap = 1; }
    else {sv->ap = ap_new;}
    if (cajsr_new < 1e-9) { IIF_warn(__i, "cajsr < 1e-9, clamping to 1e-9"); sv->cajsr = 1e-9; }
    else {sv->cajsr = cajsr_new;}
    if (cansr_new < 1e-9) { IIF_warn(__i, "cansr < 1e-9, clamping to 1e-9"); sv->cansr = 1e-9; }
    else {sv->cansr = cansr_new;}
    if (cass_new < 1e-9) { IIF_warn(__i, "cass < 1e-9, clamping to 1e-9"); sv->cass = 1e-9; }
    else {sv->cass = cass_new;}
    if (d_new < 0) { IIF_warn(__i, "d < 0, clamping to 0"); sv->d = 0; }
    else if (d_new > 1) { IIF_warn(__i, "d > 1, clamping to 1"); sv->d = 1; }
    else {sv->d = d_new;}
    if (fcaf_new < 0) { IIF_warn(__i, "fcaf < 0, clamping to 0"); sv->fcaf = 0; }
    else if (fcaf_new > 1) { IIF_warn(__i, "fcaf > 1, clamping to 1"); sv->fcaf = 1; }
    else {sv->fcaf = fcaf_new;}
    if (fcafp_new < 0) { IIF_warn(__i, "fcafp < 0, clamping to 0"); sv->fcafp = 0; }
    else if (fcafp_new > 1) { IIF_warn(__i, "fcafp > 1, clamping to 1"); sv->fcafp = 1; }
    else {sv->fcafp = fcafp_new;}
    if (fcas_new < 0) { IIF_warn(__i, "fcas < 0, clamping to 0"); sv->fcas = 0; }
    else if (fcas_new > 1) { IIF_warn(__i, "fcas > 1, clamping to 1"); sv->fcas = 1; }
    else {sv->fcas = fcas_new;}
    if (ff_new < 0) { IIF_warn(__i, "ff < 0, clamping to 0"); sv->ff = 0; }
    else if (ff_new > 1) { IIF_warn(__i, "ff > 1, clamping to 1"); sv->ff = 1; }
    else {sv->ff = ff_new;}
    if (ffp_new < 0) { IIF_warn(__i, "ffp < 0, clamping to 0"); sv->ffp = 0; }
    else if (ffp_new > 1) { IIF_warn(__i, "ffp > 1, clamping to 1"); sv->ffp = 1; }
    else {sv->ffp = ffp_new;}
    if (fs_new < 0) { IIF_warn(__i, "fs < 0, clamping to 0"); sv->fs = 0; }
    else if (fs_new > 1) { IIF_warn(__i, "fs > 1, clamping to 1"); sv->fs = 1; }
    else {sv->fs = fs_new;}
    if (hL_new < 0) { IIF_warn(__i, "hL < 0, clamping to 0"); sv->hL = 0; }
    else if (hL_new > 1) { IIF_warn(__i, "hL > 1, clamping to 1"); sv->hL = 1; }
    else {sv->hL = hL_new;}
    if (hLp_new < 0) { IIF_warn(__i, "hLp < 0, clamping to 0"); sv->hLp = 0; }
    else if (hLp_new > 1) { IIF_warn(__i, "hLp > 1, clamping to 1"); sv->hLp = 1; }
    else {sv->hLp = hLp_new;}
    sv->hTT2 = hTT2_new;
    if (hf_new < 0) { IIF_warn(__i, "hf < 0, clamping to 0"); sv->hf = 0; }
    else if (hf_new > 1) { IIF_warn(__i, "hf > 1, clamping to 1"); sv->hf = 1; }
    else {sv->hf = hf_new;}
    if (hs_new < 0) { IIF_warn(__i, "hs < 0, clamping to 0"); sv->hs = 0; }
    else if (hs_new > 1) { IIF_warn(__i, "hs > 1, clamping to 1"); sv->hs = 1; }
    else {sv->hs = hs_new;}
    if (hsp_new < 0) { IIF_warn(__i, "hsp < 0, clamping to 0"); sv->hsp = 0; }
    else if (hsp_new > 1) { IIF_warn(__i, "hsp > 1, clamping to 1"); sv->hsp = 1; }
    else {sv->hsp = hsp_new;}
    if (iF_new < 0) { IIF_warn(__i, "iF < 0, clamping to 0"); sv->iF = 0; }
    else if (iF_new > 1) { IIF_warn(__i, "iF > 1, clamping to 1"); sv->iF = 1; }
    else {sv->iF = iF_new;}
    if (iFp_new < 0) { IIF_warn(__i, "iFp < 0, clamping to 0"); sv->iFp = 0; }
    else if (iFp_new > 1) { IIF_warn(__i, "iFp > 1, clamping to 1"); sv->iFp = 1; }
    else {sv->iFp = iFp_new;}
    if (iS_new < 0) { IIF_warn(__i, "iS < 0, clamping to 0"); sv->iS = 0; }
    else if (iS_new > 1) { IIF_warn(__i, "iS > 1, clamping to 1"); sv->iS = 1; }
    else {sv->iS = iS_new;}
    if (iSp_new < 0) { IIF_warn(__i, "iSp < 0, clamping to 0"); sv->iSp = 0; }
    else if (iSp_new > 1) { IIF_warn(__i, "iSp > 1, clamping to 1"); sv->iSp = 1; }
    else {sv->iSp = iSp_new;}
    if (j_new < 0) { IIF_warn(__i, "j < 0, clamping to 0"); sv->j = 0; }
    else if (j_new > 1) { IIF_warn(__i, "j > 1, clamping to 1"); sv->j = 1; }
    else {sv->j = j_new;}
    sv->jTT2 = jTT2_new;
    if (jca_new < 0) { IIF_warn(__i, "jca < 0, clamping to 0"); sv->jca = 0; }
    else if (jca_new > 1) { IIF_warn(__i, "jca > 1, clamping to 1"); sv->jca = 1; }
    else {sv->jca = jca_new;}
    if (jp_new < 0) { IIF_warn(__i, "jp < 0, clamping to 0"); sv->jp = 0; }
    else if (jp_new > 1) { IIF_warn(__i, "jp > 1, clamping to 1"); sv->jp = 1; }
    else {sv->jp = jp_new;}
    if (kss_new < 1e-9) { IIF_warn(__i, "kss < 1e-9, clamping to 1e-9"); sv->kss = 1e-9; }
    else {sv->kss = kss_new;}
    if (mL_new < 0) { IIF_warn(__i, "mL < 0, clamping to 0"); sv->mL = 0; }
    else if (mL_new > 1) { IIF_warn(__i, "mL > 1, clamping to 1"); sv->mL = 1; }
    else {sv->mL = mL_new;}
    sv->mORd = mORd_new;
    sv->mTT2 = mTT2_new;
    if (nass_new < 1e-9) { IIF_warn(__i, "nass < 1e-9, clamping to 1e-9"); sv->nass = 1e-9; }
    else {sv->nass = nass_new;}
    if (nca_new < 1e-9) { IIF_warn(__i, "nca < 1e-9, clamping to 1e-9"); sv->nca = 1e-9; }
    else {sv->nca = nca_new;}
    if (xk1_new < 0) { IIF_warn(__i, "xk1 < 0, clamping to 0"); sv->xk1 = 0; }
    else if (xk1_new > 1) { IIF_warn(__i, "xk1 > 1, clamping to 1"); sv->xk1 = 1; }
    else {sv->xk1 = xk1_new;}
    if (xrf_new < 0) { IIF_warn(__i, "xrf < 0, clamping to 0"); sv->xrf = 0; }
    else if (xrf_new > 1) { IIF_warn(__i, "xrf > 1, clamping to 1"); sv->xrf = 1; }
    else {sv->xrf = xrf_new;}
    if (xrs_new < 0) { IIF_warn(__i, "xrs < 0, clamping to 0"); sv->xrs = 0; }
    else if (xrs_new > 1) { IIF_warn(__i, "xrs > 1, clamping to 1"); sv->xrs = 1; }
    else {sv->xrs = xrs_new;}
    if (xs1_new < 0) { IIF_warn(__i, "xs1 < 0, clamping to 0"); sv->xs1 = 0; }
    else if (xs1_new > 1) { IIF_warn(__i, "xs1 > 1, clamping to 1"); sv->xs1 = 1; }
    else {sv->xs1 = xs1_new;}
    if (xs2_new < 0) { IIF_warn(__i, "xs2 < 0, clamping to 0"); sv->xs2 = 0; }
    else if (xs2_new > 1) { IIF_warn(__i, "xs2 > 1, clamping to 1"); sv->xs2 = 1; }
    else {sv->xs2 = xs2_new;}
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    v_ext[__i] = v;

  }

            }
}
#endif // OHARA_CPU_GENERATED

bool OHaraIonType::has_trace() const {
    return true;
}

void OHaraIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("OHara_trace_header.txt","wt");
    fprintf(theader->fd,
        "CaMKa\n"
        "ICaL\n"
        "IK1\n"
        "IKb\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaCa_ss\n"
        "INaK\n"
        "INaL\n"
        "IbCa\n"
        "IbNa\n"
        "IpCa\n"
        "Ito\n"
        "Jdiff\n"
        "JdiffK\n"
        "JdiffNa\n"
        "Jleak\n"
        "Jrel\n"
        "Jtr\n"
        "Jup\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  OHara_Params *p  = imp.params();

  OHara_state *sv_base = (OHara_state *)imp.sv_tab().data();

  OHara_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t H = ((p->modelformulation==CLERX) ? 1.0e-4 : 1.0e-7);
  GlobalData_t PCa = ((p->celltype==EPI) ? (0.0001*1.2) : ((p->celltype==MCELL) ? (0.0001*2.5) : 0.0001));
  GlobalData_t Pnak = ((p->celltype==EPI) ? (30.*0.9) : ((p->celltype==MCELL) ? (30.*0.7) : 30.));
  GlobalData_t cmdnmax = ((p->celltype==EPI) ? (0.05*1.3) : 0.05);
  GlobalData_t h10_i = ((kasymm+1.0)+((p->Nao/kna1)*(1.0+(p->Nao/kna2))));
  GlobalData_t h10_ss = ((kasymm+1.0)+((p->Nao/kna1)*(1.+(p->Nao/kna2))));
  GlobalData_t hL_rush_larsen_B = (exp(((-dt)/tau_hL)));
  GlobalData_t hL_rush_larsen_C = (expm1(((-dt)/tau_hL)));
  GlobalData_t hLp_rush_larsen_B = (exp(((-dt)/tau_hLp)));
  GlobalData_t hLp_rush_larsen_C = (expm1(((-dt)/tau_hLp)));
  GlobalData_t jca_rush_larsen_B = (exp(((-dt)/tau_jca)));
  GlobalData_t jca_rush_larsen_C = (expm1(((-dt)/tau_jca)));
  GlobalData_t sqrt_Ko = (sqrt(p->Ko));
  GlobalData_t sqrt_Ko_54 = (sqrt((p->Ko/5.4)));
  GlobalData_t PCaK = (3.574e-4*PCa);
  GlobalData_t PCaNa = (0.00125*PCa);
  GlobalData_t PCap = (1.1*PCa);
  GlobalData_t h11_i = ((p->Nao*p->Nao)/((h10_i*kna1)*kna2));
  GlobalData_t h11_ss = ((p->Nao*p->Nao)/((h10_ss*kna1)*kna2));
  GlobalData_t h12_i = (1.0/h10_i);
  GlobalData_t h12_ss = (1.0/h10_ss);
  GlobalData_t PCaKp = (3.574e-4*PCap);
  GlobalData_t PCaNap = (0.00125*PCap);
  GlobalData_t k1_i = ((h12_i*p->Cao)*kCaon);
  GlobalData_t k1_ss = ((h12_ss*p->Cao)*kCaon);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t v = v_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  GlobalData_t Afcaf = (0.3+(0.6/(1.0+(exp(((v-(10.0))/10.0))))));
  GlobalData_t AiF = (1.0/(1.0+(exp(((v-(213.6))/151.2)))));
  GlobalData_t Axrf = (1.0/(1.0+(exp(((v+54.81)/38.21)))));
  GlobalData_t CaMKb = ((CaMKo*(1.0-(sv->CaMKt)))/(1.0+(KmCaM/sv->cass)));
  GlobalData_t EK = (((R*T)/F)*(log((p->Ko/sv->Ki))));
  GlobalData_t EKs = (((R*T)/F)*(log(((p->Ko+(PKNa*p->Nao))/(sv->Ki+(PKNa*sv->Nai))))));
  GlobalData_t ENa = (((R*T)/F)*(log((p->Nao/sv->Nai))));
  GlobalData_t IpCa = ((p->GpCa*sv->Cai)/(0.0005+sv->Cai));
  GlobalData_t Jdiff = ((sv->cass-(sv->Cai))/0.2);
  GlobalData_t JdiffK = ((sv->kss-(sv->Ki))/2.0);
  GlobalData_t JdiffNa = ((sv->nass-(sv->Nai))/2.0);
  GlobalData_t Jleak = ((0.0039375*sv->cansr)/15.0);
  GlobalData_t Jtr = ((sv->cansr-(sv->cajsr))/100.0);
  GlobalData_t Jupnp = ((p->celltype==EPI) ? (((0.004375*sv->Cai)/(sv->Cai+0.00092))*1.3) : ((0.004375*sv->Cai)/(sv->Cai+0.00092)));
  GlobalData_t Jupp = ((p->celltype==EPI) ? ((((2.75*0.004375)*sv->Cai)/((sv->Cai+0.00092)-(0.00017)))*1.3) : (((2.75*0.004375)*sv->Cai)/((sv->Cai+0.00092)-(0.00017))));
  GlobalData_t KNao = (KNao0*(exp(((((1.0-(delta))*v)*F)/((3.0*R)*T)))));
  GlobalData_t Knai = (Knai0*(exp((((delta*v)*F)/((3.0*R)*T)))));
  GlobalData_t KsCa = (1.0+(0.6/(1.0+(pow((3.8e-5/sv->Cai),1.4)))));
  GlobalData_t P = (eP/(((1.0+(H/Khp))+(sv->Nai/Knap))+(sv->Ki/Kxkur)));
  GlobalData_t allo_i = (1.0/(1.0+((KmCaAct_i/sv->Cai)*(KmCaAct_i/sv->Cai))));
  GlobalData_t allo_ss = (1.0/(1.0+((KmCaAct_ss/sv->cass)*(KmCaAct_ss/sv->cass))));
  GlobalData_t f = ((Aff*sv->ff)+(Afs*sv->fs));
  GlobalData_t fp = ((Aff*sv->ffp)+(Afs*sv->fs));
  GlobalData_t h = ((Ahf*sv->hf)+(Ahs*sv->hs));
  GlobalData_t h4_i = (1.0+((sv->Nai/kna1)*(1.+(sv->Nai/kna2))));
  GlobalData_t h4_ss = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
  GlobalData_t hca = (exp((((qca*v)*F)/(R*T))));
  GlobalData_t hna = (exp((((qna*v)*F)/(R*T))));
  GlobalData_t hp = ((Ahf*sv->hf)+(Ahs*sv->hsp));
  GlobalData_t rk1 = (1.0/(1.0+(exp((((v+105.8)-((2.6*p->Ko)))/9.493)))));
  GlobalData_t rkr = ((1.0/(1.0+(exp(((v+55.0)/75.0)))))/(1.0+(exp(((v-(10.0))/30.0)))));
  GlobalData_t vffrt = (((v*F)*F)/(R*T));
  GlobalData_t vfrt = ((v*F)/(R*T));
  GlobalData_t xkb = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
  GlobalData_t Afcas = (1.0-(Afcaf));
  GlobalData_t AiS = (1.0-(AiF));
  GlobalData_t Axrs = (1.0-(Axrf));
  GlobalData_t CaMKa = (CaMKb+sv->CaMKt);
  GlobalData_t IK1 = ((((p->GK1*sqrt_Ko)*rk1)*sv->xk1)*(v-(EK)));
  GlobalData_t IKb = ((p->GKb*xkb)*(v-(EK)));
  GlobalData_t IKs = ((((p->GKs*KsCa)*sv->xs1)*sv->xs2)*(v-(EKs)));
  GlobalData_t Nao_KNao_3 = (((p->Nao/KNao)*(p->Nao/KNao))*(p->Nao/KNao));
  GlobalData_t Nao_KNao_p1_3 = (((1.0+(p->Nao/KNao))*(1.0+(p->Nao/KNao)))*(1.0+(p->Nao/KNao)));
  GlobalData_t b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
  GlobalData_t exp_2vfrt = (exp((2.0*vfrt)));
  GlobalData_t exp_vfrt = (exp(vfrt));
  GlobalData_t h1_i = (1.+((sv->Nai/kna3)*(1.+hna)));
  GlobalData_t h1_ss = (1.+((sv->nass/kna3)*(1.+hna)));
  GlobalData_t h5_i = ((sv->Nai*sv->Nai)/((h4_i*kna1)*kna2));
  GlobalData_t h5_ss = ((sv->nass*sv->nass)/((h4_ss*kna1)*kna2));
  GlobalData_t h6_i = (1.0/h4_i);
  GlobalData_t h6_ss = (1.0/h4_ss);
  GlobalData_t h7_i = (1.0+((p->Nao/kna3)*(1.0+(1.0/hna))));
  GlobalData_t h7_ss = (1.0+((p->Nao/kna3)*(1.0+(1.0/hna))));
  GlobalData_t nai_Knai_3 = (((sv->Nai/Knai)*(sv->Nai/Knai))*(sv->Nai/Knai));
  GlobalData_t nai_Knai_p1_3 = (((1.0+(sv->Nai/Knai))*(1.0+(sv->Nai/Knai)))*(1.0+(sv->Nai/Knai)));
  GlobalData_t vffrt_expm1_2vfrt = ((v==0.) ? (F/2.) : (vffrt/(expm1((2.0*vfrt)))));
  GlobalData_t vffrt_expm1_vfrt = ((v==0.) ? F : (vffrt/(expm1(vfrt))));
  GlobalData_t IbCa = ((((p->factorIbCa*PCab)*4.0)*vffrt_expm1_2vfrt)*((sv->Cai*exp_2vfrt)-((0.341*p->Cao))));
  GlobalData_t IbNa = (((p->factorIbNa*PNab)*vffrt_expm1_vfrt)*((sv->Nai*exp_vfrt)-(p->Nao)));
  GlobalData_t PhiCaL = ((4.0*vffrt_expm1_2vfrt)*((sv->cass*exp_2vfrt)-((0.341*p->Cao))));
  GlobalData_t a1 = ((k1p*nai_Knai_3)/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
  GlobalData_t a3 = (((k3p*(p->Ko/KKo))*(p->Ko/KKo))/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
  GlobalData_t b2 = ((k2m*Nao_KNao_3)/((Nao_KNao_p1_3+((1.0+(p->Ko/KKo))*(1.0+(p->Ko/KKo))))-(1.0)));
  GlobalData_t b4 = (((k4m*(sv->Ki/Kki))*(sv->Ki/Kki))/((nai_Knai_p1_3+((1.0+(sv->Ki/Kki))*(1.0+(sv->Ki/Kki))))-(1.0)));
  GlobalData_t fICaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fINaLp = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fINap = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fItop = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fJrelp = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fJupp = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t fca = ((Afcaf*sv->fcaf)+(Afcas*sv->fcas));
  GlobalData_t fcap = ((Afcaf*sv->fcafp)+(Afcas*sv->fcas));
  GlobalData_t h2_i = ((sv->Nai*hna)/(kna3*h1_i));
  GlobalData_t h2_ss = ((sv->nass*hna)/(kna3*h1_ss));
  GlobalData_t h3_i = (1.0/h1_i);
  GlobalData_t h3_ss = (1.0/h1_ss);
  GlobalData_t h8_i = (p->Nao/((kna3*hna)*h7_i));
  GlobalData_t h8_ss = (p->Nao/((kna3*hna)*h7_ss));
  GlobalData_t h9_i = (1.0/h7_i);
  GlobalData_t h9_ss = (1.0/h7_ss);
  GlobalData_t i = ((AiF*sv->iF)+(AiS*sv->iS));
  GlobalData_t ip = ((AiF*sv->iFp)+(AiS*sv->iSp));
  GlobalData_t k6_i = ((h6_i*sv->Cai)*kCaon);
  GlobalData_t k6_ss = ((h6_ss*sv->cass)*kCaon);
  GlobalData_t xr = ((Axrf*sv->xrf)+(Axrs*sv->xrs));
  GlobalData_t ICaL = ((((((p->factorICaL*(1.0-(fICaLp)))*PCa)*PhiCaL)*sv->d)*((f*(1.0-(sv->nca)))+((sv->jca*fca)*sv->nca)))+((((fICaLp*PCap)*PhiCaL)*sv->d)*((fp*(1.0-(sv->nca)))+((sv->jca*fcap)*sv->nca))));
  GlobalData_t IKr = ((((p->GKr*sqrt_Ko_54)*xr)*rkr)*(v-(EK)));
  GlobalData_t INa = ((p->INa_Type==TT2INa) ? ((((((p->GNa*sv->mTT2)*sv->mTT2)*sv->mTT2)*sv->hTT2)*sv->jTT2)*(v-(ENa))) : (((((p->GNa*(v-(ENa)))*sv->mORd)*sv->mORd)*sv->mORd)*((((1.0-(fINap))*h)*sv->j)+((fINap*hp)*sv->jp))));
  GlobalData_t INaL = (((p->GNaL*(v-(ENa)))*sv->mL)*(((1.0-(fINaLp))*sv->hL)+(fINaLp*sv->hLp)));
  GlobalData_t Ito = ((p->Gto*(v-(EK)))*((((1.0-(fItop))*sv->a)*i)+((fItop*sv->ap)*ip)));
  GlobalData_t Jrel_canidate = (((1.0-(fJrelp))*sv->Jrelnp)+(fJrelp*sv->Jrelp));
  GlobalData_t Jup = ((((1.0-(fJupp))*Jupnp)+(fJupp*Jupp))-(Jleak));
  GlobalData_t k3p_i = (h9_i*wca);
  GlobalData_t k3p_ss = (h9_ss*wca);
  GlobalData_t k3pp_i = (h8_i*wnaca);
  GlobalData_t k3pp_ss = (h8_ss*wnaca);
  GlobalData_t k4p_i = ((h3_i*wca)/hca);
  GlobalData_t k4p_ss = ((h3_ss*wca)/hca);
  GlobalData_t k4pp_i = (h2_i*wnaca);
  GlobalData_t k4pp_ss = (h2_ss*wnaca);
  GlobalData_t k7_i = ((h5_i*h2_i)*wna);
  GlobalData_t k7_ss = ((h5_ss*h2_ss)*wna);
  GlobalData_t k8_i = ((h8_i*h11_i)*wna);
  GlobalData_t k8_ss = ((h8_ss*h11_ss)*wna);
  GlobalData_t x1 = ((p->modelformulation==CLERX) ? (((((a4*a1)*a2)+((b1*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)) : (((((a4*a1)*a2)+((b2*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2)));
  GlobalData_t x2 = (((((b2*b1)*b4)+((a1*a2)*a3))+((a3*b1)*b4))+((a2*a3)*b4));
  GlobalData_t x3 = (((((a2*a3)*a4)+((b3*b2)*b1))+((b2*b1)*a4))+((a3*a4)*b1));
  GlobalData_t x4 = (((((b4*b3)*b2)+((a3*a4)*a1))+((b2*a4)*a1))+((b3*b2)*a1));
  GlobalData_t E1 = (x1/(((x1+x2)+x3)+x4));
  GlobalData_t E2 = (x2/(((x1+x2)+x3)+x4));
  GlobalData_t E3 = (x3/(((x1+x2)+x3)+x4));
  GlobalData_t E4 = (x4/(((x1+x2)+x3)+x4));
  GlobalData_t Jrel = (((Jrel_canidate*p->jrel_stiff_const)>sv->cajsr) ? (sv->cajsr/p->jrel_stiff_const) : Jrel_canidate);
  GlobalData_t k3_i = (k3p_i+k3pp_i);
  GlobalData_t k3_ss = (k3p_ss+k3pp_ss);
  GlobalData_t k4_i = (k4p_i+k4pp_i);
  GlobalData_t k4_ss = (k4p_ss+k4pp_ss);
  GlobalData_t r = (((((a1*a2)*a3)*a4)-((((b1*b2)*b3)*b4)))/(((x1+x2)+x3)+x4));
  GlobalData_t JNaKK = ((p->modelformulation==CLERX) ? (-2.*r) : (2.*((E4*b1)-((E3*a1)))));
  GlobalData_t JNaKNa = ((p->modelformulation==CLERX) ? (3.*r) : (3.*((E1*a3)-((E2*b3)))));
  GlobalData_t x1_i = (((k2_i*k4_i)*(k7_i+k6_i))+((k5_i*k7_i)*(k2_i+k3_i)));
  GlobalData_t x1_ss = (((k2_ss*k4_ss)*(k7_ss+k6_ss))+((k5_ss*k7_ss)*(k2_ss+k3_ss)));
  GlobalData_t x2_i = (((k1_i*k7_i)*(k4_i+k5_i))+((k4_i*k6_i)*(k1_i+k8_i)));
  GlobalData_t x2_ss = (((k1_ss*k7_ss)*(k4_ss+k5_ss))+((k4_ss*k6_ss)*(k1_ss+k8_ss)));
  GlobalData_t x3_i = (((k1_i*k3_i)*(k7_i+k6_i))+((k8_i*k6_i)*(k2_i+k3_i)));
  GlobalData_t x3_ss = (((k1_ss*k3_ss)*(k7_ss+k6_ss))+((k8_ss*k6_ss)*(k2_ss+k3_ss)));
  GlobalData_t x4_i = (((k2_i*k8_i)*(k4_i+k5_i))+((k3_i*k5_i)*(k1_i+k8_i)));
  GlobalData_t x4_ss = (((k2_ss*k8_ss)*(k4_ss+k5_ss))+((k3_ss*k5_ss)*(k1_ss+k8_ss)));
  GlobalData_t E1_i = (x1_i/(((x1_i+x2_i)+x3_i)+x4_i));
  GlobalData_t E1_ss = (x1_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
  GlobalData_t E2_i = (x2_i/(((x1_i+x2_i)+x3_i)+x4_i));
  GlobalData_t E2_ss = (x2_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
  GlobalData_t E3_i = (x3_i/(((x1_i+x2_i)+x3_i)+x4_i));
  GlobalData_t E3_ss = (x3_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
  GlobalData_t E4_i = (x4_i/(((x1_i+x2_i)+x3_i)+x4_i));
  GlobalData_t E4_ss = (x4_ss/(((x1_ss+x2_ss)+x3_ss)+x4_ss));
  GlobalData_t INaK = ((p->factorINaK*Pnak)*((zna*JNaKNa)+(zk*JNaKK)));
  GlobalData_t JncxCa_ss = ((E2_ss*k2_ss)-((E1_ss*k1_ss)));
  GlobalData_t JncxCai = ((E2_i*k2_i)-((E1_i*k1_i)));
  GlobalData_t JncxNa_ss = (((3.0*((E4_ss*k7_ss)-((E1_ss*k8_ss))))+(E3_ss*k4pp_ss))-((E2_ss*k3pp_ss)));
  GlobalData_t JncxNai = (((3.0*((E4_i*k7_i)-((E1_i*k8_i))))+(E3_i*k4pp_i))-((E2_i*k3pp_i)));
  GlobalData_t INaCa = ((((p->factorINaCa*0.8)*p->GNaCa)*allo_i)*((zna*JncxNai)+(zca*JncxCai)));
  GlobalData_t INaCa_ss = ((((p->factorINaCass*0.2)*p->GNaCa)*allo_ss)*((zna*JncxNa_ss)+(zca*JncxCa_ss)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", CaMKa);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKb);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaCa_ss);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", INaL);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Jdiff);
  fprintf(file, "%4.12f\t", JdiffK);
  fprintf(file, "%4.12f\t", JdiffNa);
  fprintf(file, "%4.12f\t", Jleak);
  fprintf(file, "%4.12f\t", Jrel);
  fprintf(file, "%4.12f\t", Jtr);
  fprintf(file, "%4.12f\t", Jup);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}
IonIfBase* OHaraIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void OHaraIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        