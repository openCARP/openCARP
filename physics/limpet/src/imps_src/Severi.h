// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Stefano Severi, Matteo Fantini, Lara A. Charawi, Dario DiFrancesco
*  Year: 2012
*  Title: An updated computational model of rabbit sinoatrial action potential to investigate the mechanisms of heart rate modulation
*  Journal: The Journal of Physiology, 590(18):4483-99
*  DOI: 10.1113/jphysiol.2012.229435
*  Comment: Original Severi model with modified isoprenaline formulation (changes affect ICaL, Casub and Nai) and the possibility to simulate with the adapted beta-adrenergic signalling cascade (Behar et al., 2016).
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __SEVERI_H__
#define __SEVERI_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(SEVERI_CPU_GENERATED)    || defined(SEVERI_MLIR_CPU_GENERATED)    || defined(SEVERI_MLIR_ROCM_GENERATED)    || defined(SEVERI_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define SEVERI_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define SEVERI_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define SEVERI_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define SEVERI_CPU_GENERATED
#endif

namespace limpet {

#define Severi_REQDAT Vm_DATA_FLAG
#define Severi_MODDAT Iion_DATA_FLAG

struct Severi_Params {
    GlobalData_t ACh;
    GlobalData_t CCh_cas;
    GlobalData_t Cao;
    GlobalData_t Cs_5_mM;
    GlobalData_t GKr_max;
    GlobalData_t GKs_max;
    GlobalData_t GNa_max;
    GlobalData_t Gf_K_max;
    GlobalData_t Gf_Na_max;
    GlobalData_t Gto_max;
    GlobalData_t INaK_max;
    GlobalData_t Iso_1_uM;
    GlobalData_t Iso_cas;
    GlobalData_t Iva_3_uM;
    GlobalData_t K_Iso_inc_cas_ICaL;
    GlobalData_t K_Iso_inc_cas_IKs;
    GlobalData_t K_Iso_inc_cas_INaK;
    GlobalData_t K_Iso_shift_cas_ICaL_dL_gate;
    GlobalData_t K_Iso_shift_cas_IKs_n_gate;
    GlobalData_t K_Iso_shift_cas_If_y_gate;
    GlobalData_t K_Iso_slope_cas_ICaL;
    GlobalData_t K_NaCa;
    GlobalData_t K_j_SRCarel;
    GlobalData_t K_j_up;
    GlobalData_t K_k1;
    GlobalData_t K_koCa;
    GlobalData_t P_CaL;
    GlobalData_t P_CaT;

};

struct Severi_state {
    GlobalData_t A;
    GlobalData_t Ca_jsr;
    GlobalData_t Ca_nsr;
    GlobalData_t Ca_sub;
    GlobalData_t Cai;
    GlobalData_t Cam;
    GlobalData_t I;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t PLBp;
    GlobalData_t RI;
    GlobalData_t R_Ca_SR_release;
    GlobalData_t TT;
    GlobalData_t U;
    GlobalData_t a;
    GlobalData_t cAMP;
    GlobalData_t dL;
    GlobalData_t dT;
    GlobalData_t fBAPTA;
    GlobalData_t fBAPTA_sub;
    GlobalData_t fCMi;
    GlobalData_t fCMs;
    GlobalData_t fCQ;
    GlobalData_t fCa;
    GlobalData_t fL;
    GlobalData_t fT;
    GlobalData_t fTC;
    GlobalData_t fTMC;
    GlobalData_t fTMM;
    GlobalData_t h;
    GlobalData_t m;
    GlobalData_t n;
    GlobalData_t paF;
    GlobalData_t paS;
    GlobalData_t piy;
    GlobalData_t q;
    GlobalData_t r;
    GlobalData_t y;

};

class SeveriIonType : public IonType {
public:
    using IonIfDerived = IonIf<SeveriIonType>;
    using params_type = Severi_Params;
    using state_type = Severi_state;

    SeveriIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Severi(int, int, IonIfBase&, GlobalData_t**);
#ifdef SEVERI_CPU_GENERATED
void compute_Severi_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SEVERI_MLIR_CPU_GENERATED
void compute_Severi_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SEVERI_MLIR_ROCM_GENERATED
void compute_Severi_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef SEVERI_MLIR_CUDA_GENERATED
void compute_Severi_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
