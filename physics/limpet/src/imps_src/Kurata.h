// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: Dynamical description of sinoatrial node pacemaking: improved mathematical model for primary pacemaker cell
*  Authors: Yasutaka Kurata, Ichiro Hisatome, Sunao Imanishi, and Toshishige Shibamoto
*  Year: 2002
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology 2002 283:5, H2074-H2101
*  DOI: 10.1152/ajpheart.00900.2001
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __KURATA_H__
#define __KURATA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(KURATA_CPU_GENERATED)    || defined(KURATA_MLIR_CPU_GENERATED)    || defined(KURATA_MLIR_ROCM_GENERATED)    || defined(KURATA_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define KURATA_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define KURATA_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define KURATA_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define KURATA_CPU_GENERATED
#endif

namespace limpet {

#define Kurata_REQDAT Vm_DATA_FLAG
#define Kurata_MODDAT Iion_DATA_FLAG

struct Kurata_Params {
    GlobalData_t ACh;
    GlobalData_t B_max;
    GlobalData_t Cm;
    GlobalData_t GCaL;
    GlobalData_t GKACh_max;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t Gh;
    GlobalData_t GhK;
    GlobalData_t GhNa;
    GlobalData_t Gst;
    GlobalData_t Gsus;
    GlobalData_t Gto;

};

struct Kurata_state {
    GlobalData_t CaimM;
    GlobalData_t Carel;
    GlobalData_t Casub;
    GlobalData_t Caup;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype d_L;
    Gatetype d_T;
    GlobalData_t f_CMi;
    GlobalData_t f_CMs;
    GlobalData_t f_CQ;
    Gatetype f_Ca;
    Gatetype f_L;
    Gatetype f_T;
    GlobalData_t f_TC;
    GlobalData_t f_TMC;
    GlobalData_t f_TMM;
    Gatetype hf;
    Gatetype hs;
    Gatetype m;
    Gatetype n;
    Gatetype paF;
    Gatetype paS;
    Gatetype pi;
    Gatetype q;
    Gatetype q_a;
    Gatetype q_i;
    Gatetype r;
    Gatetype y;

};

class KurataIonType : public IonType {
public:
    using IonIfDerived = IonIf<KurataIonType>;
    using params_type = Kurata_Params;
    using state_type = Kurata_state;

    KurataIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Kurata(int, int, IonIfBase&, GlobalData_t**);
#ifdef KURATA_CPU_GENERATED
void compute_Kurata_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef KURATA_MLIR_CPU_GENERATED
void compute_Kurata_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef KURATA_MLIR_ROCM_GENERATED
void compute_Kurata_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef KURATA_MLIR_CUDA_GENERATED
void compute_Kurata_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
