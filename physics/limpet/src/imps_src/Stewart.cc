// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Philip Stewart, Oleg V. Aslanidi, Denis Noble, Penelope J. Noble, Mark R. Boyett and Henggui Zhang
*  Year: 2009
*  Title: Mathematical models of the electrical action potential of Purkinje fibre cells
*  Journal: Phil. Trans. R. Soc. A., 367, 2225-2255
*  DOI: 10.1098/rsta.2008.0283
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Stewart.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

StewartIonType::StewartIonType(bool plugin) : IonType(std::move(std::string("Stewart")), plugin) {}

size_t StewartIonType::params_size() const {
  return sizeof(struct Stewart_Params);
}

size_t StewartIonType::dlo_vector_size() const {

  return 1;
}

uint32_t StewartIonType::reqdat() const {
  return Stewart_REQDAT;
}

uint32_t StewartIonType::moddat() const {
  return Stewart_MODDAT;
}

void StewartIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target StewartIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef STEWART_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(STEWART_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(STEWART_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(STEWART_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef STEWART_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef STEWART_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef STEWART_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef STEWART_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void StewartIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef STEWART_MLIR_CUDA_GENERATED
      compute_Stewart_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(STEWART_MLIR_ROCM_GENERATED)
      compute_Stewart_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(STEWART_MLIR_CPU_GENERATED)
      compute_Stewart_mlir_cpu(start, end, imp, data);
#   elif defined(STEWART_CPU_GENERATED)
      compute_Stewart_cpu(start, end, imp, data);
#   else
#     error "Could not generate method StewartIonType::compute."
#   endif
      break;
#   ifdef STEWART_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Stewart_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef STEWART_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Stewart_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef STEWART_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Stewart_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef STEWART_CPU_GENERATED
    case Target::CPU:
      compute_Stewart_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Buf_c (GlobalData_t)(0.2)
#define Buf_sr (GlobalData_t)(10.)
#define Buf_ss (GlobalData_t)(0.4)
#define Ca_SR_init (GlobalData_t)(3.10836886659417)
#define Ca_SR_rush_larsen_B (GlobalData_t)(1.)
#define Cai_init (GlobalData_t)(0.000101878186157052)
#define Cai_rush_larsen_B (GlobalData_t)(1.)
#define Cao (GlobalData_t)(2.0)
#define Cass_init (GlobalData_t)(0.000446818714055411)
#define Cass_rush_larsen_B (GlobalData_t)(1.)
#define Cm (GlobalData_t)(0.185)
#define EC (GlobalData_t)(1.5)
#define F (GlobalData_t)(96485.3415)
#define K_buf_c (GlobalData_t)(0.001)
#define K_buf_sr (GlobalData_t)(0.3)
#define K_buf_ss (GlobalData_t)(0.00025)
#define K_mNa (GlobalData_t)(40.)
#define K_mk (GlobalData_t)(1.)
#define K_pCa (GlobalData_t)(0.0005)
#define K_sat (GlobalData_t)(0.1)
#define K_up (GlobalData_t)(0.00025)
#define Ki_init (GlobalData_t)(136.781894160227)
#define Km_Ca (GlobalData_t)(1.38)
#define Km_Nai (GlobalData_t)(87.5)
#define Ko (GlobalData_t)(5.4)
#define Nai_init (GlobalData_t)(8.80420286531673)
#define Nao (GlobalData_t)(140.)
#define P_kna (GlobalData_t)(0.03)
#define R (GlobalData_t)(8314.472)
#define R_prime_init (GlobalData_t)(0.991580051907845)
#define T (GlobalData_t)(310.)
#define V_c (GlobalData_t)(0.016404)
#define V_init (GlobalData_t)(-69.1370441635924)
#define V_leak (GlobalData_t)(0.00036)
#define V_rel (GlobalData_t)(0.102)
#define V_sr (GlobalData_t)(0.001094)
#define V_ss (GlobalData_t)(5.468e-5)
#define V_xfer (GlobalData_t)(0.0038)
#define Vmax_up (GlobalData_t)(0.006375)
#define Xr1_init (GlobalData_t)(0.00550281999719088)
#define Xr2_init (GlobalData_t)(0.313213286437995)
#define Xs_init (GlobalData_t)(0.00953708522974789)
#define alpha (GlobalData_t)(2.5)
#define d_init (GlobalData_t)(0.000287906256206415)
#define f2_init (GlobalData_t)(0.995474890442185)
#define fCass_init (GlobalData_t)(0.999955429598213)
#define f_init (GlobalData_t)(0.989328560287987)
#define gamma (GlobalData_t)(0.35)
#define h_init (GlobalData_t)(0.190678733735145)
#define j_init (GlobalData_t)(0.238219836154029)
#define k1_prime (GlobalData_t)(0.15)
#define k2_prime (GlobalData_t)(0.045)
#define k3 (GlobalData_t)(0.06)
#define k4 (GlobalData_t)(0.005)
#define m_init (GlobalData_t)(0.0417391656294997)
#define max_sr (GlobalData_t)(2.5)
#define min_sr (GlobalData_t)(1.)
#define partial_Buf_c_del_Cai (GlobalData_t)(0.)
#define partial_Buf_sr_del_Ca_SR (GlobalData_t)(0.)
#define partial_Buf_ss_del_Cass (GlobalData_t)(0.)
#define partial_Cao_del_Cai (GlobalData_t)(0.)
#define partial_Cao_del_Cass (GlobalData_t)(0.)
#define partial_Cm_del_Cai (GlobalData_t)(0.)
#define partial_Cm_del_Cass (GlobalData_t)(0.)
#define partial_EC_del_Ca_SR (GlobalData_t)(0.)
#define partial_EC_del_Cass (GlobalData_t)(0.)
#define partial_EC_del_R_prime (GlobalData_t)(0.)
#define partial_F_del_Cai (GlobalData_t)(0.)
#define partial_F_del_Cass (GlobalData_t)(0.)
#define partial_GCaL_del_Cass (GlobalData_t)(0.)
#define partial_Gbca_del_Cai (GlobalData_t)(0.)
#define partial_GpCa_del_Cai (GlobalData_t)(0.)
#define partial_K_buf_c_del_Cai (GlobalData_t)(0.)
#define partial_K_buf_sr_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_buf_ss_del_Cass (GlobalData_t)(0.)
#define partial_K_pCa_del_Cai (GlobalData_t)(0.)
#define partial_K_sat_del_Cai (GlobalData_t)(0.)
#define partial_K_up_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_up_del_Cai (GlobalData_t)(0.)
#define partial_Km_Ca_del_Cai (GlobalData_t)(0.)
#define partial_Km_Nai_del_Cai (GlobalData_t)(0.)
#define partial_Nao_del_Cai (GlobalData_t)(0.)
#define partial_R_del_Cai (GlobalData_t)(0.)
#define partial_R_del_Cass (GlobalData_t)(0.)
#define partial_T_del_Cai (GlobalData_t)(0.)
#define partial_T_del_Cass (GlobalData_t)(0.)
#define partial_V_c_del_Cai (GlobalData_t)(0.)
#define partial_V_c_del_Cass (GlobalData_t)(0.)
#define partial_V_leak_del_Ca_SR (GlobalData_t)(0.)
#define partial_V_leak_del_Cai (GlobalData_t)(0.)
#define partial_V_rel_del_Ca_SR (GlobalData_t)(0.)
#define partial_V_rel_del_Cass (GlobalData_t)(0.)
#define partial_V_sr_del_Cai (GlobalData_t)(0.)
#define partial_V_sr_del_Cass (GlobalData_t)(0.)
#define partial_V_ss_del_Cass (GlobalData_t)(0.)
#define partial_V_xfer_del_Cai (GlobalData_t)(0.)
#define partial_V_xfer_del_Cass (GlobalData_t)(0.)
#define partial_Vmax_up_del_Ca_SR (GlobalData_t)(0.)
#define partial_Vmax_up_del_Cai (GlobalData_t)(0.)
#define partial_alpha_del_Cai (GlobalData_t)(0.)
#define partial_alpha_xr1_del_Xr1 (GlobalData_t)(0.)
#define partial_alpha_xr2_del_Xr2 (GlobalData_t)(0.)
#define partial_alpha_xs_del_Xs (GlobalData_t)(0.)
#define partial_beta_xr1_del_Xr1 (GlobalData_t)(0.)
#define partial_beta_xr2_del_Xr2 (GlobalData_t)(0.)
#define partial_beta_xs_del_Xs (GlobalData_t)(0.)
#define partial_gamma_del_Cai (GlobalData_t)(0.)
#define partial_i_up_del_Ca_SR (GlobalData_t)(0.)
#define partial_k1_del_Cass (GlobalData_t)(0.)
#define partial_k1_prime_del_Ca_SR (GlobalData_t)(0.)
#define partial_k1_prime_del_Cass (GlobalData_t)(0.)
#define partial_k2_del_R_prime (GlobalData_t)(0.)
#define partial_k2_prime_del_R_prime (GlobalData_t)(0.)
#define partial_k3_del_Ca_SR (GlobalData_t)(0.)
#define partial_k3_del_Cass (GlobalData_t)(0.)
#define partial_k4_del_R_prime (GlobalData_t)(0.)
#define partial_kcasr_del_Cass (GlobalData_t)(0.)
#define partial_kcasr_del_R_prime (GlobalData_t)(0.)
#define partial_maxINaCa_del_Cai (GlobalData_t)(0.)
#define partial_max_sr_del_Ca_SR (GlobalData_t)(0.)
#define partial_max_sr_del_Cass (GlobalData_t)(0.)
#define partial_max_sr_del_R_prime (GlobalData_t)(0.)
#define partial_min_sr_del_Ca_SR (GlobalData_t)(0.)
#define partial_min_sr_del_Cass (GlobalData_t)(0.)
#define partial_min_sr_del_R_prime (GlobalData_t)(0.)
#define partial_tau_xr1_del_Xr1 (GlobalData_t)(0.)
#define partial_tau_xr2_del_Xr2 (GlobalData_t)(0.)
#define partial_tau_xs_del_Xs (GlobalData_t)(0.)
#define partial_xr1_inf_del_Xr1 (GlobalData_t)(0.)
#define partial_xr2_inf_del_Xr2 (GlobalData_t)(0.)
#define partial_xs_inf_del_Xs (GlobalData_t)(0.)
#define r_init (GlobalData_t)(0.00103618091196912)
#define s_init (GlobalData_t)(0.96386101799501)
#define set_R_prime_tozero_in_EC (GlobalData_t)(1.5)
#define set_R_prime_tozero_in_diff_R_prime (GlobalData_t)(0.005)
#define set_R_prime_tozero_in_k2_prime (GlobalData_t)(0.045)
#define set_R_prime_tozero_in_k4 (GlobalData_t)(0.005)
#define set_R_prime_tozero_in_max_sr (GlobalData_t)(2.5)
#define set_R_prime_tozero_in_min_sr (GlobalData_t)(1.)
#define y_init (GlobalData_t)(0.0457562667986602)
#define partial_i_leak_del_Ca_SR (GlobalData_t)(V_leak)
#define partial_i_leak_del_Cai (GlobalData_t)((V_leak*-1.))
#define partial_i_xfer_del_Cai (GlobalData_t)((V_xfer*-1.))
#define partial_i_xfer_del_Cass (GlobalData_t)(V_xfer)



void StewartIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Stewart_Params *p = imp.params();

  // Compute the regional constants
  {
    p->GCaL = 3.98e-5;
    p->GK1 = 0.065;
    p->GKr = 0.0918;
    p->GKs = 0.2352;
    p->GNa = 130.5744;
    p->Gbca = 0.000592;
    p->Gbna = 0.00029;
    p->Gf_K = 0.0234346;
    p->Gf_Na = 0.0145654;
    p->GpCa = 0.1238;
    p->GpK = 0.0146;
    p->Gsus = 0.0227;
    p->Gto = 0.08184;
    p->maxINaCa = 1000.;
    p->maxINaK = 2.724;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void StewartIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Stewart_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void StewartIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stewart_Params *p = imp.params();

  Stewart_state *sv_base = (Stewart_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Stewart_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_SR = Ca_SR_init;
    sv->Cai = Cai_init;
    sv->Cass = Cass_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->R_prime = R_prime_init;
    V = V_init;
    sv->Xr1 = Xr1_init;
    sv->Xr2 = Xr2_init;
    sv->Xs = Xs_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->f2 = f2_init;
    sv->fCass = fCass_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->r = r_init;
    sv->s = s_init;
    sv->y = y_init;
    double E_Ca = ((((0.5*R)*T)/F)*(log((Cao/sv->Cai))));
    double E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    double E_Ks = (((R*T)/F)*(log(((Ko+(P_kna*Nao))/(sv->Ki+(P_kna*sv->Nai))))));
    double E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    double ICal = ((((((((((p->GCaL*sv->d)*sv->f)*sv->f2)*sv->fCass)*4.)*(V-(15.)))*(F*F))/(R*T))*(((0.25*sv->Cass)*(exp((((2.*(V-(15.)))*F)/(R*T)))))-(Cao)))/((exp((((2.*(V-(15.)))*F)/(R*T))))-(1.)));
    double INaCa = ((p->maxINaCa*((((exp((((gamma*V)*F)/(R*T))))*((sv->Nai*sv->Nai)*sv->Nai))*Cao)-(((((exp(((((gamma-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*sv->Cai)*alpha))))/(((((Km_Nai*Km_Nai)*Km_Nai)+((Nao*Nao)*Nao))*(Km_Ca+Cao))*(1.+(K_sat*(exp(((((gamma-(1.))*V)*F)/(R*T))))))));
    double INaK = (((((p->maxINaK*Ko)/(Ko+K_mk))*sv->Nai)/(sv->Nai+K_mNa))/((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+(0.0353*(exp((((-V)*F)/(R*T)))))));
    double IpCa = ((p->GpCa*sv->Cai)/(sv->Cai+K_pCa));
    double a = (1./(1.+(exp(((5.-(V))/17.)))));
    double xK1_inf = (1./(1.+(exp((0.1*(V+75.44))))));
    double IK1 = ((p->GK1*xK1_inf)*((V-(8.))-(E_K)));
    double IKr = ((((p->GKr*(sqrt((Ko/5.4))))*sv->Xr1)*sv->Xr2)*(V-(E_K)));
    double IKs = ((p->GKs*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    double INa = ((((p->GNa*((sv->m*sv->m)*sv->m))*sv->h)*sv->j)*(V-(E_Na)));
    double IbCa = (p->Gbca*(V-(E_Ca)));
    double IbNa = (p->Gbna*(V-(E_Na)));
    double If_K = ((sv->y*p->Gf_K)*(V-(E_K)));
    double If_Na = ((sv->y*p->Gf_Na)*(V-(E_Na)));
    double IpK = ((p->GpK*(V-(E_K)))/(1.+(exp(((25.-(V))/5.98)))));
    double Isus = ((p->Gsus*a)*(V-(E_K)));
    double Ito = (((p->Gto*sv->r)*sv->s)*(V-(E_K)));
    double If = (If_Na+If_K);
    Iion = (-(-1.*(((((((((((((IK1+Ito)+Isus)+IKr)+IKs)+ICal)+INaK)+INa)+IbNa)+INaCa)+IbCa)+IpK)+IpCa)+If)));
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef STEWART_CPU_GENERATED
extern "C" {
void compute_Stewart_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  StewartIonType::IonIfDerived& imp = static_cast<StewartIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Stewart_Params *p  = imp.params();
  Stewart_state *sv_base = (Stewart_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  StewartIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Stewart_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_Ca = ((((0.5*R)*T)/F)*(log((Cao/sv->Cai))));
    GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t E_Ks = (((R*T)/F)*(log(((Ko+(P_kna*Nao))/(sv->Ki+(P_kna*sv->Nai))))));
    GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    GlobalData_t ICal = ((((((((((p->GCaL*sv->d)*sv->f)*sv->f2)*sv->fCass)*4.)*(V-(15.)))*(F*F))/(R*T))*(((0.25*sv->Cass)*(exp((((2.*(V-(15.)))*F)/(R*T)))))-(Cao)))/((exp((((2.*(V-(15.)))*F)/(R*T))))-(1.)));
    GlobalData_t INaCa = ((p->maxINaCa*((((exp((((gamma*V)*F)/(R*T))))*((sv->Nai*sv->Nai)*sv->Nai))*Cao)-(((((exp(((((gamma-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*sv->Cai)*alpha))))/(((((Km_Nai*Km_Nai)*Km_Nai)+((Nao*Nao)*Nao))*(Km_Ca+Cao))*(1.+(K_sat*(exp(((((gamma-(1.))*V)*F)/(R*T))))))));
    GlobalData_t INaK = (((((p->maxINaK*Ko)/(Ko+K_mk))*sv->Nai)/(sv->Nai+K_mNa))/((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+(0.0353*(exp((((-V)*F)/(R*T)))))));
    GlobalData_t IpCa = ((p->GpCa*sv->Cai)/(sv->Cai+K_pCa));
    GlobalData_t a = (1./(1.+(exp(((5.-(V))/17.)))));
    GlobalData_t xK1_inf = (1./(1.+(exp((0.1*(V+75.44))))));
    GlobalData_t IK1 = ((p->GK1*xK1_inf)*((V-(8.))-(E_K)));
    GlobalData_t IKr = ((((p->GKr*(sqrt((Ko/5.4))))*sv->Xr1)*sv->Xr2)*(V-(E_K)));
    GlobalData_t IKs = ((p->GKs*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    GlobalData_t INa = ((((p->GNa*((sv->m*sv->m)*sv->m))*sv->h)*sv->j)*(V-(E_Na)));
    GlobalData_t IbCa = (p->Gbca*(V-(E_Ca)));
    GlobalData_t IbNa = (p->Gbna*(V-(E_Na)));
    GlobalData_t If_K = ((sv->y*p->Gf_K)*(V-(E_K)));
    GlobalData_t If_Na = ((sv->y*p->Gf_Na)*(V-(E_Na)));
    GlobalData_t IpK = ((p->GpK*(V-(E_K)))/(1.+(exp(((25.-(V))/5.98)))));
    GlobalData_t Isus = ((p->Gsus*a)*(V-(E_K)));
    GlobalData_t Ito = (((p->Gto*sv->r)*sv->s)*(V-(E_K)));
    GlobalData_t If = (If_Na+If_K);
    Iion = (-(-1.*(((((((((((((IK1+Ito)+Isus)+IKr)+IKs)+ICal)+INaK)+INa)+IbNa)+INaCa)+IbCa)+IpK)+IpCa)+If)));
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Ki = (((-1.*(((((((IK1+Ito)+If_K)+Isus)+IKr)+IKs)+IpK)-((2.*INaK))))/(V_c*F))*Cm);
    GlobalData_t diff_Nai = (((-1.*((((INa+IbNa)+If_Na)+(3.*INaK))+(3.*INaCa)))/(V_c*F))*Cm);
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t Ca_sr_bufsr = (1./(1.+((Buf_sr*K_buf_sr)/((sv->Ca_SR+K_buf_sr)*(sv->Ca_SR+K_buf_sr)))));
    GlobalData_t Cai_bufc = (1./(1.+((Buf_c*K_buf_c)/((sv->Cai+K_buf_c)*(sv->Cai+K_buf_c)))));
    GlobalData_t Cass_bufss = (1./(1.+((Buf_ss*K_buf_ss)/((sv->Cass+K_buf_ss)*(sv->Cass+K_buf_ss)))));
    GlobalData_t aalpha_d = ((1.4/(1.+(exp(((-35.-(V))/13.)))))+0.25);
    GlobalData_t aalpha_h = ((V<-40.) ? (0.057*(exp(((-(V+80.))/6.8)))) : 0.);
    GlobalData_t aalpha_j = ((V<-40.) ? ((((-25428.*(exp((0.2444*V))))-((6.948e-6*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    GlobalData_t aalpha_m = (1./(1.+(exp(((-60.-(V))/5.)))));
    GlobalData_t aalpha_y = (exp((-2.9-((0.04*V)))));
    GlobalData_t alpha_xr1 = (450./(1.+(exp(((-45.-(V))/10.)))));
    GlobalData_t alpha_xr2 = (3./(1.+(exp(((-60.-(V))/20.)))));
    GlobalData_t alpha_xs = (1400./(sqrt((1.+(exp(((5.-(V))/6.)))))));
    GlobalData_t bbeta_d = (1.4/(1.+(exp(((V+5.)/5.)))));
    GlobalData_t bbeta_h = ((V<-40.) ? ((2.7*(exp((0.079*V))))+(310000.*(exp((0.3485*V))))) : (0.77/(0.13*(1.+(exp(((V+10.66)/-11.1)))))));
    GlobalData_t bbeta_j = ((V<-40.) ? ((0.02424*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))) : ((0.6*(exp((0.057*V))))/(1.+(exp((-0.1*(V+32.)))))));
    GlobalData_t bbeta_m = ((0.1/(1.+(exp(((V+35.)/5.)))))+(0.1/(1.+(exp(((V-(50.))/200.))))));
    GlobalData_t bbeta_y = (exp((3.6+(0.11*V))));
    GlobalData_t beta_xr1 = (6./(1.+(exp(((V+30.)/11.5)))));
    GlobalData_t beta_xr2 = (1.12/(1.+(exp(((V-(60.))/20.)))));
    GlobalData_t beta_xs = (1./(1.+(exp(((V-(35.))/15.)))));
    GlobalData_t d_inf = (1./(1.+(exp(((-8.-(V))/7.5)))));
    GlobalData_t f2_inf = ((0.67/(1.+(exp(((V+35.)/7.)))))+0.33);
    GlobalData_t fCass_inf = ((0.6/(1.+((sv->Cass/0.05)*(sv->Cass/0.05))))+0.4);
    GlobalData_t f_inf = (1./(1.+(exp(((V+20.)/7.)))));
    GlobalData_t gamma_d = (1./(1.+(exp(((50.-(V))/20.)))));
    GlobalData_t h_inf = (1./((1.+(exp(((V+71.55)/7.43))))*(1.+(exp(((V+71.55)/7.43))))));
    GlobalData_t i_leak = (V_leak*(sv->Ca_SR-(sv->Cai)));
    GlobalData_t i_up = (Vmax_up/(1.+((K_up*K_up)/(sv->Cai*sv->Cai))));
    GlobalData_t i_xfer = (V_xfer*(sv->Cass-(sv->Cai)));
    GlobalData_t j_inf = (1./((1.+(exp(((V+71.55)/7.43))))*(1.+(exp(((V+71.55)/7.43))))));
    GlobalData_t kcasr = (max_sr-(((max_sr-(min_sr))/(1.+((EC/sv->Ca_SR)*(EC/sv->Ca_SR))))));
    GlobalData_t m_inf = (1./((1.+(exp(((-56.86-(V))/9.03))))*(1.+(exp(((-56.86-(V))/9.03))))));
    GlobalData_t r_inf = (1./(1.+(exp(((20.-(V))/13.)))));
    GlobalData_t s_inf = (1./(1.+(exp(((V+27.)/13.)))));
    GlobalData_t set_Xr1_tozero_in_diff_Xr1 = ((1./(1.+(exp(((-26.-(V))/7.)))))/((450./(1.+(exp(((-45.-(V))/10.)))))*(6./(1.+(exp(((V+30.)/11.5)))))));
    GlobalData_t set_Xr2_tozero_in_diff_Xr2 = ((1./(1.+(exp(((V+88.)/24.)))))/((3./(1.+(exp(((-60.-(V))/20.)))))*(1.12/(1.+(exp(((V-(60.))/20.)))))));
    GlobalData_t set_Xs_tozero_in_diff_Xs = ((1./(1.+(exp(((-5.-(V))/14.)))))/(((1400./(sqrt((1.+(exp(((5.-(V))/6.)))))))*(1./(1.+(exp(((V-(35.))/15.))))))+80.));
    GlobalData_t tau_f = ((((1102.5*(exp(((-((V+27.)*(V+27.)))/225.))))+(200./(1.+(exp(((13.-(V))/10.))))))+(180./(1.+(exp(((V+30.)/10.))))))+20.);
    GlobalData_t tau_f2 = (((562.*(exp(((-((V+27.)*(V+27.)))/240.))))+(31./(1.+(exp(((25.-(V))/10.))))))+(80./(1.+(exp(((V+30.)/10.))))));
    GlobalData_t tau_fCass = ((80./(1.+((sv->Cass/0.05)*(sv->Cass/0.05))))+2.);
    GlobalData_t tau_r = ((10.45*(exp(((-((V+40.)*(V+40.)))/1800.))))+7.3);
    GlobalData_t tau_s = (((85.*(exp(((-((V+25.)*(V+25.)))/320.))))+(5./(1.+(exp(((V-(40.))/5.))))))+42.);
    GlobalData_t y_inf = (1./(1.+(exp(((V+80.6)/6.8)))));
    GlobalData_t diff_Cai = (Cai_bufc*(((((i_leak-(i_up))*V_sr)/V_c)+i_xfer)-(((((IbCa+IpCa)-((2.*INaCa)))*Cm)/((2.*V_c)*F)))));
    GlobalData_t f2_rush_larsen_B = (exp(((-dt)/tau_f2)));
    GlobalData_t f2_rush_larsen_C = (expm1(((-dt)/tau_f2)));
    GlobalData_t fCass_rush_larsen_B = (exp(((-dt)/tau_fCass)));
    GlobalData_t fCass_rush_larsen_C = (expm1(((-dt)/tau_fCass)));
    GlobalData_t f_rush_larsen_B = (exp(((-dt)/tau_f)));
    GlobalData_t f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    GlobalData_t k1 = (k1_prime/kcasr);
    GlobalData_t k2 = (k2_prime*kcasr);
    GlobalData_t partial_diff_Cai_del_Cai = ((((-((-((Buf_c*K_buf_c)*((sv->Cai+K_buf_c)+(sv->Cai+K_buf_c))))/(((sv->Cai+K_buf_c)*(sv->Cai+K_buf_c))*((sv->Cai+K_buf_c)*(sv->Cai+K_buf_c)))))/((1.+((Buf_c*K_buf_c)/((sv->Cai+K_buf_c)*(sv->Cai+K_buf_c))))*(1.+((Buf_c*K_buf_c)/((sv->Cai+K_buf_c)*(sv->Cai+K_buf_c))))))*(((((i_leak-(i_up))*V_sr)/V_c)+i_xfer)-(((((IbCa+IpCa)-((2.*INaCa)))*Cm)/((2.*V_c)*F)))))+(Cai_bufc*((((V_c*(((V_leak*-1.)-(((-(Vmax_up*((-((K_up*K_up)*(sv->Cai+sv->Cai)))/((sv->Cai*sv->Cai)*(sv->Cai*sv->Cai)))))/((1.+((K_up*K_up)/(sv->Cai*sv->Cai)))*(1.+((K_up*K_up)/(sv->Cai*sv->Cai)))))))*V_sr))/(V_c*V_c))+(V_xfer*-1.))-(((((2.*V_c)*F)*((((p->Gbca*(-((((0.5*R)*T)/F)*(((-Cao)/(sv->Cai*sv->Cai))/(Cao/sv->Cai)))))+((((sv->Cai+K_pCa)*p->GpCa)-((p->GpCa*sv->Cai)))/((sv->Cai+K_pCa)*(sv->Cai+K_pCa))))-((2.*(((((((Km_Nai*Km_Nai)*Km_Nai)+((Nao*Nao)*Nao))*(Km_Ca+Cao))*(1.+(K_sat*(exp(((((gamma-(1.))*V)*F)/(R*T)))))))*(p->maxINaCa*(-(((exp(((((gamma-(1.))*V)*F)/(R*T))))*((Nao*Nao)*Nao))*alpha))))/((((((Km_Nai*Km_Nai)*Km_Nai)+((Nao*Nao)*Nao))*(Km_Ca+Cao))*(1.+(K_sat*(exp(((((gamma-(1.))*V)*F)/(R*T)))))))*(((((Km_Nai*Km_Nai)*Km_Nai)+((Nao*Nao)*Nao))*(Km_Ca+Cao))*(1.+(K_sat*(exp(((((gamma-(1.))*V)*F)/(R*T))))))))))))*Cm))/(((2.*V_c)*F)*((2.*V_c)*F)))))));
    GlobalData_t r_rush_larsen_B = (exp(((-dt)/tau_r)));
    GlobalData_t r_rush_larsen_C = (expm1(((-dt)/tau_r)));
    GlobalData_t s_rush_larsen_B = (exp(((-dt)/tau_s)));
    GlobalData_t s_rush_larsen_C = (expm1(((-dt)/tau_s)));
    GlobalData_t tau_d = ((aalpha_d*bbeta_d)+gamma_d);
    GlobalData_t tau_h = (1./(aalpha_h+bbeta_h));
    GlobalData_t tau_j = (1./(aalpha_j+bbeta_j));
    GlobalData_t tau_m = (aalpha_m*bbeta_m);
    GlobalData_t tau_xr1 = (alpha_xr1*beta_xr1);
    GlobalData_t tau_xr2 = (alpha_xr2*beta_xr2);
    GlobalData_t tau_xs = ((alpha_xs*beta_xs)+80.);
    GlobalData_t tau_y = (4000./(aalpha_y+bbeta_y));
    GlobalData_t Cai_rush_larsen_A = ((expm1((partial_diff_Cai_del_Cai*dt)))*(diff_Cai/partial_diff_Cai_del_Cai));
    GlobalData_t O = (((k1*(sv->Cass*sv->Cass))*sv->R_prime)/(k3+(k1*(sv->Cass*sv->Cass))));
    GlobalData_t d_rush_larsen_B = (exp(((-dt)/tau_d)));
    GlobalData_t d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    GlobalData_t f2_rush_larsen_A = ((-f2_inf)*f2_rush_larsen_C);
    GlobalData_t fCass_rush_larsen_A = ((-fCass_inf)*fCass_rush_larsen_C);
    GlobalData_t f_rush_larsen_A = ((-f_inf)*f_rush_larsen_C);
    GlobalData_t h_rush_larsen_B = (exp(((-dt)/tau_h)));
    GlobalData_t h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    GlobalData_t j_rush_larsen_B = (exp(((-dt)/tau_j)));
    GlobalData_t j_rush_larsen_C = (expm1(((-dt)/tau_j)));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t partial_diff_R_prime_del_R_prime = (((-k2)*sv->Cass)+(k4*-1.));
    GlobalData_t partial_diff_Xr1_del_Xr1 = ((tau_xr1*-1.)/(tau_xr1*tau_xr1));
    GlobalData_t partial_diff_Xr2_del_Xr2 = ((tau_xr2*-1.)/(tau_xr2*tau_xr2));
    GlobalData_t partial_diff_Xs_del_Xs = ((tau_xs*-1.)/(tau_xs*tau_xs));
    GlobalData_t r_rush_larsen_A = ((-r_inf)*r_rush_larsen_C);
    GlobalData_t s_rush_larsen_A = ((-s_inf)*s_rush_larsen_C);
    GlobalData_t y_rush_larsen_B = (exp(((-dt)/tau_y)));
    GlobalData_t y_rush_larsen_C = (expm1(((-dt)/tau_y)));
    GlobalData_t R_prime_rush_larsen_A = ((set_R_prime_tozero_in_diff_R_prime/partial_diff_R_prime_del_R_prime)*(expm1((dt*partial_diff_R_prime_del_R_prime))));
    GlobalData_t R_prime_rush_larsen_B = (exp((dt*partial_diff_R_prime_del_R_prime)));
    GlobalData_t Xr1_rush_larsen_A = ((set_Xr1_tozero_in_diff_Xr1/partial_diff_Xr1_del_Xr1)*(expm1((dt*partial_diff_Xr1_del_Xr1))));
    GlobalData_t Xr1_rush_larsen_B = (exp((dt*partial_diff_Xr1_del_Xr1)));
    GlobalData_t Xr2_rush_larsen_A = ((set_Xr2_tozero_in_diff_Xr2/partial_diff_Xr2_del_Xr2)*(expm1((dt*partial_diff_Xr2_del_Xr2))));
    GlobalData_t Xr2_rush_larsen_B = (exp((dt*partial_diff_Xr2_del_Xr2)));
    GlobalData_t Xs_rush_larsen_A = ((set_Xs_tozero_in_diff_Xs/partial_diff_Xs_del_Xs)*(expm1((dt*partial_diff_Xs_del_Xs))));
    GlobalData_t Xs_rush_larsen_B = (exp((dt*partial_diff_Xs_del_Xs)));
    GlobalData_t d_rush_larsen_A = ((-d_inf)*d_rush_larsen_C);
    GlobalData_t h_rush_larsen_A = ((-h_inf)*h_rush_larsen_C);
    GlobalData_t i_rel = ((V_rel*O)*(sv->Ca_SR-(sv->Cass)));
    GlobalData_t j_rush_larsen_A = ((-j_inf)*j_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_inf)*m_rush_larsen_C);
    GlobalData_t y_rush_larsen_A = ((-y_inf)*y_rush_larsen_C);
    GlobalData_t diff_Ca_SR = (Ca_sr_bufsr*(i_up-((i_rel+i_leak))));
    GlobalData_t diff_Cass = (Cass_bufss*(((((-1.*ICal)*Cm)/((2.*V_ss)*F))+((i_rel*V_sr)/V_ss))-(((i_xfer*V_c)/V_ss))));
    GlobalData_t partial_diff_Ca_SR_del_Ca_SR = ((((-((-((Buf_sr*K_buf_sr)*((sv->Ca_SR+K_buf_sr)+(sv->Ca_SR+K_buf_sr))))/(((sv->Ca_SR+K_buf_sr)*(sv->Ca_SR+K_buf_sr))*((sv->Ca_SR+K_buf_sr)*(sv->Ca_SR+K_buf_sr)))))/((1.+((Buf_sr*K_buf_sr)/((sv->Ca_SR+K_buf_sr)*(sv->Ca_SR+K_buf_sr))))*(1.+((Buf_sr*K_buf_sr)/((sv->Ca_SR+K_buf_sr)*(sv->Ca_SR+K_buf_sr))))))*(i_up-((i_rel+i_leak))))+(Ca_sr_bufsr*(-((((V_rel*((((k3+(k1*(sv->Cass*sv->Cass)))*((((-(k1_prime*(-((-((max_sr-(min_sr))*((((-EC)/(sv->Ca_SR*sv->Ca_SR))*(EC/sv->Ca_SR))+((EC/sv->Ca_SR)*((-EC)/(sv->Ca_SR*sv->Ca_SR))))))/((1.+((EC/sv->Ca_SR)*(EC/sv->Ca_SR)))*(1.+((EC/sv->Ca_SR)*(EC/sv->Ca_SR))))))))/(kcasr*kcasr))*(sv->Cass*sv->Cass))*sv->R_prime))-((((k1*(sv->Cass*sv->Cass))*sv->R_prime)*(((-(k1_prime*(-((-((max_sr-(min_sr))*((((-EC)/(sv->Ca_SR*sv->Ca_SR))*(EC/sv->Ca_SR))+((EC/sv->Ca_SR)*((-EC)/(sv->Ca_SR*sv->Ca_SR))))))/((1.+((EC/sv->Ca_SR)*(EC/sv->Ca_SR)))*(1.+((EC/sv->Ca_SR)*(EC/sv->Ca_SR))))))))/(kcasr*kcasr))*(sv->Cass*sv->Cass)))))/((k3+(k1*(sv->Cass*sv->Cass)))*(k3+(k1*(sv->Cass*sv->Cass))))))*(sv->Ca_SR-(sv->Cass)))+(V_rel*O))+V_leak))));
    GlobalData_t partial_diff_Cass_del_Cass = ((((-((-((Buf_ss*K_buf_ss)*((sv->Cass+K_buf_ss)+(sv->Cass+K_buf_ss))))/(((sv->Cass+K_buf_ss)*(sv->Cass+K_buf_ss))*((sv->Cass+K_buf_ss)*(sv->Cass+K_buf_ss)))))/((1.+((Buf_ss*K_buf_ss)/((sv->Cass+K_buf_ss)*(sv->Cass+K_buf_ss))))*(1.+((Buf_ss*K_buf_ss)/((sv->Cass+K_buf_ss)*(sv->Cass+K_buf_ss))))))*(((((-1.*ICal)*Cm)/((2.*V_ss)*F))+((i_rel*V_sr)/V_ss))-(((i_xfer*V_c)/V_ss))))+(Cass_bufss*((((((2.*V_ss)*F)*((-1.*((((exp((((2.*(V-(15.)))*F)/(R*T))))-(1.))*(((((((((p->GCaL*sv->d)*sv->f)*sv->f2)*sv->fCass)*4.)*(V-(15.)))*(F*F))/(R*T))*(0.25*(exp((((2.*(V-(15.)))*F)/(R*T)))))))/(((exp((((2.*(V-(15.)))*F)/(R*T))))-(1.))*((exp((((2.*(V-(15.)))*F)/(R*T))))-(1.)))))*Cm))/(((2.*V_ss)*F)*((2.*V_ss)*F)))+((V_ss*((((V_rel*((((k3+(k1*(sv->Cass*sv->Cass)))*((k1*(sv->Cass+sv->Cass))*sv->R_prime))-((((k1*(sv->Cass*sv->Cass))*sv->R_prime)*(k1*(sv->Cass+sv->Cass)))))/((k3+(k1*(sv->Cass*sv->Cass)))*(k3+(k1*(sv->Cass*sv->Cass))))))*(sv->Ca_SR-(sv->Cass)))+((V_rel*O)*-1.))*V_sr))/(V_ss*V_ss)))-(((V_ss*(V_xfer*V_c))/(V_ss*V_ss))))));
    GlobalData_t Ca_SR_rush_larsen_A = ((expm1((partial_diff_Ca_SR_del_Ca_SR*dt)))*(diff_Ca_SR/partial_diff_Ca_SR_del_Ca_SR));
    GlobalData_t Cass_rush_larsen_A = ((expm1((partial_diff_Cass_del_Cass*dt)))*(diff_Cass/partial_diff_Cass_del_Cass));
    GlobalData_t Ca_SR_new = Ca_SR_rush_larsen_A+Ca_SR_rush_larsen_B*sv->Ca_SR;
    GlobalData_t Cai_new = Cai_rush_larsen_A+Cai_rush_larsen_B*sv->Cai;
    GlobalData_t Cass_new = Cass_rush_larsen_A+Cass_rush_larsen_B*sv->Cass;
    GlobalData_t R_prime_new = R_prime_rush_larsen_A+R_prime_rush_larsen_B*sv->R_prime;
    GlobalData_t Xr1_new = Xr1_rush_larsen_A+Xr1_rush_larsen_B*sv->Xr1;
    GlobalData_t Xr2_new = Xr2_rush_larsen_A+Xr2_rush_larsen_B*sv->Xr2;
    GlobalData_t Xs_new = Xs_rush_larsen_A+Xs_rush_larsen_B*sv->Xs;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t f2_new = f2_rush_larsen_A+f2_rush_larsen_B*sv->f2;
    GlobalData_t fCass_new = fCass_rush_larsen_A+fCass_rush_larsen_B*sv->fCass;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t s_new = s_rush_larsen_A+s_rush_larsen_B*sv->s;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_SR = Ca_SR_new;
    sv->Cai = Cai_new;
    sv->Cass = Cass_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->R_prime = R_prime_new;
    sv->Xr1 = Xr1_new;
    sv->Xr2 = Xr2_new;
    sv->Xs = Xs_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->f2 = f2_new;
    sv->fCass = fCass_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->r = r_new;
    sv->s = s_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // STEWART_CPU_GENERATED

bool StewartIonType::has_trace() const {
    return false;
}

void StewartIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* StewartIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void StewartIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        