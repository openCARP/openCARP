#!/bin/bash
#
# use this script if you do not use cmake. It is a wrapper for
# make_dynamic_model.py.
#
# We try to be smart but if that is not the case,
# just hard code your CARP_DIR and CC


#this function is needed since OSX does not implement "readlink -f"
function actualpath {
    echo $(python3 -c 'import os;print(os.path.realpath("'$1'"))') 
}

Help()
{
    echo "Convert .model to C++ code and compile it to a dynamic library"
    echo
    echo "Syntax: $0 yourModel [-d]"
    echo 
    echo "positional arguments:"
    echo "yourModel     path to yourModel.model"
    echo
    echo "optional flags:"
    echo "-d     build library in debug mode (default: Release)"
    echo "       can be set to Debug also by your openCARP build system"
    echo "       (CMake or my_switches.def)"
}

CUR_DIR=$(dirname $(actualpath ${BASH_SOURCE}))
CARP_DIR=$(actualpath ${CUR_DIR}/../../..)
DEBUG=0


INCS="-I${CARP_DIR}/physics/limpet/src \
-I${CARP_DIR}/_build/physics/limpet \
-I${CARP_DIR}/_build/simulator \
-I${CARP_DIR}/numerics \
-I${CARP_DIR}/simulator \
-I${CARP_DIR}/fem \
-I${CARP_DIR}/fem/slimfem/src \
-I${CARP_DIR}/param/include \
-I${PETSC_DIR}/include \
-I${PETSC_DIR}/${PETSC_ARCH}/include \
-I${CARP_DIR}/../../lib/petsc/include \
-I${CARP_DIR}/../../lib/openmpi/include"

if [[ $# -eq 0 ]] ; then
    Help;
    exit 0;
fi

while getopts "khd" option
do
        case "${option}"
        in
            k) keep="--keep";;
            h) Help 1>&2
               exit 1;;
            d) DEBUG=1;;
            \?) exit 1;;
        esac
done
shift $((${OPTIND}-1))

#try to determine the compiler from environment
if [[ -e ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables | awk '{print $3;}')
elif [[ -e ${PETSC_DIR}/lib/petsc/conf/petscvariables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/lib/petsc/conf/petscvariables | awk '{print $3;}')
elif [[ -e ${PETSC_DIR}/conf/variables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/conf/variables | awk '{print $3;}')
else #no petsc configuration found, try naive gcc
    CC=$(which gcc)
fi

if [[ -z ${CC} ]]
then
    echo Cannot find C compiler --- bailing
    exit 1
fi

if ${CC} --version 2>&1 >/dev/null; then
    compiler=$(${CC} --version 2>&1)
else
    compiler=$(${CC} -V 2>&1)
fi

#set compiler specific flags
if [[ $compiler =~ "GCC" ||  $compiler =~ "gcc" || $compiler =~ "clang" ]]; then
    cc="GCC"
    DYN_LIB_OPTS="-fPIC -shared -std=c++14 -lm"
    if [[ $(uname -s) = "Darwin" ]]
    then
        DYN_LIB_OPTS+=" -Wl,-undefined,dynamic_lookup"
    fi    
elif [[ $compiler =~ "ICC" ]]; then
    cc="ICC"
    DYN_LIB_OPTS="-fPIC -shared -std=c++14"
elif [[ $compiler =~ "pgc" ]]; then
    cc="PGI"
    DYN_LIB_OPTS="-fPIC -shared -std=c++14"
elif [[ $compiler =~ "Cray" ]]; then
    cc="CRAYCC"
    DYN_LIB_OPTS="-fPIC -shared -h c++14"
fi

#try to determine if we are debugging
if [ $DEBUG -eq 0 ]; then
    if [ -f ${CARP_DIR}/my_switches.def ]; then
        DEBUG=`sed 's/#.*//' ${CARP_DIR}/my_switches.def | grep -c 'DEBUG.*=.*[1-9]'`
    fi
elif [ -f ${CARP_DIR}/_build/CMakeCache.txt ]; then
    DEBUG=$(grep -c grep -c 'CMAKE_BUILD_TYPE:STRING=Debug' ${CARP_DIR}/_build/CMakeCache.txt)
fi

if [ $DEBUG -ne 0 ]; then
    echo "Compiling in DEBUG mode"
    DYN_LIB_OPTS="${DYN_LIB_OPTS} -g -O0 "
fi

if [ -f ${CARP_DIR}/my_switches.def ]; then
    #try to determine if we are using CVODE
    CVODE=$(sed 's/#.*//' ${CARP_DIR}/my_switches.def | grep -c 'CVODE\s*=\s*[1-9]')
    if [ $CVODE -ge 1 ] ; then
        echo "using CVODE"
        INCS+=" -DUSE_CVODE"
    else
        echo "not using CVODE"
    fi
elif [ -f ${CARP_DIR}/_build/CMakeCache.txt ]; then
    CVODE=$(grep -c 'SUNDIALS_DIR:PATH' ${CARP_DIR}/_build/CMakeCache.txt)
    CVODE_PATH=$(grep 'SUNDIALS_DIR:PATH' ${CARP_DIR}/_build/CMakeCache.txt | sed 's/SUNDIALS_DIR:PATH=//' | sed 's|/lib/cmake/sundials||')
    if [ $CVODE -ge 1 ] ; then
        echo "using CVODE"
        INCS+=" -DUSE_CVODE -I${CVODE_PATH}/include"
    else
        echo "not using CVODE"
    fi
else
    echo "Cannot determine build info, not using CVODE. Use _build dir when using CMake to make it work."
fi



DYN_PREFIX=''

# model name can be with or without .model extension. here we make the input consisitent
MODEL_NAME=${1}
if [[ ! $MODEL_NAME == *".model"* ]]; then
  echo "appending .model to input model name.."
  MODEL_NAME="${1}.model"
  echo "using model name: "$MODEL_NAME
fi

# first call limpet_fe.py
echo ${CARP_DIR}/physics/limpet/src/python/limpet_fe.py ${MODEL_NAME} ${CARP_DIR}/physics/limpet/models/imp_list.txt "$(dirname ${MODEL_NAME})"
if ${CARP_DIR}/physics/limpet/src/python/limpet_fe.py ${MODEL_NAME} ${CARP_DIR}/physics/limpet/models/imp_list.txt "$(dirname ${MODEL_NAME})"; then
    echo "limpet_fe.py succeeded."
else
  echo "limpet_fe.py failed. Exiting..."
  exit 1;
fi

# then we call make_dynamic_model.py
echo ${CARP_DIR}/physics/limpet/src/python/make_dynamic_model.py --compiler=${CC} --cflags="${DYN_LIB_OPTS} ${INCS}" --dynamic_prefix=${DYN_PREFIX} ${1} ${keep}
${CARP_DIR}/physics/limpet/src/python/make_dynamic_model.py --compiler=${CC} --cflags="${DYN_LIB_OPTS} ${INCS}" --dynamic_prefix=${DYN_PREFIX} $1 ${keep}

