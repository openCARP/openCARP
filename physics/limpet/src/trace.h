// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef TRACE_H
#define TRACE_H

namespace limpet {

//! manage input, output, resampling of traces
struct trace {
    int          N;          //!< number of samples
    double      *t;          //!< time vector
    float       *s;          //!< samples
    double       dur;        //!< duration
    double       dt;         //!< sampling interval if equidistant samples
    bool         eqdist;     //!< sampling intervals are equidistant
};

// trailing underscore added to avoid clash with bidomain.h
// Stimulation.c needs to be modified to make use of traces as used in here
typedef enum ip_method   { _LINEAR_IP, _NEAREST_IP } IpMeth_t;

  int  read_trace(trace *tr, const char *name );
  void free_trace(trace *tr);
  void resample_trace(trace *tr, double dt );
  void interp1(const double *x, const float *y, int N, double *xi, float *yi, int NI, double dxi, IpMeth_t);
  double trace_duration(trace *tr, const char* f);

}  // namespace limpet

#endif

