// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file MULTI_ION_IF.cc
* @brief Define multiple ionic models to be used in different regions
* @author Edward Vigmond
* @version 
* @date 2019-10-25
*/

/** \file
 * Define multiple ionic models to be used in different regions
 *
 * To use multiple ionic interfaces:
 *   -# load any IMP modules
 *   -# define consecutive integers starting from 0 to
 *      represent each region which will have a different
 *      ionic model and plugin combination
 *   -# specify the number of regions (MULTI_IF::N_IIF)
 *   -# assign the ionic model to be used in each region (MULTI_IF::IIF_IDs)
 *   -# specify the plugins for each region Or'ed together (MULTI_IF::IIF_PLUGS)
 *   -# For each node, specify the region it is in (MULTI_IF::IIF_Mask)
 *   -# If using PETSc, allocate the transmembrane voltage vector
 *      (#RVector) and  assign it to MULTI_IF:gdata[Vm]. Other vectors
 *      can be manually assigned or will be done automatically
 *   -# Initialize the MULTI_IF structure with initialize_MIIF(MULTI_IF*)
 *   -# Adjust any model specific parameters. Use ::tune_IMPs
 *   -# Initialize the "current" computation with
 *      initialize_currents()
 *   -# set transmembrane stimulation currents with set_tm_simulus()
 *   -# Now you can compute "currents" with compute_ionic_current()
 *   -# clean up with free_MIIF()
 *
 * It is possible to have multiple multiple ionic interfaces. However, all imp modules needed to be loaded before
 * any MIIFs are initialized
 */

// TODO remove the "get_sv_address()" and "get_sv_size()" calls by moving
// the functions that use them to the IonIf class

#include "MULTI_ION_IF.h"
#include "limpet_types.h"
#include <stdio.h>
#ifdef HAS_CUDA_MODEL
#include <cuda_runtime.h>
#endif
#if defined HAS_ROCM_MODEL && defined __HIP__
#include <hip/hip_runtime.h>
#endif

#include "SF_init.h" // for SF::init_xxx()
#include "petsc_utils.h" // TODO for EXIT

namespace limpet {

using ::opencarp::base_timer;
using ::opencarp::dupstr;
using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;
using ::opencarp::get_global;
using ::opencarp::get_rank;
using ::opencarp::get_size;
using ::opencarp::get_time;
using ::opencarp::log_msg;
using ::opencarp::mesh_t;
using ::opencarp::num_msh;
using ::opencarp::Salt_list;
using ::opencarp::sf_mesh;
using ::opencarp::sf_vec;
using ::opencarp::timing;
using ::opencarp::unset_msh;
#ifdef USE_FMEM_WRAPPER
using ::opencarp::fmemopen_;
#endif

/*
   PROTOTYPES
 */
void    initialize_params_MIIF(MULTI_IF *pMIIF);
void    initialize_ionic_IF(MULTI_IF *pMIIF);
void    CreateIIFLocalNodeLsts(MULTI_IF *pMIIF);
void    CreateIIFNodeLsts_(int, char *, int **, int ***, int);
void    alloc_MIIF(MULTI_IF *pMIIF);
void    initializeIMPData(MULTI_IF *pMIIF);
void    freeIMPData(MULTI_IF *pMIIF);
void    allocate_shared_data(MULTI_IF *);
int     getGlobalNodalIndex(IonIfBase& pIF, int relIdx);
void    CreateIIFGlobalNodeLsts(MULTI_IF *pMIIF);
bool    isIMPdata(const char *);

static MULTI_IF *gMIIF_Error_Recovery;
static int current_IIF_index;
static float current_time = 0;
static float start_time = 0;

static void prepare_error_recovery(MULTI_IF *MIIF, int IIF_index, float time) {
  gMIIF_Error_Recovery = MIIF;
  current_IIF_index    = IIF_index;
  current_time         = start_time+time;
}

int current_global_node(int local_node) {
  return gMIIF_Error_Recovery->NodeLists[current_IIF_index][local_node];
}

float current_global_time() {
  return current_time;
}

static int g_print_bounds_exceeded = 1;

int should_print_bounds_exceeded_messages() {
  return g_print_bounds_exceeded;
}

int set_print_bounds_exceeded_messages(int newval) {
  int oldval = g_print_bounds_exceeded;

  g_print_bounds_exceeded = newval;
  return oldval;
}

/** map the node order to the ionic model node order
 */
void CreateIIFLocalNodeLsts(MULTI_IF *pMIIF) {
  CreateIIFNodeLsts_(pMIIF->N_IIF, pMIIF->IIFmask, &pMIIF->N_Nodes,
                     &pMIIF->NodeLists, pMIIF->numNode);
}

/** map the node order to the ionic model node order
 *
 *  \param        N_IIF      Number of IIFs
 *  \param        IIF_Mask
 *  \param[out]   N_Nodes    Number of nodes per IIF
 *  \param[out]   NodeLists  List of node number for each IIF
 *  \param low    lower      bound of partition
 *  \param high   higher     bound of partition
 *
 *  \post N_Nodes and NodeLists allocated and filled in.
 */
void CreateIIFNodeLsts_(int N_IIF, IIF_Mask_t *IIF_Mask, int **N_Nodes,
                        int ***NodeLists, int numNode)
{
  /*  create node lists for each IIF
      NodeNum: store number of nodes for each IIF
      NodeLst: store node indices of each IIF
   */
  int *NodeNum  = static_cast<int *>(calloc(N_IIF, sizeof(int)));
  int **NodeLst = static_cast<int **>(calloc(N_IIF, sizeof(int *)));

  // determine number of nodes of each IIF
  for (int i = 0; i < numNode; i++)
    NodeNum[static_cast<int>(IIF_Mask[i])]++;

  /* now we know the number of nodes per IIF and store the indices of
     nodes belonging to a particular IIF into a list.
   */
  for (int i = 0; i < N_IIF; i++) {
    if (NodeNum[i] > 0)
      NodeLst[i] = static_cast<int *>(malloc(sizeof(int *) * NodeNum[i]));
    else
      NodeLst[i] = NULL;
  }

  // just store indices relative to low
  for (int j = 0; j < N_IIF; j++) {
    int hcount = 0;
    for (int i = 0; i < numNode; i++)
      if (IIF_Mask[i] == j)
        NodeLst[j][hcount++] = i;
  }

  for(int j=0; j<N_IIF; j++)
    std::sort(NodeLst[j], NodeLst[j]+NodeNum[j]);

  *N_Nodes   = NodeNum;
  *NodeLists = NodeLst;

}  // CreateIIFNodeLsts_


/** helper function to retrieve global nodal index number from within an imp
 *
 * \param [in] pIF    pointer to imp
 * \param [in] rIdx   relative index of cell within imp
 *
 * \return global index of cell
 */
int getGlobalNodalIndex(IonIfBase& pIF, int rIdx) {
  // get pointer to parent MIIF
  MULTI_IF *pMIIF = (MULTI_IF *) pIF.parent(); // TODO check?!?!?!?!?!

  if (pIF.get_type().is_plugin())
    pMIIF = (MULTI_IF *) pIF.parent()->parent();

  return pMIIF->NodeLists[pIF.miifIdx][rIdx];
}

/** allocate memory for MIIF structure
 *
 * \param pMIIF  MIIF
 *
 * \post IMPs are usable
 */
void alloc_MIIF(MULTI_IF *pMIIF) {
  int N_IIF = pMIIF->N_IIF;

  pMIIF->contiguous = static_cast<bool *>(calloc(N_IIF, sizeof(bool)));
#if 0
  pMIIF->ldata = (GlobalData_t***) build_matrix_ns<GlobalData_t>(N_IIF, NUM_IMP_DATA_TYPES, sizeof(GlobalData_t **), Target::CPU);
#else
  pMIIF->ldata = allocate_on_target<GlobalData_t**>(Target::CPU, N_IIF);
  for (std::size_t i = 0; i < pMIIF->N_IIF; i++) {
    pMIIF->ldata[i] = allocate_on_target<GlobalData_t*>(pMIIF->iontypes[i].get().select_target(pMIIF->targets[i]), NUM_IMP_DATA_TYPES);
  }
#endif

  for (int i = 0; i < pMIIF->N_IIF; i++) {
    // Create an IonIf object on the chosen target
    pMIIF->IIF.push_back(pMIIF->iontypes[i].get().make_ion_if(pMIIF->targets[i], pMIIF->N_Nodes[i], pMIIF->plugtypes[i]));
    // this is questionable. A IIF does not have a parent, parent should be NULL
    // It might make sense in some situtation to be able to refer to the MIIF
    // structure, but pIF->parent is of type IonIf and not MULTI_IF!!!!
    pMIIF->IIF[i]->set_parent((IonIfBase *)pMIIF);

    // set unique miifIdx for imp and plugins to enable refering back
    // to global entities such as global node index from within imp
    pMIIF->IIF[i]->for_each([&](IonIfBase& IF) { IF.miifIdx = i; });
  }
}  // alloc_MIIF

/** initialize the parameters of the ionic models with default values
 * \param pMIIF  MIIF
 *
 * */
void initialize_params_MIIF(MULTI_IF *pMIIF) {
  for (auto& imp : pMIIF->IIF) {
    imp->initialize_params();
  }
}

/** initlaize the ionic interfaces
 * \param pMIIF  MIIF
 */
void initialize_ionic_IF(MULTI_IF *pMIIF) {
  pMIIF->getRealData();         // needed for initializing Vm, etc.
  for (int i = 0; i < pMIIF->IIF.size(); i++)
    pMIIF->IIF[i]->initialize(pMIIF->dt, pMIIF->ldata[i]);

  pMIIF->releaseRealDataDuringInit();
}

/** Copy both reqqed and modded data back into the global vectors.
 *
 * \param m  MIIF
 *
 */
void MULTI_IF::releaseRealDataDuringInit() {
  // we need to copy all global data which has been set, even if it is not
  // listed as moddat. This is the case with Vm which may not be modified by
  // LIMPET but is initialized by LIMPET
  unsigned int *moddat = static_cast<unsigned int *>(calloc(this->N_IIF, sizeof(unsigned int)));

  for (int i = 0; i < this->N_IIF; i++) {
    moddat[i]        = this->IIF[i]->get_moddat();
    this->IIF[i]->set_moddat(moddat[i] | this->IIF[i]->get_reqdat());  // add in the reqdat
  }
  this->releaseRealData();
  for (int i = 0; i < this->N_IIF; i++) {
    this->IIF[i]->set_moddat(moddat[i]);  // remove the reqdat
  }
  free(moddat);
}

/** create lists and allocate memory for IMPs
 *
 * \param MIIF multi yyion interface
 */
void MULTI_IF::initialize_MIIF()
{
  CreateIIFLocalNodeLsts(this);
  alloc_MIIF(this);
  initialize_params_MIIF(this);

  memset(&svd, 0, sizeof(SV_DUMP) );
  svd.active = 0;
  svd.intv   = 1.0;         // 1. ms by default
}

#define FILENAME_BUF 1024

/** compare 2 integers for bsearch o qsort */
int int_cmp(const void *a, const void *b) {
  return *(int *)a - *(int *)b;
}

/** Usage for the following three functions:
 *    -# open_trace();
 *    -# while(running){ if (timer_triggered) dump_trace(time); }
 *    -# close_trace();
 *
 * By default, each model has a NULL pointer for their trace() function.
 * Those models that implement a trace function need only redefine this
 * NULL pointer in their init_parameters function.
 *
 * Each trace function invocation should print a list of whatever
 * variables it wants delimited by tabs.  The dumping routine will
 * take care of inserting extra tabs between models and plugins, and
 * will take responsibilty for printing the trailing new line on each
 * line.
 *
 * \note This is not meant for many many nodes.
 */

/**
* @brief Set up ionic model traces at some global node numbers.
*
* @param MIIF           The MIIF state struct.
* @param n_traceNodes   number of nodes in the following array
* @param traceNodes     global node numbers of the nodes we want to trace.
* @param label          output node number, if NULL, use traceNodes
* @param imesh          The intracellular mesh, may be NULL if called from bench.
*/
void open_trace(MULTI_IF *MIIF, int n_traceNodes, int *traceNodes, int *label, opencarp::sf_mesh* imesh)
{
  if (!n_traceNodes) return;

  if (n_traceNodes > 1000)
    log_msg(0, 4, 0, "%s warning: %d trace nodes may impact performance", __func__, n_traceNodes);

  MIIF->trace_info                      = (Trace_Info *)calloc(n_traceNodes+1, sizeof(Trace_Info));
  MIIF->trace_info[n_traceNodes].region = -1;  // end of list marker

  // bijective index mapping between set A (local petsc indexing) and set B (local nodal indexing)
  SF::index_mapping<mesh_int_t> petsc2nod;
  if(imesh)
    SF::local_petsc_to_nodal_mapping(*imesh, petsc2nod);

  for (int iTrace = 0; iTrace < n_traceNodes; iTrace++) {
    mesh_int_t lnode = imesh ? imesh->pl.localize(traceNodes[iTrace]) : traceNodes[iTrace];

    // here we check if lnode is in set B (i.e. local nodal indexing)
    if(imesh) {
      // here we check if lnode is in set B (i.e. local nodal indexing)
      if(petsc2nod.in_b(lnode) == false) continue;

      // lnode is local nodal, we map it to local petsc
      lnode = petsc2nod.backward_map(lnode);
    }

    for (int iRegion = 0; iRegion < MIIF->N_IIF; iRegion++)
    {
      auto target = static_cast<int *>(bsearch(&lnode, MIIF->NodeLists[iRegion],
                                   MIIF->N_Nodes[iRegion], sizeof(int), int_cmp));

      if (target != NULL ) {
        int idx = target - MIIF->NodeLists[iRegion];
        Trace_Info *trace_info = MIIF->trace_info+iTrace;
        trace_info->found    = true;
        trace_info->region   = iRegion;
        trace_info->node_idx = idx;
      }
    }
  }

  for (int iTrace = 0; iTrace < n_traceNodes; iTrace++) {
    if(get_global(int(MIIF->trace_info[iTrace].found), MPI_SUM) == 0) {
      MIIF->trace_info[iTrace].ignored = true;
      log_msg(0,4,0, "trace node %d not found", traceNodes[iTrace]);
      continue;
    }

    char traceName[FILENAME_BUF];
    snprintf(traceName, sizeof traceName, "Trace_%d.dat", label ? label[iTrace] : traceNodes[iTrace]);
    MIIF->trace_info[iTrace].file = f_open(traceName, "w");
  }
}  // open_trace

/** dump the traces from a node
 *
 * \param MIIF the ionic region
 * \param time time of dump
 *
 * We need the trace output in a buffer so we can pass it to the root for output.
 * The trace function of the IMPs requires a file stream for the fprintf
 * statements and on the GPU, there is a print statement which really copies the data
 * to the host for printing on stdout which we redirect to our desired file stream.
 * We use fmemopen() to associate a file stream with memory and trick everyone into
 * writing into the buffer.
 *
 * \note A maximum line length of 8196 bytes is supported
 */
void dump_trace(MULTI_IF *MIIF, limpet::Real time) {
  if (MIIF->trace_info == NULL)
    return;

//  struct exception_type e;

#define MAX_TRACE_LINE_LEN 8196

  char trace_buf[MAX_TRACE_LINE_LEN] = {0};

  std::vector<IonIfBase*>& IIF = MIIF->IIF;
  FILE   *fs;

  for (int iTrace = 0; MIIF->trace_info[iTrace].region >= 0; iTrace++) {
    Trace_Info *ctrace = MIIF->trace_info+iTrace;

    if (ctrace->ignored)
      continue;

    if (ctrace->found) {
      fs = fmemopen_(trace_buf, MAX_TRACE_LINE_LEN, "w");
      fprintf(fs, "%4.10f\t", time);
      if (IIF[ctrace->region]->get_type().has_trace()) {
        IIF[ctrace->region]->get_type().trace(*IIF[ctrace->region], ctrace->node_idx,
                                        fs, MIIF->ldata[ctrace->region]);
      }
      for (auto& plugin : IIF[ctrace->region]->plugins()) {
        if (plugin->get_type().has_trace()) {
          fprintf(fs, "\t");
          plugin->get_type().trace(
            *plugin, ctrace->node_idx,
            fs, MIIF->ldata[ctrace->region]);
        }
      }
      fprintf(fs, "\n");
      fclose(fs);

      fprintf(ctrace->file->fd, "%s", trace_buf);
    }
    fflush(ctrace->file->fd);
  }
}  // dump_trace

void close_trace(MULTI_IF *MIIF) {
  if (!MIIF->trace_info) return;

  for (int iTrace = 0; MIIF->trace_info[iTrace].region >= 0; iTrace++)
    f_close(MIIF->trace_info[iTrace].file);

  free(MIIF->trace_info);
  MIIF->trace_info = NULL;
}

/** initialize the computation functions
 *
 * \param MIIF   pointer to MULTI_IF structure
 * \param dt     time step (milliseconds)
 * \param subDt  number of sub time steps per global time step
 */
void MULTI_IF::initialize_currents(double idt, int subDt) {
  numSubDt = subDt;
  dt       = idt/this->numSubDt;

  allocate_shared_data(this);
  initializeIMPData(this);
  initialize_ionic_IF(this);
}

/** dump LUTs of all IIFs
 *
 * \param MIIF   pointer to MULTI_IF structure
 * \param zipped toggle between zipped||unzipped dump
 *
 */
void MULTI_IF::dump_luts_MIIF(bool zipped) {
  // if one of the IFs in MIIF does not live in partion 0
  // we won't get a LUT dumped.
  if (get_rank()) return;

  std::vector<IonIfBase*>& pIF = this->IIF;
  for (auto& IF : pIF) {
    int ndmps = IF->dump_luts(zipped);
    if (ndmps < IF->tables().size()) {
      log_msg(logger, 4, 0, "LUT dump error %s: only %d out of %d LUTs dumped.\n",
              IF->get_type().get_name().c_str(), ndmps, IF->tables().size());
    }
    for (auto& plugin : IF->plugins()) {
      ndmps = plugin->dump_luts(zipped);
      if (ndmps < plugin->tables().size()) {
        log_msg(logger, 4, 0, "LUT dump error %s: only %d out of %d LUTs dumped.\n",
                plugin->get_type().get_name().c_str(), ndmps, IF->tables().size());
      }
    }
  }
}

/** close sv table IO files
 *
 * \param MIIF   pointer to MULTI_IF structure
 */
void MULTI_IF::close_svs_dumps() {
  if (get_rank()) return;

  // close files
  for (int i = 0; i < svd.n; i++)
#ifndef USE_HDF5
    if (svd.hdls[i] != NULL)
#endif  // ifndef USE_HDF5
    f_close(svd.hdls[i]);
}

/** load a particular state variable into an array
 *
 *  \param tab    state variable array
 *  \param offset offset of variable into table
 *  \param n      \#nodes
 *  \param svSize size of structure holding all SVs
 *  \param size   size of state variable
 *
 *  \return pointer to a buffer which must be free'd afterwards
 */
char *get_sv(void *tab, int offset, int n, int svSize, int size, int dlo_vector_size) {
  char *buf = static_cast<char *>(malloc(n*size));
  char *bp  = buf;
  char *p   = static_cast<char *>(tab) + offset;

  for (int i = 0; i < n; i += dlo_vector_size) {
    int dlo_array_size = min(dlo_vector_size, n - i);
    memcpy(bp, p, size * dlo_array_size);
    bp += size * dlo_array_size;
    p  += svSize;
  }
  return buf;
}

/** dump state variables
 *
 * \param MIIF   pointer to MULTI_IF structure
 * \param iot    IO timer
 *
 * \return the number of data written per dump variable
 */
int MULTI_IF::dump_svs(base_timer *iot) {
  int nwr = 0;

  if (iot->triggered) {
    for (int i = 0; i < svd.n; i++) {
      nwr = 0;

      FILE* fd = svd.hdls[i] ? svd.hdls[i]->fd : NULL;
      char *buf = get_sv(svd.svtab[i], svd.offset[i], svd.num[i], svd.svsize[i], svd.size[i], svd.dlo_vs[i]);
      nwr += SF::root_write<char>(fd, (char*) buf, svd.size[i]*svd.num[i], PETSC_COMM_WORLD);
      free(buf);
    }
    svd.nwr += nwr;
    svd.n_dumps++;
  }

  return nwr;
}

/**
 * @brief GPU kernel to emulate the `add_scaled` call made to adjust the
 * Vm values when the update to Vm is not made externally. This is to avoid
 * computing the Vm update on CPU when runnign a GPU model.
 */
#ifdef HAS_GPU_MODEL
__global__
void update_vm(int start, int end, double *vm, double *ion, double dt)
{
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < end)
     vm[i] = vm[i] + (ion[i] * (-dt));
}
#endif

// * compute what has to be computed, current or otherwise
void MULTI_IF::compute_ionic_current(bool flag_send, bool flag_receive)
{
  gdata[Iion]->set(0.0);
  if (flag_send == 1) {
  	this->getRealData();
  }

  for (int j = 0; j < this->numSubDt; j++)
  {
    for (int i = 0; i < this->N_IIF; i++)
    {
      IonIfBase* pIF = this->IIF[i];
      if (!this->N_Nodes[i]) continue;

      prepare_error_recovery(this, i, pIF->get_tstp().cnt * this->dt);
      update_ts(&pIF->get_tstp());

      int current = 0;
      do {
        try {
          pIF->compute(current, this->N_Nodes[i], this->ldata[i]);
          current = this->N_Nodes[i];
        }
        catch(int e) { //FIXME
          if(e==-1) {
            // CHECK this->NodeLists[i][e.node], e.node);
            fprintf(stderr, "LIMPET compute fail in %s at node %d (local %d)! Aborting!\n",
                    this->name.c_str(), this->NodeLists[i][j], j);
            exit(1);
          }
          current++;
        }
      } while (current < this->N_Nodes[i]);

      for (auto& plugin : pIF->plugins()) {
        current = 0;
        update_ts(&plugin->get_tstp());

        do {
          try {
            plugin->compute(current, this->N_Nodes[i], this->ldata[i]);
            current = this->N_Nodes[i];

            if (plugin == nullptr)
              throw -1;
          }
          catch(int e) { //FIXME
            if(e==-1) {
              // CHECK this->NodeLists[i][e.node], e.node);
              fprintf(stderr, "LIMPET plugin fail in %s at node %d (local %d)! Aborting!\n",
                      this->name.c_str(), this->NodeLists[i][j], j);
              exit(1);
            }
            current++;
          }
        } while (current < this->N_Nodes[i]);
      }
    }

    #ifdef HAS_GPU_MODEL
    //TODO: extUpdateVm can be true outside of the bench executable ! But this should
    // only run in bench! This should be done for each model according to its target
    if(!extUpdateVm && is_gpu(this->targets[0])) {

	    #if defined __CUDA__ || defined __HIP__
      update_vm<<<(this->N_Nodes[0]/64)+1,64>>>(0, this->N_Nodes[0], this->ldata[0][Vm], this->ldata[0][Iion], this->dt);
        #ifdef __CUDA__
      cudaDeviceSynchronize();
        #elif defined __HIP__
      hipDeviceSynchronize();
#endif
	    #else
                fprintf(stderr, "GPU/CUDA not found");
	    #endif
	}
    #endif

    if (flag_receive == 1) {
    	this->releaseRealData();
    }

    // TODO: Target should be checked for each region
    if(!extUpdateVm && !is_gpu(this->targets[0]))
      gdata[Vm]->add_scaled(*gdata[Iion], SF_real(-dt));
  }
}

void MULTI_IF::free_MIIF() {
  assert(!this->doppel);

  // Free data first since this function needs to access memory in the IIFs
  freeIMPData(this);
  for (auto& pIF : this->IIF) {
    pIF->get_type().destroy_ion_if(pIF);
  }
}

/** allocate memory for the structures to hold real data for IMPs
 *  and check that all needed data is passed
 *
 * \post For each Ionic model, an array of IMPDataStruct is created with
 *       each entry pointing to the local portion of the data
 */
void initializeIMPData(MULTI_IF *pMIIF) {
  for (int i = 0; i < pMIIF->N_IIF; i++) {
    if (!pMIIF->N_Nodes[i])
      continue;

    // if memory contiguous, we don't need to copy data, merely pass pointers
    pMIIF->contiguous[i] = pMIIF->N_Nodes[i]-1 ==
      pMIIF->NodeLists[i][pMIIF->N_Nodes[i]-1]-pMIIF->NodeLists[i][0];

    for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
      // check if it is used by the IMPs
      if (!USED_DAT(pMIIF->IIF[i], imp_data_flag[j]) )
        continue;

      // allocate mem buffer only if not contiguous or if data is on GPU
      if (!pMIIF->contiguous[i] || is_gpu(pMIIF->IIF[i]->get_target()))
        pMIIF->ldata[i][j] =
          allocate_on_target<GlobalData_t>(pMIIF->IIF[i]->get_target(),
                                           pMIIF->N_Nodes[i]);
      // check if data supplied
      if (pMIIF->gdata[j] == NULL) {
        log_msg(pMIIF->logger, 5, LOCAL, "IMP data type %s not supplied for region %d", imp_data_names[j], i);
        exit(1);
      }
    }
  }
}  // initializeIMPData

/** for each IMP data vector, get the local portion corresponding to each ionic
 *  model
 *
 * If PETSc is used, we need to get the local data first <br>
 *
 * If nodes for a particular region are noncontigous in the original vector,
 * they must be copied to a local contiguous vector
 */
void MULTI_IF::getRealData() {
  // set rdata in the parent vector to point to the
  // local data to be computed on the local processor
  for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
    SF_real* rdata;
    if (this->gdata[j] != NULL) {
      rdata             = this->gdata[j]->ptr();
      this->procdata[j] = rdata;
    }
    else continue;

    // now map local data so that each ionic model can use it
    // if noncontiguous in the parent vector, copy to contiguous local vector
    for (int i = 0; i < this->N_IIF; i++) {
      if (!this->N_Nodes[i] || !USED_DAT(this->IIF[i], imp_data_flag[j]) )
        continue;

      if (this->contiguous[i] && !is_gpu(this->IIF[i]->get_target()))
        this->ldata[i][j] = static_cast<GlobalData_t *>(rdata) + this->NodeLists[i][0];
       else {
        const int* ip = this->NodeLists[i];
        for (int k = 0; k < this->N_Nodes[i]; k++)
          this->ldata[i][j][k] = rdata[ip[k]];
       }
    }
  }
}  // getRealData

/** copy data back into original vectors
 *
 * If the data passed from the main is noncontiguous in memory,
 * the data values must
 * first be copied from the local contiguous array
 *
 * If PETSc is used, the vector must be restored
 */
void MULTI_IF::releaseRealData() {

  for (int i = 0; i < this->N_IIF; i++) {
    if (!this->N_Nodes[i]) continue;

    // Every external variable used by the model must be copied on GPU
    if (is_gpu(this->IIF[i]->get_target())) {
    for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
        if (!USED_DAT(this->IIF[i], imp_data_flag[j]) )
          continue;
        if (this->contiguous[i])
          memcpy(&this->procdata[j][this->NodeLists[i][0]], this->ldata[i][j], sizeof(this->ldata[i][j][0])*this->N_Nodes[i]);
        else {
          for (int k = 0; k < this->N_Nodes[i]; k++)
            this->procdata[j][this->NodeLists[i][k]] = this->ldata[i][j][k];
        }
      }
    }
    else {
      for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
        if (this->IIF[i]->get_moddat() & imp_data_flag[j]) {
          if (!this->contiguous[i]) {
            for (int k = 0; k < this->N_Nodes[i]; k++)
              this->procdata[j][this->NodeLists[i][k]] = this->ldata[i][j][k];
          }
        }
      }
    }
  }

  for (int j = 0; j < NUM_IMP_DATA_TYPES; j++)
    if (this->gdata[j] != NULL)
      this->gdata[j]->release_ptr(this->procdata[j]);
}

// * free the local storage vectors
void freeIMPData(MULTI_IF *pMIIF) {
  for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
    if (pMIIF->gdata[j] != NULL) {
      for (int i = 0; i < pMIIF->N_IIF; i++)
        if ((!pMIIF->contiguous[i] || is_gpu(pMIIF->IIF[i]->get_target())) && USED_DAT(pMIIF->IIF[i], imp_data_flag[j]) && pMIIF->N_Nodes[i] > 0) {
          deallocate_on_target<GlobalData_t>(pMIIF->IIF[i]->get_target(),
                                             pMIIF->ldata[i][j]);
        }
      delete pMIIF->gdata[j];
    }
  }
  free(pMIIF->contiguous);
  deallocate_on_target(Target::CPU, pMIIF->ldata);
}

/** determine IMP plugin flag
 *
 * \param plgstr           colon separated list of plugin names
 * \param out_num_plugins  number of plugins found
 * \param out_plugins      plugin array
 *
 * \post  \p out_plugins is allocated and \p out_num_plugins set
 * \return 1 if found and 0 if not found
 */
int get_plug_flag(char *plgstr, int *out_num_plugins, IonTypeList& out_plugins) {
  if (plgstr == NULL) {
    *out_num_plugins = 0;
    out_plugins.clear();
    return 1;
  }

  // maximum number of plugins is 200 apparently
  char *plugspec = dupstr(plgstr);
  char *saveptr;
  char *token = tokstr_r(plugspec, ":", &saveptr);

  *out_num_plugins = 0;
  while (token) {
    IonType* type = get_ion_type(std::string(token));
    if (type == NULL) {
      free(plugspec);
      return 0;
    } else {
      out_plugins.push_back(*type);
    }
    token = tokstr_r(NULL, ":", &saveptr);
  }

  *out_num_plugins = out_plugins.size();

  free(plugspec);
  return 1;
}  // get_plug_flag

/** adjust a state variable within a model.
 *
 * Right now, state variables are the only variables in a model that
 * vary on a nodal basis.  By setting state variables, we can adjust
 * some parameters on a per-node basis in individual ionic models.
 * This will allow people to run simulations that would otherwise be
 * impossible without using thousands of regions.
 *
 * \param pMIIF     multi-ion interface
 * \param name      Name of parameter to adjust.  The name should be in the
 *                  format "external_var" or "Model_name.state_variable",
                    i.e. "Lambda" or "LRDII_F.m"
 * \param numNodes  Size of the following two arrays
 * \param indices   Array of local node numbers, mapping into our local mesh
 * \param values    Array of values we want to be setting.
 *
 * \return the number of items set
 */
int MULTI_IF::adjust_MIIF_variables(const char* variable,
                                    const SF::vector<SF_int> & indices,
                                    const SF::vector<SF_real> & values)
{
  int num_changed = 0;

  // determine if we're dealing with an external variable or a state variable.
  if (index(variable, '.') == NULL) {
    // external variable, the easy case.
    // which external variable?
    int data_id = -1;
    for (int ii = 0; ii < NUM_IMP_DATA_TYPES; ii++) {
      if (strcmp(variable, imp_data_names[ii]) == 0) {
        data_id = ii;
        break;
      }
    }
    if (data_id == -1) {
      log_msg(logger, 5, FLUSH, "Error!  No external variable named %s in this build of openCARP.", variable);
      exit(EXIT_FAILURE);
    }

    // make sure that external variable is being used.
    if (this->gdata[data_id] == NULL) {
      log_msg(logger, 4, 0,
              "External variable %s is not being used this processor.\n"
              "Is this really what you meant to do?\n"
              "Perhaps you don't have the correct Ionic models selected.",
              imp_data_names[data_id]);
    }
    else {
      // fill in the external variable with the data from the file.
      SF_real *raw_data = this->gdata[data_id]->ptr();

      for (size_t i = 0; i < indices.size(); i++) {
        raw_data[indices[i]] = values[i];
        num_changed++;
      }

      this->gdata[data_id]->release_ptr(raw_data);
    }
  }
  else {
    // state variable.
    // extract the IMP name and state variable name from the string.
    char *saveptr;
    char *my_variable = dupstr(variable);
    char *IIF_name    = tokstr_r(my_variable, ".", &saveptr);
    char *sv_name     = tokstr_r(NULL, ".", &saveptr);

    IonType* type = get_ion_type(std::string(IIF_name));
    if (type == NULL) {
      log_msg(logger, 5, 0, "%s error: %s is not a valid IMP name.", __func__, IIF_name);
      exit(EXIT_FAILURE);
    }

    int sv_offset;
    int sv_size;
    SVgetfcn sv_get = type->get_sv_offset(sv_name,  &sv_offset, &sv_size);
    if (sv_get == NULL) {
      log_msg(logger, 5, 0, "%s error: %s is not a valid state variable for the %s model.",
              __func__, sv_name, IIF_name);
      exit(EXIT_FAILURE);
    }
    SVputfcn sv_put = getPutSV(sv_get);

    // Ok, go through the ionic models
    for (int i_iif = 0; i_iif < this->N_IIF; i_iif++) {
      IonIfBase *lIIF = NULL;
      if (this->IIF[i_iif]->get_type() == *type) {
        lIIF = this->IIF[i_iif];
      }
      else {
        // Go through the plugins
        for (auto& plugin : this->IIF[i_iif]->plugins()) {
          if (plugin->get_type() == *type) {
            lIIF = plugin;
            break;
          }
        }
      }

      if (lIIF == NULL) {
        continue;
      }

      for (size_t ii = 0; ii < indices.size(); ii++) {
        GlobalData_t file_value = values[ii];

        int *target = static_cast<int*>(bsearch(&indices[ii], this->NodeLists[i_iif],
               this->N_Nodes[i_iif], sizeof(int), int_cmp));
        if (target) {
          // We found a point to adjust! Do the adjustment.
          num_changed++;
          sv_put(*lIIF, (int)(target - this->NodeLists[i_iif]), sv_offset, file_value);
        }
      }

    }
    free(my_variable);
  }

  MPI_Allreduce(MPI_IN_PLACE, &num_changed, 1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);

  return num_changed;
}  // adjust_MIIF_variables

/** determine which variables are output each write
 *
 * This troutine assumes there is an output buffer of size \p bufsize which
 * is continuously filled and then output. Each node has a certain amount of
 * data associated with it, contained in \p offset. This routine determines
 * how many times the buffer must be filled and output, as well as
 * deermining the range of nodes to be output with each buffer write
 *
 * \param      N       total number of nodes
 * \param      offset  byte offsets of nodes, significant only on root
 * \param      bufsize output buffer size
 * \param[out] ranges  node range of each write
 *
 * \note ranges is allocated and can be free()'d
 *
 * \return the number of writes
 */
int determine_write_ranges(int N, size_t *offset, size_t bufsize, int **ranges) {
  int nitems;

  if (!get_rank() ) {
    Salt_list r; STRUCT_ZERO(r); r.chunk = 10;
    int temp    = 0;
    SLIST_APPEND(&r, temp);
    long loff = offset[0];
    for (int i = 0; i < N; i++)
      if (offset[i]-loff > bufsize) {
        SLIST_APPEND(&r, i);
        loff = offset[i];
      }

    nitems = r.nitems;
    SLIST_APPEND(&r, N);
    *ranges = (int *)r.data;
  }

  MPI_Bcast(&nitems, 1,        MPI_INT, 0, PETSC_COMM_WORLD);
  if (get_rank() )
    *ranges = static_cast<int *>(malloc( (nitems+1)*sizeof(int) ));
  MPI_Bcast(*ranges, nitems+1, MPI_INT, 0, PETSC_COMM_WORLD);
  return nitems;
}  // determine_write_ranges

/** dump the IIF's to a file so that they can later be restored
 *
 *  the following is output as binary data in order:
 *  -# dump identifier (char *)
 *  -# format version (unsigned int)
 *  -# program version number (unsigned int)
 *  -# time file is created (time_t)
 *  -# simulation time  (float)
 *  -# total \#nodes in model  (int)
 *  -# number of used global data vectors (int)
 *  -# for each used global data vector
 *      -# the length of its name (int)
 *      -# its name (char *)
 *      -# the array (Real *) in canonical order
 *  -# number of IIF regions (int)
 *  -# for each IIF region
 *      -# length of the IMP name (int)
 *      -# IMP name (char *)
 *      -# size of state variable info (int)
 *      -# number of plug-ins (int)
 *      -# for each plug-in
 *          -# length of the IMP name (int)
 *          -# IMP name (char *)
 *          -# size of state variable info (int)
 *  -# the node mask specifying the regions (IIF_Mask_t *)
 *     in canonical order
 *  -# for each node
 *      -# state variables for the base IMP (IMP_SV)
 *      -# for each plug-in
 *          -# state variables (PLUGIN_SV)
 *
 * \param fname     output file name
 * \param simtime   simulation time at which dump occurs
 * \param gid       ID of the mesh we operate on
 * \param append    true to append to existing file
 * \param revision  calling program version
 *
 */
void MULTI_IF::dump_state(char *fname, float simtime, mesh_t gid,
                          bool append, unsigned int revision)
{
  float t0, t1;
  t0 = get_time();

  FILE_SPEC out = NULL;
  int rank = get_rank();
  int miif_node_gsize = get_global(this->numNode, MPI_SUM);
  int error = 0;

  log_msg(logger, 0, 0, "Saving state at time %f in file: %s", simtime, fname);

  if (rank == 0) {
    out = f_open(fname, append ? "a" : "w");

    if(out) {
      fseek(out->fd, 0, SEEK_END);  // NOP call to sync disk data for switch to write mode
      write_bin_string(out, Magic_MIIF_ID);

      fwrite(&MIIF_Format, sizeof(unsigned int), 1, out->fd);
      fwrite(&revision, sizeof(unsigned int), 1, out->fd);
      time_t tm = time(NULL);
      fwrite(&tm, sizeof(time_t), 1, out->fd);
      fwrite(&simtime, sizeof(float), 1, out->fd);
      fwrite(&miif_node_gsize, sizeof(int), 1, out->fd);

      int num_gdata = 0;
      for (int i = 0; i < NUM_IMP_DATA_TYPES; i++)
        if (this->gdata[i]) num_gdata++;

      fwrite(&num_gdata, sizeof(int), 1, out->fd);
    }
    else
      error++;
  }

// #define CHATTY

  if(get_global(error, MPI_SUM))
    EXIT(EXIT_FAILURE);

  FILE* fd = rank == 0 ? out->fd : NULL;
  sf_vec* outVec;

  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++) {
    if (this->gdata[i]) {
      if(rank == 0) {
        write_bin_string(out, imp_data_names[i]);
#ifdef CHATTY
        log_msg(NULL, 0, 0, "\tDumping %s at %d", imp_data_names[i], ftell(fd) );
#endif
      }

      // If we have a proper set up parallel context, we pass a gid != unset_msh. Then
      // we can query petsc-to-canonical scattering. If we are in a simpler context
      // like bench, we assume that no scattering is needed and just shallow-copy the vector.
      if(gid != unset_msh) {
        SF::init_vector(&outVec, this->gdata[Vm]);
        SF::scattering* sc = get_permutation(gid, PETSC_TO_CANONICAL, 1);
        assert(sc != NULL);

        sc->forward(*this->gdata[i], *outVec);
        outVec->write_binary<SF_real>(fd);
        delete outVec;
      }
      else {
        this->gdata[i]->write_binary<SF_real>(fd);
      }
    }
  }

  // output IMP region info
  // #regions and the IM and plugins for each region
  // determine memory requirements for each region
  int*    imp_mem = new int[this->N_IIF];
  size_t *offset = NULL;
  long filepos   = 0.;

  if (rank == 0) {
#ifdef CHATTY
    log_msg(NULL, 0, 0, "\tDumping IMP sizes at %d", ftell(fd) );
#endif

    int max = 1;
    fwrite(&this->N_IIF, sizeof(int), 1, fd);  // #IIF's

    for (int i = 0; i < this->N_IIF; i++) {
      IonIfBase *imp = this->IIF[i];
      write_bin_string(out, imp->get_type().get_name().c_str());

      std::size_t sv_size = imp->get_sv_size();
      fwrite(&sv_size, sizeof(int), 1, fd);
      unsigned long n_plugins = (unsigned long) imp->plugins().size();
      fwrite(&n_plugins, sizeof(int), 1, fd);
      imp_mem[i] = imp->get_sv_size();

      for (auto& plug : imp->plugins()) {
        write_bin_string(out, plug->get_type().get_name().c_str());

        fwrite(&sv_size, sizeof(int), 1, fd);
        imp_mem[i] += plug->get_sv_size();
      }
    }
    filepos = ftell(fd);
  }

  MPI_Bcast(imp_mem, this->N_IIF, MPI_INT, 0, PETSC_COMM_WORLD);

  // If we have a proper set up parallel context, we pass a gid != unset_msh. Then
  // we can query a mesh and its parallel numbering. If we are in a simpler context
  // like bench, we build the numbering ourselves. Then, we assume that no
  // renumbering has taken place.
  SF::vector<int> loc2canon;
  if(gid != unset_msh) {
    const sf_mesh & mesh = get_mesh(gid);
    const SF::vector<mesh_int_t> & canon_nbr = mesh.get_numbering(SF::NBR_SUBMESH);
    const SF::vector<mesh_int_t> & alg_idx   = mesh.pl.algebraic_nodes();
    loc2canon.resize(alg_idx.size());

    for(size_t i=0; i<alg_idx.size(); i++) loc2canon[i] = canon_nbr[alg_idx[i]];
  }
  else {
    int rank = get_rank();
    SF::vector<int> layout;
    layout_from_count(this->numNode, layout, PETSC_COMM_WORLD);
    loc2canon.resize(this->numNode);

    for (int i = 0; i < this->numNode; i++) loc2canon[i] = layout[rank] + i;
  }

  // write out IIF_Mask canonically ordered
  int *canord = new int[this->numNode];
  for (int i = 0; i < this->numNode; i++)
    canord[i] = loc2canon[i];

#ifdef CHATTY
  log_msg(NULL, 0, 0, "\tDumping IMP masks at %d", filepos);
#endif  // ifdef CHATTY

  SF::root_write_ordered<int, IIF_Mask_t>(fd, canord, IIFmask, numNode, PETSC_COMM_WORLD);

  // for every local node, we compute the size of its ionic model
  // and plugins, and the associated displacments between the nodes
  SF::vector<int> impsize(numNode), impdsp;
  for(int i=0; i<numNode; i++)
    impsize[i] = imp_mem[(int)IIFmask[i]];
  SF::dsp_from_cnt(impsize, impdsp);

  // this also gives us the size of the total buffer
  size_t num_entr = SF::sum(impsize);
  SF::vector<char> impdata(num_entr);

  // now we can memcpy the ionic models from the IonIfBase arrays into our buffer
  for (int imp_idx = 0; imp_idx < this->N_IIF; imp_idx++)
  {
    IonIfBase* iif = this->IIF[imp_idx];
    // When using data layout optimization, checkpointing indices have to be
    // change a bit. Without DLO, this value is simply 1
    std::size_t vec_size = iif->get_type().dlo_vector_size();
    for (int imp_nod_idx = 0; imp_nod_idx < this->N_Nodes[imp_idx]; imp_nod_idx+=vec_size) {
      int index = imp_nod_idx / vec_size;
      int loc     = this->NodeLists[imp_idx][imp_nod_idx];
      int svSize  = iif->get_sv_size();

      char* write = impdata.data() + impdsp[loc];
      char* read  = (char*) iif->get_sv_address() + index * svSize;
      memcpy(write, read, svSize);

      int shift = svSize;
      for (auto& plug : iif->plugins()) {
        int plugsize = plug->get_sv_size();

        write = impdata.data() + impdsp[loc] + shift;
        read  = (char*) plug->get_sv_address() + index * plugsize;
        memcpy(write, read, plugsize);
        shift += plugsize;
      }
    }
  }

  // finally we can write out the IonIf data to disk
  SF::root_write_ordered<int, char>(fd, canord, impsize.data(), impdata.data(),
                                    numNode, num_entr, PETSC_COMM_WORLD);
  delete [] canord;
  delete [] imp_mem;

  if(fd) fclose(fd);

  double dump_time = timing(t1, t0);
  log_msg(logger, 0, 0, " in %.3f seconds.\n", dump_time);
}

/** restore the IIF's from a file
 *
 *  The model saved must agree with the model being run. This is checked
 *  by ensuring the \#nodes agree. See dump_state()
 *
 *  Compatible IIF's are restored. To be compatible, the ionic model and
 *  ionic model size for an IIF must match. Plug-ins and their order
 *  may change. Only compatible plug-ins (matching type and size) are restored,
 *  others are ignored.
 *
 * \param fname   input file name, if NULL use open file stream
 * \param gid     the ID of the mesh we operate on (usually intra_elec_msh)
 * \param close   close the file stream afterwards?
 *
 * \pre The IMPs have all been allocated
 *
 * \note all ranks process the file
 *
 * \note There are static variables, the input file stream and file name
 *
 * \return the time at which the simulation was saved
 */
float MULTI_IF::restore_state(const char *fname, mesh_t gid, bool close)
{
  static FILE_SPEC in = NULL;
  static char *last_fn = NULL;
  double t0, t1;

  t0 = get_time();
  int error    = 0;
  int my_rank  = get_rank();
  int mpi_size = get_size();

  if (my_rank == 0) {
    if (fname) in = f_open(fname, "r");

    if (!in) {
      if (fname) log_msg(logger, 5, 0, "Error: cannot open file: %s\n", fname);
      else       log_msg(logger, 5, 0, "Error: file stream not open yet");
      error++;
    }
    else {
      if (strcmp(read_bin_string(in), Magic_MIIF_ID) ) {
        log_msg(logger, 5, 0, "%s is not a recognized MIIF dump file", fname);
        error++;
      }
    }
  }

  if (get_global(error, MPI_SUM))
    EXIT(1);

  if (fname) {
    free(last_fn);
    last_fn = strdup(fname);
  }
  else {
    fname = last_fn;
  }

  unsigned int format, version;
  float time = 0.0f;
  time_t save_date;
  f_read_par(&format,    sizeof(unsigned int), 1, in);
  f_read_par(&version,   sizeof(unsigned int), 1, in);
  f_read_par(&save_date, sizeof(time_t),       1, in);
  f_read_par(&time,      sizeof(float),        1, in);
  log_msg(logger, 0, 0, "Restoring time %f from %s (format v%d) generated\n\tby calling "
                          "program r%d on %s", time, fname, format, version, ctime(&save_date) );

  int savedNum, glob_numNode;
  MPI_Allreduce(&this->numNode, &glob_numNode, 1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);

  f_read_par(&savedNum, sizeof(int), 1, in);
  if (savedNum != glob_numNode) {
    log_msg(logger, 5, 0, "expecting %d nodes but read %d nodes", glob_numNode, savedNum);
    EXIT(1);
  }

  int num_gdata;
  f_read_par(&num_gdata, sizeof(int), 1, in);
  SF::scattering & petsc2canon = *get_permutation(gid, PETSC_TO_CANONICAL, 1);

  for (int g = 0; g < num_gdata; g++) {
    char *datatype = read_bin_string_par(in);
    int   i;
    for (i = 0; i < NUM_IMP_DATA_TYPES; i++) {
      if (!strcmp(datatype, imp_data_names[i]) ) {
        if (this->gdata[i]) {
          log_msg(logger, 0, 0, "\tRestoring global data %s", imp_data_names[i], datatype);

          FILE* fd = my_rank == 0 ? in->fd : NULL;
          this->gdata[i]->read_binary<SF_real>(fd);
          bool fwd = false;
          petsc2canon(*this->gdata[i], fwd);
        } else {
          log_msg(logger, 0, 0, "\tGlobal data %s not used", datatype);
        }
        break;
      }
    }
    if (i == NUM_IMP_DATA_TYPES)
      log_msg(logger, 3, 0, "\tSaved global data %s not recognized", datatype);

    if ( (my_rank == 0) && ((i == NUM_IMP_DATA_TYPES) || !this->gdata[i]) )
      fseek(in->fd, this->gdata[i]->gsize() * sizeof(SF_real), SEEK_CUR);

    free(datatype);
  }

  int N_IIF;
  f_read_par(&N_IIF, sizeof(int), 1, in);

  // read in the IMPs for each region
  IMPinfo *imps = static_cast<IMPinfo *>(calloc(N_IIF, sizeof(IMPinfo)));
  int *IMPsz    = static_cast<int *>(malloc(N_IIF * sizeof(int)));

  for (int i = 0; i < N_IIF; i++) {
    imps[i].name = read_bin_string_par(in);
    f_read_par(&imps[i].sz,    sizeof(int), 1, in);
    f_read_par(&imps[i].nplug, sizeof(int), 1, in);

    imps[i].offset = 0;
    IMPsz[i]       = imps[i].sz;
    imps[i].plug   = static_cast<IMPinfo *>(calloc(sizeof(IMPinfo), imps[i].nplug));

    for (int j = 0; j < imps[i].nplug; j++) {
      imps[i].plug[j].offset = IMPsz[i];
      imps[i].plug[j].name   = read_bin_string_par(in);
      f_read_par(&imps[i].plug[j].sz,  sizeof(int), 1, in);
      IMPsz[i] += imps[i].plug[j].sz;
    }
  }

  // compare new and old regions to determine which can be copied
  for (int i = 0; i < N_IIF; i++) {
    if (i >= this->N_IIF) {
      log_msg(NULL, 3, 0, "Saved IMP region %d out of range", i);
      continue;
    }
    if (strcmp(imps[i].name, this->IIF[i]->get_type().get_name().c_str()) ) {
      log_msg(NULL, 3, 0, "Saved IMP region %d ionic model does not match that of IMP region %d", i, i);
      continue;
    }
    if (imps[i].sz != this->IIF[i]->get_sv_size()) {
      log_msg(NULL, 3, 0, "Saved IMP region %d size does not match current IMP size", i);
      continue;
    }

    // the IM is good to go
    imps[i].compatible = true;
    for (int j = 0; j < imps[i].nplug; j++)
      for (int k = 0; k < this->IIF[i]->plugins().size(); k++)
        if (!strcmp(imps[i].plug[j].name, this->IIF[i]->plugins()[k]->get_type().get_name().c_str()) &&
            (imps[i].plug[j].sz == this->IIF[i]->plugins()[k]->get_sv_size()) ) {
          log_msg(NULL, 3, 0, "Saved IMP region %d plugin %s compatible", i, imps[i].plug[j].name);
          imps[i].plug[j].map        = k;
          imps[i].plug[j].compatible = true;
          break;
        }

  }

  // read in region mask
  IIF_Mask_t *canMask = static_cast<char *>(malloc(glob_numNode*sizeof(this->IIFmask[0]) ));
  f_read_par(canMask, sizeof(IIF_Mask_t), glob_numNode, in);

  // determine relative IMP data offsets based on canonical ordering
  size_t *offset = static_cast<size_t *>(calloc(glob_numNode+1, sizeof(size_t)));
  for (int i = 1; i <= glob_numNode; i++)
    offset[i] = offset[i-1] + IMPsz[static_cast<int>(canMask[i-1])];

  // read in the state variable data
  // TODO: Aurel: Ideally rank0 would read in the data and communicate it, but right now I dont have
  // time to code this up. Thus, at least the ranks will read their data one by one so
  // we dont saturate the file system.
  long SVstart;
  if (my_rank == 0) {
    SVstart = ftell(in->fd);
    f_close(in);
  }

  MPI_Bcast(&SVstart, sizeof(long), MPI_BYTE, 0, PETSC_COMM_WORLD);
  int mismatch = 0;

  // we need to map from a local algebraic index to a global canonical index
  const sf_mesh & mesh = get_mesh(gid);
  const SF::vector<mesh_int_t> & canon_nbr = mesh.get_numbering(SF::NBR_SUBMESH);
  const SF::vector<mesh_int_t> & alg_idx   = mesh.pl.algebraic_nodes();
  SF::vector<mesh_int_t> loc2canon(alg_idx.size());

  for(size_t i=0; i<alg_idx.size(); i++) loc2canon[i] = canon_nbr[alg_idx[i]];

  // the ranks read the file one-by-one
  for (int pid = 0; pid < mpi_size; pid++) {
    if (my_rank == pid) {
      if(in) delete in;
      in = f_open(fname, "r");

      for (int i = 0; i < N_IIF; i++) {
        fseek(in->fd, SVstart, SEEK_SET);
        mismatch += this->IIF[i]->restore(in, this->N_Nodes[i], this->NodeLists[i],
                                canMask, offset, imps+i, loc2canon.data());
      }

      f_close(in);
    }
    MPI_Barrier(PETSC_COMM_WORLD);
  }
  free(canMask);
  free(IMPsz);

  MPI_Reduce(my_rank ? &mismatch : MPI_IN_PLACE, &mismatch, 1, MPI_INT, MPI_SUM, 0, PETSC_COMM_WORLD);
  if ( (my_rank == 0) && mismatch)
    log_msg(NULL, 3, 0, "Number of nonmatching nodes: %d", mismatch);

  if ( (my_rank == 0) && (!close) ) {
    if(in) delete in;
    in = f_open(fname, "r");

    // position at end of current MIIF data
    fseek(in->fd, SVstart+offset[glob_numNode], SEEK_SET);
  }

  free(offset);
  for (int i = 0; i < N_IIF; i++)
    free(imps[i].plug);
  free(imps);

  double restore_time = timing(t1, t0);
  log_msg(logger, 0, 0, "State restored from file %s in %.3f seconds.\n", fname, restore_time);

  return time;
}

/** wrapper function to add sv's for dumping using a colon separated list
 *
 * \param pmiif      multi-ionic interface structure
 * \param region     IIF number
 * \param imp_name   name of IMP
 * \param reg_name   name of region
 * \param sv_lst     ionic model state variable list we want to dump
 * \param plg_lst    plugin list of this ionic model
 * \param plg_sv_lst plugin state variable list we want to dump
 *
 */
void MULTI_IF::sv_dump_add_by_name_list( int region, char *imp_name,
                                        char *reg_name, char *sv_lst, char *plg_lst,
                                        char *plg_sv_lst, double t, double dump_dt) {
  char file[8000], svs[1024], plgs[1024], plgsvs[1024];
  char *e, *l, *p, *svnames, *plgnames, *plgsvnames;

  strcpy(svs,    sv_lst);
  strcpy(plgs,   plg_lst);
  strcpy(plgsvs, plg_sv_lst);
  svnames    = &svs[0];
  plgnames   = &plgs[0];
  plgsvnames = &plgsvs[0];

  // override default setting for dump interval
  this->svd.intv = dump_dt;

  // override default start time for dumping (in case of a restart)
  this->svd.t_dump = t;

  // parse ionic model sv list
  while ( (e = get_next_list(svnames, ',') ) ) {
    snprintf(file, sizeof file, "%s.%s.bin", reg_name, svnames);
    this->sv_dump_add_by_name(region, imp_name, svnames, reg_name, file);
    svnames = e;
  }

  // Information is provided as follows:
  // region[X].plugin        = "PLG_A:PLG_B"
  // region[X].plug_sv_dumps = "m,h,n:x,y"

  // parse plugin list
  while ( (p = get_next_list(plgnames, ':') ) ) {
    // get sv list corresponding to current plugin
    l = get_next_list(plgsvnames, ':');
    while ( (e = get_next_list(plgsvnames, ',') ) ) {
      snprintf(file, sizeof file, "%s_%s.%s.bin", reg_name, plgnames, plgsvnames);
      this->sv_dump_add_by_name(region, plgnames, plgsvnames, reg_name, file);
      plgsvnames = e;
    }
    plgnames   = p;
    plgsvnames = l;
  }
}  // sv_dump_add_by_name_list

/** add a state variable to the list to be dumped
 *
 * \param miif     Multi ion IF structure
 * \param region   region containing the state variable
 * \param imp      ID of IMP within the region
 * \param offset   offset of SV within the IMP SV structure
 * \param size     size of the SV
 * \param dtype    ID of data type
 * \param filename output file name
 * \param impname  string specifying the IMP
 * \param regname  name of the region
 *
 * \note It is much easier to call this function with the ::SV_add macro
 *       or with the ::sv_dump_add_by_name function than directly.
 */
void MULTI_IF::sv_dump_add(int region, const IonType& type, int offset, int size, int dtype,
                           const char *filename, const char *regname) {
  IonIfBase *IF = this->IIF[region];
  int n      = this->svd.n;

  // find the proper IMP
  if (IF->get_type() != type) {
    int i;
    for (i = 0; i < IF->plugins().size(); i++)
      if (IF->plugins()[i]->get_type() == type)
        break;
    if (i == IF->plugins().size()) {
      log_msg(logger, 2, 0, "Warning: IMP %s not found in Region %s\n",
              type.get_name().c_str(), regname);
      return;
    } else {
      IF = IF->plugins()[i];
    }
  }

  this->svd.active = 1;
  this->svd.n++;
  this->svd.hdls    = static_cast<FILE_SPEC *>(realloc(this->svd.hdls, this->svd.n*sizeof(this->svd.hdls[0])));
  this->svd.fn      = static_cast<char **>(realloc(this->svd.fn, this->svd.n*sizeof(char *)));
  this->svd.reg     = static_cast<int *>(realloc(this->svd.reg,    this->svd.n*sizeof(int)));
  this->svd.svnames = static_cast<char **>(realloc(this->svd.svnames, this->svd.n*sizeof(char *)));
  this->svd.n_dumps = 0;
  this->svd.nwr     = 0;
  this->svd.offset  = static_cast<int *>(realloc(this->svd.offset, this->svd.n*sizeof(int)));
  this->svd.size    = static_cast<int *>(realloc(this->svd.size,   this->svd.n*sizeof(int)));
  this->svd.dlo_vs  = static_cast<int *>(realloc(this->svd.dlo_vs,   this->svd.n*sizeof(int)));
  this->svd.dtype   = static_cast<int *>(realloc(this->svd.dtype,  this->svd.n*sizeof(int)));
  this->svd.svtab   = static_cast<void **>(realloc(this->svd.svtab, this->svd.n*sizeof(void *)));
  this->svd.svsize  = static_cast<size_t *>(realloc(this->svd.svsize, this->svd.n*sizeof(size_t)));
  this->svd.num     = static_cast<int *>(realloc(this->svd.num, this->svd.n*sizeof(int)));
  this->svd.reg     = static_cast<int *>(realloc(this->svd.reg,    this->svd.n*sizeof(int)));

  if (!get_rank() ) {
#ifdef HDF5
    assert(0);
#else  // ifdef HDF5
    this->svd.hdls[n] = f_open(filename, "w+");
#endif  // ifdef HDF5
  } else {
    this->svd.hdls[n] = NULL;
  }

  this->svd.fn[n]      = dupstr(filename);
  this->svd.reg[n]     = region;
  this->svd.svnames[n] = NULL;  // has to be filled in in calling routine
  this->svd.offset[n]  = offset;
  this->svd.size[n]    = size;
  this->svd.dtype[n]   = dtype;
  this->svd.svtab[n]   = IF->get_sv_address();
  this->svd.svsize[n]  = IF->get_sv_size();
  this->svd.num[n]     = IF->get_num_node();
  this->svd.dlo_vs[n] = IF->get_type().dlo_vector_size();
}  // sv_dump_add

/** add a state variable by name to the list to be dumped
 *
 * \param miif     Multi ion IF structure
 * \param region   region containing the state variable
 * \param impname  string specifying the IMP
 * \param svname   string specifying the state variable to dump
 * \param regname  name of the region
 * \param filename output file name
 *
 */
int MULTI_IF::sv_dump_add_by_name(int region, char *impname,
                                  char *svname, char *regname, char *filename) {
  int offset, size, dtype, added = 0;
  char *svtypename = NULL;
  char *filename_bin;

  IonType* type = get_ion_type(std::string(impname));
  filename_bin = strcat(filename, ".bin");

  assert(type != NULL);
  if (type->get_sv_offset(svname, &offset, &size) ) {
    type->get_sv_type(svname, &dtype, &svtypename);
    this->sv_dump_add(region, *type, offset, size, dtype, filename_bin, regname);
    this->svd.svnames[this->svd.n-1] = dupstr(svname);
    added                            = 1;
  } else {
    log_msg(NULL, 1, 0, "No state variable added to dump list");
  }

  return added;
}

/** allocate the global vectors which are shared amongst IMPs
 *
 *  They may be manually allocated. If the Vm vector is preallocated,
 *  all vectors copy its distribution
 *
 *  \param [in] miif  Multi ion IF structure
 *
 *  \pre Vm is allocated for PETsc runs
 *
 *  \post all required shared data vectors are allocated
 */
void allocate_shared_data(MULTI_IF *miif)
{
  assert(miif->gdata[Vm] != NULL);

  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++) {
    for (int n = 0; n < miif->N_IIF; n++)
      if (USED_DAT(miif->IIF[n], imp_data_flag[i]) && (miif->gdata[i] == NULL)) {
        SF::init_vector(&miif->gdata[i], miif->gdata[Vm]);
        break;
      }
  }
}

/** test whether we have any mechanics enabled
 *
 *  \param [in] miif  Multi ion IF structure
 *
 * \return true, if mechanics is used
 *         false, otherwise
 */
bool MULTI_IF::use_stretch() {
  int num_mech_data = 0;

  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++) {
    if ( (Lambda_DATA_FLAG == i) || (delLambda_DATA_FLAG == i) ||
         (Tension_DATA_FLAG == i) || (tension_component_DATA_FLAG == i) ) {
      for (int n = 0; n < this->N_IIF; n++)
        if (USED_DAT(this->IIF[n], imp_data_flag[i]) )
          num_mech_data++;
    }
  }
  return static_cast<bool>(num_mech_data);
}

/** assign transmembrane current stimulus to an ionic species and adjust
 *  concentration of species
 *
 *  \param miif    ionic models
 *  \param charge  transmembrane charge=current*dt (µA*ms/cm^2)
 *  \param species ionic species carrying current
 *  \param beta    effective surface to volume ratio of cells (per micrometer)
 *  \param node    list of stimulated nodes
 *  \param numnode \#nodes in list
 *
 *  \note extracellular concentrations are not affected since it is assumed
 *        that electrons at the electrode interface cause
 *        depletion/creation of ions and the ground electrode is
 *        located far away
 *
 *  \note The voltage is NOT affected. This must be done separately.
 */
void MULTI_IF::transmem_stim_species(float charge, const char *species,
                                     float beta, int *node, int numnode) {
  float z;
  if (strstr(species, "Ca") != NULL) {
    charge *= 1000;                               // Ca uses micromolar instead of millimolar
    z     = 2;
  } else if (strstr(species, "Na") != NULL) {
    z = 1;
  } else if (strstr(species, "Cl") != NULL) {
    z = -1;
  } else if (strstr(species, "K") != NULL) {
    z = 1;
  } else {
    log_msg(logger, 5, 0, "Unimplemented ion species: %s\n", species);
    exit(1);
  }
  int offset, sz;
  SVgetfcn ion_get;
  SVputfcn ion_put;
  static int warned = 0;

  int rank = get_rank();
  SF::vector<int> layout;
  layout_from_count(numnode, layout, PETSC_COMM_WORLD);
  int my_low_idx = layout[rank], my_high_idx = layout[rank+1];

  for (int i = 0; i < numnode; i++) {
    if ((node[i] < my_low_idx) || (node[i] >= my_high_idx)) continue; // not on processor

    for (int j = 0; j < this->N_IIF; j++) {
      if (this->NodeLists[j] == NULL) continue; // no stim nodes on processor

      if ((node[i] < this->NodeLists[j][0]) ||
          (node[i] > this->NodeLists[j][this->N_Nodes[j]-1]) ) {
        continue;  // not in this IMP
      } else {
        ion_get = this->IIF[j]->get_type().get_sv_offset(species, &offset, &sz);
        if (ion_get == NULL) {
          if (!warned) {
            warned = 1;
            log_msg(logger, 2, 0, "Ion species not present in ionic model: %s\n",
                    species);
          }
          return;
        }
        ion_put = getPutSV(ion_get);
      }

      // does this model provide a correct conversion factor?
      double delta_conc;
      cell_geom g = this->IIF[j]->cgeom();
      if (g.sl_i2c != NDEF) {
        delta_conc = charge*g.sl_i2c/z;
      } else {
        // conversion with generic factor
        delta_conc = charge*beta/FARADAY/z*1.e-1;  // convert to millimolar/L
      }

      for (int k = 0; k < this->N_Nodes[j]; k++)
        if (node[i] == this->NodeLists[j][k])
          ion_put(*this->IIF[j], k, offset, ion_get(*this->IIF[j], k, offset)-delta_conc);
    }
  }
}  // transmem_stim_species

/** change the time step
 *
 *
 * \param miif the ionic model iinterface
 * \param Dt   the new time step
 */
void MULTI_IF::MIIF_change_dt( double Dt) {
  this->dt = Dt;
  for (int i = 0; i < this->N_IIF; i++) {
    this->IIF[i]->destroy_luts();
    this->IIF[i]->set_dt((float) Dt);
    this->iontypes[i].get().construct_tables(*this->IIF[i]);

    for (int j = 0; j < this->IIF[i]->plugins().size(); j++) {
      auto& plugin = this->IIF[i]->plugins()[j];
      plugin->destroy_luts();
      plugin->set_dt((float) Dt);
      this->plugtypes[i][j].get().construct_tables(*plugin);
    }
  }
}

#define MEMFREE(A) free(A)

/** release the memory of the doppel
 *
 * \param doppel the doppel of course
 *
 * \post doppel is zeroed
 */
void free_doppel(MULTI_IF *m) {
  assert(m->doppel);

  for (int i = 0; i < m->N_IIF; i++) {
    m->IIF[i]->get_type().destroy_ion_if(m->IIF[i]);
  }

  for (int j = 0; j < NUM_IMP_DATA_TYPES; j++) {
    if (m->gdata[j])
      delete m->gdata[j];
  }

  m->zero_data();
}

#undef MEMFREE

/** update the data of a doppel MIIF without allocating any memory
 *
 * \param doppel the copy
 * \param orig   the one to copy
 *
 * \post \p doppel will be in the same state os \p orig
 */
void doppel_update(MULTI_IF *orig, MULTI_IF *miif_doppel) {
  // removed assertion, need to be able to update bidirectionally
  // assert(doppel->doppel);

  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++)
    if (orig->gdata[i])
      SF::init_vector(&miif_doppel->gdata[i], orig->gdata[i]);

  for (int i = 0; i < orig->N_IIF; i++) {
    miif_doppel->IIF[i]->copy_SVs_from(*orig->IIF[i], false);

    for (int j = 0; j < orig->IIF[i]->plugins().size(); j++)
      miif_doppel->IIF[i]->plugins()[j]->copy_SVs_from(*orig->IIF[i]->plugins()[j], false);
  }

  miif_doppel->getRealData();
}

/** make the minimal copy of a MIIF to run independently
 *
 * \param orig   the original MIIF
 * \param doppel the copy
 *
 * \note the only memory copied is that for the state variables and
 *       private data of the ionic models. All other dynamically
 *       allocated chunks are shared. Private data cannot have dynamically
 *       allocated chunks which are updated
 */
void doppel_MIIF(MULTI_IF *orig, MULTI_IF *miif_doppel) {
  *miif_doppel         = *orig;
  miif_doppel->doppel  = true;

  // deal with the local portion of the global data
  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++)
    if (orig->gdata[i])
      SF::init_vector(&miif_doppel->gdata[i], orig->gdata[i]);

  // copy the state variables
  miif_doppel->IIF = {};

  for (int i = 0; i < orig->N_IIF; i++) {
    // Make a new IonIf for this clone and copy state variables and plugins
    miif_doppel->IIF.push_back(orig->IIF[i]->get_type().make_ion_if(orig->IIF[i]->get_target(),
          orig->IIF[i]->get_num_node(), orig->plugtypes[i]));
    miif_doppel->IIF[i]->copy_SVs_from(*orig->IIF[i], true);
    miif_doppel->IIF[i]->copy_plugins_from(*orig->IIF[i]);
  }
}

/**  check whether global IMP data vector of a given name exist
 *
 *   \param [in] sv  name of IMP data vector
 *
 *   \return true, if vector exists
 *           false, otherwise
 */
bool isIMPdata(const char *sv) {
  return IMPdataLabel2Index(sv) != -1;
}

/**  figure out global IMP data vector index within array
 *   from a given name
 *
 *   \param [in] sv  name of IMP data vector
 *
 *   \return index of IMP data vector within array
 */
int IMPdataLabel2Index(const char *sv) {
  int imp_data_id = -1;

  for (int i = 0; i < NUM_IMP_DATA_TYPES; i++) {
    if (strcmp(sv, imp_data_names[i]) == 0) {
      imp_data_id = i;
      break;
    }
  }
  return imp_data_id;
}

/* copy all of the IMP data from one node to another
 *
 * \param IF   ionic model
 * \param from local IMP number
 * \param to   local IMP number
 */
void dup_IMP_node_state(IonIfBase& IF, int from, int to, GlobalData_t **localdata) {
  for (int  i = 0; i < NUM_IMP_DATA_TYPES; i++)
    if (localdata && localdata[i])
      localdata[i][to] = localdata[i][from];

  IF.for_each([&](IonIfBase& imp) {
    char **sv_list;
    int sv_list_size = imp.get_type().get_sv_list(&sv_list);

    for (int i = 0; i < sv_list_size; i++) {
      char *sv_name = sv_list[i];
      int sv_offset;
      int sv_size;
      SVgetfcn sv_get = imp.get_type().get_sv_offset(sv_name,  &sv_offset, &sv_size);
      if (sv_get == NULL) {
        throw std::runtime_error(std::string(__func__) + " error: " + sv_name + " is not a valid state variable for the " + imp.get_type().get_name() + " model.");
      }
      SVputfcn sv_put = getPutSV(sv_get);

      GlobalData_t sv_val = sv_get(imp, from, sv_offset);
      sv_put(imp, to, sv_offset, sv_val);
    }
    free(sv_list);
  });
}

}  // namespace limpet
