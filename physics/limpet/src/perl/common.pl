# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------


# printf format characters for variable types
%prchar = ( "Gatetype",     "g",
            "Real",         "g",
            "float",        "g",
            "char",         "c",
            "int",          "d",
            "double",       "f",
            "short",        "d",
		    "GlobalData_t", "g",
		    "long",         "d",
		    "bool",         "d" );
			
@vartypes = keys %prchar;
  
# scanf format characters for variable types
%scanchar = ( "Gatetype",     "f",
              "Real",         "lf",
              "float",        "f",
              "char",         "c",
              "int",          "d",
              "double",       "lf",
              "short",        "hd",
              "GlobalData_t", "f" ,
			  "long",         "ld",    
		      "bool",         "c"  );

# indices for sv datatypes 
%typeID =  (  "Gatetype",     0,
              "Real",         1,
              "float",        2,
              "char",         3,
              "int",          4,
              "double",       5,
              "short",        6,
              "GlobalData_t", 7,
		      "long",         8,
		      "bool",         9  );


sub readIMPs {
###########################################
# R: ($@IMPS, $@PLUGS,$@I_DATA_T)
#

  #get the directory this script lives in.
  my $dir = $0;
  $dir =~ s,/[^/]+$,,;

  my $filename = "../../models/imp_list.txt";
  open(FILE, $filename) or open(FILE, "$dir/$filename") or die "Can't open $dir/$filename for reading: $!";
  #(my @IMPS, @PLUGS, @I_DATA_T);
  while(my $line = <FILE>) {
    chomp($line);

    #remove comments.
    $line =~ s,#.*$,,;

    #remove leading/trailing whitespace.
    $line =~ s,^\s+,,;
    $line =~ s,\s+$,,;

    #skip blank lines.
    next if not $line;

    if ($line =~ /^model\s+(\w+)\s*(.*?)$/) {
      my $name = $1;
      my %parameters;
      foreach my $par (split(/\s+/, $2)) {
        $parameters{$par} = 1;
      }
      $generated = defined $parameters{"generated"};
      $plugin = defined $parameters{"plugin"};

      push @IMPS, $name if not $plugin;
      push @PLUGS, $name if $plugin;
    } elsif ($line =~ /^variable\s+(\w+)\s+(\w+)\s+(.*?)$/) {
      if ($1 eq "public") {
        my $varname = $2;
        push @I_DATA_T, $varname;
      }
    }
  }
  close(FILE);

  return (\@IMPS, \@PLUGS, \@I_DATA_T);
}

sub open_header {
#######################################
# Opens an IMP header file.
# created because the header files are no longer
# guaranteed to be in the current working directory.
  my ($imp) = @_;

  #get the directory this script lives in.
  my $dir = $0;
  $dir =~ s,/[^/]+$,,;
  
  #open and return the file
  open(FILE, "$imp.h") or open(FILE, "$dir/$imp.h") or die "Can't open $dir/$imp.h for reading: $!";
  return FILE;
}
