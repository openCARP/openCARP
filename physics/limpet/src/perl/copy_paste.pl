#!/usr/bin/perl -w
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------


use strict;


sub usage {
########################################333
#
#
  print <<EOF
Usage: $0 [-s] file

Finds sections labeled with 
---- BEGIN_SAMPLE_CUT ---
text
---- END_CUT ---

and inserts them after a line containing
INSERT SAMPLE

Each cut section must have a unique name, like SAMPLE above.

This tool can be run on files over and over again without changing the
results.  It is smart enough to realize its own pasting handiwork.

With the -s option, the tool strips the cut sections from the text.

EOF
;
    die join("\n", @_);
}

if (not scalar(@ARGV)) {
  usage("Need a filename to copy/paste.");
}


my $strip = 0;
my $file = shift @ARGV;
if ($file eq "-s") {
  $strip = 1;
  if (not scalar(@ARGV)) {
    usage("Need a filename to copy/paste.");
  }
  $file = shift @ARGV;
}

open(FILE, $file) or die "Can't open file $file for reading: $!\n";
my @lines = <FILE>;
close(FILE);

my $cut_name = "";
my %cuts;
if (not $strip) {
  foreach my $line (@lines) {
    if ($line =~ m,----\s*BEGIN_(\w+)_CUT\s*----,) {
      $cut_name = $1;
      $cuts{$cut_name} = "" if not defined($cuts{cut_name});
    }
    if ($cut_name) {
      $cuts{$cut_name} .= $line;
    }
    if ($line =~ m,----\s*END_CUT\s*----,) {
      $cut_name = "";
    }
  }
}

open(FILE, ">$file") or die "Can't open $file for writing: $!";

my $paste_name = "";
my $cutting = 0;
foreach my $line (@lines) {
  if ($paste_name) {
    if ($line =~ m,----\s*BEGIN_${paste_name}_CUT\s*----,) {
      $cutting = 1;
    }
    $paste_name = "";
  }
  
  if (not $cutting) {
    print FILE $line;
  } else {
    if ($line =~ m,----\s*END_CUT\s*----,) {
      $cutting = 0;
    }
  }
  
  if ($line =~ m,//\s*INSERT\s+(\w+),) {
    $paste_name = $1;
    if (not defined($cuts{$paste_name})) {
      if (not $strip) {
	print STDERR "Can't find cut name $paste_name!\n";
      }
    } else {
      print FILE $cuts{$paste_name};
    }
  }
}

close(FILE);
