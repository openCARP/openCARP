// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file MULTI_ION_IF.h
* @brief Define multiple ionic models to be used in different regions
* @author Edward Vigmond
* @version 
* @date 2019-10-25
*/

/** \page limpet  LIMPET: Library of IMPs (Ionic Model & Plug-ins) for Electrophysiological Theorization
 *
 * \image html devil-4.jpg
 *
 * \section purpose Purpose
 *          This purpose of this library is to create a
 *          a consistent interface for a set of efficiently implemented ionic
 *          models. The library is designed to be able to specify different
 *          regions within a
 *          tissue and assign different models within each region.
 *          Furthermore, plug-ins may be added on to basic ionic
 *          models to enhance functionality. For example, an
 *          electroporation channel is available as a plug-in.
 *
 *          The library tries to be as efficient as possible. It is
 *          also designed to  work on sequential machines, under OMP,
 *          under distributed systems using PETSc, or on GPUs.
 *
 * \section usage Usage
 *          Filling the appropriate data structures is outlined in
 *          MULTI_ION_IF.cc.                                     <br>
 *          An example of using the library is found in \e bench.cc.
 *
 * \section compilation Compilation
 *          To compile code that uses the IMP library, include the header
 *          \code
 *          #include "MULTI_ION_IF.h"
 *          \endcode
 *          and link the library
 *          \code
 *          gcc ... -limp -lm
 *          \endcode
 *
 * \section addingto Adding Ionic Models
 *
 *   To add a new ionic model or plugin (an IMP), one must firsty modify
 *   \c imp_list.txt. See the file for details. Many files are generated from this file.
 *
 *   When adding an IMP called \<IMP\>,
 *   the source code for the IMP must be in \<IMP\>.h and \<IMP\>.cc.
 *   Note, the IMP name must contain at least one uppercase character.
 *   The following functions must be declared:   <br>
 *   <TABLE>
 *   <TR><TD>
 *   <TT>initialize_sv_\<IMP\>()    </TT></TD><TD> allocate and initialize the state
 *   variable table entriesgengetopt
 *   </TD></TR>   <TR> <TD>
 *   <TT>initialize_params_\<IMP\>()</TT></TD><TD> set modifiable IMP parameters to
 *   defaults
 *   </TD></TR> <TR><TD>
 *   <TT>construct_tables_\<IMP\>() </TT></TD><TD> allocate memory for and fill lookup table
 *   </TD></TR> <TR> <TD>
 *   <TT>compute_\<IMP\>() </TT>         </TD><TD> compute desired quantities
 *   </TD></TR>  <TR><TD>
 *   <TT>destroy_\<IMP\>()      </TT></TD><TD> free IMP memory
 *   </TD></TR>
 *   </TABLE>
 *   and   <br>
 *   define 2 structures, one <TT>\<IMP\>_state</TT> which contains the
 *   state variables for a node, and <TT>\<IMP\>_Params</TT> which contains IMP specific
 *   parameters in \<IMP\>.h. A script called skeleton.pl will
 *   create the outline of the code for a new IMP.
 *
 *   IMPs may require data not intrinsically provided by the IMP.
 *   You must define a mask in \<IMP\>.h specifying what data must be passed in
 *   and another mask specifying what data are modified.
 *   The mask is OR'ed together from the IMP data type flags (see ION_IF.h)<br>
 *   <TT>#define \<IMP\>_REQDAT mask </TT>   <br>
 *   <TT>#define \<IMP\>_MODDAT mask</TT>    <br>
 *   The length of these data vectors must be equal to the total
 *   number of nodes even if not all nodes use all data types. Data
 *   passed like this typically signify interaction between IMPS and other
 *   procedures, beyond the scope of an IMP, in the program.
 *   Look at MBRDR.h and MBRDR.c for an example of a relatively simple IMP.
 *
 *   State variables of the parent ionic model may be accessed by plug-ins.
 *   A common naming scheme is necessary so that this interaction
 *   may be automated. The following variables are currently defined:
 *   <TABLE>
 *   <TR><TD>State variable</TD><TD>Meaning</TD><TD>Units</TD>
 *   </TR><TR>
 *   <TD>Ki</TD><TD>[K]<sub>i</sub></TD><TD>millimolar</TD>
 *   </TR><TR>
 *   <TD>Nai</TD><TD>[Na]<sub>i</sub></TD><TD>millimolar</TD>
 *   </TR><TR>
 *   <TD>Cai</TD><TD>[Ca]<sub>i</sub></TD><TD>micromolar</TD>
 *   </TR><TR>
 *   <TD>Cab</TD><TD>[Ca] bound to troponin</TD><TD>micromolar</TD>
 *   </TR><TR>
 *   <TD>CaRel</TD><TD>[Ca] in release compartment of SR</TD><TD>micromolar</TD>
 *   </TR><TR>
 *   <TD>CaUp</TD><TD>[Ca] in uptake compartment of SR</TD><TD>micromolar</TD>
 *   </TR><TR>
 *   <TD>ATP</TD><TD>[ATP]<sub>i</sub></TD><TD>millimolar</TD>
 *   </TR><TR>
 *   <TD>ADP</TD><TD>[ADP]<sub>i</sub></TD><TD>micromolar</TD>
 *   </TR>
 *   </TABLE>
 *
 *   Plug-ins access these state variables using
 *   two helper functions provided <br>
 *   \c get_sv_offset() <br>
 *   \c getPutSV() <br>
 */

#ifndef MULTI_ION_IF_H
#define MULTI_ION_IF_H

#include <ctime>
#include <cstdarg>
#include <cstddef>
#include <string>
#include <sys/resource.h>

#include "ION_IF.h"

#include "timer_utils.h"
#include "fem_types.h"
#include "sf_interface.h"

#define FARADAY 96485                   //!< Faraday's constant

namespace limpet {

typedef double Real;

static const unsigned int MIIF_Format = 1;               //!< version of dump format
static const char *Magic_MIIF_ID      = "Dump_MIIF_ID";  //!< string identifying dump file

/** \brief data structure to manage state variable file dumps

    stores all the information related to dumping sv's into files
 */
struct SV_DUMP {
  int                   active;
  int                   n;        //!< \#state variables we want to dump
  opencarp::FILE_SPEC*  hdls;     //!< array of file handles to sv output files
  char**                fn;       //!< array to store file names
  int*                  reg;      //!< array to store region ids
  char**                svnames;  //!< array to store sv names
  double                intv;     //!< time interval for sv dumps
  double                t_dump;   //!< next instant for sv dump
  int                   n_dumps;  //!< keep track of number of dumped time slices
  long                  nwr;      //!< keep track of number of written tokens
  int*                  offset;   //!< offsets into structure for SV
  int*                  size;     //!< sizes of SV to dump
  int*                  dtype;    //!< data type
  int*                  num;      //!< number of nodes
  void**                svtab;    //!< state variable tables
  size_t*               svsize;   //!< state variable sizes
  int*                  dlo_vs;
};

/** \brief data structure to manage trace dumps.  Should eventually be
 * combined with the state variable dumps, but I'm keeping it separate
 * for now for my own sanity.
 */
struct Trace_Info {
  bool found;      //!< found on this node
  bool ignored;    //!< globally not found
  int  node_idx;   //!< local node number
  int  region;
  opencarp::FILE_SPEC file;
};

/** hold different ionic model information
 */

class MULTI_IF {

 public:
  std::string              name;                          //!< name for MIIF region
  bool                     doppel;                        //!< is this a shallow clone?
  int*                     N_Nodes;                       //!< \#nodes for each IMP
  int**                    NodeLists;                     //!< local partitioned  node lists for each IMP stored
  std::vector<IonIfBase*>      IIF;                           //!< array of IIF's
  SV_DUMP                  svd;                           //!< state variable dump
  GlobalData_t*            procdata[NUM_IMP_DATA_TYPES];  //!< data for this processor
  GlobalData_t***          ldata;                         //!< data local to each IMP
  bool*                    contiguous;                    //!< whether a region is contiguously numbered
  Trace_Info*              trace_info;                    //!< Information about traces
  bool                     extUpdateVm;                   //!< flag indicating update function for Vm
  int*                     numplugs;                      //!< number of plugins for each region
  int                      numNode;                       //!< local number of nodes
  int                      N_IIF;                         //!< how many different IIF's
  IonTypeList              iontypes;                      //!< type for each region
  std::vector<Target>      targets;                       //!< target for each region
  IIF_Mask_t*              IIFmask;                       //!< region for each node
  std::vector<IonTypeList> plugtypes;                     //!< plugins types for each region
  opencarp::sf_vec*  gdata[NUM_IMP_DATA_TYPES];     //!< data used by all IMPs
  opencarp::FILE_SPEC      logger;

  double dt;          //!< time step (ms)
  int numSubDt;       //!< number of sub-dt time steps

#ifdef HDF5
  hid_t file;            //!< file ID for miif
#endif  // ifdef HDF5

  MULTI_IF() : doppel(false), N_Nodes(NULL), NodeLists(NULL), IIF({}), extUpdateVm(false), logger(NULL)
  {
    zero_data();
  }

  ~MULTI_IF() {}

  void zero_data() {
    name = "";
    N_Nodes = NULL;
    NodeLists = NULL;
    IIF = {};
    STRUCT_ZERO(svd);
    STRUCT_ZERO(procdata);
    ldata = NULL;
    contiguous = NULL;
    trace_info = NULL;
    numplugs = NULL;
    iontypes = {};
    IIFmask = NULL;
    plugtypes = {};
    STRUCT_ZERO(gdata);
  }

  void initialize_MIIF();
  void initialize_currents(double, int);

  void compute_ionic_current(bool flag_send = 1, bool flag_receive = 1);  // pMIIF

  void free_MIIF();  // pMIIF

  void sv_dump_add(int, const IonType&, int, int, int, const char *, const char *);
  int  sv_dump_add_by_name(int, char *, char *, char *, char *);
  void sv_dump_add_by_name_list(int, char *, char *, char *, char *,
                                char *, double, double);
  int  dump_svs(opencarp::base_timer *);
  void close_svs_dumps();
  void dump_luts_MIIF(bool);

  void  dump_state(char *, float, opencarp::mesh_t gid, bool, unsigned int);
  void  transmem_stim_species(float, const char *, float, int *, int);
  float restore_state(const char *, opencarp::mesh_t gid, bool);
  void  releaseRealData();
  void  releaseRealDataDuringInit();
  void  getRealData();
  int   adjust_MIIF_variables(const char* variable,
                              const SF::vector<SF_int> & indices,
                              const SF::vector<SF_real> & values);
  void MIIF_change_dt(double);
  bool use_stretch();

  void *thread_initialize_MIIF();
};

extern opencarp::FILE_SPEC _nc_logf;

void doppel_MIIF(MULTI_IF *, MULTI_IF *);
void doppel_update(MULTI_IF *, MULTI_IF *);
void free_doppel(MULTI_IF *);
void dup_IMP_node_state(IonIfBase&, int, int, GlobalData_t **ldata);

void* get_IIF_plugparam(IonIfBase&, int);
int   get_plug_flag(char *, int *, IonTypeList&);
int   IMPdataLabel2Index(const char *);
char* tokstr_r(char *s1, const char *s2, char **lasts);

void open_trace(MULTI_IF *MIIF, int n_traceNodes, int *traceNodes, int *label, opencarp::sf_mesh* imesh);
void dump_trace(MULTI_IF *MIIF, limpet::Real time);


/** test if a global data vector is used
 *
 * \\param I IMP
 * \\param F flag
 */
#define USED_DAT(I, F) ( (I)->get_reqdat()&F || (I)->get_moddat()&F)

/** get the parameter structure for a plug-in
 *
 * This is a wrapper for get_IIF_plugparam() that casts the void pointer
 * to the proper type
 *
 * \param M MIIF
 * \param R integer specifying region
 * \param P plug-in
 *
 * \return pointer to parameter structure cast to proper type
 */
#define get_plug_params(M, R, P) (P ## _Params *)get_IIF_plugparam(*(M.IIF[R]), P ## _ID)


/** add a variable to the list of state variables to be dumped
 *
 *  This is a fancy wrapper for sv_dump_add()
 *
 * \param I MIIF
 * \param R integer region identifier
 * \param T IMP name
 * \param M member name
 */
#define SVD_add(I, R, T, M) {T ## _state *sv; int dtype = 0; sv_dump_add(&I, R,                                          \
                                                                         T ## _ID, offsetof(T ## _state,                 \
                                                                                            M),                          \
                                                                         sizeof(sv->M), dtype, # R "." # T "." # M, # T, \
                                                                         # R); }
}  // namespace limpet

#endif  // MULTI_ION_IF_H
