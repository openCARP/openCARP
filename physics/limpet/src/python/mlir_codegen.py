# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2022 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from gettext import find

import math
import mlir.ir as ir
import mlir.dialects as dialects
import mlir.dialects.arith
import mlir.dialects.func
import mlir.dialects.math
import mlir.dialects.memref
import mlir.dialects.scf
import mlir.dialects.vector

from abc import ABC, abstractmethod

from parser import AST
from im import Var, IonicModel, order
from unit import log_diff

from sys import version_info

if version_info >= (2, 4, 0):
    Set = set
else:
    from sets import Set

import os


def get_variable_name(var):
    if (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external'):
        return "sv->%s" % var.name
    elif var.is_a('param'):
        return "p->%s" % var.name
    elif var.is_a('external') and var.is_a('regional'):
        return "region->%s" % var.info('external')
    elif var.is_a('lookup'):
        return "%s_row[%s_idx]" % (var.info('lookup').name, var.name)
    else:
        return var.name

def is_integer_type(operand_type):
    return ir.IntegerType.isinstance(operand_type)


def is_floating_point_type(operand_type):
    if ir.F64Type.isinstance(operand_type):
        return True
    if ir.F32Type.isinstance(operand_type):
        return True
    if ir.F16Type.isinstance(operand_type):
        return True
    if ir.BF16Type.isinstance(operand_type):
        return True
    return False


def is_scalar_type(operand_type):
    return is_integer_type(operand_type) or is_floating_point_type(operand_type)


# Create a new type is based upon the shape found in operand_type with the base_type as its primitive type
def get_predicate_type(operand_type, base_type, is_scalar):
    if not is_scalar:
        return ir.VectorType.get(ir.ShapedType(operand_type).shape, base_type)
    else:
        return base_type

def get_predicate_attr(predicate, is_integer):
    predicate_dict_float = {
        "eq": 1, "sgt": 2, "sge": 3, "slt": 4, "sle": 5,
        "ne": 6, "ord": 7, "ueq": 8, "ugt": 9, "uge": 10,
        "ult": 11, "ule": 12, "une": 13, "uno": 14
    }
    predicate_dict_int = {
        "eq": 0, "ne": 1, "slt": 2, "sle": 3, "sgt": 4,
        "sge": 5, "ult": 6, "ule": 7, "ugt": 8, "uge": 9
    }
    if is_integer:
        predicate_dict = predicate_dict_int
    else:
        predicate_dict = predicate_dict_float
    value = predicate_dict[predicate]
    return ir.IntegerAttr.get(ir.IntegerType.get_signless(64), value)


def create_cmp(predicate, lhs, rhs):
    operand_type = rhs.result.type
    is_scalar = is_scalar_type(operand_type)
    is_integer = is_integer_type(operand_type)

    # Create an ir.IntegerAttr to represent a predicate. Values for each predicate are according to MLIR.
    predicate_attr = get_predicate_attr(predicate, is_integer)

    # Create a vector of i1 to store the results of comparisons
    predicate_type = ir.IntegerType.get_signless(1)
    predicate_type = get_predicate_type(operand_type, predicate_type, is_scalar)
    if not is_integer:
        return dialects.arith.CmpFOp(predicate_attr, lhs, rhs)
    else:
        return dialects.arith.CmpIOp(predicate_attr, lhs, rhs)


class MlirAst():
    def __init__(self, ast, is_const=False):
        self.ast = ast
        self.is_const = is_const

    def get_constant_value(self, callback_function, dict, children):

        type = self.ast.type

        if type == 'const':
            result = float(self.ast.__getitem__(0))
        elif type == 'var':
            result = callback_function(self.ast.__getitem__(0), dict)
        elif type == '.+.':
            result = float(children[0]) + float(children[1])
        elif type == '.-.':
            result = float(children[0]) - float(children[1])
        elif type == '.*.':
            result = float(children[0]) * float(children[1])
        elif type == './.':
            result = float(children[0]) / float(children[1])
        elif type == '-.':
            result = float(-float(children[0]))
        elif type == 'ifelse':
            cond = children[0]
            if cond:
                result = children[1]
            else:
                result = children[2]
        elif type == '.==.':
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond == rhs_cond:
                result = 1.0
            else:
                result = 0.0
        elif type == '.!=.':
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond != rhs_cond:
                result = 1.0
            else:
                result = 0.0
        elif type == ".<=.":
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond <= rhs_cond:
                result = 1.0
            else:
                result = 0.0
        elif type == ".>=.":
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond >= rhs_cond:
                result = 1.0
            else:
                result = 0.0
        elif type == ".>.":
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond > rhs_cond:
                result = 1.0
            else:
                result = 0.0
        elif type == '.<.':
            lhs_cond = children[0]
            rhs_cond = children[1]

            if lhs_cond < rhs_cond:
                result = 1.0
            else:
                result = 0.0
        else:
            def math_square(x):
                return x*x
            def math_cube(x):
                return x*x*x
            function_dict = {
                "pow": math.pow,
                "sqrt": math.sqrt,
                "floor": math.floor,
                "ceil": math.ceil,
                "exp": math.exp,
                "expm1": math.expm1,
                "log": math.log,
                "log2": math.log2,
                "log10": math.log10,
                "log1p": math.log1p,
                "fabs:": math.fabs,
                "square": math_square,
                "cube": math_cube
            }
            function_name = self.ast.__getitem__(0)
            if not function_name in function_dict.keys():
                print("Function " + function_name + " not implemented")
                assert False
            result = function_dict[function_name](*children)
        return result

    def toMLIR(self, callback_function, function_save_to_dict, value_dict=None, variable_printer=None):

        children = [MlirAst(child,
                            self.is_const).toMLIR(callback_function, function_save_to_dict, value_dict,
                                                  variable_printer) for child in self.ast.children()]

        if self.is_const:
            return self.get_constant_value(callback_function, value_dict, children)

        def create_binop(func, lhs, rhs):
            return func(lhs[1], rhs[1])

        def create_unaryop(func, rhs):
            return func(rhs[1])

        def create_if(cond, if_true, if_false):
            return dialects.arith.SelectOp(cond[1], if_true[1], if_false[1])

        def create_function(function_name, children):

            def create_heav(value):

                lhs_cond = callback_function(value, value_dict, variable_printer)
                rhs_cond = callback_function("0", value_dict, variable_printer)

                if_true = callback_function("0", value_dict, variable_printer)
                if_false = callback_function("1", value_dict, variable_printer)

                cond = create_cmp("slt", lhs_cond[1], rhs_cond[1])
                return create_if((0, cond), if_true, if_false)

            def create_sign(value):

                lhs_cond = callback_function(value, value_dict, variable_printer)
                rhs_cond = callback_function("0", value_dict, variable_printer)

                if_true = callback_function("-1", value_dict, variable_printer)
                if_false = callback_function("1", value_dict, variable_printer)

                cond = create_cmp("slt", lhs_cond[1], rhs_cond[1])
                return create_if((0, cond), if_true, if_false)

            def create_square(value):
                mlir_value = callback_function(value, value_dict, variable_printer)
                return create_binop(dialects.arith.MulFOp, mlir_value, mlir_value)

            def create_cube(value):
                mlir_value = callback_function(value, value_dict, variable_printer)
                return create_binop(dialects.arith.MulFOp, ("", create_square(value)), mlir_value)

            def create_call(callee, params):
                return dialects.func.CallOp(callee, params)

            predefined_functions = {
                'heav': create_heav,
                'sign': create_sign,
                'square': create_square,
                'cube': create_cube
            }

            math_functions = {
                "fabs": dialects.math.AbsFOp,
                "atan2": dialects.math.Atan2Op,
                "atan": dialects.math.AtanOp,
                "ceil": dialects.math.CeilOp,
                "copysign": dialects.math.CopySignOp,
                "cos": dialects.math.CosOp,
                "erf": dialects.math.ErfOp,
                "exp2": dialects.math.Exp2Op,
                "expm1": dialects.math.ExpM1Op,
                "exp": dialects.math.ExpOp,
                "floor": dialects.math.FloorOp,
                "fma": dialects.math.FmaOp,
                "log10": dialects.math.Log10Op,
                "log1p": dialects.math.Log1pOp,
                "log2": dialects.math.Log2Op,
                "log": dialects.math.LogOp,
                "pow": dialects.math.PowFOp,
                "rsqrt": dialects.math.RsqrtOp,
                "sin": dialects.math.SinOp,
                "sqrt": dialects.math.SqrtOp,
                "tanh": dialects.math.TanhOp,
                "max": dialects.arith.MaximumFOp
            }

            if function_name in predefined_functions.keys():

                for child in self.ast.children():
                    var = child.args[0]
                    if not isinstance(var, Var):
                        var = "".join(child[0] for child in children)
                value = predefined_functions[function_name](var)
            elif function_name in math_functions.keys():
                if function_name in ["pow", "atan2", "copysign", "max"]:
                    value = math_functions[function_name](children[0][1], children[1][1])
                elif function_name == "fma":
                    value = math_functions[function_name](children[0][1], children[1][1], children[2][1])
                else:
                    value = math_functions[function_name](children[0][1])
            else:
                operand_type = children[0][1].result.type

                # Special callback with function options
                # Let the callback create a new function
                func = callback_function("", value_dict, variable_printer, [function_name, ([operand_type],
                                                                                            [operand_type])])
                value = create_call(func, [children[0][1].result])
                print("WARNING: Calling function " + function_name + ". Is this a math function?")
            return value

        t = self.ast.type

        if t == '.+.':
            name = '+'.join(child[0] for child in children)
            if isinstance(children[0][1], dialects.arith.MulFOp):
                mul_op = children[0][1]
                value = dialects.math.FmaOp(mul_op.lhs, mul_op.rhs, children[1][1])
            elif isinstance(children[1][1], dialects.arith.MulFOp):
                mul_op = children[1][1]
                value = dialects.math.FmaOp(mul_op.lhs, mul_op.rhs, children[0][1])
            else:
                value = create_binop(dialects.arith.AddFOp, children[0], children[1])
        elif t == '.-.':
            name = children[0][0] + '-(' + children[1][0] + ')'  # to prevent a - -3 = a--3, different c semantics .
            value = create_binop(dialects.arith.SubFOp, children[0], children[1])
        elif t == '.*.':
            name = '*'.join(child[0] for child in children)
            value = create_binop(dialects.arith.MulFOp, children[0], children[1])
        elif t == './.':
            name = '/'.join(child[0] for child in children)
            value = create_binop(dialects.arith.DivFOp, children[0], children[1])
        elif t == '.|.':
            assert (False)
        elif t == 'paren':
            assert (False)
        elif t == '-.':
            name = "-" + ''.join(children[0][0])
            value = create_unaryop(dialects.arith.NegFOp, children[0])
        elif t == 'func':
            name = ''.join((self.ast[0], '(', ','.join(child[0] for child in children), ')'))
            value = create_function(self.ast[0], children)
        elif t == 'func_na':
            assert False
        elif t == 'ifelse':
            name = '%s ? %s : %s' % tuple(child[0] for child in children)
            value = create_if(children[0], children[1], children[2])
        elif t == '.==.':
            name = "==".join(child[0] for child in children)
            value = create_cmp("eq", children[0][1], children[1][1])
        elif t == '.!=.':
            name = "!=".join(child[0] for child in children)
            value = create_cmp("ne", children[0][1], children[1][1])
        elif t == ".<=.":
            name = "<=".join(child[0] for child in children)
            value = create_cmp("sle", children[0][1], children[1][1])
        elif t == ".>=.":
            name = ">=".join(child[0] for child in children)
            value = create_cmp("sge", children[0][1], children[1][1])
        elif t == ".>.":
            name = ">".join(child[0] for child in children)
            value = create_cmp("sgt", children[0][1], children[1][1])
        elif t == ".<.":
            name = "<".join(child[0] for child in children)
            value = create_cmp("slt", children[0][1], children[1][1])
        elif t == ".and.":
            name = "&&".join(child[0] for child in children)
            value = create_binop(dialects.arith.AndIOp, children[0], children[1])
        elif t == ".or.":
            name = "||".join(child[0] for child in children)
            value = create_binop(dialects.arith.OrIOp, children[0], children[1])
        elif t == "not.":
            name = "!" + ''.join(child[0] for child in children)
            assert False
        elif t == 'var':
            return callback_function(self.ast.__getitem__(0), value_dict, variable_printer)
        elif t == 'const':
            return callback_function(self.ast.__getitem__(0), value_dict)
        elif t == 'logical_const':
            return self.ast.__getitem__(0)
        else:
            return "Blah " + str(self.ast.type) + str(self.ast.args)
        return function_save_to_dict(value_dict, '(' + name + ')', value)


# Define a printer type to properly select which printer to use
# Possible options:
#   Empty: uses the Var itself
#   Lhs_printer: uses the lhs_format_func function
#   Intermediate: creates an internal intermediate printer in create_internal_scope_for_integration_method
#   Diff: uses diff_ as a prefix of the variable name
from enum import Enum


class PrinterType(Enum):
    EMPTY = 1
    LHS_PRINTER = 2
    INTERMEDIATE = 3
    DIFF = 4


class IntermediateVariablePrinter:
    def __init__(self, f, vars_to_wrap):
        self.f = f
        self.vars_to_wrap = vars_to_wrap

    def __call__(self, var):
        if var in self.vars_to_wrap:
            return "sv_intermediate_%s" % var.name
        else:
            return self.f(var)


class MlirCodegen(object):

    def __init__(self, ionic_model, model_name, filename, function_name, symbol_table, params_dict, region_dict,
                 ast_equations, regional_constants, state_dict, enums_dicts, tables, function_pointer_info,
                 lookup_variables, external_variables, external_private_variables, external_units,
                 needs_update_variables, all_lookup, external_update_variables, good_vars, update_inputs,
                 fe_targets, rush_larsen_targets, rk2_targets, rk4_targets, sundnes_half_targets, sundnes,
                 markov_be_targets, markov_be_solve, rosenbrock, rosenbrock_nodal_req, rosenbrock_partial_targets,
                 rosenbrock_diff_targets, rosenbrock_dict, global_constants, lhs_format_func, enable_store_opt,
                 enable_data_layout_opt, lookup_variables_order):

        # Store ionic model
        self.ionic_model = ionic_model
        self.model_name = model_name

        # Create an output file
        self.output_file = open(os.path.join(filename), "w")

        # AST for each equation defined in the model
        self.ast_equations = ast_equations

        # Store LUTs and cell-specific variables
        self.lookup_variables = lookup_variables
        self.external_variables = external_variables
        self.external_update_variables = external_update_variables
        self.external_private_variables = external_private_variables
        self.external_units = external_units
        self.lookup_variables_order = lookup_variables_order

        # Store dictionaries for the offset of parameters and state variables
        # They will be used to get the right offset of each variable inside the data structure, or enumerator
        self.params_dict = params_dict
        self.state_dict = state_dict
        self.enums_dicts = enums_dicts
        self.region_dict = region_dict
        self.function_pointer_info = function_pointer_info

        # List of lookup table names
        self.tables = tables

        # Constant that depend on parameters
        self.regional_constants = regional_constants

        # Constant values
        self.global_constants = global_constants

        # Function name
        self.function_name = function_name

        # Dictionary for MLIR Ops that use names as keys
        self.mlir_values_dict = dict()
        # Dictionary for constants
        self.mlir_constant_dict = dict()

        self.symbol_table = symbol_table
        self.all_lookup = all_lookup

        # Multiple variable definitions collected by the parser
        self.needs_update_variables = needs_update_variables
        self.good_vars = good_vars
        self.update_inputs = update_inputs
        self.fe_targets = fe_targets
        self.rush_larsen_targets = rush_larsen_targets
        self.rk2_targets = rk2_targets
        self.rk4_targets = rk4_targets
        self.sundnes_half_targets = sundnes_half_targets
        self.sundnes = sundnes
        self.markov_be_targets = markov_be_targets
        self.markov_be_solve = markov_be_solve

        self.enable_rosenbrock = False
        self.rosenbrock = rosenbrock
        self.rosenbrock_nodal_req = rosenbrock_nodal_req
        self.rosenbrock_partial_targets = rosenbrock_partial_targets
        self.rosenbrock_diff_targets = rosenbrock_diff_targets
        self.rosenbrock_dict = rosenbrock_dict

        self.lhs_format_func = lhs_format_func

        # Cache for helper functions
        self.function_dict = dict()

        # Enables a optimization of store to the struct
        self.enable_store_opt = enable_store_opt

        # Enables an optimization of data layout
        self.enable_data_layout_opt = enable_data_layout_opt

        # Define constants
        self.parent_table_address = "__parent_table_address"
        self.parent_table_size = "__parent_table_size"

        # Construct function arguments. Note that we cannot get them directly from func.arguments we make some changes
        self.func_args = []

    ######################################################
    # Two helper functions to track MLIR values:
    # cache_value_globally tracks values for the whole scope of the function
    ######################################################
    def cache_value_globally(self, var_name, value):
        self.mlir_values_dict[var_name] = value
        return var_name, value

    # cache_value needs an additional parameter to track values
    # in a local dictionary, a.k.a, an internal code scope.
    # Typically because names of tracked values may repeat from the global dict,
    # our codegen must be able to prioritize internal scopes rather than
    # the global one
    # This will happen on scopes of RK2, and RK4 integration methods, for example
    def cache_value(self, value_dict, var_name, value):
        if value_dict is None:
            return self.cache_value_globally(var_name, value)
        value_dict[var_name] = value
        return var_name, value

    ######################################################

    def cache_constant_value(self, var_name, value):
        self.mlir_constant_dict[var_name] = value
        return var_name, value

    def get_step_size(self, type):
        return dialects.arith.ConstantOp(type, self.get_computation_size())

    @abstractmethod
    def get_rosenbrock_functions(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def get_rosenbrock_function_names(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_rosenbrock_stepx(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def get_thread_id(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def get_computation_size(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_pointer_cast_operation(self, value, offset_index):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_conditional_load_store(self, operand_type, base_address, offset, size, mask, pass_thru, is_store, offset_int=None, is_sv=False):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def get_maxi_value(self, value):
        # Children classes may implement this function
        return value

    @abstractmethod
    def get_maxf_value(self, value):
        # Children classes may implement this function
        return value

    @abstractmethod
    def get_adjusted_value(self, type, value):
        # Children classes may implement this function
        return value

    @abstractmethod
    def get_type(self, base_type):
        # Children classes may implement this function.
        return base_type

    @abstractmethod
    def create_integer_constant(self, element_type, value):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_floating_point_constant(self, element_type, value):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def get_lut_function_name(self):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_helper_functions(self):
        raise ValueError("Abstract class cannot implement function")

    # Create a list of parameter types for the MLIR compute function of the ionic model
    def create_parameter_types(self):

        # Store the types of function parameters.
        # The order is as follows : (start_cell_number, end_cell_number, "IONIC Model Parameters"[ptr],
        #                             "dt"[f64"], "t"[f64], "Param-dep. Consts"[f64, ...],
        #                            "Cell state pointer"[ptr], "External Vars"[ptr, ...], "LUTs"[ptr, ...]
        func_param_types = []

        # Create a list of parameter strings
        params = []

        def create_parameter(type, name):
            func_param_types.append(type)
            params.append(name)

        # IONIC Model Parameters, dt, and t, and i
        i32 = ir.IntegerType.get_signless(32)
        f64 = ir.F64Type.get()
        vec_1xi8_ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        create_parameter(i32, "start_cell_number")
        create_parameter(i32, "end_cell_number")
        create_parameter(vec_1xi8_ptr, "p")
        create_parameter(vec_1xi8_ptr, "region")
        create_parameter(f64, "dt")
        create_parameter(f64, "t")

        # Cell state pointer and its size
        i8_ptr = ir.ShapedType(ir.Type.parse("memref<8xi8>", self.ctx))
        create_parameter(i8_ptr, "sv")

        # Param-dep. Consts
        [create_parameter(f64, i.name) for i in self.regional_constants]

        # External Vars and its size
        for i in self.external_variables:
            create_parameter(i8_ptr, i.name)

        # LUT addresses
        [create_parameter(vec_1xi8_ptr, "table_" + i) for i in self.tables]

        # LUT Allocations
        [create_parameter(vec_1xi8_ptr, "lut_alloc_" + i) for i in self.lookup_variables_order]

        # Add function pointer boolean variables
        if self.function_pointer_info:
            create_parameter(i8_ptr, self.parent_table_address)
            create_parameter(i32, self.parent_table_size)
            for i, j, k in self.function_pointer_info.values():
                create_parameter(i32, i)
                create_parameter(i32, j)
                create_parameter(i32, k)

        # Rosenbrock method needs 3 information:
        # (1) The size of the struct, in order to allow stride of the number of threads of the machine
        # (2) A pointer to the beginning of the struct
        # (3) A pointer to the first value to be used by the method address,
        #     this value is either inside Regional Constants (RC) or a Nodal Req (NR).
        if len(self.rosenbrock):
            create_parameter(i32, "ion_private_size")
            create_parameter(i8_ptr, "ion_private_ptr")
            create_parameter(i32, "ion_private_first_value")

        return func_param_types, params

    @abstractmethod
    def create_contiguous_load(self, base_address, operand_type, offset, is_sv=False):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_non_contiguous_load(self, base_address, operand_type, offset, size, is_sv=False):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_splat_load(self, base_address, operand_type, offset, is_sv=False):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_contiguous_store(self, base_address, store_value, operand_type, offset):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def create_non_contiguous_store(self, base_address, store_value, operand_type, offset, size):
        raise ValueError("Abstract class cannot implement function")

    @abstractmethod
    def initialize_markov_be(self, markov_values, current_bb, get_variable_name_func):
        # There is no ExecuteRegion, so return None
        # Since current_bb already has a terminator, return an InsertionPoint before it
        return None, ir.InsertionPoint.at_block_terminator(current_bb)

    @abstractmethod
    def finalize_markov_be(self, current_bb, markov_values, get_variable_name_func):
        return

    def create_load(self, base_address, operand_type, offset, size, is_contiguous, is_splat=False, is_sv=False):
        if is_contiguous:
            return self.create_contiguous_load(base_address, operand_type, offset, is_sv)
        elif not is_contiguous and not is_splat:
            return self.create_non_contiguous_load(base_address, operand_type, offset, size, is_sv)
        else:
            return self.create_splat_load(base_address, operand_type, offset, is_sv)

    def create_store(self, base_address, store_value, type, offset, size, is_contiguous, is_sv=False):
        if is_contiguous:
            self.create_contiguous_store(base_address, store_value, type, offset, is_sv)
        else:
            self.create_non_contiguous_store(base_address, store_value, type, offset, size, is_sv)

    # Create a new constant
    def get_mlir_constant(self, var):

        if var in self.mlir_values_dict:
            return self.mlir_values_dict[var]

        const_value = self.create_floating_point_constant(ir.F64Type.get(), float(var))
        self.cache_value_globally(var, const_value)
        return const_value

    def get_global_constant(self, var, var_name):

        if var_name in self.mlir_constant_dict:
            return self.mlir_constant_dict[var_name]

        eqn = self.ast_equations[var]
        mlir_ast = MlirAst(eqn.rhs, True)
        value = mlir_ast.toMLIR(self.get_global_constant, self.cache_value)

        self.cache_constant_value(var.name, value)
        return value

    def create_mask(self, value, is_inverted=False):

        i1 = ir.IntegerType.get_signless(1)

        # First truncate value of type i32 to i1
        trunc_value = dialects.arith.TruncIOp(i1, value)

        if is_inverted:
            one = dialects.arith.ConstantOp(i1, 1)
            trunc_value = dialects.arith.XOrIOp(trunc_value, one)

        # and create the mask
        mask = self.get_adjusted_value(i1, trunc_value)
        return mask

    def create_conditional_load_store_call(self, function_exists, operand_type, base_addr, offset, struct_size,
                                           pass_thru_or_stored_value, is_store, is_inverted_mask, offset_int=None, is_sv=False):
        mask = self.create_mask(function_exists, is_inverted_mask)
        value = self.create_conditional_load_store(operand_type, base_addr, offset, struct_size, mask.result,
                                                   pass_thru_or_stored_value, is_store, offset_int, is_sv)
        return value

    def create_conditional_load_store_to_parent(self, var_name, pass_thru_or_stored_value, operand_type, is_store=False,
                                                has_inverted_mask=False):

        assert (var_name in self.function_pointer_info)
        assert (self.parent_table_address in self.params)
        assert (self.parent_table_size in self.params)

        parent_tab_addr = self.func_args[self.params.index(self.parent_table_address)]
        parent_table_size = self.func_args[self.params.index(self.parent_table_size)]
        offset, getfcn_exists, putfcn_exists = self.function_pointer_info[var_name]
        offset = self.func_args[self.params.index(offset)]

        if not is_store:
            function_exists = getfcn_exists
        else:
            function_exists = putfcn_exists
        function_exists = self.func_args[self.params.index(function_exists)]

        # TableOffset = TableSize * CellNumber
        cell_number_index_i32 = dialects.arith.IndexCastOp(ir.IntegerType.get_signless(32), self.cell_number_index)
        table_offset = dialects.arith.MulIOp(parent_table_size, cell_number_index_i32)
        offset = dialects.arith.AddIOp(offset, table_offset)
        value = self.create_conditional_load_store_call(function_exists, operand_type, parent_tab_addr, offset,
                                                        parent_table_size, pass_thru_or_stored_value,
                                                        is_store, has_inverted_mask)
        return value

    # Retrieve different variable information:
    #   - Variable Name
    #   - Prefix (for structs)
    #   - Type (f32 or f64)
    #   - Offset and size (of struct)
    # Values are retrieved from offset dictionaries that are collected in the limpet_fe.py file
    def get_variable_info(self, var, params_dict, state_dict, region_dict, enums_dict, enable_data_layout_opt,
                          enable_rosenbrock = False, variable_printer=None):
        is_splat = False
        hoist_from_loop = False
        if not isinstance(var, Var):
            var_name = var
            prefix = ""
            type, offset = "f64", 0
            size = 0
            is_contiguous = True
        elif (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external'):
            # State variables are usually seen as struct types, except for situations
            # where intermediate values are created in an internal scope (see codegen for RK2, and RK4 integration methods)
            if variable_printer is None and not enable_rosenbrock:
                var_name = "sv->%s" % var.name
                prefix = "sv"
                type, offset = state_dict[var.name]
                _, size = state_dict["size"]
                if enable_data_layout_opt:
                    is_contiguous = True
                else:
                    is_contiguous = False

            elif enable_rosenbrock:
                var_name, prefix, type, offset, size, is_contiguous = self.get_rosenbrock_info(var)
            else:
                var_name = variable_printer(var)
                prefix = ""
                type, offset = "f64", 0
                size = 0
                is_contiguous = True
        elif var.is_a('param'):
            # Splat is only True when this variable is a parameter of the ionic model.
            # This means that we must generate a new function call to a function that splats (or broadcast)
            # one value read from memory (function 'load_single_value_struct_to_vector_8xf64')
            is_splat = True
            var_name = "p->%s" % var.name
            prefix = "p"
            type, offset = params_dict[var.name]
            _, size = params_dict["size"]
            is_contiguous = False
            hoist_from_loop = True
        elif var.is_a('external') and var.is_a('regional'):
            var_name = "region->%s" % var.info('external')
            prefix = "region"
            type, offset = region_dict[var.name]
            _, size = region_dict["size"]
            is_contiguous = False
            hoist_from_loop = True
        elif var.is_a('lookup'):
            prefix = var.info('lookup').name
            dict = enums_dict[prefix]
            var_name = "%s_row[%s_idx]" % (prefix, var.name)
            # Add "lut_" to prefix to avoid duplication with a regular variable
            prefix = "lut_" + prefix
            type, offset = dict[var.name]
            _, size = dict["size"]
            if enable_data_layout_opt:
                is_contiguous = False
            else:
                is_contiguous = False
        elif (var.is_a('state') or var.is_a('nodal')) and var.name in state_dict:
            var_name = var.name
            prefix = "sv"
            type, offset = state_dict[var.name]
            _, size = state_dict["size"]
            if enable_data_layout_opt:
                is_contiguous = True
            else:
                is_contiguous = False
        elif enable_rosenbrock:
            var_name, prefix, type, offset, size, is_contiguous = self.get_rosenbrock_info(var)
        else:
            var_name = var.name
            prefix = ""
            type, offset = "f64", 0
            size = 0
            is_contiguous = True
        return var_name, prefix, type, offset, size, is_contiguous, is_splat, hoist_from_loop

    # Create a new MLIR Value
    # This function returns a tuple of (VarName, MLIR Value) and adds an entry to the dictionary of MLIR values
    def get_mlir_value(self, var, value_dict=None, variable_printer=None, function_options=""):

        # Set dicts properly in the order they need to be consulted
        if value_dict:
            value_dicts = [value_dict, self.mlir_values_dict]
        else:
            value_dict = self.mlir_values_dict
            value_dicts = [self.mlir_values_dict]

        # Simple helper functions to consult dicts in order
        def get_value_from_dicts(name):
            for dictionary in value_dicts:
                if name in dictionary.keys():
                    return dictionary[name]
            return None

        # Handler for variables that are fields of a C/C++ struct from the ionic model
        def handle_variable_in_struct(var, struct_name, type, offset, size, is_contiguous, is_splat, hoist_from_loop):
            # We must create a new value that essentially comes from a C/C++ struct defined in the compute function
            # Variable is either a parameter, a region variable or coming from a lookup table
            if struct_name in self.params:
                index = self.params.index(struct_name)
                is_sv = False
                if struct_name == 'sv':
                    is_sv = True

                if hoist_from_loop:
                    with ir.InsertionPoint(self.loop):
                        value = self.create_load(self.func_args[index], type, offset, size, is_contiguous, is_splat, is_sv)
                    with ir.InsertionPoint(self.bb):
                        pass
                else:
                    value = self.create_load(self.func_args[index], type, offset, size, is_contiguous, is_splat, is_sv)

                # If this is inside self.external_private_variables, we need to do some more work
                # otherwise, our work is done
                if var in self.external_private_variables:
                    value = self.create_conditional_load_store_to_parent(var_name, value, type)
            else:
                value = get_value_from_dicts(struct_name)
                if value is not None:
                    value = self.create_load(value, type, offset, size, is_contiguous)
                else:
                    # Otherwise, it comes from a previously created node. Remove lut_ and get retrieve this value.
                    value_str = struct_name.split("lut_")[1]

                    # Create a new lut call and its loaded
                    lut_ptr = self.create_lut_call(var, value_str, False, self.enums_dicts[value_str], size, value_str)

                    # Save it with lut_ as prefix
                    value_dict[struct_name] = lut_ptr

                    # And create a load according to the offset
                    value = self.create_load(lut_ptr, type, offset, size, is_contiguous)
            return value

        # Handler for a regular variable, i.e., which is not inside any C/C++ struct from the ionic model'
        # Three handlers are needed: for global constants, parameter variables, literals, or expressions
        def handle_regular_variable(var, var_name, type, offset, size, is_contiguous):

            def is_literal_constant(n):
                try:
                    float(n)
                except ValueError:
                    return False
                else:
                    return True

            def handle_global_constant(var, var_name):
                value = self.get_global_constant(var, var_name)
                value = self.get_mlir_constant(value)

                return value

            def handle_parameter_variable(var_name, type, offset, size, is_contiguous):

                index = self.params.index(var_name)
                mlir_type = self.func_args[index].type

                f64 = ir.F64Type.get()
                if mlir_type == f64:
                    # If this is a f64 type, call get_adjusted_value
                    value = self.get_adjusted_value(f64, self.func_args[index])
                else:
                    # Otherwise, create a load from a struct
                    value = self.create_load(self.func_args[index], type, offset, size, is_contiguous)
                return value

            def handle_literal_constant(var_name):
                value = self.get_mlir_constant(var_name)
                return value

            def handle_expression(var_name):
                # Create an expression and let the parser return an AST
                expr = self.ionic_model.expression(var_name)
                mlir_ast = MlirAst(expr)

                # Run toMLIR in the newly created expression
                _, value = MlirAst.toMLIR(self.get_mlir_value, self.cache_value, value_dict, variable_printer)
                return value

            # Depending on the var type, hand it to a different function
            if var in self.global_constants:
                value = handle_global_constant(var, var_name)
            else:
                if var_name in self.params:
                    value = handle_parameter_variable(var_name, type, offset, size, is_contiguous)
                elif is_literal_constant(var_name):
                    value = handle_literal_constant(var_name)
                else:
                    value = handle_expression(var_name)

            return value

        def handle_function_options(function_name, types):
            if not function_name in self.function_dict.keys():
                # Set up the InsertionPoint of the function
                with ir.InsertionPoint(self.module.body):
                    # Create a new function and cache it for later
                    func = dialects.func.FuncOp(function_name, types, visibility="private")
                # Put it back up the InsertionPoint of the function
                with ir.InsertionPoint.at_block_terminator(self.bb):
                    pass
                self.function_dict[function_name] = func
            else:
                func = self.function_dict[function_name]
            return func

        # Retrieve variable information
        var_name, struct_name, operand_type, \
        offset, size, is_contiguous, \
        is_splat, hoist_from_loop = self.get_variable_info(var, self.params_dict, self.state_dict, self.region_dict,
                                                           self.enums_dicts, self.enable_data_layout_opt,
                                                           self.enable_rosenbrock, variable_printer)

        # Special case, handle function options: returns (and possibly creates) an MLIR function
        if function_options:
            # Unpack name and types
            function_name, *types = function_options
            return handle_function_options(function_name, *types)

        # If key is found, just return it
        value = get_value_from_dicts(var_name)
        if value is not None:
            return var_name, value

        if not struct_name:
            value = handle_regular_variable(var, var_name, operand_type, offset, size, is_contiguous)
        else:
            value = handle_variable_in_struct(var, struct_name, operand_type, offset, size,
                                              is_contiguous, is_splat, hoist_from_loop)

        # Cache var_name and return it
        return self.cache_value(value_dict, var_name, value)

    # Creates a new MLIR function for the ionic model
    def create_function(self):

        self.param_types, self.params = self.create_parameter_types()

        function = dialects.func.FuncOp(self.function_name, (self.param_types, []))

        # Create entry basic block with param_types
        entry_block = ir.Block.create_at_start(function.operation.regions[0], self.param_types)

        # Return void
        with ir.InsertionPoint(entry_block):
            dialects.func.ReturnOp([])

        return function

    def codegen_equations(self, target, valid_vars, value_dict, variable_printer=None):

        lookup_dependencies = Set([self.symbol_table[var.info('lookup').name]
                                   for var in self.all_lookup])
        all_lookup = self.ast_equations.dependencies_of(self.all_lookup, valid_vars | lookup_dependencies)

        lookup_dependencies -= valid_vars

        var_list = []
        for var in self.ast_equations.getList(target, valid_vars):
            if var in target and var in all_lookup:
                # Store in variable
                lhs_name = var.name
                expr = self.ionic_model.expression("%(rhs)s" % {
                    'rhs': var})
                mlir_ast = MlirAst(expr)
                result_name, rhs_value = mlir_ast.toMLIR(self.get_mlir_value, self.cache_value,
                                                         value_dict, variable_printer)
            elif var not in all_lookup:
                lhs_name = self.lhs_format_func(var)
                eqn = self.ast_equations[var]
                mlir_ast = MlirAst(eqn.rhs)
                result_name, rhs_value = mlir_ast.toMLIR(self.get_mlir_value, self.cache_value,
                                                         value_dict, variable_printer)
            else:
                continue

            value_dict[lhs_name] = rhs_value
            value_dict[result_name] = rhs_value
            var_list.append(var)
        return var_list

    def create_lut(self, var, enum_dict):

        lut_size = enum_dict['size'][1]

        table_name = var.name
        var_name = get_variable_name(var)
        if var_name in self.mlir_values_dict:
            is_state_variable = False
        elif (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external'):
            is_state_variable = True
            var_name = var.name
        else:
            is_state_variable = False
            var_name = var.name

        # This is a 'don't care' if Zero.
        # Otherwise, create a new lut call
        if lut_size == 0:
            lut_ptr = None
        else:
            lut_ptr = self.create_lut_call(var, table_name, is_state_variable, enum_dict, lut_size, var_name)

        # and save it with lut_ as prefix
        self.mlir_values_dict["lut_%s" % (var.name)] = lut_ptr

    @abstractmethod
    def create_lut_call(self, var, table_name, is_state_variable, enum_dict, lut_size, var_name):
        raise ValueError("Abstract class cannot implement function")

    def create_luts(self):

        for var in order(self.lookup_variables & self.update_inputs):
            self.create_lut(var, self.enums_dicts[var.name])

    def change_external_units(self, factor, needs_store=False):
        for var in order(set(self.external_variables) | set(self.needs_update_variables)):
            if var.is_a('unit'):
                current_unit = var.info('unit')
                if var.is_a('external'):
                    name = var.info('external')
                else:
                    name = var.name
                if name in self.external_units:
                    desired_unit = self.external_units[name].unit
                    if current_unit != desired_unit:

                        # Get the multiplication factor and cache it
                        mul_factor_str = "1e%d" % (factor * log_diff(desired_unit, current_unit))
                        mul_factor = self.get_mlir_constant(mul_factor_str)
                        self.cache_value_globally(mul_factor_str, mul_factor)

                        # Create the multiplication and cache it
                        old_value = self.get_mlir_value(var)
                        new_value = dialects.arith.MulFOp(old_value[1], mul_factor)

                        if needs_store:
                            # Get variable information
                            var_name, prefix, operand_type, offset, \
                            size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict,
                                                                               self.region_dict, self.enums_dicts,
                                                                               self.enable_data_layout_opt)

                            # If this is a state variable, we must use the prefix
                            if (prefix in self.params):
                                var_name = prefix

                            # Sanity Check
                            assert (var_name in self.params)

                            # get the index of this prefix in the function parameters
                            index = self.params.index(var_name)

                            is_sv = False
                            if var_name == 'sv':
                                is_sv=True

                            # And call a store function
                            self.create_store(self.func_args[index], new_value, operand_type, offset, size,
                                              is_contiguous, is_sv)
                        self.cache_value_globally(get_variable_name(var), new_value)

    def save_external_variables(self):

        for var in self.external_variables:

            # Get variable information
            var_name, prefix, operand_type, \
            offset, size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict,
                                                                       self.region_dict, self.enums_dicts,
                                                                       self.enable_data_layout_opt, False,
                                                                       get_variable_name)
            # Sanity Check
            assert (var_name in self.params)

            # get the index of this prefix in the function parameters
            index = self.params.index(var_name)

            # Retrieve the value to be stored only if exists, otherwise leave it, our work is done
            if not var_name in self.mlir_values_dict:
                continue
            store_val = self.mlir_values_dict[var_name]

            # And call a store function
            self.create_store(self.func_args[index], store_val, operand_type, offset, size, is_contiguous)

        for var in self.external_private_variables:
            # Get variable information
            var_name, prefix, operand_type,\
            offset, size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict,
                                                                       self.region_dict, self.enums_dicts,
                                                                       self.enable_data_layout_opt, False,
                                                                       get_variable_name)

            store_val = self.mlir_values_dict[var_name]

            # And call a store function
            self.create_conditional_load_store_to_parent(var_name, store_val, operand_type, True)

            # False
            # Get variable information
            var_name, prefix, \
            operand_type, offset,\
            size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict,
                                                               self.region_dict, self.enums_dicts,
                                                               self.enable_data_layout_opt, False, get_variable_name)

            # get the index of this prefix in the function parameters
            index = self.params.index(prefix)

            state_variable_ptr = self.func_args[index]

            _, _, putfcn_exists = self.function_pointer_info[var_name]
            function_exists = self.func_args[self.params.index(putfcn_exists)]

            i32 = ir.IntegerType.get_signless(32)

            # Create offset
            offset_int = offset
            if self.enable_data_layout_opt:
            	offset = offset * 8
            offset = dialects.arith.ConstantOp(i32, offset)

            # Create size
            size = dialects.arith.ConstantOp(i32, size)

            # And call a store function
            # Pass the original offset and flag this as a state variable. This
            # is needed for some targets to compute the correct offset
            self.create_conditional_load_store_call(function_exists, operand_type, state_variable_ptr, offset,
                                                    size, store_val, True, True, offset_int=offset_int, is_sv=True)

    def get_variables_with_diff_units(self):

        variables = []
        for var in order(set(self.external_variables) | set(self.needs_update_variables)):
            if var.is_a('unit'):
                current_unit = var.info('unit')
                if var.is_a('external'):
                    name = var.info('external')
                else:
                    name = var.name
                if name in self.external_units:
                    desired_unit = self.external_units[name].unit
                    if current_unit != desired_unit:
                        variables.append(get_variable_name(var))

        return variables

    def create_equations(self, already_computed, equations):

        for target in equations:
            this_var_list \
                = self.codegen_equations(target, self.good_vars | self.update_inputs | already_computed,
                                         self.mlir_values_dict)
            already_computed |= Set(this_var_list)

    def finish_variable_updates(self, variables_with_new_units, external_variables):

        def handle_regular_update():

            # Get LHS and RHS texts
            lhs_text = get_variable_name(var)
            rhs_text = self.lhs_format_func(var)

            # No need to generate code if LHS == RHS
            if lhs_text == rhs_text:
                return

            # Get variable information
            var_name, prefix, \
            type, offset, size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict,
                                                                             self.region_dict, self.enums_dicts,
                                                                             self.enable_data_layout_opt)

            # We might not have a prefix, so this is not coming from a struct variable.
            if not prefix:
                prefix = var_name

            # Sanity Check
            assert (prefix in self.params)

            # get the index of this prefix in the function parameters
            index = self.params.index(prefix)

            is_sv = False
            if prefix == 'sv':
                is_sv = True

            # Retrieve the value to be stored
            store_val = self.mlir_values_dict[rhs_text]

            # Do not generate a store if this variable will require a new unit
            # Instead, save the value that is supposed to be stored
            if var_name in variables_with_new_units or \
                    var_name in external_variables:
                # Cache this value that was use. We will need to update it later
                self.cache_value_globally(var_name, store_val)
            else:
                # Or call a store function
                self.create_store(self.func_args[index], store_val, type, offset, size, is_contiguous, is_sv)

        def handle_bounded_variable(var, is_upper_bound):

            def get_predicate_attr(is_upper_bound):
                # Values 2 and 4 are not randomly picked.
                # They correspond to the MLIR values of '>' and '<' conditions
                if is_upper_bound:
                    value = 2
                else:
                    value = 4
                return ir.IntegerAttr.get(ir.IntegerType.get_signless(64), value)

            def create_if_statement(lhs_cond, rhs_cond, predicate, lhs, rhs):

                # Create a vector of i1 to store the results of comparisons
                i1 = ir.IntegerType.get_signless(1)
                operand_type = self.get_type(i1)

                # Create comparison
                condition = dialects.arith.CmpFOp(predicate, lhs_cond, rhs_cond)

                # Create SelectOp and return it
                return dialects.arith.SelectOp(condition, lhs, rhs)

            if is_upper_bound:
                bound_str = "upper_bound"
            else:
                bound_str = "lower_bound"

            predicate = get_predicate_attr(is_upper_bound)

            # Get LHS and RHS texts
            store_variable_str = self.lhs_format_func(var)
            lhs_cond = self.mlir_values_dict[store_variable_str]

            bounded_val = var.info(bound_str)
            rhs_cond = self.get_mlir_constant(bounded_val)

            new_value_str = self.lhs_format_func(var)

            # Cache the new value
            self.mlir_values_dict[new_value_str] = create_if_statement(lhs_cond, rhs_cond, predicate,
                                                                       rhs_cond, lhs_cond)

        for var in self.needs_update_variables:

            lower_bound = var.is_a('lower_bound')
            upper_bound = var.is_a('upper_bound')

            if lower_bound or upper_bound:
                if lower_bound:
                    handle_bounded_variable(var, False)
                if upper_bound:
                    handle_bounded_variable(var, True)
            handle_regular_update()

    def create_new_expression(self, expression_str, lhs_name, dict_rhs, variable_printer, dict_lhs=None):

        if dict_rhs is None:
            dict_rhs = self.mlir_values_dict
        if dict_lhs is None:
            dict_lhs = dict_rhs

        expr = self.ionic_model.expression(expression_str)
        mlir_ast = MlirAst(expr)

        # Run toMLIR in the newly created expression
        result_name, rhs_value = mlir_ast.toMLIR(self.get_mlir_value, self.cache_value, dict_rhs, variable_printer)

        # Cache MLIR values
        dict_lhs[lhs_name] = rhs_value
        dict_lhs[lhs_name] = rhs_value

    def create_new_expressions(self, symbol_table, variable_type, expr_str, has_op1, has_op2, has_op3, dict=None,
                               variable_printer=None):

        for var in order(symbol_table):

            op1 = ""
            op2 = ""
            op3 = ""

            # Get operands
            if has_op1:
                op1 = var
            if has_op2:
                if not has_op3:
                    op2 = var.info(variable_type)
                else:
                    op2, op3 = var.info(variable_type)

            # Get the correct name of the variable with limpet_fe function for lhs's
            lhs_name = self.lhs_format_func(var)

            # Create a new expression
            self.create_new_expression(expr_str % {'rhs1': op1, 'rhs2': op2, 'rhs3': op3},
                                       lhs_name, dict, variable_printer)

    # This function implements internal scopes for rk2 and rk4 integration methods
    #   integ_method_str - string name for the integration method
    #   already_computed - the equations already computed and to be ignored
    #   targets - target equations for the integration method
    #   init_equation - first equation that initializes variables for the internal scope
    #   list_equations_scope - list of all scopes and its LHS and RHS of equations, initial and final equations
    #   external_equation - equation that connects to the internal to external scope
    def create_internal_scopes_for_rk2_rk4(self, integ_method_str, already_computed, targets,
                                           init_equation, list_equations_scope, external_equation=None):

        # Leave if this integration model needs no scope
        intermediate_vals = self.ionic_model.getVars(integ_method_str)
        if not intermediate_vals:
            return

        # Create a new printer method for intermediate values (uses "sv_intermediate_" prefix)
        integ_method_printer = IntermediateVariablePrinter(get_variable_name, intermediate_vals)

        # Returns a string according to the printing method
        def get_printer(printer_type, var):
            if printer_type == PrinterType.EMPTY:
                return var
            elif printer_type == PrinterType.LHS_PRINTER:
                return self.lhs_format_func(var)
            elif printer_type == PrinterType.INTERMEDIATE:
                return integ_method_printer(var)
            elif printer_type == PrinterType.DIFF:
                return get_variable_name(var.info('diff'))
            else:
                print("Unknown printer")
                assert (False)

        # Create new dictionary
        integ_method_dict = {}

        # Defines an internal expression.
        # For initial equations, dict_lhs == dict_rhs.
        # For final equations, dict_lhs == self.mlir_values_dict
        # This is needed in order to select the correct dictionary to save the MLIR value
        def create_internal_expression(var, lhs_str, formula, list_of_printers, dict_lhs=None):
            lhs_name = lhs_str % get_printer(list_of_printers[0], var)

            format_dict = {}
            for i in range(1, len(list_of_printers)):
                format_dict["rhs" + str(i)] = get_printer(list_of_printers[i], var)

            # Create a new expression
            self.create_new_expression(formula % format_dict, lhs_name, integ_method_dict, get_variable_name, dict_lhs)

        # From this on, we create multiple expressions in the internal scope.
        # init_equation is optional depending on the integration method.
        if init_equation is not None:
            (lhs_str, formula, list_of_printers) = init_equation
            for var in order(intermediate_vals):
                create_internal_expression(var, lhs_str, formula, list_of_printers)

        # Iterate over a list of scopes and produce new expressions
        for (init_time_variable, init_equation, finish_equation) in list_equations_scope:

            # init_time_variable may be optional
            if init_time_variable is not None:
                self.create_new_expression(init_time_variable[1], init_time_variable[0],
                                           integ_method_dict, get_variable_name)

            # Initial equations for the scope
            lhs_str, formula, list_of_printers = init_equation
            for var in order(intermediate_vals):
                create_internal_expression(var, lhs_str, formula, list_of_printers)

            # Main equations of the scope
            useable_from_previous = already_computed - self.ast_equations.influences_of(intermediate_vals)
            self.codegen_equations(targets - self.good_vars,
                                   self.good_vars | self.update_inputs | useable_from_previous,
                                   integ_method_dict, integ_method_printer)

            # Finalize the scope
            lhs_str, formula, list_of_printers = finish_equation
            for var in order(intermediate_vals):
                create_internal_expression(var, lhs_str, formula, list_of_printers, self.mlir_values_dict)

        # Connect internal to external scope if necessary
        if external_equation is not None:
            (lhs_str, formula, list_of_printers) = external_equation
            for var in order(intermediate_vals):
                create_internal_expression(var, lhs_str, formula, list_of_printers, self.mlir_values_dict)

    # This function implements internal scopes for sundness
    def create_internal_scopes_for_sundness(self, already_computed):

        # Leave if this integration model needs no scope
        intermediate_vals = self.ionic_model.getVars('sundnes')
        if not intermediate_vals:
            return

        # Create a new printer method for intermediate values (uses "sv_intermediate_" prefix)
        integ_method_printer = IntermediateVariablePrinter(get_variable_name, intermediate_vals)

        # Create new dictionary
        integ_method_dict = {}

        for var in order(intermediate_vals):
            full = var.info('sundnes').full

            for other_var in order(intermediate_vals):
                if var != other_var:
                    half = other_var.info('sundnes').half
                    lhs_name = integ_method_printer(other_var)
                    rhs_name = self.lhs_format_func(half)
                else:
                    lhs_name = integ_method_printer(var)
                    rhs_name = get_variable_name(var)
                integ_method_dict[lhs_name] = self.mlir_values_dict[rhs_name]

            # Main equations of the scope
            useable_from_previous = already_computed - self.ast_equations.influences_of(intermediate_vals)
            self.codegen_equations(Set([full]) - self.good_vars,
                                   self.good_vars | self.update_inputs | useable_from_previous,
                                   integ_method_dict, integ_method_printer)

            lhs_name = self.lhs_format_func(var)
            rhs_name = integ_method_printer(full)
            self.mlir_values_dict[lhs_name] = integ_method_dict[rhs_name]

    def create_markov_be_condition_block(self, region, while_result_types):

        # Create the BB for the condition
        condition_bb = ir.Block.create_at_start(region, while_result_types)

        # Retrieve values for later use
        error = condition_bb.arguments[0]
        count = condition_bb.arguments[1]
        while_init_vals = condition_bb.arguments

        with ir.InsertionPoint(condition_bb):
            # error > 1e-100
            f64 = ir.F64Type.get()
            const_1e_minus100 = dialects.arith.ConstantOp(f64, 1e-100)
            max_error = self.get_maxf_value(error)
            first_comparison = create_cmp("sgt", max_error, const_1e_minus100)

            # count < 50
            i32 = ir.IntegerType.get_signless(32)
            const_50 = dialects.arith.ConstantOp(i32, 50)
            max_count = self.get_maxi_value(count)
            second_comparison = create_cmp("slt", max_count, const_50)

            # error > 1e-100 && count < 50
            condition_op = dialects.arith.AndIOp(first_comparison, second_comparison)

            # Create condition for the while loop, which will run until condition_op is true
            dialects.scf.ConditionOp(condition_op, while_init_vals)

    def create_markov_be_main_block(self, region, while_result_types, markov_be_vals,
                                    already_computed, markov_be_dict, solve_format):

        def get_operation_type(val):
            if type(val) == ir.Value:
                return val.owner
            return val

        # Creates BBs for regions 'after'. This is necessary for the WhileOp operation
        main_bb = ir.Block.create_at_start(region, while_result_types)

        # Set InsertionPoint properly
        with ir.InsertionPoint(main_bb):

            # Adds entries to "error" and "count"
            markov_be_dict["error"] = self.create_floating_point_constant(ir.F64Type.get(), 0.0)
            markov_be_dict["count"] = main_bb.arguments[1]

            # Iterate through markov be vals and create entries in the dictionaries for the input values
            # Two first are error and count
            i = 2
            for var in order(markov_be_vals):
                var_name = solve_format(var)
                markov_be_dict[var_name] = main_bb.arguments[i]
                var_name = self.lhs_format_func(var)
                markov_be_dict[var_name] = main_bb.arguments[i]
                i += 1

            # Main equations of the scope
            self.codegen_equations(self.markov_be_solve - self.good_vars,
                                   self.good_vars | self.update_inputs | already_computed,
                                   markov_be_dict, solve_format)

            # Track values to be used in the YieldOp operand
            yield_list = []

            new_error = None
            for var in order(markov_be_vals):
                # Get them from dictionary
                val1 = markov_be_dict[solve_format(var)]
                val2 = markov_be_dict[self.lhs_format_func(var)]

                # Fix types if necessary
                val1 = get_operation_type(val1)
                val2 = get_operation_type(val2)

                # fabs(<var>_markov_solve-<var>__new);
                abs_op = dialects.math.AbsFOp(dialects.arith.SubFOp(val1, val2))

                # error += fabs(<var>_markov_solve-<var>__new);
                val1 = markov_be_dict["error"]
                new_error = dialects.arith.AddFOp(val1, abs_op)
                markov_be_dict["error"] = new_error

                # Add a new entry to the yield list and to the dictionary
                yield_list.append(markov_be_dict[solve_format(var)])
                markov_be_dict[self.lhs_format_func(var)] = markov_be_dict[solve_format(var)]

            # count++
            i32 = ir.IntegerType.get_signless(32)
            one = self.create_integer_constant(i32, 1)
            new_count = dialects.arith.AddIOp(markov_be_dict["count"], one)
            markov_be_dict["count"] = new_count

            # Finally create the YieldOp for the main bb of the while loop
            yield_list = [i.result for i in yield_list]
            dialects.scf.YieldOp([new_error.result, new_count.result] + yield_list)

        # Return the dictionary, since it was updated
        return markov_be_dict

    def create_markov_be_loop(self, markov_be_vals, already_computed, current_bb_insert_point):

        f64 = ir.F64Type.get()
        f64_type = self.get_type(f64)

        i32 = ir.IntegerType.get_signless(32)
        i32_type = self.get_type(i32)

        def solve_format(var, f=get_variable_name):
            if var.is_a('markov_be'):
                return f(var.info('markov_be'))
            else:
                return f(var)

        def get_value_type(val):
            if type(val) != ir.Value:
                return val.result
            return val

        # Create error and count variable
        error = self.create_floating_point_constant(f64, 1.0)
        count = self.create_integer_constant(i32, 0)

        # Add to the initialization list for initial values and result types
        while_init_vals = []
        while_init_vals.append(error.result)
        while_init_vals.append(count.result)
        while_result_types = []
        while_result_types.append(f64_type)
        while_result_types.append(i32_type)

        # Iterate through markov_be vals and add each markov_be value to the two lists
        for var in order(markov_be_vals):
            # Retrieve it and make sure to use its Value
            val = self.mlir_values_dict[get_variable_name(var)]
            val = get_value_type(val)

            # Add them to the list
            while_init_vals.append(val)
            while_result_types.append(val.type)

        # Create WhileOp operation with the appropriate result types and initial values
        while_loop = dialects.scf.WhileOp(while_result_types, while_init_vals)

        # Create a condition block
        self.create_markov_be_condition_block(while_loop.before, while_result_types)

        # Create and fill up main block
        markov_be_dict = {}
        self.create_markov_be_main_block(while_loop.after, while_result_types, markov_be_vals,
                                         already_computed, markov_be_dict, solve_format)

        # Iterate through markov be vals and create entries in the dictionaries for the input values
        # Two first are error and count
        i = 2
        for var in order(markov_be_vals):
            var_name = solve_format(var)
            markov_be_dict[var_name] = while_loop.results[i]
            var_name = self.lhs_format_func(var)
            markov_be_dict[var_name] = while_loop.results[i]
            i += 1

        # Retrieve the original bb before the creation of the while loop
        # Iterate one last time through the variables
        with current_bb_insert_point:
            for var in order(markov_be_vals):
                # Update the variables with new expressions:  <var_name>_new  sv-><var_name>+dt*<var_name>_new"
                formula = "%s+dt*%s" % (var, self.lhs_format_func(var))
                lhs_name = self.lhs_format_func(var)

                # Create a new expression
                self.create_new_expression(formula, lhs_name, markov_be_dict, get_variable_name,
                                           self.mlir_values_dict)

    # This function implements markov_be loop.
    # For a better understanding on the values created in this function,
    # use to the C/C++ of the ionic model GPVatria as a reference
    def create_markov_be_method(self, already_computed):

        # Get all markov_be values and leave if nothing needs to be done
        markov_be_vals = self.ionic_model.getVars('markov_be')
        if not markov_be_vals:
            return

        execute_region, markov_bb_insert_point = self.initialize_markov_be(markov_be_vals, self.bb, get_variable_name)

        # Set InsertionPoint properly
        with markov_bb_insert_point:
            self.create_markov_be_loop(markov_be_vals, already_computed, markov_bb_insert_point)

            self.finalize_markov_be(execute_region, markov_be_vals, get_variable_name)

    def get_function_argument(self, argument_name):
        assert(argument_name in self.params)
        values_size = self.params.index(argument_name)
        return self.func_args[values_size]

    # Rosenbrock is calculated with 'float', so return 4 given in number of bytes.
    # Use 8 to make it as 'double'
    @staticmethod
    def get_rosenbrock_precision():
        return 4

    # Rosenbrock ion private is defined with 'double', so return 8 given in number of bytes.
    @staticmethod
    def get_rosenbrock_ion_private_precision():
        return 8

    # Returns the size of the allocation needed for the rosenbrock method
    def get_rosenbrock_size(self, rb):
        return (len(rb) * self.get_rosenbrock_precision()) * self.get_computation_size()

    # Fill up the memory allocated previously with values from the state variable
    # Typically this will connect 'Rosenbrock_X[ROSEN_#Var] to 'sv->#Var'
    # Thus, it is equivalent to: 'Rosenbrock_X[ROSEN_Cai] = sv->Cai, assuming #Var is equal to Cai.
    def prepare_rosenbrock_values(self, mem_pointer_to_rosenbrock, rb, function_dict,
                                  offset_function, size, is_contiguous):

        offset = 0
        for var in rb:
            state_var = self.get_mlir_value(var, function_dict)
            assert(function_dict[state_var[0]])

            # Similar to the original openCARP, we use f32 as type for Rosenbrock values
            self.create_store(mem_pointer_to_rosenbrock, state_var[1], "f32", offset, size, is_contiguous)
            offset = offset_function(offset)

    # Restores values that have been computed with Rosenbrock's computing method
    # Typically this will connect '#Var_new' to 'Rosenbrock_X[ROSEN_#Var].
    # Thus, it is equivalent to: 'Cai_new = Rosenbrock_X[ROSEN_Cai], assuming #Var is equal to Cai.
    # It does the inverse operation of 'prepare_rosenbrock_values' after Rosenbrock_X values have been computed.
    def restore_state_values_from_rosenbrock(self, mem_pointer_to_rosenbrock, rb):

        offset = 0
        size = len(rb) * self.get_rosenbrock_precision()
        for var in order(rb):
            # Similar to the original openCARP, we use f32 as type for Rosenbrock values
            rb_load_val = self.create_load(mem_pointer_to_rosenbrock, "f32", offset, size, False)
            self.mlir_values_dict[self.lhs_format_func(var)] = rb_load_val
            offset += 4

    # Store the Node number (or cell number) in ion_private in order to access it in the Rosenbrock functions
    def store_node_number(self, ion_private):
        index_type = ir.IndexType.get()
        # offset_index is equal to 8 because we must access the second value of the Private struct. See below
        # struct Shannon_Private {
        #   ION_IF *IF;
        #   int   node_number; // This value has offset of 8. Pointer are stored in 8 bytes
        #   void* cvode_mem;
        #   bool  trace_init;
        #   Shannon_Regional_Constants rc;
        #   Shannon_Nodal_Req nr;
        # };
        offset_index = dialects.arith.ConstantOp(index_type, 8)
        var_ptr = self.create_pointer_cast_operation(ion_private, offset_index)
        vec_4xi8_ptr = ir.ShapedType(ir.Type.parse("memref<4xi8>", self.ctx))

        # Convert to memref<4xi8>
        ion_private_4xi8 = dialects.memref.ViewOp(vec_4xi8_ptr, var_ptr, dialects.arith.ConstantOp(index_type, 0), [])

        # Then to memref<1xi32>
        vec_1xf32_ptr = ir.ShapedType(ir.Type.parse("memref<1xi32>", self.ctx))
        val = dialects.memref.ViewOp(vec_1xf32_ptr, ion_private_4xi8, dialects.arith.ConstantOp(index_type, 0), [])

        # And finally store it
        dialects.memref.StoreOp(self.cell_number, val, [dialects.arith.ConstantOp(ir.IndexType.get(), 0).result])

    def get_ion_private_ptr(self):
        ion_private = self.get_function_argument("ion_private_ptr")
        return ion_private

    # Store in ion_private any nodal request value that is needed inside the rosenbrock function
    def store_rosenbrock_values(self, ion_private):

        # Offset of the first value inside ion_private that will be used to store data
        index_type = ir.IndexType.get()
        first_value_offset = self.get_function_argument("ion_private_first_value")
        first_value_offset_index = dialects.arith.IndexCastOp(index_type, first_value_offset)

        # Nodal req. offsets start after regional constants,
        # so we must multiply by ion private precision and computation size
        offset = len(self.regional_constants) * self.get_rosenbrock_ion_private_precision() *\
                 self.get_computation_size()
        for var in order(self.rosenbrock_nodal_req):
            offset_index = dialects.arith.AddIOp(first_value_offset_index, dialects.arith.ConstantOp(index_type, offset))
            var_ptr = self.create_pointer_cast_operation(ion_private, offset_index)
            self.create_store(var_ptr, self.get_mlir_value(var)[1], "f64", 0, 0, True)
            offset += self.get_computation_size()*self.get_rosenbrock_ion_private_precision()

    # Prepare Thread-specific information needed for Rosenbrock
    def prepare_rosenbrock_ion_private(self):
        ion_private = self.get_ion_private_ptr()
        self.store_node_number(ion_private)
        self.store_rosenbrock_values(ion_private)
        return ion_private

    def create_rosenbrock_function(self, function_name, args):

        types = []
        for i in args:
            if isinstance(i, str):
                types.append(ir.Type(ir.Type.parse(i, self.ctx)))
            else:
                types.append(i)

        with ir.InsertionPoint(self.module.body):
            func = dialects.func.FuncOp(function_name, (types, []), visibility="private")
        func_constant = dialects.func.ConstantOp(func.type, ir.FlatSymbolRefAttr.get(function_name))
        return func_constant, func

    # Allocate stack memory for the temporaries values needed for Rosenbrock
    # Accepts an optional alignemnt for the alloca
    def allocate_rosenbrock_data(self, rb, alignment=None):
        # Create new types that will be used for LUTs
        memref_rosenbrock_type\
            = ir.ShapedType(ir.Type.parse("memref<%sxi8>" % self.get_rosenbrock_size(rb), self.ctx))
        memref_unshaped = ir.ShapedType(ir.Type.parse("memref<?xi8>", self.ctx))
        memref_1xi8 = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        index_type = ir.IndexType.get()

        # Allocate memory so that the LUT can store data for each cell
        # And cast it to the right type (memref<1xi8>)
        rosenbrock_alloca = dialects.memref.AllocaOp(memref_rosenbrock_type, [], [], alignment=alignment)
        rosenbrock_unshaped = dialects.memref.CastOp(memref_unshaped, rosenbrock_alloca)
        offset_index = dialects.arith.ConstantOp(index_type, 0)
        return dialects.memref.ViewOp(memref_1xi8, rosenbrock_unshaped, offset_index, [])

    # Enable or disable rosenbrock codegen.
    # This is needed to select the proper naming conventions for rosenbrock
    def set_rosenbrock_codegen(self, value):
        self.enable_rosenbrock = value

    # Get rosenbrock-specific naming conventions, offset values, and general information
    def get_rosenbrock_info(self, var):
        if var in self.rosenbrock_nodal_req:
            # offset must take into account computation size of the ion_private variable
            offset = self.rosenbrock_nodal_req.index(var) * self.get_computation_size()
            return "nr->" + var.name, "nr", "f64", offset, 0, True
        elif var in self.regional_constants:
            # offset must take into account computation size of the ion_private variable
            offset = self.regional_constants.index(var) * self.get_computation_size()
            return "rc->" + var.name, "rc", "f64", offset, 0, True
        elif var in self.rosenbrock:
            offset = self.rosenbrock_dict[var.name][1]
            return "Rosenbrock_X[ROSEN_%s]" % var, "Rosenbrock_X", "f32", offset, \
                   int(len(self.rosenbrock_dict)*self.get_rosenbrock_precision()), False
        else:
            var_name, prefix, type, offset, \
            size, is_contiguous, _, _ = self.get_variable_info(var, self.params_dict, self.state_dict, self.region_dict,
                                                               self.enums_dicts, self.enable_data_layout_opt, False)
            return var_name, prefix, type, offset, size, is_contiguous

    def create_rosenbrock_compute_functions(self, allocated_data, ion_private_ptr):

        # Enable rosenbrock to allow get_variable_info to get the right information
        self.set_rosenbrock_codegen(True)

        # Get function names according to the backend
        stepx_name, f_func, jacobian_func = self.get_rosenbrock_functions()

        # Create stepX call, passing f_func and jacobian_func as parameter
        self.create_rosenbrock_stepx(allocated_data, ion_private_ptr, stepx_name, f_func, jacobian_func)

        # Disable rosenbrock
        self.set_rosenbrock_codegen(False)

    # This function handles Rosenbrock method, a rather complicated integration method that requires
    # some initial setup, followed by a computation method through function pointers and indirection,
    # and finalization to get the computed data.
    # You may refer to Shannon as an ionic model that uses Rosenbrock
    def create_rosenbrock_method(self):

        if not self.rosenbrock:
            return

        # Allocate temporary data for rosenbrock
        allocated_data = self.allocate_rosenbrock_data(self.rosenbrock)

        # Fill up values to the allocated space
        next_offset = lambda offset : offset + 4
        size = len(self.rosenbrock) * self.get_rosenbrock_precision()
        self.prepare_rosenbrock_values(allocated_data, order(self.rosenbrock), self.mlir_values_dict,
                                       next_offset, size, False)

        # Fill up ion_private data
        ion_private_ptr = self.prepare_rosenbrock_ion_private()

        # Create all rosenbrock compute functions
        self.create_rosenbrock_compute_functions(allocated_data, ion_private_ptr)

        # Once rosenbrock has run, connects rosenbrock values with the state variables
        # This essentially makes rosenbrock values be the updates values for '#Var_new'
        self.restore_state_values_from_rosenbrock(allocated_data, self.rosenbrock)

    def convert_argument_pointer(self, arg, arg_type, arg_number):
        vec_8xi8_ptr = ir.ShapedType(ir.Type.parse("memref<8xi8>", self.ctx))

        # We are only interested in arguments with 'vec_i8_ptr' type
        if arg_type != vec_8xi8_ptr:
            return arg

        def is_state_variable(arg_number):
            if self.params[arg_number] == 'sv':
                return True
            return False

        def is_ion_private(arg_number):
            if self.params[arg_number] == 'ion_private_ptr':
                return True
            return False

        index_type = ir.IndexType.get()
        if self.parent_table_address in self.params and \
                arg_number == self.params.index(self.parent_table_address):
            offset_index = dialects.arith.ConstantOp(index_type, 0)
        elif is_state_variable(arg_number):
            # Multiply by the size of the state variable
            sizeof_arg_index = dialects.arith.ConstantOp(index_type, self.state_dict["size"][1])
            offset_index = dialects.arith.MulIOp(sizeof_arg_index, self.cell_number_index)
        elif is_ion_private(arg_number):
            # A thread_id may take the following values:
            # - 1 for CPU codegen (no OpenMP)
            # - omp_get_max_threads() for CPU codegen (with OpenMP)
            # - total number of cells for GPU codegen
            sizeof_arg_index\
                = dialects.arith.IndexCastOp(ir.IndexType.get(), self.get_function_argument("ion_private_size"))
            thread_id = self.get_thread_id()
            offset_index = dialects.arith.MulIOp(thread_id, sizeof_arg_index)
        else:
            # Otherwise, multiply by 8 since external values are always passed as double.
            sizeof_arg_index = dialects.arith.ConstantOp(index_type, 8)
            offset_index = dialects.arith.MulIOp(sizeof_arg_index, self.cell_number_index_external)
 
        generic_vec_ptr = ir.ShapedType(ir.Type.parse("memref<?xi8>", self.ctx))

        # Cast to memref<?xi8>
        casted_arg = dialects.memref.CastOp(generic_vec_ptr, arg)

        # Calculate new offset result, source, byte_shift, sizes
        return self.create_pointer_cast_operation(casted_arg, offset_index).result

    # This function handles casting of arguments that are passed as 'memref<8xi8>' and need conversion to 'memref<1xi8>'
    # for further use by load and store functions that create vectors of state or external variables.
    # Arguments received by the function call points to the beginning of the data structure,
    # so offset calculated is needed. We essentially transform 'memref<8xi8>' -> 'memref<?xi8>' -> 'memref<1xi8>'
    # with a combination of MemRef operations: MemRef.CastOp and MemRef.ViewOp. The later being used for offset calc.
    def handle_argument_pointers(self, function):

        for i in range(len(function.arguments)):
            arg = function.arguments[i]
            arg_type = arg.type

            adjusted_arg = self.convert_argument_pointer(arg, arg_type, i)
            self.func_args.append(adjusted_arg)

    @abstractmethod
    def create_for_loop(self, function):

        index_type = ir.IndexType.get()

        # Get bounds and step. Start and End cell numbers are always arguments 0 and 1
        start_cell = function.arguments[0]
        start_cell_index = dialects.arith.IndexCastOp(index_type, start_cell)
        end_cell = function.arguments[1]
        end_cell_index = dialects.arith.IndexCastOp(index_type, end_cell)
        step_vector_size_index = self.get_step_size(index_type)

        self.loop = dialects.scf.ForOp(start_cell_index, end_cell_index, step_vector_size_index)
        self.bb = self.loop.body

        with ir.InsertionPoint(self.bb):
            self.cell_number_index = self.loop.induction_variable
            # Some targets might need a different index for external and
            # state variables (for example with data layout optimization)
            self.cell_number_index_external = self.cell_number_index
            self.cell_number = dialects.arith.IndexCastOp(ir.IntegerType.get_signless(32), self.cell_number_index)
            dialects.scf.YieldOp(self.loop.inner_iter_args)

    def emit_function(self, function):

        # Save function for later access
        self.bb = function.regions[0].blocks[0]

        with ir.InsertionPoint.at_block_terminator(function.regions[0].blocks[0]):
            # Create for loop
            self.create_for_loop(function)

        # Reset BB and use the one inside the for loop
        with ir.InsertionPoint.at_block_terminator(self.bb):
            # And handle argument pointers. See function description
            self.handle_argument_pointers(function)

            # Change the units of external variables as appropriate.
            self.change_external_units(1)

            # Compute lookup tables for things that have already been defined.
            self.create_luts()

            # Track equations that were already computed
            already_computed = Set()

            # List of all variables that need to be handled
            # This list mimics the order used in the limpet_fe.py file
            equations = [self.external_update_variables, self.fe_targets - self.good_vars,
                         self.rush_larsen_targets - self.good_vars, self.rk2_targets - self.good_vars,
                         self.rk4_targets - self.good_vars, self.sundnes_half_targets - self.good_vars,
                         self.markov_be_targets - self.good_vars]

            # Main part of the code generation.
            # Most equations are created here
            self.create_equations(already_computed, equations)

            # Finish fe equations
            self.create_new_expressions(self.symbol_table.getVars('fe'), 'diff', "%(rhs2)s*dt+%(rhs1)s",
                                        True, True, False)

            # Finish rush_larsen equations
            self.create_new_expressions(self.ionic_model.getVars('rush_larsen'), 'rush_larsen',
                                        "%(rhs2)s+%(rhs3)s*%(rhs1)s", True, True, True)

            # Finish rk2
            self.create_internal_scopes_for_rk2_rk4("rk2", already_computed, self.rk2_targets,
                                                    None, [(("t", "t + dt/2"),
                                                            ("%s", "%(rhs1)s+dt/2*%(rhs2)s",
                                                             [PrinterType.INTERMEDIATE, PrinterType.EMPTY,
                                                              PrinterType.DIFF]),
                                                            ("%s", "%(rhs1)s+dt*%(rhs2)s",
                                                             [PrinterType.LHS_PRINTER, PrinterType.EMPTY,
                                                              PrinterType.DIFF]))])

            # Finish rk4
            self.create_internal_scopes_for_rk2_rk4("rk4", already_computed, self.rk4_targets,
                                                    ("rk4_k1_%s", "%(rhs2)s*dt",
                                                     [PrinterType.EMPTY, PrinterType.EMPTY,
                                                      PrinterType.DIFF]),
                                                    [(("t", "t + dt/2"),
                                                      ("%s", "%(rhs1)s+rk4_k1_%(rhs2)s/2",
                                                       [PrinterType.INTERMEDIATE,
                                                        PrinterType.EMPTY,
                                                        PrinterType.EMPTY]),
                                                      ("rk4_k2_%s", "dt*%(rhs1)s",
                                                       [PrinterType.EMPTY,
                                                        PrinterType.DIFF,
                                                        PrinterType.EMPTY])),
                                                     (("t", "t + dt/2"),
                                                      ("%s", "%(rhs1)s+rk4_k2_%(rhs2)s/2",
                                                       [PrinterType.INTERMEDIATE,
                                                        PrinterType.EMPTY,
                                                        PrinterType.EMPTY]),
                                                      ("rk4_k3_%s", "dt*%(rhs1)s",
                                                       [PrinterType.EMPTY,
                                                        PrinterType.DIFF,
                                                        PrinterType.EMPTY])),
                                                     (("t", "t + dt"),
                                                      ("%s", "%(rhs1)s+rk4_k3_%(rhs2)s",
                                                       [PrinterType.INTERMEDIATE,
                                                        PrinterType.EMPTY,
                                                        PrinterType.EMPTY]),
                                                      ("rk4_k4_%s", "dt*%(rhs1)s",
                                                       [PrinterType.EMPTY,
                                                        PrinterType.DIFF,
                                                        PrinterType.EMPTY]))],
                                                    ("%s", "%(rhs1)s+(rk4_k1_%(rhs2)s +\
                                                    2*rk4_k2_%(rhs3)s  + 2*rk4_k3_%(rhs4)s + \
                                                    rk4_k4_%(rhs5)s)/6",
                                                     [PrinterType.LHS_PRINTER, PrinterType.EMPTY,
                                                      PrinterType.EMPTY, PrinterType.EMPTY,
                                                      PrinterType.EMPTY, PrinterType.EMPTY]))

            # Finish Sundness
            self.create_internal_scopes_for_sundness(already_computed)

            # Finish Markov Backward Euler method
            self.create_markov_be_method(already_computed)

            # Rosenbrock
            self.create_rosenbrock_method()

            # Get all variables that need new unit.
            # We will use them to make sure only one store is generated.
            variables_with_new_units = self.get_variables_with_diff_units()

            # Finish the update with stores back to variables
            self.finish_variable_updates(variables_with_new_units,
                                         [get_variable_name(var) for var in self.external_variables])

            # Change the units of external variables as appropriate.
            self.change_external_units(-1, True)

            # Save all external vars
            self.save_external_variables()

    def __call__(self):

        # Create a new Context if necessary
        # An MLIR context is a top-level entity that owns attributes and
        # types and is referenced from virtually all IR constructs
        self.ctx = ir.Context()

        with self.ctx, ir.Location.unknown():
            # Initiate a new MLIR Module
            self.module = ir.Module.create()

            # Create a function for the MLIR code
            with ir.InsertionPoint(self.module.body):
                # Create helper functions
                self.create_helper_functions()

                # Create an MLIR function
                func = self.create_function()

                self.emit_function(func)

            # Print to file
            print(self.module.__str__(), file=self.output_file)

