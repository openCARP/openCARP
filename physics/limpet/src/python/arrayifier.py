#!/usr/bin/python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

# simulate arrays by unrolling/flattening the expressions 
#
## let V be a variable. to make it an array of N values
#
# V;.array(N);
# group {V1;V2;}.array(N);
# V;.array();
#
# With no size specified, a default is used which can be changed as an option
#
# To assign to variable slices, use python slice syntax
# V[a:b:c]
# which is the set of values [a,a+c,a+2*c,...,b-1]
# A missing "a" means a=0, and omitting "b" implies N,-'s are measured from the end
# Note, an extension is added: a leading ^ means take the complement of the slice 
# specified. Eg, given V has 6 entries
# V[^1:3] => V[0,3,4,5] 
# To assign to a particular index
# V[2] = ...
# The RHS may also contain array variables which must match in size the LHS. All
# slices must be the same size.
# The following is a 1D diffusion example
# diff_V[1:-1] = k*(V[2:]+V[:-2]-2*V[1:-1])
#
# Slice specifications cannot be expressions. eg [a+1:] and [1-1:5] are INVALID
#
# __INDEX__ is recognized as the enumeration of the LHS set, eg,
#  V[1:] = 2+__INDEX__
# would lead to V[1] = 2, V[2] = 3, V[3] = 4
# Another word can be chosen with a commandline option
#
# If the inital predicate of the if statement contains an array, the whole expression
# is unrolled including elif's and else's, eg
# if( a[:] > 3 ) { b[:] = 2; } elif( a[:] < 1 ) {b[:] = 2; } else { b[:] = __INDEX__ }
# becomes
# if( a[0] > 3 ) { b[0] = 2; } elif( a[0] < 1 ) {b[0] = 2; } else { b[0] = 0 }
# if( a[1] > 3 ) { b[1] = 2; } elif( a[1] < 1 ) {b[1] = 2; } else { b[1] = 1 }
# etc
# If the predicate does not contain an array, arrays are expended as needed, eg
# if( a == 1 ) {b[:] = 3; }
# becomes
# if( a==1 ){ b[0] = 3; b[1] = 3; b[2] = 3; }
# elif predicates can contain arrays if and only if the initial predicate does.
# For nested if's, the outermost arrayed initial predicate determines the array index of 
# any contained array expressions.
#
# Derived variables are automatically arrayed if the original variable is:
# diff_V[:] = f(V[:],Ca[:])
# alpha_X[:] = ...
# b_X[:] = ...
# tau_X[:] = ...
# X[:]_inf = ...
#
# To initialize variables
# V[:]_init = ...
# V[2]_init = ...
#
import argparse
import re
import sys
from subprocess import call
import warnings

arrays = dict()                # dictionary of array variables and their size
arraynot = r'\[(?P<slice>[\d:^\s-]+)\]'  # simple array slice notation
muf = r'(\s*\.\w+\(.*?\)\s*;)'           # markup function
derived = ['a','alpha','b','beta','tau','d', 'diff' ]   # derived variables

#standard name for flattened array members
# v = variable name
# i = instance
def flatt( v, i ) :
    return v+'__'+str(i)

#automatically defined variables 
def add_derived_vars( v, n ) :
    global arrays, derived
    for pref in derived :
        arrays[ pref+'_'+v] = n

#was variable automatically defined or explicitly
def is_derived( v ) :
    global arrays
    m = re.match( r'(\w+)_(\w+)', v )
    if not m : return False
    return True if (m.group(1) in derived and m.group(2) in arrays) else False

#handle negative slice indicies
def rnum( exp, n, empty ):
    num = int(exp) if exp else empty
    if num < 0 : num += n
    return num

#strip comments
def remove_comments( f ) :
    f = re.sub( '#.*', '', f )
    return f


#try to make output nicely spaced
def prettify(f):
    #remove white space around newlines
    ws_re = re.compile( r'\s*\n\s*' )
    f = ws_re.sub( '\n', f )
    #add linebreak after each opening brace
    ws_re = re.compile( r'\{(\S)' )
    f = ws_re.sub( r'{\n\1', f )
    #add linebreak before  each closing brace
    ws_re = re.compile( r'(\S)[ \t]*\}' )
    f = ws_re.sub( r'\1\n}', f )
    #add linebreak before each if
    ws_re = re.compile( r'^if\(', re.M )
    f = ws_re.sub( r'\nif(', f )
    #add linebreak before group
    group_re = re.compile( r'group\s*(\{.*?\})('+muf+')*', re.S )
    m = group_re.search( f )
    while m :
        f = f[:m.start()]+'\ngroup '+f[m.start(1):m.end()]+'\n'+f[m.end():]
        m = group_re.search( f, m.end() )
    #markup functions cannot start a new line
    ws_re = re.compile( r'\n\.' )
    f = ws_re.sub( '.', f )
    #indent
    fpretty = ''
    indent = 0
    indent_str = '    '
    for c in f : 
        if c == '{' : indent += 1
        if c == '}' : 
            indent -= 1
            if indent<0: raise Exception('Unmatch braces!')
            fpretty = fpretty[:-len(indent_str)]
        fpretty += c
        if c == '\n' and indent :
            fpretty += indent_str*indent
    #format elif
    ifs_re = re.compile( r'\}\s*elif(.*?)\s*{' )
    fpretty = ifs_re.sub( r'} elif\1 {', fpretty )
    #format else
    ifs_re = re.compile( r'\}\s*else\s*{' )
    fpretty = ifs_re.sub( r'} else {', fpretty )
    return fpretty;


#return list of array elements from slice notation
# expr : array slice
# n : number of elements in array
def arr_eles( expr, n ) :
    expr = re.sub('\s', '', expr )
    complement = False
    if expr[0] == '^' :
        expr = expr[1:]
        complement = True
    if expr.count(':') == 0 :
        m = re.search( r'(?P<fr>-?\d*)', expr )
        if not m : raise Exception('Illegal array slice: '+expr )
        start = rnum(m.group('fr'),n,0 )
        end   = start+1
        inc   = 1
    elif expr.count(':') == 1 :
        m = re.search( r'(?P<fr>-?\d*)?:(?P<to>-?\d*)?', expr )
        if not m : raise Exception('Illegal array slice: '+expr )
        start = rnum(m.group('fr'), n, 0 )
        end   = rnum(m.group('to'), n, n )
        inc   = 1
    else :
        m = re.search( r'(?P<fr>-?\d*)?:(?P<to>-?\d*)?:(?P<inc>\d+)', expr )
        if not m : raise Exception('Illegal array slice: '+expr )
        start = rnum(m.group('fr'), n, 0 )
        end   = rnum(m.group('to'), n, n )
        inc   = int(m.group('inc'))

    ind = range( start, end, inc )
    if complement:
        indcomp = [ i for i in range(n) if (i<start or i>=end)  ]
        return indcomp
    else :
        return ind
        

# quick parse for potential errors in syntax
def error_check( f ) :
    m = re.search( r'(\w+)\[.*?\]', f )
    if m :
        warnings.warn("Warning: possible array not declared: "+m.group(1))
    m = re.search( r'INDEX', f )
    if m :
        raise Exception('INDEX used out of context')


#search for declarations of the form "needle[] =" 
# f = text to search
# n = size of array
def search( f, n, needle ) :
    f = gen_search_rep( f, n, r'(?P<var>'+needle+')'+arraynot ) 
    return f

#search for declarations of the form "needle[]_post ="
# f = text to search
# n = size of array
def postSearch( f, n, needle, post ) :
    f = gen_search_rep( f, n, r'(?P<var>'+needle+')'+arraynot+post ) 
    return f


# generic search and replace for assignments
# replace the matches from the search text
# also, unrolls arrays in the RHS
#
# finds : list of assignments found, one for each member of array
# f     : text to search
# n     : size of array being searched
# all_str = LHS of assignment
def gen_search_rep( f, n, all_str ) :
    all_str = r'(\s|;|\{)\s*(?P<rem>'+all_str
    all_str += r'\s*=\s*(?P<rhs>.*?;)(?P<MUFs>'+muf+r'*))'
    findall_re = re.compile( all_str, re.S|re.M )
    m = findall_re.search( f )
    while m :
        replace = m.group(1)+'\n'
        inds = arr_eles( m.group('slice'), n )
        for i,idx in enumerate(inds) : 
            replace += fix_rhs(m.group('rem'),i)+'\n'
        f = f[:m.start(0)]+replace+'\n'+f[m.end(0):]
        m = findall_re.search( f )
    return f


# substitute array expressions in RHS expression 
#
# rhs - an expression
# i   - which entry of the array slice to use
def fix_rhs( rhs, i ):
    subscr_re = re.compile( r'(\w+)'+arraynot )
    it = subscr_re.finditer( rhs ) 
    for m in it :
        var = m.group(1)
        entries = arr_eles(m.group('slice'),arrays[var])
        if i>=len(entries) :
            raise Exception("Array slice not large enough for "+m.group(1)+' in "'+rhs+'"')
        rhs = rhs.replace( m.group(0),  flatt(m.group(1),entries[i]), 1 )
    rhs = rhs.replace( opts.index, str(i) )
    return rhs.strip()


#find the array declarations
def find_arrays( f, defsz ) :
    global arrays
    # first find simple array declarations
    arrfun = r'(?P<arrdec>\s*\.array\(\s*(?P<N>\d*)\s*\)\s*;)'
    sing_arr = re.compile( r'(?P<var>\w+)\s*;' #the variable
                    + r'(?P<mfs>'+muf+r'*' # possible functions
                    + arrfun               # array function 
                    + muf+r'*)',           # more possible functions, 
                    re.M )
    m = sing_arr.search( f )
    while m :
        arrays[m.group('var')] = int(m.group('N')) if m.group('N') else opts.def_size
        add_derived_vars(m.group('var'),arrays[m.group('var')])
        fcns = m.group('mfs').replace(m.group('arrdec'), '').strip()
        replace = ''
        if fcns :
            replace += 'group {\n'
            for i in range(int(m.group('N'))): 
                replace += '\t'+flatt(m.group('var'),i)+';\n'
            replace+= '}'+fcns+'\n\n'
        f = f[:m.start()] + replace +f[m.end():]
        m = sing_arr.search( f )
    # now the grouped ones
    group_arr_decl =  re.compile( r'group\s*\{(.*?)\}' # the variables
                    + r'(?P<mfs>'+muf+r'*' # possible functions
                    + arrfun               # array function 
                    + muf+r'*)',           # more possible functions, 
                     re.S)
    var_re = re.compile( r'(\w+)\s*;', re.M )
    m = group_arr_decl.search( f )
    while m :
        fcns = m.group('mfs').replace(m.group('arrdec'), '').strip()
        replace = 'group {\n' if fcns else ''
        num = int(m.group('N')) if m.group('N') else opts.def_size
        itvar = var_re.finditer( m.group(1) )
        for mv in itvar :
            arrays[mv.group(1)] = num
            add_derived_vars(mv.group(1),num)
            if fcns :
                for i in range(num) :
                    replace += '\t'+flatt(mv.group(1),i)+';\n'
        if fcns : replace += '}'+fcns+'\n\n'
        f = f[:m.start()]+replace+f[m.end():]
        m = group_arr_decl.search( f, m.start()+len(replace) )
    return f


#return the string contained within delimiters
#
# f        : string to search
# pos      : starting point for search
# startdel : starting delimiter
# enddel   : ending delimiter
def get_contents( f, pos, startdel, enddel ) :
    while f[pos] != startdel : pos += 1
    endpos=pos+1
    count = 1
    while count :
        if f[endpos] == startdel : count += 1
        if f[endpos] == enddel : count -= 1
        endpos += 1
    return f[pos+1:endpos-1], endpos


# determine whether the if continues with an elif or an else
def elif_else( s ) :
    ma = re.match( r'\s*((elif\W)|(else\W))', s )
    return 0 if ma is None else 1


# return the if clause, the predicate and the starting and ending indices of the if
# statement
def get_if( f, cpos ):
    clause, tpos = get_contents( f, cpos, '(', ')' )
    m = re.search( arraynot, clause )
    arrayed = True if m else False
    pred, pos = get_contents( f, tpos, '{', '}' )
    while elif_else( f[pos:] ) :
        pred, pos = get_contents( f, pos, '{', '}' ) 
    return clause, tpos, pos, arrayed


# if an if clause has an array, unroll it 
def unroll_if_arrays( f, start, end ) :
    replace = ''
    m = re.search( r'(\w+)'+arraynot, f[start:end]  )
    inds = arr_eles( m.group('slice'), arrays[m.group(1)] )
    for i,idx in enumerate(inds) : 
        replace += fix_rhs( f[start:end],i )+'\n'
    return replace
    

# unroll ifs  
# note that it can be recursive
def unroll_ifs( f ):
    cpos = 0
    m = re.search( '(\W)if\s*\(', f )
    while m :
        cpos = m.start(0)+cpos
        ifclause, consq, consqend, arrayed = get_if( f, cpos )
        if arrayed : 
            replace = unroll_if_arrays( f, cpos, consqend )
            f = f[:cpos]+replace+f[consqend:]+'\n'
        else :
            replace = unroll_ifs( f[consq:consqend] ) 
            f = f[:consq]+replace+f[consqend:]+'\n'
        cpos += len(replace)
        m = re.search( '\Wif\s*\(', f[cpos:] )

    return f
    

# unroll statements which have no RHS
# f = text 
# v = var
# N = size of var
def simple_unroll( f, v, N ) :
    simpvar_re = re.compile( r'(^|;|\{)(?P<meat>\s*(?P<var>\w+)' #leading white space and var
                        +arraynot+r'(?P<suf>_i(nit)|(nf))?' #the slice
                        +r'\s*;(?P<MUFs>'+muf+r'*))' #trailing markup functions
                         )
    it = simpvar_re.finditer( f )
    for m in it :
        replace = ''
        inds = arr_eles( m.group('slice'), N )
        suf = m.group('suf') if m.group('suf') else ''
        for i,idx in enumerate(inds) : 
            replace += flatt( m.group('var'),idx)+suf+';'+m.group('MUFs')+'\n'
        f = f.replace( m.group('meat'), replace )
    
    return f


#unroll array uses
def unroll( f, v, N, re, label=None, post=False, cntNone=False ) :
    if post :
        f = postSearch( f, N, v, re )
    else :
        f = search( f, N, re+v )
    return f


if __name__=='__main__':

    parser = argparse.ArgumentParser(description='fake arrays in limpet_fe')
    parser.add_argument('arrayed',  help='original model file with array syntax' )
    parser.add_argument('unrolled', nargs='?',
               help='output model (stdout)',default=None )
    parser.add_argument('--def-size', type=int, default=10, help='default array size (10)' )
    parser.add_argument('--index', default='__INDEX__', help='loop index in RHS expr (__INDEX__)' )
    parser.add_argument('--process', action='store_true', help='run limpet_fe on unrolled file' )
    parser.add_argument('--limpet-fe', default='limpet_fe.py', help='limpet_fe program (limpet_fe.py)' )
    opts = parser.parse_args()

    if opts.arrayed[-6:] != '.model' : opts.arrayed += '.model'
    with open( opts.arrayed, 'r' ) as origin :
        f = origin.read()

    f = remove_comments( f )
    f = find_arrays( f, opts.def_size )
    f = unroll_ifs( f ) 

    # look for alphas, betas, taus, infinities and assignments
    for v in arrays :
        f = unroll( f, v, arrays[v],  ''    )
        f = simple_unroll( f, v, arrays[v] )
        if is_derived( v ) : continue
        f = unroll( f, v, arrays[v],  r'_init', '_init', True )
        f = unroll( f, v, arrays[v],  r'_inf', '_inf', True  )

    # now replace remaining array vars which should be specific instances
    subscr_re = re.compile( r'(\w+)\[\s*(\d+)\s*]', re.M )
    it = subscr_re.finditer( f )
    for m in it :
        if m.group(1) not in arrays :
            raise Exception("Array not declared: "+m.group(0))
        if arrays[m.group(1)] <= int(m.group(2)):
            raise Exception('Array out of range: '+m.group(0) )
        f = f.replace( m.group(0), flatt(m.group(1),int(m.group(2))), 1 )

    error_check( f )

    f = prettify( f )

    #output file
    if not opts.unrolled or opts.unrolled=='-' :
        if opts.process :
            print('\n*** Need to specify an output file to process ***\n')
            exit(1)
        sys.stdout.write(f+'\n')
    else :
        if opts.unrolled[-6:] != '.model' : opts.unrolled+='.model'
        if opts.unrolled == opts.arrayed :
            print('\n**** the output model name must differ from the input ***')
            exit
        with open( opts.unrolled, 'w' ) as outfile:
            outfile.write( f+'\n' )
        if opts.process :
            call(  [opts.limpet_fe, opts.unrolled] )
            
