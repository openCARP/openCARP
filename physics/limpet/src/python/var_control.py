# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'var_control.ui'
#
# Created: Thu Apr  7 13:19:27 2011
#      by: PyQt4 UI code generator 4.7.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_VarControl(object):
    def setupUi(self, VarControl):
        VarControl.setObjectName("VarControl")
        VarControl.resize(394, 48)
        self.hboxlayout = QtGui.QHBoxLayout(VarControl)
        self.hboxlayout.setSpacing(0)
        self.hboxlayout.setMargin(0)
        self.hboxlayout.setObjectName("hboxlayout")
        self.wView = QtGui.QToolButton(VarControl)
        self.wView.setCheckable(True)
        self.wView.setChecked(True)
        self.wView.setObjectName("wView")
        self.hboxlayout.addWidget(self.wView)
        self.wClamp = QtGui.QToolButton(VarControl)
        self.wClamp.setCheckable(True)
        self.wClamp.setObjectName("wClamp")
        self.hboxlayout.addWidget(self.wClamp)
        self.wName = QtGui.QLineEdit(VarControl)
        self.wName.setObjectName("wName")
        self.hboxlayout.addWidget(self.wName)
        self.wClose = QtGui.QToolButton(VarControl)
        self.wClose.setObjectName("wClose")
        self.hboxlayout.addWidget(self.wClose)

        self.retranslateUi(VarControl)
        QtCore.QMetaObject.connectSlotsByName(VarControl)

    def retranslateUi(self, VarControl):
        VarControl.setWindowTitle(QtGui.QApplication.translate("VarControl", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.wView.setToolTip(QtGui.QApplication.translate("VarControl", "View the graph  of this variable", None, QtGui.QApplication.UnicodeUTF8))
        self.wView.setText(QtGui.QApplication.translate("VarControl", "V", None, QtGui.QApplication.UnicodeUTF8))
        self.wClamp.setToolTip(QtGui.QApplication.translate("VarControl", "Clamp-- Use the saved trace in the next computation", None, QtGui.QApplication.UnicodeUTF8))
        self.wClamp.setText(QtGui.QApplication.translate("VarControl", "C", None, QtGui.QApplication.UnicodeUTF8))
        self.wClose.setToolTip(QtGui.QApplication.translate("VarControl", "Close", None, QtGui.QApplication.UnicodeUTF8))
        self.wClose.setText(QtGui.QApplication.translate("VarControl", "X", None, QtGui.QApplication.UnicodeUTF8))

