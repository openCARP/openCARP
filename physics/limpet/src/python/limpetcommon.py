# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------


# printf format characters for variable types
prchar = {"Gatetype":     "g",
          "Real":         "g",
          "float":        "g",
          "char":         "c",
          "int":          "d",
          "double":       "f",
          "short":        "d",
          "GlobalData_t": "g",
          "long":         "d",
          "bool":         "d",}

vartypes = prchar.keys()

# scanf format characters for variable types
scanchar = { "Gatetype":     "f",
             "Real":         "lf",
             "float":        "f",
             "char":         "c",
             "int":          "d",
             "double":       "lf",
             "short":        "hd",
             "GlobalData_t": "f" ,
             "long":         "ld",    
             "bool":         "c",}

# indices for sv datatypes 
typeID =  { "Gatetype":     0,
            "Real":         1,
            "float":        2,
            "char":         3,
            "int":          4,
            "double":       5,
            "short":        6,
            "GlobalData_t": 7,
            "long":         8,
            "bool":         9,};

import re
import sys
import os


def find_file(filename):
    imp_list = ['']
    # (bindir, f) = os.path.split(sys.argv[0])
    # if (len(bindir) > 0):
    #     imp_list.append(bindir+"/../include/")
    #     imp_list.append(bindir+"/")
    # We append here potential locations of imp_list.txt relative to Makefile dir
    imp_list.append("./models/")
    location = False
    for imp_list_file in imp_list:
        try:
            trial_location = imp_list_file+filename
            open(trial_location,'r')
            location = trial_location
            break
        except IOError:
            pass
    if not location:
        print("Error: Can't open "+filename+"!  Perhaps you should use the full path name to the file?")
        sys.exit(1)
    return location

def readFile(filename):
    return open(find_file(filename), encoding="utf-8")

def readIMPs(imp_file):
    f = readFile(imp_file)
    IMPS = []; PLUGS = []; I_DATA_T = [];
    for line in f:
        line = re.sub(r'#.*$','',line)
        line = line.strip()
        if not line:
            continue

        m = re.match(r'model\s+(\w+)\s*(.*?)$',line)
        if m:
            name = m.group(1)
            parameters = set(re.split(r'\s+',m.group(2)))
            if not "generated" in parameters:
                continue
            if "plugin" in parameters:
                PLUGS.append(name)
            else:
                IMPS.append(name)
        m = re.match(r'variable\s+(\w+)\+(\w+)\s+(.*?)$',line)
        if m and m.group(1) == 'public':
            name = m.group(2)
            I_DATA_T.append(name)
    return (IMPS, PLUGS, I_DATA_T)

def open_header(imp):
    return readFile(imp+".h")

# Auto-prints a string using the new (as of 2.4) string-template
# based substitution.
import copy, inspect
try:
    from string import Template
except ImportError:
    from stringtemplate import Template # backport for 2.3
try:
    set()
except:
    from sets import Set as set # backport for 2.3

def printfmt(template):
    frame = inspect.stack()[1][0]
    try:
        var = copy.copy(frame.f_globals)
        var.update(frame.f_locals)
        print(Template(template).safe_substitute(var)),
    finally:
        del frame


def print_model_functions(modelbase, impsrc_dir):
    model = re.sub(r'^.*/','',modelbase)

    model_header_base = os.path.join(impsrc_dir, modelbase);

    printfmt("////////////////////////////////////////////////\n")
    printfmt("#include \"$model.h\"\n\n")

    printfmt("""
namespace limpet {

void ${model}IonType::tune(IonIfBase& iif_base, const char* im_par) const {
  IonIfDerived& iif = static_cast<IonIfDerived&>(iif_base);
  ${model}_Params *p;							// pointer to parameter structure
  char parameter[1024], mod[1024];

  p = iif.params();

  // make sure flags is a valid member and flags specified are valid
  char *npar, *par, *parptr;
  par=tokstr_r(npar = dupstr(im_par), ",", &parptr);
  while( par ) {
    if( !strncmp(par, "flags=", 6 ) ) {
""")

    f = open_header(model_header_base)
    empty_params = False
    for line in f:
        if re.search(r'struct.*_Params',line):
            if re.search(r'}', line):
                empty_params = True
            break;
    flag_found = 0;
    if not empty_params:
        for line in f:
            if (re.search("};",line)
                or re.search("UNMODIFIABLE BELOW HERE",line)):
                break
            #ignore comments
            line = re.sub("//.*",'',line)
            line = line.strip()
            if not line:
                continue
            # parse the variables
            if re.search(r'\w+\s*\*\s*flags\s*;',line):
                flag_found = 1
                break
    if flag_found:
        flag_offset = len("flags")+1;
        printfmt("""
      bool valid_flags = verify_flags(${model}_flags, par+$flag_offset);
      if( !valid_flags ) {
        log_msg(_nc_logf, 5, 0, "Illegal flag specified: %s\\n", par );
        exit( 1 );
      }
      ((${model}_Params *)p)->flags = dupstr(par+$flag_offset);
      iif.initialize_params();
""")
    else:
        printfmt("""
      log_msg( _nc_logf, 5, 0, "Unrecognized parameter: flags\\n" );
      exit(1);
""")
    printfmt("""
    }
    par = tokstr_r( NULL, ",", &parptr );
  }
  free(npar);

  // now process the regular parameters
  par=tokstr_r(npar = dupstr(im_par), ",", &parptr);
  while( par ) {
    process_param_mod( par, parameter, mod );
    if (0) ;
""")

    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_Params',line):
            break;
    if not empty_params:
        for line in f:
            if (re.search("};",line)
                or re.search("UNMODIFIABLE BELOW HERE",line)):
                break
            #ignore comments
            line = re.sub("//.*$",'',line)
            line = line.strip()
            if not line:
                continue
            # parse the variables
            m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
            if m:
                if m.group(1) in vartypes:
                    param = m.group(2)
                    printfmt("    else if( !strcmp( \"$param\", parameter ) )\n")
                    printfmt("      CHANGE_PARAM( $model, p, $param, mod );\n")
            if re.search(r'^\s*#', line):
                printfmt(line)
    printfmt("""
    else if( !strcmp( \"flags\", parameter ) )
      ;
    else {
      log_msg( _nc_logf, 5, 0,"Unrecognized parameter: %s.\\n",parameter);
      log_msg( _nc_logf, 5, 0,"Run bench --imp=YourModel --imp-info to get a list of all parameters.\\n",parameter);
      exit(1);
    }
    par = tokstr_r( NULL, ",", &parptr );
  }
  free( npar );
}


""")

    printfmt("""
/** output all parameters which may be tuned for all IMPs
 */
void ${model}IonType::print_params() const {
  IonIfDerived IF{*this, Target::AUTO, 0, {}};
  IF.initialize_params();
  printf("Name: $model\\n" );
  printf("\\tParameters:\\n" );
""")
    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_Params',line):
            break;
    if not empty_params:
        for line in f:
            if (re.search("};",line)
                or re.search("UNMODIFIABLE BELOW HERE",line)):
                break
            #ignore comments
            line = re.sub("//.*$",'',line)
            line = line.strip()
            if not line:
                continue
            # parse the variables
            m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
            if m:
                if m.group(1) in vartypes:
                    print_char = prchar[m.group(1)]
                    param = m.group(2)
                    printfmt("  printf( \"\\t%32s\\t%$print_char\\n\",\"$param\", IF.params()->$param );\n")
            if re.search(r'^\s*#', line):
                printfmt(line)
        f.seek(1)
        for line in f:
            m=re.search(model+"_flags\s*=\s*(\S+);",line)
            if (m):
                printfmt("  printf( \"\\t%32s\\t%s\\n\",\"flags\", "+m.group(1)+" );\n")
                break

    printfmt("""
}

""")

    printfmt("""
int ${model}IonType::write_svs(IonIfBase& IF_base, FILE *out, int node) const {
  IonIfDerived& IF = static_cast<IonIfDerived&>(IF_base);
  fprintf( out, "%s\\n", IF.get_type().get_name().c_str() );
""")
    struct_decl = "  ${model}_state *sv = IF.sv_tab().data()+(node / (this->dlo_vector_size()));\n";
    printfmt("  int inner_id = node % this->dlo_vector_size();\n")
    decl_pr = 0;
    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_state',line):
            break;
    for line in f:
        if re.search("};",line):
            break
        #remove comments.
        line = re.sub(r'//.*$','',line)
        if not decl_pr:	#only print the structure def if it exists
            printfmt(struct_decl)
            decl_pr = 1;
        m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
        if m:
            print_char = prchar[m.group(1)]
            state = m.group(2)
            if m.group(3) is None:
                printfmt("  fprintf( out, \"%-20$print_char# $state\\n\", sv->$state );\n")
            else:
                printfmt("  fprintf( out, \"%-20$print_char# $state\\n\", sv->$state[inner_id] );\n")
        # pass preprocessor directives
        if re.search(r'^\s*#', line):
            printfmt(line)
    printfmt("""
  fprintf( out, "\\n");
  return 0;
}

""")

    # read in a header file and generate a C code to read in the state variables
    #
    # 1 parameter: the base name of the IMP
    #              eg read_model MBRDR;
    printfmt("""
int ${model}IonType::read_svs(IonIfBase& IF_base, FILE *in) const {
  IonIfDerived& IF = static_cast<IonIfDerived&>(IF_base);
  const int  BUFSIZE=256;
  char       impname[256], buf[BUFSIZE];
  const char *gdt_sc = sizeof(GlobalData_t)==sizeof(float)?"%f":"%lf";
  int flg = 0;
  
  // skip possible empty lines
  do {
    if( fgets(buf,BUFSIZE,in)==NULL ) {
      log_msg( _nc_logf, 4, 0, "no state information for IMP: %s\\n", IF.get_type().get_name().c_str() );
      return 2;
    }
  } while( *buf=='\\n' );
  sscanf( buf, "%s", impname );

  if( strcmp( impname, IF.get_type().get_name().c_str() ) ) {
    log_msg( _nc_logf, 5, 0, "IMPs do not match region (%s vs %s). Skipping this statefile.\\n",impname, IF.get_type().get_name().c_str());
    return 2;
  }
""")

    first = 1
    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_state',line):
            break;
    for line in f:
        if re.search("};",line):
            break
        #remove comments.
        line = re.sub(r'//.*$','',line)
        m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
        if m:
            if first:
                first = 0
                printfmt("""
  if(!IF.get_num_node())  {
    flg = 1;
  }
  ${model}_state *sv = IF.sv_tab().data();
""")
            print_char = prchar[m.group(1)]
            scan_char = scanchar[m.group(1)]
            state = m.group(2)
            # If array access brackets are matched, then the structure is using
            # Data Layout Optimization
            data_layout_opt = not (m.group(3) is None)
            if data_layout_opt:
                addr_op = ""
            else:
                addr_op = "&"
            if m.group(1) == "GlobalData_t":
                printfmt("  sscanf( fgets(buf,BUFSIZE,in), gdt_sc, ${addr_op}sv->$state );\n" )
            else:
                printfmt("  sscanf( fgets(buf,BUFSIZE,in), \"%$scan_char\", ${addr_op}sv->$state );\n")
            # When reading in an AoSoA structure, we want to propagate the value to the whole vector
            # so that the rest of the code only has to copy the first chunk to replicate values accross
            # every cell
            if data_layout_opt:
                printfmt("  for (int inner_id = 1; inner_id < IF.get_type().dlo_vector_size(); ++inner_id)\n")
                printfmt("    sv->${state}[inner_id] = sv->${state}[0];\n")

        # pass preprocessor directives
        if re.search(r'^\s*#', line):
            printfmt(line)
    printfmt("""
  return flg;
}

""")

  
    # read in a header file and generate a C code to determine size and offset of a sv
    #
    # 1 parameter: the base name of the IMP
    #              eg read_model MBRDR;
    printfmt("""
SVgetfcn ${model}IonType::get_sv_offset(const char *svname, int *off, int *sz) const {
  SVgetfcn retall = (SVgetfcn)(1);

""")

    struct_decl = "        ${model}_state *sv;\n";
    struct      = "${model}_state";
    decl_pr = 0
    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_state',line):
            break;
    for line in f:
        if re.search("};",line):
            break
        #remove comments.
        line = re.sub(r'//.*$','',line)
        if not decl_pr:	#only print the structure def if it exists
            printfmt(struct_decl)
            printfmt("        if( !strcmp(svname,\"ALL_SV\") )  {\n")
            printfmt("          *off  = 0;\n")
            printfmt("          *sz   = sizeof(${model}_state);\n")
            printfmt("          return retall;\n")
            printfmt("        }\n")
            decl_pr = 1
        m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
        if m:
            vartype = m.group(1)
            state = m.group(2)
            printfmt("        if( !strcmp(svname,\"$state\") )  {\n")
            printfmt("          *off  = offsetof(${model}_state,$state);\n")
            printfmt("          *sz   = sizeof  (sv->$state) / this->dlo_vector_size();\n")
            printfmt("          return get${vartype}SV;\n")
            printfmt("        }\n")
        # pass preprocessor directives
        if re.search(r'^\s*#', line):
            printfmt(line)
    printfmt("""
  return NULL;
}

""")

    printfmt("""
int ${model}IonType::get_sv_list(char*** list) const {
""")
    svlist = [];

    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_state',line):
            break;
    for line in f:
        if re.search("};",line):
            break
        #remove comments.
        line = re.sub(r'//.*$','',line)
        m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
        if m:
            svlist.append(m.group(2))
        # pass preprocessor directives
        if re.search(r'^\s*#', line):
            printfmt(line)
    sv_count = len(svlist)
    printfmt("  *list = (char**)malloc( sizeof(char*)*$sv_count );\n")
    index = 0
    for SV in svlist:
        printfmt("  (*list)[$index] = dupstr(\"$SV\");\n")
        index += 1
    printfmt("  return $sv_count;\n")
    printfmt("""
}

""")

    printfmt("""
#define BOGUSTYPE -1
int ${model}IonType::get_sv_type(const char *svname, int *type, char **Typename) const
{
  *type = BOGUSTYPE;
  if (0) ;
""")

    f = open_header(model_header_base)
    for line in f:
        if re.search(r'struct.*_state',line):
            break;
    for line in f:
        if re.search("};",line):
            break
        #remove comments.
        line = re.sub(r'//.*$','',line)
        m = re.search(r'(\w+)\s+(\w+)\s*(\[\d+])?\s*;',line)
        if m:
            this_typeID = typeID[m.group(1)]
            state = m.group(2)
            printfmt("  else if( !strcmp(svname,\"$state\") )")
            printfmt(" *type = $this_typeID;\n")
        # pass preprocessor directives
        if re.search(r'^\s*#', line):
            printfmt(line)
    printfmt("""
  else return 0;
  *Typename = get_typename(*type);
  return 1;
}

""")
    f = open_header(model_header_base)
    with f as file:
      lines = file.readlines()

    keywords=[
        "Authors",
        "Year",
        "Title",
        "Journal",
        "DOI",
        "Comment"
        ]

    ref_vars = {'model': model, 'Authors': '', 'Year': '', 'Title': '', 'Journal': '', 'DOI': '', 'Comment': ''}
    for line in lines:
        if line == "\n" or not line.lstrip() or line.lstrip()[0] != "*":
            continue
        for keyword in keywords:
            if re.search(rf"{keyword}", line, flags=re.IGNORECASE) is not None: 
                ref_vars[keyword] = keyword + ": " + line.split(":",1)[1].strip()

    print(Template("""

void ${model}IonType::print_metadata() const {
  printf("Metadata:\\n");
  printf("\\t$Authors\\n");
  printf("\\t$Year\\n");
  printf("\\t$Title\\n");
  printf("\\t$Journal\\n");
  printf("\\t$DOI\\n");
  printf("\\t$Comment\\n");
}

}  // namespace limpet

        """).safe_substitute(ref_vars))

def print_static_model_decl(model,is_plugin):
    if is_plugin:
        is_plugin = 1
    else:
        is_plugin = 0
    model_upper = model.upper()
    printfmt("""
  TYPES.push_back(new ${model}IonType($is_plugin));
""")

def print_dynamic_model_decl(model,is_plugin):
    if is_plugin:
        is_plugin = 1
    else:
        is_plugin = 0

    printfmt("""
namespace limpet {
extern "C" {
  int __is_plugin = $is_plugin;
  char __name[] = "$model";
  void (*__initialize_params)(ION_IF *) = initialize_params_$model;
  void (*__construct_tables)(ION_IF *) = construct_tables_$model;
  void (*__destroy)(ION_IF *) = destroy_$model;
  void (*__initialize_sv)(ION_IF *, GlobalData_t**) = initialize_sv_$model;
  void (*__compute)(int, int, ION_IF *, GlobalData_t**) = compute_$model;
  void      (*__tune)(ION_IF*, const char*) = tune_IMP_$model;
  void      (*__print_params)() = print_IMP_parameters_$model;
  int       (*__read_svs)(ION_IF*, FILE*) = read_IIF_svs_$model;
  int       (*__write_svs)(ION_IF*, FILE*, int) = write_IIF_svs_$model;
  SVgetfcn  (*__get_sv_offset)(ION_TYPE, const char*, int*, int*) = get_sv_offset_$model;
  int       (*__get_sv_list)(char ***) = get_sv_list_$model;
  int       (*__get_sv_type)(const char*, int *, char **) = get_sv_type_$model;
  unsigned int __reqdat = ${model}_REQDAT;
  unsigned int __moddat = ${model}_MODDAT;
  size_t __params_size = sizeof(${model}_Params);
  void      (*__print_metadata)() = print_IMP_metadata_$model;
}
}
""")

def load_dynamic_type():
    print("""
  allocator = (IonType* (*)(bool))dlsym(handle, "__new_IonType");
  {
    char* msg = dlerror(); 
    if (msg) { 
      log_msg( _nc_logf, 5, 0, "%s", msg); errors++; 
    }
  }
""")

def print_type_decl():
    print("""
struct ion_type {
    bool        is_plugin;            //!< true if model is a plugin.
    char        name[256];            //!< model name
    /* functions */
    void      (*initialize_params)(ION_IF *); //!< initialize parameters
    void      (*cstrct_lut)(ION_IF *);        //!< construct lookup tables
    void      (*destroy)(ION_IF *);           //!< destroy IMP
    //! build and initialize state variable table
    void      (*initialize_sv)(ION_IF *, GlobalData_t **); 
    //! perform computation for 1 time step
    void      (*compute)(int, int, ION_IF *, GlobalData_t **);
    void      (*trace)(ION_IF*, int, FILE*, GlobalData_t **);
    void      (*tune)(ION_IF*, const char*);
    void      (*print_params)();
    int       (*read_svs)(ION_IF*, FILE*);
    void      (*write_svs)(ION_IF*, FILE*, int);
    SVgetfcn  (*get_sv_offset)(ION_TYPE, const char*, int*, int*);
    int       (*get_sv_list)(ION_TYPE, char ***);
    int       (*get_sv_type)(const char*, int *, char **);
    void      (*print_metadata)();

	//! data that must be externally allocated
	unsigned int reqdat;                     
	//! externally allocated data that is modified by ionic_model and plugins 
	unsigned int moddat;                     
        size_t      params_size;                  //!< number of bytes needed to represent the params structure.
};
""")


