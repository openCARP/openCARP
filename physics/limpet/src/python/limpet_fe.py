#!/usr/bin/env python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

##
# \todo Please add documentation to this file
#

import genericpath
from im import *
from unit import si,log_diff
from enum import Enum, auto
import re
import parser
import os
import sys
import math

# Auto-prints a string using the new (as of 2.4) string-template
# based substitution.
import copy, inspect
try:
    from string import Template
except ImportError:
    from stringtemplate import Template # backport for 2.3



def parse_external_units(filename):
    '''Takes in a filename, extracts all the external
    variables from that filename, and records their units.
    '''
    var_external = {}

    f = open(filename, "r")
    for line in f:
        #remove comments.
        line = re.sub(r'#.*$','',line)
        #remove blank lines.
        line = line.strip()
        if not line:
            continue
        m = re.match(r'variable\s+(\w+)\s+(\w+)\s+(.*?)$',line)
        if m:
            class Blank: pass
            b = Blank();
            b.type = m.group(1)
            b.name = m.group(2)
            b.unit = parser.parse_unit(m.group(3))
            var_external[b.name] = b
    return var_external

def basic(var):
    return var.name

def lookup_format(var, f=basic):
    if var.is_a('lookup'):
        return "%s_row[%s_idx]" % (var.info('lookup').name, var.name)
    else:
        return f(var)

# Caches (in a dictionary) the offset value of a struct field.
# Offset dictionaries are used by the MLIR code generator
def add_to_dictionary_offset(dictionary, var_name, type, offset):

    if type == "GlobalData_t":
        type = "f64"
        import math
        offset = int(math.ceil(float(offset) / 8.0) * 8)
        new_offset = int(math.ceil(float(offset) / 8.0) * 8) + 8
    elif type == "Gatetype":
        type = "f32"
        new_offset = offset + 4
    else:
        raise ValueError("unknown dictionary type")

    dictionary[var_name] = (type, offset)

    return new_offset

def add_to_dictionary_offset_data_layout(dictionary, var_name, type, offset, sizeof_dict, dl_sizeof_offset):

    # Compute the offset and the size of the struct according
    # to the data types.
    # There is no padding to take into account when computing the size of
    # the DLO struct since the vector size will always be even
    if type == "GlobalData_t":
        fp_type = "f64"
        import math
        new_offset = offset + (8 * num_elements)
        offset = int(math.ceil(float(offset) / 8.0))
        dl_sizeof_offset += 8
    elif type == "Gatetype":
        fp_type = "f32"
        import math
        new_offset = offset + (4 * num_elements)
        offset = int(math.ceil(float(offset) / 4.0))
        dl_sizeof_offset += 4
    else:
        raise ValueError("unknown dictionary type")

    dictionary[var_name] = (fp_type, offset)

    return new_offset, dl_sizeof_offset

# Adds a last field in the offset dictionary to store the struct size
def add_size_offset(dictionary, offset):

    # Assume the type to be Gatetype and upgrade to GlobalData_t only if necessary
    # This avoids add_to_dictionary_offset to wrongfully calculate the offset for 'size' key
    type = "Gatetype"
    for first, _ in dictionary.values():
        if first == "f64":
            type = "GlobalData_t"
            break
    add_to_dictionary_offset(dictionary, "size", type, offset)

# Build a dictionary of (value, size) with all possible values from a region.
# Added a 'size' entry at the end
# Regions are defined as cell_geom which has the following definition (found in ION_IF.h.pl:227):
# struct cell_geom {
#     float      SVratio;      //!< single cell surface-to-volume ratio (per um)
#     float      v_cell;       //!< cell volume
#     float      a_cap;        //!< capacitive cell surface
#     float      fr_myo;       //!< volume of myoplasm
#     float      sl_i2c;       //!< convert sl-currents in uA/cm^2 to mM/L without valence
# };
def build_region_dict():
    return {
        "SVratio" : ('f32', 0),
        "v_cell" : ('f32', 4),
        "a_cap" : ('f32', 8),
        "fr_myo" : ('f32', 12),
        "sl_i2c" : ('f32', 16),
        "size" : ('f32', 20)
    }

# An enum to represent targets for code generation
class GenerationTarget(Enum):

    # This allows to add additional attributes to the enum members,
    # like macro guards or suffixes / prefixes
    def __new__(cls, guard_suffix = '', particle = ''):
        # New ID for the created enum member
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj.__value__ = value
        return obj

    def __init__(self, guard_suffix = '', particle = ''):
        self.guard_suffix = guard_suffix
        self.particle = particle
        self.prefix = particle + '_'
        self.suffix = '_' + particle

    # Define the different values of the enum
    CPU_CODEGEN = ('_CPU_GENERATED', 'cpu')
    MLIR_CPU_CODEGEN = ('_MLIR_CPU_GENERATED', 'mlir_cpu')
    MLIR_CUDA_CODEGEN = ('_MLIR_CUDA_GENERATED', 'mlir_gpu_cuda')
    MLIR_ROCM_CODEGEN = ('_MLIR_ROCM_GENERATED', 'mlir_gpu_rocm')
    MLIR_GPU_CODEGEN = ('_MLIR_GPU_GENERATED', 'mlir_gpu')

    # Some utility functions to test the generation type
    def isMlirCodegen(self):
        return self != GenerationTarget.CPU_CODEGEN

    def isMlirGpu(self):
        return self == GenerationTarget.MLIR_CUDA_CODEGEN or self == GenerationTarget.MLIR_ROCM_CODEGEN

class Limpet:
    def __init__(self, root, name, external_units, enable_mlir_codegen):
        self.root = root
        (self.directory, self.filebase) = os.path.split(name)

        if len(self.directory) == 0:
            self.directory = '.'

        self.external_units = external_units
        self.private = Set()
        self.public = Set()

        # Dictionaries to construct MLIR structs and its offsets
        # Parameters dictionaries
        self.params_dict = dict()
        self.state_dict = dict()
        # Offset dictionary for use with the optimized data layout
        self.dl_state_dict = dict()
        self.rosenbrock_dict = dict()

        # Tables dictionaries for search for lookup tables
        self.tables = []
        # Enumerators dictionaries for lookup tables
        self.enumerators_dict = dict()
        # Dictionary of function pointers
        self.function_pointer_info = dict()

        for var in self.root.getVars('external'):
            for e in list(external_units.values()):
                if e.name == var.info('external'):
                    if e.type == "private":
                        self.private.add(var)
                        break
                    else:
                        self.public.add(var)
                        break
            else:
                self.private.add(var)
        self.ext_regional = self.root.getVars('external') & self.root.getVars('regional')
        self.ext_nodal = self.root.getVars('external') & self.root.getVars('nodal')
        #the code below will incorrectly set a variable as a reqvar if
        #it is assigned before it is used.  I can fix this by doing
        #dependency analysis, but it's complicated and the need hasn't
        #come up yet.
        self.reqvars = self.ext_nodal & (self.root.getVars('store') | self.root.used)
        self.modvars = self.ext_nodal & (self.root.assigned | self.root.statevars)

        self.model_name = basename(self.filebase)
        self.value_hash = {
            'header' : self.model_name.upper(),
            'name'  : self.model_name,
            'private' : f'{self.model_name}IonType::private_type',
            'private_vector': f'{self.model_name}IonType::private_type_vector',
            'time_factor' : "1e%d" % log_diff(si.ms,self.root.time_unit),
            }
        if self.root.getVars('cvode'):
            print("WARNING: Model %s: MLIR code generation does not support CVODE integration method. "
                  "Using original codegen..." %self.model_name)
        self.enable_mlir_codegen = enable_mlir_codegen and not self.root.getVars('cvode')

    def type(self, var):
        if var.is_a('gate'):
            return "Gatetype"
        else:
            return "GlobalData_t"

    def printExternalDef(self, out, print_mlir = False):
        out("//Prepare all the public arrays.")
        for var in order((self.reqvars | self.modvars) & self.public):
            out("GlobalData_t *%s_ext = impdata[%s];" % (var.name, var.info('external')))
        out("//Prepare all the private functions.")
        for var in order((self.reqvars | self.modvars) & self.private):
            out('''
int __%(var)s_sizeof;
int __%(var)s_offset;
SVgetfcn __%(var)s_SVgetfcn = imp.parent()->get_type().get_sv_offset( "%(ext)s", &__%(var)s_offset, &__%(var)s_sizeof );
SVputfcn __%(var)s_SVputfcn = __%(var)s_SVgetfcn ? getPutSV(__%(var)s_SVgetfcn) : NULL;
''' % { "var" : var.name, "ext": var.info('external') })
            if print_mlir:
                out('''
// Calculates addresses for the MLIR code generator
int __%(var)s_getfcn_exists = __%(var)s_SVgetfcn ? 1 : 0;
int __%(var)s_putfcn_exists = __%(var)s_SVputfcn ? 1 : 0;
''' % { "var" : var.name })
                # Create __parent_table_address and __parent_table_size only once
                if len(self.function_pointer_info) == 0:
                    if enable_data_layout_opt:
                        vec_size = num_elements
                    else:
                        vec_size = 1
                    out('''
        char* __parent_table_address = (char*)imp.parent()->get_sv_address();
        int __parent_table_size = imp.parent()->get_sv_size() / %(vec_size)s;
    ''' % { "var" : var.name, "vec_size": vec_size })

                # Add new entries to the dictionary of function pointers
                self.function_pointer_info[var.name] = ("__%(var)s_offset" % { "var" : var.name},
                                                        "__%(var)s_getfcn_exists" % { "var" : var.name},
                                                        "__%(var)s_putfcn_exists" % { "var" : var.name})


    def printGetExternal(self, out, dlo=False, dlo_index="inner_id"):
        out("//Initialize the external vars to their current values")
        for var in order((self.reqvars | self.modvars) & self.public):
            if dlo:
                out("GlobalData_t %(var)s = %(var)s_ext[__i + %(dlo_index)s];" % {"var" : var, "dlo_index": dlo_index})
            else:
                out("GlobalData_t %(var)s = %(var)s_ext[__i];" % {"var" : var})
        for var in order((self.reqvars | self.modvars) & self.private):
            if dlo:
                out('GlobalData_t {0} = __{0}_SVgetfcn ? __{0}_SVgetfcn(*imp.parent(), __i, __{0}_offset) :'
                                                                         'sv->__{0}_local[{1}];'.format(var, dlo_index))
            else:
                out('GlobalData_t {0} = __{0}_SVgetfcn ? __{0}_SVgetfcn(*imp.parent(), __i, __{0}_offset) :'
                                                                         'sv->__{0}_local;'.format(var) )

    def printSetExternal(self, out, dlo=False, dlo_index="inner_id"):
        out("//Save all external vars")
        for var in order((self.reqvars | self.modvars) & self.public):
            if dlo:
                out("%(var)s_ext[__i + %(dlo_index)s] = %(var)s;" % {"var" : var, "dlo_index": dlo_index})
            else:
                out("%(var)s_ext[__i] = %(var)s;" % {"var" : var})
        for var in order((self.reqvars | self.modvars) & self.private):
            if dlo:
                # See "printGetExternal" for info about __i / num_elements
                out('if( __{0}_SVputfcn ) \n\t__{0}_SVputfcn(*imp.parent(), __i, __{0}_offset, {0});'
                     '\nelse\n\tsv->__{0}_local[{1}]={0};'.format(var, dlo_index))
            else:
                out('if( __{0}_SVputfcn ) \n\t__{0}_SVputfcn(*imp.parent(), __i, __{0}_offset, {0});'
                     '\nelse\n\tsv->__{0}_local={0};'.format(var))

    def printLookupTableRow(self, out, var, rhs, use_device_getter=False):
        # We need to get a device pointer when generating device code
        getter = "tables()"
        if use_device_getter:
            getter = "tables_d()"
        out('''LUT_data_t %(row)s[NROWS_%(var)s];
LUT_interpRow(&IF->%(lut_getter)s[%(table)s], %(value)s, __i, %(row)s);'''
            % {'row': var.name + "_row",
               'table': var.name + "_TAB",
               'value': rhs(var),
               'var': var.name,
               'model': self.model_name,
               'lut_getter': getter,
               })
        if self.enable_mlir_codegen:
            alloc_str = "LUT_data_t %(row)s[NROWS_%(var)s*%(num_elements)s];" % {'row': var.name + "_row",
                   'var': var.name,
                   'num_elements': num_elements,
                   }
            self.lut_list.append((var.name, alloc_str))

    def changeExternalUnits(self, factor, out, complete_format):
        out("//Change the units of external variables as appropriate.")
        for var in order(self.root.statevars | self.modvars | self.reqvars):
            if var.is_a('unit'):
                current_unit = var.info('unit')
                if var.is_a('external'):
                    name = var.info('external')
                else:
                    name = var.name
                if name in self.external_units:
                    desired_unit = self.external_units[name].unit
                    if current_unit != desired_unit:
                        #print(var,desired_unit,current_unit)
                        out("%s *= 1e%d;" % (complete_format(var), 
                                             factor*log_diff(desired_unit,current_unit)))
        out("\n\n")

    def printEquations(self,
                       out,
                       eqnmap,
                       valid,
                       target,
                       declared=Set(),
                       lhs_format=basic,
                       rhs_format=basic,
                       format="%(decl)s%(lhs)s = %(rhs)s;",
                       lookup=Set(),
                       post_print=lambda var : '',
                       declare_type="GlobalData_t ",
                       use_device_getter=False,
                       ) :
        lookup_dependencies = Set([self.root.table[var.info('lookup').name]
                                   for var in lookup
                                   ])
        all_lookup = eqnmap.dependencies_of(lookup, valid | lookup_dependencies)
        def specific_lookup_format(var, f, lookup=lookup):
            if var in lookup:
                return lookup_format(var)
            else:
                return f(var)
        rhs = lambda v: specific_lookup_format(v, rhs_format)
        lhs = lambda v: specific_lookup_format(v, lhs_format)
        #for var in order(lookup_dependencies & valid):
        #    self.printLookupTableRow(out, var, rhs)
        lookup_dependencies -= valid
        functions = 0
        func_vars = Set()
        var_list = []
        for var in eqnmap.getList(target, valid):
            if var not in declared:
                declare = declare_type
            else:
                declare = ""
            write = 1
            if var in target and var in all_lookup:
                lhs_text = lhs_format(var)
                rhs_text = rhs(var)
            elif var not in all_lookup:
                eqn = eqnmap[var]
                lhs_text = lhs(var)
                rhs_text = eqn.formatRHS(rhs)
                new_funcs = eqn.countFunctions()
                if new_funcs > 0:
                    functions += new_funcs
                    func_vars.add(eqn.lhs)
            else:
                write = 0
            if write:
                var_list.append(var)
                out(format
                    % {
                        "decl" : declare,
                        "lhs" : lhs_text,
                        "rhs" : rhs_text
                        })
                post = post_print(var)
                if post:
                    out(post)
            if var in lookup_dependencies:
                self.printLookupTableRow(out, var, rhs, use_device_getter)
        return (functions, func_vars, var_list)

    def license(self):
        return '''// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

'''

    def reference(self):
        with open(self.directory+'/'+self.model_name+'.model', 'r', encoding="utf-8") as file:
            lines = file.readlines()

        keywords=[
        "Authors",
        "Year",
        "Title",
        "Journal",
        "DOI",
        "Comment"
        ]

        reference=[]        
        for i,line in enumerate(lines):
            line = line.strip()
            if line == "\n" or not line or line[0] != "#":
                continue
            for keyword in keywords:
                m = re.match( r'#\s*'+keyword+r'\s*::=(.*)', line, re.IGNORECASE )
                if m:
                    reference.append(keyword + ": " + m.group(1).strip() )
            
        ref_vars = { 'reference' : '\n'.join('*  ' + x for x in reference) }

        ref_source = Template("""
/*
*
${reference}
*
*/
        """)

        ref = ref_source.safe_substitute(ref_vars)
        return ref

    ###################################################################333
    def printHeader(self, out_dir):
        out = Indenter(open(os.path.join(out_dir, self.model_name+".h"), "w"))
        out(self.license())
        out(self.reference())
        out('''//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __%(header)s_H__
#define __%(header)s_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(%(header)s_CPU_GENERATED)\
    || defined(%(header)s_MLIR_CPU_GENERATED)\
    || defined(%(header)s_MLIR_ROCM_GENERATED)\
    || defined(%(header)s_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define %(header)s_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define %(header)s_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define %(header)s_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define %(header)s_CPU_GENERATED
#endif

namespace limpet {

''' % self.value_hash)

        req_flags = [ var.info('external')+"_DATA_FLAG"
                      for var
                      in order(self.reqvars & self.public)
                      ]
        out("#define "+self.model_name+"_REQDAT "+"|".join(req_flags))

        mod_flags = [ var.info('external')+"_DATA_FLAG"
                      for var
                      in order(self.modvars & self.public)
                      ]
        out("#define "+self.model_name+"_MODDAT "+"|".join(mod_flags))

        out('''
struct %(name)s_Params {
''' % self.value_hash)
        out.inc()
        i = 0
        for var in order(self.root.getVars('param')):
            out("%s %s;" % (self.type(var), var.name))

            # Add to the dictionary
            i = add_to_dictionary_offset(self.params_dict, var.name, self.type(var), i)

        # Add a last key ('size') that store the size info of the struct
        add_size_offset(self.params_dict, i)

        flags = self.root.getVars('flag')
        if flags:
            out("char* flags;")
        out.dec()
        out('''
};
''' % self.value_hash)

        if flags:
            valid_flags = []
            for flag in order(flags):
                if not flag.info('flag'):
                    valid_flags.append(str(flag))
                    valid_flags.append('no_'+str(flag))
                else:
                    for enum in flag.info('flag'):
                        valid_flags.append(str(enum))
            out(('static const char* %(name)s_flags = "' % self.value_hash)
                +"|".join(valid_flags)
                +'";\n\n')

        out('''
struct %(name)s_state {
''' % self.value_hash)
        out.inc()
        statevars = self.root.getVars('nodal') - (self.root.getVars('external'))

        i = 0
        dl_i = 0
        # Data layout opt needs extra dictionary to calculate the size of the state variable
        # since state_dict contains offsets for accessing values in the optimized way
        data_layout_tmp_dict = dict()
        dl_sizeof_offset = 0
        for var in order(statevars):
            # When using DLO, we use an Array of Structs of Array (AoSoA)
            # So each state variable in the structure is replaced with an
            # array of <vector_size> state variables
            if enable_data_layout_opt:
                out("%s %s[%d];" % (self.type(var), var.name, num_elements))
            else:
                out("%s %s;" % (self.type(var), var.name))

            # Add to the dictionary
            if enable_mlir_codegen and enable_data_layout_opt:
                dl_i, dl_sizeof_offset = add_to_dictionary_offset_data_layout(self.dl_state_dict, var.name, self.type(var), dl_i,
                                                                        data_layout_tmp_dict, dl_sizeof_offset)
            i = add_to_dictionary_offset(self.state_dict, var.name, self.type(var), i)

        for var in order(self.private):
            if enable_data_layout_opt:
                out("{} __{}_local[{}];".format(self.type(var), var.name, num_elements))
            else:
                out("{} __{}_local;".format(self.type(var), var.name))

            # Add to the dictionary
            if enable_mlir_codegen and enable_data_layout_opt:
                dl_i, dl_sizeof_offset = add_to_dictionary_offset_data_layout(self.dl_state_dict, var.name, self.type(var), dl_i,
                                                                        data_layout_tmp_dict, dl_sizeof_offset)
            i = add_to_dictionary_offset(self.state_dict, var.name, self.type(var), i)

        # Add a last key ('size') that store the size info of the struct.
        # Use the value of 'i'
        if enable_mlir_codegen:
            if enable_data_layout_opt:
                # Again, we don't want to compute any padding when using DLO
                # as there shouldn't be any alignement problems
                self.dl_state_dict['size'] = ('f64', dl_sizeof_offset)
            add_size_offset(self.state_dict, i)

        if len(statevars)==0 and len(self.private) == 0:
            out("char dummy; //PGI doesn't allow a structure of 0 bytes.")
        out.dec()
        out('''
};''')

        cvode = self.root.getVars('cvode')
        rosenbrock = self.root.getVars('rosenbrock')
        good_vars = Set()
        nodal_vars = self.root.getVars('nodal')
        nodal_constants = self.root.getVars('nodal') - self.public - self.root.statevars
        nodal_influences = self.root.main.influences_of(nodal_constants)

        param_vars = self.root.getVars('param') | self.ext_regional
        param_constants = param_vars - nodal_vars
        param_init = param_vars - param_constants
        param_influences = self.root.main.influences_of(param_constants)

        time_vars = self.root.getVars('time') - Set([self.root.table['dt']])
        time_influences = self.root.main.influences_of(time_vars)
        dt = Set([self.root.table['dt']])
        dt_influences = self.root.main.influences_of(dt)

        constants = self.root.main.reachable_from(Set())
        just_dt = self.root.main.reachable_from(dt) - constants

        global_constants = constants - param_influences - time_influences - dt_influences - nodal_influences
        regional_constants = ((constants & param_influences) | just_dt ) - nodal_influences
        recomputed_regional_constants = regional_constants - param_constants

        rosenbrock_nodal_req = self.reqvars - rosenbrock

        cvode = self.root.getVars('cvode')
        cvode_nodal_req = self.reqvars - cvode

        def printRosenbrockDeclarations(generation_target = GenerationTarget.CPU_CODEGEN):
            rosenbrock_strings = {
                    'name': self.value_hash['name'],
                    'header': self.value_hash['header'] + generation_target.guard_suffix,
                    'regional_constants_struct_name': '%(name)s_Regional_Constants' % self.value_hash + generation_target.suffix,
                    'nodal_req_struct_name': '%(name)s_Nodal_Req' % self.value_hash + generation_target.suffix,
                    'private': '%(name)s_Private' % self.value_hash + generation_target.suffix,
                    'suffix': generation_target.suffix
                    }

# We're removing the header for now, everything can be generated without problem
# Only generate the structures for the CPU and MLIR_CPU targets as the GPU
# targets use the CPU structure
#            out('''#ifdef %(header)s''' % rosenbrock_strings)
            if not generation_target.isMlirGpu():
                out('''
    struct %(regional_constants_struct_name)s {
                    ''' % rosenbrock_strings)
                out.inc()
                for var in order(recomputed_regional_constants):
                    if generation_target == GenerationTarget.CPU_CODEGEN:
                        out("GlobalData_t %s;" % var.name)
                    elif generation_target.isMlirCodegen:
                        if generation_target.isMlirGpu():
                            out("GlobalData_t %s;" % (var.name))
                        else:
                            out("GlobalData_t %s[%s];" % (var.name, num_elements))
                # Empty struct has size 0 in C, size 1 in C++. Place a dummy to avoid this
                if not len(recomputed_regional_constants):
                    out("char dummy;")
                out.dec()
                out('''
    };

    struct %(nodal_req_struct_name)s {
    ''' % rosenbrock_strings)
                out.inc()
                for var in order(rosenbrock_nodal_req | cvode_nodal_req):
                    if generation_target == GenerationTarget.CPU_CODEGEN:
                        out("GlobalData_t %s;" % var.name)
                    elif generation_target.isMlirCodegen():
                        if generation_target.isMlirGpu():
                            out("GlobalData_t %s;" % (var.name))
                        else:
                            out("GlobalData_t %s[%s];" % (var.name, num_elements))
                out.dec()
                out('''
    };

    struct %(private)s {
      using nodal_req_type = %(nodal_req_struct_name)s;
      using regional_constants_type = %(regional_constants_struct_name)s;
      IonIfBase *IF;
      int   node_number;
      void* cvode_mem;
      void* sunctx_ptr;
      bool  trace_init;
      regional_constants_type rc;
      nodal_req_type nr;
    };
    ''' % rosenbrock_strings)
            out('''
//Printing out the Rosenbrock declarations
extern "C" {
''')
            if generation_target.isMlirGpu():
                out('''
__device__
''')
            out('''
void %(name)s_rosenbrock_f%(suffix)s(float*, float*, void*);
''' % rosenbrock_strings)
            if generation_target.isMlirGpu():
                out('''
__device__
''')
            out('''
void %(name)s_rosenbrock_jacobian%(suffix)s(float**, float*, void*, int);
''' % rosenbrock_strings)
            if generation_target.isMlirGpu():
                out('''
__device__
''')
            out('''
void rbStepX ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );
}
''')
#            out("#endif")
        if rosenbrock | cvode:
            printRosenbrockDeclarations()
            if self.enable_mlir_codegen:
                printRosenbrockDeclarations(GenerationTarget.MLIR_CPU_CODEGEN)
                if enable_gpu_codegen:
                    printRosenbrockDeclarations(GenerationTarget.MLIR_CUDA_CODEGEN)
                    printRosenbrockDeclarations(GenerationTarget.MLIR_ROCM_CODEGEN)
        out('''
class %(name)sIonType : public IonType {
public:
    using IonIfDerived = IonIf<%(name)sIonType>;
    using params_type = %(name)s_Params;
    using state_type = %(name)s_state;''' % self.value_hash)
        if self.root.getVars('rosenbrock') or self.root.getVars('cvode'):
            out('''
    using private_type = %(name)s_Private_cpu;
    #ifdef %(header)s_MLIR_CPU_GENERATED
    using private_type_vector = %(name)s_Private_mlir_cpu;
    #endif''' % self.value_hash)
        out('''
    %(name)sIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_%(name)s(int, int, IonIfBase&, GlobalData_t**);
#ifdef %(header)s_CPU_GENERATED
void compute_%(name)s_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef %(header)s_MLIR_CPU_GENERATED
void compute_%(name)s_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef %(header)s_MLIR_ROCM_GENERATED
void compute_%(name)s_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef %(header)s_MLIR_CUDA_GENERATED
void compute_%(name)s_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif
''' % self.value_hash)

        if self.enable_mlir_codegen and not self.root.getVars('cvode'):
            # Hold printing into the file until after all information from parameters of the MLIR function are known
            # Until then, keep the file, and text to be printed in variables.
            self.header_file = out
            self.header_file_final_string = '''
}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
'''
        else:
            out('''
}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
''')

    ###################################################################333

    def printSource(self, out_dir):
        out = Indenter(open(os.path.join(out_dir, self.model_name+".cc"), "w"))
        out(self.license())
        out(self.reference())
        out.indent_amount = 2

        # Define all the necessary variable sets here
        #
        # constants - variables that are constant no matter what.
        # param_vars - variables that are defined as parameters but are otherwise
        #    constant
        # param_nodal_vars - variables that are parameters and nodal vars.
        # regional_constants - variables that are constant for a region
        # recomputed_regional_constants - regional constants that need to be recomputed
        #     once every timestep for each region
        # nodal_constants - variables that are nodal yet constant
        if self.enable_mlir_codegen and enable_gpu_codegen:
            cuda_string = "#ifdef HAS_CUDA_MODEL\n#include <cuda_runtime.h>\n#endif\n"
            cuda_string += "#ifdef HAS_ROCM_MODEL\n#include <hip/hip_runtime.h>\n#endif"
        else:
            cuda_string = ""
        out('''
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "%(name)s.h"

#ifdef _OPENMP
#include <omp.h>
#endif
%(cuda_string)s

''' % {'name'  : self.model_name, 'cuda_string': cuda_string})
        if self.root.getVars('rosenbrock'):
            out('''
#include "Rosenbrock.h"
''' % self.value_hash)
        if self.root.getVars('cvode'):
            out('''
#ifdef USE_CVODE
#include <nvector/nvector_serial.h>
#include <cvode/cvode.h>
#include <cvode/cvode_diag.h>
#endif
''' % self.value_hash)
        out('''
namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

%(name)sIonType::%(name)sIonType(bool plugin) : IonType(std::move(std::string("%(name)s")), plugin) {}

size_t %(name)sIonType::params_size() const {
  return sizeof(struct %(name)s_Params);
}''' % self.value_hash)
# Define the dlo_vector_size function by returning the vector size if data
# layout optimization is enabled, or 1 otherwise
        out('''
size_t %(name)sIonType::dlo_vector_size() const {''' % self.value_hash)
        if enable_data_layout_opt:
            out(f'''
  return {num_elements};
}}''')
        else:
            out('''
  return 1;
}''')
        out('''
uint32_t %(name)sIonType::reqdat() const {
  return %(name)s_REQDAT;
}

uint32_t %(name)sIonType::moddat() const {
  return %(name)s_MODDAT;
}

void %(name)sIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target %(name)sIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef %(header)s_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(%(header)s_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(%(header)s_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(%(header)s_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef %(header)s_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef %(header)s_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef %(header)s_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef %(header)s_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void %(name)sIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef %(header)s_MLIR_CUDA_GENERATED
      compute_%(name)s_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(%(header)s_MLIR_ROCM_GENERATED)
      compute_%(name)s_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(%(header)s_MLIR_CPU_GENERATED)
      compute_%(name)s_mlir_cpu(start, end, imp, data);
#   elif defined(%(header)s_CPU_GENERATED)
      compute_%(name)s_cpu(start, end, imp, data);
#   else
#     error "Could not generate method %(name)sIonType::compute."
#   endif
      break;
#   ifdef %(header)s_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_%(name)s_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef %(header)s_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_%(name)s_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef %(header)s_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_%(name)s_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef %(header)s_CPU_GENERATED
    case Target::CPU:
      compute_%(name)s_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
''' % self.value_hash)

        init = self.root.init_system()

        good_vars = Set()
        nodal_vars = self.root.getVars('nodal')
        nodal_constants = self.root.getVars('nodal') - self.public - self.root.statevars
        nodal_influences = self.root.main.influences_of(nodal_constants)

        param_vars = self.root.getVars('param') | self.ext_regional
        param_constants = param_vars - nodal_vars
        param_init = param_vars - param_constants
        param_influences = self.root.main.influences_of(param_constants)

        time_vars = self.root.getVars('time') - Set([self.root.table['dt']])
        time_influences = self.root.main.influences_of(time_vars)
        dt = Set([self.root.table['dt']])
        dt_influences = self.root.main.influences_of(dt)

        constants = self.root.main.reachable_from(Set())
        just_dt = self.root.main.reachable_from(dt) - constants

        global_constants = constants - param_influences - time_influences - dt_influences - nodal_influences
        regional_constants = ((constants & param_influences) | just_dt ) - nodal_influences
        recomputed_regional_constants = regional_constants - param_constants

        self.printEquations(out=out,
                            eqnmap=self.root.main,
                            valid=good_vars,
                            target=global_constants,
                            format="#define %(lhs)s (GlobalData_t)(%(rhs)s)"
                            )
        good_vars |= global_constants

        out("\n\n")

        out('''
void %(name)sIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  %(name)s_Params *p = imp.params();

  // Compute the regional constants
  {
'''
% self.value_hash)
        if self.root.getVars('cvode') :
            out('''
#ifndef USE_CVODE
        //log_msg(NULL,5,0,"%(name)s needs CVODE. Recompilation needed.");
        exit(1);
#endif
'''
%self.value_hash)
        out.inc()
        out.inc()

        def basic(var):
            if var.is_a('param'):
                return "p->%s" % var.name
            elif var.is_a('external') and var.is_a('regional'):
                return "region->%s" % var.info('external')
            else:
                return var.name

        def post_flag_behavior(var):
            ret = []
            if var.is_a('flag'):
                ret.append('if (0) ;')
                if not var.info('flag'):
                    ret.append('else if (flag_set(p->flags, "%s")) %s = 1;' % (var, basic(var)))
                    ret.append('else if (flag_set(p->flags, "no_%s")) %s = 0;' % (var, basic(var)))
                else:
                    for enum in var.info('flag'):
                        ret.append('else if (flag_set(p->flags, "%s")) %s = %s;' % (enum, basic(var), basic(enum)))
            return "\n".join(ret)

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars,
                            target=param_constants - (self.ext_regional - self.root.assigned),
                            declared=param_constants,
                            lhs_format=basic,
                            rhs_format=basic,
                            post_print=post_flag_behavior,
                            )
        good_vars = good_vars | param_constants

        out.dec()
        out("}")
        out("// Compute the regional initialization")
        out("{")
        out.inc()

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars,
                            target=param_init,
                            declared=param_init,
                            lhs_format=basic,
                            rhs_format=basic,
                            )

        out.dec()
        out("}")
        #if self.root.getVars('trace'):
        #    out("IF->type->trace = trace_%(name)s;" % self.value_hash);
        out.dec()
        out('''
}


// Define the parameters for the lookup tables
enum Tables {
''' % self.value_hash)
        out.inc()
        for lookup in self.root.lookup:
            out("%s_TAB," % lookup.name)

            # Add a new entry to the list of tables to look for in MLIR codegen
            self.tables.append(lookup.name)
        out.dec()
        out('''
  N_TABS
};

// Define the indices into the lookup tables.
''' % self.value_hash)

        def printRegionalConstants(
            self=self,
            out=out,
            eqnmap=self.root.main,
            target = recomputed_regional_constants,
            valid = global_constants | param_constants | dt,
            format = basic,
            declare_type = "GlobalData_t "
            ):
            out("// Define the constants that depend on the parameters.")
            self.printEquations(out=out,
                                eqnmap=eqnmap,
                                valid=valid,
                                target=target,
                                lhs_format=format,
                                rhs_format=format,
                                declare_type=declare_type,
                                )

        good_vars |= Set([self.root.table['dt']]) | recomputed_regional_constants
        update_inputs = self.root.statevars | self.reqvars | nodal_constants

        all_targets = Set()
        external_update_targets = self.root.getVars('store') | (self.modvars - self.root.getVars('state'))
        all_targets |= external_update_targets

        rush_larsen_targets = Set()
        for var in self.root.getVars('rush_larsen'):
            rush_larsen_targets |= Set(var.info('rush_larsen'))
        all_targets |= rush_larsen_targets

        sundnes_half_targets = Set()
        for var in self.root.getVars('sundnes'):
            sundnes_half_targets.add(var.info('sundnes').half)
            all_targets.add(var.info('sundnes').full)
        all_targets |= sundnes_half_targets

        rosenbrock = self.root.getVars('rosenbrock')
        rosenbrock_diff_targets = Set()
        rosenbrock_partial_targets = Set()
        for var in rosenbrock:
            rosenbrock_diff_targets.add(var.info('implicit_method').diff)
            rosenbrock_partial_targets |= var.info('implicit_method').partial
        all_targets |= rosenbrock_diff_targets | rosenbrock_partial_targets
        rosenbrock_nodal_req = self.reqvars - rosenbrock

        # Generate the rosenbrock enum
        out('''
    enum Rosenbrock {
    ''' % self.value_hash)
        out.inc()
        for var in order(rosenbrock):
            out("ROSEN_%s," % var)
        out.dec()
        out('''
      N_ROSEN
    };''')

        cvode = self.root.getVars('cvode')
        cvode_targets = Set([var.info('diff') for var in self.root.getVars('cvode')])
        all_targets |= cvode_targets
        cvode_nodal_req = self.reqvars - cvode

        fe_targets = Set([var.info('diff') for var in self.root.getVars('fe')])
        all_targets |= fe_targets

        rk2_targets = Set([var.info('diff') for var in self.root.getVars('rk2')])
        all_targets |= rk2_targets

        rk4_targets = Set([var.info('diff') for var in self.root.getVars('rk4')])
        all_targets |= rk4_targets

        markov_be_targets = Set()
        markov_be_solve = Set()
        for var in self.root.getVars('markov_be'):
            markov_be_targets.add(var.info('implicit_method').diff)
            markov_be_targets |= var.info('implicit_method').partial
            markov_be_solve.add(var.info('markov_be'))
        all_targets |= markov_be_targets | markov_be_solve

        for lookup in self.root.lookup:
            lookup_var = Set([self.root.table[lookup.name]])
            lookup_canidates = self.root.main.influences_of(lookup_var, update_inputs)
            lookup_canidates &= self.root.main.reachable_from(good_vars | lookup_var, update_inputs)
            lookup.set = self.root.main.extract_external(lookup_canidates, all_targets)
            for var in lookup.set:
                var.make_a('lookup', lookup)

        for lookup in self.root.lookup:

            # Construct a new enumerator dictionary
            lookup_dict = dict()
            out("enum %s_TableIndex {" % lookup.name)
            out.inc()
            i = 0
            for var in order(lookup.set):
                out("%s_idx," % var.name)

                # Add a new entry to enum_dict assuming its size as 'double' (GlobalData_t)
                if self.enable_mlir_codegen and enable_data_layout_opt:
                    i = add_to_dictionary_offset(lookup_dict, var.name, "GlobalData_t", i)
                else:
                    i = add_to_dictionary_offset(lookup_dict, var.name, "GlobalData_t", i)

            # Add a last key ('size') that store the size info of the struct
            add_size_offset(lookup_dict, i)

            self.enumerators_dict[lookup.name] = lookup_dict
            out("NROWS_%s" % lookup.name)
            out.dec()
            out("};")
            out("\n")

        out('''

void %(name)sIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * %(time_factor)s;
  cell_geom* region = &imp.cgeom();
  %(name)s_Params *p = imp.params();

  imp.tables().resize(N_TABS);

''' % self.value_hash)
        out.inc()
        printRegionalConstants(declare_type="double ")


        for lookup in self.root.lookup:
            if not lookup.set:
                continue
            lookup_hash = { 'name' : lookup.name,
                            'min' : lookup.lb,
                            'max' : lookup.ub,
                            'step' : lookup.step,
                            'model' : self.model_name,
                            }
            out("""
// Create the %(name)s lookup table
LUT* %(name)s_tab = &imp.tables()[%(name)s_TAB];
LUT_alloc(%(name)s_tab, NROWS_%(name)s, %(min)s, %(max)s, %(step)s, "%(model)s %(name)s", imp.get_target());
for (int __i=%(name)s_tab->mn_ind; __i<=%(name)s_tab->mx_ind; __i++) {
  double %(name)s = %(name)s_tab->res*__i;
  LUT_data_t* %(name)s_row = %(name)s_tab->tab[__i];
"""
                % lookup_hash)
            out.inc()

            def construct_lookup_format(var, base, f=basic):
                if var == base:
                    return var.name
                else:
                    return lookup_format(var, f)

            lookup_var = self.root.table[lookup.name]
            lookup_var_set = Set([lookup_var])
            self.printEquations(out=out,
                                eqnmap=self.root.main,
                                target=lookup.set,
                                valid=good_vars | lookup_var_set,
                                declared=lookup.set,
                                lhs_format=lambda v: construct_lookup_format(v, lookup_var),
                                rhs_format=lambda v: construct_lookup_format(v, lookup_var),
                                declare_type="double "
                                )
            out.close()
            out('check_LUT(%(name)s_tab);\n\n' % lookup_hash)
            if self.enable_mlir_codegen and enable_gpu_codegen:
                out('#ifdef HAS_CUDA_MODEL')
                out('cudaMemPrefetchAsync(%(name)s_tab, sizeof(LUT_data_t)*(%(max)s-(%(min)s)/%(step)s), 0);' % lookup_hash);
                out('#elif defined HAS_ROCM_MODEL')
                out('hipMemPrefetchAsync(%(name)s_tab, sizeof(LUT_data_t)*(%(max)s-(%(min)s)/%(step)s), 0);' % lookup_hash);
                out('#endif')

        out.dec()
        out('''
}
''' % self.value_hash)
        if cvode:
            out('''
enum Cvode_Vars {
''' % self.value_hash)
            out.inc()
            for var in order(cvode):
                out("CVODE_%s," % var)
            out.dec()
            out('''
  N_CVODE
};

#ifdef USE_CVODE
#if SUNDIALS_VERSION_MAJOR >= 7
int %(name)s_internal_rhs_function(sunrealtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data);
#else
int %(name)s_internal_rhs_function(realtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data);
#endif
#endif

#ifndef LIMPET_CVODE_RTOL
#define LIMPET_CVODE_RTOL 1e-5
#endif

//disable absolute tolerances for all gates.
#ifndef LIMPET_CVODE_ATOL
#define LIMPET_CVODE_ATOL 1e-16
#endif

''' % self.value_hash)
        out('''


void %(name)sIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * %(time_factor)s;
  cell_geom *region = &imp.cgeom();
  %(name)s_Params *p = imp.params();

  %(name)s_state *sv_base = (%(name)s_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
''' % self.value_hash)
        out.inc()
        printRegionalConstants(declare_type="double ")

        self.printExternalDef(out)

        out.dec()

        if enable_data_layout_opt:
            step = num_elements
        else:
            step = 1
        out('''
  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=%(step)s ){
    %(name)s_state *sv = sv_base+__i / %(step)s;''' % { "name": self.value_hash["name"], "step": step})
        if enable_data_layout_opt:
            out('''
    for (int inner_id = 0; inner_id < %s && (inner_id + __i) < imp.get_num_node(); ++inner_id) {

    // Initialize nodal variables that have been declared with param''' % num_elements)
        good_vars |= Set([self.root.table['t']])

        out.inc()
        out.inc()

        def complete_format(var, dlo_index="inner_id", dlo=enable_data_layout_opt, f=basic):
            if (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external') and dlo:
                return "sv->%s[%s]" % (var.name, dlo_index)
            elif (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external'):
                return "sv->%s" % var.name
            else:
                return f(var)

        self.printGetExternal(out, enable_data_layout_opt, "inner_id")
        self.changeExternalUnits(1, out, complete_format)

        for var in order(param_init):
            out("%s = %s;" % (complete_format(var, "inner_id"), basic(var)))

        out("// Initialize the rest of the nodal variables")
        init_ex_mod = self.root.getVars('init') & (self.ext_nodal)
        init_ex_req = self.reqvars - init_ex_mod

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars | init_ex_req | param_vars,
                            target=((self.root.getVars('nodal')-init_ex_req) | init_ex_mod) - param_vars,
                            declared = self.root.getVars('nodal') | self.root.getVars('external'),
                            lhs_format = complete_format,
                            rhs_format = complete_format,
                            declare_type = "double ",
                            )

        self.changeExternalUnits(-1, out, complete_format)
        self.printSetExternal(out, enable_data_layout_opt, "inner_id")

        out.dec()
        out.dec()

        out("  }")
        if enable_data_layout_opt:
            out("}")
        if self.enable_mlir_codegen and enable_gpu_codegen:
            out("  #ifdef HAS_CUDA_MODEL")
            out("  cudaMemPrefetchAsync(sv_base, sizeof(%(name)s_state)*(imp.sv_tab().size()), 0);" % self.value_hash)
            out("  #elif defined HAS_ROCM_MODEL")
            out("  hipMemPrefetchAsync(sv_base, sizeof(%(name)s_state)*(imp.sv_tab().size()), 0);" % self.value_hash)
            out("  #endif")

#        if self.enable_mlir_codegen and enable_data_layout_opt:
#            t2 = self.root.getVars('nodal') - (self.root.getVars('external'))
#            u2 = self.private
#            t1 = len(t2) + len(u2)
#
#            out('''  #if defined(MLIR_DATA_LAYOUT_OPT) && (defined %(header)s%(suffix_cpu)s || defined %(header)s%(suffix_gpu)s)\n  %(name)s_state *sv = sv_base;'''
#                % {'name': self.value_hash['name'], 'header': self.value_hash['header'], 'suffix_cpu': GenerationTarget.MLIR_CPU_CODEGEN.guard_suffix, 'suffix_gpu': GenerationTarget.MLIR_GPU_CODEGEN.guard_suffix})
#            statevars = self.root.getVars('nodal') - (self.root.getVars('external'))
#            out("  int state_ele = %s;" % t1)
#            out("  int arr[state_ele];")
#
#            t3 = 0
#            for var in order(statevars):
#                out("  arr[%s]=sizeof(sv->%s);" % (t3, var.name))
#                t3 = t3 + 1
#
#            for var in order(self.private):
#                out("  arr[%s]=sizeof(sv->__%s_local);" % (t3, var.name))
#                t3 = t3 + 1
#
#            out('''  %(name)s_state my_state;'''% self.value_hash)
#
#            out ('''
#  for(int __i=0; __i< (imp.sv_tab().size() >> %(log2_num_elements)s) << %(log2_num_elements)s ; __i = __i + %(num_elements)s){
#    %(name)s_state *sv = sv_base+__i;
#    char *mlir_sv = (char*)sv;
#    char *mlir_my = (char*)&my_state;
#
#    int temp = 0;
#    for (int m = 0; m < state_ele; m++) {
#      if (arr[m] == 8) {
#        if (temp%%8 != 0)
#          temp = temp + 4;
#        *(double*)(mlir_my + temp) = *(double*)(mlir_sv + temp);
#      } else
#        if (arr[m] == 4)
#          *(float*)(mlir_my + temp) = *(float*)(mlir_sv + temp);
#        temp = temp + arr[m];
#      }
#      temp = 0;
#      int temp1 = 0;
#      for (int m = 0; m < state_ele; m++) {
#        if (arr[m] == 8) {
#          if ( temp1%%8 != 0)
#            temp1 = temp1 + 4;
#            for (int __z=0; __z<%(num_elements)s; __z++)
#              *(double*)(mlir_sv + (%(num_elements)s*temp) + (__z * 8)) = *(double*)(mlir_my + temp1);
#        } else if (arr[m] == 4) {
#          for (int __z=0; __z<%(num_elements)s; __z++)
#            *(float*)(mlir_sv + (%(num_elements)s*temp) + (__z * 4)) = *(float*)(mlir_my + temp1);
#        }
#        temp = temp + arr[m];
#        temp1 = temp1 + arr[m];
#      }
#  }
#  #endif'''% {'name' : self.model_name,
#        "num_elements": num_elements, "log2_num_elements": int(math.log2(num_elements)), })

        if cvode:
            out('''
  //setup CVODE
  {
#ifdef USE_CVODE
#if SUNDIALS_VERSION_MAJOR >= 6
    SUNContext sunctx;
    SUNContext_Create(SUN_COMM_NULL, &sunctx);
    void* cvode_mem = CVodeCreate(CV_BDF, sunctx);
#elif SUNDIALS_VERSION_MAJOR >= 4
    void* cvode_mem = CVodeCreate(CV_BDF);
#else
    void* cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
#endif
    assert(cvode_mem != NULL);
#if SUNDIALS_VERSION_MAJOR >= 6
    N_Vector cvode_y = N_VNew_Serial(N_CVODE, sunctx);
#else
    N_Vector cvode_y = N_VNew_Serial(N_CVODE);
#endif
    int flag = CVodeInit(cvode_mem, %(name)s_internal_rhs_function, 0, cvode_y);
    assert(flag == CV_SUCCESS);
    N_VDestroy(cvode_y);

    flag = CVodeSStolerances(cvode_mem, LIMPET_CVODE_RTOL, LIMPET_CVODE_ATOL);
    assert(flag == CV_SUCCESS);
    flag = CVodeSetMaxStep(cvode_mem, dt);
    assert(flag == CV_SUCCESS);
    %(name)s_Private_cpu* userdata = (%(name)s_Private_cpu*)(imp.ion_private().data());
    userdata->cvode_mem = cvode_mem;
#if SUNDIALS_VERSION_MAJOR >= 6
    userdata->sunctx_ptr = malloc(sizeof(SUNContext));
    memcpy(userdata->sunctx_ptr, &sunctx, sizeof(SUNContext));
#endif
    flag = CVodeSetUserData(cvode_mem, userdata);
    assert(flag == CV_SUCCESS);
    flag = CVDiag(cvode_mem);
    assert(flag == CV_SUCCESS);
#endif
  }
''' % self.value_hash)
        out('''
}''')

        def dont_set_modvars(var, f=complete_format, modvars=self.modvars):
            if (var in modvars
                and not var.is_a('state')):
                return basic(var)
            else:
                return f(var)


        all_lookup = Set()
        func_vars = Set()
        class lhs_wrapper:
            def __init__(self, f):
                self.f = f;
            def __call__(self, var):
                if var.is_a('state'):
                    return var.name + "_new"
                else:
                    return self.f(var)
        lhs_complete = lhs_wrapper(complete_format)
        lhs_dont = lhs_wrapper(dont_set_modvars)
        functions = 0
        specified_lookup_variables = Set([self.root.table[lookup.name] for lookup in self.root.lookup])

        def printComputeFunction(self=self, out=out, generation_target=GenerationTarget.CPU_CODEGEN):
            nonlocal all_lookup
            nonlocal func_vars
            nonlocal lhs_complete
            nonlocal lhs_dont
            nonlocal functions
            nonlocal specified_lookup_variables
            function_name = 'compute_' + self.value_hash['name']
            if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                private_name = self.value_hash['private_vector']
                private_getter = "ion_private_vector"
            else:
                private_name = self.value_hash['private']
                private_getter = "ion_private"
            function_guard = self.value_hash['header'] + generation_target.guard_suffix
            function_name += generation_target.suffix

            out('''
/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef %s
extern "C" {
''' % function_guard)
            out('''void %s(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )''' % function_name)
            out('''{
  %(name)sIonType::IonIfDerived& imp = static_cast<%(name)sIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*%(time_factor)s;
  cell_geom *region = &imp.cgeom();
  %(name)s_Params *p  = imp.params();
  %(name)s_state *sv_base = (%(name)s_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  %(name)sIonType::IonIfDerived* IF = &imp;

''' % self.value_hash)

            # Track all LUTs created in regular codegen. They will be later used in the MLIR Codegen
            self.lut_list = []

            out.inc()
            printRegionalConstants()
            if rosenbrock | cvode:
                out('''
//Initializing the userdata structures.
%(private)s* ion_private = imp.%(private_getter)s().data();
int nthread = 1;''' % {'private': private_name, 'private_getter': private_getter})
                if generation_target.isMlirCodegen():
                    if generation_target.isMlirGpu():
                        out('''nthread = end;''')
                    else:
                        out('''  #if defined _OPENMP
  nthread = omp_get_max_threads();
  #endif''')
                else:
                    out('''#ifdef _OPENMP
nthread = omp_get_max_threads();
#endif''')
                    out.dec()
                    out.inc()
                out('''for( int j=0; j<nthread; j++ ) {
''')
                out.inc()
                if generation_target.isMlirCodegen():
                    if not generation_target.isMlirGpu():
                        out("for (unsigned i = 0; i < %s; ++i) {" % num_elements)
                        out.inc()
                        for var in order(recomputed_regional_constants):
                            out("ion_private[j].rc.%s[i] = %s;" % (var, complete_format(var)))
                        out("}")
                        out.dec()
                    else:
                        for var in order(recomputed_regional_constants):
                            out("ion_private[j].rc.%s = %s;" % (var, complete_format(var)))
                else:
                    for var in order(recomputed_regional_constants):
                        out("ion_private[j].rc.%s = %s;" % (var, complete_format(var)))
                out.dec()
                out("}")
            if cvode:
                out('''
#ifdef USE_CVODE
#if SUNDIALS_VERSION_MAJOR >= 7
sunrealtype CVODE_array[N_CVODE];
N_Vector CVODE = N_VMake_Serial(N_CVODE, CVODE_array, *((SUNContext*)((%(private)s*)(imp.%(private_getter)s().data()))->sunctx_ptr));
#else
realtype CVODE_array[N_CVODE];
N_Vector CVODE = N_VMake_Serial(N_CVODE, CVODE_array);
#endif
#endif
void* cvode_mem = ((%(private)s*)(imp.%(private_getter)s().data()))->cvode_mem;
                    ''' % {'private': private_name, 'private_getter': private_getter})

            self.printExternalDef(out, True)

            out.dec()
            if enable_data_layout_opt:
                out('''
      int inner_offset = start %% %s;''' % num_elements)
            if generation_target.isMlirCodegen():
                if not generation_target.isMlirGpu():
                    if enable_data_layout_opt:
                        step = num_elements
                    else:
                        step = 1
                    # Loop through "remaining cells" (the (end % 8) last cells)
                    out('''      #pragma omp parallel for schedule(static)
      for (int __i = ((end >> %(num_elements)s) << %(num_elements)s); __i<end; __i+=%(step)s) {'''
                        % {'name'  : self.model_name, "num_elements": int(math.log2(num_elements)), "step": step})
                    out('''
          %(name)s_state *sv = sv_base+__i / %(step)s;
''' % {'name'  : self.model_name,
       "step": step})
            else:
                if enable_data_layout_opt:
                    step = num_elements
                else:
                    step = 1
                # Loop through cells by step. Step is num_elements when DLO
                # is enabled, otherwise it's just 1
                out('''
#pragma omp parallel for schedule(static)
  for (int __i=(start / %(step)s) * %(step)s; __i<end; __i+=%(step)s) {
    %(name)s_state *sv = sv_base+__i / %(step)s;
                    ''' % { "name": self.value_hash["name"], "step": step})
            if not generation_target.isMlirGpu() and enable_data_layout_opt:
                # Loop through the inner arrays of each structure.
                # We need to check the termination as to not compute
                # unecessary cells and access invalid external variable 
                # elements
                out('''
        for (int inner_id = inner_offset; inner_id < %s && (inner_id + __i) < end; ++inner_id) {
''' % num_elements)
            # We don't want to generate any of the following code for the MLIR
            # GPU compute function but still need this python code to execute
            # because it sets some needed variables for later.
            # We switch the indenter for this section in this case and replace
            # it back later.
            if generation_target.isMlirGpu():
                old_out = out
                out = Indenter(open(os.devnull, "w"))
            out.inc()
            out.inc()

            all_lookup = Set()
            for lookup in self.root.lookup:
                all_lookup |= lookup.set




            functions = 0
            func_vars = Set()
            already_computed = Set()

            self.printGetExternal(out, enable_data_layout_opt, "inner_id")
            self.changeExternalUnits(1, out, complete_format)

            out("//Compute lookup tables for things that have already been defined.")
            specified_lookup_variables = Set([self.root.table[lookup.name] for lookup in self.root.lookup])
            for var in order(specified_lookup_variables & update_inputs):
                self.printLookupTableRow(out, var, complete_format)
            out("\n\n")

            out("//Compute storevars and external modvars")
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs,
                                      target=external_update_targets,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars-self.root.statevars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)



            ####################################################################
            out("\n\n")
            out("//Complete Forward Euler Update")
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=fe_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)
            for var in order(self.root.getVars('fe')):
                diff = var.info('diff')
                out("GlobalData_t %(lhs)s = %(rhs)s+%(diff)s*dt;" % {
                        'lhs' : lhs_complete(var),
                        'rhs' : complete_format(var),
                        'diff' : complete_format(diff)})

            ########################################################################
            out("\n\n")
            out("//Complete Rush Larsen Update")
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=rush_larsen_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)
            for var in order(self.root.getVars('rush_larsen')):
                (a, b) = var.info('rush_larsen')
                out("GlobalData_t %(lhs)s = %(a)s+%(b)s*%(rhs)s;" % {
                        'a': complete_format(a),
                        'b': complete_format(b),
                        'lhs': lhs_complete(var),
                        'rhs': complete_format(var)})

            ########################################################################
            out("\n\n")
            out("//Complete RK2 Update")

            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=rk2_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)

            class intermediate_wrapper:
                def __init__(self, f, vars_to_wrap):
                    self.f = f
                    self.vars_to_wrap = vars_to_wrap
                def __call__(self, var):
                    if var in self.vars_to_wrap:
                        return "sv_intermediate_%s" % var.name
                    else:
                        return self.f(var)

            if self.root.getVars('rk2'):
                for var in order(self.root.getVars('rk2')):
                    out("GlobalData_t %s;" % lhs_dont(var))

                rk2_dont = intermediate_wrapper(dont_set_modvars, self.root.getVars('rk2'))

                out("{"); out.inc()
                out("GlobalData_t t = t + dt/2;")
                for var in order(self.root.getVars('rk2')):
                    out("GlobalData_t %s = %s+dt/2*%s;" % (
                            rk2_dont(var),
                            complete_format(var),
                            complete_format(var.info('diff'))))
                for var in order(specified_lookup_variables & self.root.getVars('rk2')):
                    self.printLookupTableRow(out, var, rk2_dont)
                useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk2'))
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | useable_from_previous,
                                          target=rk2_targets-good_vars,
                                          lhs_format = lhs_wrapper(rk2_dont),
                                          rhs_format = rk2_dont,
                                          lookup=all_lookup
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for var in order(self.root.getVars('rk2')):
                    out("%s = %s+dt*%s;" % (
                            lhs_dont(var), dont_set_modvars(var), dont_set_modvars(var.info('diff'))))
                out.dec(); out('}')

            ########################################################################
            out("\n\n")
            out("//Complete RK4 Update")
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=rk4_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)

            if self.root.getVars('rk4'):
                for var in order(self.root.getVars('rk4')):
                    out("GlobalData_t %s;" % lhs_dont(var))
                    out("GlobalData_t rk4_k4_%s;" % var)
                    out("GlobalData_t rk4_k3_%s;" % var)
                    out("GlobalData_t rk4_k2_%s;" % var)
                    out("GlobalData_t rk4_k1_%s = %s*dt;" % (var, var.info('diff')))

                rk4_dont = intermediate_wrapper(dont_set_modvars, self.root.getVars('rk4'))

                out("{"); out.inc()
                out("GlobalData_t t = t + dt/2;")
                for var in order(self.root.getVars('rk4')):
                    out("GlobalData_t %s = %s+rk4_k1_%s/2;" % (
                            rk4_dont(var),
                            complete_format(var),
                            var))
                for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                    self.printLookupTableRow(out, var, rk4_dont)
                useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | useable_from_previous,
                                          target=rk4_targets-good_vars,
                                          lhs_format = lhs_wrapper(rk4_dont),
                                          rhs_format = rk4_dont,
                                          lookup=all_lookup
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for var in order(self.root.getVars('rk4')):
                    out("rk4_k2_%s = dt*%s;" % (
                            var, dont_set_modvars(var.info('diff'))))
                out.dec(); out('}')
                out("{"); out.inc()
                out("GlobalData_t t = t + dt/2;")
                for var in order(self.root.getVars('rk4')):
                    out("GlobalData_t %s = %s+rk4_k2_%s/2;" % (
                            rk4_dont(var),
                            complete_format(var),
                            var))
                for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                    self.printLookupTableRow(out, var, rk4_dont)
                useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | useable_from_previous,
                                          target=rk4_targets-good_vars,
                                          lhs_format = lhs_wrapper(rk4_dont),
                                          rhs_format = rk4_dont,
                                          lookup=all_lookup
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for var in order(self.root.getVars('rk4')):
                    out("rk4_k3_%s = dt*%s;" % (
                            var, dont_set_modvars(var.info('diff'))))
                out.dec(); out('}')
                out("{"); out.inc()
                out("GlobalData_t t = t + dt;")
                for var in order(self.root.getVars('rk4')):
                    out("GlobalData_t %s = %s+rk4_k3_%s;" % (
                            rk4_dont(var),
                            complete_format(var),
                            var))
                for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                    self.printLookupTableRow(out, var, rk4_dont)
                useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | useable_from_previous,
                                          target=rk4_targets-good_vars,
                                          lhs_format = lhs_wrapper(rk4_dont),
                                          rhs_format = rk4_dont,
                                          lookup=all_lookup
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for var in order(self.root.getVars('rk4')):
                    out("rk4_k4_%s = dt*%s;" % (
                            var, dont_set_modvars(var.info('diff'))))
                out.dec(); out('}')

                for var in order(self.root.getVars('rk4')):
                    out("%s = %s+(rk4_k1_%s + 2*rk4_k2_%s + 2*rk4_k3_%s + rk4_k4_%s)/6;" % (
                            lhs_dont(var), dont_set_modvars(var), var,var,var,var))


            ###########################################################
            out("\n\n")
            out("//Complete Sundnes Update")
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=sundnes_half_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)

            sundnes = self.root.getVars('sundnes')
            for var in order(sundnes):
                out("GlobalData_t %s;" % lhs_dont(var))
            sundnes_dont = intermediate_wrapper(dont_set_modvars, sundnes)

            for var in order(sundnes):
                full = var.info('sundnes').full
                out("{"); out.inc()
                for other_var in order(sundnes):
                    if var != other_var:
                        half = other_var.info('sundnes').half
                        out("GlobalData_t %s = %s;" % (sundnes_dont(other_var), dont_set_modvars(half)))
                    else:
                        out("GlobalData_t %s = %s;" % (sundnes_dont(var), dont_set_modvars(var)))
                for other_var in specified_lookup_variables & sundnes:
                    self.printLookupTableRow(out, other_var, sundnes_dont)
                useable_from_previous = already_computed - self.root.main.influences_of(sundnes)
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | useable_from_previous,
                                          target = Set([full])-good_vars,
                                          lhs_format = lhs_wrapper(sundnes_dont),
                                          rhs_format = sundnes_dont,
                                          lookup=all_lookup
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                out("%s = %s;" % (lhs_dont(var), sundnes_dont(full)))
                out.dec(); out("}")

            ###########################################################
            out("\n\n")
            out("//Complete Markov Backward Euler method")
            if self.root.getVars('markov_be'):
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs | already_computed,
                                          target=markov_be_targets-good_vars,
                                          lhs_format = lhs_dont,
                                          rhs_format = dont_set_modvars,
                                          lookup=all_lookup,
                                          declared=self.modvars
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                already_computed |= Set(this_var_list)

                for var in order(self.root.getVars('markov_be')):
                    out("GlobalData_t %s = %s;" % (lhs_dont(var), dont_set_modvars(var)))

                def solve_format(var, f=lhs_dont):
                    if var.is_a('markov_be'):
                        return f(var.info('markov_be'))
                    else:
                        return f(var)

                out('''
int __count=0;
GlobalData_t __error=1;
while (__error > 1e-100 && __count < 50) {
'''); out.inc()
                for var in order(self.root.getVars('markov_be')):
                    out("GlobalData_t %s = %s;" % (solve_format(var), lhs_dont(var)))

                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=markov_be_solve-good_vars,
                                      lhs_format = solve_format,
                                      rhs_format = solve_format,
                                      lookup=all_lookup,
                                      declared=markov_be_solve
                                      )
                functions += this_func_count; func_vars |= this_func_vars
                already_computed |= Set(this_var_list)
                out("__error=0;")
                for var in order(self.root.getVars('markov_be')):
                    out("__error += fabs(%s-%s);" % (solve_format(var),lhs_dont(var)))
                    out("%s = %s;" % (lhs_dont(var),solve_format(var)))
                out("__count++;")
                out.dec(); out("}")
                for var in order(self.root.getVars('markov_be')):
                    out("%s = %s+dt*%s;" % (lhs_dont(var), dont_set_modvars(var), lhs_dont(var)))
            ###########################################################
            out("\n\n")
            out("//Complete Rosenbrock Update")
            if rosenbrock:
                out('''
int thread=0;
#ifdef _OPENMP
thread = omp_get_thread_num();
#endif
float Rosenbrock_X[N_ROSEN];
''' % self.value_hash)
                offset = 0
                for var in order(rosenbrock):
                    out("Rosenbrock_X[ROSEN_%s] = %s;" % (var, complete_format(var)))
                    if generation_target.isMlirCodegen():
                        offset = add_to_dictionary_offset(self.rosenbrock_dict, var.name, "Gatetype", offset)
                for var in order(rosenbrock_nodal_req):
                    if not generation_target.isMlirCodegen():
                        out("ion_private[thread].nr.%s = %s;" % (var, complete_format(var)))
                    else:
                        out("ion_private[thread].nr.%s[0] = %s;" % (var, complete_format(var)))
                if enable_data_layout_opt:
                    inner_id = "inner_id"
                else:
                    inner_id = "0"
                out('''ion_private[thread].node_number = __i + %(inner_id)s;

rbStepX(Rosenbrock_X, %(name)s_rosenbrock_f%(suffix)s, %(name)s_rosenbrock_jacobian%(suffix)s, (void*)(ion_private+thread), dt, N_ROSEN);
                    ''' % {"name": self.value_hash['name'], "suffix": generation_target.suffix, "inner_id": inner_id})
                for var in order(rosenbrock):
                    out("GlobalData_t %s = Rosenbrock_X[ROSEN_%s];" % (lhs_dont(var), var))

            ###########################################################
            out("\n\n")
            out("//Complete Cvode Update")
            if cvode:
                def cvode_format(var,
                                 f=dont_set_modvars,
                                 nr=cvode_nodal_req,
                                 rc=recomputed_regional_constants,
                                 cvode=cvode):
                    if var in nr:
                        return "nr->"+var.name
                    elif var in rc:
                        return "rc->"+var.name
                    elif var in cvode:
                        return "NV_Ith_S(CVODE,CVODE_%s)" % var
                    else:
                        return f(var)
                out('#ifdef USE_CVODE')
                for var in order(cvode):
                    out("%s = %s;" % (cvode_format(var), dont_set_modvars(var)))
                for var in order(cvode_nodal_req):
                    out("ion_private->nr.%s = %s;" % (var, dont_set_modvars(var)))
                if enable_data_layout_opt:
                    inner_id = "inner_id"
                else:
                    inner_id = "0"
                out('''

//Do the solve
int CVODE_flag;
CVODE_flag = CVodeReInit(cvode_mem, t, CVODE);
assert(CVODE_flag == CV_SUCCESS);
CVODE_flag = CVodeSetInitStep(cvode_mem, dt);
assert(CVODE_flag == CV_SUCCESS);
#if SUNDIALS_VERSION_MAJOR >= 7
sunrealtype tret;
#else
realtype tret;
#endif
ion_private->node_number = __i + %(inner_id)s;
CVODE_flag = CVode(cvode_mem, t+dt, CVODE, &tret, CV_NORMAL);
assert(CVODE_flag == CV_SUCCESS);
#endif

//Unload the data back into LIMPET structures
''' % {"inner_id": inner_id})
                out('#ifdef USE_CVODE')
                for var in order(cvode):
                    out("GlobalData_t %s = %s;" % (lhs_dont(var), cvode_format(var)))
                out('#else')
                for var in order(cvode):
                    out("GlobalData_t %s = 0;" % (lhs_dont(var)))
                out('#endif')


            out("\n\n")
            out("//Finish the update")
            for var in order(self.modvars | self.root.statevars):
                lower_bound = var.is_a('lower_bound')
                upper_bound = var.is_a('upper_bound')

                if lower_bound or upper_bound:
                    if lower_bound:
                        out('if (%(new)s < %(val)s) { IIF_warn(__i, "%(var)s < %(val)s, clamping to %(val)s"); %(actual)s = %(val)s; }' % {
                                "var" : var.name,
                                "actual" : complete_format(var),
                                "new" : lhs_dont(var),
                                "val" : var.info('lower_bound'),
                                })
                    if upper_bound:
                        if lower_bound:
                            cond_str = 'else if'
                        else:
                            cond_str = 'if'

                        out(cond_str+' (%(new)s > %(val)s) { IIF_warn(__i, "%(var)s > %(val)s, clamping to %(val)s"); %(actual)s = %(val)s; }' % {
                                "var" : var.name,
                                "actual" : complete_format(var),
                                "new" : lhs_dont(var),
                                "val" : var.info('upper_bound'),
                                })
                    out("else {%s = %s;}" % (complete_format(var), lhs_dont(var)))
                else:
                    out("%s = %s;" % (complete_format(var), lhs_dont(var)))

            self.changeExternalUnits(-1, out, complete_format)
            self.printSetExternal(out, enable_data_layout_opt, "inner_id")

            if generation_target.isMlirGpu():
                out.close()
                out = old_out
            if generation_target.isMlirCodegen():

                if not generation_target.isMlirGpu():
                    out('''}\n  ''')
                    if enable_data_layout_opt:
                        out('''}\n ''')
                for (_, allocation) in self.lut_list:
                    out("%(allocation)s" % {"allocation": allocation})

                def create_mlir_call_arguments(end_str, state_suffix, external_var_suffix, lut_name):
                    mlir_call_arguments = "(start, " + end_str + ", (char*)p, (char*)region, dt, t," \
                                                                 "(char*)sv%(suffix)s" \
                                          % {"name": self.model_name, "suffix": state_suffix}
                    for i in order(recomputed_regional_constants):
                        mlir_call_arguments += ", " + i.name
                    for var in order((self.reqvars | self.modvars) & self.public):
                        mlir_call_arguments += ", (char*)%(var)s%(suffix)s" \
                                               % {"var": var, "suffix": external_var_suffix}
                    for i in order(specified_lookup_variables):
                        mlir_call_arguments += ", (char*)%(lut_name)s" % {'lut_name': lut_name} %{'table': i.name}
                    for (name, _) in self.lut_list:
                        mlir_call_arguments += ", (char*)%s_row" % name
                    if self.function_pointer_info:
                        mlir_call_arguments += ", __parent_table_address, __parent_table_size"
                        for i, j, k in self.function_pointer_info.values():
                            mlir_call_arguments += ", %s, %s, %s" % (i, j, k)
                    if len(rosenbrock):
                        if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                            mlir_call_arguments += ", sizeof(%(name)sIonType::private_type_vector), (char*)ion_private" % {"name": self.model_name, "suffix": generation_target.suffix}
                        else:
                            mlir_call_arguments += ", sizeof(%(name)sIonType::private_type), (char*)ion_private" % {"name": self.model_name, "suffix": generation_target.suffix}
                        if len(recomputed_regional_constants):
                            if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                                mlir_call_arguments += ", (char*)ion_private->rc.%s - (char*)ion_private"\
                                                   % order(recomputed_regional_constants)[0]
                            else:
                                mlir_call_arguments += ", (char*)&ion_private->rc.%s - (char*)ion_private"\
                                                   % order(recomputed_regional_constants)[0]
                        elif len(rosenbrock_nodal_req):
                            if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                                mlir_call_arguments += ", (char*)ion_private->nr.%s - (char*)ion_private"\
                                                       % order(rosenbrock_nodal_req)[0]
                            else:
                                mlir_call_arguments += ", (char*)&ion_private->nr.%s - (char*)ion_private"\
                                                       % order(rosenbrock_nodal_req)[0]

                    mlir_call_arguments += ");"
                    return mlir_call_arguments

                if generation_target == GenerationTarget.MLIR_ROCM_CODEGEN:
                    out("compute_%(name)s_mlir_rocm" % {"name": self.model_name} +
                        create_mlir_call_arguments("end", "_base", "_ext", "(char*)&imp.tables_d()[%(table)s_TAB]"))
                elif generation_target == GenerationTarget.MLIR_CUDA_CODEGEN:
                    out("compute_%(name)s_mlir_cuda" % {"name": self.model_name} +
                        create_mlir_call_arguments("end", "_base", "_ext", "(char*)&imp.tables_d()[%(table)s_TAB]"))
                    #for var in order((self.reqvars | self.modvars) & self.public):
                        #out("cudaMemPrefetchAsync(%(var)s_ext, sizeof(%(var)s_ext[0])*(end-start), cudaCpuDeviceId);" % {"var" : var})
                else:
                    out("#ifndef _OPENMP")
                    out("compute_%(name)s_mlir" % {"name": self.model_name} +
                        create_mlir_call_arguments("((end >> %(num_elements)s) << %(num_elements)s)"
                                                   % {"num_elements": int(math.log2(num_elements))},
                                                   "_base", "_ext", "(char*)&imp.tables()[%(table)s_TAB]"))
                    out("#else")
                    out("compute_%(name)s_mlir_openmp" % {"name": self.model_name} +
                        create_mlir_call_arguments("((end >> %(num_elements)s) << %(num_elements)s)"
                                                   % {"num_elements": int(math.log2(num_elements))},
                                                   "_base", "_ext", "(char*)&imp.tables()[%(table)s_TAB]"))
                mlir_function_definition_arguments = "(int, int, char*, char*, double, double, char*"

                for i in range(len(recomputed_regional_constants)):
                    mlir_function_definition_arguments += ", double"
                for var in range(len(order((self.reqvars | self.modvars) & self.public))):
                    mlir_function_definition_arguments += ", char*"
                for i in range(len(order(specified_lookup_variables))):
                    mlir_function_definition_arguments += ", char*"
                for i in range(len(self.lut_list)):
                    mlir_function_definition_arguments += ", char*"
                if self.function_pointer_info:
                    mlir_function_definition_arguments += ", char*, int"
                    for i in range(len(self.function_pointer_info)):
                        mlir_function_definition_arguments += ", int, int, int"
                if len(rosenbrock):
                    mlir_function_definition_arguments += ", int, char*, int"

                mlir_function_definition_arguments += ");\n"
                mlir_function_definition = ""
                if generation_target.isMlirGpu():
                    mlir_function_definition += "#ifdef MLIR_CODEGEN\n"

                mlir_function_definition += "#ifndef _OPENMP\n"
                mlir_function_definition += "void compute_%(name)s_mlir" \
                                            % {"name": self.model_name} + mlir_function_definition_arguments
                mlir_function_definition += "#else\n"
                mlir_function_definition += "void compute_%(name)s_mlir_openmp" \
                                                % {"name": self.model_name} + mlir_function_definition_arguments
                mlir_function_definition += "#endif\n"

                if generation_target.isMlirGpu():
                    if generation_target == GenerationTarget.MLIR_ROCM_CODEGEN:
                        mlir_function_definition += "#ifdef %s_MLIR_ROCM_GENERATED\n" % self.value_hash['header']
                        mlir_function_definition += "void compute_%(name)s_mlir_rocm" \
                                                    % {"name": self.model_name } + mlir_function_definition_arguments
                    elif generation_target == GenerationTarget.MLIR_CUDA_CODEGEN:
                        mlir_function_definition += "#ifdef %s_MLIR_CUDA_GENERATED\n" % self.value_hash['header']
                        mlir_function_definition += "void compute_%(name)s_mlir_cuda" \
                                                    % {"name": self.model_name } + mlir_function_definition_arguments
                    if generation_target.isMlirGpu() and rosenbrock:
                        mlir_function_definition += "#if defined __CUDA__ || defined __HIP__ \n"
                        mlir_function_definition += "void rbStepX_%(name)s(float *X, void *params, float h, int N );\n" \
                                                    % self.value_hash
                        mlir_function_definition += "#endif\n"
                    mlir_function_definition += "#endif\n"
                    mlir_function_definition += "#endif\n"

                # With all info being known, print the MLIR function
                self.header_file(mlir_function_definition)

                if not generation_target.isMlirGpu():
                    out('#endif')

                if enable_data_layout_opt:
                    state_dict = self.dl_state_dict
                else:
                    state_dict = self.state_dict

                region_dict = build_region_dict()
                # Create a new MLIR code generator and run it
                if not generation_target.isMlirGpu():
                    vector_codegen_single_thread \
                        = VectorCodegen(filename=out_dir + "/" + self.model_name + ".mlir",
                                        ionic_model=self.root,
                                        model_name=self.model_name,
                                        num_elements=num_elements,
                                        function_name='compute_{}_mlir'.format(self.model_name),
                                        symbol_table=self.root.table,
                                        ast_equations=self.root.main,
                                        params_dict=self.params_dict,
                                        region_dict=region_dict,
                                        regional_constants=order(recomputed_regional_constants),
                                        state_dict=state_dict,
                                        enums_dicts=self.enumerators_dict,
                                        function_pointer_info=self.function_pointer_info,
                                        lookup_variables=specified_lookup_variables,
                                        lookup_variables_order=[name for (name, _) in self.lut_list],
                                        tables=self.tables,
                                        all_lookup=all_lookup,
                                        external_variables=order((self.reqvars | self.modvars) & self.public),
                                        external_private_variables=order((self.reqvars | self.modvars) & self.private),
                                        external_update_variables=external_update_targets,
                                        external_units=external_units,
                                        needs_update_variables=order(self.modvars | self.root.statevars),
                                        good_vars=good_vars,
                                        update_inputs=update_inputs,
                                        fe_targets=fe_targets,
                                        rush_larsen_targets=rush_larsen_targets,
                                        rk2_targets=rk2_targets,
                                        rk4_targets=rk4_targets,
                                        sundnes_half_targets=sundnes_half_targets,
                                        sundnes=sundnes,
                                        markov_be_targets=markov_be_targets,
                                        markov_be_solve=markov_be_solve,
                                        rosenbrock=rosenbrock,
                                        rosenbrock_nodal_req=order(rosenbrock_nodal_req),
                                        rosenbrock_partial_targets=rosenbrock_partial_targets,
                                        rosenbrock_diff_targets=rosenbrock_diff_targets,
                                        rosenbrock_dict=self.rosenbrock_dict,
                                        global_constants=global_constants,
                                        lhs_format_func=lhs_dont,
                                        enable_store_opt=enable_store_opt,
                                        enable_data_layout_opt=enable_data_layout_opt,
                                        is_openmp=False)
                    vector_codegen_single_thread()

                    # # Create a new MLIR code generator for OpenMP and run it
                    vector_codegen_openmp\
                        = VectorCodegen(filename=out_dir + "/" + self.model_name + ".openmp.mlir",
                                        ionic_model=self.root,
                                        model_name=self.model_name,
                                        num_elements=num_elements,
                                        function_name='compute_{}_mlir_openmp'.format(self.model_name),
                                        symbol_table=self.root.table,
                                        ast_equations=self.root.main,
                                        params_dict=self.params_dict,
                                        region_dict=region_dict,
                                        regional_constants=order(recomputed_regional_constants),
                                        state_dict=state_dict,
                                        enums_dicts=self.enumerators_dict,
                                        function_pointer_info=self.function_pointer_info,
                                        lookup_variables=specified_lookup_variables,
                                        lookup_variables_order=[name for (name, _) in self.lut_list],
                                        tables=self.tables,
                                        all_lookup=all_lookup,
                                        external_variables=order((self.reqvars | self.modvars) & self.public),
                                        external_private_variables=order((self.reqvars | self.modvars) & self.private),
                                        external_update_variables=external_update_targets,
                                        external_units=external_units,
                                        needs_update_variables=order(self.modvars | self.root.statevars),
                                        good_vars=good_vars,
                                        update_inputs=update_inputs,
                                        fe_targets=fe_targets,
                                        rush_larsen_targets=rush_larsen_targets,
                                        rk2_targets=rk2_targets,
                                        rk4_targets=rk4_targets,
                                        sundnes_half_targets=sundnes_half_targets,
                                        sundnes=sundnes,
                                        markov_be_targets=markov_be_targets,
                                        markov_be_solve=markov_be_solve,
                                        rosenbrock=rosenbrock,
                                        rosenbrock_nodal_req=order(rosenbrock_nodal_req),
                                        rosenbrock_partial_targets=rosenbrock_partial_targets,
                                        rosenbrock_diff_targets=rosenbrock_diff_targets,
                                        rosenbrock_dict=self.rosenbrock_dict,
                                        global_constants=global_constants,
                                        lhs_format_func=lhs_dont,
                                        enable_store_opt=enable_store_opt,
                                        enable_data_layout_opt=enable_data_layout_opt,
                                        is_openmp=True)
                    # And run it
                    vector_codegen_openmp()
                else:
                    if generation_target == GenerationTarget.MLIR_ROCM_CODEGEN:
                        gpu_func_name='compute_{}_mlir_rocm'.format(self.model_name)
                        gpu_filename=out_dir + "/" + self.model_name + ".hip.mlir"
                    elif generation_target == GenerationTarget.MLIR_CUDA_CODEGEN:
                        gpu_func_name='compute_{}_mlir_cuda'.format(self.model_name)
                        gpu_filename=out_dir + "/" + self.model_name + ".cuda.mlir"
                    # If there is no data layout optimization, use the default state_dict
                    state_dict_gpu = self.state_dict
                    if enable_data_layout_opt:
                        dl_state_dict_gpu = self.dl_state_dict
                    else:
                        dl_state_dict_gpu = self.state_dict
                    # # Create a new MLIR code generator for OpenMP and run it
                    gpu_codegen \
                        = GpuCodegen(filename=gpu_filename,
                                     ionic_model=self.root,
                                     model_name=self.model_name,
                                     function_name=gpu_func_name,
                                     symbol_table=self.root.table,
                                     ast_equations=self.root.main,
                                     params_dict=self.params_dict,
                                     region_dict=region_dict,
                                     regional_constants=order(recomputed_regional_constants),
                                     state_dict=state_dict_gpu,
                                     enums_dicts=self.enumerators_dict,
                                     function_pointer_info=self.function_pointer_info,
                                     lookup_variables=specified_lookup_variables,
                                     lookup_variables_order=[name for (name, _) in self.lut_list],
                                     tables=self.tables,
                                     all_lookup=all_lookup,
                                     external_variables=order((self.reqvars | self.modvars) & self.public),
                                     external_private_variables=order((self.reqvars | self.modvars) & self.private),
                                     external_update_variables=external_update_targets,
                                     external_units=external_units,
                                     needs_update_variables=order(self.modvars | self.root.statevars),
                                     good_vars=good_vars,
                                     update_inputs=update_inputs,
                                     fe_targets=fe_targets,
                                     rush_larsen_targets=rush_larsen_targets,
                                     rk2_targets=rk2_targets,
                                     rk4_targets=rk4_targets,
                                     sundnes_half_targets=sundnes_half_targets,
                                     sundnes=sundnes,
                                     markov_be_targets=markov_be_targets,
                                     markov_be_solve=markov_be_solve,
                                     rosenbrock=rosenbrock,
                                     rosenbrock_nodal_req=order(rosenbrock_nodal_req),
                                     rosenbrock_partial_targets=rosenbrock_partial_targets,
                                     rosenbrock_diff_targets=rosenbrock_diff_targets,
                                     rosenbrock_dict=self.rosenbrock_dict,
                                     global_constants=global_constants,
                                     lhs_format_func=lhs_dont,
                                     enable_store_opt=enable_store_opt,
                                     enable_data_layout_opt=enable_data_layout_opt,
                                     num_elements=num_elements,
                                     data_layout_state_dict=dl_state_dict_gpu
                                     )
                    # And run it
                    gpu_codegen()

                if "PRINT_STATS" in mlir_options:
                    roofline_file = open(os.path.join(out_dir, "roofline.stats"), "a+")
                    roofline_file.write(self.model_name)
                    sizeof_lut = 0
                    for table_name in self.enumerators_dict:
                        sizeof_lut += self.enumerators_dict[table_name]["size"][1]
                    roofline_file.write("State Variables: %s bytes" % (self.state_dict["size"][1]))
                    roofline_file.write("LUT Variables: %s bytes" % sizeof_lut)
                    roofline_file.close()
            out.dec()
            out.dec()
            if not generation_target.isMlirCodegen():
                out('''
  }''' % self.value_hash)
                if enable_data_layout_opt:
                    out('''
                    inner_offset = 0;
}''')
            if cvode:
                out('''
  //free y
#ifdef USE_CVODE
  N_VDestroy(CVODE);
#endif
''' % self.value_hash)
            out('''
            }
}''')
            out('''#endif // %s
''' % function_guard)
            # Reset the function_pointer_info once the compute function is printed
            self.function_pointer_info = dict()
        printComputeFunction()
        if self.enable_mlir_codegen:
            printComputeFunction(self, out, GenerationTarget.MLIR_CPU_CODEGEN)
            if enable_gpu_codegen:
                printComputeFunction(self, out, GenerationTarget.MLIR_CUDA_CODEGEN)
                printComputeFunction(self, out, GenerationTarget.MLIR_ROCM_CODEGEN)
            self.header_file(self.header_file_final_string)
        if rosenbrock:
            def rosen_format_gen(generation_target = GenerationTarget.CPU_CODEGEN):
                def rosen_format(var,
                                 f=dont_set_modvars,
                                 nr=rosenbrock_nodal_req,
                                 rc=recomputed_regional_constants,
                                 rosenbrock=rosenbrock):
                    if var in nr:
                        if not generation_target.isMlirCodegen() or generation_target.isMlirGpu():
                            return "nr->"+var.name
                        else:
                            return "nr->"+var.name+"[0]"
                    elif var in rc:
                        if not generation_target.isMlirCodegen() or generation_target.isMlirGpu():
                            return "rc->"+var.name
                        else:
                            return "rc->"+var.name+"[0]"
                    elif var in rosenbrock:
                        return "Rosenbrock_X[ROSEN_%s]" % var
                    else:
                        return f(var)
                return rosen_format

            def printRosenbrockDefinitions(generation_target = GenerationTarget.CPU_CODEGEN):
                # We need to declare as non-local some variables that are modified here
                # but defined in the outer context
                nonlocal functions
                nonlocal func_vars

                # In the vectorized CPU case, we need to use the vectorized private structure
                if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                    private_type = '%(name)sIonType::private_type_vector' % self.value_hash
                else:
                    private_type = '%(name)sIonType::private_type' % self.value_hash

                if enable_data_layout_opt:
                    step = num_elements
                else:
                    step = 1
                rosenbrock_strings = {
                    'name': self.value_hash['name'],
                    'header': self.value_hash['header'] + generation_target.guard_suffix,
                    'regional_constants_struct_name': f'typename {private_type}::regional_constants_type',
                    'nodal_req_struct_name': f'typename {private_type}::nodal_req_type',
                    'private': private_type,
                    'suffix': generation_target.suffix,
                    'step': step
                }



                # First thing we do is generate a "global" guard for these functions
                # and extern "C" for linkage
                out('''#ifdef %(header)s
                extern "C" {
''' % rosenbrock_strings)

                if generation_target.isMlirCodegen():
                    if generation_target.isMlirGpu():
                        out('''
    __device__
    void rbStepX_%(name)s(float *X, void *params, float h, int N ) {
                        ''' % rosenbrock_strings)
                        out('''rbStepX(X, %(name)s_rosenbrock_f%(suffix)s, %(name)s_rosenbrock_jacobian%(suffix)s, params, h, N);'''
                            % rosenbrock_strings)
                        out('''}
''')

                    lut_types = ""
                    lut_values = ""
                    lut_getter = "tables()"
                    if generation_target.isMlirGpu():
                        lut_getter = "tables_d()"
                    for var in order(specified_lookup_variables):
                        lut_types += ",char*"
                        lut_values += ", (char*)&IF->%(lut_getter)s[%(table)s_TAB]" %{'table': var.name, 'lut_getter': lut_getter}
                    if not generation_target.isMlirGpu():
                        out('''
    void %(name)s_rosenbrock_f_mlir_vector(float*, float*, char*, char*, char*, char*, char* %(lut_types)s);
    void %(name)s_rosenbrock_jacobian_1d_mlir_vector(float*, float*, void*, int);
    void %(name)s_rosenbrock_jacobian_mlir_vector(float*, float*, char*, char*, char*, char*, char*, int dddd %(lut_types)s);

    void %(name)s_rosenbrock_f_mlir(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
      %(private)s* ion_private = (%(private)s*)user_data;
      %(regional_constants_struct_name)s* rc = &ion_private->rc;
      %(name)sIonType::IonIfDerived* IF = static_cast<%(name)sIonType::IonIfDerived*>(ion_private->IF);
      int __i = ((%(private)s*)user_data)->node_number;
      %(nodal_req_struct_name)s* nr = &ion_private->nr;
      cell_geom *region = &IF->cgeom();
      %(name)s_Params *p = IF->params();
      %(name)s_state *sv_base = IF->sv_tab().data();
      %(name)s_state *sv = sv_base+__i/%(step)s;
      int inner_id = __i %% %(step)s;
      %(name)s_rosenbrock_f_mlir_vector(Rosenbrock_DX, Rosenbrock_X, (char*)rc, (char*)nr, (char*)region, (char*)p, (char*)sv %(lut_vals)s);
                        ''' % {'name': self.model_name, 'private': rosenbrock_strings['private'],'lut_types': lut_types, 'lut_vals': lut_values, 'header': self.value_hash['header'], 'suffix': generation_target.suffix, 'regional_constants_struct_name': rosenbrock_strings['regional_constants_struct_name'], 'nodal_req_struct_name': rosenbrock_strings['nodal_req_struct_name'], 'step': rosenbrock_strings['step']} )
                        out('''
    }
''')
                    if generation_target.isMlirGpu():
                        out('''
__device__
''')
                out('''
    void %(name)s_rosenbrock_f%(suffix)s(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
      %(private)s* ion_private = (%(private)s*)user_data;
      %(regional_constants_struct_name)s* rc = &ion_private->rc;
      %(name)sIonType::IonIfDerived* IF = static_cast<%(name)sIonType::IonIfDerived*>(ion_private->IF);
      int __i = ((%(private)s*)user_data)->node_number;
      %(nodal_req_struct_name)s* nr = &ion_private->nr;
      cell_geom *region = &IF->cgeom();
      %(name)s_Params *p = IF->params();
      %(name)s_state *sv_base = IF->sv_tab().data();
      %(name)s_state *sv = sv_base+__i/%(step)s;
      int inner_id = __i %% %(step)s;

    ''' % rosenbrock_strings)
                out.inc()
                for var in order(specified_lookup_variables & update_inputs):
                    self.printLookupTableRow(out, var, rosen_format_gen(generation_target), generation_target.isMlirGpu())
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs,
                                          target=rosenbrock_diff_targets-good_vars,
                                          lhs_format = lhs_wrapper(rosen_format_gen(generation_target)),
                                          rhs_format = rosen_format_gen(generation_target),
                                          lookup=all_lookup,
                                          use_device_getter=generation_target.isMlirGpu(),
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for var in order(rosenbrock):
                    out("Rosenbrock_DX[ROSEN_%s] = %s;" % (var, rosen_format_gen(generation_target)(var.info('diff'))))
                out.dec()
                if generation_target.isMlirCodegen() and not generation_target.isMlirGpu():
                    out('''
    }

    void %(name)s_rosenbrock_jacobian_1d_mlir_vector(float* Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
      %(private)s* ion_private = (%(private)s*)user_data;
      %(regional_constants_struct_name)s* rc = &ion_private->rc;
      %(name)sIonType::IonIfDerived* IF = static_cast<%(name)sIonType::IonIfDerived*>(ion_private->IF);
      int __i = ((%(private)s*)user_data)->node_number;
      %(nodal_req_struct_name)s* nr = &ion_private->nr;
      cell_geom *region = &IF->cgeom();
      %(name)s_Params *p = IF->params();
      %(name)s_state *sv_base = IF->sv_tab().data();
      %(name)s_state *sv = sv_base+__i/%(step)s;
      int inner_id = __i %% %(step)s;
    ''' % rosenbrock_strings)
                    out('''
      %(name)s_rosenbrock_jacobian_mlir_vector(Rosenbrock_jacobian, Rosenbrock_X, (char*)rc, (char*)nr, (char*)region, (char*)p, (char*)sv, dddd %(lut_vals)s);
    }
''' % {'name': self.model_name, 'lut_vals': lut_values})
                else:
                    out('''
    }
    ''')
                if generation_target.isMlirGpu():
                    out('''
    __device__
''')
                out('''
    void %(name)s_rosenbrock_jacobian%(suffix)s(float** Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
      %(private)s* ion_private = (%(private)s*)user_data;
      %(regional_constants_struct_name)s* rc = &ion_private->rc;
      %(name)sIonType::IonIfDerived* IF = static_cast<%(name)sIonType::IonIfDerived*>(ion_private->IF);
      int __i = ((%(private)s*)user_data)->node_number;
      %(nodal_req_struct_name)s* nr = &ion_private->nr;
      cell_geom *region = &IF->cgeom();
      %(name)s_Params *p = IF->params();
      %(name)s_state *sv_base = IF->sv_tab().data();
      %(name)s_state *sv = sv_base+__i/%(step)s;
      int inner_id = __i %% %(step)s;
    ''' % rosenbrock_strings)
                out.inc()
                for var in order(specified_lookup_variables & update_inputs):
                    self.printLookupTableRow(out, var, rosen_format_gen(generation_target), generation_target.isMlirGpu())
                (this_func_count, this_func_vars, this_var_list) \
                    = self.printEquations(out=out,
                                          eqnmap=self.root.main,
                                          valid=good_vars | update_inputs,
                                          target=rosenbrock_partial_targets - good_vars,
                                          lhs_format = lhs_wrapper(rosen_format_gen(generation_target)),
                                          rhs_format = rosen_format_gen(generation_target),
                                          lookup=all_lookup,
                                          use_device_getter=generation_target.isMlirGpu(),
                                          )
                functions += this_func_count; func_vars |= this_func_vars
                for row_var in order(rosenbrock):
                    for col_var in order(rosenbrock):
                        out("Rosenbrock_jacobian[ROSEN_%s][ROSEN_%s] = %s;" %
                            (row_var,
                             col_var,
                             rosen_format_gen(generation_target)(self.root.partial(row_var.info('diff'), col_var))))
                out.dec()
                out('''
    }
    } // extern "C"
    #endif
    ''' % self.value_hash)
            printRosenbrockDefinitions()
            if self.enable_mlir_codegen:
                printRosenbrockDefinitions(GenerationTarget.MLIR_CPU_CODEGEN)
                if enable_gpu_codegen:
                    printRosenbrockDefinitions(GenerationTarget.MLIR_CUDA_CODEGEN)
                    printRosenbrockDefinitions(GenerationTarget.MLIR_ROCM_CODEGEN)
        if cvode:
            out('''
#ifdef USE_CVODE
#if SUNDIALS_VERSION_MAJOR >= 7
int %(name)s_internal_rhs_function(sunrealtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data) {
#else
int %(name)s_internal_rhs_function(realtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data) {
#endif

  %(private)s* ion_private = (%(private)s*)user_data;
  %(name)s_Private_cpu::regional_constants_type* rc = &ion_private->rc;
  %(name)sIonType::IonIfDerived *IF = static_cast<%(name)sIonType::IonIfDerived*>(ion_private->IF);
  %(name)s_Nodal_Req_cpu* nr = &ion_private->nr;
  int __i = ((%(private)s*)user_data)->node_number;
  cell_geom *region = &IF->cgeom();
  %(name)s_Params *p = IF->params();
  %(name)s_state *sv_base = IF->sv_tab().data();
  %(name)s_state *sv = sv_base+__i;
  int inner_id = 0;

''' % self.value_hash)
            out.inc()
            out("//Check to see if input is valid")
            def cvode_format(var,
                                 f=dont_set_modvars,
                                 nr=cvode_nodal_req,
                                 rc=recomputed_regional_constants,
                                 cvode=cvode):
                    if var in nr:
                        return "nr->"+var.name
                    elif var in rc:
                        return "rc->"+var.name
                    elif var in cvode:
                        return "NV_Ith_S(CVODE,CVODE_%s)" % var
                    else:
                        return f(var)
            for var in order(cvode):
                if var.is_a('lower_bound'):
                    out("if (%(var)s < %(bound)s) { return 1; }"
                        % {"var" : cvode_format(var), "bound" : var.info('lower_bound')})
                if var.is_a('upper_bound'):
                    out("if (%(var)s > %(bound)s) { return 1; }"
                        % {"var" : cvode_format(var), "bound" : var.info('upper_bound')})
            out("\n//Compute the equations")
            for var in order(specified_lookup_variables & update_inputs):
                self.printLookupTableRow(out, var, cvode_format, False)
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs,
                                      target=cvode_targets-good_vars,
                                      lhs_format = lhs_wrapper(cvode_format),
                                      rhs_format = cvode_format,
                                      lookup=all_lookup,
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            out("\n\n")
            out("//Complete the update")
            for var in order(cvode):
                out("NV_Ith_S(CVODE_dot,CVODE_%s) = %s;" % (var.name, cvode_format(var.info('diff'))))
            out.dec()
            out('''
  return 0;
}
#endif
''' % self.value_hash)

        if not self.root.getVars('trace'):
            out('''
bool %(name)sIonType::has_trace() const {
    return false;
}

void %(name)sIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
''' % self.value_hash)
        else:
            out('''
bool %(name)sIonType::has_trace() const {
    return true;
}

void %(name)sIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("%(name)s_trace_header.txt","wt");
    fprintf(theader->fd,
''' % self.value_hash)
            out.inc()
            out.inc()
            out.inc()
            out.inc()
            for var in order(self.root.getVars('trace')):
                out('\"%s\\n\"' % dont_set_modvars(var))
            out.dec()
            out(");")
            out.dec()
            out.dec()
            out.dec()
            out('''
    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * %(time_factor)s;
  cell_geom *region = &imp.cgeom();
  %(name)s_Params *p  = imp.params();

  %(name)s_state *sv_base = (%(name)s_state *)imp.sv_tab().data();
''' % self.value_hash)
            if enable_data_layout_opt:
                out('''
  %(name)s_state *sv = sv_base+node / %(num_elements)s;
  int __i = node;
  int inner_id = node %% %(num_elements)s;
''' % { "name": self.value_hash["name"], "num_elements": num_elements })
            else:
                out('''
  %(name)s_state *sv = sv_base+node;
  int __i = node;
''' % self.value_hash)
            out('''
  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
''' % self.value_hash)

            out.inc()
            printRegionalConstants()
            self.printExternalDef(out, enable_data_layout_opt)

            self.printGetExternal(out, enable_data_layout_opt)
            self.changeExternalUnits(1, out, complete_format)

            self.printEquations(out=out,
                                eqnmap=self.root.main,
                                valid=good_vars | update_inputs,
                                target=self.root.getVars('trace') - good_vars - update_inputs,
                                declared=(self.root.statevars | self.modvars) - self.root.getVars('store'),
                                lhs_format=lhs_dont,
                                rhs_format=dont_set_modvars,
                                )
            out("")
            out("//Output the desired variables")
            for var in order(self.root.getVars('trace')):
                out('fprintf(file, "%%4.12f\\t", %s);' % dont_set_modvars(var))
            self.changeExternalUnits(-1, out, complete_format)
            out.dec()
            out('''
}
''' % self.value_hash)
        out('''IonIfBase* %(name)sIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void %(name)sIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}''' % self.value_hash)

    def append(self, out_dir):
        File = open(os.path.join(out_dir, self.model_name+".cc"), "a+")

        File.write('''
}  // namespace limpet
        ''')
        File.close()

# # The environment variable has been replaced by a cmake variable (+ auto-detected)
# def configure_mlir_codegen():
#     def is_integer(n):
#         try:
#             int(n)
#         except ValueError:
#             return False
#         else:
#             return float(n).is_integer()
# 
#     def is_power_of_two(n):
#         return (n != 0) and (n & (n - 1) == 0)
# 
#     # Parse the number of elements of the vector. Default: 8
#     num_elements = 8
#     if 'MLIR_NUM_ELEMENTS' in os.environ:
#         val = os.environ['MLIR_NUM_ELEMENTS']
#         if not is_integer(val):
#             print("WARNING -> MLIR_NUM_ELEMENTS is not a integer. Using default value: " + str(num_elements))
#         else:
#             val = int(val)
#             if not is_power_of_two(int(val)):
#                 print( "WARNING -> MLIR_NUM_ELEMENTS is not power of 2. Using default value: " + str(num_elements))
#             else:
#                 num_elements = val
#     return True, num_elements

if __name__=='__main__':
    try:
        import val
    except ImportError as e:
        if e.args[0] == "No module named val" or "No module named 'val'":
            #running from the directory, import fake credentials.
            import fake_val as val
        else:
            raise e

    if val.isLicensed(sys.argv,'limpet_translator'):

        if len(sys.argv) < 4:
            print('Use '+sys.argv[0]+' <.model file> <path of imp_list.txt> <imp source dir> [MLIR_CODEGEN <loop unroll and vector size> [DATA_LAYOUT_OPT] [IS_GPU]]')
            print('Build openCARP (LIMPET) C++ code from EasyML .model files')
            sys.exit(1)

        filename = sys.argv[1]
        imp_list = sys.argv[2]
        out_dir  = sys.argv[3]

        # Check for MLIR Code generation options
        if not 'MLIR_CODEGEN' in sys.argv:
            enable_mlir_codegen = False
            enable_data_layout_opt = False
        else:
            from vector_mlir_codegen import *
            enable_mlir_codegen = True
            # follow-up argument : size of the vector/loop unroll factor (number of double's)
            try:
                num_elements = int(sys.argv[(sys.argv.index('MLIR_CODEGEN')+1)])
            except (IndexError, ValueError):
                print('Use '+sys.argv[0]+' <.model file> <path of imp_list.txt> <imp source dir> [MLIR_CODEGEN <loop unroll and vector size> [DATA_LAYOUT_OPT] [IS_GPU]]')
                print('Build openCARP (LIMPET) C++ code from EasyML .model files')
                sys.exit(1)

            mlir_options = sys.argv
            if 'STORE_OPT' in mlir_options:
                enable_store_opt = True
            else:
                enable_store_opt = False
            if 'DATA_LAYOUT_OPT' in mlir_options:
                enable_data_layout_opt = True
            else:
                enable_data_layout_opt = False
            if 'IS_GPU' in mlir_options:
                from gpu_mlir_codegen import *
                enable_gpu_codegen = True
            else:
                enable_gpu_codegen = False
        # end MLIR options

        external_units = parse_external_units(imp_list)

        try :
            im = IonicModel(open(filename, "r",  encoding="utf-8").read());
        except IOError :
            print('\nFile not found: '+filename+'\n')
            exit(1)

        obj = Limpet(im, filename, external_units, enable_mlir_codegen)
        obj.printHeader(out_dir)
        obj.printSource(out_dir)
        obj.append(out_dir)

