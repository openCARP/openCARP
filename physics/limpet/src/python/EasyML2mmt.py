#!/usr/bin/env python3
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   ITALIC = '\033[3m'
   STRIKE = '\033[9m'
   REVERSE = '\033[7m'
   BLINK = '\033[5m'
   END = '\033[0m'

description='''
convert a EasyML model file to myokit mmt format

In the EasyML file, comments with meta data provide the component name, which is not part of the 
EasyML specification. A component called {0.ITALIC}cname{0.END} is identified by one of the 2 forms: 
{0.BOLD}
# COMPONENT cname
// COMPONENT cname
{0.END}
All variables and equations belong to the last component declared. 
By default, an initial component {0.BLUE}cell{0.END} is in effect, but this particular name
can be changed.

{0.UNDERLINE}N.B.{0.END}: groups within groups are not supported
{0.UNDERLINE}N.B.{0.END}: hyperbolic trig functions are not supported
{0.UNDERLINE}N.B.{0.END}: try to avoid using variables named just {0.BOLD}d{0.END} or {0.BOLD}inf{0.END}


'''.format(color)

import argparse
import re
import sys
from subprocess import call
import warnings
import collections
import string

derived = [ r'alpha_{}',
            r'beta_{}',
            r'tau_{}', 
            r'diff_{}', 
            r'{}_inf', 
            r'{}_infinity', 
            r'd_{}_dt' ]   # derived variables

# regular expressions for finding variable names
var_re=r'(?<![.0-9])(?P<v>[A-Za-z]\w*)'
#var_re=r'([A-Za-z]\w*)'
veq_re=var_re+r'\s*='
vname_re = var_re+r'($|(\s+and)|(\s+or)|(\s*[^(\s\w]))'

digits=string.digits+'.'

# some EasyML keywords which look like variables
keywords = [ 'else', 'and', 'or', 'elif', 'if', 'not', 'inf' ]

# functions in EasyML needing conversion and their MMT equivalent
eml2mmt = { 'fabs':  'abs( {0[0]} )',
            'square':'( ({0[0]})*({0[0]}) )',
            'cube':  '( ({0[0]})*({0[0]})*({0[0]}) )',
            'pow':   '(({0[0]})^({0[1]}))',
            'heav':  'if( {0[0]}<0., 0., 1. )',
            'sign':  'if( {0[0]}<0, -1., 1. )',
            'expm1': '(exp({0[0]})-1.)',
            'min':   'if(({0[0]})<({0[1]}),{0[0]},{0[1]})',
            'max':   'if(({0[0]})>({0[1]}),{0[0]},{0[1]})',
            'atan2': 'atan(({0[0]})/({0[1]}))'
          }

class Vars :
    '''
    class listing variables used in a component and variables defined in a component
    '''
    def __init__(self):
        self.used = set()
        self.deffed = set()

    def use(self,v) :
        return v in self.used

    def defined(self, v):
        return v in self.deffed

    def add_use(self, v ) :
        self.used.add(v)

    def add_def(self, v ) :
        self.deffed.add(v)
        self.used.add(v)

    def extract(self, line ):
        '''
        identify the variables in the expression
        '''
        debug = False
        m = re.match( r'\s*([A-Za-z]\w*)\s*=', line )
        if debug: print(line)
        if m:
            self.deffed.add(m.group(1))
            if debug: print( 'def: ', m.group(1) )
            for exp in derived:
                rexp = exp.format( '([A-Za-z]\w*)' )
                m1   = re.match( rexp, m.group(1) )
                if m1:
                    self.add_def(m1.group(1))
                    if debug: print( 'def: ', m1.group(1) )

        for m in re.finditer(vname_re, line ):
            if not not_var( m.group(1), line, m.start(1) ):
                self.add_use(m.group(1))
                if debug: print( 'used: ', m.group(1) )
        if debug: print()


class EasyML_to_MMT :

    def comps( self ):
        # list of components
        return self.comp_lines.keys()

    def var_used( self, var ):
        # is var used ?
        if var in self.var2comp :
            for comp,cvars in self.comp_vars.items():
                if cvars.use( var ):
                    return True
        return False

    def comp_data( self, comp ):
        # return lines and variables in a component
        return self.comp_lines[comp], self.comp_vars[comp]

    def var_comp( self, var ):
        # return component containing variable
        return self.var2comp[var]

    def var_def( self, var, retline=False ) :
        '''
        return the variable definition
        var is unqualified name
        retline = return the line number as well, -1 if not found, -2 for init assign
        '''
        val  = None
        line = -1
        comp = self.var2comp.get(var)
        if comp is not None : 
            for i,l in enumerate(self.comp_lines[comp]):
                m = re.match( veq_re+r'(?P<def>.*)', l )
                if m:
                    val  = m.group('def').strip()
                    line = i
                    break
            else:
                val = self.init_assignments.get(comp+'.'+var,'')
                val = -2
        return (val, line) if retline else val


    def __init__( self, filename, init_component, istim='Istim', plug=False, verbose=False ):

        self.comp_lines     = collections.defaultdict(list)
        self.comp_vars      = dict()
        self.groups         = []
        self.init_component = init_component
        self.Vm             = None
        self.Iion           = None
        self.I_stim         = istim

        commented = False                # in the middle of a multiline comment?
        component = init_component       # current component
        glevel    = 0                    # group level (0=not in a group)

        with open( filename, 'r' ) as origin :
            f = origin.readlines()

        # break up file into components and groups
        for l in f :
            if verbose : print(l.strip(), end=' -> ')
            line, component, commented = remove_comments(l, component, commented)
            if verbose : print(line, end=' -> ')

            self.find_VI(line)

            line = remove_markup(line)
            if verbose : print(line, end=' | ')

            if glevel : line, glevel = end_group(line, self.groups[-1], glevel)
            if verbose : print( glevel, end=' | ' )

            if not glevel : line, glevel = group_start( line, self.groups )
            if verbose : print( glevel, '->', line  )

            self.comp_lines[component].append(line)
 
        # restructure lines, one statement per line
        for comp, lines in self.comp_lines.items() :
            self.comp_lines[comp] = reline( lines )
        for i,g in enumerate(self.groups) :
            self.groups[i] = reline( g )
            
        self.extract_variables()

        self.check_Iion(plug)

        for comp, lines in self.comp_lines.items() :
            self.comp_lines[comp] = self.if_handler( lines )
     
        for comp, lines in self.comp_lines.items() : error = check_errors( lines )

        self.find_init_assign()

        for comp in self.comp_lines.keys() : self.convert_to_mmt_funcs( comp )

        for comp in self.comp_lines.keys() : self.dt_to_dot( comp )

        self.sub_out_vars_in_init()

        for comp in self.comp_lines.keys() : self.qualify_vars( comp )


    def convert_to_mmt_funcs( self, comp ):
        '''
        convert the EasyML functions to mmt equivalents
        '''
        lines = self.comp_lines[comp]
        converted = []
        for l in lines :
            sub = True  # substitution made?
            while sub:
                nl = ''
                pos = 0
                sub = False
                for m in re.finditer( r'([a-zA-Z]\w{2,})\s*(\()', l ) :
                    if m.start(2) < pos : break   # function within function
                    func = m.group(1)
                    if func in eml2mmt :
                        end, args = find_args( l, m.start(2) )
                        cnvt = eml2mmt[func].format(args)
                        nl += l[pos:m.start(1)] + cnvt
                        pos = end + 1
                        sub = True 
                nl += l[pos:]
                if sub : l = nl
            nl  = re.sub(r'ternOP\(', r'if(', nl )
            converted.append(nl)

        self.comp_lines[comp] = converted


    def extract_variables( self ):
        '''
        find all variables used
        '''
        self.comp_vars = dict()
        for k, lines in self.comp_lines.items(): 
            self.comp_vars[k] = Vars()
            for l in lines :
                self.comp_vars[k].extract(l)
        self.find_grp_comp()
        self.var2comp = self.map_vars()

        #X is only declared for a_X,b_X if X is used
        for v,c in self.var2comp.items() :
            m = re.match( r'(a|b)_'+var_re, v )
            if m:
                var = m.group('v')
                for comp, cvars in self.comp_vars.items() :
                    if cvars.use(var):
                        self.comp_vars[c].add_def(var)
                        self.var2comp[var] = c
                        break
            

    def dt_to_dot( self, comp ) :
        ''' 
        add implied variables and explicit derivatives
        '''
        lines  = self.comp_lines[comp]
        alphas = dict()
        betas  = dict()
        taus   = dict()
        infs   = dict()

        nls   = []
        diff_fnd = set()
        for l in lines :
            # convert diff_X and d_X_dt to dot(X)
            m = re.match( r'(?P<var>diff_(?P<v>[A-Za-z]\w*))\s*=\s*(?P<eqn>.*)', l )
            if not m :
                m = re.match( r'(?P<var>d_(?P<v>[A-Za-z]\w*)_dt)\s*=\s*(?P<eqn>.*)', l )
            if m and m.group('v') in self.var2comp : 
                if self.var_used(m.group('v')):        #derivative used elsewhere so keep it
                    nls.append(l)
                    nls.append( 'dot({0}) = {1}'.format( m.group('v'), m.group('var') ) )
                else:
                    nls.append( 'dot({}) = {}'.format( m.group('v'), m.group('eqn')) )
                diff_fnd.add(m.group(2))
                continue
            else:
                # find alphas, betas, taus, and infinity values
                nls.append(l)
                prefs= { r'tau_':taus, r'a_':alphas, r'alpha_':alphas, 'b_':betas, r'beta_':betas }
                for p, pset in prefs.items() :
                    m = re.match( p+r'([A-Za-z]\w*)\s*=', l )
                    if m :
                        pset[m.group(1)] = p
                        break;
                m = re.match( r'([A-Za-z]\w*)_(inf(inity)?)\s*=', l )
                if m:
                    infs[m.group(1)] = m.group(2)

        #if alpha/beta or tau_inf pair, add dot()
        found = set()
        for a,alpha in alphas.items():
            if a not in diff_fnd and a in betas and a in self.var2comp:
                found.add(a)
                if self.var_used('diff_'+a) :
                    nls.append( 'diff_{0} = {1}{0}*(1.-{0}) - {2}{0}*{0}'.format(a,alpha,betas[a] ) )
                    nls.append( 'dot({}) = diff_{}'.format(a) )
                else:
                    nls.append( 'dot({0}) = {1}{0}*(1.-{0}) - {2}{0}*{0}'.format(a,alpha,betas[a] ) )
        for t,tau in taus.items():
            if t not in diff_fnd and t in infs and t in self.var2comp:
                found.add(t)
                if self.var_used('diff_'+t) :
                    nls.append( 'diff_{0} = ({0}_{1} - {0})/tau_{0}'.format(t,infs[t]) )
                    nls.append( 'dot({0}) = diff_{0}'.format(t) )
                else:
                    nls.append( 'dot({0}) = ({0}_{1} - {0})/tau_{0}'.format(t,infs[t]) )
        for v in found:
            fqv = self.var2comp[v]+'.'+v
            if fqv not in self.init_assignments:
                print( 'Assigning initial value of 0 to {0.BOLD}{1}{0.END}'.format(color,fqv) )
                self.init_assignments[fqv] = '0.'
                
        self.comp_lines[comp] = nls


    def find_init_assign( self ) :
        '''
        find variables which declare initial values
        '''
        self.init_assignments = dict()
        remove = collections.defaultdict(list)
        #first do initializations which are constants
        for comp, lines in self.comp_lines.items() :
            for i,l in enumerate(lines):
                m = re.search( var_re+r'_init\s*=\s*(.*)', l )
                if m:
                    comp_found = self.var2comp.get(m.group(1))
                    if comp_found is None :
                        print( "Attn: "+m.group(1)+" initialized but not defined, added to", self.init_component)
                        comp_found = self.init_component
                    remove[comp].append(i)
                    self.init_assignments[comp_found+'.'+m.group(1)] = m.group(2)
                
        for c, rl in remove.items():
            for r in reversed(rl):
                del self.comp_lines[c][r] 


    def sub_out_vars_in_init( self ) :
        '''
        substitute any variables in RHS inital assignments with their definition
        '''

        for v,rhs in self.init_assignments.items():
            substitute = True
            count      = 0
            new_rhs    = rhs
            while substitute :
                rhs = new_rhs
                #print(count,'>>>>>',rhs)
                substitute = False
                for m in re.finditer( vname_re, rhs ):             #find variables
                    #print( '--',m.group(1))
                    comp = self.var2comp[m.group('v')]               #identify their component
                    #first check init values - all constant values
                    qualvar = comp+'.'+m.group('v')
                    if qualvar in self.init_assignments:
                        sub = self.init_assignments[qualvar]
                    else :
                        sub = self.var_def(m.group('v')) 
                        if sub is not None :
                            substitute = True                      #substitute the definition for the variable
                        else :
                            print('{0.RED}Error{0.END}: {0.BOLD}{1}{0.END} not found'.format(color, m.group('v')))
                            sys.exit(1)
                    new_rhs = re.sub( r'(\W|^)'+m.group('v')+r'(\W|$)', r'\1('+sub+r')\2', new_rhs )
                count += 1
            self.init_assignments[v] = new_rhs
            #print('<<<<<',new_rhs)

    
    def if_handler( self, lines ):
        '''
        We need to make an if statement for every variable that is part of a clause
        
        assume { and } are on lines by themselves
        '''
        converted = []
        ELSE = None
        lno = 0

        while lno < len(lines) :
            pred = []
            l = replace_ternary( lines[lno] )
            m = re.search( r'if\s*\((.*)\)', l ) # look for if
            lno += 1

            if not m :
                converted.append(l)
                continue
            #print(l,'---',m.group(1))
        
            vlist = collections.defaultdict(list) # every variable has a list entry for every if clause
            pred.append(m.group(1))               # list of predicates for each clause
            lno += 1
            mode = 'assign'                       # assign = look for variable assignments
            while True and lno<len(lines) :
                
                if mode=='assign' :
                    if lines[lno] == '}':         # } = end of clause
                        lno += 1
                        if pred[-1] is ELSE:      # we are in the final else clause
                            break
                        mode = 'check_clause'     # check_more = look for another clause
                    else:                         # get an assignment from the clause
                        m = re.search( veq_re+r'\s*(.*)', lines[lno] )
                        if m :
                            vlist[m.group(1)].append(m.group(2))
                        lno += 1

                elif mode=='check_clause' :         # examine if next line is another clause
                    if lines[lno] == 'else' :       # check if else clause
                        mode = 'assign'
                        pred.append(ELSE)
                        lno += 2                    # skip line of isolated "{"
                        continue
                    m = re.search( r'elif\s*\((.*?)\)', lines[lno] )  # check for elif clause
                    if m:                           # elif found
                        pred.append(m.group(1))
                        lno += 2                    # skip line of isolated "{"
                        mode = 'assign'            
                    else:                           # end of if statement (no more clauses)
                        break
            
            for var, vals in vlist.items():       # need to construct separate if functions for each variable
                l = var+" = "
                if len(pred)==1 :                                           #if, no else
                    l += 'if( {0[0]}, {1[0]}, {2} )'.format(pred,vals,var)
                elif len(pred) == 2 and pred[1] is ELSE:                    #if/else
                    l += 'if( {0[0]}, {1[0]}, {1[1]} )'.format(pred,vals)
                else:                                                       #1 or more elifs
                    if len(pred) != len(vals) :
                        printf( '{0.RED}IF error{0.END}: {0.BOLD){1}{0.END} not found in every clause'.format(color,var) )
                        sys.exit(1)
                    l += 'piecewise('
                    for p,v in zip(pred,vals):
                        if p is ELSE:
                            l += ' '+v
                        else:
                            l += ' {}, {},'.format(p, v)
                    if pred[-1] is not ELSE:
                        l += ' '+var
                    l += ' )'
                converted.append(l)
                
        return converted


    def find_grp_comp( self ):
        '''
        find component for group variables
        '''
        for i,g in enumerate(self.groups):
            self.groups[i] = self.if_handler(g)

        for g in self.groups:
            for l in g:
                m = re.search( veq_re, l )
                if m:
                    for c,vlst in self.comp_vars.items() :
                        if vlst.use(m.group(1)) :
                            comp = c
                            break
                    else :
                        print( "Warning: "+m.group(1)+" assigned but not used")
                        comp = self.init_component
                    self.comp_lines[comp].append(l)
                    self.comp_vars[comp].add_def(m.group(1))


    def map_vars( self ):
        '''
        assign variables to a component and look for external variables
        '''
        varmap = dict()
        for k, vset in self.comp_vars.items() :
            for v in vset.deffed :
                varmap[v] = k

        extern = set()
        for k, vset in self.comp_vars.items() :
            for v in vset.used :
                comp = varmap.get(v)
                if comp is None :
                    print( 'Assuming '+v+ " external" )
                    varmap[v] = self.init_component
                    extern.add(v)

        for v in extern :
            self.comp_vars[self.init_component].deffed.add(v)
            self.comp_vars[self.init_component].used.add(v)

        return varmap


    def qualify_vars( self, comp ) :
        '''
        if variable is defined in another component, use qualified name
        '''
        newlines = []
        for l in self.comp_lines[comp]: 
            nl = ''
            cpos = 0
            for m in re.finditer(vname_re, l ):
                if not m.group('v') or (m.start('v')>0 and l[m.start('v')-1] in digits) : continue 
                nl += l[cpos:m.start('v')]
                try :
                    c = self.var2comp[m.group(1)]
                except KeyError:
                    if l[m.start('v'):m.start('v')+9] != 'stimulus.':
                        print(l,'{0.RED}ERROR{0.END} --- {0.RED}{1}{0.END} not found elsewhere'.format(color,m.group('v')) )
                        sys.exit(1)
                if c != comp :
                    nl += c+'.'
                nl += m.group('v')
                cpos = m.end('v')
            nl += l[cpos:]
            newlines.append(nl)

        self.comp_lines[comp] = newlines


    def find_VI( self, line ) :
        '''
        find the variable names used for Vm and Iion. 
        '''
        if self.Vm is None :
            m = re.match( r'Vm\s*;.*\.external\(\s*\)', line.strip() )
            if m : 
                self.Vm = 'Vm'
            else:
                m = re.match( var_re+r'\s*;.*\.external\(\s*Vm\s*\)', line.strip() )
                if m : 
                    self.Vm = m.group(1)

        if self.Iion is not None : return

        m = re.match( var_re+r'\s*([^;]*);.*\.external\(\s*Iion\s*\)', line.strip() )
        if m : self.Iion = m.group(1)
        m = re.match( r'Iion\s*([^;]*);.*\.external\(\s*\)', line.strip() )
        if m : self.Iion = 'Iion'

    def check_Iion( self, plug ):
        '''
        make sure Iion is defined
        if not, see if Vm_diff is and try that
        '''
        if self.Iion is None :
            vdiffdef,rem = self.var_def( 'diff_'+self.Vm, True )
            if vdiffdef is None :
                print( '{0.RED}Error{0.END}: {0.BOLD}Iion{0.END} needs to be used'.format(color) )
                sys.exit(1)

            # this avoids an error if Iion is used on the RHS of diff_V 
            m = re.search( r'(\W|^)Iion(\W|$)', vdiffdef )
            if m and 'Iion' in self.var2comp :
                print( '{0.RED}Attn{0.END}: assuming {0.BOLD}{1} = -Iion{0.END}'.format(color,self.Vm) )
            else:
                self.var2comp.add_def('Iion')
                print( '{0.RED}Attn{0.END}: {0.BOLD}{1}{0.END} converted to -Iion'.format(color,self.Vm) )
                self.comp_lines[self.init_component].append('Iion = -({})'.format(vdiffdef))
            del self.comp_lines[self.init_component][rem]
            self.Iion = 'Iion'
        
        if plug:
            i, rhs = get_rhs( self.comp_lines[self.var2comp[self.Iion]], self.Iion )
            if rhs is not None:
                l = self.Iion+' = '+re.sub( 'Iion','0', rhs )
                self.comp_lines[self.var2comp[self.Iion]][i] = l


    def print_mmt( self, mmtfile, bcl=500, tend=1000, author=None, desc=None ) :
        '''
        print thee mmt file
        '''
        with open( mmtfile, 'w') as outf :

            outf.write('[[model]]\nname: {}\n'.format(mmtfile[:-4]))
            if desc:
                outf.write('desc: {}\n'.format(desc))
            if author:
                outf.write('author: {}\n'.format(author))
            outf.write('\n')
 
            for v,rhs in self.init_assignments.items():
                outf.write( v+' = '+rhs+'\n' )
            outf.write('\n')

            for c, lines in self.comp_lines.items():
                outf.write('['+c+']\n')
                for l in lines :
                    outf.write( l+'\n' )
                outf.write('\n')

            outf.write( '''

[[protocol]]
# Level  Start    Length   Period   Multiplier
1.0      20.0     0.5      {bcl}      0

[[script]]
import matplotlib.pyplot as plt
import myokit

# Get model and protocol, create simulation
m = get_model()
p = get_protocol()
s = myokit.Simulation(m, p)

# Run simulation
d = s.run({tend})

# Display the results
var = '{comp}.{V}'
plt.figure()
d.save_csv( var+'.dat', header=False, precision=None, delimiter=' ' )
plt.plot(d.time(), d[var])
plt.title(var)
plt.show()
'''.format(bcl=bcl, tend=tend, comp=self.var2comp[self.Vm], V=self.Vm) )


    def add_stimulus( self, stim_amp=-80, pace='pace' ):
        '''
        define the stimulus section
        '''
        ncomp = 'stimulus'
        new_vars = [ 't', pace, self.I_stim, 'stimulus_amplitude' ]
        
        self.comp_vars[ncomp] = Vars()
        for nv in new_vars:
            self.comp_vars[ncomp].add_def( nv )

        self.comp_lines[ncomp] = [
            't = 0 bind time',
            pace+' = 0 bind pace',
            'stimulus_amplitude = '+str(stim_amp),
            '{} = {} * stimulus_amplitude'.format(self.I_stim, pace)
            ]

        compV = self.var2comp[self.Vm]
        compI = self.var2comp[self.Iion]
        self.comp_lines[compV].append( 'dot({}) = -{}{}-{}'.format(
                                                self.Vm,
                                                compI+'.' if compV!=compI else '',
                                                self.Iion,
                                                'stimulus.'+self.I_stim) )


def remove_markup( line ) :
    # remove EasyML markup functions
    return re.sub( r'\.\w+\(.*?\)\s*;', '', line )


def not_var( v, line, i ):
    '''
    return true if not really a variable
    '''
    if v in keywords : return True
    if v[0]=='e' and i>0 and line[i-1] in digits : return True
    return False


def check_errors( lines ) :
    '''
    draw attention to a couple of obvious errors
    '''
    error = False
    for l in lines:
        m = re.search( r'(?P<op>(\+|-|/|\*)\s*=)', l )
        if m: 
            print( '{0.RED}Warning:{0.END} operator {0.RED}{1}{0.END} not permitted in mmt format'.format(color,m.group('op')) )
            print( '\t',re.sub( r'(?P<op>(\+|-|/|\*)\s*=)', r'{0.UNDERLINE}\g<op>{0.END}',l).format(color))
            error = True
        m = re.search( r'(\W|^)dt(\W|$)', l )
        if m :
            print( '{0.RED}Warning:{0.END} use of {0.RED}dt{0.END} variable not allowed - dynamic time stepping'.format(color) )
            print( '\t',re.sub(r'(\W)(dt)(\W)', r'\1{0.UNDERLINE}dt{0.END}\3', l ).format(color) )
            error = True
    if error : 
        sys.exit(1)

    return error


def remove_comments( line, component, commented ):
    if commented:                                   #in the middle of a multiline comment
        line, no = re.subn( r'.*?\*/', '', line )
        if no : 
            commented = False
        else :
            line = ''
    while True and line:
        m = re.search( r'(?P<keep>.*?)(?P<notation>#|(//)|(/\*))(?P<rest>.*)', line )
        if m :
            line = m.group('keep')
            if m.group('notation') == '/*' :
                m0 = re.search( r'(?P<comment>.*?)(\*/)(?P<rest>.*)', m.group('rest') )
                if m0 :
                    line += m0.group('rest')
                else :
                    commented = True
                    break;
            else:                         # '#' or '//'
                m1 = re.search( r'\s*COMPONENT\s+(\w+)', m.group('rest'), re.I )
                if m1:
                    component = m1.group(1)
                break
        else:
            break

    return line.strip(), component, commented


def end_group( line, grp, level ):
    '''
    look for end of a group
    line  - line to search
    grp   - lines of group
    level - level of brace indentation

    if line in group, append it to group
    return: part of line not in group, brace level
    '''

    if re.search( r'\Wgroup\W', line ):
        print( 'nested groups need work' )
        sys.exit(1)

    for c in range(len(line)):
        if line[c] == '}' :
            level -= 1
            if level == 0 :
                grp.append(line[:c+1])
                return line[c+1:], 0
        elif line[c] == '{' :
            level += 1
    else:
        grp.append(line)
        return '', level


#look for start of a group
def group_start( line, grps ) :
    m = re.search( r'(?P<pre>.*)group\s*{(?P<post>.*)', line )
    if m :
        line = m.group('pre')
        grps.append(list())
        grps[-1].append(m.group('post'))
        return m.group('pre'), 1
    else:
        return line, 0


# make each line a complete statement 
# eliminate empty lines
# remove lines with only a variable name
def reline( lines ) :
    all_lines = ' '.join( lines )
    all_lines = re.sub( r'{', r';{;', all_lines ) # on line by itself
    all_lines = re.sub( r'}', r';};', all_lines ) # on line by itself
    all_lines = re.sub( r'\s*(?P<OP>([><!=*/+-]=)|=)\s*', r' \g<OP> ', all_lines ) #prettify =
    lines = all_lines.split(';')
    remove = set()
    for i,l in enumerate(lines):
        lines[i] = l.strip()
        if not lines[i] : remove.add(i)
        m = re.match( var_re+r'\s*$', lines[i] )
        if m and m.group(1) is not None and m.group(1)!='else': 
            remove.add(i)  #"else" is the only word that can appear by itself on a line
    lines = [ e for i,e in enumerate(lines) if i not in remove ]
    return lines


def get_rhs( lines, v ):
    '''
    get  RHS of adssignment
    '''
    for i,l in enumerate(lines) :
        m = re.match( v+r'\s*=(.*)', l )
        if m :
            return i, m.group(1).strip()
    return 0, None


def find_args( l, pos ) :
    '''
    find the comma separated arguments in a function call
    l   = line with function call
    pos = opening bracket position

    return position of the last bracket, arglist
    '''
    level = 1
    pos  += 1
    start = pos
    args  = []

    while level>0 :
        if l[pos] == ')':
            level -= 1
        elif l[pos] == '(':
            level += 1
        elif l[pos]==',' and level==1 :
            args.append( l[start:pos].strip() )
            start = pos+1
        pos += 1

    pos -= 1
    args.append( l[start:pos].strip() )
    return pos, args
    

def replace_ternary( line ) :
        '''
        replace ()?: with ternOP(,,)
        note that we will later replace with if
        '''
        
        while True:
            m1 = re.search( '(.*)\)\s*\?\s*([^:]+?)\s*:(.*)', line ) #ternary operator
            if not m1 :
                return line
            
            # look backwards to determine predicate: (...) ? 
            ind = m1.end(1)-1
            pre = line[:ind+1]
            level=1
            while level: 
                if pre[ind] == ')' : 
                    level += 1
                elif pre[ind] == '(' :
                    level -= 1
                ind -= 1
            pred = pre[ind+2:]
            pre  = pre[:ind+1]

            post = m1.group(3).strip()
            if pre.strip()[-1] == '(' :   #is the whole expression wrapped in ()?
                end, arg = find_args( post, -1  )
                post = post[:end]+')'+post[end:]
            else:
                post += ')'

            line = pre + 'ternOP({}, {}, {}'.format( pred, m1.group(2), post )

def reference( easyMLfile ):
        with open(easyMLfile, 'r', encoding="utf-8") as file:
            astr = file.read().replace('\n', '')

        keyword=[
        "Author:",
        "Year:",
        "Title:",
        "Journal:",
        "DOI:",
        "Comments:"
        ]

        reference=[]
        tokens=[]

        tokens=re.split(';|\n',astr)
        tokens = list(filter(None, tokens))

        for i in range(len(tokens)):
            if re.search(r'((Author)|(author))',tokens[i]) is not None:
                reference.append(keyword[0]+''.join(re.findall('[^::=]*$',tokens[i])))

            if re.search(r'((Year)|(year))',tokens[i]) is not None:
                reference.append(keyword[1]+''.join(re.findall('[^::=]*$',tokens[i])))

            if re.search(r'((Title)|(title))',tokens[i]) is not None:
                reference.append(keyword[2]+''.join(re.findall('[^::=]*$',tokens[i])))

            if re.search(r'((Journal)|(journal))',tokens[i]) is not None:
                reference.append(keyword[3]+''.join(re.findall('[^::=]*$',tokens[i])))

            if re.search(r'((DOI)|(doi))',tokens[i]) is not None:
                reference.append(keyword[4]+''.join(re.findall('[^::=]*$',tokens[i])))

            if re.search(r'((Comments)|(comments))',tokens[i]) is not None:
                reference.append(keyword[5]+''.join(re.findall('[^::=]*$',tokens[i])))

        ref_vars = { 'reference' : '\n'.join('*  ' + x for x in reference) }

        return reference


if __name__=='__main__':

    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('eml', help='EasyML file' )
    parser.add_argument('--verbose', action='store_true', help='verbose output' )
    parser.add_argument('--init_component', default='cell', help='initial component [%(default)s]' )
    parser.add_argument('--istim', default='I_stim', help='name of stimulus current [%(default)s]' )
    parser.add_argument('--pace_name', default='pace', help='name of pacing variable [%(default)s]' )
    parser.add_argument('--tend', default=1100., type=float, help='duration [%(default)s]' )
    parser.add_argument('--bcl', default=500., type=float, help='pacing cycle length [%(default)s]' )
    parser.add_argument('--plugin', action='store_true', help='IMP is a plugin' )
    opts = parser.parse_args()

    if opts.eml[-6:] != '.model' : opts.eml += '.model'

    ref = reference(opts.eml)
    if ref[0]: opts.author = ref[0][8:]
    if ref[1]: opts.desc = "; ".join(ref[1:])
    model = EasyML_to_MMT( opts.eml, opts.init_component, opts.istim, opts.plugin )
    model.add_stimulus( pace=opts.pace_name ) 
    model.print_mmt( opts.eml[:-6]+'.mmt', opts.bcl, opts.tend, opts.author, opts.desc )

