# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2022 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from im import order
from mlir_codegen import MlirCodegen
import mlir.ir as ir
import mlir.dialects as dialects
import mlir.dialects.arith
import mlir.dialects.cf
import mlir.dialects.scf
import mlir.dialects.func
import mlir.dialects.memref
import mlir.dialects.vector

class GpuCodegen(MlirCodegen):

    def __init__(self, ionic_model, filename, function_name, symbol_table, params_dict, region_dict,
                 ast_equations, regional_constants, state_dict, enums_dicts, tables, function_pointer_info,
                 lookup_variables, external_variables, external_private_variables, external_units,
                 needs_update_variables, all_lookup, external_update_variables, good_vars, update_inputs,
                 fe_targets, rush_larsen_targets, rk2_targets, rk4_targets, sundnes_half_targets, sundnes,
                 markov_be_targets, markov_be_solve, rosenbrock, rosenbrock_nodal_req, rosenbrock_partial_targets,
                 rosenbrock_diff_targets, rosenbrock_dict, global_constants, lhs_format_func, enable_store_opt,
                 enable_data_layout_opt, lookup_variables_order, model_name, num_elements, data_layout_state_dict):
        # The number of elements per vector and the data layout offset dictionary for state variables are used when
        # compiling for GPU with the data layout optimization for vectorization enabled
        self.num_elements = num_elements
        self.data_layout_state_dict = data_layout_state_dict
        super().__init__(ionic_model, model_name, filename, function_name, symbol_table, params_dict, region_dict,
                         ast_equations, regional_constants, data_layout_state_dict, enums_dicts, tables, function_pointer_info,
                         lookup_variables, external_variables, external_private_variables, external_units,
                         needs_update_variables, all_lookup, external_update_variables, good_vars, update_inputs,
                         fe_targets, rush_larsen_targets, rk2_targets, rk4_targets, sundnes_half_targets, sundnes,
                         markov_be_targets, markov_be_solve, rosenbrock, rosenbrock_nodal_req,
                         rosenbrock_partial_targets, rosenbrock_diff_targets, rosenbrock_dict, global_constants,
                         lhs_format_func, enable_store_opt, enable_data_layout_opt, lookup_variables_order)


    def get_computation_size(self):
        return 1

    def create_integer_constant(self, element_type, value):
        return dialects.arith.ConstantOp(element_type, int(value))

    def create_floating_point_constant(self, element_type, value):
        return dialects.arith.ConstantOp(element_type, float(value))

    def get_lut_function_name(self):
        return "LUT_interpRow"

    def get_thread_id(self):
        return self.cell_number_index_external

    # This backend does not need to generate Rosenbrock functions in MLIR. We may rely on the original codegen
    def get_rosenbrock_functions(self):
        f_func_name = "%s_rosenbrock_f" % self.model_name
        f_symbol, _ = self.create_rosenbrock_function(f_func_name, ["memref<8xi8>", "memref<8xi8>", "memref<8xi8>"])

        # Get variables names to properly store them back at the end and generate the function
        jacobian_name = "%s_rosenbrock_jacobian" % self.model_name
        jacobian_symbol, _ = self.create_rosenbrock_function(jacobian_name, ["memref<8xi8>",
                                                             "memref<8xi8>", "memref<8xi8>", "i32"])

        return "rbStepX_%s" % self.model_name, f_symbol, jacobian_symbol

    # We override the parent method in this backend because allocating memory
    # outside of the first BB of the GPU kernels can cause problems.
    def allocate_rosenbrock_data(self, rb):
        with ir.InsertionPoint.at_block_begin(self.inner_loop.body):
            # Add an alignment to the rosenbrock data allocation (f32 so it's an alignment of 4)
            return super(GpuCodegen, self).allocate_rosenbrock_data(rb, ir.IntegerAttr.parse("4"))

    def create_rosenbrock_stepx_call(self, rb_function, data_ptr, ion_private_ptr):
        params = [data_ptr, ion_private_ptr,
                  dialects.arith.TruncFOp(ir.F32Type.get(), self.get_function_argument("dt")),
                  dialects.arith.ConstantOp(ir.IntegerType.get_signless(32), len(self.rosenbrock))]
        return dialects.func.CallOp(rb_function, params)

    def create_rosenbrock_stepx(self, data_ptr, ion_private_ptr, stepx_name, f_func, jacobian_func):
        _, stepx = self.create_rosenbrock_function(stepx_name, ["memref<1xi8>",  "memref<1xi8>", "f32", "i32"])
        self.create_rosenbrock_stepx_call(stepx, data_ptr, ion_private_ptr)

    def allocate_lut(self, size):

        # Create new types that will be used for LUTs
        memref_lut_type = ir.ShapedType(ir.Type.parse("memref<%sxi8>" % (size), self.ctx))
        memref_unshaped = ir.ShapedType(ir.Type.parse("memref<?xi8>", self.ctx))
        memref_1xi8 = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        index_type = ir.IndexType.get()

        # Allocate memory so that the LUT can store data for each cell
        # And cast it to the right type (memref<1xi8>)
        with ir.InsertionPoint.at_block_begin(self.inner_loop.body):
            # The alignment is 8 because this is going to be used as a buffer of f64s
            lut_alloca = dialects.memref.AllocaOp(memref_lut_type, [], [], alignment=ir.IntegerAttr.parse("8"))
            lut_unshaped = dialects.memref.CastOp(memref_unshaped, lut_alloca)
            offset_index = dialects.arith.ConstantOp(index_type, 0)
            lut_1xi8 = dialects.memref.ViewOp(memref_1xi8, lut_unshaped, offset_index, [])

        return lut_1xi8.result

    def create_lut_call(self, var, table_name, is_state_variable, enum_dict, lut_size, var_name):

        def get_argument(name):
            if name not in self.params:
                assert name + " is not a valid parameter"
            index = self.params.index(name)
            return self.func_args[index]

        params = []
        # Get table argument
        arg = get_argument("table_" + table_name)
        params.append(arg)

        if is_state_variable:
            lut_base_address = "sv"
        else:
            lut_base_address = var_name

        # If lut_base_address is a parameter, this is a pointer type,
        # Otherwise, this is a value
        if lut_base_address in self.mlir_values_dict:
            arg = self.mlir_values_dict[lut_base_address]
            params.append(arg.result)
        else:
            _, value = self.get_mlir_value(var)
            params.append(value.result)

        params.append(self.cell_number)

        # Get the pointer to allocated struct.
        # It is sent as a parameter of the function with its name having the prefix "lut_alloc_"
        lut_ptr = self.allocate_lut(lut_size)
        params.append(lut_ptr)
        dialects.func.CallOp(self.function_dict[self.get_lut_function_name()], params)
        return lut_ptr

    def get_speculative_function_name(self, fp_type, is_store):
        if not is_store:
            return "cuda_speculative_load_%s" % fp_type
        else:
            return "cuda_speculative_store_%s" % fp_type

    # Create helper function to be called by the MLIR module
    def create_helper_functions(self):

        i1 = ir.IntegerType.get_signless(1)
        i8_ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        i32 = ir.IntegerType.get_signless(32)
        f32 = ir.F32Type.get()
        f64 = ir.F64Type.get()

        def create_speculative_load(fp_type):
            # Signature double speculative_load(bool, double*, double)
            function_name = self.get_speculative_function_name(fp_type, False)
            param_types = [i1, i8_ptr, fp_type]

            self.function_dict[function_name] = dialects.func.FuncOp(function_name, (param_types, [fp_type]),
                                                                     visibility="private")

        def create_speculative_store(fp_type):
            # Signature double cuda_speculative_store(int, double*, double)
            function_name = self.get_speculative_function_name(fp_type, True)
            param_types = [i1, i8_ptr, fp_type]

            self.function_dict[function_name] = dialects.func.FuncOp(function_name, (param_types, []),
                                                                     visibility="private")

        def create_lut_interpRow_function():
            # Signature: void LUT_data_t LUT_interpRow(char*, double, int, double*)
            function_name = self.get_lut_function_name()
            param_types = [i8_ptr, f64, i32, i8_ptr]

            self.function_dict[function_name] = dialects.func.FuncOp(function_name, (param_types, []),
                                                                     visibility="private")

        # Create speculative memory functions
        [ create_speculative_load(i) for i in [f32, f64]]
        [ create_speculative_store(i) for i in [f32, f64]]

        # Create LUT_interpRow_n_elements_vector_<num elemnts>xf64
        create_lut_interpRow_function()

    @staticmethod
    def create_memref(ctx, base_address, operand_type, offset, has_size_info):
        memref_type = ir.ShapedType(ir.Type.parse("memref<%s>" % operand_type, ctx))
        if type(offset) == int:
            offset_val = dialects.arith.ConstantOp(ir.IndexType.get(), offset).result
        elif not ir.IndexType.isinstance(offset.result.type):
            offset_val = dialects.arith.IndexCastOp(ir.IndexType.get(), offset).result
        else:
            offset_val = offset
        if has_size_info:
            size = [dialects.arith.ConstantOp(ir.IndexType.get(), 4).result]
        else:
            size = []
        return dialects.memref.ViewOp(memref_type, base_address, offset_val, size)

    @staticmethod
    def sizeof_fp_type(fp_type):
        # Returns the size in bytes of the given floating point type
        if fp_type == "f32":
            return 4
        elif fp_type == "f64":
            return 8
        else:
            raise ValueError(f'fp_type must be a floating point type (f32, f64), got "{fp_type}"')

    def get_data_layout_offset(self, type, offset):
        # Return the offset to use for loading a state variables when data layout
        # optimization is enabled
        var_name = [name for name, (var_type, off) in self.state_dict.items() if off == offset and var_type == type][0]
        return self.data_layout_state_dict[var_name][1] * self.sizeof_fp_type(type)

    def create_inside_offset(self, operand_type, offset):
        # When loading state variables with the optimized data layout, we
        # need to chose the right cell data with an offset inside of the
        # vector

        # Some type definitions
        index_type = ir.IndexType.get()

        # State variable offsets should only be python ints normally
        if type(offset) != int:
            t = type(offset)
            raise TypeError(f'the offset for state variables should be a python int, got "{t}"')

        offset_const = dialects.arith.ConstantOp(index_type, self.get_data_layout_offset(operand_type, offset))
        type_size = dialects.arith.ConstantOp(index_type, self.sizeof_fp_type(operand_type))
        inside_offset = dialects.arith.MulIOp(self.inside_index, type_size)
        final_offset =  dialects.arith.AddIOp(offset_const, inside_offset)

        return final_offset

    def create_pointer_cast_operation(self, value, offset_index):
        memref_type = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))

        # Calculate new offset result, source, byte_shift, sizes
        return dialects.memref.ViewOp(memref_type, value, offset_index, [])

    def create_load_call(self, base_address, operand_type, offset, is_sv=False):
        if self.enable_data_layout_opt and is_sv:
            offset = self.create_inside_offset(operand_type, offset)

        fp_operand = self.create_memref(self.ctx, base_address, "?x%s" % operand_type, offset, True)
        loaded_value = dialects.memref.LoadOp(fp_operand, [dialects.arith.ConstantOp(ir.IndexType.get(), 0).result])

        if operand_type == "f32":
            return dialects.arith.ExtFOp(ir.F64Type.get(), loaded_value)
        return loaded_value

    def create_contiguous_load(self, base_address, operand_type, offset, is_sv=False):
        return self.create_load_call(base_address, operand_type, offset, is_sv)

    def create_non_contiguous_load(self, base_address, operand_type, offset, size, is_sv=False):
        return self.create_load_call(base_address, operand_type, offset, is_sv)

    def create_splat_load(self, base_address, operand_type, offset, is_sv=False):
        return self.create_load_call(base_address, operand_type, offset, is_sv)

    def create_store_call(self, store_value, base_address, operand_type, offset, is_sv=False):
        if self.enable_data_layout_opt and is_sv:
            offset = self.create_inside_offset(operand_type, offset)

        fp_operand = self.create_memref(self.ctx, base_address, "?x%s" % operand_type, offset, True)
        if operand_type == "f32":
            store_value = dialects.arith.TruncFOp(ir.F32Type.get(), store_value)
        return dialects.memref.StoreOp(store_value, fp_operand,
                                       [dialects.arith.ConstantOp(ir.IndexType.get(), 0).result])

    def create_contiguous_store(self, base_address, store_value, operand_type, offset, is_sv=False):
        return self.create_store_call(store_value, base_address, operand_type, offset, is_sv)

    def create_non_contiguous_store(self, base_address, store_value, operand_type, offset, size, is_sv=False):
        return self.create_store_call(store_value, base_address, operand_type, offset, is_sv)

    def create_conditional_load(self, base_address, operand_type, mask, pass_thru):
        function_name = self.function_dict[self.get_speculative_function_name(operand_type, False)]
        params = [mask, base_address, pass_thru]
        return dialects.func.CallOp(function_name, params)

    def create_conditional_store(self, base_address, operand_type, mask, pass_thru):
        function_name = self.function_dict[self.get_speculative_function_name(operand_type, True)]
        params = [mask, base_address, pass_thru]
        return dialects.func.CallOp(function_name, params)

    def create_conditional_load_store(self, operand_type_str, base_address, offset, size, mask, pass_thru, is_store, offset_int=None, is_sv=False):
        # When using data layout optimization, we ignore the passed offset and
        # use the original offset and create the correct offset for handling
        # data layout optimization on GPU
        if self.enable_data_layout_opt and not (offset_int is None) and is_sv:
            offset = self.create_inside_offset(operand_type_str, offset_int)
        fp_operand = self.create_memref(self.ctx, base_address, "1xi8", offset, False)
        if operand_type_str == "f32":
            operand_type = ir.F32Type.get()
        else:
            operand_type = ir.F64Type.get()
        if not is_store:
            return self.create_conditional_load(fp_operand, operand_type, mask, pass_thru)
        else:
            return self.create_conditional_store(fp_operand, operand_type, mask, pass_thru)

    def create_execute_region(self, types):
        execute_region = dialects.scf.ExecuteRegionOp(types)
        # Create the BB for the condition
        bb = ir.Block.create_at_start(execute_region.regions[0])

        return execute_region, bb

    # The following two functions provide special treatment for markov_be methods in GPUs
    # that encapsulates all operations of the markov method into a region of the ExecuteRegion operation
    # Because AffineFor can only handle single regions, it is not possible to add control flow into its BB.
    # The solution, thus, is to add an ExecuteRegion that will contain the control flow of markov_be.
    # This prevents AffineFor from complaining about markov_be's control flow.
    def initialize_markov_be(self, markov_values, current_bb, get_variable_name):

        result_types = []

        def get_value_type(val):
            if type(val) != ir.Value:
                return val.result
            return val

        # Iterate through markov_be vals and add each markov_be value to the two lists
        for var in order(markov_values):
            # Retrieve it and make sure to use its Value
            val = self.mlir_values_dict[get_variable_name(var)]
            val = get_value_type(val)

            # Add them to the list
            result_types.append(val.type)

        execute_region, bb = self.create_execute_region(result_types)

        # Returns the ExecuteRegion and the Insertion Point to operations
        return execute_region, ir.InsertionPoint(bb)

    def finalize_markov_be(self, current_bb, markov_values, get_variable_name):

        # Track values to be used in the YieldOp operand
        # ExecuteRegionOp requires YieldOp to be the last instruction of the BB
        yield_list = []
        for var in order(markov_values):
            lhs_name = self.lhs_format_func(var)
            yield_list.append(self.mlir_values_dict[lhs_name])

        # Add an YieldOp
        dialects.scf.YieldOp(yield_list)

        # and update values in the dictionary
        i = 0
        for var in order(markov_values):
            lhs_name = self.lhs_format_func(var)

            # Cache MLIR values
            self.mlir_values_dict[lhs_name] = current_bb.results[i]
            i += 1

    @staticmethod
    def get_number_of_cuda_threads():
        return 64

    @staticmethod
    def create_yield_op(bb, args):
        with ir.InsertionPoint(bb):
            dialects.scf.YieldOp(args)

    @staticmethod
    def create_index_cast(type, value):
        return dialects.arith.IndexCastOp(type, value)

    @staticmethod
    def calculate_thread_id(outer_loop, inner_loop, num_threads_index):
        block_id = dialects.arith.MulIOp(outer_loop.induction_variable, num_threads_index)
        thread_id = dialects.arith.AddIOp(block_id, inner_loop.induction_variable)

        return thread_id
    
    def create_bb_with_yield(self, predecessor):
        dest = ir.Block.create_after(predecessor)

        self.create_yield_op(dest, [])
        return dest

    def create_new_loop(self, begin, end, step):
        loop = dialects.scf.ForOp(begin, end, step)

        # Finishes with an YieldOp
        self.create_yield_op(loop.body, loop.inner_iter_args)
        return loop

    def create_thread_id_checker(self, bb, thread_id_index, end_cell_index):

        with ir.InsertionPoint(bb):
            # Create FalseDest
            false_dest = self.create_bb_with_yield(bb)

            # And TrueDest
            true_dest = self.create_bb_with_yield(bb)

            # Conditional Branch to check thread_id
            # According to MLIR, 2 means STL (signed less than)
            predicate_type = ir.IntegerType.get_signless(1)
            predicate_attr = ir.IntegerAttr.get(ir.IntegerType.get_signless(64), 2)
            cmp_thread_id = dialects.arith.CmpIOp(predicate_attr, thread_id_index, end_cell_index)
            dialects.cf.CondBranchOp(cmp_thread_id, [], [], true_dest, false_dest)

            # Returns the bb where new instructions will be added
            return true_dest

    def create_data_layout_opt_indices(self, thread_id_index, end_cell_index):
        # Compute base indices to use when the data layout optimization is
        # enabled

        # Some needed types and constants
        index_type = ir.IndexType.get()
        num_elems = dialects.arith.ConstantOp(index_type, self.num_elements)

        # The cells are vectorized (cell_number_index is stepped by
        # num_elems and inside index is thread_is % num_elems)
        # When using data layout optimization on GPU, we need to get the base
        # address to the beginning of each group of cell data. So we use the
        # integer division to get that base address
        cell_number_index_trunc = dialects.arith.DivUIOp(thread_id_index, num_elems)
        cell_number_index = dialects.arith.MulIOp(cell_number_index_trunc, num_elems)
        # Index to add to SV offsets when using the data layout
        # optimization (basically, which cell inside the vector is this
        # thread computing ?)
        inside_index = dialects.arith.RemUIOp(thread_id_index, num_elems)

        return cell_number_index, inside_index

    def create_for_loop(self, function):

        index_type = ir.IndexType.get()

        # Get bounds and step. Start and End cell numbers are always arguments 0 and 1
        start_cell_index = self.create_index_cast(index_type, function.arguments[0])
        end_cell_index = self.create_index_cast(index_type, function.arguments[1])

        # Calculate end_cell_index = (end_cell_index + N-1)/N
        num_threads = self.get_number_of_cuda_threads()
        num_threads_index = dialects.arith.ConstantOp(index_type, num_threads)
        zero_index = dialects.arith.ConstantOp(index_type, 0)
        last_block_index = dialects.arith.AddIOp(end_cell_index, dialects.arith.ConstantOp(index_type, num_threads - 1))
        last_block_index = dialects.arith.DivUIOp(last_block_index, num_threads_index)
        step_vector_size_index = self.get_step_size(index_type)

        # Step == 1 for both outer loop, and inner loop

        # Outer loop goes from start_cell to last_block_index which is (end_cell_index + N-1)/N
        self.loop = self.outer_loop = self.create_new_loop(start_cell_index, last_block_index, step_vector_size_index)

        # Create Inner loop
        # Inner loop goes from 0 to 31
        with ir.InsertionPoint.at_block_terminator(self.outer_loop.body):
            self.inner_loop = self.create_new_loop(zero_index, num_threads_index, step_vector_size_index)

        # Calculates a new thread id in the form of: blockid.x * N + threadidx
        with ir.InsertionPoint.at_block_terminator(self.inner_loop.body):

            thread_id_index = self.calculate_thread_id(self.outer_loop, self.inner_loop, num_threads_index)
            self.cell_number = self.create_index_cast(ir.IntegerType.get_signless(32), thread_id_index)

            # Create a region to place the Condition that checks the thread id
            execute_region, bb = self.create_execute_region([])

            # Sets up BB where new instructions will be added
            self.bb = self.create_thread_id_checker(bb, thread_id_index, end_cell_index)
        with ir.InsertionPoint.at_block_terminator(self.bb):
            if self.enable_data_layout_opt:
                self.cell_number_index, self.inside_index = self.create_data_layout_opt_indices(thread_id_index, end_cell_index)
            else:
                self.cell_number_index = thread_id_index
            self.cell_number_index_external = thread_id_index
