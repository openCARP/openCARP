#!/usr/bin/env python3
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

## \file
#
# Useful miscellaneous python functions and classes.
#
#

import re
import sys

## Returns true if the object input is a list.
#
def isList(obj):
    if type(obj) == type([]):
        return 1
    else:
        return 0

## The output of this function is always a list.  
#
# This function was created to get around a strange 'feature' in the
# gnosis.xml.objectify parser.  If you have the following XML tree:
# 
# \verbatim   
#    <a>
#      <b />
#      <c />
#      <c />
#    </a>
# \endverbatim
#
# ...then a.b will be an object because there is only one b tag on
# object a.  However, a.c will be a list of two c objects.
#
# This poses a problem for the underlying code because you address
# items of a list with a different syntax than just the
# object. Therefore, I made this function to regularize everything
# into lists.
def makeList(obj):
    if not isList(obj):
        obj = [obj]
    return obj

## Prints an error message and exits.  Inspired by the perl function.
def die(text):
    print(text, file=sys.stderr)
    sys.exit(1)

## Joins together a list of strings into one big string, separated by
#  \a sep.  Inspired by the perl function.
def join(sep, list):
    return sep.join(list)

## Takes a list of anything, returns a list with duplicate entries removed.
#
def unique(list):
    temp={}
    for item in list:
        temp.setdefault(item, 1)

    return list(temp.keys())

## Returns a string \a s repeated \times times.  Inspired by the 'x'
#  operator in perl.
def repeat(s, times):
    return "".join([s for i in range(0, times)])

## Returns a tuple containing the given object repeated n times.
# Useful when using the '%' python operator, for when you have to
# output the same string more than once on the same line, ie.
#
# \verbatim
#
# print "%s = %s+1;" % repeat_tuple(var, 2)
#
# \endverbatim
def repeat_tuple(obj, n):
    return tuple([obj for i in range(0, n)])


## Any object that inherets this mixin can be used like a map.
class MapClass:
    def __init__(self):
        self._map={}
    def __contains__(self, key):
        return key in self._map.keys() 
    def __getitem__(self, key):
        if key not in self._map.keys() : raise KeyError(key)
        return self._map[key]
    def __setitem__(self, key, val):
        self._map[key] = val
    def __delitem__(self, key):
        del self._map[key]
    def __len__(self):
        return len(self._map)
    def keys(self):
        return list(self._map.keys())
    def values(self):
        return list(self._map.values())
    def setdefault(self, key, val=None):
        if key not in self:
            self[key]=val
        return self[key]
    def has_key(self, key):
        return key in self._map

## A replacement for the print function that indents code properly.
#  An instance of this class can be used like a function.  When called
#  like a function, it prints out to whatever file it was initialized
#  too.  However, the object also keeps a record of the current level
#  of indentation.  Calling code can increment or decrement the level
#  of indentation by calling inc() or dec() respectively.
class Indenter:
    ## Constructor.
    # \param file the file that all this class's output should go
    #   to. Defaults to stdout.
    #
    def __init__(self, file=sys.stdout):
        self.file = file
        self.current = 0
        self.indent_amount = 4

    ## When the class is called as a function, it effects a print statement.
    #  A trailing newline is always added.
    def __call__(self, text):
        lines = text.splitlines()
        for line in lines:
            print(repeat(" ", self.current) + line, file=self.file)

    ## Increments the level of indentation.
    def inc(self):
        self.current += self.indent_amount

    ## Decrements the level of indentation.
    def dec(self):
        self.current -= self.indent_amount

    ## Decrements the level of indentation and outputs a closing 'C'
    #  brace.  Used as an abbreviation when outputing C code.
    def close(self):
        self.dec()
        self("}")

## Funtions for manipulating file names ###########################

def basename(filename):
    '''
    P: filename
    filename we are parsing
    R: same name with the .better.mod stripped off
    '''
    match = re.compile(r"(\w+)\.model$").search(filename)

    if (match):
        base = match.group(1)
    else:
        raise Exception("filename must end in .model")

    return base

def default_header(filename):
    return basename(filename) + ".h"

def default_source(filename):
    return basename(filename) + ".cc"


class AttributeBase:
    def __getattr__(self, name):
        if name[0:2]!='__':
            return Void()
        else:
            raise AttributeError(name)

## Tests false when tested as a boolean.
class Void(AttributeBase):
    def __bool__(self):
        return False

## Tests true when tested as a boolean.
class Valid(AttributeBase):
    def __bool__(self):
        return True
