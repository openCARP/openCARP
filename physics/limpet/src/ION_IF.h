/**
 * \note All numerical IDs and flags are automatically generated
 *
 * Units for all IMPS must be
 *
 * vm        - mV (transmembrane potential)
 * [Ca]_i    - micromolar (intracellular calcium)
 * [Ca]_b    - micromolar (troponin bound calcium)
 * t         - ms (time)
 * Lambda    - normalized (stretch ratio)
 * delLambda - 1/ms (stretch rate)
 * Tension   - kPa (active stress)
 * [Na]_e    - millimolar (extracellular sodium)
 * [K]_e     - millimolar (extracellular potassium)
 * [Ca]_e    - micromolar (extracellular calcium)
 * Iion      - uA/cm^2 (ionic current)
 */
#ifndef IONIC_IF_H
#define IONIC_IF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdexcept>
#include "LUT.h"
#include "ODEint.h"
#include <math.h>
#include <assert.h>
#include <functional>
#ifdef _OPENMP
#include <omp.h>
#endif
#ifdef HAS_CUDA_MODEL
#include <cuda_runtime.h>
#endif
#ifdef HAS_ROCM_MODEL
#include <hip/hip_runtime.h>
#endif

#include "basics.h"
#include "target.h"

#ifdef I
#undef I
#endif

namespace limpet {

#ifndef M_PI  //C99 does not define M_PI
#define M_PI  3.14159265358979323846264338327
#endif

//Defines used in Slava's LIMPET model '
#define heav(x) ( (x)<0 ? 0 : 1)
#define sign(x) ( (x)<0 ? -1 : 1 )
#define square(x) ((x)*(x))
#define cube(x) ((x)*(x)*(x))

// MPI message tags used in the IMP world
#define MPI_RESTORE_IMP_POLL_BUFSIZE_TAG  10
#define MPI_RESTORE_IMP_SNDRCV_BUFFER_TAG 11
#define MPI_DUMP_IMP_POLL_BUFSIZE_TAG     20
#define MPI_DUMP_IMP_SNDRCV_BUFFER_TAG    21
#define MPI_DUMP_SVS_POLL_BUFSIZE_TAG     30
#define MPI_DUMP_SVS_SNDRCV_BUFFER_TAG    31

/** \brief time constant groups
 *
 *  The time constant group structure will hold all the information for
 *  integrating the svs using different time steps depending on the time
 *  constant that governs the process.
 */
struct tc_grp {
  float dt;
  int   skp;
  int   rat;
  int   update;
};

/** \brief time stepper
 *
 *  Holds all data to control time stepping
 */
struct ts {
  int     cnt;  /**< linear step counter                            */
  int     ng;   /**< number of time constant groups in this ION_IF  */
  tc_grp *tcg;  /**< array of structures holding the tcg data       */
};


/** @brief array of stat variable structures
 */
struct SV_TAB {
	int	           svSize;    //!< size of structure holding SV's for a node
	int	           numSeg;    //!< number of unknowns
  void  	      *y;         /**< pointer to the sv array. in practice this
                                 should be casted to the SV struct specific to
                                 a given model.*/
};

/** \brief definition of cell geometry

   Each ionic model is based on an underlying cell geometry.
   Several factors depend on the geometry like conversion factors
   how currents crossing the membrane translate into a change
   in ionic concentration. This structure is defined to provide a
   standardized mechanisms to define cell geometry and subdomain fractions
   to compute all dependent factors automatically. These factors can be
   accessed by plugins to derive factors for concentration updates.
 */
#define NDEF       0
struct cell_geom {
    float      SVratio=NDEF;      //!< single cell surface-to-volume ratio (per um)
    float      v_cell=NDEF;       //!< cell volume
    float      a_cap=NDEF;        //!< capacitive cell surface
    float      fr_myo=NDEF;       //!< volume of myoplasm
    float      sl_i2c=NDEF;       //!< convert sl-currents in uA/cm^2 to mM/L without valence
};

}
#include "ion_type.h"
namespace limpet {

struct IMPinfo {
  char    *name;          //!< IMP name
  int      sz;            //!< storage required
  int      nplug;         //!< number of plugins
  IMPinfo *plug;          //!< plugins
  bool     compatible;    //!< does IM match stored IM
  int      map;           //!< which plugin does this IMO match
  int      offset;        //!< offset into node data
};

/**
 * @brief Represents the ionic model and plug-in (IMP) data structure.
 */
class IonIfBase {
private:
  // Information about the model type.
  const IonType& _type;
  int _num_node; //!< number of nodes (cells) handled by the IMP
  IonIfBase* _parent; //!< parent IMP
  std::vector<IonIfBase*> _plugins; //!< list of plugins
  uint32_t _reqdat; /**< bitmap indicating which external variables are used
                         (but not modified) by the IMP */
  uint32_t _moddat; /**< bitmap indicating which external variables ar
                         modified by the IMP */
public:
  int         miifIdx;              //!< imp index within miif
  Target _target;                   //!< execution target for this IMP
protected:
  ts          _tstp;                //!< control time stepping
private:
  cell_geom   _cgeom;               //!< cell geometry data
  float       dt;                   //!< basic integration time step
  std::vector<LUT> _tables;         //!< lookup table list
  LUT * _tables_d = nullptr;        //!< device side lookup table array
  size_t _n_tables_d = 0;           //!< number of lookup tables in the device side array
  GlobalData_t **ldata = nullptr; /**!< pointer to external variables (Vm, Iion,
                                        etc.) that is local to this IMP */

public:
  /**
   * @brief Constructor for IonIfBase.
   *
   * @param type ionic model type
   * @param target this IMP will run on
   * @param num_node number of cells handled by this IMP
   * @param plugins array of plugin types (plugins attached to this IMP)
   */
  IonIfBase(const IonType& type, Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins);

  /**
   * @brief Virtual destructor declaration
   */
  virtual ~IonIfBase();

  /**
   * @brief Gets this IMP's model type.
   *
   * @returns a reference to the IMP type (set of generated functions from
   * model code).
   */
  const IonType& get_type() const;

  /**
   * @brief Gets the number of nodes handled by this IMP.
   *
   * @returns the number of nodes
   */
  int get_num_node() const;

  /**
   * @brief Gets the number of threads used for running this IMP.
   *
   * @returns the number of threads running this IMP (dependant on target)
   */
  std::size_t get_num_threads() const;

  /**
   * @brief Gets the parent IMP.
   *
   * @returns a pointer to the parent of this IMP
   */
  IonIfBase* parent() const;

  // VERY UNRECOMMENDED TO CALL, THIS IS ONLY USED FOR AN UGLY HACK.
  void set_parent(IonIfBase* parent);

  /**
   * @brief Returns a vector containing the plugins of this IMP.
   *
   * @returns a vector filled with plugins attached to this model
   */
  std::vector<IonIfBase*>& plugins();

  /**
   * @brief Gets the data flags for this IMP's required data.
   *
   * @returns a bitmap containing the data flags
   */
  uint32_t get_reqdat() const;

  /**
   * @brief Gets the data flags for this IMP's modified data.
   *
   * @returns a bitmap containing the data flags
   */
  uint32_t get_moddat() const;

  /**
   * @brief Set the data flag for this IMP's modified data.
   *
   * @param data bitmap containing the data flags
   */
  void set_moddat(uint32_t data);

  /**
   * @brief Gets the cell geometry data.
   *
   * @return the cell geometry data
   */
#if defined HAS_CUDA_MODEL || defined HAS_ROCM_MODEL
  __device__ __host__
#endif
  cell_geom& cgeom() {
    return this->_cgeom;
  }

  /**
   * @brief Gets the basic integration time step.
   *
   * @return the basic integration time step
   */
  float get_dt() const;

  /**
   * @brief Sets the basic integration time step.
   *
   * @param dt the basic integration time step
   */
  void set_dt(float dt);


  /**
   * @brief Gets the time stepper.
   *
   * @returns the time stepper.
   */
  ts& get_tstp();

  /**
   * @brief Gets the raw address of the state variables for this IMP.
   *
   * TODO: This is needed for now as many functions (write and read functions)
   * and plugins (using the get_sv_offset function) still use the raw address
   * of the SV table. This can be changed by rewriting the code that calls
   * this function and by writing better tools that get_sv_offset for parent
   * models.
   */
  virtual void *get_sv_address() = 0;

  /**
   * @brief Gets the size of the structure this IMP uses for state variables.
   *
   * TODO: This is also needed for calling code to be able to use
   * IonIfBase::get_sv_address as it returns a void pointer.
   */
  virtual std::size_t get_sv_size() const = 0;

  /**
   * @brief Gets the array of state variables.
   *
   * @returns a reference to the state variable array structure.
   */

//#if defined HAS_CUDA_MODEL || defined HAS_ROCM_MODEL
//  __device__ __host__
//#endif
//  SV_TAB& sv_tab() {
//    return this->_sv_tab;
//  }

  /**
   * @brief Gets the lookup tables.
   *
   * @returns a reference to the vector of lookup tables (LUT) used by this
   * IMP.
  */
  std::vector<LUT>& tables();

  /**
   * @brief Gets an array of LUTs
   *
   * @returns an array to LUT structures. Those LUTs are
   * allocated on a device (GPU) and mirror the data in the vector returned by
   * the IonIf::tables getter method.
   */
#if defined HAS_CUDA_MODEL || defined HAS_ROCM_MODEL
  __device__ __host__
#endif
  LUT *tables_d() const {
    return this->_tables_d;
  }

  /**
   * @brief Gets the size of the array returned by IonIf::tables_d.
   *
   * @returns the number of LUTs stored in the device-side LUT array (this
   * number should be the same as IonIf::tables().size().
   */
  size_t get_n_tables_d() const;

#if defined HAS_GPU_MODEL
  __device__ __host__
#endif
  Target get_target() const {
    return this->_target;
  };

  virtual void set_target(Target target);

  /**
   * @brief Initializes user modifiable parameters
   * with default values defined in the respective ionic models.
   */
  void initialize_params();

  /**
   * @brief Initializes lookup table and state variable tables.
   *
   * @param dt     time step
   * @param impdat data
   *
   * \pre numNode is set
   *
   * \post lookup tables are allocated and filled <br>
   *       state variables are set to inital conditions
   */
  virtual void initialize(double dt, GlobalData_t **impdat);

  /**
   * @brief Perform ionic model computation for 1 time step.
   *
   * Calls the compute functon if this IonIf's type with this IonIf's target.
   *
   * @note See limpet::IonType::compute
  */
  void compute(int start, int end, GlobalData_t** data);

  /**
   * @brief Appends the state variables to a buffer.
   *
   *  \param buf      the buffer
   *  \param n[inout] current buffer size
   *  \param l        list of local nodes to write
   *
   *  \note \p buf is reallocated to hold new data
   *
   *  \return pointer to the increased buffer
   */
  char* fill_buf(char *buf, int* n, opencarp::Salt_list *l) const;

  /**
   * @brief Reads in the state variables for an IMP.
   *
   * The data is verified to make sure it belongs to the expected type of IMP.
   *
   * \param in      output file
   * \param n       number of nodes
   * \param pos     partitioned node numbers
   * \param mask    global region mask ordered canonically
   * \param offset  file offset to SV info canonically ordered
   * \param impinfo IMP information
   *
   * \pre  the SV tables are allocated
   * \post the SV tables have new values
   */
  int restore(opencarp::FILE_SPEC in, int n, const int *pos, IIF_Mask_t *mask,
              size_t *offset, IMPinfo *impinfo, const int* loc2canon);

  /**
   * @brief Dumps array of LUTs to file.
   *
   * \param zipped    flag to toggle zipped output
   *
   * \param return number of LUTs dumped
   */
  int dump_luts(bool zipped);

  /**
   * @brief Destroys array of LUTs.
   */
  void destroy_luts();

  /**
   * @brief Tunes specific IMP parameters from files
   *
   * For each IMP, a comma separated list of expressions is specified.
   * Each expression is of the form <br>
   * parameter[+|-|=|/|*][-]###[.[###][e|E[-|+]###][\%] <br>
   * which specifies a float optionally preceded by \b *
   * or \b +  or \b - or \b / or \b =
   * and optionally followed by a \b \% (float is expressed as a percent
   * of the default value). The meanings of the flags are
   * <dl>
   * <dt>=</dt><dd> assign this value to the parameter </dd>
   * <dt>*</dt><dd> multiply the default parameter value by this </dd>
   * <dt>/</dt><dd> divide the default parameter value by this </dd>
   * <dt>+</dt><dd> add this to the default value </dd>
   * <dt>-</dt><dd> subtract this from the default value</dd>
   * <dt>\%</dt><dd> treat the float as this percentage of the default</dd>
   * </dl>
   *
   * Plugins and their respective parameters must be specified in the same order
   *
   * \param im_par   comma separated list of IM specific parameters
   * \param plugs    plugins specified in colon separated list
   * \param plug_par colon separated list of comma separated lists
   *                 specifiying parameters for each plugin
   */
  void tune(const char *im_par, const char *plugs, const char *plug_par);

  /**
   * @brief Reads state variable values for one cell from a file.
   *
   * @retval 0 good
   * @retval 1 temporary table allocated
   * @retval >1 error
   */
  int read_svs(FILE* file);

  int write_svs(FILE* file, int node);

  /**
   * @brief Copies the state variables of an IMP.
   *
   * \param other where the values are copied from
   * \param alloc true to allocate memory and copy IIF
   *
   * \post the copy is only as deep as required for independent
   *       functioning of the copy and the original. Static data are shared.
   */
  virtual void copy_SVs_from(IonIfBase& other, bool alloc) = 0;

  /**
   * @brief Copies the plugins of an IMP.
   *
   * A new IonIf object is allocated for each plugin and the state variables
   * are copies with a call to IonIf::copy_SVs_from.
   *
   * @param other IMP to copy the plugins from
   */
  void copy_plugins_from(IonIfBase& other);

  /**
   * @brief Executes the @p consumer functions on this IMP and each of its
   * plugins.
   *
   * @param consumer functions to execute on each IMP
   */
  void for_each(const std::function<void(IonIfBase&)>& consumer);
};

/**
 * @brief Child class of IonIfBase specialized for each ionic model type.
 *
 * The template parameter T should be an IonType class and should define
 * some type aliases:
 * - params_type: the type of the parameters structure used by the model
 * - state_type: the type of the state variables structure used by the model
 * - private_type: the type of the private structure used by the model (or void
 *   if none if used)
 */
template<typename T>
class IonIf : public IonIfBase {
  /**
   * Structs used for meta-programming by removing calls involving the
   * ion_private type when models don't use it.
   *
   * These are used for using overloads that manipulate the private data
   * structures
   */
    struct has_rosenbrock_type {}; //!< the IMP uses private data
    struct has_rosenbrock_vector_type {}; //!< the IMP uses vectorized private data
    struct no_rosenbrock_type {}; //!< the IMP doesn't use private data
  public:
    /**
     * @brief Utility class for handling arrays of data used by IMPs.
     *
     * @note For now this class can only be moved to avoid copying the owned
     * data. This could probably be changed later by using this class only
     * with shared pointers (_data cannot be a shared pointer as it
     * could be a GPU pointer)
     *
     * @note If this class is used with void as a template parameter, most functions
     * will not do anything
     */
    template<typename S>
    class LimpetArray {
      static constexpr bool is_void = std::is_void<S>::value; /**!< is S of type
                                                               * void ?
                                                               */
      S *_data; //!< state variable array
      std::size_t _size; //<! size of the SV array
      bool _allocated = false; //!< was data allocated ?
      Target _target; //!< target on which data is allocated
      public:

      /**
       * @brief Default constructor with no allocated data.
       */
      LimpetArray() : _size(0), _target(Target::UNKNOWN) {
      }

      /**
       * @brief Constructs a LimpetArray.
       *
       * @param target target on which to allocate the array
       * @param size size of the array
       */
      LimpetArray(Target target, std::size_t size) : _size(size),
      _target(target) {
        this->allocate(target, size);
      }

      /**
       * @brief Destroy a limpet array.
       */
      ~LimpetArray() {
        // Check if memory was allocated, if it's the case, deallocate on the
        // corresponding target
        if (!is_void && _allocated) {
          deallocate_on_target<S>(this->_target, this->_data);
        }
      }

      /**
       * @brief Allocate the array on the given target.
       *
       * @param target target to allocate on
       * @param size size of the array
       */
      void allocate(Target target, std::size_t size) {
        this->_size = size;
        this->_target = target;
        if (!is_void){
          bool do_zero = false;
          this->_data = allocate_on_target<S>(this->_target, this->_size, do_zero);
          this->_allocated = true;
        }
      }

      /**
       * @brief Get the size of a single element (size of type S)
       *
       * @retval sizeof(S) if S is non-void
       * @retval 0 is S is void
       */
      template<typename Type = S>
      typename std::enable_if<!std::is_void<Type>::value, std::size_t>::type get_element_size() const {
        return sizeof(Type);
      }

      template<typename Type = S>
      typename std::enable_if<std::is_void<Type>::value, std::size_t>::type get_element_size() const {
        return 0;
      }

      /**
       * @brief Gets a pointer to the underlying data.
       *
       * @returns a pointer of type S* pointing to the allocated data (if any)
       */
#ifdef HAS_GPU_MODEL
  __device__ __host__
#endif
      S *data() const {
        static_assert(!is_void, "The LimpetArray class can't be used with type 'void'");
        return this->_data;
      }

      /**
       * @brief Gets the sizee of the array.
       *
       * @returns the size of the array
       */
#ifdef HAS_GPU_MODEL
  __device__ __host__
#endif
      std::size_t size() const {
        static_assert(!is_void, "The LimpetArray class can't be used with type 'void'");
        return this->_size;
      }

      /**
       * @brief Returns whether data has been allocated for this LimpetArray.
       *
       * @retval true if data is allocated
       * @retval false if data isn't allocated
       */
      bool is_allocated() const {
        return this->_allocated;
      }

      /**
       * @brief Move assignement operator.
       *
       * This is a move assignement, ensuring only one reference to the data
       * exists (can probably be improved)
       */
      LimpetArray& operator=(LimpetArray&& other) {
        if (this == &other) {
          return *this;
        }
        this->_target = other._target;
        this->_data = other._data;
        this->_size = other._size;
        if (other._allocated) {
          this->_allocated = true;
          other._allocated = false;
        }
        return *this;
      }
    };
  private:
    using SvTab = LimpetArray<typename T::state_type>; //!< type alias for a LimpetArray of state variables
    using PrivateTab = LimpetArray<typename T::private_type>; //!< type alias for a LimpetArray of private structures (persistent private data workspace
    using PrivateVectorTab = LimpetArray<typename T::private_type_vector>; //!< type alias for a LimpetArray of vectorized private structures (for MLIR CPU)

    /**
     * which type of private data is used by this IMP. The type can be one of
     * has_rosenbrock_vector_type, has_rosenbrock_type, or no_rosenbrock_type.
     *
     * @note used to select the correct overloaded function manipulating private data
     */
    using rosenbrock_usage = typename std::conditional<std::is_void<typename T::private_type>::value, no_rosenbrock_type, typename std::conditional<std::is_void<typename T::private_type_vector>::value, has_rosenbrock_type, has_rosenbrock_vector_type>::type>::type;

    // These fields are C-style arrays because they need to be accessible from device code
    SvTab _sv_tabs[Target::N_TARGETS]; //!< array of SvTab (one per possible target)
    typename T::params_type* _params[Target::N_TARGETS] = {nullptr}; //!< model specific parameters (one per possible target)
    PrivateTab _ion_private[Target::N_TARGETS]; //!< array of PrivateTab (one per possible target)
    PrivateVectorTab _ion_private_vector; //! PrivateVectorTab data
    size_t private_sz; //!< size of the private data structure
  public:

    /**
     * @brief Constructs an IonIf object.
     *
     * @note see IonIfBase::IonIfBase for explanatiosn on the parameters
     *
     * This constructor allocated all necessary memory for the IMP
     */
    IonIf(const IonType &type, Target target, int num_node, const
        std::vector<std::reference_wrapper<IonType>>& plugins) : IonIfBase(type,
          target, num_node, plugins) {
          this->allocate_model_data();
        }

    /**
     * @brief Destroy the IMP.
     *
     * @note This function should deallocate all memory used by the IMP
     */
    ~IonIf() {
      for (int i = 0; i < Target::N_TARGETS; ++i) {
        if (this->_params[i] != nullptr) {
          deallocate_on_target((Target) i, this->_params[i]);
        }
      }
    }

    /**
     * @brief Gets a pointer to the parameter structure for the current target.
     *
     * @returns a pointer to the parameters structure
     */
#ifdef HAS_GPU_MODEL
    __device__ __host__
#endif
      typename T::params_type *params() const {
        return this->_params[this->get_target()];
      }

    /**
     * @brief Gets the SV LimpetArray for the current target.
     *
     * @returns a reference to the SV array
     */
#ifdef HAS_GPU_MODEL
    __device__ __host__
#endif
      SvTab& sv_tab() {
        return this->_sv_tabs[this->get_target()];
      }

    /**
     * @brief Gets the ion private LimpetArray for the current target.
     *
     * @returns a reference to the ion private array
     */

#ifdef HAS_GPU_MODEL
    __device__ __host__
#endif
    PrivateTab& ion_private() {
      return this->_ion_private[this->get_target()];
    }

    PrivateVectorTab& ion_private_vector() {
      if (this->get_target() != Target::MLIR_CPU) {
        throw std::logic_error("the vectorized ion private structure can only be used with the MLIR_CPU (vectorized CPU) target");
      }
      return this->_ion_private_vector;
    }

    /**
     * @brief Gets the raw address of the SV array for the current target.
     *
     * @returns a raw pointer to the SV array
     */
    void * get_sv_address() override {
      return (void *) this->_sv_tabs[this->get_target()].data();
    }

    /**
     * @brief Gets the size of a SV structure.
     *
     * @returns the size of the state variable structure
     */
    std::size_t get_sv_size() const override {
      return sizeof(typename T::state_type);
    }

    /**
     * @brief Set a new execution target for this IMP.
     *
     * @param target the new target to set
     *
     * A call to this function will trigger allocation of memory on the new
     * target and a copy of memory from the old target to the new target.
     */
    void set_target(Target target) override {
      Target old_target = this->get_target();
      IonIfBase::set_target(target);
      this->allocate_model_data();
      memcpy(this->sv_tab().data(), this->_sv_tabs[old_target].data(), this->sv_tab().size());
      // TODO: Copy ion private data for Rosenbrock, making transformations as necessary between
      // vectorized and non-vectorized structures
      //    memcpy(this->ion_private().data(), this->_ion_private[old_target].data(), this->ion_private().size());
      memcpy(this->params(), this->_params[old_target], sizeof(typename T::params_type));
    }

    /**
     * @brief Allocate memory for the IMP data for the current target.
     */
    void allocate_model_data() {
      std::size_t vec_size = this->get_type().dlo_vector_size();
      if (!this->_sv_tabs[this->get_target()].is_allocated())
        this->_sv_tabs[this->get_target()] =
          SvTab(this->get_target(), (this->get_num_node() + vec_size - 1) / vec_size);
      if (!this->_ion_private[this->get_target()].is_allocated())
        this->_ion_private[this->get_target()] = PrivateTab(this->get_target(), this->get_num_threads());
      // The private vector structure is always made for the vectorized CPU target
      if (!this->_ion_private_vector.is_allocated() && this->get_target() == Target::MLIR_CPU)
        this->_ion_private_vector = PrivateVectorTab(this->get_target(), this->get_num_threads());
      if (this->_params[this->get_target()] == nullptr)
        this->_params[this->get_target()] = allocate_on_target<typename T::params_type>(this->get_target(), 1, true);
    }

    /**
     * @brief Override of the initialization function to add the initialization
     * of the private structures.
     *
     * @param dt time step
     * @param impdata data
     *
     * @note see IonIfBase::initialize
     */
    void initialize(double dt, GlobalData_t **impdat) override {
      IonIfBase::initialize(dt, impdat);
      this->init_ion_private(rosenbrock_usage {});
    }

    /**
     * @brief Copy state and private variables from another IMP.
     *
     * @pre other is of the same type as @p this (IonIf<T>)
     * @pre @p this handles the same amount of cells as @p other_base
     * (this->get_num_nodes() == other.get_num_nodes())
     *
     * @param other_base IMP to copy the values from (passed as IonIfBase)
     * @param alloc whether to allocate new storage for the copied values
     *
     * @warning alloc is deprecated and is not used since the IonIf constructor
     * should always allocate memory, and if the IMPs don't handle the same amount
     * of cells (same size of SV table), the function should throw an exception.
     */
    void copy_SVs_from(IonIfBase& other_base, bool alloc) override {
      IonIf<T>& other = static_cast<IonIf<T>&>(other_base);
      int tcg_size = other.get_tstp().ng * sizeof(tc_grp);

      if (this->get_num_node() != other.get_num_node()) {
        throw std::logic_error("cannot copy SVs if both IMPs don't handle the same amount of cells");
      }

      memcpy(this->sv_tab().data(), other.sv_tab().data(), sizeof(typename T::state_type) * other.sv_tab().size());
      // Only copy memory when the model actually defines a private type
      this->copy_ion_private(other, rosenbrock_usage {});
      memcpy(this->get_tstp().tcg, other.get_tstp().tcg, tcg_size);
    }

    /**
     * @brief Copy the ion private array from @p other.
     *
     * @param other IMP to copy the array from
     *
     * @note this function is an overload to use for models using the vectorized private
     * data workspace
     */
    void copy_ion_private(IonIf<T>& other, has_rosenbrock_vector_type) {
      if (this->get_target() == Target::MLIR_CPU) {
        memcpy(this->ion_private_vector().data(), other.ion_private_vector().data(), sizeof(typename T::private_type_vector) * other.ion_private_vector().size());
      }
      else {
        memcpy(this->ion_private().data(), other.ion_private().data(), sizeof(typename T::private_type) * other.ion_private().size());
      }
    }

    /**
     * @brief Copy the ion private array from @p other.
     *
     * @param other IMP to copy the array from
     *
     * @note this function is an overload to use for models using the private
     * data workspace
     */
    void copy_ion_private(IonIf<T>& other, has_rosenbrock_type) {
      memcpy(this->ion_private().data(), other.ion_private().data(), sizeof(typename T::private_type) * other.ion_private().size());
    }

    /**
     * @brief This function does nothing (overload of copy_ion_private(IonIf<T>&, has_rosenbrock_type)).
     *
     * This is used to avoid trying to copy data on models that don't use private data
     */
    void copy_ion_private(IonIf<T>& other, no_rosenbrock_type) { }

    /**
     * @brief Initialize private data.
     *
     * @note Overload for the vectorized private structures
     */
    void init_ion_private(has_rosenbrock_vector_type) {
      // We need to fill the vector version of the private data for vectorized CPU
      if (this->get_target() == Target::MLIR_CPU) {
        for (std::size_t i = 0; i < this->ion_private_vector().size(); ++i) {
          this->ion_private_vector().data()[i].node_number = 0;
          this->ion_private_vector().data()[i].IF = this;
        }
      }
      else {
        for (std::size_t i = 0; i < this->ion_private().size(); ++i) {
          this->ion_private().data()[i].node_number = 0;
          this->ion_private().data()[i].IF = this;
        }
      }
    }

    /**
     * @brief Initialize private data.
     *
     * @note Overload for the private structures
     */
    void init_ion_private(has_rosenbrock_type) {
        for (std::size_t i = 0; i < this->ion_private().size(); ++i) {
          this->ion_private().data()[i].node_number = 0;
          this->ion_private().data()[i].IF = this;
        }
    }

    /**
     * @brief Doesn't do anything
     *
     * @note Overload for models without the need for a private structure
     */

    void init_ion_private(no_rosenbrock_type) { }
};

/*
   function prototypes
 */

void     initialize_ts(ts *tstp, int ng, int *skp, double dt);
void     update_ts(ts *tstp);
void     SV_alloc( SV_TAB *psv, int numSeg, int struct_size );
void     SV_free( SV_TAB *psv );
void     free_sv_table( void * );
void     print_IMPs(void);
bool     flag_set( const char *flags, const char *target );
void     print_models(bool );
float    modify_param( float a, char *expr );
int      process_param_mod( char *pstr, char *par, char *mod );
char*    get_typename(int type);
bool     verify_flags( const char *flags, const char* given );
int      load_ionic_module(const char*);
char    *get_next_list( char *lst, char delimiter );

// Functions for getting SVs in sv_init.c
SVputfcn getPutSV( SVgetfcn );

#include "ION_IF_sv.h"

#define CHANGE_PARAM( T, P, V, F )  do { \
	((T##_Params *)P)->V = modify_param( ((T##_Params *)P)->V, F ); \
	log_msg( _nc_logf,0, 0, "	%-20s modifier: %-15s value: %g",\
	#V,F,(float)((T##_Params *)P)->V);} while (0)

}  // namespace limpet

#endif
