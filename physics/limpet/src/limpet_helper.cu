// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "LUT.h"
#include "basics.h"
#include "MULTI_ION_IF.h"

#include <string.h>
#include <assert.h>
#include <cmath>

namespace limpet {

using ::opencarp::dupstr;
using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;
using ::opencarp::is_big_endian;
using ::opencarp::log_msg;


#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

#if defined __CUDA__ || defined __HIP__

/** compute normalized disretization error
 *
 * This is defined as the distance of the lookup value from the closest
 * lower value used by the table lookup. It is computed as
 * \f$ e = (X-x[i])/\Delta x\f$ where X is the value used in the lookup up, x[i] is
 * the closest table value and \f$\Delta x\f$ is the table resolution
 *
 *  \param t   table
 *  \param idx closest lower index
 *  \param x   value to lookup
 *
 *  \return the normalized error
 */
__device__
inline LUT_data_t LUT_derror( LUT *t, int idx, GlobalData_t x )
{
  return  (x-idx*t->res)/t->res;
}

#ifndef IMP_FAST
__device__
int LUT_index( LUT *tab, GlobalData_t val, int locind )
{
  int indx =  (int)(tab->step*val);

  if (indx < tab->mn_ind) {
    indx = tab->mn_ind;
  } else if (indx > tab->mx_ind) {
    indx = tab->mx_ind;
  } else {
    return indx;
  }
  return indx;
}
#endif

__device__
int LUT_out_of_bounds(LUT *tab, GlobalData_t val) {
  return (val < tab->mn || val > tab->mx) ;
}

/** interpolate in a lookup table
 *
 *  \param t the table
 *  \param i the row in the table
 *  \param j the column in the table
 *  \param x normalized discretizaton error
 *
 *  \return a linear interpolation of table values
 */
__device__
inline LUT_data_t LUT_interp( LUT *t, int i, int j, GlobalData_t x )
{
  return (1.-x)*t->tab[i][j] + x*t->tab[i+1][j];
}

/** interpolate a row of a lookup table
 *  
 * The given name is defined with the prefix '_mlir_ciface_' provided
 * the 'GpuKernelOutlining' MLIR transformation pass
 * 
 *  \param tab the table
 *  \param val current value for which we are looking up the dependent values
 *  \param i   local index of the vertex for which we are doing the lookup
 *  \param row interpfolated values
 *
 *  \return  discretization error
 *  \pre     row is allocated
 *  \note    This is now threadsafe
 */
__host__ __device__
LUT_data_t LUT_interpRow(LUT *const tab, GlobalData_t val,int i,LUT_data_t* row)
{
  int    idx  = LUT_index(tab, val, i);
  GlobalData_t derr = LUT_derror(tab, idx, val);
  if (LUT_out_of_bounds(tab, val)) {
    for (int j=0;j<tab->cols;j++)
      row[j] = tab->tab[idx][j];
  } else {
    for (int j=0;j<tab->cols;j++)
      row[j] = LUT_interp(tab, idx, j, derr );
  }
  return 0;
}

__device__
float cuda_speculative_load_f32(bool condition, float *ptr, float fall_through) {
  if (condition)
    return *ptr;
  else
    return fall_through;
}

__device__
void cuda_speculative_store_f32(bool condition, float *ptr, float value) {
  if (condition)
    *ptr = value;
}

__device__
double cuda_speculative_load_f64(bool condition, double *ptr, double fall_through) {
  if (condition)
    return *ptr;
  else
    return fall_through;
}

__device__
void cuda_speculative_store_f64(bool condition, double *ptr, double value) {
  if (condition)
    *ptr = value;
}

#endif

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

