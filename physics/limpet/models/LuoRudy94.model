group { 
  #Authors ::= Luo CH, Rudy Y
  #Year ::= 1994
  #Title ::= A dynamic model of the cardiac ventricular action potential. I. Simulations of ionic currents and concentration changes
  #Journal ::= Circ Res., 74(6),1071-96
  #DOI ::= 10.1161/01.res.74.6.1071
} .reference();

V; .external(Vm); .nodal();
Iion; .external(); .nodal();

V; .lookup(-800, 800, 0.01);
VEK1 = V-EK1; .lookup(-800, 800, 0.01);
Cai; .lookup(1e-4, 10, 1e-4);
catotal; .lookup(1e-4, 1, 1e-4);

///////////////////////////////////

group {
  ICaIold = ICai;
  fdiff_ICaIold = fdiff_ICai;
}.store();

ICaIold_init = ICai;
fdiff_ICaIold_init = fdiff_ICai;


fdiff_ICai = (ICai-ICaIold)/dt;
fdiff_V = (V-V_old)/dt;

///////////////////////////////

V_init =  -88.654973;

Ko = 4.5;
Nao = 140;
Cao = 1.8;

/* Cell Geometry */
len= 0.01; .units(cm);  // Length of the cell (cm)
rad= 0.0011; .units(cm);  // Radius of the cell (cm)
pi= 3.141592;  // Pi
Volcell= (1000*pi*rad*rad*len); .units(uL);       // Cell volume 3.801e-5 [uL]
ageo= (2*pi*rad*rad+2*pi*rad*len); .units(cm^2); // Geometric membrane area 7.671e-5 [cm^2]
acap= (ageo*2); .units(cm^2);                    // Capacitive membrane area 1.534e-4 [cm^2]

/* percentage of total cell volume */
fr_NSR =    0.0552;              // fraction of nsr of total cell volume
fr_JSR =    0.0048;              // fraction of jsr of total cell volume
fr_MYO =    0.68;                // fraction of myoplasma of total cell volume
fr_MITO =   0.26;                // fraction of mitochondria of total cell volume
fr_SR =     0.06;                // fraction of SR of total cell volume
fr_CLEFT = (0.12/0.88);          // fraction of cleft space


VolNSR =   5.52;                // percentage of nsr of total cell volume
VolJSR =   0.48;                // percentage of jsr of total cell volume
VolMYO =  68.;                  // myoplasma % of total cell volume
VolMITO=   2.6;                 // mitochondrial % of total cell volume
VolSR  =   6.0;                 // percentage of SR of total cell volume

vmyo =     (Volcell*fr_MYO);       // Myoplasm volume (uL)
vmito =    (Volcell*fr_MITO);      // Mitochondria volume (uL)
vsr  =     (Volcell*fr_SR);        // SR volume (uL)
vnsr =     (Volcell*fr_NSR);       // NSR volume (uL)
vjsr =     (Volcell*fr_JSR);       // JSR volume (uL)
vcleft=    (Volcell*fr_CLEFT);     // Cleft volume (uL)

R = 8314.; .units(J/kmol*K); // Universal Gas Constant
F = 96485.; .units(C/mol);   // Faraday's Constant
T = 310.; .units(K);         // Temperature
F_RT = F/(R*T);

ENa  = log(Nao/Nai)/F_RT;
ECa = log( Cao_uM/Cai )/F_RT/2.;
EK1  = (1/F_RT)*log(Ko/Ki);
prnak = 0.01833;
EKs  = (1/F_RT)*log((Ko+prnak*Nao)/(Ki+prnak*Nai));
Ekdv = EK1;
Nao3 = Nao*Nao*Nao;


// L-Type Calcium Channel
group {
  PNa = 0.000000675; //!< Membrane permeability to Na
  PK = 0.000000193; //!< Membrane permeability to K
  PCa = 0.00054; //!< Membrane permeability to Ca
} .units(cm/s);
zNa = 1;
zK  = 1;
zCa = 2;
KmCa = 0.0006; .units(mM);
KmCa_uM  = KmCa *1000.;

d_inf = 1/(1 + exp(-(V+10)/6.24)) ;
tau_d = (( V != -10 )
         ? d_inf * (1 - exp(-(V+10)/6.24))/(0.035*(V+10))
         : d_inf/0.035/6.24
         );
f_inf = 1/(1+exp((V+32.)/8.))+0.6/(1+exp((50-V)/20));
tau_f = 1/(0.0197*exp(-(0.0337*(V+10))*(0.0337*(V+10)))+0.02);
fCa  = KmCa_uM/(KmCa_uM+Cai);
k_CaL = d*f*fCa;
eterm = exp(V*F_RT);
em1term = expm1(V*F_RT);

IbarNa_c = ((V == 0)
             ? PNa*zNa*zNa*F*0.75
             : PNa*zNa*zNa*V*F_RT*F*0.75/em1term
             );
IbarNa = IbarNa_c*(Nai*eterm-Nao);
ICaNa = IbarNa*k_CaL;

IbarK_c = ((V == 0)
            ? PK*zK*zK*F*0.75
            : PK*zK*zK*V*F_RT*F*0.75/em1term
            );
IbarK = IbarK_c*(Ki*eterm-Ko);
ICaK  = IbarK*k_CaL;

ezca_term = exp(zCa*V*F_RT);
IbarCa_c = ((V == 0)
             ? PCa*zCa*zCa*F/zCa
             : PCa*zCa*zCa*V*F_RT*F/expm1(zCa*V*F_RT)
             );
IbarCa = IbarCa_c*(Cai/1000*ezca_term - 0.341*Cao);
ICa   = IbarCa*k_CaL;

ICaL = ICa+ICaNa+ICaK;

// T-type Calcium current
b_inf = (( V > -120. )
         ? 1./(1+exp(-(V+14)/10.8))
         : 0
         );
tau_b = (( V < 40. )
         ? 3.7+6.1/(1+exp((V+25.)/4.5))
         : 3.7
         );
tau_g = ((V <= 0)
         ? -0.875*V+12.
         : 12.
         );
g_inf = ((V<= 0.)
         ? 1./(1+exp((V+60.)/5.6))
         : 0
         );
GCaT = 0.05; .units(mS/uF);
Cao_uM  = Cao *1000.;
ECaT = log(Cao_uM/Cai)/F_RT/2.;
ICa_T = GCaT*g*b*b*(V-ECaT);

// plataeu
IbarpCa = 1.15; .units(uA/uF);  // Max. Ca current through sarcolemmal Ca pump
KmpCa = 0.0005; .units(mM); // Half-saturation [Ca] of sarcolemmal Ca pump
KmpCa_uM = KmpCa*1000;
IpCa = IbarpCa*Cai/(KmpCa_uM+Cai);

// background
a_Cab = 0.003016;
ICab = a_Cab*(V-ECa);

// Natrium currents
a_m = 0.32*(V+47.13)/(1-exp(-0.1*(V+47.13)));
b_m = 0.08*exp(-V/11.);
a_h = ((V >= -40)
       ? 1.0e-10
       : 0.135*exp((80+V)/(-6.8))
       );
b_h = ((V >= -40)
       ? 1./( 0.13*(1+exp((V+10.66)/(-11.1))))
       : 3.56*exp(0.079*V)+3.1e5*exp(0.35*V)
       );
a_j = ((V >= -40)
       ? 1.0e-10
       : (-1.2714e5*exp(0.2444*V)-3.474e-5*exp(-0.04391*V))*(V+37.78)/(1+exp(0.311*(V+79.23)))
       );
b_j = ((V >= -40)
       ? 0.3*exp( -2.535e-7*V)/(1+exp(-0.1*(V+32)))
       : 0.1212*exp(-0.01052*V)/(1+exp(-0.1378*(V+40.14)))
       );
GNa = 16.; .units(mS/uF);//!< Max. Channel conductance 
INa  = GNa*(V-ENa)*m*m*m*h*j;

GbNa = 0.004; .units(mS/uF);  // Max. conductance of Na background 
INab = (V-ENa)*GbNa;

// NaCa exchanger
C1  = .00025; .units(uA/uF); // Scaling factor for inaca 
C2  = 0.0001; .units(mM);   // Half-saturation [Ca] of NaCa exhanger
gam = .15; // Position of energy barrier controlling voltage dependance of iNaCa
Nao3_cai  = Nao3*Cai*0.001;
INaCa_A = C1*exp((gam-1)*V*F_RT);
INaCa_B = eterm*Nai*Nai*Nai*Cao;
INaCa_C = C2*exp((gam-1)*V*F_RT);INaCa_A;
INaCa    = INaCa_A*(INaCa_B-Nao3_cai)/(1+INaCa_C*(INaCa_B+Nao3_cai));

// Sodium-Potassium Pump
IbarNaK = 2.25; .units(uA/uF);  // Max. current through Na-K pump 
KmNai = 10.; .units(mM);         // Half-saturation [Na] of NaK pump (mM)
KmKo = 1.5; .units(mM);          // Half-saturation [K] of NaK pump (mM)
GNaK = 1; .param();
sigma = (exp(Nao/67.3)-1)/7;
fNaK  = 1./(1+0.1245*exp(-0.1*V*F_RT)+0.0365*sigma*exp(-V*F_RT));
INaK = GNaK*IbarNaK*fNaK*(1/(1+KmNai*KmNai/(Nai*Nai)))*(Ko/(Ko+KmKo));

tau_xr = (( V == -14.2 )
          ? 1./(0.00138/0.123 +
                0.00061*(V+38.9)/(exp(0.145*(V+38.9))-1) )
          : (( V == -38.9 )
             ? 1./(0.00138*(V+14.2)/(1-exp(-0.123*(V+14.2))) +
                   0.00061/0.145 )
             : 1./(0.00138*(V+14.2)/(1-exp(-0.123*(V+14.2))) +
                   0.00061*(V+38.9)/(exp(0.145*(V+38.9))-1) )
             ));
xr_inf = 1./(1+exp(-(V+21.5)/7.5));
r      = 1./(1+exp((V+9.)/22.4));
GKr = 0.02614*sqrt(Ko/5.4);
IKr     = xr*GKr*r*(V-EK1);

//IKs_temp = (0.057 + 0.19/(1.+exp((-7.2-log10(Cai)+3)/0.6)));
tau_xs1 = (( V != -30 )
           ? 1/( 7.19e-5*(V+30)/(1.-exp(-0.148*(V+30))) +
                 1.31e-4*(V+30)/(exp(0.0687*(V+30))-1))
           : 1./( 7.19e-5/0.148 + 1.31e-4/0.0687 )
           );
xs1_inf = 1/(1+exp((1.5-V)/16.7));
tau_xs2 = tau_xs1*4;
xs2_inf = xs1_inf;
IKs_temp = 0.433*(1+0.6/(1+pow((0.000038/(Cai*.001)),1.4)));
IKs     = IKs_temp*xs1*xs2*(V-EKs);

EKp  = EK1;
Kp   = 1/(1+exp((7.488-V)/5.98));
GKp = 0.00552; .units(mS/uF);
IKp = GKp*Kp*(V-EKp);

aa_K1 = 1.02/(1+exp(0.2385*(VEK1-59.215)));
bb_K1 = (0.49124*exp(0.08032*(VEK1+5.476))+exp(0.06175*(VEK1-594.31)))/
  (1+exp(-0.5143*(VEK1+4.753)));
K1_in= aa_K1/(aa_K1 +bb_K1);
GK1 = 0.75*sqrt(Ko/5.4);
IK1 = GK1*K1_in*(VEK1);

a_zdv    = (10*exp((V-40)/25))/(1+exp((V-40)/25));
b_zdv    = (10*exp(-(V+90)/25))/(1+exp(-(V+90)/25));
a_ydv    = 0.015/(1+exp((V+60)/5));
b_ydv    = (0.1*exp((V+25)/5))/(1+exp((V+25)/5));
gitodv = 0.5;
rvdv = exp(V/100);
Ito_temp = gitodv*rvdv*(V-Ekdv);
Ito   = Ito_temp*zdv*zdv*zdv*ydv;


ICai = ICaL + ICa_T -INaCa -INaCa + IpCa + ICab;
INai = INa+INab+ICaNa+3*INaK+3*INaCa;
IKi  = IKr+IKs+IK1+IKp+ICaK-INaK-INaK;
diff_Nai = -INai*acap/(vmyo*zNa*F);
Nai_init = 12.236437;
diff_Ki = -IKi*acap/(vmyo*zK*F);
Ki_init = 136.89149;

// compute total current
Iion = INai+IKi+ICai;

Kmup = 0.00092; .units(mM); // Half-saturation concentration of iup (mM)
Kmup_uM = Kmup*1000; //convert(Kmup, uM);
Iupbar = 0.00875; .units(mM/ms); // Max. current through iup channel (mM/ms)
Irup = Iupbar*Cai/(Cai+Kmup_uM);

CaNSRbar =  15.; .units(mM);
Kleak = Iupbar/CaNSRbar;
Ileak = Kleak*nsr;

Iup = Irup-Ileak;
Itr = (nsr-jsr)/180;
diff_nsr = Iup-Itr*vjsr/vnsr;
nsr_init = 1.179991;

tcicr = (tcicr < 0 
         ? (V > -35 and fdiff_ICai > fdiff_ICaIold
            ? 0
            : -1
            )
         : (-Iion > 10
            ? -1
            : tcicr + dt
            )
         );
.store();
tcicr_init = 1000;
exp_tcicr = (tcicr < 0
             ? 0
             : exp((-tcicr+4)/tau_on)
             );
tau_on = 0.5;
magrel = 1/(1+exp((ICai+5)/0.9));
gmaxrel = 150;
ryr_open = 1/(1+exp_tcicr);
ryr_close = 1-ryr_open;
irelcicr = gmaxrel*magrel*ryr_open*ryr_close*(jsr-1e-3*Cai)*1;

kmcsqn  = 0.8; .units(mM); // Equalibrium constant of buffering for CSQN
csqnbar =  10; .units(mM); // Max. [Ca] buffered in CSQN
csqn    = csqnbar*(jsr/(jsr+kmcsqn));
csqnth  = 8.75; .units(mM); // Threshold for release of Ca from CSQN due to JSR overload

tjsrol = ((csqn>=csqnth and tjsrol>50)
          ? 0
          : tjsrol + dt
          );
.store();
tjsrol_init = 1000;

tau_on_ol = tau_on;
exp_tjsrol = exp(-tjsrol/tau_on_ol);
ol_open = 1-exp_tjsrol;
ol_close = exp_tjsrol;
grelbarjsrol = 4;
ireljsrol = grelbarjsrol*ol_open*ol_close*(jsr-1e-3*Cai);


cmdnbar =     0.050; .units(mM);      // Max. [Ca] buffered in CMDN (mM)
trpnbar =     0.070; .units(mM);      // Max. [Ca] buffered in TRPN (mM)
kmcmdn  =     0.00238; .units(mM);    // Equalibrium constant of CMDN buffering (mM)
kmtrpn  =     0.0005; .units(mM);     // Equalibrium constant of TRPN buffering (mM)
trpn    = trpnbar*(1e-3*Cai/(1e-3*Cai+kmtrpn));
cmdn    = cmdnbar*(1e-3*Cai/(1e-3*Cai+kmcmdn));

djsr = (Itr-irelcicr-ireljsrol)*dt;
bjsr    = csqnbar-csqn-djsr-jsr+kmcsqn;
cjsr    = kmcsqn*(csqn+djsr+jsr);
jsr_init = 1.179991;
jsr = (sqrt(bjsr*bjsr+4*cjsr)-bjsr)/2;
.store();

// update cai
dCai = -((ICai*acap)/(vmyo*zCa*F)
          +Iup     *vnsr/vmyo
          -irelcicr *vjsr/vmyo
          -ireljsrol*vjsr/vmyo)*dt;     // in mM

catotal = trpn+cmdn+dCai+Cai*1e-3;


bmyo    = cmdnbar+trpnbar+kmtrpn+kmcmdn-catotal;
cmyo    = kmcmdn*kmtrpn-catotal*(kmtrpn+kmcmdn)
  +trpnbar*kmcmdn+cmdnbar*kmtrpn;
dmyo    = -kmtrpn*kmcmdn*catotal;
fmyo = sqrt(bmyo*bmyo-3*cmyo);
CaIupdate = ((2*fmyo/3)*cos(acos((9*bmyo*cmyo-2*bmyo*bmyo*bmyo-27*dmyo)/(2*pow((bmyo*bmyo-3*cmyo),1.5)))/3)-(bmyo/3))*1000;
Cai = CaIupdate;
.store();
Cai_init = 0.079;

