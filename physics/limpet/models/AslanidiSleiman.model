group { 
  #Authors ::= Aslanidi OV, Sleiman RN, Boyett MR, Hancox JC, Zhang H
  #Year ::= 2010
  #Title ::= Ionic mechanisms for electrical heterogeneity between rabbit Purkinje fiber and Ventricular cells
  #Journal ::= Biophys J, 98(11),2420-31
  #DOI ::= 10.1016/j.bpj.2010.02.033
} .reference();

V;      .external(Vm); .nodal();
Iion;   .external();   .nodal();
Ke;     .external(Ke); .nodal();


V;      .lookup(-1500,1500,0.05); .units(mV);
Cai_mM; .lookup(1e-5, 20, 1e-5);  .units(mM);

Cai_mM = Cai / 1000.;

V_init   = -85.49190000;
m_init   = 0.00142380;
h_init   = 0.98646900;
j_init   = 0.99907100;
mL_init  = 0.00142380;
hL_init  = 0.93722800;
xr_init  = 0.00195127;
x_init   = 0.00036627;
y1_init  = 0.99816200;
y2_init  = 0.99816200;
xs_init  = 0.03065770;
d_init   = 1.71221e-06;
f_init   = 0.99764600;
b_init   = 8.06835e-05;
g_init   = 0.97941500;

Cai_init  = 9.93905e-02;   // Ca_buffer_Cai (umolar)

Y0_init  = 1.28293;        // Ca_buffer_Ca_Calsequestrin (millimolar)
Y1_init  = 0.000131111;    // Ca_buffer_Ca_SL (millimolar)
Y2_init  = 0.0121366;      // Ca_buffer_Ca_SLB_SL (millimolar)
Y3_init  = 0.0116031;      // Ca_buffer_Ca_SLB_jct (millimolar)
Y4_init  = 0.132481;       // Ca_buffer_Ca_SLHigh_SL (millimolar)
Y5_init  = 0.0981466;      // Ca_buffer_Ca_SLHigh_jct (millimolar)
Y6_init  = 0.633156;       // Ca_buffer_Ca_SR (millimolar)
Y7_init  = 0.00026248;     // Ca_buffer_Ca_jct (millimolar )

Y11_init = 0.04;
Y23_init = 2.67486e-07;    // Jrel_SR_I (dimensionless)
Y24_init = 1.94911e-06;    // Jrel_SR_O (dimensionless)
Y25_init = 0.879232;       // Jrel_SR_R (dimensionless)
Y32_init = 0.000336009;    // cytosolic_Ca_buffer_Ca_Calmodulin (millimolar)
Y33_init = 0.00244706;     // cytosolic_Ca_buffer_Ca_Myosin (millimolar)
Y34_init = 0.00243042;     // cytosolic_Ca_buffer_Ca_SRB (millimolar)
Y35_init = 0.00996049;     // cytosolic_Ca_buffer_Ca_TroponinC (millimolar)
Y36_init = 0.12297;        // cytosolic_Ca_buffer_Ca_TroponinC_Ca_Mg (millimolar)
Y37_init = 0.136961;       // cytosolic_Ca_buffer_MgMyosin (millimolar)
Y38_init = 0.0079682;      // cytosolic_Ca_buffer_MgTroponinC_Ca_Mg (millimolar)

group {
 V;
 Cai;
 INCX;
}.trace();
 
group {
 Y0;
 Y1;
 Y6;
 Y7;
 Cai;
 Y23;
 Y24;
 Y25;
}.method(rosenbrock);

group { 
GNa   = 20;                      .units(mS / cm^2);
GNaL  = 0.0162;                  .units(mS / cm^2);
GKr   = 0.015583333333333;       .units(mS / cm^2);
GKs   = 0.011738183745949;       .units(mS / cm^2);
GK1   = 0.5;                     .units(mS / cm^2);
Gto   = 0.112;                   .units(mS / cm^2);
GCaL  = 0.27;                    .units(mS / cm^2);
GCaT  = 0.2;                     .units(mS / cm^2);
GCl   = 0.3;                     .units(mS / cm^2);
GClB  = 0.0009;                  .units(mS / cm^2);
GCaB  = 3.51820e-04;             .units(mS / cm^2);
GNaB  = 2.97000e-05;             .units(mS / cm^2);
GKB   = 5.00000e-05;             .units(mS / cm^2);

Nae     = 140.0;                 .units(mM);
Nai     = 8.8;                   .units(mM);
Ki      = 135.0;                 .units(mM);
Ke_init = 5.4;                   .units(mM);
Cle     = 150.0;                 .units(mM);
Cli     = 30.0;                  .units(mM);
Mgi     = 1.0;                   .units(mM);
Cae     = 1.8;                   .units(mM);

T = 310.0;                       .units(K);

INaCamax = 4.5;
kmcaact  = 0.000125;  .units(mM);   
kmnai1   = 12.3;      .units(mM);
kmnao    = 87.5;      .units(mM);
kmcai    = 0.0036;    .units(mM);       
kmcao    = 1.3;       .units(mM);
nu       = 0.35;      // position of energy barrier for INaCa
ksat     = 0.27;      // saturation factor for INaCa at negatiVole potentials

PNaK    = 0.01833;

Ca_handling = 1;      // by default, Ca2+ handling is turned on

ICaPHill = 1.6;
ICaPVmf  = 0.0538 * 1.25;
ICaPKmf  = 0.5;
} .param();

F     = 96486.7;        .units(J / kmol / K);
R     = 8314.3;         .units(C / mol);
RTonF = R * T / F;          .units(mV);
PI    = 3.14159265359;  // unitless

Ca_buffer_Bmax_Calsequestrin  = 0.14;       // mM
Ca_buffer_Bmax_SLB_SL         = 0.0374;     // mM
Ca_buffer_Bmax_SLB_jct        = 0.0046;     // mM
Ca_buffer_Bmax_SLHigh_SL      = 0.0134;     // mM
Ca_buffer_Bmax_SLHigh_jct     = 0.00165;    // mM
Ca_buffer_koff_Calsequestrin  = 65.0;       // ms^-1
Ca_buffer_koff_SLB            = 1.3;        // ms^-1
Ca_buffer_koff_SLHigh         = 30.0e-3;    // ms^-1
Ca_buffer_kon_Calsequestrin   = 100.0;      // mM^-1 ms^-1
Ca_buffer_kon_SL              = 100.0;      // mM^-1 ms^-1

JleaKSR_KSRleak               = 5.348e-6;   // per_millisecond
JpumPSR_H                     = 1.787;      // dimensionless
JpumPSR_Kmf                   = 0.000246;   // millimolar
JpumPSR_Kmr                   = 1.7;        // millimolar
JpumPSR_Q10_SRCaP             = 2.6;        // dimensionless
JpumPSR_V_max                 = 286.0e-6;   // millimolar_per_millisecond
Jrel_SR_EC50_SR               = 0.45;       // millimolar
Jrel_SR_HSR                   = 2.5;        // dimensionless
Jrel_SR_Max_SR                = 15.0;       // dimensionless
Jrel_SR_Min_SR                = 1.0;        // dimensionless
Jrel_SR_kiCa                  = 0.5;        // per_millimolar_per_millisecond
Jrel_SR_kim                   = 0.005;      // per_millisecond
Jrel_SR_koCa                  = 10.0;       // per_millimolar2_per_millisecond
Jrel_SR_kom                   = 0.06;       // per_millisecond
Jrel_SR_ks                    = 25.0;       // per_millisecond

cytosolic_Ca_buffer_Bmax_Calmodulin         = 0.024;      // millimolar
cytosolic_Ca_buffer_Bmax_Myosin_Ca          = 0.14;       // millimolar
cytosolic_Ca_buffer_Bmax_Myosin_Mg          = 0.14;       // millimolar
cytosolic_Ca_buffer_Bmax_SRB                = 0.0171;     // millimolar
cytosolic_Ca_buffer_Bmax_TroponinC          = 0.07;       // millimolar
cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa  = 0.14;       // millimolar
cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg  = 0.14;       // millimolar
cytosolic_Ca_buffer_koff_Calmodulin         = 238.0e-3;   // per_millisecond
cytosolic_Ca_buffer_koff_Myosin_Ca          = 0.46e-3;    // per_millisecond
cytosolic_Ca_buffer_koff_Myosin_Mg          = 0.057e-3;   // per_millisecond
cytosolic_Ca_buffer_koff_SRB                = 60.0e-3;    // per_millisecond
cytosolic_Ca_buffer_koff_TroponinC          = 19.6e-3;    // per_millisecond
cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa  = 0.032e-3;   // per_millisecond
cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg  = 3.33e-3;    // per_millisecond
cytosolic_Ca_buffer_kon_Calmodulin          = 34.0;       // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_Myosin_Ca           = 13.8;       // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_Myosin_Mg           = 15.7e-3;    // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_SRB                 = 100.0;      // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_TroponinC           = 32.7;       // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa   = 2.37;       // per_millimolar_per_millisecond
cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg   = 3.0e-3;     // per_millimolar_per_millisecond

ion_diffusion_A_SL_cytosol     = 1.3e-4;     // cm2
ion_diffusion_A_jct_SL         = 3.01e-6;    // cm2
ion_diffusion_D_Ca_SL_cytosol  = 1.22e-6;    // dm2_per_second
ion_diffusion_D_Ca_jct_SL      = 1.64e-6;    // dm2_per_second
ion_diffusion_D_NaSL_cytosol   = 1.79e-5;    // dm2_per_second
ion_diffusion_D_Najct_SL       = 1.09e-5;    // dm2_per_second
ion_diffusion_x_SL_cytosol     = 0.45;       // micrometre
ion_diffusion_x_jct_SL         = 0.5;        // micrometre

Cm_per_area   = 2.0e-6;     // farad_per_cm2
cell_length   = 100.0;      // micrometre
cell_radius   = 10.25;      // micrometre

Vol_Cell      = PI * square(cell_radius / 1000.0) * cell_length / cube(1000.0);         .units(L);
Vol_cytosol   = 0.65 * Vol_Cell;                                                        .units(L);
Vol_SR        = 0.035 * Vol_Cell;                                                       .units(L);
Vol_SL        = 0.02 * Vol_Cell;                                                        .units(L);
Vol_jct       = 0.00051 * Vol_Cell;                                                     .units(L);
Cm            = Cm_per_area * 2.0 * cell_radius / 10000.0 * PI * cell_length / 10000.0; .units(F);

E_Na = RTonF * log(Nae / Nai);                                .units(mV);
E_K  = RTonF * log(Ke  / Ki );                                .units(mV);
E_Ks = RTonF * log((Ke + PNaK * Nae)/(Ki + PNaK * Nai));      .units(mV);

a_m = 0.32 * (V+47.13)/(1- exp (-0.1* (V+47.13)));
b_m = 0.08 * exp (-V / 11);

a_h = (V >= -40) ? 0 : 0.135 * exp ((80+V)/-6.8);
b_h = (V >= -40) ? 1/(0.13 * (1+exp ((V+10.66)/-11.1))) :
    0.3 * exp (-2.535 * 10e-7 * V)/(1+ exp (-0.1* (V+32)));

a_j = (V >= -40) ? 0 : ((-1.2714 * 10e5 * exp (0.2444* V) 
    - (3.474 * 10e-5 * exp (-0.0439 * V)) * (V+37.78)))
    /(1+exp (0.311 * (V+79.23)));
b_j = (V >= -40) ? 0.3 * exp (-2.535 * 10e-7 * V)/(1+ exp (-0.1* (V+32))) :
    0.1212 * exp (-0.01052* V)/(1+exp (-0.1378 * (V+40.14)));

INa = GNa * m * m * m * h * j * (V - E_Na);

a_mL   = (fabs(V+47.13) < 0.05) ? 0.32 / 0.1 : 0.32*(V+47.13)/(1-exp(-0.1*(V+47.13)));
b_mL   = 0.08 * exp(-V / 11);

tau_hL = 132.4+ 112.8 * exp(0.02325 * V);
hL_inf = 1/(1+exp((V-22.0+91.0)/6.1));

INaL  = GNaL * mL * hL * (V - E_Na);

INaB  = GNaB * (V - E_Na);

sigma = (exp(Nae / 67.3) - 1.0)/7.0;
fNaK  = 1.0/(1.0 + 0.1245 * exp(-0.1 * V/(RTonF)) + 0.0365 * sigma * exp(-1.0 * V/(RTonF)));
INaK = 0.61875 * fNaK * 1/(1+square(10 / Nai))*(Ke/(Ke+1.5));

Nai3 = Nai * Nai * Nai;
Nae3 = Nae * Nae * Nae;

kmnai13     = kmnai1 * kmnai1 * kmnai1;
kmnao3      = kmnao * kmnao * kmnao;
exPnuVFRT   = exp(nu * V * F/(R * T));
exPnum1VFRT = exp((nu-1)*V * F/(R * T));
allo        = 1/(1+square((kmcaact/(1.5 * Cai_mM))));
num         = 0.4 * INaCamax*(Nai3 * Cae * exPnuVFRT-Nae3 * 1.5 * Cai_mM * exPnum1VFRT);
denommult   = 1+ksat * exp((nu-1)*V * F/(R * T));
denomterm1  = kmcao * Nai3+kmnao3 * 1.5 * Cai_mM+kmnai13 * Cae*(1+1.5 * Cai_mM / kmcai);
denomterm2  = kmcai * Nae3*(1+Nai3 / kmnai13)+Nai3 * Cae+Nae3 * 1.5 * Cai_mM;
deltaE      = num/(denommult*(denomterm1+denomterm2));

INCX = allo * deltaE;

xr_inf = 1.0/(1.0 + exp(-1.0 * (V + 20.0)/10.5));
# tau_xr has singularities at V = -7 and -10 mV due to exponentials
# l'Hopital's rule is used to eliminate these from the LUT calculation
tau_xr = 1.0/( (fabs(V+7)  < 0.05) ? 0.00138 / 0.123 : 0.00138 * (V + 7.0)/(1.0 - exp(-0.123 * (V + 7.0))) 
             + (fabs(V+10) < 0.05) ? 0.00061 / 0.145 : 0.00061 * (V + 10.0)/(exp(0.145 * (V + 10.0)) - 1.0));
r_infty = 1.0/(1.0 + exp(V / 50));

IKr = GKr * xr * r_infty * (V - E_K);

xs_inf = 1.1/(1.0+exp(-1.0*(V-1.5)/20));
tau_xs = (600.0/(1.0+exp((V-20)/15.0)) + 250.0);

IKs = GKs * xs * (V - E_Ks);

aa_k1 = 0.3/(1.0 + exp(0.2385 * (V - E_K - 59.215)));
bb_k1 = (0.49124 * exp(0.08032 * (V - E_K + 5.476)) 
      + exp(0.06175 * (V - E_K - 594.31)))/(1.0 + exp(-0.5143 * (V - E_K + 4.753)));
k1_infty = aa_k1/(aa_k1 + bb_k1);

IK1 = GK1 * (k1_infty + 0.008) * (V - E_K);

IKPKp = 1.0/(1.0 + exp((7.488 - V)/5.98));      
IKp    = 0.001 * IKPKp * (V - E_K);

IKB  = GKB * (V - E_K);

aa_x  = (0.0451 * exp (0.85 * 0.03577 * V));
bb_x  = (0.0989 * exp (-0.85 * 0.0623 * V));
x_inf = aa_x / (aa_x + bb_x);
tau_x = 0.2  / (aa_x + bb_x);

aa_y   = (0.05415 * exp (-(V+12.5)/15)/(1+0.051335 * exp(-(V+12.5)/15)));
bb_y   = (0.05415 * exp ((V+33.5)/15)/(1+0.051335 * exp((V+33.5)/15)));
y1_inf = aa_y/(aa_y+bb_y);
y2_inf = y1_inf;
tau_y1 = 0.7*(15+20.0/(aa_y+bb_y));
tau_y2 = 4.0/(aa_y+bb_y);

Ito = Gto * x * (y1 * 0.75 + y2 * 0.25) * (V - E_K);

E_CaL = 60.0; .units(mV);

d_inf = (1/(1.0+exp(-(V-4.0)/6.74)));
tau_d = (0.59+0.8 * exp(0.052*(V+13))/(1+exp(0.132*(V+13))));
f_inf = 1./(1.0 + exp((V+25.)/10.));
tau_f = 0.005 * square((V-2.5))+4.0;

ICaL = GCaL * d * f * (1-Y11) * (V - E_CaL);

E_CaT = 50.0;   .units(mV);

b_inf = 1/(1+ exp (-(V+28)/6.1));
aa_b = 1.068 * exp((V+16.3)/30.0);
bb_b  = 1.068 * exp(-(V+16.3)/30.0);
tau_b = 1.0/(aa_b+bb_b);

g_inf = 1/(1+exp((V+60)/6.6));
aa_g  = 0.015 * exp(-(V+71.7)/83.3);
bb_g  = 0.015 * exp((V+71.7)/15.4);
tau_g = 1.0/(aa_g+bb_g);

ICaT = GCaT * b * g * (V - E_CaT);

ICa = ICaL + ICaT;

E_Ca  = (RTonF)/2.0 * log(Cae/(Cai_mM));
ICaB = GCaB * (V - E_Ca);

ICaP = 0.5 * ICaPVmf / (1.0 + pow((ICaPKmf/(Cai_mM * 1000)), ICaPHill));

E_Cl = RTonF * log(Cli / Cle);

a_inf = 1.0/(1.0 + exp(-(V + 5.0)/10.0));
i_inf = 1.0/(1.0 + exp((V + 75.0)/10.0));
tau_i = (10.0 / (1.0 + exp((V + 33.5)/10.0)) + 10.0)*2;

ICl  = GCl * a_inf * i / (1.0 + 0.1/(Cai_mM * 1000)) * (V - E_Cl);
IClB = GClB* (V - E_Cl);

Iion = INa + INaL + IKr + IKs + IK1 + IKp + Ito + ICaL + ICaT + ICl 
     + IClB + ICaB + INaB + IKB + INaK + ICaP + INCX;

Ca_buffer_dCalsequestrin = Ca_buffer_kon_Calsequestrin * Y6 * (Ca_buffer_Bmax_Calsequestrin * Vol_cytosol / Vol_SR - Y0) - Ca_buffer_koff_Calsequestrin * Y0;

diff_Y0  = Ca_buffer_dCalsequestrin;

Ca_buffer_dCa_SLB_SL     = Ca_buffer_kon_SL * Y1 * (Ca_buffer_Bmax_SLB_SL * Vol_cytosol / Vol_SL-Y2)
                           -Ca_buffer_koff_SLB * Y2;
Ca_buffer_dCa_SLB_jct    = Ca_buffer_kon_SL * Y7 * (Ca_buffer_Bmax_SLB_jct * 0.1 * Vol_cytosol / Vol_jct-Y3)
                           -Ca_buffer_koff_SLB * Y3;
Ca_buffer_dCa_SLHigh_SL  = Ca_buffer_kon_SL * Y1 * (Ca_buffer_Bmax_SLHigh_SL * Vol_cytosol / Vol_SL-Y4)
                           -Ca_buffer_koff_SLHigh * Y4;
Ca_buffer_dCa_SLHigh_jct = Ca_buffer_kon_SL * Y7 * (Ca_buffer_Bmax_SLHigh_jct * 0.1 * Vol_cytosol / Vol_jct-Y5)
                           -Ca_buffer_koff_SLHigh * Y5;

diff_Y2 = Ca_buffer_dCa_SLB_SL;
diff_Y3 = Ca_buffer_dCa_SLB_jct;
diff_Y4 = Ca_buffer_dCa_SLHigh_SL;
diff_Y5 = Ca_buffer_dCa_SLHigh_jct;

Ca_buffer_dCa_jct_tot_bound = Ca_buffer_dCa_SLB_jct + Ca_buffer_dCa_SLHigh_jct;
Ca_buffer_dCa_SL_tot_bound  = Ca_buffer_dCa_SLB_SL  + Ca_buffer_dCa_SLHigh_SL;

Ca_buffer_ICa_jct_tot = ICa;
Ca_buffer_ICaSL_tot   = -2.0 * INCX + 1.0 * ICaB + 1.0 * ICaP;

JpumPSR_j_pumPSR = 2.0 * JpumPSR_V_max * Vol_cytosol / Vol_SR*(pow(Cai_mM / JpumPSR_Kmf, JpumPSR_H)
                     -pow(Y6 / JpumPSR_Kmr, JpumPSR_H))/(1.0+pow(Cai_mM / JpumPSR_Kmf, JpumPSR_H)
                     +pow(Y6 / JpumPSR_Kmr, JpumPSR_H));
JleaKSR_j_leaKSR = 0.5 * JleaKSR_KSRleak*(Y6-Y7);
Jrel_SR_j_rel_SR = 2.0 * Jrel_SR_ks * Y24*(Y6-Y7);

diff_Y6  = JpumPSR_j_pumPSR-(JleaKSR_j_leaKSR * Vol_cytosol / Vol_SR+Jrel_SR_j_rel_SR)-Ca_buffer_dCalsequestrin;
ion_diffusion_J_Ca_jct_SL = (Y7 - Y1) * 8.2413e-13;

ion_diffusion_J_Ca_SL_cytosol = (Y1 - Cai_mM) * 3.7243e-12;
diff_Y1 = -0.5 * Ca_buffer_ICaSL_tot * Cm / (Vol_SL * 2.0 * F)
        + (ion_diffusion_J_Ca_jct_SL - ion_diffusion_J_Ca_SL_cytosol) / Vol_SL - 1.0 * Ca_buffer_dCa_SL_tot_bound;

cytosolic_Ca_buffer_dCa_TroponinC = cytosolic_Ca_buffer_kon_TroponinC * Cai_mM*(cytosolic_Ca_buffer_Bmax_TroponinC-Y35)-cytosolic_Ca_buffer_koff_TroponinC * Y35;
cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg = cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa * Cai_mM*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa-(Y36+Y38))-cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa * Y36;
cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg = cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg * Mgi*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg-(Y36+Y38))-cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg * Y38;
cytosolic_Ca_buffer_dCa_Calmodulin = cytosolic_Ca_buffer_kon_Calmodulin * Cai_mM*(cytosolic_Ca_buffer_Bmax_Calmodulin-Y32)-cytosolic_Ca_buffer_koff_Calmodulin * Y32;
cytosolic_Ca_buffer_dCa_Myosin = cytosolic_Ca_buffer_kon_Myosin_Ca * Cai_mM*(cytosolic_Ca_buffer_Bmax_Myosin_Ca-(Y33+Y37))-cytosolic_Ca_buffer_koff_Myosin_Ca * Y33;
cytosolic_Ca_buffer_dMgMyosin = cytosolic_Ca_buffer_kon_Myosin_Mg * Mgi*(cytosolic_Ca_buffer_Bmax_Myosin_Mg-(Y33+Y37))-cytosolic_Ca_buffer_koff_Myosin_Mg * Y37;
cytosolic_Ca_buffer_dCa_SRB = cytosolic_Ca_buffer_kon_SRB * Cai_mM*(cytosolic_Ca_buffer_Bmax_SRB-Y34)-cytosolic_Ca_buffer_koff_SRB * Y34;
cytosolic_Ca_buffer_dCa_cytosol_tot_bound = cytosolic_Ca_buffer_dCa_TroponinC+cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg+cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg+cytosolic_Ca_buffer_dCa_Calmodulin+cytosolic_Ca_buffer_dCa_Myosin+cytosolic_Ca_buffer_dMgMyosin+cytosolic_Ca_buffer_dCa_SRB;

Jrel_SR_kCaSR = Jrel_SR_Max_SR-(Jrel_SR_Max_SR-Jrel_SR_Min_SR)/(1.0+pow(Jrel_SR_EC50_SR / Y6, Jrel_SR_HSR));

diff_Y32 = cytosolic_Ca_buffer_dCa_Calmodulin;
diff_Y33 = cytosolic_Ca_buffer_dCa_Myosin;
diff_Y34 = cytosolic_Ca_buffer_dCa_SRB;
diff_Y35 = cytosolic_Ca_buffer_dCa_TroponinC;
diff_Y36 = cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg;
diff_Y37 = cytosolic_Ca_buffer_dMgMyosin;
diff_Y38 = cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg;

diff_Y7  = -0.5 * Ca_buffer_ICa_jct_tot * Cm /(Vol_jct * 2.0 * F)
         -ion_diffusion_J_Ca_jct_SL / Vol_jct
         +Jrel_SR_j_rel_SR * Vol_SR / Vol_jct
         +JleaKSR_j_leaKSR * Vol_cytosol / Vol_jct
         -1.0 * Ca_buffer_dCa_jct_tot_bound;

Jrel_SR_koSRCa = Jrel_SR_koCa / Jrel_SR_kCaSR;
Jrel_SR_kiSRCa = Jrel_SR_kiCa * Jrel_SR_kCaSR;
Jrel_SR_RI     = 1.0-Y25-Y24-Y23;

diff_Y23 = Jrel_SR_kiSRCa * Y7 * Y24-Jrel_SR_kim * Y23-(Jrel_SR_kom * Y23-Jrel_SR_koSRCa * Y7 * Y7 * Jrel_SR_RI);
diff_Y24 = Jrel_SR_koSRCa * Y7 * Y7 * Y25-Jrel_SR_kom * Y24-(Jrel_SR_kiSRCa * Y7 * Y24-Jrel_SR_kim * Y23);
diff_Y25 = Jrel_SR_kim * Jrel_SR_RI-Jrel_SR_kiSRCa * Y7 * Y25-(Jrel_SR_koSRCa * Y7 * Y7 * Y25-Jrel_SR_kom * Y24);

if ( Ca_handling == 1 ) {
   diff_Cai  =
   (-JpumPSR_j_pumPSR * Vol_SR / Vol_cytosol+ion_diffusion_J_Ca_SL_cytosol / Vol_cytosol-1.0 * cytosolic_Ca_buffer_dCa_cytosol_tot_bound)*1000.;
   diff_Y11 = 0.7 * Y7*(1.0-Y11)-11.9e-3 * Y11;
} else {
   diff_Cai = 0;
   diff_Y11 = 0;
}
