group { 
  #Authors ::= A. Nygren , C. Fiset , L. Firek, J. W. Clark , D. S. Lindblad , R. B. Clark , and W. R. Giles
  #Year ::= 1998
  #Title ::= Mathematical model of an adult human atrial cell: the role of K+ currents in repolarization
  #Journal ::= Circulation Research,9-23,82(1),63-81
  #DOI ::= 10.1161/01.res.82.1.63
} .reference();

V; .external(Vm); .nodal();
Iion; .external(); .nodal();

V; .lookup(-1000, 1000, 0.01);
Cai; .lookup(1e-6, 100.0e-3, 1e-6);
Ca_rel; .lookup(0, 1, 1e-5);
Ca_d; .lookup(1e-6, 100e-3, 1e-6);
F2; .lookup(0, 1, 1e-5);

group {
  GCaL;
  GKr;
  GKS;
  Gt;
  Gsus;
  GK1;
  GNaB;
  GCaB;
  KNaCa;
  maxINaK;
} .param();

group {
  V;
  INa;
  ICaL;
  INaCa;
  It;
  IbCa;
  ICaP;
  Isus;
  IKs;
  IKr;
  IK1;
  IBNa;
  INaK;
  Irel;
  Iup;
  Itr;
  Idi;
  Iion;
}.trace();


  
R        = 8314.0;             /* mJ/molK */
T        = 306.15;             /* K       */
F        = 96487.0;            /* C/mol   */

Cm       = 1.0;                /* uF/cm^2 */
Voli     = 0.11768;            /* mm3/cm2 */
DOM_FRAC = 0.02;



Nab     = 130.0;              /* mM      */
Kb      = 5.4;                /* mM      */

MGi     = 2.5;                /* mM      */
Eca_app = 60.0;               /* mV      */


Nao        = 130.0;
concKo     = 5.4;
Cao        = 1.8;


Nai = 8.55474;
Ki  = 129.435;

Nac  = Nao;
Kc   = concKo;
Ca_c = Cao;

CaB_MAX = 0.08;


Vol_up  = 0.007938;           /* mm3/cm2 */

O_TMgC_init  = 0.196085;
O_TMgMg_init  = 0.709417;
F1_init      = 0.428396;
F2_init      = 0.0028005;
Ca_d_init    = 7.24947e-05;
Ca_up_init   = 0.664564;
Ca_rel_init  = 0.646458;
Cai_init     = 6.72905e-05;
V_init       = -74.2525;
/* ------------------------------------------------------------------ */
/* Calcium buffers                                                    */
/* ------------------------------------------------------------------ */

/* ------------------------------------------------------------------ */
/* Voltage-dependent state functions                                  */
/* ------------------------------------------------------------------ */

E_K = R * T / F * log(Kc / Ki);
E_Na = R * T / F * log(Nac / Nai);
E_Ca = R * T / (2.0 * F) * log(Ca_c / Cai);

///////////////////////////////////////////////////////////////////////

V_half_m    = -27.12;
Km         = -8.21;
V_half_h    = -63.6;
Kh         = 5.3;

m_inf = 1.0 /(1.0 + exp((V - V_half_m) / Km));
tau_m = (4.2e-2 * exp(-pow((V + 25.57)/28.8, 2)) + 2.4e-2);
h1_inf =  1.0 /(1.0 + exp((V - V_half_h) / Kh));
tau_h1 = (30.0 / (1.0 + exp((V + 35.1)/3.2)) + 0.3);
h2_inf = h1_inf;
tau_h2 = (120.0 / (1.0 + exp((V + 35.1)/3.2)) + 3.0);

P_Na        = 3.2e-5;
INaterm = P_Na * Nac * V *
  F*F / (R * T) * (exp((V - E_Na) * F / (R * T)) - 1.0) /
  (exp(V * F / (R * T)) - 1.0);  
if (V == 0) {
  INaterm = P_Na * Nac * F * (exp((V - E_Na) * F / (R * T)) - 1.0);
}
INa = m*m*m * (0.9*h1+0.1*h2) * INaterm;

//////////////////////////////////////////////////////////////////

V_half_d_L  = -9.0;
Kd_L       = -5.8;
V_half_f_L  = -27.4;
Kf_L       = 7.1;

d_L_inf = 1.0 / (1.0 + exp(( V - V_half_d_L ) / Kd_L ));
tau_d_L = (2.7*exp(-pow(( V + 35.0)/30.0, 2)) + 2.0);

f_L1_inf = 1.0 / (1.0 + exp(( V - V_half_f_L) / Kf_L ));
tau_f_L1 = (161.0 * exp(-1.0 * pow(( V + 40.0)/14.4,2)) + 10.0);

f_L2_inf = 1.0 / (1.0 + exp((V - V_half_f_L) / Kf_L ));
tau_f_L2 = (1332.3 * exp(-1.0 * pow((V + 40.0)/14.2,2)) + 62.6);

f_Ca = Ca_d/(Ca_d + 0.025);
GCaL       = 0.135;
ICaL_term = GCaL * (V - Eca_app);
ICaL  = d_L * (f_Ca*f_L1 + ((1.0 - f_Ca) * f_L2)) * ICaL_term;

////////////////////////////////////////////////////////////////

GAMMA   = 0.45; 
d_NaCa  = 0.0003;
KNaCa  = 7.49684e-04;

INaCa_exp1_term = exp(GAMMA * V * F / (R * T));
INaCa_exp2_term = exp((GAMMA - 1.0) * V * F / (R * T));
INaCa_frc1_term = KNaCa * (pow(Nai,3) * Ca_c) /
  (1.0 + d_NaCa * (pow(Nac,3) * Cai + pow(Nai,3) * Ca_c));
INaCa_frc2_term = KNaCa * (pow(Nac,3) * Cai) /
  (1.0 + d_NaCa * (pow(Nac,3) * Cai + pow(Nai,3) * Ca_c));
INaCa = INaCa_frc1_term*INaCa_exp1_term -
  INaCa_frc2_term*INaCa_exp2_term;

////////////////////////////////////////////////////////////////
V_half_r    = 1.0;
Kr         = -11.0;
V_half_s    = -40.5;
Ks         = 11.5;

r_inf = 1.0 / (1.0 + exp((V - V_half_r) / Kr));
tau_r = (3.5*exp(-pow(V / 30.0,2)) + 1.5);

s_inf = 1.0 / (1.0 + exp((V - V_half_s) / Ks));
tau_s = (481.2 * exp(-pow((V + 52.45) / 14.97,2)) + 14.14);

Gt         = 0.15;
It_term = Gt * (V - E_K);
It = r*s*It_term;

////////////////////////////////////////////////////////////////

GCaB       = 1.57362e-3;
IbCa  = GCaB * (V - E_Ca);

////////////////////////////////////////////////////////////////

ICaP_ss= 0.08;               /* nS      */
ICaP = ICaP_ss * (Cai / (Cai + 0.0002));

///////////////////////////////////////////////////////////////
V_half_r_sus= -4.3;
Kr_sus     = -8.0;
V_half_s_sus= -20.0;
Ks_sus     = 10.0;

tau_r_sus = (9.0 / (1.0 + exp((V + 5.0)/12.0)) + 0.5);
r_sus_inf = 1.0 / (1.0 + exp((V - V_half_r_sus) / Kr_sus ));

s_sus_init = 0.991169;
tau_s_sus = (47.0 / (1.0 + exp((V + 60.0) / 10.0)) + 300.0);
s_sus_inf = 0.4 / (1.0 + exp((V - V_half_s_sus) / Ks_sus)) + 0.6;

Gsus       = 0.055;
Isus_term = Gsus * (V - E_K);
Isus = r_sus * s_sus * Isus_term;

///////////////////////////////////////////////////////////
V_half_n    = 19.9;
Kn         = -12.7;

n_init = 0.00483573;
tau_n = (700.0 + 400.0 * exp(-pow(( V - 20.0)/20.0,2)));
n_inf = 1.0 / (1.0 + exp((V - V_half_n) / Kn));

GKS        = 0.02;
IKs_term = GKS * (V - E_K);
IKs = n * IKs_term;

///////////////////////////////////////////////////////////
V_half_p_a  = -15.0;
Kp_a       = -6.0;
V_half_p_i  = -55.0;
Kp_i       = 24.0;


tau_p_a = (31.18 + 217.18 * exp(-pow(( V + 20.1376)/22.1996,2)));
p_a_inf = 1.0 / (1.0 + exp((V - V_half_p_a) / Kp_a));
p_i = 1.0 / (1.0 + exp((V - V_half_p_i) / Kp_i));

GKr        = 0.01;
IKr_term = GKr * p_i * (V - E_K);
IKr = p_a * IKr_term;

////////////////////////////////////////////////////////////

GK1        = 0.06;
IK1 = GK1 * pow(Kc,0.4457) * (V - E_K) /
  (1.0 + exp(1.5 * (V - E_K + 3.6) * F / (R * T)));

////////////////////////////////////////////////////////////

GNaB       = 1.21198e-3;
IBNa = GNaB * (V - E_Na);

/////////////////////////////////////////////////////////////

maxINaK    = 1.416506;
INaK = maxINaK * Kc / (Kc + 1.0) *
      pow(Nai,1.5) / (pow(Nai,1.5) +36.482873) *
      (V + 150.0) / (V + 200.0);

////////////////////////////////////////////////////////////

ICa = -2.0*INaCa + IbCa + ICaP;

///////////////////////////////////////////////////////////

Krel_i = 0.0003;             /* mM      */
Krel_d = 0.003;              /* mM      */
r_inact = 0.001*(33.96 + 339.6*pow(Cai / (Cai + Krel_i),4));
r_act1 = 0.001*(203.8*pow(Cai /(Cai + Krel_i),4));
r_act2 = 0.001*(203.8*pow(Ca_d /(Ca_d + Krel_d),4));

R_RECOV = 0.815e-3;           /* ms^-1   */
F_temp = (r_act1 + r_act2) * F1;
diff_F1 = R_RECOV * (1.0 - F1 - F2) - F_temp;
diff_F2 = F_temp - r_inact * F2;

alpha_rel   = 4000.0;
C_rel = alpha_rel * pow(F2 / (F2 + 0.25),2);
Irel = C_rel * (Ca_rel - Cai);

////////////////////////////////////////////////////////////////////
Kcyca  = 0.0003;             /* mM      */
Kxcs   = 0.4; 
Ksrca  = 0.5;                /* mM      */
Kxcs_srca   = 0.00024;       /* Kxcs*Kcyca/Ksrca */
Kxcs2_srca  = 9.6e-5;        /* Kxcs*Kxcs/Ksrca   */
IUP_SS     = 56.0;

Iup  = IUP_SS * (Cai - Kxcs2_srca*Ca_up) /
           ( Cai + Kcyca + Kxcs_srca*(Ca_up + Ksrca) );

////////////////////////////////////////////////////////
Vol_rel = 0.000882;           /* mm3/cm2 */
tau_tr  = 10.0;               /* ms      */
C_ITR     = 2.0*F*Vol_rel/tau_tr;
Itr  = (Ca_up - Ca_rel) * C_ITR;

//////////////////////////////////////////////////////
Vol_d   = 0.11768*DOM_FRAC;   /* mm3/cm2 */
tau_di  = 10.0;               /* ms      */
C_IDI     = 2.0*F*Vol_d/tau_di;

Idi = (Ca_d - Cai) * C_IDI;

///////////////////////////////////////////////////////

O_C;
tau_O_C = 1/(200.*Cai+0.476);
O_C_inf = 200.*Cai*tau_O_C;

O_TC;
tau_O_TC = 1/(78.4*Cai+0.392);
O_TC_inf = 78.4*Cai*tau_O_TC;

O_Calse;
tau_O_Calse = 1/(0.48*Ca_rel+0.4);
O_Calse_inf = 0.48*Ca_rel*tau_O_Calse;

diff_O_TMgC = 200.*Cai*(1-O_TMgC-O_TMgMg)-0.0066*O_TMgC;
diff_O_TMgMg = 2.*MGi*(1-O_TMgC-O_TMgMg)-0.66*O_TMgMg;

odot = 0.045*diff_O_C + 0.08*diff_O_TC + 0.16*diff_O_TMgC;
diff_Cai = -(-Idi+ICa+Iup - Irel)/(2.0*Voli*F) - odot;
diff_Ca_d = -(ICaL + Idi)/(2.0*Vol_d*F);

ocalsedot = 31.0*diff_O_Calse;
diff_Ca_rel = (Itr - Irel)/(2.0*Vol_rel*F) - ocalsedot;
diff_Ca_up  = (Iup - Itr )/(2.0*F*Vol_up);

/*
extern_Cab == 0;
if ( externCab == 1 ) {
  sv->O_TC    = sv->Ca_b/CaB_MAX;
  dO_TC = 0;
} else {
  sv->O_TC += (dO_TC = A_O_TC(cairow) + ( B_O_TC(cairow) - 1. ) * sv->O_TC);
}

*/

////////////////////////////////////////////////////////

Iion = INaK + IBNa + IK1 + IKr + IKs + Isus + ICaP + IbCa + It + INaCa + ICaL + INa;
