// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "InternalLinkagePass.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/Regex.h"
#include "llvm/Support/raw_ostream.h"

#define DEBUG_TYPE "add-internal-linkage"

using namespace llvm;

bool requireInternalLinkage(StringRef FunctionName) {
  llvm::Regex RegexStr("(load|store)_(consecutive_elements|single_value_struct|"
                       "struct|gather|scatter|conditional|consecutive_conditional).+(vector_)");
  if (RegexStr.match(FunctionName) ||
      FunctionName.startswith("LUT_interpRow_n_elements_vector"))
    return true;
  return false;
}

void changeLinkage(Function &F) {
  F.setLinkage(llvm::GlobalValue::InternalLinkage);
}

PreservedAnalyses InternalLinkagePass::run(Module &M,
                                           ModuleAnalysisManager &AM) {

  for (Function &F : M) {
    if (requireInternalLinkage(F.getName()))
      changeLinkage(F);
  }

  return PreservedAnalyses::all();
}

//-----------------------------------------------------------------------------
// New PM Registration
//-----------------------------------------------------------------------------
llvm::PassPluginLibraryInfo getInternalLinkagePassPluginInfo() {
  return {LLVM_PLUGIN_API_VERSION, "AddInternalLinkage", LLVM_VERSION_STRING,
          [](PassBuilder &PB) {
            PB.registerPipelineParsingCallback(
                [](StringRef Name, ModulePassManager &MPM,
                   ArrayRef<PassBuilder::PipelineElement>) {
                  if (Name == DEBUG_TYPE) {
                    MPM.addPass(InternalLinkagePass());
                    return true;
                  }
                  return false;
                });
          }};
}

// This is the core interface for pass plugins. It guarantees that 'opt' will
// be able to recognize InternalLinkagePass when added to the pass pipeline on
// the command line, i.e. via '-passes=add-internal-linkage'
extern "C" LLVM_ATTRIBUTE_WEAK ::llvm::PassPluginLibraryInfo
llvmGetPassPluginInfo() {
  return getInternalLinkagePassPluginInfo();
}
