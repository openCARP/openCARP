// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/Math/IR/Math.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "convert-fma-to-fmul-fadd"

using namespace mlir;
using namespace mlir::vector;

namespace {
struct ConvertFmaToFMulFAdd : ConvertFmaToFMulFAddBase<ConvertFmaToFMulFAdd> {
  void runOnOperation() override;
};
} // namespace

static std::pair<bool, Value> hasOneUseFMulOp(Operation *fmaOp, Value op0,
                                              Value op1) {

  auto users = op0.getUsers();

  auto mulOp = llvm::find_if(users, [&](Operation *user) {
    if (fmaOp->getBlock() != user->getBlock())
      return false;
    return isa<arith::MulFOp>(user) && user->isBeforeInBlock(fmaOp) &&
           (user->getOperand(0) == op0 && user->getOperand(1) == op1);
  });

  if (mulOp == users.end())
    return {true, {}};
  return {mulOp->hasOneUse(), (*mulOp)->getResult(0)};
}

struct FmaToFMulFAddPass : OpRewritePattern<math::FmaOp> {

  using OpRewritePattern<math::FmaOp>::OpRewritePattern;

  LogicalResult matchAndRewrite(math::FmaOp fmaOp,
                                PatternRewriter &rewriter) const final {

    Value op0 = fmaOp.getOperand(0);
    Value op1 = fmaOp.getOperand(1);

    // Check if the multiplication from FMAOp has one single use
    bool isSingleUse;
    Value mulOp;
    std::tie(isSingleUse, mulOp) = hasOneUseFMulOp(fmaOp.getOperation(), op0, op1);

    // Leave if single use
    if (isSingleUse)
      return failure();

    // Set up the Insertion Point to before op
    rewriter.setInsertionPoint(fmaOp);

    // Create an AddFOp to replace the FMAOp
    auto addOp =
        rewriter.create<arith::AddFOp>(fmaOp.getLoc(), mulOp, fmaOp.getOperand(2));

    // And replace it
    fmaOp.replaceAllUsesWith(addOp.getResult());
    return success();
  }
};

void ConvertFmaToFMulFAdd::runOnOperation() {
  RewritePatternSet patterns(&getContext());
  patterns.insert<FmaToFMulFAddPass>(&getContext());
  (void)applyPatternsAndFoldGreedily(getOperation(), std::move(patterns));
}

std::unique_ptr<mlir::Pass> mlir::createConvertFmaToFMulFAddPass() {
  return std::make_unique<ConvertFmaToFMulFAdd>();
}
