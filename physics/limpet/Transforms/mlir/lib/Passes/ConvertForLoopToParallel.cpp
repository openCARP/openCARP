// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/SCF/IR/SCF.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "convert-forloop-to-wsloop"

using namespace mlir;

namespace {
struct ConvertForLoopToParallel : ConvertForLoopToParallelBase<ConvertForLoopToParallel> {
  void runOnOperation() override;
};
} // namespace

struct ForToParallelPass : OpRewritePattern<scf::ForOp> {

  using OpRewritePattern<scf::ForOp>::OpRewritePattern;

  LogicalResult matchAndRewrite(scf::ForOp loopOp,
                                PatternRewriter &rewriter) const final {
    auto parOp
    = rewriter.create<scf::ParallelOp>(loopOp.getLoc(), loopOp.getLowerBound(),
                                       loopOp.getUpperBound(), loopOp.getStep(),
                                       /*bodyBuilderFn=*/nullptr);

    //  Copy the body of the for op.
    rewriter.eraseBlock(parOp.getBody());
    rewriter.inlineRegionBefore(loopOp.getRegion(), parOp.getRegion(),
                                parOp.getRegion().end());

    loopOp.replaceAllUsesWith(parOp);
    return success();
  }
};

struct YieldToReducePass : OpRewritePattern<scf::YieldOp> {

  using OpRewritePattern<scf::YieldOp>::OpRewritePattern;

  LogicalResult matchAndRewrite(scf::YieldOp yieldOp,
                                PatternRewriter &rewriter) const final {
    if (yieldOp->getParentOp()->getName().getStringRef().str() == "scf.for")
      rewriter.replaceOpWithNewOp<scf::ReduceOp>(yieldOp, yieldOp.getOperands());

    return success();
  }
};

void ConvertForLoopToParallel::runOnOperation() {
  RewritePatternSet patterns(&getContext());
  patterns.insert<ForToParallelPass>(&getContext());
  patterns.insert<YieldToReducePass>(&getContext());
  (void)applyPatternsAndFoldGreedily(getOperation(), std::move(patterns));
}

std::unique_ptr<mlir::Pass> mlir::createConvertForLoopToParallelPass() {
  return std::make_unique<ConvertForLoopToParallel>();
}
