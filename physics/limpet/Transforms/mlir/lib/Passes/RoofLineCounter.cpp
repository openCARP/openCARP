// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Dialect/SCF/IR/SCF.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/Math/IR/Math.h"
#include "mlir/Dialect/Vector/IR/VectorOps.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "roofline-counter"

using namespace mlir;

namespace {
struct RoofLineCounter
  : RoofLineCounterBase<RoofLineCounter> {
  void runOnOperation() override;
};
} // namespace

void RoofLineCounter::runOnOperation() {

  auto operation = getOperation();
  if (!isa<func::FuncOp>(operation))
    return;
  auto f = cast<func::FuncOp>(operation);
  if (f.isDeclaration())
    return;

  f.walk([&](Operation *op) {

    auto parentOp = op->getParentOp();
    if (!isa<scf::ForOp>(parentOp))
      return;

    if (isa<func::CallOp>(op)) {
      LLVM_DEBUG(llvm::dbgs() << "Memory Operation: ");
      LLVM_DEBUG(op->dump());
      auto callOp = cast<func::CallOp>(op);
      auto calledFuncName = callOp.getCallee();
      if (calledFuncName.starts_with("LUT_interpRow"))
        ++numLUTCalls;
      else
        ++numMemOps;
    } else if (isa<arith::AddFOp, arith::SubFOp, arith::MulFOp,
                   arith::MulFOp, arith::DivFOp, arith::NegFOp,
                   arith::CmpFOp, arith::MaximumFOp, vector::FMAOp>(op)){
      LLVM_DEBUG(llvm::dbgs() << "Arith Operation: ");
      LLVM_DEBUG(op->dump());
      ++numArithOps;
    } else if (isa<math::AbsFOp, math::Atan2Op, math::AtanOp,
                   math::CeilOp, math::CopySignOp, math::CosOp,
                   math::ErfOp, math::Exp2Op, math::ExpM1Op, math::ExpOp,
                   math::FloorOp, math::FmaOp, math::Log10Op, math::Log1pOp,
                   math::Log2Op, math::LogOp, math::PowFOp, math::RsqrtOp,
                   math::SinOp, math::SqrtOp, math::TanhOp>(op)) {
      LLVM_DEBUG(llvm::dbgs() << "Math Operation: ");
      op->dump();
      ++numMathOps;
    }
  });
  llvm::errs() << "numArithOps: " << numArithOps << "\n";
  llvm::errs() << "numMemOps: " << numMemOps << "\n";
  llvm::errs() << "numMathOps: " << numMathOps << "\n";
  llvm::errs() << "numLUTCalls: " << numLUTCalls << "\n";
}

std::unique_ptr<mlir::Pass> mlir::createRoofLineCounterPass() {
  return std::make_unique<RoofLineCounter>();
}
