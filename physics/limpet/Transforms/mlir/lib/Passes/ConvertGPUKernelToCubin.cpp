// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------
//===-----  ConvertGPUKernelToCubin.cpp - Gpu kernel cubin lowering -------===//
//
// Originally part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
// Changes made to the original:
// Enables the use of helper functions inside the GPU kernel.
// Such functions come from file __CUDA_HELPER_FILE__ and are linked to each
// model before lowering to the kernel to cubin. 
// This version of the code is licensed under the openCARP APL.
//===----------------------------------------------------------------------===//

#include "mlir/ExecutionEngine/OptUtils.h"
#include "mlir/Dialect/GPU/Transforms/Passes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Target/LLVMIR/Dialect/NVVM/NVVMToLLVMIRTranslation.h"
#include "mlir/Target/LLVMIR/Dialect/GPU/GPUToLLVMIRTranslation.h"
#include "mlir/Target/LLVMIR/Export.h"
#include "mlir/Target/LLVMIR/Dialect/LLVMIR/LLVMToLLVMIRTranslation.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/MC/TargetRegistry.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/VirtualFileSystem.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/IPO/Internalize.h"
#include "Passes/Passes.h"
using namespace mlir;

#ifdef __CUDA_LIBDEVICE_PATH__
#include <cuda.h>
#include <optional>

static void emitCudaError(const llvm::Twine &expr, const char *buffer,
                          CUresult result, Location loc) {
  const char *error;
  cuGetErrorString(result, &error);
  
  emitError(loc, expr.concat(" failed with error code ")
                     .concat(llvm::Twine{error})
                     .concat("[")
                     .concat(buffer)
                     .concat("]"));
}

#define RETURN_ON_CUDA_ERROR(expr)                                             \
  do {                                                                         \
    if (auto status = (expr)) {                                                \
      emitCudaError(#expr, jitErrorBuffer, status, loc);                       \
      return {};                                                               \
    }                                                                          \
  } while (false)

namespace {
class SerializeToCubinPass
  : public PassWrapper<SerializeToCubinPass, gpu::SerializeToBlobPass> {
public:
  MLIR_DEFINE_EXPLICIT_INTERNAL_INLINE_TYPE_ID(SerializeToCubinPass);

  SerializeToCubinPass();
  SerializeToCubinPass(const SerializeToCubinPass &other)
    : PassWrapper<SerializeToCubinPass, gpu::SerializeToBlobPass>(other) {}
  
  StringRef getArgument() const final { return "gpu-to-cubin-opencarp"; }
  StringRef getDescription() const final {
    return "Lower GPU kernel function to CUBIN binary annotations";
  }

protected:
  Option<int> optLevel{
      *this, "opt-level-stc",
      llvm::cl::desc("Optimization level for NVVM compilation"),
      llvm::cl::init(3)};

  Option<std::string> cudaHelperFile{
      *this, "cuda-helper-file",
      llvm::cl::desc("Cuda helper functions file"),
      llvm::cl::init(__CUDA_HELPER_FILE__)};

  // Overload to allow linking in libdevice
  std::unique_ptr<llvm::Module>
  translateToLLVMIR(llvm::LLVMContext &llvmContext) override;

  /// Adds LLVM optimization passes
  LogicalResult optimizeLlvm(llvm::Module &llvmModule,
                             llvm::TargetMachine &targetMachine) override;

private:
  void getDependentDialects(DialectRegistry &registry) const override;

  std::optional<SmallVector<std::unique_ptr<llvm::Module>, 2>>
  loadLibraries(SmallVectorImpl<std::string> &libraries,
              llvm::LLVMContext &llvmContext);

  // Serializes PTX to CUBIN.
  std::unique_ptr<std::vector<char>>
  serializeISA(const std::string &isa) override;
};
} // namespace

// Sets the 'option' to 'value' unless it already has a value.
static void maybeSetOption(Pass::Option<std::string> &option,
                           const char *value) {
  if (!option.hasValue())
    option = value;
}

inline static const char* getCudaArchitecture() {
  return __CUDA_ARCHITECTURE__;
}

SerializeToCubinPass::SerializeToCubinPass() {
  maybeSetOption(this->triple, "nvptx64-nvidia-cuda");
  maybeSetOption(this->chip, getCudaArchitecture());
  maybeSetOption(this->features, "");
}

void SerializeToCubinPass::getDependentDialects(
    DialectRegistry &registry) const {
  mlir::registerGPUDialectTranslation(registry);
  mlir::registerLLVMDialectTranslation(registry);
  registerNVVMDialectTranslation(registry);
  gpu::SerializeToBlobPass::getDependentDialects(registry);
}

static std::string getLibDevicePath() {
  return __CUDA_LIBDEVICE_PATH__;
}

static std::string findLibDeviceLibrary(std::string &path) {

  std::error_code EC;
  for (llvm::sys::fs::directory_iterator I(path, EC), E; I != E;
    I.increment(EC)) {
    if (EC)
      return "libdevice.bc"; 
    StringRef fileName = I->path();
    // Process bitcode filename that looks like libdevice.XX.bc
    const StringRef libDeviceName = "libdevice.";
    if (fileName.contains(libDeviceName) && fileName.ends_with(".bc"))
      return fileName.str();
  }
  return "libdevice.bc";
}

std::optional<SmallVector<std::unique_ptr<llvm::Module>, 2>>
SerializeToCubinPass::loadLibraries(SmallVectorImpl<std::string> &libraries,
              llvm::LLVMContext &llvmContext) {
  
  SmallVector<std::unique_ptr<llvm::Module>, 2> libModules;

  for (std::string library : libraries) {
    llvm::SMDiagnostic error;
    std::unique_ptr<llvm::Module> libModule =
        llvm::getLazyIRFileModule(library, error, llvmContext);
    if (!libModule) {
      getOperation().emitError() << "Failed to load library " << library
                                 << error.getMessage();
      return std::nullopt;
    }
    libModules.push_back(std::move(libModule));
  }
  return libModules;
}

std::unique_ptr<llvm::Module>
SerializeToCubinPass::translateToLLVMIR(llvm::LLVMContext &llvmContext) {
// MLIR -> LLVM translation
  std::unique_ptr<llvm::Module> ret =
      gpu::SerializeToBlobPass::translateToLLVMIR(llvmContext);

  if (!ret) {
    getOperation().emitOpError("Module lowering failed");
    return ret;
  }

  bool needsLibDevice = false;
  for (llvm::Function &f : ret->functions()) {
    if (f.hasExternalLinkage() && f.hasName() && !f.hasExactDefinition()) {
      StringRef funcName = f.getName();
        if (funcName.starts_with("__nv_")) {
          needsLibDevice = true;

        }
    }
  }
  needsLibDevice = false; // wip libdevice is now handled by -gpu-to-nvptx mlir pass

  llvm::SmallVector<std::string, 2> libraries;
  if (needsLibDevice) {
    std::string libDevicePath = getLibDevicePath();
    libraries.push_back(findLibDeviceLibrary(libDevicePath));
  }

  libraries.push_back(this->cudaHelperFile.getValue());
  
  std::optional<SmallVector<std::unique_ptr<llvm::Module>, 2>> libModules;
  libModules = loadLibraries(libraries, llvmContext);
  if (!libModules) {
    getOperation()
            .emitWarning("Could not load required device libraries")
            .attachNote()
        << "This will probably cause link-time or run-time failures";
    return ret; // We can still abort here
  }


  llvm::Linker linker(*ret);
  for (std::unique_ptr<llvm::Module> &libModule : libModules.value()) {
    bool err = linker.linkInModule(
        std::move(libModule), llvm::Linker::Flags::LinkOnlyNeeded,
        [](llvm::Module &m, const StringSet<> &gvs) {
          llvm::internalizeModule(m, [&gvs](const llvm::GlobalValue &gv) {
            return !gv.hasName() || (gvs.count(gv.getName()) == 0);
          });
        });
    // True is linker failure
    if (err) {
      getOperation().emitError(
          "Unrecoverable failure during device library linking.");
      // We have no guaranties about the state of `ret`, so bail
      return nullptr;
    }
  }
 
  return ret;
}

LogicalResult
SerializeToCubinPass::optimizeLlvm(llvm::Module &llvmModule,
                                   llvm::TargetMachine &targetMachine) {
  int optLevel = this->optLevel.getValue();
  if (optLevel < 0 || optLevel > 3)
    return getOperation().emitError()
           << "Invalid HSA optimization level" << optLevel << "\n";

  targetMachine.setOptLevel(static_cast<llvm::CodeGenOptLevel>(static_cast<llvm::CodeGenOptLevel>(optLevel)));

  auto transformer =
      makeOptimizingTransformer(optLevel, /*sizeLevel=*/0, &targetMachine);
  auto error = transformer(&llvmModule);
  if (error) {
    InFlightDiagnostic mlirError = getOperation()->emitError();
    llvm::handleAllErrors(
        std::move(error), [&mlirError](const llvm::ErrorInfoBase &ei) {
          mlirError << "Could not optimize LLVM IR: " << ei.message() << "\n";
        });
    return mlirError;
  }

  // Strip debug information
  // This is necessary for PTX toolchains below 7.5 
  StripDebugInfo(llvmModule);
  return success();
}

std::unique_ptr<std::vector<char>>
SerializeToCubinPass::serializeISA(const std::string&isa) {
  Location loc = getOperation().getLoc();
  char jitErrorBuffer[4096] = {0};

  RETURN_ON_CUDA_ERROR(cuInit(0));

  // Linking requires a device context.
  CUdevice device;
  RETURN_ON_CUDA_ERROR(cuDeviceGet(&device, 0));
  CUcontext context;
  RETURN_ON_CUDA_ERROR(cuCtxCreate(&context, 0, device));
  CUlinkState linkState;
  
  CUjit_option jitOptions[] = {CU_JIT_ERROR_LOG_BUFFER,
                               CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES};
  void *jitOptionsVals[] = {jitErrorBuffer,
                            reinterpret_cast<void *>(sizeof(jitErrorBuffer))};

  RETURN_ON_CUDA_ERROR(cuLinkCreate(2,              /* number of jit options */
                                    jitOptions,     /* jit options */
                                    jitOptionsVals, /* jit option values */
                                    &linkState));

  auto kernelName = getOperation().getName().str();
  RETURN_ON_CUDA_ERROR(cuLinkAddData(
      linkState, CUjitInputType::CU_JIT_INPUT_PTX,
      const_cast<void *>(static_cast<const void *>(isa.c_str())), isa.length(),
      kernelName.c_str(), 0, /* number of jit options */
      nullptr,               /* jit options */
      nullptr                /* jit option values */
      ));

  void *cubinData;
  size_t cubinSize;
  RETURN_ON_CUDA_ERROR(cuLinkComplete(linkState, &cubinData, &cubinSize));

  char *cubinAsChar = static_cast<char *>(cubinData);
  auto result =
      std::make_unique<std::vector<char>>(cubinAsChar, cubinAsChar + cubinSize);

  // This will also destroy the cubin data.
  RETURN_ON_CUDA_ERROR(cuLinkDestroy(linkState));
  RETURN_ON_CUDA_ERROR(cuCtxDestroy(context));

  return result;
}

// Register pass to serialize GPU kernel functions to a CUBIN binary annotation.
void mlir::registeropenCARPGpuSerializeToCubinPass() {
  PassRegistration<SerializeToCubinPass> registerSerializeToCubin(
      [] {
        // Initialize LLVM NVPTX backend.
        LLVMInitializeNVPTXTarget();
        LLVMInitializeNVPTXTargetInfo();
        LLVMInitializeNVPTXTargetMC();
        LLVMInitializeNVPTXAsmPrinter();

        return std::make_unique<SerializeToCubinPass>();
      });
}

#else // CUDA IS DISABLED
// Register pass to serialize GPU kernel functions to a CUBIN binary annotation.
void mlir::registeropenCARPGpuSerializeToCubinPass() {}
#endif

