// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/Arith/IR/Arith.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "mlir/Conversion/LLVMCommon/ConversionTarget.h"

#include "Passes/OpToFuncCallLowering.h"

#define DEBUG_TYPE "convert-max-to-nv-max"

using namespace mlir;
using namespace mlir::vector;

namespace {
struct ConvertMaxToRocdlMax : ConvertMaxToRocdlMaxBase<ConvertMaxToRocdlMax> {
  void runOnOperation() override;
};
} // namespace

void ConvertMaxToRocdlMax::runOnOperation() {
  LLVMTypeConverter converter(&getContext());
  RewritePatternSet patterns(&getContext());

  patterns.add<OpToFuncCallLowering<arith::MaximumFOp>>(converter, "__ocml_max_f32", "__ocml_max_f64");

  LLVMConversionTarget target(getContext());
  if (failed(applyPartialConversion(getOperation(), target, std::move(patterns))))
    signalPassFailure();
}

std::unique_ptr<mlir::Pass> mlir::createConvertMaxToRocdlMaxPass() {
  return std::make_unique<ConvertMaxToRocdlMax>();
}
