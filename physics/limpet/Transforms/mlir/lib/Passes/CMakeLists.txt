# always build a static library for the MLIR passes:
set(BUILD_SHARED_LIBS OFF)

add_mlir_dialect_library(MLIROpenCARPPass
  ConvertFMAToFMulFAdd.cpp
  ConvertMaxToNvMax.cpp
  ConvertMaxToRocdlMax.cpp
  ConvertForLoopToParallel.cpp
  EliminateImplicitBarrier.cpp
  RoofLineCounter.cpp
  ConvertForLoopToAffineFor.cpp
  ConvertGPUKernelToCubin.cpp
  ConvertGPUKernelToHsaco.cpp
  AddrSpaceAllocaOpLowering.cpp

  ADDITIONAL_HEADER_DIRS
  ${MLIR_OPENCARP_SOURCE_DIR}/include/Passes
  
  DEPENDS
  MLIROpenCARPPassIncGen

  LINK_COMPONENTS
  Core
  IRReader

  LINK_LIBS PUBLIC
  MLIRIR
  MLIRPass
  MLIRTransformUtils
  LLVMLinker
  )

if (NOT MLIR_CUDA_DISABLED)
    target_compile_definitions(obj.MLIROpenCARPPass
      PUBLIC
      __CUDA_LIBDEVICE_PATH__="${CUDA_LIBDEVICE_PATH}"
      __CUDA_ARCHITECTURE__="${CUDA_ARCHITECTURE}"
      __CUDA_HELPER_FILE__="${CUDA_HELPER_FILE}")
endif()
if (NOT MLIR_ROCM_DISABLED)
    target_compile_definitions(obj.MLIROpenCARPPass
        PUBLIC
        MLIR_GPU_TO_HSACO_PASS_ENABLE=1
        __DEFAULT_ROCM_PATH__="${ROCM_PATH}"
        __HIP_HELPER_FILE__="${HIP_HELPER_FILE}"
        )
    target_link_libraries(MLIROpenCARPPass
        PRIVATE
        MLIRExecutionEngine
        MLIRROCDLToLLVMIRTranslation
        )
endif()

target_compile_features(MLIROpenCARPPass PUBLIC cxx_std_17)

# Visual Studio or Xcode requires more work
# Set up location to be the same as other generator
if (CMAKE_GENERATOR MATCHES "Visual Studio" OR CMAKE_GENERATOR STREQUAL Xcode)
    set_target_properties(MLIROpenCARPPass PROPERTIES
      LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CMAKE_CURRENT_BINARY_DIR}
      LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_BINARY_DIR})
endif()
