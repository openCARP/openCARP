// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file electric_integrators.cc
* @brief FEM integrators fo electrics.
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#ifndef _ELEC_INTEGRATORS_H
#define _ELEC_INTEGRATORS_H

#include "SF_base.h"
#include "basics.h"
#include "fem_types.h"

namespace opencarp {

/// maximum supported number of integration points
#define EP_MAX_IPOINTS 128
/// maximum supported number of points in a local element
#define EP_MAX_LPOINTS 8

class elec_stiffness_integrator : public SF::matrix_integrator<mesh_int_t,mesh_real_t>
{
  MaterialType & material;   ///< Reference to material type struct.

  SF::dmat<double> cond;          ///< Conductivity tensor
  SF::dmat<double> shape;         ///< buffer for local element shape functions and derivatives
  SF::dmat<double> gshape;        ///< buffer for global shape function and derivatives.
  double J[9];                    ///< buffer for Jacobian mapping shape -> gshape
  SF::Point lpts[EP_MAX_LPOINTS]; ///< the local element points

  int p_order = 1;  ///< Ansatz function order

  SF::Point           ipts[EP_MAX_IPOINTS];    ///< integration points
  double              w   [EP_MAX_IPOINTS];    ///< integration weights

  public:
  elec_stiffness_integrator(MaterialType & inp_mat) : material(inp_mat)
  {}

  void operator() (const SF::element_view<mesh_int_t,mesh_real_t> & elem, SF::dmat<double> & buff);
  void dpn(mesh_int_t & row_dpn, mesh_int_t & col_dpn);
};

class mass_integrator : public SF::matrix_integrator<mesh_int_t,mesh_real_t>
{
  SF::dmat<double> shape;         ///< buffer for local element shape functions and derivatives
  double J[9];                    ///< buffer for Jacobian mapping shape -> gshape
  SF::Point lpts[EP_MAX_LPOINTS]; ///< the local element points

  int p_order = 1;  ///< Ansatz function order

  SF::Point           ipts[EP_MAX_IPOINTS];    ///< integration points
  double              w   [EP_MAX_IPOINTS];    ///< integration weights

  public:
  void operator() (const SF::element_view<mesh_int_t,mesh_real_t> & elem, SF::dmat<double> & buff);
  void dpn(mesh_int_t & row_dpn, mesh_int_t & col_dpn);
};

}  // namespace opencarp

#endif
