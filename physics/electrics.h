// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file electrics.h
* @brief Tissue level electrics, main Electrics physics class.
* @author Aurel Neic, Gernot Plank, Edward Vigmond
* @version
* @date 2019-10-25
*/

#ifndef _ELECTRICS_H
#define _ELECTRICS_H

#include "physics_types.h"
#include "sim_utils.h"
#include "stimulate.h"

#include "sf_interface.h"
#include "timers.h"
#include "ionics.h"

namespace opencarp {

class elliptic_solver
{
  public:
  sf_vec* phie       = nullptr; //!< phi_e
  sf_vec* phie_i     = nullptr; //!< phi_e on intracellular grid
  sf_vec* phiesrc    = nullptr; //!< I_e
  sf_vec* currtmp    = nullptr; //!< temp vector for phiesrc

  sf_mat* mass_e     = nullptr; //!< mass matrix for RHS elliptic calc
  sf_mat* phie_mat   = nullptr; //!< lhs matrix to solve elliptic

  // the linear solver
  sf_sol* lin_solver = nullptr; //!< petsc or ginkgo lin_solver

  // linear solver stats
  lin_solver_stats stats;

  /// dbcs require a dbc manager
  dbc_manager* dbc                    = nullptr;
  bool         phie_mat_has_nullspace = false;

  // solver config
  double       tol            = 1e-8;                 //!< CG stopping tolerance
  int          max_it         = 100;                  //!< maximum number of iterations

  void init();
  void rebuild_matrices(MaterialType* mtype, SF::vector<stimulus> & stimuli, FILE_SPEC logger);
  void rebuild_stiffness(MaterialType* mtype, SF::vector<stimulus> & stimuli, FILE_SPEC logger);
  void rebuild_mass(FILE_SPEC logger);
  void solve(sf_mat & Ki, sf_vec & Vmv, sf_vec & tmp_i);
  void solve_laplace();

  ~elliptic_solver()
  {
    if(dbc) delete dbc;
    if(lin_solver) delete lin_solver;
    // matrices
    if(phie_mat) delete phie_mat;
    if(mass_e) delete mass_e;
    // vectors
    if(phie) delete phie;
    if(phie_i) delete phie_i;
    if(phiesrc) delete phiesrc;
    if(currtmp) delete currtmp;
  }

  private:
  void enforce_dbc();
  void setup_linear_solver(FILE_SPEC logger);
};

class parabolic_solver
{
  public:

  // Has to be kept in line with param_globals::parab_solve
  enum parabolic_t { EXPLICIT = 0, CN = 1, O2dT = 2};

  sf_vec* IIon           = nullptr; //!< ionic currents
  sf_vec* Vmv            = nullptr; //!< global Vm vector

  sf_vec* old_vm         = nullptr; //!< older Vm needed for 2nd order dT
  sf_vec* kappa_i        = nullptr; //!< scaling vector for intracellular mass matrix, \b M
  sf_vec* tmp_i1         = nullptr; //!< scratch vector for i-grid
  sf_vec* tmp_i2         = nullptr; //!< scratch vector for i-grid
  sf_vec* Irhs           = nullptr; //!< weighted transmembrane currents
  sf_vec* inv_mass_diag  = nullptr; //!< inverse diagonal of mass matrix, for EXPLICIT solving

  sf_mat* u_mass_i       = nullptr; //!< unscaled mass matrix, \b M
  sf_mat* mass_i         = nullptr; //!< lumped \f$\kappa M\f$ for parabolic problem
  sf_mat* rhs_parab      = nullptr; //!< rhs matrix to solve parabolic
  sf_mat* lhs_parab      = nullptr; //!< lhs matrix (CN) to solve parabolic
  sf_mat* phie_recov_mat = nullptr; //!< rhs for phie recovery with pseudo bidomain

  // the linear solver
  sf_sol* lin_solver     = nullptr; //!< petsc or ginkgo lin_solver

  // linear solver stats
  lin_solver_stats stats;

  // solver config
  double       tol               = 1e-8;                 //!< CG stopping tolerance
  int          max_it            = 100;                  //!< maximum number of iterations
  parabolic_t  parab_tech        = CN;                   //!< manner in which parabolic equations are solved

  // solver statistics
  double       final_residual    = -1.0;                 //!< Holds the residual after convergence
  int          niter             = -1;                   //!< number of iterations

  ~parabolic_solver()
  {
    if (lin_solver) delete lin_solver;
    // matrices
    if (u_mass_i) delete u_mass_i;
    if (mass_i) delete mass_i;
    if (rhs_parab) delete rhs_parab;
    if (lhs_parab) delete lhs_parab;
    if (phie_recov_mat) delete phie_recov_mat;
    // vectors
    // IIon and Vmv are shallow copies of global vectors, do not delete them
    if (old_vm) delete old_vm;
    if (kappa_i) delete kappa_i;
    if (tmp_i1) delete tmp_i1;
    if (tmp_i2) delete tmp_i2;
    if (Irhs) delete Irhs;
    if (inv_mass_diag) delete inv_mass_diag;
  }

  void init();
  void rebuild_matrices(MaterialType* mtype, limpet::MULTI_IF & miif, FILE_SPEC logger);
  void solve(sf_vec & phie_i);

  private:
  void setup_linear_solver(FILE_SPEC logger);

  void solve_CN(sf_vec & phie_i);
  void solve_O2dT(sf_vec & phie_i);
  void solve_EF(sf_vec & phie_i);
};

enum PotType {VM, PHIE};
enum ActMethod {NONE, ACT_THRESH, ACT_DT};

/// event detection data structures
struct Activation {
  ActMethod  method;              //!< method to check whether activation occured
  float      threshold;           //!< threshold for detection of activation
  int        mode;                //!< toggle mode from standard to reverse
  int        all;                 //!< determine all or first instants of activation
  int        init;                //!< true if intialized
  sf_vec*    phi       = nullptr; //!< signal
  sf_vec*    phip      = nullptr; //!< previous value of signal
  int       *ibuf      = nullptr; //!< buffer indices where activation occured
  double    *actbuf    = nullptr; //!< buffer activation times if method==all
  sf_vec*    tm        = nullptr; //!< activation times
  sf_vec*    dvp0      = nullptr; //!< additional vector for derivative
  sf_vec*    dvp1      = nullptr; //!< additional vector for derivative
  FILE_SPEC  fout;                //!< output file
  char      *fname     = nullptr; //!< output file name
  char      *ID        = nullptr; //!< ID used to name output file
  char      *prv_fname = nullptr; //!< file name of previous run when restarting
  PotType    measurand;           //!< quantity being monitored
  int        offset;              //!< node number offset (used for PS)
  int        nacts;               //!< number of events detected over time step
};

//! sentinel for checking activity in the tissue
struct Sentinel {
  bool           activated = false;  //!< flag sentinel activation
  double         t_start  = -1.0;    //!< start of observation window
  double         t_window = -1.0;    //!< duration of observation window
  double         t_quiesc = -1.0;    //!< measure current duration of quiescence
  int            ID = -1;            //!< ID of LAT detector used as sentinel
};

class LAT_detector
{
  private:
  /// check activation via the vm threshold cross criterion
  int check_cross_threshold(sf_vec & vm, sf_vec & vmp, double tm,
                            int *ibuf, double *actbuf, float threshold, int mode);

  /// check activation via the vm derivative threshold criterion
  int check_mx_derivative(sf_vec & vm, sf_vec & vmp, double tm,
                          int *ibuf, double *actbuf, sf_vec & dvp0, sf_vec & dvp1,
                          float threshold, int mode);

  public:
  SF::vector<Activation>        acts;
  SF::index_mapping<mesh_int_t> petsc_to_nodal;
  Sentinel sntl;

  /// constructor, sets up basic datastructs from global_params
  LAT_detector();

  /// initializes all datastructs after electric solver setup
  void init(sf_vec & vm, sf_vec & phie, int offset);

  /// check activations at sim time tm
  int check_acts(double tm);

  /// check for quiescence
  int check_quiescence(double tm, double dt);

  /// output one nodal vector of initial activation time
  void output_initial_activations();
};

struct phie_recovery_data
{
  SF::vector<mesh_real_t> pts; ///< The phie recovery locations
  sf_vec* phie_rec = nullptr;  ///< The phie recovery output vector buffer
  sf_vec* Im = nullptr;
  sf_vec* dphi = nullptr;      ///< Auxiliary vectors
  SF_real gBath;               ///< Bath conductivity
};

class Electrics : public Basic_physic
{
  public:
  /**
  * @brief An electrics grid identifier to distinguish between intra and extra grids.
  *
  * The indexing of the enum is used to access the MaterialType array.
  *
  */
  enum grid_t {intra_grid = 0, extra_grid};
  enum linsys_t {elliptic_sys, parabolic_sys};

  /// the material types of intra_grid and extra_grid grids.
  MaterialType mtype[2];
  /// the electrical stimuli
  SF::vector<stimulus> stimuli;

  /// The ionics physics is managed as a member of the Electrics. This allows for thighter
  /// coupling in the Electrics compute step.
  Ionics ion;

  /// Solver for the elliptic bidomain equation
  elliptic_solver  ellip_solver;
  /// Solver for the parabolic bidomain equation
  parabolic_solver parab_solver;

  /// the activation time detector
  LAT_detector lat;

  /// datastruct holding global IMP state variable output
  gvec_data gvec;

  /// class handling the igb output
  igb_output_manager output_manager;

  /// struct holding helper data for phie recovery
  phie_recovery_data phie_rcv;

  generic_timing_stats IO_stats;

  /**
  * @brief Most of the initialization is done with initialize()
  */
  Electrics() : ion(intra_elec_msh)
  {
    name = "Electrics";
  }

  /**
  * @brief Initialize the Electrics.
  *
  * This could also be a constructor. But it might be better to expicitely
  * call the initialization. This way we can instanciate electrics structs
  * without calling the full setup as well.
  *
  */
  void initialize();

  void destroy();

  // This funcs from the Basic_physic interface are currently empty
  void compute_step();
  void output_step();

  inline void output_timings()
  {
    // since ionics are included in electrics, we substract ionic timings from electric timings
    compute_time    -= ion.compute_time;
    initialize_time -= ion.initialize_time;

    // now we call the base class timings output for Electrics
    Basic_physic::output_timings();
    // and then for ionics
    ion.output_timings();
  }

  ~Electrics();

  /// figure out current value of a signal linked to a given timer
  double timer_val(const int timer_id);

  /// figure out units of a signal linked to a given timer
  std::string timer_unit(const int timer_id);


  private:

  /// set up the electrics stimuli
  void setup_stimuli();

  /// apply the intracellular stimuli
  void stimulate_intracellular();
  /// clamp Vm to a value, controled by associated stimulus, needs to be applied before
  /// ionic step and after parabolic step
  void clamp_Vm();

  /// apply the extracellular potential stimuli
  void stimulate_extracellular();

  /// set up the elliptic and parabolic pde solvers
  void setup_solvers();

  /// VecScatter setup
  void setup_mappings();

  /// do the setup for the output_step
  void setup_output();

  /// dump the linear_solver matrices of ellip_solver and parab_solver
  void dump_matrices();

  void checkpointing();

  void balance_electrodes();

  void prepace();
};

/**
 * @brief Fill the RegionSpec of an electrics grid with the associated inputs
 *        from the param parameters.
 *
 * @param g  The electrics grid we want to set up.
 */
void set_elec_tissue_properties(MaterialType* mtype, Electrics::grid_t g, FILE_SPEC logger);

/// return wheter any stimuli require dirichlet boundary conditions
bool have_dbc_stims(const SF::vector<stimulus> & stimuli);

/// figure out stimulus index linked to a given timer
int stimidx_from_timeridx(const SF::vector<stimulus> & stimuli, const int timer_id);

void setup_phie_recovery_data(phie_recovery_data & data);
void recover_phie_std(sf_vec & vm, phie_recovery_data & rcv);
int  postproc_recover_phie();

class Laplace : public Basic_physic
{
  public:
  /// the material types of intra_grid and extra_grid grids.
  MaterialType mtype[2];
  /// the electrical stimuli
  SF::vector<stimulus> stimuli;

  /// Solver for the elliptic bidomain equation
  elliptic_solver  ellip_solver;
  /// class handling the igb output
  igb_output_manager output_manager;

  Laplace()
  {
    name = "Laplace solver";
  }

  void initialize();
  void destroy();
  void compute_step();
  void output_step();
  /// figure out current value of a signal linked to a given timer
  double timer_val(const int timer_id);
  /// figure out units of a signal linked to a given timer
  std::string timer_unit(const int timer_id);
};

/** scale current densities to keep total injected current constant
 *
 * \param [in] stimuli The stimuli vector.
 * \param [in] mass_i  intracellular mass matrix, \f$M_i\f$
 * \param [in] mass_e  extracellular mass matrix, \f$M_e\f$
 * \param [in] miif    ionic interface
 * \param [in] logger  handle to output log
 *
 * \note We assume that the strength is in uA
 */
void constant_total_stimulus_current(SF::vector<stimulus> & simuli,
                                     sf_mat & mass_i,
                                     sf_mat & mass_e,
                                     limpet::MULTI_IF *miif,
                                     FILE_SPEC logger);

void balance_electrode(SF::vector<stimulus> & stimuli, int balance_from, int balance_to);

void apply_stim_to_vector(const stimulus & s, sf_vec & vec, bool add);

void set_cond_type(MaterialType & m, cond_t type);

}  // namespace opencarp

#endif
