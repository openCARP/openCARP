// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------

#include "Filter.h"

/** make a Butterworth filter
 *
 * \param n     filter order
 * \param low   normalized lower frequency cutoff
 * \param upper normalized upper frequency cutoff
 *
 * \note a low pass filter is made if \p low ==0, a high pass 
 *       is \p high is 0 and a band pass otherwise
 *
 */
Butterworth::Butterworth( int order, double low, double upper)
{
  if( !low && !upper ) {
    fprintf( stderr, "Must specify at least one frequency\n" );
    return;
  }

  if( !low ) {
   _n = order+1;
   _c = ccof_bwlp( order );
   _d = dcof_bwlp( order, upper );
   _sf = sf_bwlp( order, upper );
  } else if( !upper ) {
   _n = order+1;
   _c = ccof_bwhp( order );
   _d = dcof_bwhp( order, low );
   _sf = sf_bwhp( order, low );
  } else {
   _n = 2*order+1;
   _c = ccof_bwbp( order );
   _d = dcof_bwbp( order, low, upper );
   _sf = sf_bwbp( order, low, upper );
  }
}

void zero_avg( double *x, int n )
{ 
  double avg=0;
  for( int i=0;i<n;i++ )
    avg += x[i];
  avg /= (double)n;
  for( int i=0;i<n;i++ )
    x[i] -= avg;
}


/** apply filter to time series
 *
 * \param in    time series
 * \param nr    number of time traces
 * \param filt  the filter
 * \param t     number of time samples
 *
 */
void filter_all( double **in, int nr, IIR_Filt *filt, double *out, int t )
{
  for( int i=0; i<nr; i++ )  {
     filt->apply( in[i], out, t );
     memcpy( in[i], out, t*sizeof(double) );
  }
}


