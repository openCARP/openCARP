/*
 *                            COPYRIGHT
 *
 *  liir - Recursive digital filter functions
 *  Copyright (C) 2007 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  stefan(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#ifdef __cplusplus
extern "C"
{
#endif

long double  *binomial_mult( int n, long double  *p );
long double  *trinomial_mult( int n, long double  *b, long double  *c );

long double  *dcof_bwlp( int n, long double  fcf );
long double  *dcof_bwhp( int n, long double  fcf );
long double  *dcof_bwbp( int n, long double  f1f, long double  f2f );
long double  *dcof_bwbs( int n, long double  f1f, long double  f2f );

int *ccof_bwlp( int n );
int *ccof_bwhp( int n );
int *ccof_bwbp( int n );
long double  *ccof_bwbs( int n, long double  f1f, long double  f2f );

long double  sf_bwlp( int n, long double  fcf );
long double  sf_bwhp( int n, long double  fcf );
long double  sf_bwbp( int n, long double  f1f, long double  f2f );
long double  sf_bwbs( int n, long double  f1f, long double  f2f );

#ifdef __cplusplus
}
#endif

