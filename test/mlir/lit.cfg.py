import os
import platform
import lit.formats
import lit.util

config.name = 'opencarp-mlir'
config.test_format = lit.formats.ShTest(True)

config.suffixes = ['.test']

path = config.environment['PATH']
path = os.path.pathsep.join((config.opencarp_bin_root, path))
config.environment['PATH'] = path

config.test_source_root = os.path.dirname(__file__)
config.test_exec_root = os.path.join(config.bench_obj_root, 'test')

config.substitutions.append(('%bench-test',
    os.path.join(config.bench_obj_root, 'bench-test')))

config.substitutions.append(('%filecheck', config.filecheck))
