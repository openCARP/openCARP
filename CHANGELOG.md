# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
openCARP uses a vMAJOR.PATCH versioning scheme. We increase the
* MAJOR version when we add functionality, and the
* PATCH version when we make backwards compatible bug fixes

## Unreleased
### Added
- AC-cAMP-PKA signalling cascade based on Behar et al. (2016) to Severi and Fabbri sinus node models (thanks @moritz.linder)
- Better support for MLIR builds on ARM architectures including the SLEEF vector math library (thanks @vincent.loechner)
- Support for distributed memory Ginkgo numerical backend
- Documentation of the .bin format

### Changed
- Set BUILD_SHARED_LIBS to OFF for building the MLIR Passes lib.
- openCARP Docker image is now based on Ubuntu 22.04, and uses PETSc version 3.22.3.
- The regular `deb` package is no longer compatible with Ubuntu 20.04 and Debian 11. A supplementary package tagged `ubuntu20` is now generated for these OS.
- Use compilation optimization `-O3` in docker openCARP build
- MLIR build instructions now based on LLVM 18, added MacOS specific MLIR build instructions (including a patch to LLVM 18)
- MLIR vector size (and loop unroll factor, data layout inner array size) is no longer an environment variable (MLIR_NUM_ELEMENTS) but the CMake option -DVECTOR_WIDTH=[64|128|256|512], and is auto-detected on the configuration cpu architecture by default, if not specified
- Ginkgo backend:
  - uses its `develop` branch
  - Ginkgo is built automatically in the openCARP build process if required but not avaialble  
  - Ginkgo matrices are adjusted to support rectangular matrices

### Fixed
- ISO-related concentration responses in sinus node cell models of Fabbri (2017) and Severi (2012) et al. (thanks @moritz.linder)
- Initialization of old_vm and linear_solver in case of 2nd order time integration (@TobiasG)
- A number of details in the interface to Ginkgo (@fritzgoebel)
- Clean OpenMP invocation when cmake flag USE_OPENMP is on
- Update test_integrators to correct syntax (@TobiasG)


## [17.0] - 2024-12-21
### Added
- Sinus node cell models of Fabbri (2017) and Severi (2012) et al. + AC-cAMP-PKA signalling cascade based on Behar et al. (2016) (thanks @moritz.linder)
- Update to new stimulus definition (experiments, regression tests and carputils functions) (thanks @moritz.linder)
- openCARP Docker image is now built for architectures `arm64` and `amd64` 
- New operations for igbops (`intX_t` and `t_maxX`)
- Bipole mode for igbapd
- igbtools can be piped together (reading/writing stdin and stdout)
- Documentation about running openCARP simulations on HPC systems in user manual (thanks @m.houillon)
- Three onboarding tutorials for single cell using openCARP JupyterLab included
- Visualisation Example using ParaView (follow up of openCARP User Meeting, thanks @moritz.linder, @christian.goetz, @csales, @hermenegild, @pascal.maierhofer)

### Changed
- Simplified the petsc interface (#352)
- Reduced Docker image size by 50% (!242, thanks @m.houillon)
- Remove declaration-only header files from CMakeLists
- Use current meshtool version
- Time integration for tension models (#357)
- Moved MLIR test jobs to CI `test` stage
- Use regression test references specified in git submodule also for Ginkgo and MLIR (!263)
- No more automatic updates of IMP sources in master branch, only warnings

### Fixed
- The deps Docker image was never updated (!242)
- Surface mesh creation for hexahedral to quad elements (!257)
- Rare bug when reading multiple state variable files (!259)
- Support for EasyML `heav()` and `sign()` with MLIR (!260)

## [16.0] - 2024-08-19
### Added
- Pig model based on Gaur & Vigmond et al. (2021) (thanks @moritz.linder)
- Support for `max` function in MLIR builds
- Support for SUNDIALS v6 and v7
- Update C++ standard to 17
- Update LLVM to 18
- MLIR noground_bidom benchmarks (thanks @huppe) to xbat (formerly Continuous Benchmarking)
- Add `FindSUNDIALS.cmake` to support cases where SUNDIALS doesn't provide CMake configuration file
- A link to `make_dynamic_model.sh` is created in the `bin` folder during CMake builds
- More verbose error message when statefile does not match IMP
- Flag to enable [OHara model modifications as suggested by Michael Clerx](https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990) (thanks @jorge.sanchez)

### Fixed
- `sv_dump` in MLIR+DLO build where extra "0" lines were output
- Conversion of transmembrane charge into concentration (#356)
- Reference to stimulus file format for `bench`
- Fixed gvec output for plugins.

### Changed
- Docker image archives are not stored on the web server anymore, only in the Docker registry.
- CI artifacts are kept for 12 hours instead of 2
- Removed duplicate Steward model (Stewart still exists)
- Changed the initial value of APD90 for restitution curve calculation to work with less depolarized ionic models (thanks @jorge.sanchez)
- Change in OpenMP activation: the option USE_OPENMP can now take three different values:
  - `UTILS` (default): OpenMP is activated for igbutils and meshtool
  - `OFF`: OpenMP is deactivated
  - `ON`: OpenMP is activated globally
  - Note: this changes the default behavior (before, OpenMP was deactivated by default).
- Compiler optimization level set to zero for `param` (issues with Ubuntu 24.04, gcc 13.2.0, glibc 2.39)

## [15.0] - 2024-03-18
### Added
- More detailed information on how stimulus currents are treated in chapter 3 of the manual
- Regression test for stress models
- Option to calculate cell surface to cell volume ratio (convert stimulus current to concentration change) (thanks @ml0401)
- Regression test for the feature above (thanks @ml0401)
- Generic build target defines below for ionic models introduced. By default all targets for a given model are built,
  define model specific macros to only generate code for specific targets, e.g. ALIEVPANFILOV\_MLIR\_CPU\_GENERATED.
  Cleaner build logs (#243, !184, thanks @huppe)
  - CPU\_GENERATED
  - MLIR\_CPU\_GENERATED
  - MLIR\_ROCM\_GENERATED
  - MLIR\_CUDA\_GENERATED
- CI job to keep generated IMP sources (`physics/limpet/src/imps_src`) in sync with .model files (on `master`)
- CI job to warn if IMP sources (`physics/limpet/src/imps_src`) are out of sync with .model files (on non-master branches)
- Support for non-activating nodes when using prepacing
- Regression test for non-existing imp regions (#332, thanks @tomas.stary)
- Documentation for MLIR tests (thanks @huppe)
- Modifications to OHara model [suggested by Michael Clerx]([Title](https://journals.plos.org/ploscompbiol/article/comment?id%253D10.1371%252Fannotation%252Fec788479-502e-4dbb-8985-52b9af657990)) (flag `CLERX`)

### Fixed
- Stress model variables `Ca_i` renamed to `Cai` (thanks @jorge.sanchez)
- Calling of python and shell scripts for dynamic model building now supported with AppImage after extraction
- Bugs related to IMP data layout optimisation (DLO) (thanks @huppe)
- The `deps` Docker image is now built on branches when Dockerfile-deps is modified
- Correctly convert math function `pow` to corresponding vector math library function call for MLIR code (thanks @huppe)
- Fixed buildinfo output
- Fixed Vm\_clamp and GND\_ex pulse forms
- Fixed HodgkinHuxley.model
- Bug when dumping state variables in DLO builds (thanks @huppe)
- Enable build in source folders which are not a repository (#351)

### Changed
- State vector extension in `bench` when validation flag is set true (e.g. `OHara.Cai` -> `OHara.Cai.bin`) (thanks @ml0401)

## [14.0] - 2023-12-07
### Added
- `latest` tag pointing to the most current release (#334)

### Changed
- The documentation of bench commands in the PDF manual is now autogenerated from `bench --help`
- Improved documentation for MLIR builds (thanks @vincent.loechner)
- Major rewrite of LIMPET moving from C to C++ (thanks @Raphael)
- Deprecated `-buildinfo` argument, now the build info is printed by default
- `igbapd`: linear interpolation of AP start
- State structure data layout optimization of ionic models (thanks @Raphael)
- Do `--stim-assign` preprocessing in bench only once and not per timestep

### Fixed
- `ISAC_Hu` model was missing some parentheses
- Missing initialization of pointers to parameter structures in `ION_IF.h`
- Include units in PDF and online parameter help
- make\_dynamic\_model.sh works with CMake based compilation for CVODE and DEBUG (#330)
- Compatibility with SUNDIALS v6
- Checkpointing when data layout optimization (DLO) was enabled (thanks @Raphael)
- Dynamic models (#310)
- Incorrect memory copies and memory initialization on GPU when using multiple regions
- `--stim-assign` in `bench` didn't work (#338, thanks @sophiaohnms)
- Stim species assignment for multiple species with non-equal ratios
- Linear solver name changed from const char\* to std::string to prevent name corruption.

## [13.0] - 2023-05-25
### Added
- Elec matrix integration: Added output of element and region info in case of NaN
  matrix entries. This help with spotting errors in the tissue parametrization
  (e.g zero fibers or conductivities, deteriorated elements).
- Ionic models: Added support for AMD code generation with MLIR. Ionic models can now be compiled and ran on AMD GPUs.
-  Compilation of multiple versions of a given ionic model in the same build (see `docs/BUILD_WITH_MLIR.md`)
- `--target` option to select execution platform in `bench` (see `docs/BUILD_WITH_MLIR.md`)
- User manual: Cite carphelp and give an example of usage
- CI job building openCARP using the provided Makefile

### Changed
- Elec matrix integration: Optimized choice of integration order.
- CMake process in `physics/limpet` refactored for readability
- GPU models can now access data correctly with `-DENABLE_MLIR_CODEGEN=DATA_LAYOUT_OPTIMIZATION`
- Use `overlay2` as `DOCKER_DRIVER` in CI pipelines

### Fixed
- Fixed a number of compiler warnings. Now builds clean with clang15.
- Build process based on plain Makefile.

### Known issues
- Some regression tests are failing when using Ginkgo as the numerical backend (#151)

## [12.0] - 2022-12-19
### Added
- New macOS package, compatible with Apple Silicon Macs.
- Tomek ionic model.
- Pre-commit hook to generate CITATION.cff from codemeta.json
- Added Ginkgo numerical backend (use CMake option `ENABLE_GINKGO`)
- Added the printout of all parameters with `--output-setup` option
- Optimized CPU and GPU code generation for ionic models using MLIR compiler infrastructure (use CMake option `ENABLE_MLIR_CODEGEN`, see `docs/BUILD_WITH_MLIR`).

### Changed
- macOS precompiled packages now contain MPICH, shipped with PETSc, like the Linux packages.
- Continuous Benchmarking can now be run on any branch (commit hash passed to Continuous Benchmarking trigger)
- Abstraction of interface to numerical backend (vectors etc.)
- Release step consists of two steps (manual tag pre-vX.Y and automatic tag vX.Y) now to include specific DOI in metadata in repo
- `Steward` model renamed to `Stewart` (fixing a typo in the name)

### Fixed
- cleaned up stim.crct.type and stimulus.stimtype to only contain available stimulus types.
- `--initial` flag in example 01/02B

### Known issues
- Some regression tests are failing when using Ginkgo as the numerical backend (#151)

## [11.0] - 2022-07-18
### Added
- Control over output domain for (phie, phie\_i, and vm) via `-dataout_i` and `-dataout_e`.
- Dedicated, asynchronous IO processes via `-num_io_nodes`.
- Support for arctan, atan, sin, floor functions in .model and CellML files
- CI pipeline extended by Continuous Benchmarking via Megware's framework
- Modify package generation and include paths in `make_dynamic_model.sh` to make it usable in precompiled packages (rpm, deb, pkg and AppImage).

### Fixed
- When using cmake to build openCARP with external tools, openMP is disabled in meshtool if it is disabled for openCARP
- Right-handed coordinate system also for 2D elements in `SF::get_transformed_pts()` (thanks @jk)
- Maintenance on tutorials (thanks @joshuasteyer, @jk)
- CMake aborted when libgfortran wasn't found (thanks @teo.puig)
- Fix include path hints in `make_dynamic_model.sh` for the case where openCARP is compiled from sources

## [10.0] - 2022-05-30
### Added
- EasyML2mmt.py to faciliatate conversion from .model to .cellml via .mmt, see example 01/11 for detailed instructions
- Documentation on monodmain and bidomain boundary conditions (sections 3.2 and 3.3 of the manual)
- Performance stats output for ionics (ODE\_stats.dat) and IO (IO\_stats.dat) in sim output dir

### Changed
- The AppImage package now contains some helper scripts: one to extract openCARP binaries from the AppImage, the other one to install carputils.
- In the postinstall phase of cmake, symbolic links created for MPI executables in /usr/local/bin could potentially interfer with an existing installation. These symbolic links were removed and the path to the MPI executable is now configured in carputils settings.
- openCARP Docker image is now based on Ubuntu 20.04
- `bench --imp-info` now also shows plugin metadata
- Metadata lines in .model files do not need to end with a semicolon anymore
- Added \_build/physics/limpet to make_dynamic_model.sh include directories to be compatible with CMake-based installations

### Fixed
- square(X) and cube(X) in .model files were causing error during code generation when using the Rosenbrock method
- make\_dynamic\_model.sh: Skip tests on contents of `my_switches.def` if it does not exist in install directory
- Fix the inclusion of MPI executables in Linux precompiled packages
- Fix documentation pipeline to include Stim structure.
- Fix error when trying to recompile openCARP after adding a new ionic model
- Ionic model metadata fields were cut if they contained a colon
- Made ionic model metadata fields consistent
- Compilation on macOS with M1 processor
- Changed xmltree.etree.getchildren() to list(xmltree.etree) for Python3.9 compatibility
- Better support for exponential notation in CellML files

## [9.0] - 2022-02-22
### Added
- Implementation for `--stim-species` and `--stim-ratios` parameters in `bench`
- Option for OpenMP support in CMake (`-DUSE_OPENMP=ON`)

### Changed
- Names of ionic models and plugins. See physics/limpet/models/README.md for details of the naming scheme and a mapping from old to new names. Additionally, variables were named consistenly across all available ionic models and plugins.
- bench: consistently no output of progress to terminal if using `--fout` irrespective of wheter `--validate` is being used or not.
- model files: changed pow(x,2) and pow(x,3) to square(X) and cube(X), respectively.
- Ensure fibre vectors have unit length (https://git.opencarp.org/openCARP/openCARP/-/merge_requests/73)
- Courtemanche.model now considers dynamically changing intracellular potassium concentration as in the original publication

### Fixed
- Add missing link to `hydra_pmi_proxy` in `/usr/local/bin` in postinstall phase for MPICH
- Kurata et al. ionic model
- `mesher` fiber generation for off-center meshes (https://git.opencarp.org/openCARP/openCARP/-/issues/132)
- Loading of external IMPs when openCARP is built with `-DNDEBUG` flag

## [8.2] - 2021-12-18
### Added
- Add CI jobs for testing (on schedule) and releasing openCARP Spack package when a new version is released.
- Added libjpeg as a dependency in CPack for being able to install pillow python package
- Documentation for the installation of openCARP via Spack.
- Optional building `igbdft`, required FFTW3 library
- `--buildinfo` flag for `bench`
- Added autotester pipeline
- Added AppImage building and release

## [8.1] - 2021-10-01
### Fixed
- Fixed bench not starting when tracing is used.
- Fixed trace output granularity.

## [8.0] - 2021-09-23
### Added
- Added trace functionality.
- Added human readable interpretations of PETSc solver divergence errors.
- Automatically add dropdown entries on webpage (parameters, doxygen, test reports) for release versions.

### Fixed
- Many fixes and improvements.

## [7.0] - 2021-06-29

### Added
- Pathmanathan & Gray cell model for test cases with analytical solution
- Loewe-Lutz-Fabbri-Severi cell model (human sinus node)
- Extended documentation on `gvec[]` parameters
- A filename given as parameter can now be given with its extension
- Warning in header to not change auto-generated files in `simulator`
- Numerical schemes as chapter 29 of the user manual

### Changed
- Default PETSc linear solver options set more sensibly.
- limpet/common.py module renamed to limpet/limpetcommon.py for better compatibility with other `common` modules
- Ionic model code is only re-generated in the CMake compilation when changes are present (determined per model)
- Types of some parameters of type string but corresponding to file names to `RFile`/`WFile`
- carp.prm was renamed to openCARP.prm as well as the auto-generated files in `simulator`

### Fixed
- Adjustments per node were only applied to the first node of the mesh.

## [6.0] - 2021-05-17
### Added
- illumination ionics interface and IchR2 model supporting it
- Bench parameter `--start-out` to control first output time step
- dump\_vtx option for stimuli

### Fixed
- Archiving of releases in the RADAR repository
- mesher fix for 2D mesh lon files.
- many fixes throughout the codebase.
- CI fixes.

## [5.0] - 2021-01-20
### Fixed
- CVODE related fixes.

### Added
- Added EasyML function rand01(): random number in range [0,1]
- Header to restitution\*.dat files generated by bench (starting with '# ')
- Added `stim[].ptcl.stimlist` to specify list of stimulation times.
- Documentation on units for currents on the single cell level.

## [4.0] - 2020-09-29

### Fixed
- Several fixes to FEM integration.

### Added
- External projects (carputils, examples, and meshtool) in the packages.
- Added support for Prism and Pyramid element types.
- Added support for per-element conductivity scaling.
- Added support for element renumbering and element-based input data.

### Changed
- Restructured docker files, merged user-version and developer-version docker images.


## [3.2] - 2020-07-13

### Added
- mMS ionic model.
- Increased robustness of `im_param` option by applying whitespace removal to option string.
- CI improvements.

### Fixed
- Fixed `write_statef` option not working.
- Fixed dynamic model building workflow.

## [3.1] - 2020-06-18

### Added
- MacOS installer.


## [3.0] - 2020-06-17

### Added
- Support for heterogenous electrode strength scaling via `stimulus[].vtx_fcn` (legacy stim)
  and `stim[].elec.vtx_fcn` (new stim format).

### Fixed
- Fixes to igbapd. It is now more consistent in its output handling.

### Changed
- Exposed more state vars in COURTEMANCHE model.
- CI updates.
- Reworked index and indexed data reading.



## [2.0] - 2020-05-09

### Added
- Python3 support complete.
- Added support for different (i.e. non-PETSc) matrix and solver implementations.
- KDtree partitioner is faster and requires less memory.

### Fixed
- Fixed `MacCannell_Fb_plug` and `I_KATP` models.
- Laplace solver only requires extracellular mesh.
- Many fixes and optimizations to the cmake building and packaging workflow.

### Changed
- `CONTRIBUTORS.md` is now `CONTRIBUTORS.yml`, which will also be used to generate the metadata for archives of the releases.
- Exposed `TT2.Gto` limpet variable.
- Build-time code generation now uses python3.


## [1.2] - 2020-03-17
### Fixed
- `adjust_MIIF_variable` now tells the user what adjustment went wrong.

### Added
- Added `gridout_p` for partitioning output.

### Removed
- Removed some unsupported simulator parameters.

### Changed
- Further improved python3 compatibility.
- Improved message output for `gridout`.

## [1.1] - 2020-03-05
### Fixed
- Dynamic ionic model loading was fixed.

### Changed
- Many python scripts are now python3 compatible. The transition is not complete yet.


## [1.0] - 2020-03-03
- Initial public release
