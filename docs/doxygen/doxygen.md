# openCARP C++ code documention

## Introduction

openCARP is an open cardiac electrophysiology simulator for in-silico experiments.

To start navigating through the documentation, please click on "Related Pages", "Namespaces", "Classes", "Files", or "Examples" above to start walking through the documentation.

## Software architecture description

Here is a brief dependency graph of the simulator

@dot
digraph graphname {
  a [ label="simulator" URL="dir_f56d48452e298091a290bc1200c44e7b.html" ];
  b [ label="numerics" URL="dir_40617c18ece8d47898b3425d74601b1f.html" ];
  c [ label="physics" URL="dir_59dc24c326e2a72d1164f014b22de83d.html" ];
  d [ label="limpet" URL="dir_8b6bfe5882d253cca67f891badd2e0ee.html" ];
  e [ label="fem" URL="dir_53dfd975dba04c9c8aff68f375276075.html" ];
  f [ label="slimfem" URL="dir_0ac08bc2c90437fb0da33698e7086a12.html" ];
  a -> b;
  a -> c;
  c -> d;
  a -> e;
  e -> f;
}
@enddot

For more detailed dependency and explanation, please check

* [simulator](dir_f56d48452e298091a290bc1200c44e7b.html): openCARP C++ simulator main functions.
* [numerics](dir_40617c18ece8d47898b3425d74601b1f.html): Managing stimulation signals and PETSc utilites.
* [physics](dir_59dc24c326e2a72d1164f014b22de83d.html): Tissue level electrics, electrical ionics functions and LIMPET wrappers. 
* [limpet](dir_8b6bfe5882d253cca67f891badd2e0ee.html): Library of IMPs (Ionic Model & Plug-ins) for Electrophysiological Theorization and the performance measurements program `bench`.
* [fem](dir_53dfd975dba04c9c8aff68f375276075.html): Finite element method solvers and interface to slimfem.
* [slimfem](dir_0ac08bc2c90437fb0da33698e7086a12.html): A lightweight header-only mesh management and FEM framework.

Other binaries

* param: Generating C++ source codes from `.prm` files.
* tools
	* [mesher](dir_f157a3884e78659b0d586ef350954cae.html): Generating regular meshes.
	* [igbutils](dir_a29b3d696af6a5c71a10d5d22e92953e.html): `.igb` files utilities including `igbapd`, `igbdft`, `igbextract`, `igbhead`, and `igbops`.


[Homepage](https://www.openCARP.org)
