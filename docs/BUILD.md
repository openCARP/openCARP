## Building from Source
After making sure the prerequisites are installed, you can build openCARP using CMake (recommended) or the shipped Makefile.

### Prerequisites
First install the following openCARP prerequisites using your package manager (e.g. apt or rpm on Linux, [homebrew](https://brew.sh/) or [macports](https://macports.org/) on macOS) or from source.
* [binutils](https://www.gnu.org/software/binutils/)
* C and C++ compilers (e.g. [gcc/g++](https://gcc.gnu.org) or [clang/clang++](https://clang.llvm.org). On macOS, we recommend clang provided by the homebrew package llvm to support openMP)
* [CMake](https://cmake.org) (Optional, minimum required version 3.13)
* [FFTW3](https://fftw.org) (Optional, for building `igbdft`)
* [gengetopt](https://www.gnu.org/software/gengetopt/gengetopt.html) (mininum version for current compilers: 2.23)
* [gfortran](https://gcc.gnu.org/fortran/)
* [git](https://git-scm.com)
* [make](https://git-scm.com)
* [PETSc](https://petsc.org/release/)
* [PkgConfig](https://www.freedesktop.org/wiki/Software/pkg-config/)
* [Python3](https://www.python.org) and the [pip package installer](https://pip.pypa.io/en/stable/)
* [SUNDIALS](https://computing.llnl.gov/projects/sundials) (Optional, for using advanced ODE time integration)
* [zlib](https://zlib.net) development package

[Building `PETSc` from source](https://petsc.org/release/install/) would be a better practice, so you could configure it depending on your needs.
If you are not experienced with its various [configurations](https://petsc.org/release/install/install_tutorial/#configuration), please refer to the following suggestions.
Set `--prefix=` to the location you would install `PETSc`.
After installation, set the [environment variable `PETSC_DIR` and `PETSC_ARCH`](https://petsc.org/release/install/multibuild/#environmental-variables-petsc-dir-and-petsc-arch) accordingly.

```
./configure \
    --prefix=/opt/petsc \
    --download-mpich \
    --download-fblaslapack \
    --download-metis \
    --download-parmetis \
    --download-hypre \
    --with-debugging=0 \
    COPTFLAGS='-O2' \
    CXXOPTFLAGS='-O2' \
    FOPTFLAGS='-O2'
make all
make install
```

In this case, to make `mpirun` available on your machine, add `$PETSC_DIR/bin` to your `PATH`. That is, add the following line to your `.bashrc` (or equivalent if you don't use the `bash` shell).
```
export PATH=$PATH:$PETSC_DIR/bin
```

If you have all the listed dependencies, a quick way to get openCARP up and running is the following:

Clone the repository and enter the codebase folder `openCARP`
```
git clone https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
```

To have a more stable environment, we recommend using the latest release version instead of the bleeding edge commit on the master branch:
```
git checkout latest
```
If you want to develop and push your changes back to the openCARP repository, then staying on a branch is the better option and you should skip the command above.

We provide CMake files and Makefiles for building the codebase, choose one fits your workflow.

### Building using CMake

Use CMake to create the `_build` build folder, optionally add `-DDLOPEN=ON` to enable ionic shared library loading, and `-DCMAKE_BUILD_TYPE=Release` to optimize compilation.

By default, the OpenMP parallelization is activated for `igbutils` and `meshtool`. This is equivalent to setting the option `-DUSE_OPENMP=UTILS` and requires that OpenMP is installed on your system.
You can use `-DUSE_OPENMP=ON` to activate OpenMP parallelization globally.
If you want to deactivate OpenMP parallelization completely, set `-DUSE_OPENMP=OFF`.

If you also want to build the additional tools ([meshtool](https://bitbucket.org/aneic/meshtool/src/master/), [carputils](https://git.opencarp.org/openCARP/carputils), and [examples](https://opencarp.org/documentation/examples)) locally, use in addition the option `-DBUILD_EXTERNAL=ON`:
```
cmake -S. -B_build -DDLOPEN=ON -DBUILD_EXTERNAL=ON -DCMAKE_BUILD_TYPE=Release
```

If you do not want to install the additional tools, you can use:
```
cmake -S. -B_build -DDLOPEN=ON -DCMAKE_BUILD_TYPE=Release
```

Compile the codebase, and all built executables will be located in the `_build/bin` folder.
```
cmake --build _build
```

If you built the additional tools, [meshtool](https://bitbucket.org/aneic/meshtool/src/master/) executable is also located in the `_build/bin` folder, [carputils](https://git.opencarp.org/openCARP/carputils) and [experiments](https://git.opencarp.org/openCARP/experiments) (contain [examples](https://opencarp.org/documentation/examples)) are cloned to the `external` folder.

Please note that in this case, carputils was downloaded, but you will still have to set it up in order to be able to run the [examples](https://opencarp.org/documentation/examples). Check the [installation instructions for carputils](https://opencarp.org/download/installation#development-installation) in order to do so.

### Building using the Makefile

The codebase can be compiled using

    make setup

This target includes updating the codebase (`make update`) and code compilation (`make all`).

Make sure to edit the `my_switches.def` file to match you requirements (e.g., to enable the use of shared libraries for ionic models).

Links to all compiled executables are located in the `bin` folder.

Please also refer to the [carputils main page](https://git.opencarp.org/openCARP/carputils), the [meshtool main page](https://bitbucket.org/aneic/meshtool/src/master/). 
Download the openCARP [examples](https://opencarp.org/documentation/examples) to the location of your choice using `git clone https://git.opencarp.org/openCARP/experiments.git`.
