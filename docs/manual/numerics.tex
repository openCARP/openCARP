\section{Numerical Schemes}
\label{sec:numerics}

\subsection{Spatial discretization using the Galerkin FEM}

For the spatial discretization of PDEs \eqref{eq:elliptic}, \eqref{eq:parabolic}
we use the classical finite element method (FEM)
(for the introduction's details see \cite{SzaboBabuska1991},
\cite{Ciarlet1978}, \cite{Trew2005}, \cite{OdenIV1983}, \cite{Bathe1995}).
An application of the FEM to the cardiac electro physiology (bidomain model) is in details described in \cite{Sundnesandothers2006}.

Briefly, the bidomain equations are multiplied with a weighting function, $\alpha$, and integrated over the entire domain, $\Omega$:
\begin{align}
\label{eq:elliptic_weak}
\int_\Omega \nabla \cdot (\boldsymbol{\sigma_i}+\boldsymbol{\sigma_e}) \nabla \phi_e \alpha d\Omega &=
            -\int_\Omega \nabla \cdot \boldsymbol{\sigma_i} \nabla V_m \alpha d\Omega - \int_\Omega \alpha I_{e} d\Omega \\
\label{eq:parabolic_weak}
\int_\Omega \nabla \cdot \boldsymbol{\sigma_i} \nabla V_m \alpha d\Omega &=
-\int_\Omega \nabla \cdot \boldsymbol{\sigma_i} \nabla \phi_e \alpha d\Omega + \beta \int_\Omega \alpha I_m d\Omega
\end{align}
Arbitrary weighting functions can be chosen for $\alpha$,
however, $\alpha$ has to fulfill any prescribed boundary conditions.
Using the identity
\begin{equation}
 \nabla \cdot (\alpha \mathbf{j} ) = \nabla \alpha \cdot \mathbf{j} + \alpha \nabla \cdot \mathbf{j}
\end{equation}
or
\begin{equation}
 \alpha \nabla \cdot \mathbf{j} =  \nabla \cdot (\alpha \mathbf{j} ) - \nabla \alpha \cdot \mathbf{j}
\end{equation}
where the term $\alpha \nabla \cdot \mathbf{j}$ with $\mathbf{j} = \boldsymbol{\sigma} \nabla \phi$
appears in the integral of Eqs.~\eqref{eq:elliptic_weak}-\eqref{eq:parabolic_weak},
allows to rewrite these integrals as
\begin{equation}
 \int_\Omega \alpha \nabla \cdot \mathbf{j} d\Omega = \int_\Omega \nabla \cdot (\alpha \mathbf{j} ) d\Omega
                                                    - \int_\Omega \nabla \alpha \cdot \mathbf{j} d\Omega
\end{equation}
or
\begin{equation}
 \int_\Omega \alpha \nabla \cdot \mathbf{j} d\Omega = \int_\Gamma \alpha \mathbf{j} d\Gamma
                                                    - \int_\Omega \nabla \alpha \cdot \mathbf{j} d\Omega \label{eq:_identity}
\end{equation}
where the surface integral over the domain boundary $\partial \Omega$ is zero,
unless non-zero Neumann flux boundary conditions are enforced.
Substituting Eq.~\eqref{eq:_identity} into Eqs.~\eqref{eq:elliptic_weak}-\eqref{eq:parabolic_weak} yields
\begin{align}
\label{eq:elliptic_weak_a}
-\int_\Omega \nabla \alpha \cdot (\boldsymbol{\sigma_i}+\boldsymbol{\sigma_e}) \nabla \phi_e d\Omega &=
            \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla V_m d\Omega - \int_\Omega \alpha I_{e} d\Omega \\
\label{eq:parabolic_weak_a}
-\int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla V_m d\Omega &=
 \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla \phi_e d\Omega + \beta \int_\Omega \alpha I_m d\Omega
\end{align}

If we further assume that $\alpha(x)=1$ holds everywhere over the domain, except maybe along Dirichlet boundaries,
we may write all sought after functions in terms of products such as
\begin{align}
 V_m(x) = \alpha(x) V_m(x) \\
 I_m(x) = \alpha(x) I_m(x) \\
 I_{ioin}(x) = \alpha(x) I_{ion}(x) \\
 \phi_e(x) = \alpha(x) \phi_e(x)
\end{align}
and substituting into Eqs.~\eqref{eq:elliptic_weak_a}-\eqref{eq:parabolic_weak_a} yields
the final form
\begin{align}
\label{eq:elliptic_weak_b}
-\int_\Omega \nabla \alpha \cdot (\boldsymbol{\sigma_i}+\boldsymbol{\sigma_e}) \nabla \phi_e d\Omega &=
            \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla V_m d\Omega - \int_\Omega \alpha I_{e} d\Omega \\
\label{eq:parabolic_weak_b}
-\int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla V_m d\Omega &=
 \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma_i} \nabla \phi_e d\Omega + \beta \int_\Omega \alpha I_m d\Omega
\end{align}


Stiffness matrices, $\boldsymbol{\sigma}$ are discretized to have positive main diagonals,
that is
\begin{equation}
 \mathbf{K_\zeta} \phi \approx -\nabla \cdot \boldsymbol{\sigma_\zeta} \nabla \phi
\end{equation}

We write the elliptic parabolic cast of the bidomain equations,
as given by Eq.~\eqref{eq:elliptic}-\eqref{eq:parabolic}, in their spatially discrete form as follows:
\begin{align}
  \mathbf{K_{i+e}} \mathbf{\boldsymbol\phi_e} &= -
          \mathbf{K_i} \mathbf{v_m} + \mathbf{M_e I_e} \\%\label{eq:ellip_x} \\  \label{eq:parab_x}
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m}}{\partial t} &= -
  \mathbf{K_i}\left(\mathbf{v_m}+\boldsymbol\phi_e \right) - \beta \mathbf{M_i} (I_{ion}-I_{tr})
\end{align}
By default the implementation of the bidomain equations in CARP relies on
elliptic-parabolic decoupling and operator splitting of the parabolic equations
which results in
\begin{align}
  \mathbf{K_{i+e}} \mathbf{\boldsymbol\phi_e} &= -
          \mathbf{K_i} \mathbf{v_m} + \mathbf{M_e I_e} \\%\label{eq:ellip_x_splt} \\
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m}}{\partial t} &=
  -\beta \mathbf{M_i} (\mathbf{I_{ion}}-\mathbf{I_{tr}}) \label{eq:_ode_x_splt} \\
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m}}{\partial t} &=
  -\mathbf{K_i}\left(\mathbf{v_m}+\boldsymbol\phi_e \right) %\label{eq:parab_x_splt}
\end{align}
where in Eq.~\eqref{eq:_ode_x_splt} $\beta \mathbf{M_i}$ cancels out.
The implementation of Eq.~\eqref{eq:_ode_x_splt} is then
\begin{equation}
  \frac{\partial \mathbf{v_m}}{\partial t} = -\frac{1}{C_m}(\mathbf{I_{ion}}-\mathbf{I_{tr}}) \label{eq:_ode_x_splt_imp} \\
\end{equation}

\subsection{Domain mapping}

In the presence of a bath intracellular and extracellular domain differ in size.
Extracellular and intracellular domain overlap over the entire domain $\Omega_i$,
however, in the bath domain $\Omega_b=\Omega_e \setminus \Omega_i$ the intracellular space does not exist.
Therefore, vectors defined on the two discrete spaces have to mapped as follows
\begin{align}
  \mathbf{K_{i+e}} \mathbf{\boldsymbol\phi_e} &= -
          \mathbf{K_i P} \mathbf{v_m} + \mathbf{M_e I_e}\\% \label{eq:ellip_x_splt} \\
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m}}{\partial t} &=
  -\mathbf{K_i}\left(\mathbf{v_m}+\mathbf{P^{-T}} \boldsymbol\phi_e \right) \\%\label{eq:parab_x_splt}
\end{align}
where $\mathbf{P}$ is the prolongation $\Omega_i \mapsto \Omega_e$ and
$\mathbf{P^{-T}}$ is the restriction $\Omega_e \mapsto \Omega_i$.
It is worth noting that in our implementation the same spatial discretization is shared
in the overlapping domain, thus only index-based mapping is performed for transferring
data between the grids.


\subsection{Temporal discretization schemes}
\label{sec:_app_temporal}

Temporal discretization is based on the assumption of parabolic-elliptic decoupling of the bidomain equations.
That is, unlike in \cite{southern09:_coupled}, elliptic and parabolic portions are not solved as a single system.
First, the elliptic portion is solved for to obtain $\phi_e(t)$ as a function
of the current distribution of the transmembrane voltage, $V_m$, enforced Dirichlet potentials $\phi_{eD}$
or extracellular stimulus currents, $I_e$.
The extracellular potential field $\phi_e$ is used then in the parabolic equation
to solve for $V_m^{t+dt}$.
There are two basic approaches implemented for this solve
where the standard method relies upon operator splitting \cite{strang68:_split}.
In theory, a Strang splitting should be used to achieve second order accuracy,
however, in practice only a first order accurate Godunov splitting scheme is used.
See \cite{sundnes05:_split} for details.
In practice, we do not expect any difference between the two schemes.
Only the first time step is different between the two schemes.
That is, if the solution during the first shift by half a time step to achieve a $dt/2$ delay
between ODE and parabolic PDE solution does not change,
there cannot be any differences between a Godunov and a Strang splitting.
The main advantage of operator splitting is that it renders the parabolic PDE linear,
which can be beneficial in terms of iteration numbers when using an iterative method.
In the alternative scenario we refrain from operator splitting
and solve the parabolic PDE which is non-linear now
since the non-linear term $I_{ion}(V_m,\boldsymbol{\eta},t)$ shows up on the right hand side.

Using the spatially discrete representations of $V_m$, $\mathbf{v_m}$, and of $\phi_e$, $\boldsymbol{\phi_e}$,
and discretizing in time with
\begin{equation}
  t = k dt
\end{equation}
we write spatio-temporally discretized representations as
\begin{align}
 V_m(x,t)    & \approx \mathbf{v_m^k} \\
 \phi_e(x,t) & \approx \boldsymbol{\phi_e}\mathbf{^k}
\end{align}

The basics of the two schemes are given as follows:

\paragraph{Temporal discretization with operator splitting}

\begin{align}
  \mathbf{K_{i+e}} \mathbf{\boldsymbol\phi_e^{k}} &= -
          \mathbf{P K_i} \mathbf{v_m^{k}} + \mathbf{M_e I_e} \\
  \frac{\partial{\boldsymbol{\eta}}}{\partial t} &= g( \mathbf{v_m^{k}},\boldsymbol{\eta}\mathbf{^{k}}) \Longrightarrow \boldsymbol{\eta^{k+1}} \\
  \mathbf{I_{ion}^{\chi}} & = f(\mathbf{v_m^k},\boldsymbol{\eta}\mathbf{^{k+1}}) \\
  \mathbf{v_m^{\chi}} & = \mathbf{v_m^k}-\frac{\Delta t_o}{C_m}\left( \mathbf{I_{ion}^{\chi}} -\mathbf{I_{tr}} \right) \label{eq:_osplit_vm_update} \\
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m^{\chi}}}{\partial t} &= -
  \mathbf{K_i}\left(\mathbf{v_m^{\chi}}+\mathbf{P^{-T}}\boldsymbol\phi_e\mathbf{^{k}}\right) \label{eq:_osplit_diffuse}
\end{align}
Here $\mathbf{M}$ is the mass matrix, $\mathbf{K}$ is the stiffness matrix with the subscript specifying which conductivity to use,
intracellular $(i)$, extracellular $(e)$ or their sum $(i+e)$,
$\boldsymbol\eta$ is the set of state variables of the ionic model,
$g()$ is the ionic model dependent set of state equations and
$f()$ is a function describing the total ionic current across the membrane as a function of the current state, $\boldsymbol{\eta}$.
Note that $\chi$ indicates the result of an intermediate compute step
where $\mathbf{I_{ion}^\chi}$ is computed with an updated state $\boldsymbol{\eta}\mathbf{^{k+1}}$
and the current transmembrane voltage $\mathbf{v_m^{k}}$.
The symbol $\Longrightarrow$ indicates that the set of ODEs is solved to compute $\boldsymbol{\eta^{k+1}}$.
Unlike in Eq.~\eqref{eq:_osplit_vm_update} where we always use a simple forward Euler update
where we advance the solution by the ODE time step, $\Delta t_o$.
Numerous options are available for updating $\boldsymbol{\eta^{k+1}}$, thus only a symbol is used to indicate
that an implementation-dependent solver step is executed.
By default our implementation relies upon an accelerated Rush-Larsen technique
which has been described in detail elsewhere \cite{plank08:_ecme_rirr}.
In the following derivations it is convenient to scale the intracellular mass matrix, $\mathbf{M_i}$
with $\kappa$ such that
\begin{align}
 \kappa &=\frac{\beta C_m}{\Delta t_p} \\
 \mathbf{\bar{M}_i} &= \kappa \mathbf{M_i} \label{eq:_mass_scaled} \\
 \Delta t_p &= \frac{\Delta t} {n_s}
\end{align}
where $\mathbf{M_i}$ is the unscaled mass matrix and $\Delta t_p$ is the time step
used for advancing the parabolic solution
which can be a fraction (but not a multiple) of the global electrical time step, $\Delta t$.
That is, $n_s \ge 1$ is the integer number which specifies the number of parabolic sub-timesteps
which are used to diffuse the change in $V_m$ due to the ODE reaction terms.
In general, we use $\Delta t = \Delta t_o = \Delta t_p$ since $n_s=1$ is the default value.
Using a value $n_s>1$ is beneficial when using an explicit method with an unstructured grid
where the CFL condition may impose severe limits upon the choice of $\Delta t$.
In such cases, one may chose $\Delta t_o=\Delta t$ and $n_s$ sufficiently large.
Thus the ODE load is kept constant, the parabolic compute lead increases linearly with $n_s$,
since parabolic solver steps are repeated $n_s$ times.
However, significant improvements in strong scalaling characteristics may allow to achieve
shorter execution times. Details are found in \cite{niederer11:_clinical}.


\paragraph{Temporal discretization without operator splitting}

\begin{align}
  \mathbf{K_{i+e}} \mathbf{\boldsymbol\phi_e^{k}} &= -
          \mathbf{P K_i} \mathbf{v_m^{k}} + \mathbf{M_e I_e} \\
  \frac{\partial{\boldsymbol{\eta}}}{\partial t} &= g( \mathbf{v_m^{k}},\boldsymbol{\eta}\mathbf{^{k}}) \Longrightarrow \boldsymbol{\eta^{k+1}}  \\
  \mathbf{I_{ion}^{\chi}} & = f(\mathbf{v_m^k},\boldsymbol{\eta}\mathbf{^{k+1}}) \\
  \label{eq:parabdiscret}
  \beta C_m \mathbf{M_i} \frac{\partial \mathbf{v_m^{\chi}}}{\partial t} &= -
  \mathbf{K_i}\left
  (\mathbf{v_m^{k}}+\mathbf{P^{-T}} \boldsymbol\phi_e\mathbf{^{k}}\right ) - \beta \mathbf{M_i} \left( \mathbf{I_{ion}^{\chi}-I_{tr}} \right)
\end{align}

For the time discretization we consider one of the simple, for construction and implementation,
explicit technique --- Forward Euler scheme, as well as two fully Implicit methods
--- Crank-Nicolson and Second order time stepping schemes.

\subsubsection{Forward Euler scheme (FE)}
Forward Euler scheme is well known as Explicit method and has a following form:
\begin{align}
 \mathbf{v_m^{\chi}} &= \mathbf{v_m^k}-\frac{\Delta t_o}{C_m}\left( \mathbf{I_{ion}^{\chi}} -\mathbf{I_{tr}} \right) \\
 \mathbf{v_m^{k+1}}  &= \mathbf{v_m^k} + \mathbf{\bar{M}_i^{-1}K_i}
                          \left(\mathbf{v_m^{\chi}}+\mathbf{P^{-T}} \boldsymbol{\phi_e}\mathbf{^{k}}\right ) \label{eq:_fe_splt}
\end{align}
or, in the case without operator splitting,
\begin{equation}
\mathbf{v_m^{k+1}} = \mathbf{v_m^k} + \mathbf{\bar{M}_i^{-1}K_i}\left
(\mathbf{v_m^{k}}+\mathbf{P^{-T}} \boldsymbol{\phi_e}\mathbf{^{k}} \right )
-\beta \mathbf{M_i}\mathbf{\bar{M}_i^{-1}}(\mathbf{I_{ion}-I_{tr}}) \label{eq:_fe}
\end{equation}
where
\begin{equation}
 \beta \mathbf{M_i}\mathbf{\bar{M}_i^{-1}} = \frac{\Delta t_p}{C_m}.
\end{equation}

It has advantages in sense of less requirements of memory and computational time,
but is not as stable and has some stability restrictions on the time step
which are governed by the Courant-Friedrichs-Levy (CFL) condition.
This restrictions upon $\Delta t_p$ may become prohibitive when using fine spatial discretizations.
This is of particular relevance when using unstructured grids for spatial discretization
since a single short element edge in the grid may enforce a very small time step.
In this case, FE mesh quality is of importance.
One way of alleviating this problem is to use parabolic sub-timestepping.
In this scenario a global $\Delta t_p$ is used for solving the system  of ODEs,
but diffusion is computed in time steps of $\Delta t_p/n_s$ where $n_s$ is an integer
for subdividing the interval.
Since a single Forward Euler step is so cheap, the method can still be beneficial,
see for instance, in Niederer et al \cite{niederer11:_clinical}.
A further disadvantage is due to the requirement of mass lumping
which is necessitated for simple inversion of the mass matrix
since $\mathbf{M_i^{-1}}$ is used at the right hand side of Eq.~\eqref{eq:_fe}.

\subsubsection{Crank-Nicolson scheme (CN)}
The Crank-Nicolson method gives possibility to avoids the strict stability restrictions on the time step
and to improve accuracy and stability. In the case of operator splitting we solve
\begin{align}
\mathbf{v_m^{\chi}} &= \mathbf{v_m^k}-\frac{\Delta t_o}{C_m}\left( \mathbf{I_{ion}^{\chi}} -\mathbf{I_{tr}} \right) \\
\left (\mathbf{\bar{M}_i}+\frac{\mathbf{K_i}}{2}\right)\mathbf{v_m^{k+1}} &=  - \mathbf{K_i}\left
  (\frac{\mathbf{v_m^{\chi}}}{2}+\mathbf{P^{-T}} \boldsymbol\phi_e\mathbf{^{k}}\right )+\mathbf{\bar{M}_i}\mathbf{v_m^{\chi}}
\end{align}
or, without operator splitting, we have
\begin{equation}
\left (\mathbf{\bar{M}_i}+\frac{\mathbf{K_i}}{2}\right)\mathbf{v_m^{k+1}} =  - \mathbf{K_i}\left
  (\frac{\mathbf{v_m^{\chi}}}{2}+\mathbf{P^{-T}} \boldsymbol\phi_e\mathbf{^k}\right )+\mathbf{\bar{M}_i}\mathbf{v_m^{\chi}}
  -\beta \mathbf{M_i}(\mathbf{I_{ion}-I_{tr}}).
\end{equation}
Note that in the case where we refrain from operator splitting both versions of the intracellular mass matrix,
the unscaled matrix $\mathbf{M_i}$ and the scaled matrix $\mathbf{\bar{M}_i}$, are used.
Since only the scaled matrix $\mathbf{\bar{M}_i}$ is stored we compute the right hand side contribution
of ionic current and transmembrane stimulus current differently, using
\begin{equation}
\beta \mathbf{M_i}(\mathbf{I_{ion}-I_{tr}}) = \frac{\Delta t_p}{C_m}\mathbf{\bar{M}_i}(\mathbf{I_{ion}-I_{tr}}).
\end{equation}
In our current implementation we allow the bidomain surface-to-volume ratio $\beta$ to vary, however,
the membrane capacitance $C_m$ is considered to be constant, i.e.\ $C_m=1\mu F/cm^2$.
As opposed to the explicit method, the solution of large systems of non-linear equations is required
for each time step.
However, due to the diagonal dominance of the problem the systems is solved quite efficiently with iterative methods,
requiring only a few iterations.
A particular advantage of the operator splitting approach is
that the number of iterations tends to be even lower since the parabolic problem is linear.

\subsubsection{\texorpdfstring{$\Theta$}{t}-schemes}

Crank-Nicolson, forward as well as backward Euler can be seen
as special cases of a general $\Theta$-scheme
where the individual methods correspond to the choices $\Theta=0.5$ (CN), $\Theta=0.$ (FE) and $\Theta=1.0$ (BE).
CN is second order accurate, but may be more prone to oscillations than BE.
The general $\Theta$scheme for the bidomain is given as
\begin{align}
\mathbf{v_m^{\chi}} &= \mathbf{v_m^k}-\frac{\Delta t_o}{C_m}\left( \mathbf{I_{ion}^{\chi}} -\mathbf{I_{tr}} \right) \\
\left (\mathbf{\bar{M}_i}+\theta \mathbf{K_i}\right)\mathbf{v_m^{k+1}} &=  - \mathbf{K_i}\left
  ((1-\Theta) \mathbf{v_m^{\chi}}+\mathbf{P^{-T}} \boldsymbol\phi_e\mathbf{^{k}}\right )+\mathbf{\bar{M}_i}\mathbf{v_m^{\chi}}
\end{align}
or, without operator splitting, we have
\begin{equation}
\left (\mathbf{\bar{M}_i}+\frac{\mathbf{K_i}}{2}\right)\mathbf{v_m^{k+1}} =  - \mathbf{K_i}\left
  (\frac{\mathbf{v_m^{\chi}}}{2}+\mathbf{P^{-T}} \boldsymbol\phi_e\mathbf{^k}\right )+\mathbf{\bar{M}_i}\mathbf{v_m^{\chi}}
  -\beta \mathbf{M_i}(\mathbf{I_{ion}-I_{tr}}).
\end{equation}


% \subsection{Second order time stepping scheme (TSt)}
% Beside Crank-Nicolson Method we use another one fully Implicit type method --- Second order time stepping scheme
% which is of the form
% \begin{align}
% \mathbf{v_m^{\chi}} &= \mathbf{v_m^k}-\frac{\Delta t}{C_m}\left( \mathbf{I_{ion}^{\chi}} -\mathbf{I_{tr}} \right) \\
%          \frac{1}{2} \Bigl (3\kappa \mathbf{M_i}- \mathbf{K_i} \Bigr ) \mathbf{v_m^{k+1}} & =
%          \mathbf{K_i} \left (\frac{\mathbf{v_m^{\chi}}}{2}+\mathbf{P^{-T}}\boldsymbol{\phi_e} \right ) +
%          \kappa \mathbf{M_i} \left ( 2 \mathbf{v_m^{\chi}} - \frac{1}{2} \mathbf{v_m^{k-1}} \right )
% \end{align}
% or, without operator splitting
% \begin{equation}
%          \frac{1}{2} \Bigl (3\kappa \mathbf{M_i}- \mathbf{K_i} \Bigr ) \mathbf{v_m^{k+1}} =
%          \mathbf{K_i} \left (\frac{\mathbf{v_m^{\chi}}}{2}+\mathbf{P^{-T}}\boldsymbol{\phi_e} \right ) +
%          \kappa \mathbf{M_i} \left ( 2 \mathbf{v_m^{\chi}} - \frac{1}{2} \mathbf{v_m^{k-1}} \right )
%          -\beta \mathbf{M_i}(\mathbf{I_{ion}-I_{tr}}).
% \end{equation}
% \textbf{This is not correct yet and needs further checking.
% The implementation is also wrong for the non operator-splitting case.}


