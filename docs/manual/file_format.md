File Formats
===========
The input and output files used in openCARP adhere to their own specific structure. This structure brings flexibility making different datasets easier to export or import. These file formats have been optimized to handle large amounts of data for fast I/O for pre- and post-processing. Using meshtool you can also import/export to different standard file formats. (e.g. VTK, obj, ensight).

Input Files
------------
### Parameter file
***Extension:*** .par

The parameter file contains all the input options to define a simulation. These parameters can be specified either by using this file or by giving them as command line arguments. The last option definition, whether in a file or on the command line, overrides any previous definition. The command line is parsed from left to right. Command line options may be placed in the parameter file by removing the preceding `-` and using `=`. For example, the time step is specified on the command line by `-dt f` where `f` is a floating point number. This can be placed in a parameter file on a line by itself as `dt = f`.

openCARP uses the parameter file as an input file to read all information regarding input files, output files and parameters for the simulation as follows:

`> openCARP +F parameter.par`


### Element file
***Extension:*** .elem

In general, openCARP supports various types of elements which can be mixed in a single mesh. The file begins with a single header line containing the number of elements, followed by one element definition per line. The element definitions are composed of an element type specifier string, the nodes for that element, then optionally an integer specifying the region to which the element belongs.

In the table below is an example of the file format:


| Format                                                  | Example               | 
|:--------------------------------------------------------|:----------------------|
| n                                                       |  2000                 | 
| $T_0\ N_{0,0}\ N_{0,1}\ N_{0,_{mi-1 }}\ \textrm{[elemTag]}_0$  |  Tt 0 1 2 150 1       |  
| ...                                                     |    ...                | 
| $T_{n-1}\ N_{{n-1},0}\ N_{{n-1},1}\ N_{{n-1},_{mi-1}}\ \textrm{[elemTag]}_{n-1}$ |  Tt 123 124 125 210 10|

The node indicies are determined by their order in the points file. Note that the nodes are 0-indexed, as indicated in Sec. Node file above. The element formats supported in openCARP, including their element type specifier strings are given in table below. Note that cH element type is for internal use only, not supported in mesh files. The ordering of nodes in each of the 3D element types is shown in Nodal ordering of 3D element types.
    
| Type Specifier | Description   | Interpolation | #Nodes |
|:---------------|:--------------|:--------------|:-------|
| Ln             | Line          | linear        | 2      |
| cH *           | Line          | cubic         | 2      |
| Tr             | Triangle      | linear        | 3      |
| Qd             | Quadrilateral | Ansatz        | 4      |
| Tt             | Tetrahedron   | linear        | 4      |
| Py             | Pyramid       | Ansatz        | 5      |
| Pr             | Prism         | Ansatzv       | 6      |
| Hx             | Hexahedron    | Ansatz        | 8      |


### Node file
***Extension:*** .pts

The node (or points) file starts with a single header line with the number of nodes, followed by the coordinates (x,y,z) of the nodes, one per line, in $\mu$m.

| Format                      | Example                   |
|:----------------------------|:--------------------------|
| n                           | 2000                      |
| $x_0\ y_0\ z_0$             | 1000.00 1000.00 300.00    |
| ...                         | ...                       |
| $x_{n-1}\ y_{n-1}\ z_{n-1}$ | 10000.00 10000.00 3000.00 |

### Fiber orientation file
***Extension:*** .lon

Fibres are defined on a per-element basis in openCARP. They may be defined by just the main fibre direction, in which case a transversely isotropic conductivity must be used, or additionally specifying the sheet (or transverse) direction to allow full orthotropy in the model.
The file format starts with a single header line with the number of fibre vectors defined in the file (1 for fibre direction only, 2 for fibre and sheet directions), and then one line per element with the values of the fibre vector(s). Note that the number of fibres must be equal to the number of elements read from the element file.
    
| Format                                           | Example                               |
|:-------------------------------------------------|:--------------------------------------|
| $n_f$                                            | 2                                     |
| $f_{0,x}\ f_{0,y}\ f_{0,z}\ s_{0,x}\ s_{0,y}\ s_{0,z}$ | 0.831 0.549 0.077  0.473 -0.775 0.417 |
| ...                                              | ...                                   |
| $f_{n-1,x}\ f_{n-1,y}\ f_{n-1,z}\ s_{n-1,x}\ s_{n-1,y}\ s_{n-1,z}$ | 0.0 0.0 0.0 0.0 0.0 0.0               |

### Pulse definition file
***Extension:*** .trc

The pulse definition file allows to introduce a signal with a predefined shape by the user. It starts with a single header line that is the total number of samples of the signal. Then, the signal is defined with one line per sample (time followed by the signal value to be applied between this time and the following time step). Note that the time must always increase from line to line.
   
| Format              | Example |
|:--------------------|:--------|
| n                   | 1000    |
| $t_0\ \textrm{val}_0$         | 0 60    |
| ...                 | ...     |
| $t_{n-1}\ \textrm{val}_{n-1}$ | 1000 0  |

### Vertex specification file
***Extension:*** .vtx

| Format       | Example         |
|:-------------|:----------------|
| n            | 1000            |
| intra \| extra | intra         |
| $\textrm{node}_0$     | 0               |
| ...          | ...             |
| $\textrm{node}_{n-1}$ | 123             |

### Vertex adjustment file
***Extension:*** .adj

| Format                 | Example        |
|:-----------------------|:---------------|
| n                      | 1000           |
| intra \|  extra        | intra          |
| $\textrm{node}_0\ \textrm{val}_0$         | 0  1000        |
| ...                    | ...            |
| $\textrm{node}_{n-1}\ \textrm{val}_{n-1}$ | 123  65        |

Output Files
------------
### IGB file
***Extension:*** .igb

openCARP simulations usually export data to a binary format called IGB. IGB is a format developed at the University of Montreal and originally used for visualizing regularly spaced data. An IGB file is composed of a 1024-byte long header followed by binary data which is terminated by the ASCII form feed character $(\string^L / character 12)$. For unstructured grids, the individual dimensions are meaningless but x * y * z should be equal to the number of vertices. The header is composed of strings of the following format, separated by white space: Keyword: value. Note that the header must be padded to 1024 bytes.

| Keywords    | Type   | Keywords    | Type   |
|:------------|:-------|:------------|:-------| 
| x           | int    | inc\_x      | float  |
| y           | int    | inc\_y      | float  |
| z           | int    | inc\_z      | float  |
| t           | int    | dim\_x      | float  |
| systeme     | string | dim\_y      | float  |
| type        | string | dim\_z      | float  |
| unites      | string | unites\_x   | string |
| facteur     | float  | unites\_y   | string |
| zero        | float  | unites\_z   | string |
| org\_x      | float  | comment     | string |
| org\_y      | float  | aut\_name   | string |
| org\_z      | float  | transparent | hex    |
| org\_t      | float  |


| Format                                                                                                                                                         | Example                                                                                                                                                       |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| x:int y:int z:int t:int type:string systeme:string org\_t:float dim\_t:float unites\_x:string unites\_y:string unites\_z:string unites\_t:string unites:string | x:333136 y:1 z:1 t:1452 type:float systeme:little\_endian org\_t:0 dim\_t:1451 unites\_x:um unites\_y:um unites\_z:um unites\_t:ms unites:mV facteur:1 zero:0 |
|                                                                                                                                                                |                                                                                                                                                               |
|$\textrm{float}_{n0}\ \textrm{float}_{n1}\ … \textrm{float}_{n-1}$                                                                                                                                     | -80 -80 ... -80                                                                                                                                               |
| ...                                                                                                                                                            | ...                                                                                                                                                           |
| $\textrm{float}_{n0}\ \textrm{float}_{n1}\ … \textrm{float}_{n-1}$                                                                                                                                     | -75 -75 ... -75                                                                                                                                               |




### Dynamic points file
***Extension:*** .dynpts

Points may also move in space as functions of time. This can be used to represent displacement during contraction, for example. The number of points must remain constant for all time instants, as well as the elements defined by them. Dynamic point files use the IGB format (see IGB file) with data type vec3f.

### Vector data file
***Extension:*** .vec and .vpts
To display vector data, an auxiliary set of points must be defined by a file with the .vpts suffix. It follows the same format as the Node file. A file with the same base name but having the extension .vec defines the vector and scalar data.
The scalar datum, as indicated, is optional. The .vec format can also be used for visualization of fiber orientations stored in .lon files. For this sake, a .vpts file must be generated holding the coordinates of the centers of each element in the mesh. This is conveniently achieved with GlElemCenters and changing the fiber file extension to .vec. For example, for a mesh with  elements and where  corresponds to the components of the fiber vector :

| Format                              | Example |
|:------------------------------------|:--------|
|data.x data.y data.z [scalar_datum]| 8.12453e-01 -2.22623e-01 1.25001e00 |
|data.x data.y data.z [scalar_datum]| 5.68533e-01 -1.81807e-01 1.04956e00 |
|data.x data.y data.z [scalar_datum]| 5.70671e-01 -1.67572e-01 1.06615e00 |
 
Vector data can also be input as an IGB file using the types vec3f, vec4f, vec3d, vec4d where 3 or 4 refers to the number of elements in each datum, and d and f refer to float or double. The first 3 elements define the value of the vector field, and the optional 4-th element is the scalar component as above. This file has the suffix .vec.igb.

### Auxiliary grid file
***Extension:***  .pts\_t, .elem\_t, .dat\_t

This format is used by Meshalyzer to display additional data on an auxiliary grid. The grid may contain any of the elements forming the main grid (points, lines, surface elements, volume elements), and the elements may change as a function of time. If scalar data were already read, the number of time instants in the auxiliary grid must either be one, or the same as the scalar data. The points file may have only one time instant, which is then assumed to be constant and applied for all time steps.

A vertex file is mandatory and has the extension .pts\_t. An element file is optional. If present, it has the same base name as the vertex file but with the extension .elem\_t. Scalar data may also be optionally defined on the auxiliary grid. The file has the same basename as the vertex file but with the extension .dat\_t. 

### Binary file
***Extension:***  .bin

This format is utilized by bench when running with the --validate option, which creates a binary file for each quantity explicitly labeled with .trace() in the .model file. The binary files contain only the raw binary data. The the byte order (little or big endian), the data type and size (4 byte floats: Gatetype or 8 byte doubles: Real, GlobalData_t), the number of samples and the available quantities are provided in the text file BENCH_REG_header.txt. 

Binary files can be converted to a HDF5 repository with the carputils script bin/bin2h5.py. This is also a good resource for an example how to read these files for postprocessing.