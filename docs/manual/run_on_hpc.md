# Running simulations on HPC systems

The openCARP ecosystem offers several possibilities for running simulations efficiently on High Performance Computing (HPC) systems.

## carputils option `--platform`

The carputils option `--platform` allows to select a hardware profile associated to a given platform. By default, the option `desktop` is selected.

Other hardware profiles available provide templates for job scheduling systems (such as SLURM) on several HPC platforms, such as Archer2 (UK National Supercomputing Service), VSC-5 (Vienna Scientific Cluster) or HoreKa (Karlsruhe Institute of Technology).

The complete list of systems is available in the [carputils repository](https://git.opencarp.org/openCARP/carputils/-/tree/master/carputils/machines?ref_type=heads) or using the `--help` option on a carputils script.

For example, the following command submits a SLURM job on bwUniCluster, which runs the carputils script `run.py` on 40 cores on the partition `single`, with a runtime limit of one hour:
```
python3 run.py --platform bwunicluster --queue single --np 40 --runtime 01:00:00
```
This is a good option if you want to run the openCARP call in your carputils experiment in parallel using all the allocated resources. This option is not right for you if want to run several independent openCARP simulations in parallel.

## Run carputils script within SLURM job

For more flexibility, or if your HPC platform is not supported in carputils, you can also run carputils scripts within your SLURM (or any other job scheduling system) script.

Here is an example of a SLURM job script running a carputils experiment on 2 nodes, with 8 tasks per node:

```
#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH --ntasks-per-core=1
#SBATCH --output=output.out
#SBATCH --error=output.err
#SBATCH --job-name=peerp

## importing required dependencies and executables
# activate Python virtual environment
source $HOME/opencarp/bin/activate
# add openCARP executables to the PATH
export PATH=$PATH:$HOME/openCARP/_build/bin

# no buffering on python side
export PYTHONUNBUFFERED=1

echo -e "$SLURM_JOB_NAME started on $(date)\n"

# carputils run script for the openCARP example 21_reentry_induction
# https://opencarp.org/documentation/examples/02_ep_tissue/21_reentry_induction
python run.py --np 16 \
  --protocol PEERP  \
  --max_n_beats_PEERP 2

echo -e "$SLURM_JOB_NAME finished on $(date)\n"
```
You can submit several of these jobs to run independent openCARP jobs or carputils experiments in parallel.

## Use carputils in JupyterLab

If your HPC system offers access through Jupyter as well as Jupyter container integration, you can run openCARP within JupyterLab, using the openCARP Docker image.

More information on how to use openCARP with JupyterLab is available on the [openCARP website](https://opencarp.org/documentation/jupyterlab#opencarp-jupyterlab-on-hpc-systems).

