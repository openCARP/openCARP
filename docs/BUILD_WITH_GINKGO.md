## Build openCARP with the Ginkgo backend

The linear algebra library [Gingko](https://ginkgo-project.github.io/) can be used as an alternative to PETSc. The Ginkgo backend can be run on a single or on multiple GPU using CUDA (NVIDIA GPUs), HIP (AMD GPUs), DPC++ (Intel GPUs), and for CPUs OpenMP. It requires MPI to be available.
This file explains how to use the Ginkgo backend in openCARP.

### Build openCARP with Ginkgo

You can build openCARP with Ginkgo support with:
```
git clone https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
cmake -S. -B_build -DENABLE_GINKGO=ON -DCMAKE_PREFIX_PATH=$HOME/install -DDLOPEN=ON
cmake --build _build
```

If you already have Ginkgo installed, this step should pick up your installation and use it. Otherwise, Ginkgo is fetched from GitHub and installed alongside openCARP.
For finding an already installed version of Ginkgo, the user can either:
- Put Ginkgo installation in a system of standard environment path.
- Add the Ginkgo installation directory to CMAKE_PREFIX_PATH.
- Give the Ginkgo installation directory to the `GINKGO_DIR` variable.
Note that 1 and 2 are standard CMake.


### Run experiments

#### Using the carputils options

You can use carputils to run experiments using the Ginkgo backend:
```
python3 run.py --flavor ginkgo
```
By default, this option will use the reference executor of Ginkgo, for sequential CPU execution.

In order to run the simulation in parallel on an NVIDIA GPU, you will have to set up the following openCARP option inside `run.py`:
```
cmd += ['-ginkgo_exec', 'cuda']
```
Alternatively, the option `-ginkgo_exec` can take the following values:
- `ref`: Reference Executor for sequential CPU execution
- `omp`: OpenMP parallelized CPU execution
- `cuda`: Cuda executor for execution on NVIDIA GPUs
- `hip`: Hip executor for execution on AMD GPUs
- `dpcpp`: Dpcpp executor for execution on Intel GPUs

#### Using the openCARP executable directly

You can also set up the Ginkgo backend by using the openCARP options directly:
```
openCARP [...] -flavor ginkgo -ginkgo_exec cuda -device_id 0
```
where the `-ginkgo_exec` and `-device_id` options can take similar values as the ones introduced in the previous section.

In this case, you will also have to set up the `-parab_options_file` and `-ellip_options_file` options by providing the paths to ginkgo configuration files, available here: https://git.opencarp.org/openCARP/carputils/-/tree/master/carputils/resources/ginkgo_options .

