## Installation of Precompiled openCARP

Before installing `openCARP`, install [Python3](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installing/).
Using your package manager would be preferable for most users.

Make sure they work by running the following commands.
```
python3 --version
python3 -m pip --version
```

For Ubuntu users: you might require to run these commands before installing your `python3-pip`.
```
add-apt-repository universe
apt-get update
```

### Installation Packages
We provide openCARP installation packages on our [release page](https://git.opencarp.org/openCARP/openCARP/-/releases).
Currently, they will install [openCARP](https://git.opencarp.org/openCARP/openCARP), [carputils](https://git.opencarp.org/openCARP/carputils), [meshtool](https://bitbucket.org/aneic/meshtool/src/master/), and the [examples](https://opencarp.org/documentation/examples).
[Meshalyzer](https://git.opencarp.org/openCARP/meshalyzer) is not yet included, please install it separately.

After installation, you can find the [examples](https://opencarp.org/documentation/examples) installed to `/usr/local/lib/opencarp/share/tutorials`.

#### Linux

For Ubuntu and Debian, please download the `.deb` package.

> **Note**: the packages of versions <= 9.0 are compatible with Ubuntu 18.04 and 20.04, and Debian 10 (and potentially more recent distributions).
> The packages of openCARP versions from 10.0 to 17.0 are compatible with Ubuntu 20.04 and 22.04, and Debian 11 (and potentially more recent distributions).
> openCARP versions > 17.0 provide a package compatible with Ubuntu 22.04, 24.04 and Debian 12, as well as a package compatible with Ubuntu 20.04 and Debian 11.
> If the latest `.deb` package is not compatible with your OS version, we invite you to use the AppImage package. 

Installing the package can require to install the following packages as a prerequisite:
```
apt-get install git python3 python3-testresources python-is-python3
```

We recommand to install the package via `apt-get` like
```
cd <path-to-your-downloaded-deb-file>
apt-get update
apt-get install ./opencarp-vXX.X.deb
```
with XX.X the version that you downloaded.

For CentOS (8 or later) and Fedora (31 or later), please download the `.rpm` package.

We recommand to install the package via `dnf` like
```
cd <path-to-your-downloaded-rpm-file>
dnf install ./opencarp-vXX.X.rpm
```
with XX.X the version that you downloaded.

> **Note:** If you are not using carputils, and want to run simulations with MPI using a command such as `mpiexec -n 4 openCARP ...`, you will in addition have to add the location of the MPI executables to your path: `export PATH=/usr/local/lib/opencarp/lib/petsc/bin:${PATH}`.

If the above packages don't work for you for some reason (no support of the operating system, no superuser permissions, etc.), please use the openCARP `AppImage`, which is a package for any recent enough Linux-based operating system. The documentation for using the `AppImage` is available inside the `README.md` file of the `AppImage` package or [here](https://git.opencarp.org/openCARP/openCARP/-/blob/master/appimage/user_helpers/README.md).

The `AppImage` only supports openCARP binaries, meshtool and carputils installation, so please check other components ([examples](https://opencarp.org/documentation/examples) and [Meshalyzer](https://git.opencarp.org/openCARP/meshalyzer)) according to your needs.

#### macOS

For macOS (10.14 or later), please download the `.pkg` installer. Before running the installer, call `xcode-select --install` on the command line (Programs -> Utilities -> Terminal).

> **Note:** For versions of openCARP >11.0, please use the `.pkg` installer coresponding to the architecture of your computer: use the `arm64` installer for an Apple Silicon computer, and the `x86_64` installer for systems with an Intel chip. If you don't know which one to choose, click on the Apple logo on the top left of the screen, then click on "About This Mac". If your processor's name contains "Intel", then use the `x86_64` installer ; if it contains "Apple", use the `arm64` installer.

You may need to open the installer using the context menu (right click on the item and then select `Open`) to confirm the security warning.

> **Note:** If you are not using carputils, and want to run simulations with MPI using a command such as `mpiexec -n 4 openCARP ...`, you will in addition have to add the location of the MPI executables to your path: `export PATH=/usr/local/lib/opencarp/lib/openmpi/bin:${PATH}`

### Confirm the Installation
To confirm if the package is successfully installed, open a terminal and try the following commands.

Run `openCARP` binary as follows. If it prints the building information (e.g. `GITtag: v10.0`, etc), your `openCARP` is successfully installed. Enjoy the simulator!
```
openCARP -buildinfo
```

Run `carputils` in Python as follows. If it prints the installation location, your `carputils` is successfully installed. Enjoy Python coding!
```
python3 -c "import carputils; print(carputils)"
```

### Docker
If you want to keep your host system clean but still be able to run and change openCARP, our Docker container might be most suitable for you. Docker containers should also run on Windows.
See [docker/INSTALL.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/INSTALL.md) for detailed instructions on how to install and use ([docker/README.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/INSTALL.md)) the openCARP docker container.

### Installation from Source
If you expect to change the openCARP source code, we suggest you follow the build instructions ([docs/BUILD.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD.md)) to install from source.
