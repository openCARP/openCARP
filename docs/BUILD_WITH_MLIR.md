## Build openCARP with the MLIR code generator

The Multi-Level Intermediate Representation [(MLIR)](https://mlir.llvm.org/) from the [LLVM](https://llvm.org/) compiler infrastructure can be used to generate optimized CPU and GPU code for the ionic models of the LIMPET library. At present, with the MLIR code generator, one can generate vectorized (with openMP) CPU and single GPU CUDA (NVIDIA GPUs) or ROCm (AMD GPUs) code. The steps to use MLIR as a code generator are detailed below.
Compatibility between openCARP and LLVM/MLIR is done through LLVM Tags, i.e., updates to openCARP is based upon tagged versions of LLVM. For now, the latest tagged LLVM version supported by openCARP is [18.1.8](https://github.com/llvm/llvm-project/tree/llvmorg-18.1.8).

Before trying to compile this version, make sure that your environment is correctly set up by compiling the baseline openCARP, using the instructions found in [BUILD.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD.md)

You can also skip directly to 'Build openCARP with MLIR' in this documentation, if you use the provided [DockerFiles](https://git.opencarp.org/openCARP/openCARP/-/tree/master/docker) including all required tools and compilers.

### CMake

**Building MLIR and building openCARP both require `cmake` version 3.19 at least**. Before going any further, if your distribution does not provide a recent version (try `cmake --version`), you can get and install the latest version from [here](https://github.com/Kitware/CMake); or you can find a precompiled binary of version 3.23.2 for Linux/x86_64 [here](https://github.com/Kitware/CMake/releases/download/v3.23.2/cmake-3.23.2-linux-x86_64.tar.gz): uncompress the archive and add the `bin/` subfolder to your `PATH`. For example:
```
wget -qO- https://github.com/Kitware/CMake/releases/download/v3.23.2/cmake-3.23.2-linux-$(uname -m).tar.gz \
| sudo tar xz --one-top-level=/usr/local/cmake --strip-components 1 \
&& export PATH="/usr/local/cmake/bin:$PATH"
```

### Build LLVM/MLIR

Build MLIR from the [LLVM](https://github.com/llvm/llvm-project) git repository. Clone LLVM tag [18.1.8](https://github.com/llvm/llvm-project/tree/llvmorg-18.1.8), then build MLIR, Clang and openMP project from the [instructions](https://mlir.llvm.org/getting_started/).

- The flag `MLIR_ENABLE_BINDINGS_PYTHON` should be set (to use python bindings).

For example, you can build LLVM/MLIR as below (enabling CUDA and ROCm code generation):
```
git clone -b llvmorg-18.1.8 --single-branch https://github.com/llvm/llvm-project.git && cd llvm-project
mkdir build && cd build
cmake -G Ninja ../llvm -DLLVM_ENABLE_PROJECTS="clang;mlir;openmp" \
 -DLLVM_BUILD_EXAMPLES=OFF -DLLVM_TARGETS_TO_BUILD="host;NVPTX;AMDGPU" \
 -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON \
 -DMLIR_ENABLE_BINDINGS_PYTHON=ON \
 -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi;libunwind" \
 -DMLIR_ENABLE_CUDA_RUNNER=ON -DMLIR_ENABLE_ROCM_RUNNER=ON \
 -DBUILD_SHARED_LIBS=True -DLLVM_ENABLE_LLD=ON
ninja all -j 4
```

You can use LLVM from the build directory, or choose the path for installation it using `-DCMAKE_INSTALL_PREFIX="llvm-install-path"` and do `ninja all install`.
The flag `LLVM_ENABLE_RUNTIMES` is optional and can be removed if the build fails.
Update your `PATH` to include the LLVM executables. For example:
```
export PATH="path-to-llvm-project/build/bin/:$PATH"
```

Now, set python bindings for MLIR with the [instructions](https://mlir.llvm.org/docs/Bindings/Python/) and add them to the `PYTHONPATH`. Updating your environment variable to point to the build directory is sufficient:
```
export PYTHONPATH="path-to-llvm-project/build/tools/mlir/python_packages/mlir_core:${PYTHONPATH}"
```
To verify your python/MLIR environment, check that the command `python3 -m mlir.ir` does not fail.

### Build openCARP with MLIR

Now, you can build openCARP with MLIR.
- The CMake flag `ENABLE_MLIR_CODEGEN` should be set to build the ionic models with the MLIR code generator.
  It can take the values: `TRUE` and `DATA_LAYOUT_OPT` to enable contiguous vector-sized data layout in memory.
- For CPU vectorized code generation, the CPU architecture vector size is auto-detected.
  It can be modified by the `-DVECTOR_WIDTH=<size>` (the size is given in bits) CMake option.
  The `VECTOR_WIDTH` divided by 64 is the ionic model loop unroll factor and the optimized data layout inner arrays size.
  
  On X86_64, it is detected as `128` for SSE vector architecture (2 `double`), `256` for AVX2 vector architecture
    (4 `double`), and `512` is for AVX512 vector architecture (8 `double`).You can check your processor capability
    on Linux by running `grep avx /proc/cpuinfo`.
  On ARM processors without SVE (like Apple Silicon chips), the NEON vector size is 128 bits (2 `double`).
- For CPU, the flag `MLIR_VECTOR_LIBRARY[= SVML | SLEEF | LIBMVEC-X86 | none]` is optional for mentioning the vector math library to use.
    * X86_64: the MLIR generated code performs *much* better with the `SVML` library (from the Intel proprietary oneAPI).
      You can get it from the [Intel website](https://www.intel.com/content/www/us/en/developer/tools/oneapi/base-toolkit-download.html).
      For the apt version, `intel-oneapi-compiler-shared-2025.0` is the package containing the shared library.
      The `libsvml.so` and `libintlc.so.?` files must be present in the path of the `LD_LIBRARY_PATH` environment
      variable (`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/intel/oneapi/compiler/latest/lib/` for example).
    * Apple Silicon: for now (Feb. 2025) the standard libm seems to perform best. You can also try to install
      [SLEEF](https://sleef.org/) and activate the option `-DMLIR_VECTOR_LIBRARY=SLEEF`.
- For GPU, the flag `MLIR_CUDA_PTX_FEATURE` is optional for mentioning the `ptx` version (for example `-DMLIR_CUDA_PTX_FEATURE=ptx72`)

For example, you can build openCARP as below for vectorized X86_64 AVX2 CPU code generation with data layout optimization and OpenMP:
```
git clone https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
cmake -S. -B_build -GNinja -DDLOPEN=OFF -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ \
  -DCMAKE_CXX_FLAGS="-O3 -march=native -mprefer-vector-width=256" \
  -DENABLE_MLIR_CODEGEN=DATA_LAYOUT_OPT -DUSE_OPENMP=ON \
  -DMLIR_VECTOR_LIBRARY=SVML
cd _build && ninja all
```

You should use the compilation option `-mprefer-vector-width=512` to take advantage of AVX512 vector architectures,
as clang will not enable AVX512 by default. For SSE and AVX2 it is optional.

For GPU code generation, the `-march` and `-mprefer-vector-width` flags are ignored. If your compiling
infrastructure has CUDA or ROCm available, CMake will detect it and generate the corresponding GPU code.

#### Choosing which versions to generate

The `physics/limpet/models/imp_list.txt` file is used by the build system to
know which version of each ionic model should be compiled. At the beginning of
each line, you can find the `model` model keyword followed by the model's name.
The `plugin` keyword indicate that the IMP is a a plugin.
The presence of the `generated` keyword tells the build system that this model
should be generated during the build. After the `generated` keyword, you can
specify one or more targets to build the IMP for. Possible values are `cpu`,
`mlir-cpu`, `mlir-rocm` and `mlir-cuda`. If no target is specified, the build
system will try to generated every possible target according to your environment
and build options.

### CUDA specifics

Clang/LLVM and CMake require a specific version of the nvcc CUDA compiler. LLVM 15 supports nvcc up to version 11.5. LLVM 18 supports nvcc up to version 12.1.
If an unsupported nvcc version is installed on your machine in `/usr/local/cuda` you should rename this directory, or uninstall it. Some versions of Linux also have a `/etc/alternatives/cuda` link that should be set correctly.

Another nvcc/Clang incompatibility is the ptx version they can both handle. If you get an error message like:
```
clang-15: warning: CUDA version is newer than the latest supported version 11.5 [-Wunknown-cuda-version]
ptxas /tmp/MULTI_ION_IF-2e39ef/MULTI_ION_IF-sm_35.s, line 5; fatal   : Unsupported .version 7.5; current version is '7.2'
```
then you should add `-DCUDA_GPU_FLAGS='--cuda-feature=+ptx72'` to the CMake options.

### ROCm specifics

When using PETSc and compiling for AMD GPUs, the rocThrust library is needed as it is a dependency of PETSc. In turn,
the rocPrim library is also needed as it is a dependency of rocThrust.

### MacOS/Apple Silicon native build

- PETSc: take a recent version from the git (`git clone -b release https://gitlab.com/petsc/petsc.git petsc`).
  Older versions do not compile on MacOS 15. You can compile it with your default compiler (not the built clang-18).
- You can install the dependencies with brew:
  ```
  brew install pybind11 zlib numpy lit
  ```
- Some Python packages need to be installed through pip (using a `venv` would be a better practice):
  ```
  pip3 install [--break-system-package] dataclasses pyyaml
  ```
- LLVM specifics

  This configuration works on MacOS 15.3/Apple Silicon m4:
  Start from llvm-project/ llvmorg-18.1.8 branch:
  ```
  git clone -b llvmorg-18.1.8 --single-branch https://github.com/llvm/llvm-project.git && cd llvm-project
  ```
  
  Need to patch clang to enable generation of functional OpenMP programs.
  Apply my patch (copy/paste this as is, in the llvm-project directory).
  ```
  git apply <<EOF
  diff --git a/clang/lib/Driver/ToolChains/CommonArgs.cpp b/clang/lib/Driver/ToolChains/CommonArgs.cpp
  index 2b916f000336..9e2a8cf798ec 100644
  --- a/clang/lib/Driver/ToolChains/CommonArgs.cpp
  +++ b/clang/lib/Driver/ToolChains/CommonArgs.cpp
  @@ -1104,6 +1104,10 @@ void tools::addOpenMPRuntimeLibraryPath(const ToolChain &TC,
         llvm::sys::path::parent_path(TC.getDriver().Dir);
     llvm::sys::path::append(DefaultLibPath, CLANG_INSTALL_LIBDIR_BASENAME);
     CmdArgs.push_back(Args.MakeArgString("-L" + DefaultLibPath));
  +  #if defined(__APPLE__)
  +    CmdArgs.push_back(Args.MakeArgString("-rpath"));
  +    CmdArgs.push_back(Args.MakeArgString(DefaultLibPath));
  +  #endif
   }
  
   void tools::addArchSpecificRPath(const ToolChain &TC, const ArgList &Args,
  EOF
  ```
  Now build and install LLVM
  ```
  mkdir build && cd build
  LLVM_INST=/opt/llvm
  cmake -G Ninja ../llvm \
    -DLLVM_ENABLE_PROJECTS="clang;mlir;openmp" \
    -DLLVM_BUILD_EXAMPLES=OFF \
    -DLLVM_TARGETS_TO_BUILD="host" \
    -DCMAKE_BUILD_TYPE=Release \
    -DLLVM_ENABLE_ASSERTIONS=ON \
    -DMLIR_ENABLE_BINDINGS_PYTHON=ON \
    -DLLVM_INSTALL_UTILS=ON \
    -DLLVM_ENABLE_LLD=ON \
    -DDEFAULT_SYSROOT="$(xcrun --show-sdk-path)" \
    -DCMAKE_INSTALL_PREFIX=${LLVM_INST} \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON
    
  ninja -j 4

  # if the install path is in /opt, create the directory and allow user write
  sudo mkdir -p ${LLVM_INST} && sudo chown ${USER} ${LLVM_INST}

  ninja install
  ```
  Explanations:
  * Do not activate `-DLLVM_ENABLE_RUNTIMES="..."`, the libraries conflict with the default system libs
  * Do not activate `-DBUILD_SHARED_LIBS=ON`, dynamic libraries in non-standard path are disabled on MacOS
  * For the compiler to find the system libs, add the MacOS SDK with: `-DDEFAULT_SYSROOT=$(xcrun --show-sdk-path)`

- After installing LLVM, set the PYTHONPATH and PATH (check that your PETSC_DIR is correctly defined - put PETSC path first in case you have another mpicc in your PATH):
  ```
  export PATH="$PETSC_DIR/bin:$LLVM_INST/bin:$PATH"
  export LD_LIBRARY_PATH="$LLVM_INST/lib:$LD_LIBRARY_PATH"
  export PYTHONPATH="$LLVM_INST/python_packages/mlir_core/:$PYTHONPATH"
  ```

- Build openCARP with:
  ```
  cmake -S. -B_build -GNinja \
    -DDLOPEN=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_COMPILER=clang \
    -DCMAKE_CXX_COMPILER=clang++ \
    -DCMAKE_C_FLAGS="-O3 -march=native" \
    -DCMAKE_CXX_FLAGS="-O3 -march=native" \
    -DENABLE_MLIR_CODEGEN=DATA_LAYOUT_OPT \
    -DUSE_OPENMP=ON
  ```
  
  et voilà!

### Run experiments

You can use the `bench` executable available in bin to test the experiments.
The `--target` option is used to chose which version of the ionic model to run.
Possible values are:
- `cpu`
- `mlir-cpu`
- `mlir-rocm`
- `mlir-cuda`
- `auto`
The default value is `auto`. It tries to select the "most specialized" target
generated for the given model. It goes in the following order:
`mlir-cuda > mlir-rocm > mlir-cpu > cpu`.
For example, you can use bench like this:
```
./bin/bench -I Courtemanche -n 819200 -a 10 --target mlir-cpu
```
and compare to the baseline:
```
./bin/bench -I Courtemanche -n 819200 -a 10 --target cpu
```

##### Using the openCARP executable or carputils options
You can run openCARP with default options for MLIR CPU vectorized code using your newly compiled openCARP executable.
This also works when openCARP is called from carputils.

For GPU, you have to build and run openCARP with Ginkgo options enabled (see [BUILD_WITH_GINKGO.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD_WITH_GINKGO.md)).
