// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_ABSTRACT_MATRIX_H
#define _SF_ABSTRACT_MATRIX_H

#include <mpi.h>

#include "SF_abstract_vector.h"
#include "SF_container.h"
#include "SF_globals.h"
#include "SF_parallel_layout.h"

namespace SF {


/**
 * FEM matrix class.
 *
 * It connects a mesh with a sparse matrix.
 *
 * @tparam T  Integer type (indices).
 * @tparam S  Floating point type (values).
 *
 * @see ginkgo_matrix
 * @see petsc_matrix
 */
template <class T, class S>
class abstract_matrix
{
 protected:
  /** Default constructor. */
  abstract_matrix() : mesh(NULL),
                      NRows(0),
                      NCols(0),
                      row_dpn(0),
                      col_dpn(0),
                      lsize(0),
                      start(0),
                      stop(0)
  {
  }

  /// pointer to the associated mesh
  const meshdata<mesh_int_t, mesh_real_t> * mesh;

  int  NRows;         ///< global number of rows
  int  NCols;         ///< global number of cols
  int  row_dpn;       ///< row dpn
  int  col_dpn;       ///< col dpn

  int  lsize;         //!< size of local matrix (\#locally stored rows)
  int  start;         //!< start row index of local matrix portion
  int  stop;          //!< stio row index of local matrix portion

  public:

  /** Default destructor */
  virtual ~abstract_matrix() = default;

  /**
  * Initialize a matrix with the requested sizes.
  *
  * @param iNRows       global number of rows
  * @param iNCols       global number of cols
  * @param ilrows       local number of rows
  * @param ilcols       local number of cols
  * @param loc_offset   offset of local partition in parallel layout
  * @param mxent        maximum expected number of entries per row
  */
  virtual inline void init(T iNRows, T iNCols, T ilrows, T ilcols,
                           T loc_offset, T mxent)
  {
    this->NRows = iNRows;
    this->NCols = iNCols;
    this->lsize = ilrows;
    this->start = loc_offset;
    this->stop  = this->start + this->lsize;
  }

  /**
  * Initialize a matrix dimensions based on a given mesh.
  *
  * @param imesh      mesh we associated to matrix
  * @param irow_dpn   row dpn
  * @param icol_dpn   col dpn
  * @param max_edges  max number of edges, if -1 max edges will be computed on
  *                   the fly
  */
  inline void init(const meshdata<mesh_int_t, mesh_real_t> & imesh,
                   const T irow_dpn,
                   const T icol_dpn,
                   const T max_edges)
  {
    mesh = &imesh;
    this->row_dpn = irow_dpn;
    this->col_dpn = icol_dpn;

    int rank;
    MPI_Comm_rank(mesh->comm, &rank);

    T M = mesh->pl.num_global_idx() * this->row_dpn;
    T N = mesh->pl.num_global_idx() * this->col_dpn;
    T m = mesh->pl.num_algebraic_idx() * this->row_dpn;
    T n = mesh->pl.num_algebraic_idx() * this->col_dpn;

    T nmax = max_edges;
    if(max_edges < 0) nmax = max_nodal_edgecount(*mesh);

    T loc_offset = mesh->pl.algebraic_layout()[rank]*this->row_dpn;

    init(M, N, m, n, loc_offset, nmax*this->col_dpn);
    zero();
  }

  /**
   * Get the associated mesh pointer.
   *
   * @returns the associated mesh pointer.
   */
  inline const meshdata<mesh_int_t, mesh_real_t>* mesh_ptr() const
  {
    return this->mesh;
  }

  /**
   * Get the number of row d.o.f per mesh node
   *
   * @returns the number of row d.o.f per mesh node
   */
  inline int dpn_row() const { return this->row_dpn; }

  /**
   * Get the number of col d.o.f per mesh node
   *
   * @returns the number of col d.o.f per mesh node
   */
  inline int dpn_col() const { return this->col_dpn; }

  /**
   * Zero matrix values.
   */
  virtual void zero() = 0;

  /**
   * Multiply this matrix with x and store the result in b (compute b := M x).
   *
   * @param x  vector x to multiply this matrix with
   * @param b  output vector
   */
  virtual void mult(const abstract_vector<T,S> & x, abstract_vector<T,S> & b) const = 0;

  /**
   * Scales the matrix on the left and right using diagonal matrices stored as
   * vectors (compute M := L M R).
   *
   * @param L  left diagonal matrix used for scaling
   * @param R  right diagonal matrix used for scaling
   */
  virtual void mult_LR(const abstract_vector<T, S>& L, const abstract_vector<T, S>& R) = 0;

  /**
   * Add values to the diagonal of this matrix.
   *
   * @param diag  The values to add to the diagonal.
   */
  virtual void diag_add(const abstract_vector<T, S>& diag) = 0;

  /**
   * Store the diagonal values of this martix in a vector.
   *
   * @param vec  The output vector containing the matrix's diagonal.
   */
  virtual void get_diagonal(abstract_vector<T, S>& vec) const = 0;

  /**
   * Mark assembly as being finished.
   */
  virtual void finish_assembly() = 0;

  /**
   * Scale the matrix with a scalar.
   *
   * @param s  The scalar to scale the matrix with.
   */
  virtual void scale(S s) = 0;

  /**
   * Compute M += sA
   *
   * @param A         The matrix to scale and add into this matrix.
   * @param s         The scalar to scale the matrix A with.
   * @param same_nnz  Whether A and this matrix share the same nonzero pattern.
   */
  virtual void add_scaled_matrix(const abstract_matrix<T, S>& A, const S s, const bool same_nnz) = 0;

  /**
   * Duplicate the matrix M into this matrix.
   *
   * @param M  The duplicated matrix.
   */
  virtual void duplicate(const abstract_matrix<T,S>& M) = 0;

  /**
   * Overwrite or add the values into this matrix.
   *
   * @param row_idx  The row indices.
   * @param col_idx  The column indices.
   * @param vals     The values to set.
   * @param add      Whether to add the values into this matrix or overwrite.
   *
   * @note  finish_assembly() needs to be called after all calls to this function.
   */
  virtual void set_values(const vector<T>& row_idx, const vector<T>& col_idx, const vector<S>& vals, bool add) = 0;

  /**
   * Overwrite or add the values into this matrix.
   *
   * @param row_idx  The row indices.
   * @param col_idx  The column indices.
   * @param vals     The values to set.
   * @param add      Whether to add the values into this matrix or overwrite.
   *
   * @note  finish_assembly() needs to be called after all calls to this function.
   */
  virtual void set_values(const vector<T> & row_idx, const vector<T> & col_idx,
                         const S* vals, bool add) = 0;

  /**
   * Overwrite or add one value into this matrix.
   *
   * @param row_idx  The row index.
   * @param col_idx  The column index.
   * @param val      The value to set.
   * @param add      Whether to add the value into this matrix or overwrite.
   */
  virtual void set_value(T row_idx, T col_idx, S val, bool add) = 0;

  /**
   * Extract values of the matrix at specified indices.
   *
   * @param row_idx  The requested row indices.
   * @param col_idx  The requested column indices.
   * @param values   The returned values of the matrix.
   *
   * @note This function is a sequential implementation and iterates over the
   *       matrix and can be slow.
   */
  inline void get_values(const vector<T> & row_idx, const vector<T> & col_idx,
                         vector<S> & values) const {
    assert(row_idx.size() == col_idx.size() == values.size());

    for (int i = 0; i < row_idx.size(); i++)
      values[i] = get_value(row_idx[i], col_idx[i]);
  }

  /**
   * Get a single value of the matrix.
   *
   * @param row_idx  The requested row index.
   * @param col_idx  The requested column index.
   *
   * @returns the requested value.
   */
  virtual S get_value(SF_int row_idx, SF_int col_idx) const = 0;

  /**
   * Writes the matrix to a file.
   *
   * @param filename  The file to write to.
   */
  virtual void write(const char* filename) const = 0;


  /**
   * Check equality between this matrix and another.
   *
   * @param rhs  The other matrix.
   *
   * @returns whether this matrix and rhs are equal.
   *
   * @note This function is a sequential implementation and iterates over the
   *       matrix and can be slow.
   */
  inline bool equals(const abstract_matrix<T,S> & rhs) const
  {
    if (NRows != rhs.NRows or NCols != rhs.NCols)
      return false;

    bool equal = true;
    for (size_t row = 0; row < NRows; row++)
      for (size_t col = 0; col < NCols; col++)
        if (fabs(get_value(row, col) - rhs.get_value(row, col)) > 0.0001)
          equal = false;

    return equal;
  }

  /**
   * A string format for the current matrix.
   *
   * @returns a string format for the current matrix.
   */
  inline std::string to_string() const
  {
    std::ostringstream stream;
    stream << "matrix (" << this->NRows << " x " << this->NCols << ") [\n";

    for (auto row = 0; row < this->NRows; row++) {
      stream << "   [";
      for (auto col = 0; col < this->NCols; col++) {
        if (col > 0) stream << ", ";
        stream << get_value(row, col);
      }
      stream << "]\n";
    }
    stream << "]";

    return stream.str();
  }
};

} // namespace SF



#endif // _SF_ABSTRACT_MATRIX_H
