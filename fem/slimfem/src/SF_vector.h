// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file SF_vector.h
* @brief The vector class and related algorithms.
* @author Aurel Neic
* @version 
* @date 2017-02-14
*/

#ifndef _SF_VECTOR_H
#define _SF_VECTOR_H

#include <iterator>
#include <cstring>

namespace SF {

/**
 * \brief A vector storing arbitrary data.
 *
 * The design and API are similar to std::vector.
 *
 */
template <class T>
class vector
{
public:
  /// Initialize an empty vector.
  vector(): _data(NULL), _capacity(0), _size(0)
  {}

  /// Initialize a vector of size n.
  vector(size_t n): _data(NULL), _capacity(0), _size(0)
  {
    this->resize(n);
  }

  /// Initialize a vector of size n and of constant value val.
  vector(size_t n, const T val): _data(NULL), _capacity(0), _size(0)
  {
    this->assign(n, val);
  }

  /// Initialize a vector from another vector.
  vector(const vector<T> &vec): _data(NULL), _capacity(0), _size(0)
  {
    this->assign(vec.begin(), vec.end());
  }

  virtual ~vector()
  {
    delete [] _data;
  }

  /// Vector access.
  inline const T& operator[](size_t i) const
  {
    return _data[i];
  }

  /// Vector access.
  inline T& operator[](size_t i)
  {
    return _data[i];
  }

  /// Deep copy of a vector.
  inline void operator= (const vector<T> & vec)
  {
    this->assign(vec.begin(), vec.end());
  }

  /// Pointer to the vector's start.
  inline T* data()
  {
    return _data;
  }

  /// Pointer to the vector's start.
  inline const T* data() const
  {
    return _data;
  }


  /// The current size of the vector.
  inline size_t size() const
  {
    return _size;
  }

  /// The maximum amount of entries the vector can hold without reallocating.
  inline size_t capacity() const
  {
    return _capacity;
  }

  /// Pointer to the vector's start.
  inline const T* begin() const
  {
    return _data;
  }

  /// Pointer to the vector's start.
  inline T* begin()
  {
    return _data;
  }

  /// Pointer to the vector's end.
  inline const T* end() const
  {
    return _data + _size;
  }

  /// Pointer to the vector's end.
  inline T* end()
  {
    return _data + _size;
  }

  T& front()
  {
      return _data[0];
  }

  T& back()
  {
      return _data[_size - 1];
  }

  const T& front() const
  {
      return _data[0];
  }

  const T& back() const
  {
      return _data[_size - 1];
  }

  /// Assign a memory range.
  template<class InputIterator>
  inline void assign(InputIterator s, InputIterator e)
  {
    long int n = std::distance(s, e);
    assert(n >= 0);

    // realloc if necessary
    if ( ((long int)_capacity) < n) {
      delete [] _data;
      _data = new T[n];
      _capacity = n;
    }

    // copy data
    size_t idx = 0;
    while(s != e) {
      _data[idx++] = *s;
      ++s;
    }

    _size = n;
  }

  /// Assign n many elements of value val.
  inline void assign(size_t n, T val = T()) {
    if (_capacity < n) {
      if(_size > 0) delete [] _data;
      _data = new T[n];
      _capacity = n;
    }

    for (size_t i = 0; i < n; i++) _data[i] = val;
    _size = n;
  }

  /**
   * \brief Make a vector point to an existing array.
   *
   * Only recommended for experts.
   *
   */
  inline void assign(size_t n, T* array, bool del) {
    if (del) delete [] _data;
    _data = array;
    _size = n;
    _capacity = n;
  }

  /// Resize a vector.
  inline void resize(size_t n)
  {
    if(_capacity < n)
    {
      if(_size > 0) {
        T* buf = _data;
        _data = new T[n];
        for (size_t i = 0; i < _size; i++) _data[i] = buf[i];
        delete [] buf;
      }
      else {
        _data = new T[n];
      }
      _capacity = n;
    }
    _size = n;
  }

  /// Resize a vector setting the newly allocated elements to val.
  inline void resize(size_t n, const T val)
  {
    size_t oldsize = this->size();
    this->resize(n);

    if(n > oldsize) {
      T*     out = _data + oldsize;
      size_t num = n - oldsize;

      for (size_t i = 0; i < num; i++) out[i] = val;
    }
  }

  void reserve(size_t n)
  {
    size_t osize = _size;
    this->resize(n);
    this->resize(osize);
  }

  void zero()
  {
    if(_size != 0) memset(_data, 0, _size * sizeof(T));
  }

  /** Allocate new data chunk for the current size.
   *  This is useful if the vector has been resized to a
   *  considerably smaller size.
   */
  inline void reallocate()
  {
    _capacity = _size;
    T* buf = _data;
    _data = new T[_size];
    for (size_t i = 0; i < _size; i++) _data[i] = buf[i];
    delete [] buf;
  }

  /// Append data to the current data chunk.
  template<class InputIterator>
  inline void append(InputIterator s, InputIterator e)
  {
    size_t addsize = std::distance(s, e);
    size_t oldsize = _size;

    this->resize(oldsize + addsize);

    // copy data
    size_t idx = 0;
    while(s != e) {
      _data[oldsize + idx++] = *s;
      ++s;
    }
  }

  T & push_back(T val)
  {
    if(_capacity > _size)
    {
      _data[_size] = val;
      _size++;
    }
    else {
      // if current capacity is not larger than size + 1 we reallocate, but keep size to
      // old value plus one.
      size_t newcap = _capacity > 0 ? _capacity * 2 : 1;
      size_t cursize = _size;
      this->resize(newcap);
      _data[cursize] = val;
      _size = cursize + 1;
    }
    return _data[_size-1];
  }
private:
  T* _data;         ///< The data array of the vector.
  size_t _capacity; ///< Maximum vector capacity. No reallocation needed for a resize below capacity limit.
  size_t _size;     ///< The current vector size.

};

/// Compute displacements from counts.
template<class T>
inline void dsp_from_cnt(const vector<T> & cnt, vector<T> & dsp)
{
  dsp.resize(cnt.size()+1);
  dsp[0] = 0;
  for(size_t i=0; i<cnt.size(); i++) dsp[i+1] = dsp[i] + cnt[i];
}

/// Compute counts from displacements.
template<class T>
inline void cnt_from_dsp(const vector<T> & dsp, vector<T> & cnt)
{
  cnt.resize(dsp.size() - 1);
  for(size_t i=0; i<dsp.size()-1; i++) cnt[i] = dsp[i+1] - dsp[i];
}

/**
* @brief Count number of occurrences of indices.
*
* @param data  Vector of index data.
* @param cnt   cnt[i] is the number of occurrences of index i.
*/
template<class T, class S>
inline void count(const vector<T> & data, vector<S> & cnt)
{
  cnt.zero();
  for(size_t i=0; i<data.size(); i++) cnt[data[i]]++;
}

/// Compute sum of a vector's entries.
template<class T>
inline T sum(const vector<T> & vec)
{
  T sum = 0;
  for(size_t i=0; i<vec.size(); i++) sum += vec[i];
  return sum;
}


/// Create an integer interval between start and end.
template<class T>
inline void interval(vector<T> & vec, size_t start, size_t end)
{
  vec.resize(end - start);
  for(size_t i=0; i<vec.size(); i++) vec[i] = start + i;
}

/// divide gsize into num_parts local parts with even distribution of the remainder
template<class T>
inline void divide(const size_t gsize, const size_t num_parts, vector<T> & loc_sizes)
{
  // initialize the distribution to gsize / size
  loc_sizes.assign(size_t(num_parts), T(gsize/num_parts));

  // then we evenly distribute the remainder of the division
  for(size_t i=0; i<size_t(gsize % num_parts); i++) loc_sizes[i]++;
}


/// \brief Assign the values in rhs to lhs. The data-type of rhs is cast to
/// the type of lhs.
template<class S, class V>
inline void vec_assign(S* lhs, const V* rhs, size_t size)
{
  for(size_t i=0; i<size; i++) lhs[i] = (S)rhs[i];
}

/// Return whether an vector is empty (all values are 0).
template<class T>
inline bool isEmpty(vector<T> & v)
{
  bool ret = true;
  for(size_t i=0; i<v.size(); i++)
    if(v[i] != T(0)) {ret = false; break;}

  return ret;
}


}

#endif

