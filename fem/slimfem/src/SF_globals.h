// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GLOBALS_H
#define _SF_GLOBALS_H

#include <cstdint>
#include <mpi.h>

// This defines are used in multiple header files so have to put them here
/// the default SlimFem MPI communicator
#define SF_COMM MPI_COMM_WORLD
/// the MPI tag when communicating
#define SF_MPITAG 100

// Define the global int and real types used in SlimFem
#ifdef WITH_PETSC
#include <petscsys.h>
using SF_int = PetscInt;      //!< Set SF_int to PetscInt in the PETSc case
using SF_real = PetscReal;    //!< Set SF_real to PetscReal in the PETSc case
#else
using SF_int = std::int32_t;  //!< Use the general std::int32_t as int type
using SF_real = double;       //!< Use the general double as real type
#endif // WITH_PETSC


#endif // _SF_GLOBALS_H
