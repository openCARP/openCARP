#ifndef _SF_IO_BASE
#define _SF_IO_BASE

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define htobe(x) SF::byte_swap(x)
#define betoh(x) SF::byte_swap(x)
#define htole(x) (x)
#define letoh(x) (x)
#define MT_ENDIANNESS 0
#else
#define htobe(x) (x)
#define betoh(x) (x)
#define htole(x) SF::byte_swap(x)
#define letoh(x) SF::byte_swap(x)
#define MT_ENDIANNESS 1
#endif

namespace SF {

/// swap the bytes of int, float, doubles, etc..
template<typename T>
T byte_swap(T in)
{
  T out; // output buffer

  char* inp   = ( char* ) (& in );
  char* outp  = ( char* ) (& out);
  size_t size = sizeof(T);

  switch(size)
  {
    case 4:
      outp[0] = inp[3];
      outp[1] = inp[2];
      outp[2] = inp[1];
      outp[3] = inp[0];
      break;

    case 2:
      outp[0] = inp[1];
      outp[1] = inp[0];
      break;

    case 8:
      outp[0] = inp[7], outp[1] = inp[6];
      outp[2] = inp[5], outp[3] = inp[4];
      outp[4] = inp[3], outp[5] = inp[2];
      outp[6] = inp[1], outp[7] = inp[0];
      break;

    default:
      for(size_t i=0; i<size; i++)
        outp[i] = inp[size-1-i];
  }

  return out;
}

/**
 * \brief Function which checks if a given file exists.
 *
 * \param [in] filename  Name of the file.
 * \return               Boolean indicating existence.
 *
 */
inline bool fileExists(std::string filename)
{
  return (access(filename.c_str(), F_OK) == 0);
}

/// return file size from a file descriptor
inline size_t file_size(FILE* fd)
{
  size_t oldpos = ftell(fd);
  fseek(fd, 0L, SEEK_END);
  size_t sz = ftell(fd);
  fseek(fd, oldpos, SEEK_SET);

  return sz;
}

/// treat a file open error by displaying the errnum string interpretation and the caller
inline void treat_file_open_error(const char* file, const char* caller,
                           const int errnum, const bool do_exit, int rank)
{
  if(rank == 0)
    fprintf(stderr, "%s: IO error occured when opening file %s.\n%s\n\n", caller, file,
            strerror(errnum));

  if(do_exit) {
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
}

}

#endif
