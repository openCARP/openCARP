// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_ABSTRACT_LIN_SOLVER_H
#define _SF_ABSTRACT_LIN_SOLVER_H

#include "SF_abstract_matrix.h"
#include "SF_abstract_vector.h"

namespace SF {

/**
 * Simple struct holding informations and operations related to linear solvers.
 *
 * @tparam T  Integer type (indices).
 * @tparam S  Floating point type (values).
 *
 * @see ginkgo_solver
 * @see petsc_solver
 *
 * @note The initialization of solvers varies hugely, so we provide a generic
 *       setup function containing redundant information for some solvers.
 */
template<class T, class S>
struct abstract_linear_solver
{
  std::string         name;         //!< the solver name
  const char*         options_file; //!< the file containing the solver options

  /** Enum representing the residual norm stopping criterion type. */
  enum norm_t {absPreResidual, absUnpreResidual, relResidual, absPreRelResidual, norm_unset};
  norm_t norm = norm_unset;

  // solver statistics
  SF_real final_residual    = -1.0;                 //!< Holds the residual after convergence
  SF_real time              = 0.0;                  //!< solver runtime
  SF_int  niter             = -1;                   //!< number of iterations
  SF_int  max_it            = 0;                    //!< max number of iterations
  SF_int  reason            = 0;                    //!< number of iterations

  /**
   * Solve the system Ax=b.
   *
   * @param x   The solution vector
   * @param b   The right hand side
   */
  virtual void operator() (abstract_vector<T,S> & x, const abstract_vector<T,S> & b) = 0;

  /** Default destructor. */
  virtual ~abstract_linear_solver() = default;

  /**
   * Setup the solver characteristics.
   *
   * @param mat            The system matrix.
   * @param tol            The tolerance to which the solver should converge.
   * @param mat_it         The maximum number of iterations for the solver.
   * @param name           The solver name.
   * @param has_nullspace  Whether there is a constant nullspace or not.
   * @param logger         A generic pointer to the logger object.
   * @param default_opts   A string representing default PETSc options. Used by petsc only.
   */
  virtual void setup_solver(abstract_matrix<T, S>& mat, double tol, int max_it,
                            short norm, std::string name, bool has_nullspace,
                            void*       logger,  // TODO: maybe solver logging needs to be revisited
                            const char* solver_opts_file,
                            const char* default_opts) = 0;

 protected:
  /**
   * Set the stopping criterion parameters for the solver.
   *
   * @param normtype       The type of residual norm.
   * @param tol            The tolerance to which the solver should converge.
   * @param mat_it         The maximum number of iterations for the solver.
   * @param verbose        Whether to print detailed informations.
   * @param logger         A generic pointer to the logger object.
   */
  virtual void set_stopping_criterion(norm_t normtype, double tol, int max_it,
                                      bool verbose, void* logger) = 0;

  /**
   * Simple helper function converting a short (number) to norm_t
   *
   * @param param_norm_type  a short number representing the norm type
   *
   * @returns the norm type as a norm_t enum
   */
  norm_t convert_param_norm_type(short param_norm_type) const
  {
    norm_t normtype = absPreResidual;

    switch (param_norm_type) {
      default:
      case 0: normtype = absPreResidual; break;
      case 1: normtype = absUnpreResidual; break;
      case 2: normtype = relResidual; break;
      case 3: normtype = absPreRelResidual; break;
    }

    return normtype;
  }
};

} // namespace SF


#endif // _SF_ABSTRACT_LIN_SOLVER_H
