// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file test.cpp
* @brief A slimfem test example.
* @author Aurel Neic
* @version 
* @date 2017-11-15
*/

#include <stdio.h>
#include <iostream>
#include <string>
#include <mpi.h>

#include "src/SF_base.h"

/**
* @brief This is an example of how a parallel mesh might be set up using the
*        functionality provided by slimfem.
*
* In this example we read a mesh which represents an extracellular mesh in a bidomain
* context. We extract the intracellular mesh based on the elements with empty fibers
* definitions.
*
* @param emesh    Extracellular mesh.
* @param imesh    Intracellular mesh
* @param basename Base name of the reference mesh we read mesh.
*/
template<class T, class S>
void setup_mesh_parallel(SF::meshdata<T, S> & emesh, SF::meshdata<T, S> & imesh, std::string basename)
{
  MPI_Comm comm = emesh.comm;

  int size, rank;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  double t1, t2, s1, s2;

  // read element mesh data
  t1 = MPI_Wtime(); s1 = t1;
  if(!rank) std::cout << "Reading extracellular mesh " << basename << ".*" << std::endl;
  SF::read_elements(emesh, basename);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  SF::vector<S> pts;
  SF::vector<T> ptsidx;
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Reading points .." << std::endl;
  SF::read_points(basename, comm, pts, ptsidx);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // extract myocard
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Extracting intracellular mesh .." << std::endl;
  SF::extract_myocardium(emesh, imesh);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  std::list< SF::meshdata<T, S>* > meshlist;
  meshlist.push_back(&emesh);
  meshlist.push_back(&imesh);

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Inserting points in all meshes .." << std::endl;
  SF::insert_points(pts, ptsidx, meshlist);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // compute parmetis partitioning
  //SF::parmetis_partitioner<T, S> partitioner(1.0001, 2);
  SF::kdtree_partitioner<T,S> partitioner;
  SF::vector<T> part;
  bool output_part = true;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Partitioning and redistributing extracellular mesh .." << std::endl;
  partitioner(emesh, part);

  if(output_part)
    write_data_ascii(emesh.comm, emesh.get_numbering(SF::NBR_ELEM_REF), part, "emesh_part.dat");

  SF::redistribute_elements(emesh, part);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Partitioning and redistributing intracellular mesh .." << std::endl;
  partitioner(imesh, part);

  if(output_part)
    write_data_ascii(imesh.comm, imesh.get_numbering(SF::NBR_ELEM_REF), part, "imesh_part.dat");

  SF::redistribute_elements(imesh, part);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Generating parallel layouts .." << std::endl;
  emesh.generate_par_layout();
  imesh.generate_par_layout();
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Renumbering intracellular mesh .." << std::endl;
  SF::submesh_numbering<T, S> sub_numbering;
  sub_numbering(imesh);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // read points
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Inserting points in all meshes again .." << std::endl;
  SF::insert_points(pts, ptsidx, meshlist);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Generating unique PETSc numberings .." << std::endl;
  if(!rank) std::cout << "Extracellular" << std::endl;
  {
    SF::petsc_numbering<T, S> petsc_numbering(emesh.pl);
    petsc_numbering(emesh);
  }
  if(!rank) std::cout << "Intracellular" << std::endl;
  {
    SF::petsc_numbering<T, S> petsc_numbering(imesh.pl);
    petsc_numbering(imesh);
  }
  t2 = MPI_Wtime(); s2 = t2;
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;
  if(!rank) std::cout << "All done in " << s2 - s1 << " sec." << std::endl;

  // debug section for reduce
  SF::vector<T> sca(imesh.l_numpts, 1);
  imesh.pl.reduce(sca, "sum");

  bool test = false;
  if(test)
  {
    const SF::vector<T> & snod = imesh.get_numbering(SF::NBR_PETSC);
    SF::vector<T>         nod  = snod;

    imesh.pl.reduce(nod, "sum");
    for(size_t i=0; i<sca.size(); i++) nod[i] /= sca[i];

    {
      bool good = true;
      for(size_t i=0; i<nod.size(); i++) {
        if(nod[i] != snod[i]) {
          good = false;
          break;
        }
      }
      if(!good) std::cerr << "Error: Nodal reduce and/or numbering is wrong!" << std::endl;
    }
  }

  // print some infos about the DDs
  if(!rank) std::cout << "================= Extracellular ===================" << std::endl;
  SF::print_DD_info(emesh);
  // SF::print_mesh_graph(emesh);
  if(!rank) std::cout << "================= Intracellular ===================" << std::endl;
  SF::print_DD_info(imesh);
  // SF::print_mesh_graph(imesh);

  // small test for indexing
  // test = true;
  if(test) {
    SF::vector<int> eidx = emesh.pl.algebraic_nodes();
    emesh.pl.globalize(eidx);

    SF::vector<int> gidx, rcnt(size, 0), rdsp(size+1, 0);
    int eisize = eidx.size();
    if(!rank) gidx.resize(emesh.g_numpts);
    MPI_Gather(&eisize, 1, MPI_INT, rcnt.data(), 1, MPI_INT, 0, comm);
    SF::dsp_from_cnt(rcnt, rdsp);
    MPI_Gatherv(eidx.data(), eidx.size(), MPI_INT, gidx.data(), rcnt.data(), rdsp.data(), MPI_INT, 0, comm);
    if(!rank) {
      binary_sort(gidx);
      unique_resize(gidx);
      bool bad = false;
      if(gidx.size() != emesh.g_numpts)
        bad = true;
      else {
        for(size_t i=0; i<emesh.g_numpts; i++)
          if( gidx[i] != int(i) ) {bad = true; break;}
      }
      if(bad)
        std::cerr << "ERROR: Renumbered index range bad!" << std::endl;
    }
  }

}

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  MPI_Comm comm = SF_COMM;

  int rank, size;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  if(argc != 2) {
    fprintf(stderr, "Error, wrong usage. Use: %s <mesh>\n", argv[0]);
    MPI_Finalize();
    return 1;
  }

  std::string base = argv[1];
  struct SF::meshdata<int, float> emesh, imesh;
  setup_mesh_parallel(emesh, imesh, base);

  // for debugging reasons we can write the mesh to harddisk
  short writeflag = 0, gwriteflag = 0;
#if 1
  if(!rank) {
    std::string answ;
    std::cout << "Write the mesh to HD ? (y/n)" << std::endl;
    std::cin >> answ;
    if(answ.compare("y") == 0) {
      std::cout << "[i]ntracellular or [e]xtracellular? (i/e)" << std::endl;
      std::cin >> answ;
      switch(answ[0])
      {
        case 'i': writeflag = 1; break;
        case 'e': writeflag = 2; break;
        default:
          std::cerr << "Invalid mesh! No output!" << std::endl;
          writeflag = 0;
          break;
      }
    }
  }
#else
  writeflag = 1;
#endif

  MPI_Allreduce(&writeflag, &gwriteflag, 1, MPI_SHORT, MPI_MAX, comm);
  if(gwriteflag) {
    std::string base = "debugMesh";
    bool write_bin = false;
    SF::meshdata<int, float> surfmesh;

    switch(gwriteflag) {
      case 1:
        SF::write_mesh_parallel(imesh, write_bin, base);
        compute_surface_mesh(imesh, SF::NBR_SUBMESH, surfmesh);
        break;

      case 2:
        SF::write_mesh_parallel(emesh, write_bin, base);
        compute_surface_mesh(emesh, SF::NBR_SUBMESH, surfmesh);
        break;
    }

    SF::write_surface(surfmesh, base + ".surf");
  }

  MPI_Finalize();
  return 0;
}
