// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file test.cpp
* @brief A slimfem test example.
* @author Aurel Neic
* @version 
* @date 2017-11-15
*/

#include <stdio.h>
#include <iostream>
#include <string>
#include <mpi.h>

#include "SF_container.h"
#include "SF_petsc_matrix.h"
#include "SF_petsc_vector.h"
#include "sf_interface.h"
#include "electric_integrators.h"

/**
* @brief This is an example of how a parallel mesh might be set up using the
*        functionality provided by slimfem.
*
* In this example we read a mesh which represents an extracellular mesh in a bidomain
* context. We extract the intracellular mesh based on the elements with empty fibers
* definitions.
*
* @param emesh    Extracellular mesh.
* @param imesh    Intracellular mesh
* @param basename Base name of the reference mesh we read mesh.
*/
template<class T, class S>
void setup_mesh_parallel(SF::meshdata<T, S> & emesh, SF::meshdata<T, S> & imesh, std::string basename)
{
  MPI_Comm comm = emesh.comm;

  int size, rank;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  double t1, t2, s1, s2;

  // read element mesh data
  t1 = MPI_Wtime(); s1 = t1;
  if(!rank) std::cout << "Reading extracellular mesh " << basename << ".*" << std::endl;
  SF::read_elements(emesh, basename);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  SF::vector<S> pts;
  SF::vector<T> ptsidx;
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Reading points .." << std::endl;
  SF::read_points(basename, comm, pts, ptsidx);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // extract myocard
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Extracting intracellular mesh .." << std::endl;
  SF::extract_myocardium(emesh, imesh);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  std::list< SF::meshdata<T, S>* > meshlist;
  meshlist.push_back(&emesh);
  meshlist.push_back(&imesh);

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Inserting points in all meshes .." << std::endl;
  SF::insert_points(pts, ptsidx, meshlist);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // compute parmetis partitioning
  //SF::parmetis_partitioner<T, S> partitioner(1.0001, 2);
  SF::kdtree_partitioner<T,S> partitioner;
  SF::vector<T> part;
  bool output_part = true;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Partitioning and redistributing extracellular mesh .." << std::endl;
  partitioner(emesh, part);

  SF::redistribute_elements(emesh, part);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Partitioning and redistributing intracellular mesh .." << std::endl;
  partitioner(imesh, part);

  SF::redistribute_elements(imesh, part);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Generating parallel layouts .." << std::endl;
  emesh.generate_par_layout();
  imesh.generate_par_layout();
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Renumbering intracellular mesh .." << std::endl;
  SF::submesh_numbering<T, S> sub_numbering;
  sub_numbering(imesh);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  // read points
  t1 = MPI_Wtime();
  if(!rank) std::cout << "Inserting points in all meshes again .." << std::endl;
  SF::insert_points(pts, ptsidx, meshlist);
  t2 = MPI_Wtime();
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;

  t1 = MPI_Wtime();
  if(!rank) std::cout << "Generating unique PETSc numberings .." << std::endl;
  if(!rank) std::cout << "Extracellular" << std::endl;
  {
    SF::petsc_numbering<T, S> petsc_numbering(emesh.pl);
    petsc_numbering(emesh);
  }
  if(!rank) std::cout << "Intracellular" << std::endl;
  {
    SF::petsc_numbering<T, S> petsc_numbering(imesh.pl);
    petsc_numbering(imesh);
  }
  t2 = MPI_Wtime(); s2 = t2;
  if(!rank) std::cout << "Done in " << t2 - t1 << " sec." << std::endl;
  if(!rank) std::cout << "All done in " << s2 - s1 << " sec." << std::endl;
}

int main(int argc, char** argv)
{
  // only send PETSc the PETSc specific options
  PetscInitialize(&argc, &argv, NULL, NULL);

  // MPI_Init(&argc, &argv);
  MPI_Comm comm = SF_COMM;

  int rank, size;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  if(argc != 2) {
    fprintf(stderr, "Error, wrong usage. Use: %s <mesh>\n", argv[0]);
    MPI_Finalize();
    return 1;
  }

  std::string base = argv[1];
  struct SF::meshdata<mesh_int_t, mesh_real_t> emesh, imesh;
  setup_mesh_parallel(emesh, imesh, base);

  // we use a seperate scope for the vec/mat so that the matrix gets destroyed
  // before PetscFinalize().
  {
    double t1, t2;
    opencarp::sf_vec* vec = nullptr;
    opencarp::sf_mat* mass = nullptr;
    int dpn = 1;

    t1 = MPI_Wtime();
    SF::init_vector_petsc(&vec);
    vec->init_common(imesh, dpn, opencarp::sf_vec::algebraic);
    SF::init_matrix_petsc(&mass);
    mass->init(imesh, dpn, dpn, 128);

    opencarp::mass_integrator mass_integrator;
    SF::assemble_matrix(*mass, imesh, mass_integrator);
    t2 = MPI_Wtime();
    if(!rank) std::cout << "Mass assembly in " << t2 - t1 << "sec." << std::endl;

    std::string output_name = "Mass.bin";
    if(!rank) std::cout << "Writing matrix to: " << output_name << std::endl;
    mass->write(output_name.c_str());
  }

  // MPI_Finalize();
  PetscFinalize();
  return 0;
}
