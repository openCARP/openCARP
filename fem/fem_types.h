// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file fem_types.h
* @brief Structs and types for (electric) FEM.
* @author Aurel Neic, Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/


#ifndef _FEM_TYPES_H
#define _FEM_TYPES_H

#include "basics.h"
#include "SF_vector.h"

namespace opencarp {

/**
* @brief identifier for physical material properties
*/
enum physMat_t {PhysMat=0, ElecMat=1, DiffuseMat=2, MechMat=3, OpticsMat=4};

/// description of electrical tissue properties
enum cond_t {
  intra_cond, extra_cond, sum_cond, para_cond
};

// typedef CondTensModifier bidmT;

/** description of physical properties 
 *  Note that PhysMat properties must be the first fields in every  
 *  property defined for each physics
 */
struct physMaterial {
  physMat_t     material_type; //!< ID of physics material
  double        fluct;         //!< random fluctuation around standard value
};


/** description of electrical tissue properties */
struct elecMaterial : physMaterial
{
  double  InVal[3];    //!< intracellular conductivity eigenvalues
  double  ExVal[3];    //!< extracellular conductivity eigenvalues
  double  BathVal[3];  //!< bath conductivity eigenvalues
  cond_t  g;           //!< rule to build conductivity tensor
  int    *augment;     //!< augmented element list
  int     nAug;        //!< number of augmented elements 
};


/** description of diffusive tissue properties */
struct diffMaterial : physMaterial
{
  double InVal[3];     //!< intracellular diffusion eigenvalues
  double ExVal[3];     //!< extracellular diffusion eigenvalues
  double BathVal[3];   //!< bath diffusion eigenvalues
  double clearance;    //!< clearance rate (mol/s)
  double basal_conc;   //!< basal concentration  
};

/** description of diffusive tissue properties */
struct opticsMaterial : physMaterial
{
  int    mode;         //!< 0=illumination, 1=emmission
  double Dil[3];       //!< diffusion coefficient illumination
  double Dem[3];       //!< diffusion coefficient emmission
  double muAil;        //!< absorption coefficient illumination
  double muAem;        //!< absorption coefficient emmission
};

// TODO: Write destructor for Regions

//! region based variations of arbitrary material parameters
struct RegionSpecs {
  char         *regname;      //!< name of region
  int           regID;        //!< region ID
  int           nsubregs;     //!< \#subregions forming this region
  int          *subregtags;   //!< FEM tags forming this region
  physMaterial *material;     //!< material parameter description
  void         *customize;    //!< any data for further customization
};


/** random parameter fluctuations */
// struct rndFluct {
//   long          rseed;        //!< random seed
//   double        var;          //!<
// };

//! description of materal properties in a mesh
struct MaterialType {
  // rndFluct      rfluct;            //!< random material variations
  // double       *fluct     = NULL;  //!< random fluctuation vector

  SF::vector<int>         regionIDs;  //!< elemental region IDs (multiple tags are grouped in one region)
  SF::vector<RegionSpecs> regions;    //!< array with region params
  SF::vector<double>      el_scale;      //!< optionally provided per-element params scale
};

}  // namespace opencarp

#endif
