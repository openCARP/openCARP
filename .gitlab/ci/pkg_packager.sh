#!/bin/bash
# 

if [ "$(uname -s)" != "Darwin" ]; then
    echo "This script only works on macOS" && exit
fi

export PKG_PACKAGER="1"

WORK_DIR=$(pwd)
BUILD_DIR=$WORK_DIR/_build_mac
INST_DIR=/usr/local/lib/opencarp

#######################################################################
#                       Xcode CommandLineTools                        #
#######################################################################
#sudo rm -rf /Library/Developer/CommandLineTools
#xcode-select --install
#sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /

#######################################################################
#                                petsc                                #
#######################################################################
PETSC_INSTALL_PREFIX=$INST_DIR/lib/petsc

if [ -d $PETSC_INSTALL_PREFIX ]; then
    echo "PETSc is installed in $PETSC_INSTALL_PREFIX"
else
    echo "Install PETSc in $PETSC_INSTALL_PREFIX"
    mkdir -p $PETSC_INSTALL_PREFIX

    cd $WORK_DIR
    curl -L https://web.cels.anl.gov/projects/petsc/download/release-snapshots/petsc-3.22.3.tar.gz | tar xz
    cd $(ls | grep petsc)
    PETSC_DIR=$(pwd)

    ./configure \
	--prefix=$PETSC_INSTALL_PREFIX \
	--download-mpich \
	--with-fc=0 \
	--download-f2cblaslapack \
	--download-metis \
	--download-parmetis \
        --download-hypre \
        --with-debugging=0 \
        --with-x=0 \
        COPTFLAGS='-O2' \
        CXXOPTFLAGS='-O2'
    make && make install
fi

export PETSC_DIR=$PETSC_INSTALL_PREFIX
export MPI_DIR=$PETSC_INSTALL_PREFIX

#######################################################################
#                              openCARP                               #
#######################################################################
cd $WORK_DIR
# OpenMP is disabled in macOS packages
cmake -S. -B$BUILD_DIR -DCMAKE_PREFIX_PATH=$MPI_DIR -DUSE_OPENMP=OFF -DDLOPEN=ON -DBUILD_EXTERNAL=ON -DCMAKE_BUILD_TYPE=Release
cmake --build $BUILD_DIR --parallel 8
cd $BUILD_DIR
cpack -G productbuild
