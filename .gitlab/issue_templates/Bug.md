<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "Bug" label:

- https://git.opencarp.org/groups/openCARP/-/issues?label_name=Bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the bug encountered concisely)

### What version of openCARP are you using?

(As detailed as possible: tagged release version / commit ID)

### What operating system and processor architecture are you using?

(As detailed as possible)

### Steps to reproduce

(First try to reproduce the issue in our docker image.)
(How can one reproduce the issue? this is very important! If files are needed, please provide a download link to a minimal working example.)


### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's tough to read otherwise.)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~Bug
