### Problem to solve with the new feature

(What problem do we solve? Why does the openCARP community need this feature?)

### Intended users

(Who will use this feature?)
(If known, include any of the following: types of users (e.g. Developer), persons, or specific roles. It's okay to write "Unknown" and fill this field in later.)

### Further details

(Include use cases, benefits, and/or goals.)

### Proposal

(How are we going to solve the problem?)
(Are there reference implementations of this feature? Is the method described in a paper? If so, please add references.)

### Testing

(What risks does this change pose? How might it affect the quality of the software? What additional changes to tests will be needed?)

### Links / references

/label ~"Feature Request"
