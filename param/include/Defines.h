// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*  Number of lines allocated at once */
#define PrMNALLOC 100

#define PrMTRUE  1
#define PrMFALSE 0

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#ifndef VRAI
#define VRAI 1
#endif
#ifndef FAUX
#define FAUX 0
#endif

/* Status codes; odd if unrunable */
#define PrMRUN      0
#define PrMCOMPILED 2
#define PrMERROR    1
#define PrMHELP     3
#define PrMFATAL    5
#define PrMQUIT     7
