// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    file_code:  Contains read_file_code() function.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  April 4, 2000

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* read_file_code()  ******************************************************/
/*
    Prints code for reading a variable in a file.
*/
void read_file_code(FILE* file, Variable* variable, const char* tabs, Boolean compiled)
{
  int  a_level, bound_level, dep_number, index, level, min, max;
  char format[257], *last_tab, *match, name[257], tabs0[81], tabs1[81], tabs2[81], tabs3[81],
      tabs4[81], *vname;
  Value*       bound;
  Variable*    dep_variable;
  extern char *Type_Formats[], *Type_Names[];

  if (variable->description->output || variable->description->auxiliary ||
      variable->description->hidden)
    return;

  if (compiled) {
    if (variable->description->kind != COMPILED) return;
  }

  if (tabs[strlen(tabs) - 1] == '\t') {
    strcpy(tabs0, tabs);
    strcpy(tabs1, tabs0);
    strcat(tabs1, "    ");
    strcpy(tabs2, tabs0);
    strcat(tabs2, "\t");
    strcpy(tabs3, tabs0);
    strcat(tabs3, "\t    ");
    strcpy(tabs4, tabs0);
    strcat(tabs4, "\t\t");
  } else {
    strcpy(tabs0, tabs);
    last_tab = strrchr(tabs0, '\t');
    if (last_tab != NULL) {
      last_tab++;
      *last_tab = '\000';
    } else
      tabs0[0] = '\000';
    strcpy(tabs1, tabs0);
    strcat(tabs1, "\t");
    strcpy(tabs2, tabs0);
    strcat(tabs2, "\t    ");
    strcpy(tabs3, tabs0);
    strcat(tabs3, "\t\t");
    strcpy(tabs4, tabs0);
    strcat(tabs4, "\t\t    ");
    strcpy(tabs0, tabs);
  }

  if (variable->description->type->type == BYTE) {
    min = 0;
    max = 255;
  } else if (variable->description->type->type == CHAR) {
    min = -128;
    max = 127;
  } else if (variable->description->type->type == SHORT) {
    min = -32768;
    max = 32767;
  }
  index   = variable->description->type->type - TYPE_BASE;
  a_level = variable->array_level;
  strcpy(format, variable->format);
  if (variable->description->key)
    vname = variable->description->key;
  else
    vname = variable->name;
  if (variable->description->storage != SCALAR) {
    strcat(format, "[%d]");
    snprintf(name, sizeof name, "%s[PrMelem%d]", vname, a_level);
  } else
    strcpy(name, vname);

  /*
      Code to read a scalar or array element
  */
  if (a_level) {
    fprintf(file, "%selse if (1==sscanf(PrMwords[0],\n", tabs0);
    fprintf(file, "%s \"%s\", PrMspace)) {\n", tabs0, variable->match);
    fprintf(file, "%ssscanf(PrMwords[0], \"%s\"\n%s ", tabs1, format, tabs1);
    for (level = 1; level <= a_level; level++) fprintf(file, ", &PrMelem%d", level);
    fprintf(file, ");\n");
    for (level = 1; level <= a_level; level++) {
      bound = variable->bounds;
      for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
      fprintf(file, "%sif (PrMelem%d>=%s || PrMelem%d<0) {\n", tabs1, level, bound->data, level);
      fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
      fprintf(file, "%s \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n", tabs2,
              level);
      fprintf(file, "%s PrMelem%d, PrMwords[0], %s-1);\n", tabs2, level, bound->data);
      fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
      fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
      fprintf(file, "%s}\n", tabs1);
    }
  } else {
    fprintf(file, "%selse if (0==strcmp(PrMwords[0], \"%s \")) {\n", tabs0, vname);
  }
  if (variable->description->type->type != FLAG) {
    fprintf(file, "%sif (PrMnwords<2) {\n", tabs1);
    fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
    fprintf(file, "%s \"\\n*** Missing argument after %%s\\n\\n\", PrMwords[0]);\n", tabs2);
    fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
    fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
    fprintf(file, "%s}\n", tabs1);
  }

  dep_number   = 0;
  dep_variable = variable->dependents;
  while (dep_variable) {
    fprintf(file, "%sPrMold_size%d = %s;\n", tabs1, dep_number, dep_variable->bottom_bound->data);
    dep_number++;
    dep_variable = dep_variable->next;
  }

  switch (variable->description->type->type) {
    case FLAG:
      fprintf(file, "%s%s = PrMTRUE;", tabs1, name);
      fprintf(file, "%sif (PrMnwords>=2) {\n", tabs1);
      fprintf(file, "%sPrMlowercase(PrMwords[1]);\n", tabs2);
      fprintf(file, "%sif (0==strcmp(PrMwords[1], \"true \"))\n", tabs2);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs3, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"vrai \"))\n", tabs2);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs3, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"1 \"))\n", tabs2);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs3, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"false \"))\n", tabs2);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs3, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"faux \"))\n", tabs2);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs3, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"0 \"))\n", tabs2);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs3, name);
      fprintf(file, "%s}\n", tabs1);
      break;
    case BOOLEAN:
      fprintf(file, "%sPrMlowercase(PrMwords[1]);\n", tabs1);
      fprintf(file, "%sif (0==strcmp(PrMwords[1], \"true \"))\n", tabs1);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs2, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"vrai \"))\n", tabs1);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs2, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"1 \"))\n", tabs1);
      fprintf(file, "%s%s = PrMTRUE;\n", tabs2, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"false \"))\n", tabs1);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs2, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"faux \"))\n", tabs1);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs2, name);
      fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"0 \"))\n", tabs1);
      fprintf(file, "%s%s = PrMFALSE;\n", tabs2, name);
      fprintf(file, "%selse {\n", tabs1);
      break;
    case BYTE:
    case CHAR:
    case SHORT:
      fprintf(file, "%sif (1==sscanf(PrMwords[1], \"%%d\", &PrMint_arg)) {\n", tabs1);
      fprintf(file, "%sif (PrMint_arg<%d || PrMint_arg>%d) {\n", tabs2, min, max);
      fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs3);
      fprintf(file, "%s \"\\n*** Invalid %s value %%d for %%s\\n\\n\",\n", tabs3,
              Type_Names[index]);
      fprintf(file, "%s PrMint_arg, PrMwords[0]);\n", tabs3);
      fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs3);
      fprintf(file, "%sreturn(PrMTRUE);\n", tabs3);
      fprintf(file, "%s}\n", tabs2);
      fprintf(file, "%selse {\n", tabs2);
      fprintf(file, "%s%s = PrMint_arg;\n", tabs3, name);
      fprintf(file, "%s}\n", tabs2);
      fprintf(file, "%s}\n", tabs1);
      fprintf(file, "%selse {\n", tabs1);
      break;
    case COMPLEX:
    case D_COMPLEX:
      fprintf(file, "%sif (3!=sscanf(PrMwords[1], %s, &%s.real, &%s.imag, junk)) {\n", tabs1,
              Type_Formats[index], name, name);
      break;
    case RDIR:
    case RWDIR:
    case WDIR:
    case RFILE:
    case RWFILE:
    case WFILE:
    case STRING:
    case EXPR:
      fprintf(file, "%sPrMwords[1][strlen(PrMwords[1])-1] = '\\000';\n", tabs1);
      if (variable->description->length) {
        fprintf(file, "%sstrncpy(%s, PrMwords[1], %d-1);\n", tabs1, name,
                variable->description->length);
        fprintf(file, "%sif (strlen(PrMwords[1])>%d-1) {\n", tabs1, variable->description->length);
      } else {
        fprintf(file, "%s%s = (char*)realloc(%s, strlen(PrMwords[1])+1);\n", tabs1, name, name);
        fprintf(file, "%sif (NULL==strcpy(%s, PrMwords[1])) {\n", tabs1, name);
      }
      break;
    default:
      fprintf(file, "%sif (1!=sscanf(PrMwords[1], %s, &%s)) {\n", tabs1, Type_Formats[index], name);
      break;
  } /* switch (variable->description->type->type) */
  if (variable->description->type->type != FLAG) {
    fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
    fprintf(file, "%s \"\\n*** Error reading value %%s for %%s\\n\\n\", \n", tabs2);
    fprintf(file, "%s PrMwords[1], PrMwords[0]);\n", tabs2);
    fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
    fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
    fprintf(file, "%s}\n", tabs1);
  }
  if (!compiled) {
    if (variable->description->storage == SCALAR)
      fprintf(file, "%s%s = PrMTRUE;\n", tabs1, variable->boolean);
    else
      fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs1, variable->boolean, a_level);
  }

  /*
      Code to realloc dependents variables
  */
  dep_number   = 0;
  dep_variable = variable->dependents;
  while (dep_variable) {
    realloc_code(file, dep_variable, tabs1, dep_number);
    dep_number++;
    dep_variable = dep_variable->next;
  }
  /*
      Code to reinitialize default_ctl variables
  */
  dep_variable = variable->default_ctl;
  while (dep_variable) {
    reinitial_code(file, dep_variable, tabs1);
    dep_variable = dep_variable->next;
  }
  fprintf(file, "%s}\n", tabs0);

  /*
      Code to read an array
  */
  if (variable->description->storage != SCALAR) {
    match = substitute_all(variable->format, "%d", "%*d");
    strcpy(format, match);
    if (match != variable->format) free(match);
    strcat(format, "%[ ]");
    match = format;
    if (a_level > 1) {
      fprintf(file, "%selse if (1==sscanf(PrMwords[0],\n", tabs0);
      fprintf(file, "%s \"%s\", PrMspace)) {\n", tabs0, match);
      fprintf(file, "%ssscanf(PrMwords[0], \"%s\"\n%s ", tabs1, variable->format, tabs1);
      for (level = 1; level < a_level; level++) fprintf(file, ", &PrMelem%d", level);
      fprintf(file, ");\n");
      for (level = 1; level < a_level; level++) {
        bound = variable->bounds;
        for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
        fprintf(file, "%sif (PrMelem%d>=%s || PrMelem%d<0) {\n", tabs1, level, bound->data, level);
        fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
        fprintf(file, "%s \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n",
                tabs2, level);
        fprintf(file, "%s PrMelem%d, PrMwords[0], %s-1);\n", tabs2, level, bound->data);
        fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
        fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
        fprintf(file, "%s}\n", tabs1);
      }
    } else {
      fprintf(file, "%selse if (0==strcmp(PrMwords[0], \"%s \")) {\n", tabs0, vname);
    }
    fprintf(file, "%sif (PrMnwords<2) {\n", tabs1);
    fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
    fprintf(file, "%s \"\\n*** Missing argument after %s\\n\\n\");\n", tabs2, vname);
    fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
    fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
    fprintf(file, "%s}\n", tabs1);
    fprintf(file, "%sif (PrMnwords>2 && PrMnwords<1+%s) {\n", tabs1, variable->bounds->data);
    fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs2);
    fprintf(file, "%s\"\\n*** %%d argument(s) missing after %s\\n\\n\",\n", tabs2, vname);
    fprintf(file, "%s 1+%s-PrMnwords);\n", tabs2, variable->bounds->data);
    fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs2);
    fprintf(file, "%sreturn(PrMTRUE);\n", tabs2);
    fprintf(file, "%s}\n", tabs1);

    dep_number   = 0;
    dep_variable = variable->dependents;
    while (dep_variable) {
      fprintf(file, "%sPrMold_size%d = %s;\n", tabs1, dep_number, dep_variable->bottom_bound->data);
      dep_number++;
      dep_variable = dep_variable->next;
    }

    /*
  Code to read different values for each array element
    */
    fprintf(file, "%sif (PrMnwords>2) for (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs1,
            a_level, a_level, variable->bounds->data, a_level);
    switch (variable->description->type->type) {
      case FLAG:
      case BOOLEAN:
        fprintf(file, "%sPrMlowercase(PrMwords[1+PrMelem%d]);\n", tabs2, a_level);
        fprintf(file, "%sif (0==strcmp(PrMwords[1+PrMelem%d], \"true \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1+PrMelem%d], \"vrai \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1+PrMelem%d], \"1 \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1+PrMelem%d], \"false \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMFALSE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1+PrMelem%d], \"faux \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMFALSE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1+PrMelem%d], \"0 \"))\n", tabs2, a_level);
        fprintf(file, "%s%s[PrMelem%d] = PrMFALSE;\n", tabs3, vname, a_level);
        fprintf(file, "%selse {\n", tabs2);
        break;
      case BYTE:
      case CHAR:
      case SHORT:
        fprintf(file, "%sif (1==sscanf(PrMwords[1+PrMelem%d], \"%%d\", \n", tabs2, a_level);
        fprintf(file, "%s &PrMint_arg)) {\n", tabs2);
        fprintf(file, "%sif (PrMint_arg<%d || PrMint_arg>%d) {\n", tabs3, min, max);
        fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs4);
        fprintf(file, "%s \"\\n*** Invalid %s value %%d for %s[PrMelem%d]\\n\\n\",\n", tabs4,
                Type_Names[index], vname, a_level);
        fprintf(file, "%s PrMint_arg);\n", tabs4);
        fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs4);
        fprintf(file, "%sreturn(PrMTRUE);\n", tabs4);
        fprintf(file, "%s}\n", tabs3);
        fprintf(file, "%selse {\n", tabs3);
        fprintf(file, "%s%s[PrMelem%d] = PrMint_arg;\n", tabs4, vname, a_level);
        fprintf(file, "%s}\n", tabs3);
        fprintf(file, "%s}\n", tabs2);
        fprintf(file, "%selse {\n", tabs2);
        break;
      case COMPLEX:
      case D_COMPLEX:
        fprintf(file, "%sif (3!=sscanf(PrMwords[1+PrMelem%d],", tabs2, a_level);
        fprintf(file, "%s %s, &%s[PrMelem%d].real, &%s[PrMelem%d].imag, junk)) {\n", tabs2,
                Type_Formats[index], vname, a_level, vname, a_level);
        break;
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        fprintf(file, "%sPrMwords[1+PrMelem%d][strlen(PrMwords[1+PrMelem%d])-1] = '\\000';\n",
                tabs2, a_level, a_level);
        if (variable->description->length) {
          fprintf(file, "%sstrncpy(%s[PrMelem%d], PrMwords[1+PrMelem%d], %d-1);\n", tabs2, vname,
                  a_level, a_level, variable->description->length);
          fprintf(file, "%sif (strlen(PrMwords[1+PrMelem%d])>%d-1) {\n", tabs2, a_level,
                  variable->description->length);
        } else {
          fprintf(file, "%s%s[PrMelem%d] = \n", tabs2, vname, a_level);
          fprintf(file, "%s (char*)realloc(%s[PrMelem%d], strlen(PrMwords[1+PrMelem%d])+1);\n",
                  tabs2, vname, a_level, a_level);
          fprintf(file, "%sif (NULL==\n", tabs2);
          fprintf(file, "%s strcpy(%s[PrMelem%d], PrMwords[1+PrMelem%d])) {\n", tabs2, vname,
                  a_level, a_level);
        }
        break;
      default:
        fprintf(file, "%sif (1!=\n", tabs2);
        fprintf(file, "%s sscanf(PrMwords[1+PrMelem%d], %s, &%s[PrMelem%d])) {\n", tabs2, a_level,
                Type_Formats[index], vname, a_level);
        break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs3);
    fprintf(file, "%s \"\\n*** Error reading value %%s for %%s\\n\\n\",\n", tabs3);
    fprintf(file, "%s PrMwords[1+PrMelem%d], PrMwords[0]);\n", tabs3, a_level);
    fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs3);
    fprintf(file, "%sreturn(PrMTRUE);\n", tabs3);
    fprintf(file, "%s}\n", tabs2);
    if (!compiled) {
      fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs2, variable->boolean, a_level);
    }
    fprintf(file, "%s}\n", tabs1);
    /*
  Code to read a single value and assign it to the entire array
    */
    fprintf(file, "%selse {\n", tabs1);
    switch (variable->description->type->type) {
      case FLAG:
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs2, vname);
        fprintf(file, "%sif (PrMnwords>=2) {\n", tabs2);
        fprintf(file, "%sPrMlowercase(PrMwords[1]);\n", tabs3);
        fprintf(file, "%sif (0==strcmp(PrMwords[1], \"true \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs4, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"vrai \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs4, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"1 \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs4, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"false \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs4, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"faux \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs4, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"0 \"))\n", tabs3);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs4, vname);
        fprintf(file, "%s}\n", tabs2);
        break;
      case BOOLEAN:
        fprintf(file, "%sPrMlowercase(PrMwords[1]);\n", tabs2);
        fprintf(file, "%sif (0==strcmp(PrMwords[1], \"true \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs3, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"vrai \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs3, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"1 \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs3, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"false \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs3, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"faux \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs3, vname);
        fprintf(file, "%selse if (0==strcmp(PrMwords[1], \"0 \"))\n", tabs2);
        fprintf(file, "%s%s[0] = PrMFALSE;\n", tabs3, vname);
        fprintf(file, "%selse {\n", tabs2);
        break;
      case BYTE:
      case CHAR:
      case SHORT:
        fprintf(file, "%sif (1==sscanf(PrMwords[1], \"%%d\", &PrMint_arg)) {\n", tabs2);
        fprintf(file, "%sif (PrMint_arg<%d || PrMint_arg>%d) {\n", tabs3, min, max);
        fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs4);
        fprintf(file, "%s \"\\n*** Invalid %s value %%d for %s\\n\\n\",\n", tabs4,
                Type_Names[index], vname);
        fprintf(file, "%s PrMint_arg);\n", tabs4);
        fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs4);
        fprintf(file, "%sreturn(PrMTRUE);\n", tabs4);
        fprintf(file, "%s}\n", tabs3);
        fprintf(file, "%selse {\n", tabs3);
        fprintf(file, "%s%s[0] = PrMint_arg;\n", tabs4, vname);
        fprintf(file, "%s}\n", tabs3);
        fprintf(file, "%s}\n", tabs2);
        fprintf(file, "%selse {\n", tabs2);
        break;
      case COMPLEX:
      case D_COMPLEX:
        fprintf(file, "%sif (3!=sscanf(PrMwords[1],", tabs2);
        fprintf(file, "%s %s, &%s[0].real, &%s[0].imag, junk)) {\n", tabs2, Type_Formats[index],
                vname, vname);
        break;
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        fprintf(file, "%sPrMwords[1][strlen(PrMwords[1])-1] = '\\000';\n", tabs2);
        if (variable->description->length) {
          fprintf(file, "%sstrncpy(%s[0], PrMwords[1], %d-1);\n", tabs2, vname,
                  variable->description->length);
          fprintf(file, "%sif (strlen(PrMwords[1])>%d-1) {\n", tabs2,
                  variable->description->length);
        } else {
          fprintf(file, "%s%s[0] = (char*)realloc(%s[0], strlen(PrMwords[1])+1);\n", tabs2, vname,
                  vname);
          fprintf(file, "%sif (NULL==strcpy(%s[0], PrMwords[1])) {\n", tabs2, vname);
        }
        break;
      default:
        fprintf(file, "%sif (1!=sscanf(PrMwords[1], %s, &%s[0])) {\n", tabs2, Type_Formats[index],
                vname);
        break;
    } /* switch (variable->description->type->type) */
    if (variable->description->type->type != FLAG) {
      fprintf(file, "%ssnprintf(PrMlasterror, sizeof PrMlasterror,\n", tabs3);
      fprintf(file, "%s \"\\n*** Error reading value %%s for %%s\\n\\n\",\n", tabs3);
      fprintf(file, "%sPrMwords[1], PrMwords[0]);\n", tabs3);
      fprintf(file, "%sPrMadderror(PrMlasterror);\n", tabs3);
      fprintf(file, "%sreturn(PrMTRUE);\n", tabs3);
      fprintf(file, "%s}\n", tabs2);
    }
    if (!compiled) { fprintf(file, "%s%s[0] = PrMTRUE;\n", tabs2, variable->boolean); }
    fprintf(file, "%sfor (PrMelem%d=1; PrMelem%d<%s; PrMelem%d++) {\n", tabs2, a_level, a_level,
            variable->bounds->data, a_level);
    switch (variable->description->type->type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->length) {
          fprintf(file, "%sstrncpy(%s[PrMelem%d], %s[0], %d-1);\n", tabs3, vname, a_level, vname,
                  variable->description->length);
        } else {
          fprintf(file, "%sfree(%s[PrMelem%d]);\n", tabs3, vname, a_level);
          fprintf(file, "%s%s[PrMelem%d] = PrMnewstr(%s[0]);\n", tabs3, vname, a_level, vname);
        }
        break;
      default: fprintf(file, "%s%s[PrMelem%d] = %s[0];\n", tabs3, vname, a_level, vname); break;
    }
    if (!compiled) {
      fprintf(file, "%s%s[PrMelem%d] = PrMTRUE;\n", tabs3, variable->boolean, a_level);
    }
    fprintf(file, "%s}\n", tabs2);
    fprintf(file, "%s}\n", tabs1);
    /*
  Code to realloc dependents variables
    */
    dep_number   = 0;
    dep_variable = variable->dependents;
    while (dep_variable) {
      realloc_code(file, dep_variable, tabs1, dep_number);
      dep_number++;
      dep_variable = dep_variable->next;
    }
    /*
  Code to reinitialize default_ctl variables
    */
    dep_variable = variable->default_ctl;
    while (dep_variable) {
      reinitial_code(file, dep_variable, tabs1);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "%s}\n", tabs0);
  } /* if (variable->description->storage!=SCALAR) */
}
