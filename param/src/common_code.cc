// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

common_code:Prints code for common functions for
the text interface and the Motif interface.

Author:  Andre Bleau, eng.

Laboratoire de Modelisation Biomedicale
Institut de Genie Biomedical
Ecole Polytechnique / Faculte de Medecine
Universite de Montreal

Revision:  April 4, 2000

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

void read_file_code(FILE*, Variable*, const char*, Boolean);
void save_code(FILE*, Variable*, int, Boolean);
void valid_code(FILE*, Variable*);

/* common_code()    ******************************************************/
/*
   Prints code for common functions for
   the text interface and the Motif interface.
   */

void common_code(FILE* file, Parameters* parameters, char* autosave)
{
  extern int c_dialect;
  int        level;
  Variable * dyn_variable, *variable;
  Paramgrp*  paramgrp;
  Function*  function;
  Arg*       arg;

  fprintf(file, "/* PrMconstructnames()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    int\t\tPrMconstructnames(void)\n\n");
  } else {
    fprintf(file, "static\t    int\t\tPrMconstructnames()\n\n");
  }
  fprintf(file, "{\n\n");
  fprintf(file, "\t    int\n");
  for (level = 1; level <= parameters->max_array_level; level++)
    fprintf(file, "\tPrMelem%d,\n", level);
  fprintf(file, "\tPrMerror = PrMFALSE;\n");
  fprintf(file, "\t    char\n");
  if (parameters->n_mandatories) fprintf(file, "\tPrMlasterror[257],\n");
  fprintf(file, "\t*PrMfile_name;\n\n");
  variable = parameters->param_list;
  while (variable) {
    file_name_code(file, variable);
    variable = variable->next;
  }
  fprintf(file, "\n");
  fprintf(file, "return(PrMerror);\n\n");
  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMdefaults()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    void\t\tPrMdefaults(void)\n\n");
  } else {
    fprintf(file, "static\t    void\t\tPrMdefaults()\n\n");
  }
  fprintf(file, "{\n\n");
  for (level = 1; level <= parameters->max_array_level; level++) {
    if (level == 1) fprintf(file, "\t    int\n");
    if (level < parameters->max_array_level)
      fprintf(file, "\tPrMelem%d,\n", level);
    else
      fprintf(file, "\tPrMelem%d;\n", level);
  }
  if (parameters->dyn_vec_list) {
    fprintf(file, "\t    int\n");
#ifdef OLD
    for (level = 0; level <= parameters->max_depend_level; level++) {
      fprintf(file, "\tPrMold_size%d,\n", level);
    }
#endif
    fprintf(file, "\tPrMnew_size;\n");
  }
  fprintf(file, "static\t    int\n");
  fprintf(file, "\tPrMfirst = PrMTRUE;\n");

  fprintf(file, "/*\n");
  fprintf(file, "    Initialization of variables and locations\n");
  fprintf(file, "*/\n");
  level = 0;
  while (level <= parameters->max_depend_level) {
    fprintf(file, "/*  Dependence level %d */\n", level);
    variable = parameters->param_list;
    while (variable) {
      initial_code(file, variable, "  ", level);
      variable = variable->next;
    }
    fprintf(file, "\n");
    if (parameters->dyn_vec_list) {
      fprintf(file, "if (PrMfirst) {\n");
      fprintf(file, "    /*\n");
      fprintf(file, "\tFirst allocation and initialization of dynamic vectors of level %d\n",
              level);
      fprintf(file, "    */\n");
      variable = parameters->dyn_vec_list;
      while (variable) {
        alloc_code(file, variable, "    ", level);
        variable = variable->next;
      }
      fprintf(file, "} /* if (PrMfirst) */\n");
#ifdef OLD
      fprintf(file, "else {\n");
      fprintf(file, "    /*\n");
      fprintf(file, "\tReallocation and initialization of dynamic vectors of level %d\n", level);
      fprintf(file, "    */\n");
      variable = parameters->dyn_vec_list;
      while (variable) {
        realloc_code(file, variable, "    ", level);
        variable = variable->next;
      }
      fprintf(file, "} /* else (PrMfirst) */\n\n");
#endif
    }
    level++;
  }

  fprintf(file, "if (PrMfirst)\n");
  fprintf(file, "    PrMfirst = PrMFALSE;\n");

  /*
     Print code to reset compiled parameters modfication
     */
  variable = parameters->param_list;
  while (variable) {
    if (variable->description->kind == COMPILED)
      fprintf(file, "%s = PrMFALSE;\n", variable->boolean);
    variable = variable->next;
  }

  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMreadfile()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    int\t\tPrMreadfile(char *PrMfilename, char **PrMsave)\n\n");
  } else {
    fprintf(file, "static\t    int\t\tPrMreadfile(PrMfilename, PrMsave)\n\n");
    fprintf(file, "char\t*PrMfilename;\n");
    fprintf(file, "char\t**PrMsave;\n\n");
  }

  fprintf(file, "{\n\n");

  fprintf(file, "\t    int\n");
  if (parameters->n_short_ints > 0) fprintf(file, "\tPrMint_arg,\n");
  for (level = 1; level <= parameters->max_array_level; level++)
    fprintf(file, "\tPrMelem%d,\n", level);
  fprintf(file, "\tPrMlineno,\n");
  for (level = 0; level < parameters->max_dependents; level++)
    fprintf(file, "\tPrMold_size%d,\n", level);
  if (parameters->dyn_vec_list) fprintf(file, "\tPrMnew_size,\n");
  fprintf(file, "\tPrMnwords;\n");
  fprintf(file, "\t    BooleaN\n");
  fprintf(file, "\tPrMcontinuation,\n");
  fprintf(file, "\tPrMerror = PrMFALSE;\n");
  fprintf(file, "\t    Text\n");
  fprintf(file, "\tPrMwords;\n");
  fprintf(file, "\t    char\n");
  fprintf(file, "\t*PrMcomment,\n");
  fprintf(file, "\t*PrMeol,\n");
  fprintf(file, "\tPrMlasterror[257],\n");
  fprintf(file, "\tPrMline[2049],\n");
  fprintf(file, "\t*PrMmore_lines,\n");
  fprintf(file, "\t*PrMword;\n");
  if (parameters->max_array_level > 0) {
    fprintf(file, "\t    char\n");
    fprintf(file, "\tPrMspace[81];\n");
  }
  fprintf(file, "\t    FILE\n");
  fprintf(file, "\t*PrMfile;\n");

  fprintf(file, "PrMclearerrors();\n");
  fprintf(file, "PrMwords = PrMnewtext();\n\n");

  fprintf(file, "if (strcmp(PrMfilename, \"-\")==0)\n");
  fprintf(file, "    PrMfile = stdin;\n");
  fprintf(file, "else\n");
  fprintf(file, "    PrMfile = fopen(PrMfilename, \"r\");\n");
  fprintf(file, "if (PrMfile==NULL) {\n");
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \"\\n*** Cannot open file %%s\\n\",\n");
  fprintf(file, "     PrMfilename);\n");
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
#if (SYSTEME == SUNOS)
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n");
#else
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \"*** %%s\\n\", strerror(errno));\n");
#endif
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
  fprintf(file, "    return(PrMTRUE);\n");
  fprintf(file, "}\n");
  fprintf(file, "PrMlineno = 0;\n");
  fprintf(file, "while (PrMTRUE) {\n");
  fprintf(file, "    PrMnwords = 0;\n");
  fprintf(file, "    PrMwords[0] = NULL;\n");
  fprintf(file, "    do {\n");
  fprintf(file, "\tPrMcontinuation = PrMFALSE;\n");
  fprintf(file, "\tPrMmore_lines = fgets(PrMline, 2048, PrMfile);\n");
  fprintf(file, "\tPrMeol = strrchr(PrMline, '\\n');\n");
  fprintf(file, "\tPrMcomment = strchr(PrMline, '#');\n");
  fprintf(file, "\tif (PrMcomment)\n");
  fprintf(file, "\t    PrMeol = PrMcomment;\n");
  fprintf(file, "\tif (PrMeol) {\n");
  fprintf(file, "\t    *PrMeol-- = '\\000';\n");
  fprintf(file, "\t    if (PrMeol==PrMline-1 || *PrMeol=='\\\\')\n");
  fprintf(file, "\t    PrMcontinuation = PrMTRUE;\n");
  fprintf(file, "\t}\n");
  fprintf(file, "\tPrMquote(PrMline);\n");
  fprintf(file, "\tPrMword = strtok(PrMline, \" \\t=,\\\\\");\n");
  fprintf(file, "\twhile (PrMword) {\n");
  fprintf(file, "\t    PrMunquote(PrMword);\n");
  fprintf(file, "\t    PrMnwords++;\n");
  fprintf(file, "\t    PrMwords = PrMaddline(PrMwords, PrMword);\n");
  fprintf(file, "\t    if (PrMnwords==1) PrMword = strtok(NULL, \" \\t=,\\\\\");\n");
  fprintf(file, "\t    else              PrMword = strtok(NULL, \" \\t,\\\\\");\n");
  fprintf(file, "\t}\n");
  fprintf(file, "\tPrMlineno++;\n");
  fprintf(file, "    } while ((PrMcontinuation || !PrMwords[0]) && PrMmore_lines);\n");
  fprintf(file, "    if (NULL==PrMmore_lines) break;\n");
  fprintf(file, "    if (0==strcmp(PrMwords[0], \"+Save\")) {\n");
  fprintf(file, "#ifdef AUTOSAVE\n");
  fprintf(file, "\tsnprintf(PrMlasterror, sizeof PrMlasterror,\n");
  fprintf(file, "\t \"\\n*** Save file may not be changed from %s\\n\\n\");\n", autosave);
  fprintf(file, "\tPrMadderror(PrMlasterror);\n");
  fprintf(file, "\treturn(PrMTRUE);\n");
  fprintf(file, "#else\n");
  fprintf(file, "\tif (PrMnwords<2) {\n");
  fprintf(file, "\t    snprintf(PrMlasterror, sizeof PrMlasterror, \"\\n*** Missing argument after +Save\\n\\n\");\n");
  fprintf(file, "\t    PrMadderror(PrMlasterror);\n");
  fprintf(file, "\t    return(PrMTRUE);\n");
  fprintf(file, "\t}\n");
  fprintf(file, "\t*PrMsave = PrMnewstr(PrMwords[1]);\n");
  fprintf(file, "#endif\n");
  fprintf(file, "    } /* if (0==strcmp(PrMwords[0], \"+Save\")) */\n");

  /*
     Loop to print code that tries to match each parameter in turn
     to words read in a file
     */
  variable = parameters->param_list;
  while (variable) {
    read_file_code(file, variable, "    ", 0);
    variable = variable->next;
  }

  fprintf(file, "    else {\n");
  fprintf(file, "\tsnprintf(PrMlasterror, sizeof PrMlasterror,\n");
  fprintf(file, "\t \"\\n*** Unrecognized keyword %%s\\n\\n\", PrMwords[0]);\n");
  fprintf(file, "\tPrMadderror(PrMlasterror);\n");
  fprintf(file, "\treturn(PrMTRUE);\n");
  fprintf(file, "    }\n\n");
  fprintf(file, "    for (int i = 0; i<PrMnwords; i++)\n");
  fprintf(file, "      free(PrMwords[i]);\n");
  fprintf(file, "} /* while (PrMTRUE) */\n\n");

  fprintf(file, "free(PrMwords);\n");
  fprintf(file, "if (PrMfile!=stdin) fclose(PrMfile);\n\n");

  fprintf(file, "return(PrMerror);\n\n");

  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMsavefile()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    int\t\tPrMsavefile(char *PrMfilename, char *PrMprogname)\n\n");
  } else {
    fprintf(file, "static\t    int\t\tPrMsavefile(PrMfilename, PrMprogname)\n\n");
    fprintf(file, "char\t*PrMfilename;\n");
    fprintf(file, "char\t*PrMprogname;\n\n");
  }

  fprintf(file, "{\n\n");

  for (level = 1; level <= parameters->max_array_level; level++) {
    if (level == 1) fprintf(file, "\t    int\n");
    if (level < parameters->max_array_level)
      fprintf(file, "\tPrMelem%d,\n", level);
    else
      fprintf(file, "\tPrMelem%d;\n", level);
  }
  if (parameters->n_mandatories > 0) {
    fprintf(file, "\t    int\n");
    fprintf(file, "\tPrMerror;\n");
  }
  fprintf(file, "\t    time_t\n");
  fprintf(file, "\tPrMtime;\n");
  fprintf(file, "\t    char\n");
  fprintf(file, "\tPrMlasterror[257];\n");
  fprintf(file, "\t    FILE\n");
  fprintf(file, "\t*PrMsave_file;\n\n");

  fprintf(file, "if (strcmp(PrMfilename, \"-\")==0)\n");
  fprintf(file, "    PrMsave_file = stdout;\n");
  fprintf(file, "else\n");
  fprintf(file, "    PrMsave_file = fopen(PrMfilename, \"w\");\n");
  fprintf(file, "if (PrMsave_file==NULL) {\n");
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \n");
  fprintf(file, "     \"\\n*** Error opening save file %%s for writing\\n\",\n");
  fprintf(file, "     PrMfilename);\n");
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
#if (SYSTEME == SUNOS)
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n");
#else
  fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \"*** %%s\\n\", strerror(errno));\n");
#endif
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
  fprintf(file, "    return(PrMTRUE);\n");
  fprintf(file, "}\n");
  fprintf(file, "PrMtime = time(NULL);\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  fprintf(file, "fprintf(PrMsave_file, \"# %%s\\n\", PrMprogname);\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  fprintf(file, "fprintf(PrMsave_file, \"# %%s\", ctime(&PrMtime));\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  level = 0;
  while (level <= parameters->max_depend_level) {
    fprintf(file, "/*  Dependence level %d */\n", level);
    variable = parameters->param_list;
    while (variable) {
      save_code(file, variable, level, 0);
      variable = variable->next;
    }
    level++;
  }
  fprintf(file, "if (PrMsave_file!=stdout) fclose(PrMsave_file);\n\n");

  fprintf(file, "return(PrMFALSE);\n\n");

  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMstripnames()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    void\t\tPrMstripnames(void)\n\n");
  } else {
    fprintf(file, "static\t    void\t\tPrMstripnames()\n\n");
  }
  fprintf(file, "{\n\n");
  for (level = 1; level <= parameters->max_array_level; level++) {
    if (level == 1) fprintf(file, "\t    int\n");
    if (level < parameters->max_array_level)
      fprintf(file, "\tPrMelem%d,\n", level);
    else
      fprintf(file, "\tPrMelem%d;\n", level);
  }
  if (parameters->n_mandatories > 0) {
    fprintf(file, "\t    int\n");
    fprintf(file, "\tPrMerror;\n");
  }
  fprintf(file, "\t    char\n");
  fprintf(file, "\t*PrMfile_name;\n\n");
  variable = parameters->param_list;
  while (variable) {
    strip_name_code(file, variable);
    variable = variable->next;
  }
  fprintf(file, "\n");
  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMvalid()\t\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static\t    int\t\tPrMvalid(void)\n\n");
  } else {
    fprintf(file, "static\t    int\t\tPrMvalid()\n\n");
  }
  fprintf(file, "{\n\n");
  fprintf(file, "\t    int\n");
  for (level = 1; level <= parameters->max_array_level; level++)
    fprintf(file, "\tPrMelem%d,\n", level);
  fprintf(file, "\tPrMerror = PrMFALSE;\n");
  fprintf(file, "\t    char\n");
  fprintf(file, "\tPrMlasterror[257];\n");
  fprintf(file, "\t    struct stat\n");
  fprintf(file, "\tPrMbuf;\n\n");

  fprintf(file, "PrMclearerrors();\n\n");
  variable = parameters->param_list;
  while (variable) {
    valid_code(file, variable);
    variable = variable->next;
  }
  paramgrp = parameters->paramgrp_list;
  while (paramgrp) {
    if (paramgrp->validf) {
      fprintf(file, "if (%s(", paramgrp->validf->name);
      arg = paramgrp->validf->arg_list;
      while (arg) {
        if (arg->next)
          fprintf(file, "%s,", arg->str);
        else
          fprintf(file, "%s", arg->str);
        arg = arg->next;
      }
      fprintf(file, ")) {\n");
      fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \n");
      fprintf(file, "     \"\\n*** Invalid parameter combination in group %s\\n\\n\");\n",
              paramgrp->name);
      fprintf(file, "    PrMadderror(PrMlasterror);\n");
      fprintf(file, "    PrMerror = PrMTRUE;\n");
      fprintf(file, "}\n");
    }
    paramgrp = paramgrp->next;
  }
  for (function = parameters->gvalidf; function; function = function->next) {
    fprintf(file, "if (%s(", function->name);
    arg = function->arg_list;
    while (arg) {
      if (arg->next)
        fprintf(file, "%s,", arg->str);
      else
        fprintf(file, "%s", arg->str);
      arg = arg->next;
    }
    fprintf(file, ")) {\n  ");
    fprintf(file, "    snprintf(PrMlasterror, sizeof PrMlasterror, \n");
    fprintf(file, "     \"\\n*** Invalid parameter combination\\n\\n\");\n");
    fprintf(file, "    PrMadderror(PrMlasterror);\n");
    fprintf(file, "    PrMerror = PrMTRUE;\n");
    fprintf(file, "}\n");
  }
  fprintf(file, "\n");
  fprintf(file, "return(PrMerror);\n\n");
  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMclean()   *********");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static void PrMclean(void)\n");
  } else {
    fprintf(file, "static void tPrMclean()\n");
  }
  fprintf(file, "{\n");
  fprintf(file, "  /*\n    Cleaning of variables\n  */\n\n");
  level = parameters->max_depend_level;
  fprintf(file, "  int PrMnew_size;\n\n");

  while (level >= 0) {
    fprintf(file, "  /*  Dependence level %d */\n", level);
    variable = parameters->param_list;
    while (variable) {
      if (level == 0 ||
          (!variable->dyn_ancestor && variable->description->storage != DYNAMIC_VECTOR))
        clean_code(file, variable, "  ", level);
      variable = variable->next;
    }
    if (parameters->dyn_vec_list) {
      variable = parameters->dyn_vec_list;
      while (variable) {
        clean_code(file, variable, "  ", level);
        variable = variable->next;
      }
    }
    level--;
    fprintf(file, "\n");
  }
  fprintf(file, "}\n\n");
}
