// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    header_code:Build header file for param() function.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  October 4, 1995

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* header_code()  *****************************************************/

void header_code(FILE* file, char* func_name, char* macro_name, Parameters* parameters)
{
  extern int   c_dialect, language;
  extern char* Type_Names[];
  const char*  type_name;
  int          line, loc_index, var_index;
  Function*    function;
  Variable *   location, *variable;
  Structure*   structure;
  Member*      member;

  /*
    Print preprocessor definitions
  */
  if (language == FORTRAN) {
    fprintf(file, "/*  Maximum number of arguments accepted */\n");
    fprintf(file, "#ifndef NARGMAX\n");
    fprintf(file, "#define NARGMAX\t64\n");
    fprintf(file, "#endif\n");
    fprintf(file, "/*  Maximum length of strings */\n");
    fprintf(file, "#ifndef STRLEN\n");
    fprintf(file, "#define STRLEN\t64\n");
    fprintf(file, "#endif\n\n");
  }

  fprintf(file, "#define %s \\\n", macro_name);

  for (var_index = 0; var_index < parameters->n_variables; var_index++) {
    variable = parameters->variable_vector[var_index];
    switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "\tparam_globals::%s", variable->name);
        if (var_index < parameters->n_variables - 1) fprintf(file, ", \\\n");
        break;
      case DYNAMIC_VECTOR:
        fprintf(file, "\t&param_globals::%s", variable->name);
        if (var_index < parameters->n_variables - 1) fprintf(file, ", \\\n");
        break;
      case SCALAR:
        if (variable->description->length)
          fprintf(file, "\tparam_globals::%s", variable->name);
        else
          fprintf(file, "\t&param_globals::%s", variable->name);
        if (var_index < parameters->n_variables - 1) fprintf(file, ", \\\n");
        break;
    }
  }

  if (parameters->n_variables == 0)
    ;
  else if (parameters->n_locations > 0)
    fprintf(file, ", \\\n");
  else
    fprintf(file, "\n");

  for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
    location = parameters->location_vector[loc_index];
    switch (location->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "\t%s", location->name);
        if (loc_index < parameters->n_locations - 1)
          fprintf(file, ", \\\n");
        else
          fprintf(file, "\n");
        break;
      case DYNAMIC_VECTOR:
        fprintf(file, "\t&%s", location->name);
        if (loc_index < parameters->n_locations - 1)
          fprintf(file, ", \\\n");
        else
          fprintf(file, "\n");
      case SCALAR:
        if (location->description->length)
          fprintf(file, "\t%s", location->name);
        else
          fprintf(file, "\t&%s", location->name);
        if (loc_index < parameters->n_locations - 1)
          fprintf(file, ", \\\n");
        else
          fprintf(file, "\n");
        break;
    }
  }
  fprintf(file, "\n");

  fprintf(file, "#include <Defines.h>\n");
  fprintf(file, "#include <Typedefs.h>\n");
  fprintf(file, "#include <Functions.h>\n\n");

  /*
      Print specified include code
  */
  if (parameters->inc_code) {
    line = 0;
    for (line = 0; parameters->inc_code[line]; line++) {
      fprintf(file, "%s\n", parameters->inc_code[line]);
    }
  }

/*
    Print definitions of user types and associate BooleaN types (Stypes)
*/
#if 0
structure = parameters->structure_list;
while (structure) {
    fprintf(file, "typedef struct %s %s;\n",
     structure->name, structure->name);
    fprintf(file, "typedef struct PrMB%s PrMB%s;\n",
     structure->name, structure->name);
    structure = structure->next;
}
fprintf(file, "\n");
#endif

  /*
      Print definitions of user structures and associate BooleaN structures
  */
  structure = parameters->structure_list;
  while (structure) {
    fprintf(file, "struct %s {\n", structure->name);
    member = structure->member_list;
    while (member) {
      if (member->description->structure) {
        switch (member->description->storage) {
          case DYNAMIC_VECTOR:
            fprintf(file, "    %s\t*%s;\n", member->description->type->data, member->name);
            break;
          case STATIC_VECTOR:
            fprintf(file, "    %s\t %s[%s];\n", member->description->type->data, member->name,
                    member->description->type->next->data);
            break;
          case SCALAR:
            fprintf(file, "    %s\t %s;\n", member->description->type->data, member->name);
            break;
        }
      } else {
        if (member->description->type->type == ANY)
          type_name = member->description->type->data;
        else
          type_name = Type_Names[member->description->type->type - TYPE_BASE];
        switch (member->description->storage) {
          case DYNAMIC_VECTOR: fprintf(file, "    %s\t*%s;\n", type_name, member->name); break;
          case STATIC_VECTOR:
            fprintf(file, "    %s\t %s[%s];\n", type_name, member->name,
                    member->description->type->next->data);
            break;
          case SCALAR: fprintf(file, "    %s\t %s;\n", type_name, member->name); break;
        }
      }
      member = member->next;
    }
    fprintf(file, "};\n");
    fprintf(file, "struct PrMB%s {\n", structure->name);
    member = structure->member_list;
    while (member) {
      if (member->description->structure) {
        switch (member->description->storage) {
          case DYNAMIC_VECTOR:
            fprintf(file, "    PrMB%s\t*%s;\n", member->description->type->data, member->name);
            break;
          case STATIC_VECTOR:
            fprintf(file, "    PrMB%s\t %s[%s];\n", member->description->type->data, member->name,
                    member->description->type->next->data);
            break;
          case SCALAR:
            fprintf(file, "    PrMB%s\t %s;\n", member->description->type->data, member->name);
            break;
        }
      } else {
        switch (member->description->storage) {
          case DYNAMIC_VECTOR: fprintf(file, "    BooleaN\t*%s;\n", member->name); break;
          case STATIC_VECTOR:
            fprintf(file, "    BooleaN\t %s[%s];\n", member->name,
                    member->description->type->next->data);
            break;
          case SCALAR: fprintf(file, "    BooleaN\t %s;\n", member->name); break;
        }
      }
      member = member->next;
    }
    fprintf(file, "};\n\n");
    structure = structure->next;
  }

  /*
      Print definitions of user functions
  */
  function = parameters->function_list;
  while (function) {
    /*
  Functions beginning by '$' are predefined
    */
    if (function->name[0] != '$') {
      if (function->type->data)
        fprintf(file, "%s\t*%s();\n", function->type->data, function->name);
      else {
        type_name = Type_Names[function->type->type - TYPE_BASE];
        fprintf(file, "%s\t%s();\n", type_name, function->name);
      }
    }
    function = function->next;
  }
  fprintf(file, "\n");

  // open namespace
  fprintf(file, "\nnamespace param_globals {\n\n");

  /*
      Print declarations of global parameter variables
  */
  fprintf(file, "#ifdef PrMGLOBAL\n");
  fprintf(file, "/* Global parameter variables */\n");
  for (var_index = 0; var_index < parameters->n_variables; var_index++) {
    variable = parameters->variable_vector[var_index];
    if (variable->description->allocation == GLOBAL ||
        variable->description->allocation == 0 && parameters->gallocation == GLOBAL) {
      if (variable->description->structure || variable->description->type->type == ANY)
        type_name = variable->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[variable->description->type->type - TYPE_BASE];
      switch (variable->description->storage) {
        case SCALAR: fprintf(file, "%s\t %s", type_name, variable->name); break;
        case STATIC_VECTOR:
          fprintf(file, "%s\t %s[%s]", type_name, variable->name,
                  variable->description->type->next->data);
          break;
        case DYNAMIC_VECTOR: fprintf(file, "%s\t*%s", type_name, variable->name); break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      fprintf(file, ";\n");
    }
  }
  fprintf(file, "#endif\n");
  fprintf(file, "\n");

  // close namespace
  fprintf(file, "\n}\n\n");

  /* Print extern C style guards */
  fprintf(file, "#ifdef  __cplusplus\n");
  fprintf(file, "extern \"C\"\n");
  fprintf(file, "{\n");
  fprintf(file, "#endif\n");

  /*
      Print param() function prototype
  */
  if (c_dialect == ANSI) {
    fprintf(file, "\nint\t%s(\n\t", func_name);
    if (parameters->n_variables) fprintf(file, "/* Variables */\n\t");
    for (var_index = 0; var_index < parameters->n_variables; var_index++) {
      variable = parameters->variable_vector[var_index];
      if (variable->description->structure || variable->description->type->type == ANY)
        type_name = variable->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[variable->description->type->type - TYPE_BASE];
      switch (variable->description->storage) {
        case STATIC_VECTOR:
          fprintf(file, "%s PrMp%.3d[%s]", type_name, variable->index,
                  variable->description->type->next->data);
          break;
        case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, variable->index); break;
        case SCALAR:
          if (variable->description->length)
            fprintf(file, "%s PrMp%.3d", type_name, variable->index);
          else
            fprintf(file, "%s *PrMp%.3d", type_name, variable->index);
          break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (var_index + 1) % 5 || var_index == parameters->n_variables - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    if (parameters->n_locations) fprintf(file, "/* Locations */\n\t");
    for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
      location = parameters->location_vector[loc_index];
      if (location->description->structure || location->description->type->type == ANY)
        type_name = location->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[location->description->type->type - TYPE_BASE];
      switch (location->description->storage) {
        case STATIC_VECTOR:
          fprintf(file, "%s PrMp%.3d[%s]", type_name, location->index,
                  location->description->type->next->data);
          break;
        case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, location->index); break;
        case SCALAR:
          if (location->description->length)
            fprintf(file, "%s PrMp%.3d", type_name, location->index);
          else
            fprintf(file, "%s *PrMp%.3d", type_name, location->index);
          break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (loc_index + 1) % 5 || loc_index == parameters->n_locations - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    fprintf(file, "int *PrMpargc, char *PrMargv[]);\n\n");
  } else {
    fprintf(file, "\nint\t%s(/* \n\t", func_name);
    for (var_index = 0; var_index < parameters->n_variables; var_index++) {
      variable = parameters->variable_vector[var_index];
      if (variable->description->structure || variable->description->type->type == ANY)
        type_name = variable->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[variable->description->type->type - TYPE_BASE];

      switch (variable->description->storage) {
        case STATIC_VECTOR:
          fprintf(file, "%s PrMp%.3d[%s]", type_name, variable->index,
                  variable->description->type->next->data);
          break;
        case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, variable->index); break;
        case SCALAR:
          if (variable->description->length)
            fprintf(file, "%s PrMp%.3d", type_name, variable->index);
          else
            fprintf(file, "%s *PrMp%.3d", type_name, variable->index);
          break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (var_index + 1) % 5 || var_index == parameters->n_variables - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
      location = parameters->location_vector[loc_index];
      if (location->description->structure || location->description->type->type == ANY)
        type_name = location->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[location->description->type->type - TYPE_BASE];
      switch (location->description->storage) {
        case STATIC_VECTOR:
          fprintf(file, "%s PrMp%.3d[%s]", type_name, location->index,
                  location->description->type->next->data);
          break;
        case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, location->index); break;
        case SCALAR:
          if (location->description->length)
            fprintf(file, "%s PrMp%.3d", type_name, location->index);
          else
            fprintf(file, "%s *PrMp%.3d", type_name, location->index);
          break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (loc_index + 1) % 5 || loc_index == parameters->n_locations - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    fprintf(file, "int *PrMpargc, char *PrMargv[] */);\n\n");
  }

  /*
    Print param_clean() function prototype
  */
  fprintf(file, "\nvoid\tparam_clean();\n\n");
 
  /*
    Print param_output() function prototype
  */
  if (c_dialect == ANSI) {
    // fprintf(file, "\nint\t%s(\n\t", func_name);
	fprintf(file, "\nvoid\tparam_output(\n");
    if (parameters->n_variables) fprintf(file, "/* Variables */\n\t");
    for (var_index = 0; var_index < parameters->n_variables; var_index++) {
      variable = parameters->variable_vector[var_index];
      if (variable->description->structure || variable->description->type->type == ANY)
        type_name = variable->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[variable->description->type->type - TYPE_BASE];
      switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s PrMp%.3d[%s]", type_name, variable->index,
                variable->description->type->next->data);
        break;
      case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, variable->index); break;
      case SCALAR:
        if (variable->description->length)
          fprintf(file, "%s PrMp%.3d", type_name, variable->index);
        else
          fprintf(file, "%s *PrMp%.3d", type_name, variable->index);
        break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (var_index + 1) % 5 || var_index == parameters->n_variables - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    if (parameters->n_locations) fprintf(file, "/* Locations */\n\t");
    for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
      location = parameters->location_vector[loc_index];
      if (location->description->structure || location->description->type->type == ANY)
        type_name = location->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[location->description->type->type - TYPE_BASE];
      switch (location->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s PrMp%.3d[%s]", type_name, location->index,
                location->description->type->next->data);
        break;
      case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, location->index); break;
      case SCALAR:
        if (location->description->length)
          fprintf(file, "%s PrMp%.3d", type_name, location->index);
        else
          fprintf(file, "%s *PrMp%.3d", type_name, location->index);
        break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (loc_index + 1) % 5 || loc_index == parameters->n_locations - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    fprintf(file, "int a);\n\n");
  } else {
    fprintf(file, "\nint\t%s(/* \n\t", func_name);
    for (var_index = 0; var_index < parameters->n_variables; var_index++) {
      variable = parameters->variable_vector[var_index];
      if (variable->description->structure || variable->description->type->type == ANY)
        type_name = variable->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[variable->description->type->type - TYPE_BASE];

      switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s PrMp%.3d[%s]", type_name, variable->index,
                variable->description->type->next->data);
        break;
      case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, variable->index); break;
      case SCALAR:
        if (variable->description->length)
          fprintf(file, "%s PrMp%.3d", type_name, variable->index);
        else
          fprintf(file, "%s *PrMp%.3d", type_name, variable->index);
        break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (var_index + 1) % 5 || var_index == parameters->n_variables - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
      location = parameters->location_vector[loc_index];
      if (location->description->structure || location->description->type->type == ANY)
        type_name = location->description->type->data;
      else if (variable->description->length)
        type_name = "char";
      else
        type_name = Type_Names[location->description->type->type - TYPE_BASE];
      switch (location->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s PrMp%.3d[%s]", type_name, location->index,
                location->description->type->next->data);
        break;
      case DYNAMIC_VECTOR: fprintf(file, "%s **PrMp%.3d", type_name, location->index); break;
      case SCALAR:
        if (location->description->length)
          fprintf(file, "%s PrMp%.3d", type_name, location->index);
        else
          fprintf(file, "%s *PrMp%.3d", type_name, location->index);
        break;
      }
      if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
      if (0 == (loc_index + 1) % 5 || loc_index == parameters->n_locations - 1)
        fprintf(file, ", \n\t");
      else
        fprintf(file, ", ");
    }
    fprintf(file, "int *PrMpargc, char *PrMargv[] */);\n\n");
  }

  /* close extern C style guards */
  fprintf(file, "#ifdef  __cplusplus\n");
  fprintf(file, "}\n");
  fprintf(file, "#endif\n");
}
