// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    c_code:  Functions called by build().

    functions:  alloc_code(), assign_code(), file_name_code(), initial_code(),
    realloc_code(), reinitial_code(), save_code(),
    voloutaux_code().

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  29 mars 2000

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

#if (SYSTEME == WINNT)
#define SLASH "\\\\"
#else
#define SLASH "/"
#endif

/* initial_code()  *****************************************************/
/*
    Prints initialization code for variables and locations
*/
void initial_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level)
{
  int         itl, nelem, level;
  char *      boolean, *name;
  Value *     bound, *default_value;
  static char tabs[81];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->dependence_level != dependence_level) return;

  /*
      For dynamic variables and those with a dynamic ancestor,
      initialization is written by alloc_code().
  */
  if (variable->dyn_ancestor) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  boolean = variable->boolean;

  strcpy(tabs, initial_tabs);
  itl = strlen(initial_tabs);

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
      /*
   For dynamic variables and those with a dynamic ancestor,
   initialization is written by alloc_code().
     */
      break;
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "  ");
      }
      sscanf(variable->description->type->next->data, "%d", &nelem);
      default_value = variable->description->default_value;
      if (default_value->next) {
        for (int elem = 0; elem < nelem; elem++) {
          switch (variable->description->type->type) {
            case RDIR:
            case RWDIR:
            case WDIR:
            case RFILE:
            case RWFILE:
            case WFILE:
            case STRING:
            case EXPR:
              if (variable->description->length) {
                fprintf(file, "%sstrncpy(%s[%d], %s, %d-1);\n", tabs, name, elem,
                        default_value->data, variable->description->length);
              } else {
                fprintf(file, "%s%s[%d] = PrMnewstr(%s);\n", tabs, name, elem, default_value->data);
              }
              break;
            default:
              fprintf(file, "%s%s[%d] = %s;\n", tabs, name, elem, default_value->data);
              break;
          }
          fprintf(file, "%s%s[%d] = PrMFALSE;\n", tabs, boolean, elem);
          if (default_value->next) default_value = default_value->next;
        }
      } else {
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%d; PrMelem%d++) {\n", tabs, level, level,
                nelem, level);
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%s    strncpy(%s[PrMelem%d], %s, %d-1);\n", tabs, name, level,
                      default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s    %s[PrMelem%d] = PrMnewstr(%s);\n", tabs, name, level,
                      default_value->data);
            }
            break;
          default:
            fprintf(file, "%s    %s[PrMelem%d] = %s;\n", tabs, name, level, default_value->data);
            break;
        }
        fprintf(file, "%s    %s[PrMelem%d] = PrMFALSE;\n", tabs, boolean, level);
        fprintf(file, "%s}\n", tabs);
      }
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      switch (variable->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          if (variable->description->length) {
            fprintf(file, "%sstrncpy(%s, %s, %d-1);\n", tabs, name,
                    variable->description->default_value->data, variable->description->length);
          } else {
            fprintf(file, "%s%s = PrMnewstr(%s);\n", tabs, name,
                    variable->description->default_value->data);
          }
          break;
        default:
          fprintf(file, "%s%s = %s;\n", tabs, name, variable->description->default_value->data);
          break;
      }
      fprintf(file, "%s%s = PrMFALSE;\n", tabs, boolean);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* alloc_code()    *****************************************************/
/*
    Prints allocation code for dynamic vectors and initialization for their
    elements
*/
void alloc_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level)
{
  int          itl, nelem, level;
  char *       boolean, *name, *size, *type;
  Value *      bound, *default_value;
  static char  tabs[81];
  extern char *Type_Formats[], *Type_Names[];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->dependence_level != dependence_level) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;

  boolean = variable->boolean;

  strcpy(tabs, initial_tabs);
  itl = strlen(initial_tabs);

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
      if (variable->description->structure)
        type = variable->description->type->data;
      else
        type = Type_Names[variable->description->type->type - TYPE_BASE];

      size = variable->description->type->next->data;
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sPrMnew_size = %s;\n", tabs, size);
      fprintf(file, "%sif (PrMnew_size<1) PrMnew_size = 1;\n", tabs);
      if (variable->description->length) {
        fprintf(file, "%s%s =\n", tabs, name);
        fprintf(file, "%s (%s *)malloc(PrMnew_size * %d * sizeof(char));\n", tabs, type,
                variable->description->length);
      } else {
        fprintf(file, "%s%s =\n", tabs, name);
        fprintf(file, "%s (%s *)malloc(PrMnew_size * sizeof(%s));\n", tabs, type, type);
      }
      fprintf(file, "%s%s =\n", tabs, boolean);

      if (variable->description->structure)
        fprintf(file, "%s (PrMB%s *)malloc(PrMnew_size * sizeof(PrMB%s));\n", tabs, type, type);
      else
        fprintf(file, "%s (BooleaN *)malloc(PrMnew_size * sizeof(BooleaN));\n", tabs);

      if (variable->description->structure == NULL) {
        default_value = variable->description->default_value;
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<PrMnew_size; PrMelem%d++) {\n", tabs, level,
                level, level);
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%s    strncpy(%s[PrMelem%d], %s, %d-1);\n", tabs, name, level,
                      default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s    %s[PrMelem%d] = PrMnewstr(%s);\n", tabs, name, level,
                      default_value->data);
            }
            break;
          default:
            fprintf(file, "%s    %s[PrMelem%d] = %s;\n", tabs, name, level, default_value->data);
            break;
        }  // end switch
        fprintf(file, "%s    %s[PrMelem%d] = PrMFALSE;\n", tabs, boolean, level);
        fprintf(file, "%s}\n", tabs);
      }
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
      }
      break;
    case STATIC_VECTOR:
      if (variable->description->structure == NULL) {
        for (level = 1; level < variable->array_level; level++) {
          bound = variable->bounds;
          for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) {
            bound = bound->next;
          }
          fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                  bound->data, level);
          strcat(tabs, "    ");
        }
        sscanf(variable->bounds->data, "%d", &nelem);
        default_value = variable->description->default_value;
        if (default_value->next) {
          for (int elem = 0; elem < nelem; elem++) {
            switch (variable->description->type->type) {
              case RDIR:
              case RWDIR:
              case WDIR:
              case RFILE:
              case RWFILE:
              case WFILE:
              case STRING:
              case EXPR:
                if (variable->description->length) {
                  fprintf(file, "%sstrncpy(%s[%d], %s, %d-1);\n", tabs, name, elem,
                          default_value->data, variable->description->length);
                } else {
                  fprintf(file, "%s%s[%d] = PrMnewstr(%s);\n", tabs, name, elem,
                          default_value->data);
                }
                break;
              default:
                fprintf(file, "%s%s[%d] = %s;\n", tabs, name, elem, default_value->data);
                break;
            }
            fprintf(file, "%s%s[%d] = PrMFALSE;\n", tabs, boolean, elem);
            if (default_value->next) default_value = default_value->next;
          }  // end loop over elem
        } else {
          fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%d; PrMelem%d++) {\n", tabs, level, level,
                  nelem, level);
          switch (variable->description->type->type) {
            case RDIR:
            case RWDIR:
            case WDIR:
            case RFILE:
            case RWFILE:
            case WFILE:
            case STRING:
            case EXPR:
              if (variable->description->length) {
                fprintf(file, "%s    strncpy(%s[PrMelem%d], %s, %d-1);\n", tabs, name, level,
                        default_value->data, variable->description->length);
              } else {
                fprintf(file, "%s    %s[PrMelem%d] = PrMnewstr(%s);\n", tabs, name, level,
                        default_value->data);
              }
              break;
            default:
              fprintf(file, "%s    %s[PrMelem%d] = %s;\n", tabs, name, level, default_value->data);
              break;
          }
          fprintf(file, "%s    %s[PrMelem%d] = PrMFALSE;\n", tabs, boolean, level);
          fprintf(file, "%s}\n", tabs);
        }
        for (level = 1; level < variable->array_level; level++) {
          fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
        }
      }
      break;
    case SCALAR:
      if (variable->description->structure == NULL) {
        for (level = 1; level <= variable->array_level; level++) {
          bound = variable->bounds;
          for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) {
            bound = bound->next;
          }
          fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                  bound->data, level);
          strcat(tabs, "    ");
        }
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%sstrncpy(%s, %s, %d-1);\n", tabs, name,
                      variable->description->default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s%s = PrMnewstr(%s);\n", tabs, name,
                      variable->description->default_value->data);
            }
            break;
          default:
            fprintf(file, "%s%s = %s;\n", tabs, name, variable->description->default_value->data);
            break;
        }
        fprintf(file, "%s%s = PrMFALSE;\n", tabs, boolean);
        for (level = 1; level <= variable->array_level; level++) {
          fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
        }
      }  // end if
      break;
  } /* switch (variable->storage) */
}  // end alloc_code()

/* realloc_code()  *****************************************************/
/*
    Prints reallocation code for dynamic vectors and initialization for their
    elements
*/
void realloc_code(FILE* file, Variable* variable, const char* initial_tabs, int dep_number)
{
  int          elem, level, nelem, start_level;
  char *       boolean, *name, *size, *type;
  Value *      bound, *default_value;
  static char  tabs[81];
  extern char *Type_Formats[], *Type_Names[];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;

  boolean = variable->boolean;

  if (variable->related)
    start_level = variable->bottom_level;
  else
    start_level = 1;

  tabs[0] = '\000';

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
      if (variable->description->structure)
        type = variable->description->type->data;
      else
        type = Type_Names[variable->description->type->type - TYPE_BASE];

      size = variable->description->type->next->data;
      for (level = start_level; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        if (level == variable->bottom_level) {
          fprintf(file,
                  "%s%sfor (PrMelem%d=PrMold_size%d; PrMelem%d<%s;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, dep_number, level, bound->data, level);
        } else {
          fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
                  level, level, bound->data, level);
        }
        strcat(tabs, "    ");
      }
      fprintf(file, "%s%sPrMnew_size = %s;\n", initial_tabs, tabs, size);
      fprintf(file, "%s%sif (PrMnew_size<1) PrMnew_size = 1;\n", initial_tabs, tabs);
      fprintf(file, "%s%s%s =\n", initial_tabs, tabs, name);
      if (level == variable->bottom_level) {
        if (variable->description->length) {
          fprintf(file, "%s%s (%s *)malloc(\n", initial_tabs, tabs, type);
          fprintf(file, "%s%s PrMnew_size * %d * sizeof(char));\n", initial_tabs, tabs,
                  variable->description->length);
        } else {
          fprintf(file, "%s%s (%s *)realloc(%s,\n", initial_tabs, tabs, type, name);
          fprintf(file, "%s%s PrMnew_size * sizeof(%s));\n", initial_tabs, tabs, type);
        }
        if (variable->description->structure) {
          fprintf(file, "%s%s%s =\n", initial_tabs, tabs, boolean);
          fprintf(file, "%s%s (PrMB%s *)realloc(%s,\n", initial_tabs, tabs, type, boolean);
          fprintf(file, "%s%s PrMnew_size * sizeof(PrMB%s));\n", initial_tabs, tabs, type);
        } else {
          fprintf(file, "%s%s%s =\n", initial_tabs, tabs, boolean);
          fprintf(file, "%s%s (BooleaN *)realloc(%s,\n", initial_tabs, tabs, boolean);
          fprintf(file, "%s%s PrMnew_size * sizeof(BooleaN));\n", initial_tabs, tabs);
        }
      } else {
        if (variable->description->length) {
          fprintf(file, "%s%s (%s *)malloc(\n", initial_tabs, tabs, type);
          fprintf(file, "%s%s PrMnew_size * %d * sizeof(char));\n", initial_tabs, tabs,
                  variable->description->length);
        } else {
          fprintf(file, "%s%s (%s *)malloc(\n", initial_tabs, tabs, type);
          fprintf(file, "%s%s PrMnew_size * sizeof(%s));\n", initial_tabs, tabs, type);
        }

        if (variable->description->structure) {
          fprintf(file, "%s%s%s =\n", initial_tabs, tabs, boolean);
          fprintf(file, "%s%s (PrMB%s *)malloc(\n", initial_tabs, tabs, type);
          fprintf(file, "%s%s PrMnew_size * sizeof(PrMB%s));\n", initial_tabs, tabs, type);
        } else {
          fprintf(file, "%s%s%s =\n", initial_tabs, tabs, boolean);
          fprintf(file, "%s%s (BooleaN *)malloc(\n", initial_tabs, tabs);
          fprintf(file, "%s%s PrMnew_size * sizeof(BooleaN));\n", initial_tabs, tabs);
        }
      }

      if (variable->description->structure == NULL) {
        default_value = variable->description->default_value;
        if (variable->array_level == variable->bottom_level) {
          fprintf(file,
                  "%s%sfor (PrMelem%d=PrMold_size%d; PrMelem%d<%s;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, dep_number, level, variable->bounds->data, level);
        } else {
          fprintf(file,
                  "%s%sfor (PrMelem%d=0; PrMelem%d<PrMnew_size;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, level, level);
        }

        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%s%s    strncpy(%s[PrMelem%d], %s, %d-1);\n", initial_tabs, tabs, name,
                      level, default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s%s    %s[PrMelem%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name,
                      level, default_value->data);
            }
            break;
          default:
            fprintf(file, "%s%s    %s[PrMelem%d] = %s;\n", initial_tabs, tabs, name, level,
                    default_value->data);
            break;
        }
        fprintf(file, "%s%s    %s[PrMelem%d] = PrMFALSE;\n", initial_tabs, tabs, boolean, level);
        fprintf(file, "%s%s}\n", initial_tabs, tabs);
      }
      for (level = start_level; level < variable->array_level; level++)
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      break;  // dynamic vector
    case STATIC_VECTOR:
      if (variable->description->structure) return;
      for (level = start_level; level < variable->array_level; level++) {
        bound = variable->bounds;

        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        if (level == variable->bottom_level) {
          fprintf(file,
                  "%s%sfor (PrMelem%d=PrMold_size%d; PrMelem%d<%s;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, dep_number, level, bound->data, level);
        } else {
          fprintf(file,
                  "%s%sfor (PrMelem%d=0; PrMelem%d<%s;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, level, bound->data, level);
        }
        strcat(tabs, "    ");
      }
      sscanf(variable->bounds->data, "%d", &nelem);
      default_value = variable->description->default_value;
      if (default_value->next) {
        for (elem = 0; elem < nelem; elem++) {
          switch (variable->description->type->type) {
            case RDIR:
            case RWDIR:
            case WDIR:
            case RFILE:
            case RWFILE:
            case WFILE:
            case STRING:
            case EXPR:
              if (variable->description->length) {
                fprintf(file, "%s%sstrncpy(%s[%d], %s, %d-1);\n", initial_tabs, tabs, name, elem,
                        default_value->data, variable->description->length);
              } else {
                fprintf(file, "%s%s%s[%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name, elem,
                        default_value->data);
              }
              break;
            default:
              fprintf(file, "%s%s%s[%d] = %s;\n", initial_tabs, tabs, name, elem,
                      default_value->data);
              break;
          }
          fprintf(file, "%s%s%s[%d] = PrMFALSE;\n", initial_tabs, tabs, boolean, elem);
          if (default_value->next) default_value = default_value->next;
        }
      } else {
        fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%d; PrMelem%d++) {\n", initial_tabs, tabs,
                level, level, nelem, level);
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%s%s    strncpy(%s[PrMelem%d], %s, %d-1);\n", initial_tabs, tabs, name,
                      level, default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s%s    %s[PrMelem%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name,
                      level, default_value->data);
            }
            break;
          default:
            fprintf(file, "%s%s    %s[PrMelem%d] = %s;\n", initial_tabs, tabs, name, level,
                    default_value->data);
            break;
        }
        fprintf(file, "%s%s    %s[PrMelem%d] = PrMFALSE;\n", initial_tabs, tabs, boolean, level);
        fprintf(file, "%s%s}\n", initial_tabs, tabs);
      }
      for (level = start_level; level < variable->array_level; level++)
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      break;  // static vector
    case SCALAR:
      if (variable->description->structure) return;
      for (level = start_level; level <= variable->array_level; level++) {
        bound = variable->bounds;

        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        if (level == variable->bottom_level) {
          fprintf(file,
                  "%s%sfor (PrMelem%d=PrMold_size%d; PrMelem%d<%s;"
                  " PrMelem%d++) {\n",
                  initial_tabs, tabs, level, dep_number, level, bound->data, level);
        } else {
          fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
                  level, level, bound->data, level);
        }
        strcat(tabs, "    ");
      }
      switch (variable->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          if (variable->description->length) {
            fprintf(file, "%s%sstrncpy(%s, %s, %d-1);\n", initial_tabs, tabs, name,
                    variable->description->default_value->data, variable->description->length);
          } else {
            fprintf(file, "%s%s%s = PrMnewstr(%s);\n", initial_tabs, tabs, name,
                    variable->description->default_value->data);
          }
          break;
        default:
          fprintf(file, "%s%s%s = %s;\n", initial_tabs, tabs, name,
                  variable->description->default_value->data);
          break;
      }
      fprintf(file, "%s%s%s = PrMFALSE;\n", initial_tabs, tabs, boolean);

      for (level = start_level; level <= variable->array_level; level++)
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      break;
  } /* switch (variable->storage) */
}  // realloc_code

/* reinitial_code()  *****************************************************/
/*
    Prints re-initialization code for unassigned dependent variables
*/
void reinitial_code(FILE* file, Variable* variable, const char* initial_tabs)
{
  int         bound_level, dep_number, elem, level, nelem;
  char *      boolean, *name;
  Variable*   dep_variable;
  Value *     bound, *default_value;
  static char tabs[81];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  boolean = variable->boolean;

  tabs[0] = '\000';

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
                level, level, bound->data, level);
        strcat(tabs, "    ");
      }
      default_value = variable->description->default_value;
      fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
              level, level, variable->description->type->next->data, level);
      strcat(tabs, "    ");
      fprintf(file, "%s%sif (!%s[PrMelem%d]) {\n", initial_tabs, tabs, boolean, level);
      strcat(tabs, "    ");
      switch (variable->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          if (variable->description->length) {
            fprintf(file, "%s%sstrncpy(%s[PrMelem%d], %s, %d-1);\n", initial_tabs, tabs, name,
                    level, default_value->data, variable->description->length);
          } else {
            fprintf(file, "%s%s%s[PrMelem%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name, level,
                    default_value->data);
          }
          break;
        default:
          fprintf(file, "%s%s%s[PrMelem%d] = %s;\n", initial_tabs, tabs, name, level,
                  default_value->data);
          break;
      }
      dep_number   = 0;
      dep_variable = variable->dependents;
      while (dep_variable) {
        realloc_code(file, dep_variable, tabs, dep_number);
        dep_number++;
        dep_variable = dep_variable->next;
      }
      dep_variable = variable->default_ctl;
      while (dep_variable) {
        reinitial_code(file, dep_variable, tabs);
        dep_variable = dep_variable->next;
      }
      tabs[4 * (level + 1)] = '\000';
      fprintf(file, "%s%s}\n", initial_tabs, tabs);
      tabs[4 * level] = '\000';
      fprintf(file, "%s%s}\n", initial_tabs, tabs);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      }
      break;
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
                level, level, bound->data, level);
        strcat(tabs, "    ");
      }
      sscanf(variable->bounds->data, "%d", &nelem);
      default_value = variable->description->default_value;
      if (default_value->next) {
        for (elem = 0; elem < nelem; elem++) {
          fprintf(file, "%s%sif (!%s[%d]) {;\n", initial_tabs, tabs, boolean, elem);
          strcat(tabs, "    ");
          switch (variable->description->type->type) {
            case RDIR:
            case RWDIR:
            case WDIR:
            case RFILE:
            case RWFILE:
            case WFILE:
            case STRING:
            case EXPR:
              if (variable->description->length) {
                fprintf(file, "%s%sstrncpy(%s[%d], %s, %d-1);\n", initial_tabs, tabs, name, elem,
                        default_value->data, variable->description->length);
              } else {
                fprintf(file, "%s%s%s[%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name, elem,
                        default_value->data);
              }
              break;
            default:
              fprintf(file, "%s%s%s[%d] = %s;\n", initial_tabs, tabs, name, elem,
                      default_value->data);
              break;
          }
          dep_number   = 0;
          dep_variable = variable->dependents;
          while (dep_variable) {
            realloc_code(file, dep_variable, tabs, dep_number);
            dep_number++;
            dep_variable = dep_variable->next;
          }
          dep_variable = variable->default_ctl;
          while (dep_variable) {
            reinitial_code(file, dep_variable, tabs);
            dep_variable = dep_variable->next;
          }
          tabs[4 * level] = '\000';
          fprintf(file, "%s}\n", tabs);
          if (default_value->next) default_value = default_value->next;
        }
      } /* if (default_value->next) */
      else {
        fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%d; PrMelem%d++) {\n", initial_tabs, tabs,
                level, level, nelem, level);
        strcat(tabs, "    ");
        fprintf(file, "%s%sif (!%s[PrMelem%d]) {\n", initial_tabs, tabs, boolean, level);
        strcat(tabs, "    ");
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (variable->description->length) {
              fprintf(file, "%s%sstrncpy(%s[PrMelem%d], %s, %d-1);\n", initial_tabs, tabs, name,
                      level, default_value->data, variable->description->length);
            } else {
              fprintf(file, "%s%s%s[PrMelem%d] = PrMnewstr(%s);\n", initial_tabs, tabs, name, level,
                      default_value->data);
            }
            break;
          default:
            fprintf(file, "%s%s%s[PrMelem%d] = %s;\n", initial_tabs, tabs, name, level,
                    default_value->data);
            break;
        }
        dep_number   = 0;
        dep_variable = variable->dependents;
        while (dep_variable) {
          realloc_code(file, dep_variable, tabs, dep_number);
          dep_number++;
          dep_variable = dep_variable->next;
        }
        dep_variable = variable->default_ctl;
        while (dep_variable) {
          reinitial_code(file, dep_variable, tabs);
          dep_variable = dep_variable->next;
        }
        tabs[4 * (level + 1)] = '\000';
        fprintf(file, "%s%s}\n", initial_tabs, tabs);
        tabs[4 * level] = '\000';
        fprintf(file, "%s%s}\n", initial_tabs, tabs);
      } /* else (default_value->next) */
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      }
      break;
    case SCALAR:
      default_value = variable->description->default_value;
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%s%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", initial_tabs, tabs,
                level, level, bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%s%sif (!%s) {\n", initial_tabs, tabs, boolean);
      strcat(tabs, "    ");
      switch (variable->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          if (variable->description->length) {
            fprintf(file, "%s%sstrncpy(%s, %s, %d-1);\n", initial_tabs, tabs, name,
                    default_value->data, variable->description->length);
          } else {
            fprintf(file, "%s%s%s = PrMnewstr(%s);\n", initial_tabs, tabs, name,
                    default_value->data);
          }
          break;
        default:
          fprintf(file, "%s%s%s = %s;\n", initial_tabs, tabs, name, default_value->data);
          break;
      }
      dep_number   = 0;
      dep_variable = variable->dependents;
      while (dep_variable) {
        realloc_code(file, dep_variable, tabs, dep_number);
        dep_number++;
        dep_variable = dep_variable->next;
      }
      dep_variable = variable->default_ctl;
      while (dep_variable) {
        reinitial_code(file, dep_variable, tabs);
        dep_variable = dep_variable->next;
      }
      tabs[4 * (level - 1)] = '\000';
      fprintf(file, "%s%s}\n", initial_tabs, tabs);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* save_code()    *****************************************************/
/*
    Prints code to save assigned parameters in a file
*/
void save_code(FILE* file, Variable* variable, int dependence_level, Boolean compiled)
{
  int          a_level, bound_level, level;
  const char*  format;
  char *       boolean, *name;
  Value*       bound;
  static char  tabs[81];
  extern char *Type_Formats[], *Type_Names[];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->dependence_level != dependence_level) return;

  if (compiled) {
    if (variable->description->kind != COMPILED) return;
  } else {
    if (variable->description->kind == COMPILED) return;
  }

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  boolean = variable->boolean;
  switch (variable->description->type->type) {
    case BYTE:
    case CHAR:
    case SHORT:
    case INT:
    case BOOLEAN:
    case FLAG: format = "%d"; break;
    case LONG: format = "%ld"; break;
    case FLOAT: format = "%g"; break;
    case DOUBLE: format = "%lg"; break;
    case COMPLEX: format = "%g + %g i"; break;
    case D_COMPLEX: format = "%lg + %lg i"; break;
    default: format = "%s"; break;
  }
  strcpy(tabs, "");

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
              variable->bounds->data, level);
      switch (variable->description->type->type) {
        case RDIR:
        case WDIR:
        case RWDIR:
        case RFILE:
        case WFILE:
        case RWFILE:
        case STRING:
        case EXPR:
          fprintf(file, "%s    if (%s[PrMelem%d] && %s[PrMelem%d][0]) \n", tabs, boolean, level,
                  name, level);
          fprintf(file, "%s        fprintf(PrMsave_file, \"%s[%%d] = %s\\n\",\n", tabs,
                  variable->format, format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "PrMrequote(%s[PrMelem%d]));\n", name, level);
          break;
        case FLAG:
          fprintf(file, "%s    if (%s[PrMelem%d] && %s[PrMelem%d]) \n", tabs, boolean, level, name,
                  level);
          fprintf(file, "%s        fprintf(PrMsave_file, \"%s[%%d] = %s\\n\",\n", tabs,
                  variable->format, format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          break;
        default:
          fprintf(file, "%s    if (%s[PrMelem%d]) \n", tabs, boolean, level);
          fprintf(file, "%s        fprintf(PrMsave_file, \"%s[%%d] = %s\\n\",\n", tabs,
                  variable->format, format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          break;
      }
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      switch (variable->description->type->type) {
        case RDIR:
        case WDIR:
        case RWDIR:
        case RFILE:
        case WFILE:
        case RWFILE:
        case STRING:
        case EXPR:
          fprintf(file, "%sif (%s && %s[0]) {\n", tabs, boolean, name);
          fprintf(file, "%s    fprintf(PrMsave_file, \"%s = %s\\n\",\n", tabs, variable->format,
                  format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "PrMrequote(%s));\n", name);
          break;
        case FLAG:
          fprintf(file, "%sif (%s && %s) {\n", tabs, boolean, name);
          fprintf(file, "%s    fprintf(PrMsave_file, \"%s = %s\\n\",\n", tabs, variable->format,
                  format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          break;
        default:
          fprintf(file, "%sif (%s) {\n", tabs, boolean);
          fprintf(file, "%s    fprintf(PrMsave_file, \"%s = %s\\n\",\n", tabs, variable->format,
                  format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          break;
      }
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */

}  // save_code

/* file_name_code()  *****************************************************/
/*
    Prints code to generate file names
*/

void file_name_code(FILE* file, Variable* variable)
{
  int         bound_level, level;
  char*       name;
  Value*      bound;
  static char tabs[81];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;

  tabs[0] = '\000';

  switch (variable->description->type->type) {
    case RFILE:
    case RWFILE:
    case WFILE: break;
    default: return;
  }

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
              variable->bounds->data, level);
      fprintf(file, "%s    PrMfile_name = %s[PrMelem%d];\n", tabs, name, level);
      fprintf(file, "%s    if (0==strlen(PrMfile_name)) {\n", tabs);
      if (variable->description->mandatory) {
        fprintf(file, "%s\tsnprintf(PrMlasterror, sizeof PrMlasterror, \"\\n%s[%%d] cannot be empty\\n\", PrMelem%d);\n",
                tabs, name, level);
        fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
        fprintf(file, "%s\tPrMerror = PrMTRUE;\n", tabs);
      }
      fprintf(file, "%s\t%s[PrMelem%d] = (char*) \"\";\n", tabs, name, level);
      fprintf(file, "%s    }\n", tabs);
      fprintf(file, "%s    else {\n", tabs);
      fprintf(file, "%s    int length = 2 + strlen(PrMfile_name);\n", tabs);
      if (variable->description->dir)
        fprintf(file, "%s    length += strlen(%s);\n", tabs, variable->description->dir->data);
      if (variable->description->ext) {
	// If the extension is provided by the user, don't add it
	fprintf(file, "%s    if (strcmp(PrMfile_name + strlen(PrMfile_name) - strlen(%s), %s) != 0)\n", tabs, variable->description->ext->data, variable->description->ext->data);
        fprintf(file, "%s        length += strlen(%s);\n", tabs, variable->description->ext->data);
      }
      fprintf(file, "%s\t%s[PrMelem%d] = (char*)malloc(length);\n", tabs, name, level);
      if (variable->description->dir) {
        fprintf(file, "%s\tstrcpy(%s[PrMelem%d], %s);\n", tabs, name, level,
                variable->description->dir->data);
        fprintf(file, "%s\tstrcat(%s[PrMelem%d], \"%s\");\n", tabs, name, level, SLASH);
        fprintf(file, "%s\tstrcat(%s[PrMelem%d], PrMfile_name);\n", tabs, name, level);
      } else {
        fprintf(file, "%s\tstrcpy(%s[PrMelem%d], PrMfile_name);\n", tabs, name, level);
      }
      if (variable->description->ext) {
	fprintf(file, "%s    if (strcmp(PrMfile_name + strlen(PrMfile_name) - strlen(%s), %s) != 0)\n", tabs, variable->description->ext->data, variable->description->ext->data);
        fprintf(file, "%s        strcat(%s[PrMelem%d], %s);\n", tabs, name, level,
		variable->description->ext->data);
      }
      fprintf(file, "%s    }\n", tabs);
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sPrMfile_name = %s;\n", tabs, name);
      fprintf(file, "%sif (0==strlen(PrMfile_name)) {\n", tabs);
      if (variable->description->mandatory) {
        fprintf(file, "%s    snprintf(PrMlasterror, sizeof PrMlasterror, \"\\n%s cannot be empty\\n\");\n", tabs, name);
        fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
        fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
      }
      fprintf(file, ";\n");
      fprintf(file, "%s}\n", tabs);
      fprintf(file, "%selse {\n", tabs);
      fprintf(file, "%s    int length = 2 + strlen(PrMfile_name);\n", tabs);
      if (variable->description->dir)
        fprintf(file, "%s    length += strlen(%s);\n", tabs, variable->description->dir->data);
      if (variable->description->ext) {
	//If the extension is provided by the user, don't add it
	fprintf(file, "%s    if (strcmp(PrMfile_name + strlen(PrMfile_name) - strlen(%s), %s) != 0)\n", tabs, variable->description->ext->data, variable->description->ext->data);
        fprintf(file, "%s        length += strlen(%s);\n", tabs, variable->description->ext->data);
      }
      fprintf(file, "%s    %s = (char*)malloc(length);\n", tabs, name);
      if (variable->description->dir) {
        fprintf(file, "%s    strcpy(%s, %s);\n", tabs, name, variable->description->dir->data);
        fprintf(file, "%s    strcat(%s, \"%s\");\n", tabs, name, SLASH);
        fprintf(file, "%s    strcat(%s, PrMfile_name);\n", tabs, name);
      } else {
        fprintf(file, "%s    strcpy(%s, PrMfile_name);\n", tabs, name);
      }
      if (variable->description->ext) {
	fprintf(file, "%s    if (strcmp(PrMfile_name + strlen(PrMfile_name) - strlen(%s), %s) != 0)\n", tabs, variable->description->ext->data, variable->description->ext->data);
        fprintf(file, "%s        strcat(%s, %s);\n", tabs, name, variable->description->ext->data);
      }
      fprintf(file, "%s    free(PrMfile_name);\n", tabs);
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* strip_name_code()  *****************************************************/
/*
    Prints code to strip directory and extension from file names
*/
void strip_name_code(FILE* file, Variable* variable)
{
  int         bound_level, level;
  char*       name;
  Value*      bound;
  static char tabs[81];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;

  tabs[0] = '\000';

  switch (variable->description->type->type) {
    case RFILE:
    case RWFILE:
    case WFILE: break;
    default: return;
  }

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
              variable->bounds->data, level);
      fprintf(file, "%s    PrMfile_name = %s[PrMelem%d];\n", tabs, name, level);
      fprintf(file, "%s    if (0==strlen(PrMfile_name)) continue;\n", tabs);
      if (variable->description->dir)
        fprintf(file, "%s    PrMfile_name += strlen(%s)+1;\n", tabs,
                variable->description->dir->data);
      if (variable->description->ext)
        fprintf(file, "%s    PrMfile_name[strlen(PrMfile_name)-strlen(%s)] = '\\000';\n", tabs,
                variable->description->ext->data);
      fprintf(file, "%s    %s[PrMelem%d] = PrMfile_name;\n", tabs, name, level);
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%sPrMfile_name = %s;\n", tabs, name);
      fprintf(file, "%sif (strlen(PrMfile_name)>0) {\n", tabs);
      if (variable->description->dir)
        fprintf(file, "%s    PrMfile_name += strlen(%s)+1;\n", tabs,
                variable->description->dir->data);
      if (variable->description->ext)
        fprintf(file, "%s    PrMfile_name[strlen(PrMfile_name)-strlen(%s)] = '\\000';\n", tabs,
                variable->description->ext->data);
      fprintf(file, "    %s%s = PrMfile_name;\n", tabs, name);
      fprintf(file, "%s}\n", tabs);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* assign_code()  *****************************************************/
/*
    Print code to assign the value of local variables to the corresponding
    reference in main
*/
void assign_code(FILE* file, Variable* variable)
{
  Boolean     output;
  int         bound_level, level;
  char *      name, *reference;
  Value*      bound;
  static char tabs[81];

  output = variable->description->output != NULL;

  if (output && variable->description->output->data == NULL || variable->description->auxiliary)
    return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  reference = variable->reference;

  tabs[0] = '\000';

  switch (variable->description->storage) {
    case STATIC_VECTOR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      level--;
      if (output && variable->description->output->data != NULL)
        fprintf(file, "%s%s[PrMelem%d] = %s;\n", tabs, name, level,
                variable->description->output->data);
      fprintf(file, "%s%s[PrMelem%d] = %s[PrMelem%d];\n", tabs, reference, level, name, level);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case DYNAMIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      fprintf(file, "%s%s = %s;\n", tabs, reference, name);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      if (output && variable->description->output->data != NULL)
        fprintf(file, "%s%s = %s;\n", tabs, name, variable->description->output->data);
      if (variable->description->length)
        fprintf(file, "%s%s = %s;\n", tabs, &reference[1], name);
      else
        fprintf(file, "%s%s = %s;\n", tabs, reference, name);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* voloutaux_code()  *****************************************************/
/*
    Print code to assign the value of volatil, output and auxiliaries
    to local variables
*/
void voloutaux_code(FILE* file, Variable* variable)
{
  int         bound_level, level;
  char *      name, *reference;
  Value*      bound;
  static char tabs[81];

  if (!variable->description->volatil && !variable->description->output &&
      !variable->description->auxiliary)
    return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  reference = variable->reference;

  tabs[0] = '\000';

  switch (variable->description->storage) {
    case STATIC_VECTOR:
      for (level = 1; level < variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      if (variable->description->length) {
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++)\n", tabs, level, level,
                variable->description->type->next->data, level);
        fprintf(file, "%s    %s[PrMelem%d] = %s[PrMelem%d];\n", tabs, name, level,
                variable->reference, level);
      } else
        fprintf(file, "%s%s = %s;\n", tabs, name, variable->reference);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      if (variable->description->length)
        fprintf(file, "%s%s = %s;\n", tabs, name, &reference[1]);
      else
        fprintf(file, "%s%s = %s;\n", tabs, name, reference);
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}

/* clean_code()  *****************************************************/
/*
    Cleans code
*/
void clean_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level)
{
  int         itl, nelem, level;
  char *      name, *size;
  static char tabs[81];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->dependence_level != dependence_level) return;

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;

  strcpy(tabs, initial_tabs);
  itl = strlen(initial_tabs);

  switch (variable->description->storage) {
    case SCALAR:
      if (variable->description->structure == NULL) {
        for (level = 1; level <= variable->array_level; level++) {
          Value* bound = variable->bounds;

          for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

          fprintf(file,
                  "%sfor (int PrMelem%d = 0; PrMelem%d < %s;"
                  " PrMelem%d++) {\n",
                  tabs, level, level, bound->data, level);
          strcat(tabs, "    ");
        }
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (!variable->description->length)
              fprintf(file, "%sfree(%s); %s = nullptr;\n", tabs, name, name);
            break;
          default: break;
        }
        for (level = 1; level <= variable->array_level; level++) {
          fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
        }
      }      // end if
      break; /* if scalar */
    case STATIC_VECTOR:
      size = variable->description->type->next->data;
      for (level = 1; level < variable->array_level; level++) {
        Value* bound = variable->bounds;

        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        fprintf(file,
                "%sfor (int PrMelem%d = 0; PrMelem%d < %s;"
                " PrMelem%d++) {\n",
                tabs, level, level, bound->data, level);
        strcat(tabs, "  ");
      }
      fprintf(file, "%sPrMnew_size = %s;\n", tabs, size);
      fprintf(file, "%sif (PrMnew_size < 1) PrMnew_size = 1;\n", tabs);
      if (variable->description->structure == NULL) {
        fprintf(file,
                "%sfor (int PrMelem%d = 0; PrMelem%d < PrMnew_size;"
                " PrMelem%d++) {\n",
                tabs, level, level, level);
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (!variable->description->length)
              fprintf(file,
                      "%s  free(%s[PrMelem%d]);\n"
                      "%s  %s[PrMelem%d] = nullptr;\n",
                      tabs, name, level, tabs, name, level);
            break;
          default:
            fprintf(file, "%s  ;  // no need to deallocate %s[PrMelem%d];\n", tabs, name, level);
            break;
        }  // end switch
        fprintf(file, "%s}\n", tabs);
      }
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
      }
      break; /* if static vector */
    case DYNAMIC_VECTOR:
      size = variable->description->type->next->data;
      for (level = 1; level < variable->array_level; level++) {
        Value* bound = variable->bounds;

        for (int bnd_lvl = variable->array_level; bnd_lvl > level; bnd_lvl--) bound = bound->next;

        fprintf(file,
                "%sfor (int PrMelem%d = 0; PrMelem%d < %s;"
                " PrMelem%d++) {\n",
                tabs, level, level, bound->data, level);
        strcat(tabs, "  ");
      }
      fprintf(file, "%sPrMnew_size = %s;\n", tabs, size);
      fprintf(file, "%sif (PrMnew_size < 1) PrMnew_size = 1;\n", tabs);
      if (variable->description->structure == NULL) {
        fprintf(file,
                "%sfor (int PrMelem%d = 0; PrMelem%d < PrMnew_size;"
                " PrMelem%d++) {\n",
                tabs, level, level, level);
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (!variable->description->length)
              fprintf(file,
                      "%s  free(%s[PrMelem%d]);\n"
                      "%s  %s[PrMelem%d] = nullptr;\n",
                      tabs, name, level, tabs, name, level);
            break;
          default: break;
        }  // end switch
        fprintf(file, "%s}\n", tabs);
      }
      fprintf(file, "%sfree(%s); %s = nullptr;\n", tabs, name, name);
      if (variable->index > 0)
        fprintf(file, "%sfree(PrMs%.3d); PrMs%.3d = nullptr;\n", tabs, variable->index,
                variable->index);
      for (level = 1; level < variable->array_level; level++) {
        fprintf(file, "%s%s}\n", initial_tabs, &tabs[itl + 4 * level]);
      }
      break; /* if dynamic vector */
    default: break;
  }  // end switch variable->description->storage
}  // end function clean_code
