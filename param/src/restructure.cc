// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    restructure:Rearrange parameters read by the parser in new sub-structures
    suitable for the code builder.

    Functions:  restructure(), add_variable(), substitute_father(),
    substitute_index(), dependences().

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  March 25, 1999

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

void add_variable(Parameters*, Variable*, Member*);
int  dependences(Variable**, int, Variable*, Boolean*);

/* restructure()  ******************************************************/

void restructure(Parameters* parameters, int verbose)
{
  static char index_name[81];
  int         arr_index, loc_index, sca_index, stg_index, str_index, s_a_index, var_index;
  Variable *  grp_loc_list, *grp_var_list, *inv_loc_list, *inv_var_list, *location, *param, *variable;
  Pgrp_item * grp_item_list, *inv_item_list, *pgrp_item;
  Value*      bound;
  Paramgrp*   paramgrp;

  /*
      Reverse Main paramgrp variable, location and pgrp_item lists
  */
  grp_var_list = parameters->main_paramgrp->variable_list;
  inv_var_list = NULL;
  while (grp_var_list) {
    variable               = grp_var_list;
    grp_var_list           = grp_var_list->next_in_pgrp;
    variable->next_in_pgrp = inv_var_list;
    inv_var_list           = variable;
  }
  parameters->main_paramgrp->variable_list = inv_var_list;
  grp_loc_list                             = parameters->main_paramgrp->location_list;
  inv_loc_list                             = NULL;
  while (grp_loc_list) {
    location               = grp_loc_list;
    grp_loc_list           = grp_loc_list->next_in_pgrp;
    location->next_in_pgrp = inv_loc_list;
    inv_loc_list           = location;
  }
  parameters->main_paramgrp->location_list = inv_loc_list;
  grp_item_list                            = parameters->main_paramgrp->pgrp_item_list;
  inv_item_list                            = NULL;
  while (grp_item_list) {
    if (grp_item_list->type == PARAMSGRP) {
      paramgrp = (Paramgrp*)grp_item_list->item;
      if (strcmp(paramgrp->father->name, "Main") == 0) {
        pgrp_item       = grp_item_list;
        grp_item_list   = grp_item_list->next;
        pgrp_item->next = inv_item_list;
        inv_item_list   = pgrp_item;
      } else {
        grp_item_list = grp_item_list->next;
      }
    } else {
      pgrp_item       = grp_item_list;
      grp_item_list   = grp_item_list->next;
      pgrp_item->next = inv_item_list;
      inv_item_list   = pgrp_item;
    }
  }
  parameters->main_paramgrp->pgrp_item_list = inv_item_list;

  /*
      Merge variable and location lists disseminated in parameter groups
  */
  parameters->n_parameters = parameters->n_variables + parameters->n_locations;
  parameters->n_scalars =
      parameters->n_parameters -
      (parameters->n_arrays + parameters->n_structures + parameters->n_struct_arrays);
  if (parameters->n_parameters)
    parameters->variable_vector =
        (struct Variable**)malloc(parameters->n_parameters * sizeof(Variable*));
  if (parameters->n_locations)
    parameters->location_vector = &parameters->variable_vector[parameters->n_variables];
  if (parameters->n_scalars)
    parameters->scalar_vector =
        (struct Variable**)malloc(parameters->n_scalars * sizeof(Variable*));
  if (parameters->n_arrays)
    parameters->array_vector =
        (struct Variable**)malloc(parameters->n_arrays * sizeof(Variable*));
  if (parameters->n_structures)
    parameters->structure_vector =
        (struct Variable**)malloc(parameters->n_structures * sizeof(Variable*));
  if (parameters->n_struct_arrays)
    parameters->struct_array_vector =
        (struct Variable**)malloc(parameters->n_struct_arrays * sizeof(Variable*));
  if (parameters->n_strings)
    parameters->string_vector =
        (struct Variable**)malloc(parameters->n_strings * sizeof(Variable*));
  paramgrp  = parameters->paramgrp_list;
  var_index = loc_index = arr_index = stg_index = str_index = s_a_index = sca_index = 0;
  while (paramgrp) {
    if (paramgrp != parameters->main_paramgrp && strcmp(paramgrp->father->name, "Main") == 0) {
      paramgrp->next_in_sgrp                    = parameters->main_paramgrp->paramsgrp_list;
      parameters->main_paramgrp->paramsgrp_list = paramgrp;
    }
    grp_var_list = paramgrp->variable_list;
    while (grp_var_list) {
      grp_var_list->index                      = var_index + loc_index;
      parameters->variable_vector[var_index++] = grp_var_list;
      if (grp_var_list->description->structure)
        if (grp_var_list->description->type->next)
          parameters->struct_array_vector[s_a_index++] = grp_var_list;
        else
          parameters->structure_vector[str_index++] = grp_var_list;
      else {
        if (grp_var_list->description->type->next)
          parameters->array_vector[arr_index++] = grp_var_list;
        else
          parameters->scalar_vector[sca_index++] = grp_var_list;
        switch (grp_var_list->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR: parameters->string_vector[stg_index++] = grp_var_list; break;
        }
      }
      grp_var_list->paramgrp = paramgrp;
      grp_var_list           = grp_var_list->next_in_pgrp;
    }
    grp_loc_list = paramgrp->location_list;
    while (grp_loc_list) {
      grp_loc_list->index                      = var_index + loc_index;
      parameters->location_vector[loc_index++] = grp_loc_list;
      if (grp_loc_list->description->structure)
        if (grp_loc_list->description->type->next)
          parameters->struct_array_vector[s_a_index++] = grp_loc_list;
        else
          parameters->structure_vector[str_index++] = grp_loc_list;
      else {
        if (grp_loc_list->description->type->next)
          parameters->array_vector[arr_index++] = grp_loc_list;
        else
          parameters->scalar_vector[sca_index++] = grp_loc_list;
        switch (grp_loc_list->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR: parameters->string_vector[stg_index++] = grp_loc_list; break;
        }
      }
      grp_loc_list->paramgrp = paramgrp;
      grp_loc_list           = grp_loc_list->next_in_pgrp;
    }
    grp_var_list = paramgrp->textbox_list;
    while (grp_var_list) {
      grp_var_list->paramgrp = paramgrp;
      grp_var_list           = grp_var_list->next_in_pgrp;
    }
    paramgrp = paramgrp->next;
  }

  /*
      From original variable and location lists,
    - create a new list including all assignable parameters
    - create a new list including all dynamic parameters
  */
  parameters->dictionary   = NULL;
  parameters->param_list   = NULL;
  parameters->dyn_vec_list = NULL;
  parameters->assign_list  = NULL;
  parameters->node_list    = NULL;
  parameters->n_leafs      = 0;
  parameters->n_nodes      = 0;
  for (var_index = 0; var_index < parameters->n_variables; var_index++) {
    add_variable(parameters, parameters->variable_vector[var_index], NULL);
  }
  for (loc_index = 0; loc_index < parameters->n_locations; loc_index++) {
    add_variable(parameters, parameters->location_vector[loc_index], NULL);
  }
  if (parameters->error) exit(1);

  /*
      Create and populate the vector that will contain pointers to every
      type of variable: plain variables, locations and nodes
  */
  parameters->n_param = parameters->n_leafs + parameters->n_nodes;
  if (parameters->n_param)
    parameters->param_vector = (struct Variable**)malloc(parameters->n_param * sizeof(Variable*));
  for (variable = parameters->param_list, var_index = 0; variable;
       variable = variable->next, var_index++) {
    parameters->param_vector[var_index] = variable;
  }
  for (variable = parameters->node_list; variable; variable = variable->next, var_index++) {
    parameters->param_vector[var_index] = variable;
  }

  /*
      Substitute the right variable for $Index in descriptions
  */
  param = parameters->param_list;
  while (param) {
    if (param->array_level > 0) {
      snprintf(index_name, sizeof index_name, "PrMelem%d", param->array_level);
      param->description = substitute_index(param->description, index_name);
    }
    param = param->next;
  }
  param = parameters->dyn_vec_list;
  while (param) {
    if (param->array_level > 0) {
      snprintf(index_name, sizeof index_name, "PrMelem%d", param->array_level);
      param->description = substitute_index(param->description, index_name);
    }
    param = param->next;
  }

  /*
      Check and re-arrange dependences information
  */
  parameters->max_depend_level = dependences(parameters->param_vector, parameters->n_param,
                                             parameters->dyn_vec_list, &parameters->run_on_comp);

  parameters->max_array_level = parameters->max_dependents = 0;
  param                                                    = parameters->param_list;
  while (param) {
    if (param->array_level > parameters->max_array_level)
      parameters->max_array_level = param->array_level;
    if (param->n_dependents > parameters->max_dependents)
      parameters->max_dependents = param->n_dependents;
    param = param->next;
  }

  if (verbose < 1) return;

  printf("\nVariables:\n");
  param = parameters->param_list;
  while (param) {
    printf("%s\n\tarray_level=%d dependence_level=%d\n", param->name, param->array_level,
           param->dependence_level);
    printf("\tboolean=%s\n", param->boolean);
    printf("\tformat=%s\n", param->format);
    printf("\treference=%s\n", param->reference);
    if (param->father) printf("\tfather=%s\n", param->father);
    if (param->bounds) {
      printf("\tbounds=\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->data);
        bound = bound->next;
      }
      printf("\n\t\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->label);
        bound = bound->next;
      }
      printf("\n");
    }
    if (param->dependents) {
      printf("\tdependents = \n");
      variable = param->dependents;
      while (variable) {
        printf("\t\t%s (%d) %s\n", variable->name, variable->bottom_level,
               variable->bottom_bound->data);
        variable = variable->next;
      }
      printf("\n");
    }
    if (param->influenced) {
      printf("\tinfluenced = \n");
      variable = param->influenced;
      while (variable) {
        printf("\t\t%s\n", variable->name);
        variable = variable->next;
      }
      printf("\n");
    }
    if (param->default_ctl) {
      printf("\tdefault_ctl = \n");
      variable = param->default_ctl;
      while (variable) {
        printf("\t\t%s\t(%s)\n", variable->name, variable->description->default_value->data);
        variable = variable->next;
      }
      printf("\n");
    }
    if (param->max_ctl) {
      printf("\tmax_ctl = \n");
      variable = param->max_ctl;
      while (variable) {
        printf("\t\t%s\t(%s)\n", variable->name, variable->description->max->data);
        variable = variable->next;
      }
      printf("\n");
    }
    if (param->min_ctl) {
      printf("\tmin_ctl = \n");
      variable = param->min_ctl;
      while (variable) {
        printf("\t\t%s\t(%s)\n", variable->name, variable->description->min->data);
        variable = variable->next;
      }
      printf("\n");
    }
    param = param->next;
  }
  if (parameters->dyn_vec_list) printf("\nDynamic variables:\n");
  param = parameters->dyn_vec_list;
  while (param) {
    printf("%s\n\tarray_level=%d dependence_level=%d\n", param->name, param->array_level,
           param->dependence_level);
    printf("\tboolean=%s\n", param->boolean);
    printf("\tformat=%s\n", param->format);
    printf("\treference=%s\n", param->reference);
    if (param->father) printf("\tfather=%s\n", param->father);
    if (param->bounds) {
      printf("\tbounds=\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->data);
        bound = bound->next;
      }
      printf("\n\t\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->label);
        bound = bound->next;
      }
      printf("\n");
    }
    param = param->next;
  }
  if (parameters->assign_list) printf("\nAssign list:\n");
  param = parameters->assign_list;
  while (param) {
    printf("%s\n\tarray_level=%d dependence_level=%d\n", param->name, param->array_level,
           param->dependence_level);
    printf("\tboolean=%s\n", param->boolean);
    printf("\tformat=%s\n", param->format);
    printf("\treference=%s\n", param->reference);
    if (param->father) printf("\tfather=%s\n", param->father);
    if (param->bounds) {
      printf("\tbounds=\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->data);
        bound = bound->next;
      }
      printf("\n\t\t");
      bound = param->bounds;
      while (bound) {
        printf("%s\t", bound->label);
        bound = bound->next;
      }
      printf("\n");
    }
    param = param->next;
  }
}

/* add_variable()  ******************************************************/
/*
    Adds a variable and its descendants to the correct lists by a recursive
    descent of the variable familly tree.
*/

void add_variable(Parameters* parameters, Variable* variable, Member* father_member)
{
  char boolean[257], child_name[257], father_name[257], format[257], func_name[257], *member_name_b,
      *member_name_c, *member_name_f, *member_name_r, *member_name_u, reference[257];
  int *n_leafs,              /*  Number of leafs */
      *n_nodes;              /*  Number of nodes */
  Variable **assign_list,    /*  List of variables to assign in main */
      **dyn_vec_list,        /*  List of dynamic variables */
      *new_var, **node_list, /*  List of node variables */
      **param_list,          /*  List of leaf variables */
      var;
  Value * bound, *new_bound;
  Member* member;

  param_list   = &parameters->param_list;
  n_leafs      = &parameters->n_leafs;
  dyn_vec_list = &parameters->dyn_vec_list;
  assign_list  = &parameters->assign_list;
  node_list    = &parameters->node_list;
  n_nodes      = &parameters->n_nodes;

  if (variable->index == -1) {
    /*
  This is a sub-variable
    */
    strcpy(format, variable->format);
    strcpy(func_name, variable->func_name);
    var.dyn_ancestor = variable->dyn_ancestor;
    if (variable->description->storage == DYNAMIC_VECTOR) var.dyn_ancestor = TRUE;
    if (var.dyn_ancestor) {
      if (variable->description->output) {
        fprintf(stderr, "*** %s is $Output with dynamic ancestor, which is forbiden\n",
                variable->name);
        exit(-1);
      }
      if (variable->description->volatil) {
        fprintf(stderr, "*** %s is $Volatil with dynamic ancestor, which is forbiden\n",
                variable->name);
        exit(-1);
      }
    }
  } else {
    /*
  This is a root variable
    */
    variable->array_level = 0;
    variable->bounds      = NULL;
    variable->father      = NULL;
    snprintf(boolean, sizeof boolean, "PrMs%.3d", variable->index);
    variable->boolean = newstr(boolean);
    if (variable->description->storage == DYNAMIC_VECTOR)
      snprintf(reference, sizeof reference, "*PrMp%.3d", variable->index);
    else if (variable->description->storage == STATIC_VECTOR)
      snprintf(reference, sizeof reference, "PrMp%.3d", variable->index);
    else if (variable->description->structure)
      snprintf(reference, sizeof reference, "PrMp%.3d", variable->index);
    else
      snprintf(reference, sizeof reference, "*PrMp%.3d", variable->index);
    variable->reference = newstr(reference);
    if (variable->description->key) {
      strcpy(format, variable->description->key);
      strcpy(func_name, variable->description->key);
    } else {
      strcpy(format, variable->name);
      strcpy(func_name, variable->name);
    }
    variable->format       = newstr(format);
    variable->func_name    = newstr(func_name);
    variable->dyn_ancestor = FALSE;
    if (variable->description->storage == DYNAMIC_VECTOR)
      var.dyn_ancestor = TRUE;
    else
      var.dyn_ancestor = FALSE;
    if (variable->description->storage == STATIC_VECTOR)
      variable->static_root = TRUE;
    else
      variable->static_root = FALSE;
  } /* else (variable->index==-1) */

  if (variable->structure && !variable->description->auxiliary) {
    /*  This is a node (non-leaf) variable; find its children */
    var.array_level = variable->array_level;
    var.bounds      = variable->bounds;
    var.static_root = variable->static_root;
    if (variable->description->key)
      strcpy(father_name, variable->description->key);
    else
      strcpy(father_name, variable->name);
    if (variable->description->storage == SCALAR) {
      if (variable->index == -1) {
        strcpy(reference, variable->reference);
        strcpy(boolean, variable->boolean);
      }
    } else {
      new_bound              = (Value*)malloc(sizeof(Value));
      bound                  = variable->description->type->next;
      new_bound->type        = bound->type;
      new_bound->data        = bound->data;
      new_bound->label       = newstr(father_name);
      new_bound->dependences = bound->dependences;
      new_bound->related     = bound->related;
      new_bound->next        = var.bounds;
      var.bounds             = new_bound;
      var.array_level++;
      if (variable->description->key)
        snprintf(father_name, sizeof father_name, "%s[PrMelem%d]", variable->description->key, var.array_level);
      else
        snprintf(father_name, sizeof father_name, "%s[PrMelem%d]", variable->name, var.array_level);
      strcat(format, "[%d]");
      if (variable->index == -1) {
        snprintf(reference, sizeof reference, "%s[PrMelem%d]", variable->reference, var.array_level);
        snprintf(boolean, sizeof boolean, "%s[PrMelem%d]", variable->boolean, var.array_level);
      } else {
        snprintf(reference, sizeof reference, "(PrMp%.3d[0] + PrMelem%d)", variable->index, var.array_level);
        snprintf(boolean, sizeof boolean, "PrMs%.3d[PrMelem%d]", variable->index, var.array_level);
      }
    }
    strcpy(child_name, father_name);
    member_name_b = boolean;
    member_name_c = child_name;
    member_name_f = format;
    member_name_r = reference;
    member_name_u = func_name;
    /*  member_name_X will point to first character after variable name */
    member_name_b += strlen(boolean);
    member_name_c += strlen(child_name);
    member_name_f += strlen(format);
    member_name_r += strlen(reference);
    member_name_u += strlen(func_name);
    var.name         = child_name;
    var.father       = father_name;
    var.format       = format;
    var.func_name    = func_name;
    var.boolean      = boolean;
    var.reference    = reference;
    var.index        = -1;
    var.paramgrp     = variable->paramgrp;
    var.next_in_pgrp = variable->next_in_pgrp;
    var.next         = NULL;
    member           = variable->structure->member_list;
    while (member) {
      /*  Add member name to variable name to form child name */
      if (variable->description->storage == SCALAR && variable->index != -1)
        snprintf(member_name_b, sizeof boolean, "->%s", member->name);
      else
        snprintf(member_name_b, sizeof boolean, ".%s", member->name);
      snprintf(member_name_c, sizeof child_name, ".%s", member->name);
      snprintf(member_name_f, sizeof format, ".%s", member->name);
      if (variable->description->storage == STATIC_VECTOR || variable->index == -1)
        snprintf(member_name_r, sizeof reference, ".%s", member->name);
      else
        snprintf(member_name_r, sizeof reference, "->%s", member->name);
      snprintf(member_name_u, sizeof func_name, "_%s", member->name);
      var.description = substitute_father(member->description, var.father);
      var.structure   = copy_structure(var.description->structure);
      if (var.description->auxiliary) parameters->n_auxiliaries++;
      if (var.description->output) parameters->n_outputs++;
      if (var.description->volatil) parameters->n_volatils++;
      add_variable(parameters, &var, member);
      /*  Truncate member name from child name */
      *member_name_b = '\000';
      *member_name_c = '\000';
      *member_name_f = '\000';
      *member_name_r = '\000';
      *member_name_u = '\000';
      member         = member->next;
    } /* while (member) */
    if (variable->index == -1) {
      new_var                      = (Variable*)calloc(1, sizeof(Variable));
      new_var->name                = newstr(variable->name);
      new_var->father              = newstr(variable->father);
      new_var->format              = newstr(variable->format);
      new_var->func_name           = newstr(variable->func_name);
      new_var->boolean             = newstr(variable->boolean);
      new_var->reference           = newstr(variable->reference);
      new_var->array_level         = variable->array_level;
      new_var->bounds              = variable->bounds;
      new_var->index               = variable->index;
      new_var->description         = variable->description;
      new_var->structure           = variable->structure;
      new_var->dyn_ancestor        = variable->dyn_ancestor;
      new_var->paramgrp            = variable->paramgrp;
      new_var->next_in_pgrp        = variable->next_in_pgrp;
      father_member->associate_var = new_var;
    } else {
      new_var = variable;
    }
    if (new_var->description->storage != SCALAR) {
      new_bound->next = new_var->bounds;
      new_var->bounds = new_bound;
      new_var->array_level++;
    }
    new_var->next = *node_list;
    *node_list    = new_var;
    (*n_nodes)++;
  } /* if (variable->structure) */
  else {
    /*  This is a leaf variable; add it to param list */
    if (variable->index == -1) {
      new_var = (Variable*)calloc(1, sizeof(Variable));
      if (variable->description->key)
        new_var->name = newstr(variable->description->key);
      else
        new_var->name = newstr(variable->name);
      new_var->father              = newstr(variable->father);
      new_var->format              = newstr(variable->format);
      new_var->func_name           = newstr(variable->func_name);
      new_var->boolean             = newstr(variable->boolean);
      new_var->reference           = newstr(variable->reference);
      new_var->array_level         = variable->array_level;
      new_var->bounds              = variable->bounds;
      new_var->index               = variable->index;
      new_var->description         = variable->description;
      new_var->structure           = variable->structure;
      new_var->dyn_ancestor        = variable->dyn_ancestor;
      new_var->paramgrp            = variable->paramgrp;
      new_var->next_in_pgrp        = variable->next_in_pgrp;
      father_member->associate_var = new_var;
    } else {
      new_var = variable;
    }
    if (variable->description->storage != SCALAR) {
      new_bound              = (Value*)malloc(sizeof(Value));
      bound                  = variable->description->type->next;
      new_bound->type        = bound->type;
      new_bound->data        = bound->data;
      new_bound->label       = new_var->name;
      new_bound->dependences = bound->dependences;
      new_bound->related     = bound->related;
      new_bound->next        = new_var->bounds;
      new_var->bounds        = new_bound;
      new_var->array_level++;
    }
    new_var->next = *param_list;
    *param_list   = new_var;
    (*n_leafs)++;
  } /* else (variable->structure) */

  /*
      Check if there is a name conflict with another variable
  */
  parameters->dictionary = add_entry(parameters->dictionary, new_var);

  /*
      If variable is dynamic or has a dynamic ancestor,
      add it to dynamic variable list;
      if variable has no dynamic ancestor nor a static array root
      and is a leaf or dynamic array, add it to assign variable list.
  */
  if (variable->dyn_ancestor || variable->description->storage == DYNAMIC_VECTOR) {
    new_var = (Variable*)calloc(1, sizeof(Variable));
    if (variable->description->key)
      new_var->name = newstr(variable->description->key);
    else
      new_var->name = newstr(variable->name);
    new_var->father      = newstr(variable->father);
    new_var->format      = newstr(variable->format);
    new_var->func_name   = newstr(variable->func_name);
    new_var->boolean     = newstr(variable->boolean);
    new_var->reference   = newstr(variable->reference);
    new_var->array_level = variable->array_level;
    new_var->bounds      = variable->bounds;
    if (variable->description->storage != SCALAR && variable->index == -1) {
      new_bound              = (Value*)malloc(sizeof(Value));
      bound                  = variable->description->type->next;
      new_bound->type        = bound->type;
      new_bound->data        = bound->data;
      new_bound->label       = new_var->name;
      new_bound->dependences = bound->dependences;
      new_bound->related     = bound->related;
      new_bound->next        = new_var->bounds;
      new_var->bounds        = new_bound;
      new_var->array_level++;
    }
    new_var->index        = variable->index;
    new_var->description  = variable->description;
    new_var->structure    = variable->structure;
    new_var->dyn_ancestor = variable->dyn_ancestor;
    new_var->related      = variable->related;
    new_var->paramgrp     = variable->paramgrp;
    new_var->next_in_pgrp = variable->next_in_pgrp;
    new_var->next         = *dyn_vec_list;
    *dyn_vec_list         = new_var;
  }
  if (!variable->dyn_ancestor && !variable->static_root &&
      (!variable->description->length || variable->description->storage != SCALAR) &&
      (!variable->description->structure || variable->description->storage == DYNAMIC_VECTOR)) {
    new_var = (Variable*)calloc(1, sizeof(Variable));
    if (variable->description->key)
      new_var->name = newstr(variable->description->key);
    else
      new_var->name = newstr(variable->name);
    new_var->father      = newstr(variable->father);
    new_var->format      = newstr(variable->format);
    new_var->func_name   = newstr(variable->func_name);
    new_var->boolean     = newstr(variable->boolean);
    new_var->reference   = newstr(variable->reference);
    new_var->array_level = variable->array_level;
    new_var->bounds      = variable->bounds;
    if (variable->description->storage != SCALAR && variable->index == -1) {
      new_bound              = (Value*)malloc(sizeof(Value));
      bound                  = variable->description->type->next;
      new_bound->type        = bound->type;
      new_bound->data        = bound->data;
      new_bound->label       = new_var->name;
      new_bound->dependences = bound->dependences;
      new_bound->related     = bound->related;
      new_bound->next        = new_var->bounds;
      new_var->bounds        = new_bound;
      new_var->array_level++;
    }
    new_var->index        = variable->index;
    new_var->description  = variable->description;
    new_var->structure    = variable->structure;
    new_var->dyn_ancestor = variable->dyn_ancestor;
    new_var->paramgrp     = variable->paramgrp;
    new_var->next_in_pgrp = variable->next_in_pgrp;
    new_var->next         = *assign_list;
    *assign_list          = new_var;
  }
}

/* substitute_father()  ******************************************************/

Description* substitute_father(Description* in_desc, char* father_name)
{
  Description* out_desc;
  Value*       value;
  Arg*         arg;
  Boolean      replace;
  int          l;

  out_desc = in_desc;

  if (out_desc->default_value) {
    value = out_desc->default_value;
    while (value) {
      if (substr(value->data, (char*)"$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      value = value->next;
    }
    value = out_desc->default_value;
    while (value) {
      if (substr(value->data, "$Father")) {
        value->data = substitute_all(value->data, "$Father", father_name);
        substitute_in_text(value->dependences, "$Father", father_name);
      }
      value = value->next;
    }
  }
  if (out_desc->dir) {
    if (substr(out_desc->dir->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->dir->data = substitute_all(out_desc->dir->data, "$Father", father_name);
      substitute_in_text(out_desc->dir->dependences, "$Father", father_name);
    }
  }
  if (out_desc->ext) {
    if (substr(out_desc->ext->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->ext->data = substitute_all(out_desc->ext->data, "$Father", father_name);
      substitute_in_text(out_desc->ext->dependences, "$Father", father_name);
    }
  }
  if (out_desc->max) {
    if (substr(out_desc->max->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->max->data = substitute_all(out_desc->max->data, "$Father", father_name);
      substitute_in_text(out_desc->max->dependences, "$Father", father_name);
    }
  }
  if (out_desc->menu) {
    replace = FALSE;
    value   = out_desc->menu;
    while (value) {
      if (substr(value->data, "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      if (substr(value->label, "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      if (substr(value->sensitive, "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      value = value->next;
    }
    if (replace) {
      value = out_desc->menu;
      while (value) {
        if (substr(value->data, "$Father")) {
          value->data = substitute_all(value->data, "$Father", father_name);
          substitute_in_text(value->dependences, "$Father", father_name);
          replace = FALSE;
        }
        if (substr(value->label, "$Father")) {
          value->label = substitute_all(value->label, "$Father", father_name);
          if (replace) {
            substitute_in_text(value->dependences, "$Father", father_name);
            replace = FALSE;
          }
        }
        if (substr(value->sensitive, "$Father")) {
          value->sensitive = substitute_all(value->sensitive, "$Father", father_name);
          if (replace) {
            substitute_in_text(value->dependences, "$Father", father_name);
            replace = FALSE;
          }
        }
        value = value->next;
      }
    }
  }
  if (out_desc->min) {
    if (substr(out_desc->min->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->min->data = substitute_all(out_desc->min->data, "$Father", father_name);
      substitute_in_text(out_desc->min->dependences, "$Father", father_name);
    }
  }
  if (out_desc->type->next) {
    if (substr(out_desc->type->next->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->type->next->data =
          substitute_all(out_desc->type->next->data, "$Father", father_name);
      substitute_in_text(out_desc->type->next->dependences, "$Father", father_name);
    }
  }
  if (out_desc->units) {
    if (substr(out_desc->units->data, "$Father")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->units->data = substitute_all(out_desc->units->data, "$Father", father_name);
      substitute_in_text(out_desc->units->dependences, "$Father", father_name);
    }
  }
  if (out_desc->validf) {
    arg = out_desc->validf->arg_list;
    while (arg) {
      if (substr(arg->str, "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      arg = arg->next;
    }
    arg = out_desc->validf->arg_list;
    while (arg) {
      if (substr(arg->str, "$Father")) {
        arg->str = substitute_all(arg->str, "$Father", father_name);
      }
      arg = arg->next;
    }
  }
  if (out_desc->display) {
    arg = out_desc->display->arg_list;
    while (arg) {
      if (substr(arg->str, "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      arg = arg->next;
    }
    arg = out_desc->display->arg_list;
    while (arg) {
      if (substr(arg->str, "$Father")) {
        arg->str = substitute_all(arg->str, "$Father", father_name);
      }
      arg = arg->next;
    }
  }
  if (out_desc->dependences) {
    l = 0;
    while (out_desc->dependences[l]) {
      if (substr(out_desc->dependences[l], "$Father")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        out_desc->dependences[l] = substitute_all(out_desc->dependences[l], "$Father", father_name);
      }
      l++;
    }
  }

  return (out_desc);
}

/* substitute_index()  ******************************************************/

Description* substitute_index(Description* in_desc, char* index_name)
{
  Description* out_desc;
  Value*       value;
  Arg*         arg;
  Boolean      replace;
  int          l;

  out_desc = in_desc;

  if (out_desc->default_value) {
    value = out_desc->default_value;
    while (value) {
      if (substr(value->data, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      value = value->next;
    }
    value = out_desc->default_value;
    while (value) {
      if (substr(value->data, "$Index")) {
        value->data = substitute_all(value->data, "$Index", index_name);
        substitute_in_text(value->dependences, "$Index", index_name);
      }
      value = value->next;
    }
  }
  if (out_desc->dir) {
    if (substr(out_desc->dir->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->dir->data = substitute_all(out_desc->dir->data, "$Index", index_name);
      substitute_in_text(out_desc->dir->dependences, "$Index", index_name);
    }
  }
  if (out_desc->ext) {
    if (substr(out_desc->ext->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->ext->data = substitute_all(out_desc->ext->data, "$Index", index_name);
      substitute_in_text(out_desc->ext->dependences, "$Index", index_name);
    }
  }
  if (out_desc->max) {
    if (substr(out_desc->max->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->max->data = substitute_all(out_desc->max->data, "$Index", index_name);
      substitute_in_text(out_desc->max->dependences, "$Index", index_name);
    }
  }
  if (out_desc->menu) {
    replace = FALSE;
    value   = out_desc->menu;
    while (value) {
      if (substr(value->data, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      if (substr(value->label, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      if (substr(value->sensitive, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        replace = TRUE;
      }
      value = value->next;
    }
    if (replace) {
      value = out_desc->menu;
      while (value) {
        if (substr(value->data, "$Index")) {
          value->data = substitute_all(value->data, "$Index", index_name);
          substitute_in_text(value->dependences, "$Index", index_name);
          replace = FALSE;
        }
        if (substr(value->label, "$Index")) {
          value->label = substitute_all(value->label, "$Index", index_name);
          if (replace) {
            substitute_in_text(value->dependences, "$Index", index_name);
            replace = FALSE;
          }
        }
        if (substr(value->sensitive, "$Index")) {
          value->sensitive = substitute_all(value->sensitive, "$Index", index_name);
          if (replace) {
            substitute_in_text(value->dependences, "$Index", index_name);
            replace = FALSE;
          }
        }
        value = value->next;
      }
    }
  }
  if (out_desc->min) {
    if (substr(out_desc->min->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->min->data = substitute_all(out_desc->min->data, "$Index", index_name);
      substitute_in_text(out_desc->min->dependences, "$Index", index_name);
    }
  }
  if (out_desc->type->next) {
    if (substr(out_desc->type->next->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->type->next->data = substitute_all(out_desc->type->next->data, "$Index", index_name);
      substitute_in_text(out_desc->type->next->dependences, "$Index", index_name);
    }
  }
  if (out_desc->units) {
    if (substr(out_desc->units->data, "$Index")) {
      if (out_desc == in_desc) out_desc = copy_description(in_desc);
      out_desc->units->data = substitute_all(out_desc->units->data, "$Index", index_name);
      substitute_in_text(out_desc->units->dependences, "$Index", index_name);
    }
  }
  if (out_desc->validf) {
    arg = out_desc->validf->arg_list;
    while (arg) {
      if (substr(arg->str, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      arg = arg->next;
    }
    arg = out_desc->validf->arg_list;
    while (arg) {
      if (substr(arg->str, "$Index")) { arg->str = substitute_all(arg->str, "$Index", index_name); }
      arg = arg->next;
    }
  }
  if (out_desc->display) {
    arg = out_desc->display->arg_list;
    while (arg) {
      if (substr(arg->str, "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
      }
      arg = arg->next;
    }
    arg = out_desc->display->arg_list;
    while (arg) {
      if (substr(arg->str, "$Index")) { arg->str = substitute_all(arg->str, "$Index", index_name); }
      arg = arg->next;
    }
  }
  if (out_desc->dependences) {
    l = 0;
    while (out_desc->dependences[l]) {
      if (substr(out_desc->dependences[l], "$Index")) {
        if (out_desc == in_desc) out_desc = copy_description(in_desc);
        out_desc->dependences[l] = substitute_all(out_desc->dependences[l], "$Index", index_name);
      }
      l++;
    }
  }

  return (out_desc);
}

/* dependences()  ******************************************************/

int dependences(Variable** var_vector, int nvar, Variable* dyn_vec_list, Boolean* p_run_on_comp)
{
  Boolean     all_lower, error = FALSE, found;
  int         bottom_level, dep_number, level, master_number, mvar, ndone, ndone_level, var;
  char*       master;
  Value*      bound;
  Variable *  dep_variable, *master_variable, *new_variable, *prec_variable, *slave_list, *variable;
  static char string[257];

  level = ndone = ndone_level = 0;
  for (var = 0; var < nvar; var++) {
    variable        = var_vector[var];
    variable->match = substitute_all(variable->format, "%d", "%*d");
    strcpy(string, variable->match);
    if (variable->match != variable->format) free(variable->match);
    if (variable->description->type->next) strcat(string, "[%*d]");
    strcat(string, "%[ ]");
    variable->match = newstr(string);
    if (variable->description->dependences[0])
      variable->dependence_level = PrMMAXINT;
    else {
      variable->dependence_level = 0;
      ndone++;
      ndone_level++;
    }
    variable->n_dependents = 0;
    variable->dependents   = NULL;
    variable->default_ctl  = NULL;
    variable->max_ctl      = NULL;
    variable->min_ctl      = NULL;
    variable->influenced   = NULL;
  }

  /*
      Find the dependence level of each variable
  */
  while (ndone_level > 0 && ndone < nvar) {
    level++;
    ndone_level = 0;
    for (var = 0; var < nvar; var++) {
      variable = var_vector[var];
      if (variable->dependence_level == PrMMAXINT) {
        all_lower     = TRUE;
        master_number = 0;
        while ((master = variable->description->dependences[master_number])) {
          found = FALSE;
          for (mvar = 0; mvar < nvar; mvar++) {
            master_variable = var_vector[mvar];
            if ((found = match(master_variable, master))) break;
          }
          if (found) {
            if (master_variable->dependence_level >= level) {
              all_lower = FALSE;
              break;
            }
            if (master_variable->description->output) {
              error = TRUE;
              fprintf(stderr, "*** %s depends on $Output %s, which is forbiden\n", variable->name,
                      master_variable->name);
            } else if (master_variable->description->volatil) {
              error = TRUE;
              fprintf(stderr, "*** %s depends on $Volatil %s, which is forbiden\n", variable->name,
                      master_variable->name);
            } else if (master_variable->description->auxiliary && master_variable->dyn_ancestor) {
              error = TRUE;
              fprintf(stderr,
                      "*** %s depends on $Auxiliary with dynamic ancestor %s, which is forbiden\n",
                      variable->name, master_variable->name);
            }
          } else {
            fprintf(stderr, "*** Unable to resolve dependence on %s for %s\n", master,
                    variable->name);
            variable->dependence_level = -1;
            all_lower                  = FALSE;
            error                      = TRUE;
          }
          master_number++;
        }
        if (all_lower) {
          variable->dependence_level = level;
          ndone++;
          ndone_level++;
        }
      }
    }
  }

  if (ndone < nvar) {
    error = TRUE;
    for (var = 0; var < nvar; var++) {
      variable = var_vector[var];
      if (variable->dependence_level == PrMMAXINT) {
        fprintf(stderr, "*** Dependence loop for %s\n", variable->name);
      }
    }
  }

  for (var = 0; var < nvar; var++) {
    variable      = var_vector[var];
    master_number = 0;
    while ((master = variable->description->dependences[master_number])) {
      for (mvar = 0; mvar < nvar; mvar++) {
        master_variable = var_vector[mvar];
        if (match(master_variable, master)) break;
      }
      dep_variable = master_variable->influenced;
      found        = FALSE;
      while (dep_variable) {
        if (0 == strcmp(dep_variable->name, variable->name)) {
          found = TRUE;
          break;
        }
        dep_variable = dep_variable->next;
      }
      if (!found) {
        new_variable                = copy_variable(variable);
        new_variable->related       = variable->description->related[master_number];
        new_variable->next          = master_variable->influenced;
        master_variable->influenced = new_variable;
      }
      if (variable->description->kind == COMPILED &&
          master_variable->description->kind == RUNTIME) {
        fprintf(stderr, "*** Dependence on runtime parameter %s for compiled %s\n",
                master_variable->name, variable->name);
        error = TRUE;
      }
      if (variable->description->kind == RUNTIME &&
          master_variable->description->kind == COMPILED) {
        *p_run_on_comp = TRUE;
      }
      master_number++;
    }
    master_number = 0;
    if (variable->description->default_value && variable->description->default_value->dependences) {
      while ((master = variable->description->default_value->dependences[master_number])) {
        for (mvar = 0; mvar < nvar; mvar++) {
          master_variable = var_vector[mvar];
          if (match(master_variable, master)) break;
        }
        dep_variable = master_variable->default_ctl;
        found        = FALSE;
        while (dep_variable) {
          if (0 == strcmp(dep_variable->name, variable->name)) {
            found = TRUE;
            break;
          }
          dep_variable = dep_variable->next;
        }
        if (!found) {
          new_variable                 = copy_variable(variable);
          new_variable->related        = variable->description->default_value->related[master_number];
          new_variable->next           = master_variable->default_ctl;
          master_variable->default_ctl = new_variable;
        }
        master_number++;
      }
    } /* if (variable->description->default_value &&
          variable->description->default_value->dependences) */
    master_number = 0;
    if (variable->description->output && variable->description->output->dependences) {
      while ((master = variable->description->output->dependences[master_number])) {
        for (mvar = 0; mvar < nvar; mvar++) {
          master_variable = var_vector[mvar];
          if (match(master_variable, master)) break;
        }
        dep_variable = master_variable->default_ctl;
        found        = FALSE;
        while (dep_variable) {
          if (0 == strcmp(dep_variable->name, variable->name)) {
            found = TRUE;
            break;
          }
          dep_variable = dep_variable->next;
        }
        if (!found) {
          new_variable                 = copy_variable(variable);
          new_variable->related        = variable->description->output->related[master_number];
          new_variable->next           = master_variable->default_ctl;
          master_variable->default_ctl = new_variable;
        }
        master_number++;
      }
    } /* if (variable->description->output &&
          variable->description->output->dependences) */
    master_number = 0;
    if (variable->description->max && variable->description->max->dependences) {
      while ((master = variable->description->max->dependences[master_number])) {
        for (mvar = 0; mvar < nvar; mvar++) {
          master_variable = var_vector[mvar];
          if (match(master_variable, master)) break;
        }
        dep_variable = master_variable->max_ctl;
        found        = FALSE;
        while (dep_variable) {
          if (0 == strcmp(dep_variable->name, variable->name)) {
            found = TRUE;
            break;
          }
          dep_variable = dep_variable->next;
        }
        if (!found) {
          new_variable             = copy_variable(variable);
          new_variable->related    = variable->description->max->related[master_number];
          new_variable->next       = master_variable->max_ctl;
          master_variable->max_ctl = new_variable;
        }
        master_number++;
      }
    } /* if (variable->description->min &&
          variable->description->min->dependences) */
    master_number = 0;
    if (variable->description->min && variable->description->min->dependences) {
      while ((master = variable->description->min->dependences[master_number])) {
        for (mvar = 0; mvar < nvar; mvar++) {
          master_variable = var_vector[mvar];
          if (match(master_variable, master)) break;
        }
        dep_variable = master_variable->min_ctl;
        found        = FALSE;
        while (dep_variable) {
          if (0 == strcmp(dep_variable->name, variable->name)) {
            found = TRUE;
            break;
          }
          dep_variable = dep_variable->next;
        }
        if (!found) {
          new_variable             = copy_variable(variable);
          new_variable->related    = variable->description->min->related[master_number];
          new_variable->next       = master_variable->min_ctl;
          master_variable->min_ctl = new_variable;
        }
        master_number++;
      }
    } /* if (variable->description->min &&
          variable->description->min->dependences) */
  }

  variable = dyn_vec_list;
  while (variable) {
    dep_variable = variable_named(NULL, variable->name);
    bottom_level = variable->array_level;
    bound        = variable->bounds;
    while (bound) {
      if (bound->dependences) {
        dep_number = 0;
        while ((master = bound->dependences[dep_number])) {
          if (bound->type != INUM) {
            master_variable = variable_named(NULL, master);
            if (master_variable) {
              new_variable                = copy_variable(dep_variable);
              new_variable->bottom_level  = bottom_level;
              new_variable->bottom_bound  = bound;
              new_variable->related       = bound->related[dep_number];
              new_variable->next          = master_variable->dependents;
              master_variable->dependents = new_variable;
              master_variable->n_dependents++;
            } else {
              fprintf(stderr, "*** Unable to resolve %s in bound %s for %s\n", master, bound->data,
                      variable->name);
              error = TRUE;
            }
          }
          dep_number++;
        }
        variable->bottom_level = bottom_level;
        variable->bottom_bound = bound;
      }
      bottom_level--;
      bound = bound->next;
    }
    variable->dependence_level = dep_variable->dependence_level;
    variable                   = variable->next;
  }

  if (error) {
    fprintf(stderr, "\n");
    exit(-1);
  }

  /*
      For each variable with dependents, max_ctl, min_ctl, or default_ctl,
      sort these in increasing dependence level order.
  */
  for (var = 0; var < nvar; var++) {
    master_variable = var_vector[var];
    if (master_variable->n_dependents > 1) {
      slave_list                  = master_variable->dependents;
      master_variable->dependents = NULL;
      for (ndone_level = level; ndone_level >= 0; ndone_level--) {
        for (variable = slave_list; variable; variable = new_variable) {
          new_variable = variable->next;
          if (variable->dependence_level == ndone_level) {
            if (variable == slave_list)
              slave_list = slave_list->next;
            else
              prec_variable->next = new_variable;
            variable->next              = master_variable->dependents;
            master_variable->dependents = variable;
          } else
            prec_variable = variable;
        }
      }
    }
    if (master_variable->default_ctl) {
      slave_list                   = master_variable->default_ctl;
      master_variable->default_ctl = NULL;
      for (ndone_level = level; ndone_level >= 0; ndone_level--) {
        for (variable = slave_list; variable; variable = new_variable) {
          new_variable = variable->next;
          if (variable->dependence_level == ndone_level) {
            if (variable == slave_list)
              slave_list = slave_list->next;
            else
              prec_variable->next = new_variable;
            variable->next               = master_variable->default_ctl;
            master_variable->default_ctl = variable;
          } else
            prec_variable = variable;
        }
      }
    }
  }

  return (level);
}
