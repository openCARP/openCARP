// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    arg_code:  Contains read_arg_code() function.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  April 4, 2000

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* read_arg_code()  ******************************************************/
/*
    Prints code for reading a variable in command line arguments
*/
void read_arg_code(FILE* file, Variable* variable)
{
  int          a_level, bound_level, dep_number, index, level, min, max;
  char         format[257], *match, name[257], *vname;
  Value*       bound;
  Variable*    dep_variable;
  extern char *Type_Formats[], *Type_Names[];

  if (variable->description->output || variable->description->auxiliary ||
      variable->description->hidden)
    return;

  if (variable->description->type->type == BYTE) {
    min = 0;
    max = 255;
  } else if (variable->description->type->type == CHAR) {
    min = -128;
    max = 127;
  } else if (variable->description->type->type == SHORT) {
    min = -32768;
    max = 32767;
  }
  index   = variable->description->type->type - TYPE_BASE;
  a_level = variable->array_level;
  if (variable->description->key)
    vname = variable->description->key;
  else
    vname = variable->name;
  strcpy(format, variable->format);
  if (variable->description->storage != SCALAR) {
    strcat(format, "[%d]");
    snprintf(name, sizeof name, "%s[PrMelem%d]", vname, a_level);
  } else
    strcpy(name, vname);

  if (a_level) {
    fprintf(file, "    else if (1==sscanf(PrMargument,\n");
    fprintf(file, "     \"-%s\", PrMspace)) {\n", variable->match);
    fprintf(file, "\tsscanf(PrMargv[PrMarg], \"-%s\"\n\t ", format);
    for (level = 1; level <= a_level; level++) fprintf(file, ", &PrMelem%d", level);
    fprintf(file, ");\n");
    for (level = 1; level <= a_level; level++) {
      bound = variable->bounds;
      for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
      fprintf(file, "\tif (PrMelem%d>=%s || PrMelem%d<0) {\n", level, bound->data, level);
      fprintf(file, "\t    fprintf(stderr,\n");
      fprintf(file, "\t     \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n",
              level);
      fprintf(file, "\t     PrMelem%d, PrMargv[PrMarg], %s-1);\n", level, bound->data);
      fprintf(file, "\t    return(PrMFATAL);\n");
      fprintf(file, "\t}\n");
    }
  } else {
    fprintf(file, "    else if (0==strcmp(PrMargument, \"-%s \")) {\n", vname);
  }
  if (variable->description->type->type != FLAG) {
    fprintf(file, "\tif (PrMargc <= ++PrMarg) {\n");
    fprintf(file, "\t    fprintf(stderr,\n");
    fprintf(file, "\t     \"\\n*** Missing argument after %%s\\n\\n\",\n");
    fprintf(file, "\t     PrMargv[PrMarg-1]);\n");
    fprintf(file, "\t    return(PrMFATAL);\n");
    fprintf(file, "\t}\n");
  }

  dep_number   = 0;
  dep_variable = variable->dependents;
  while (dep_variable) {
    fprintf(file, "\tPrMold_size%d = %s;\n", dep_number, dep_variable->bottom_bound->data);
    dep_number++;
    dep_variable = dep_variable->next;
  }

  if (variable->description->type->type == FLAG) {
    fprintf(file, "\t%s = PrMTRUE;\n", name);
  } else {
    switch (variable->description->type->type) {
      case BOOLEAN:
        fprintf(file, "\tPrMlowercase(PrMargv[PrMarg]);\n");
        fprintf(file, "\tif (0==strcmp(PrMargv[PrMarg], \"true\"))\n");
        fprintf(file, "\t    %s = PrMTRUE;\n", name);
        fprintf(file, "\telse if (0==strcmp(PrMargv[PrMarg], \"vrai\"))\n");
        fprintf(file, "\t    %s = PrMTRUE;\n", name);
        fprintf(file, "\telse if (0==strcmp(PrMargv[PrMarg], \"1\"))\n");
        fprintf(file, "\t    %s = PrMTRUE;\n", name);
        fprintf(file, "\telse if (0==strcmp(PrMargv[PrMarg], \"false\"))\n");
        fprintf(file, "\t    %s = PrMFALSE;\n", name);
        fprintf(file, "\telse if (0==strcmp(PrMargv[PrMarg], \"faux\"))\n");
        fprintf(file, "\t    %s = PrMFALSE;\n", name);
        fprintf(file, "\telse if (0==strcmp(PrMargv[PrMarg], \"0\"))\n");
        fprintf(file, "\t    %s = PrMFALSE;", name);
        fprintf(file, "\telse {\n");
        break;
      case BYTE:
      case CHAR:
      case SHORT:
        fprintf(file, "\tif (1==sscanf(PrMargv[PrMarg], \"%%d\", &PrMint_arg)) {\n");
        fprintf(file, "\t    if (PrMint_arg<%d || PrMint_arg>%d) {\n", min, max);
        fprintf(file, "\t\tfprintf(stderr,\n");
        fprintf(file, "\t\t \"\\n*** Invalid %s value %%d for %%s\\n\\n\",\n", Type_Names[index]);
        fprintf(file, "\t\t PrMint_arg, PrMargv[PrMarg-1]);\n");
        fprintf(file, "\t\treturn(PrMFATAL);\n");
        fprintf(file, "\t    }\n");
        fprintf(file, "\t    else {\n");
        fprintf(file, "\t\t%s = PrMint_arg;\n", name);
        fprintf(file, "\t    }\n");
        fprintf(file, "\t}\n");
        fprintf(file, "\telse {\n");
        break;
      case COMPLEX:
      case D_COMPLEX:
        fprintf(file, "\tif (3!=sscanf(PrMargv[PrMarg], %s, &%s.real, &%s.imag, junk)) {\n",
                Type_Formats[index], name, name);
        break;
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->length) {
          fprintf(file, "\t{\n");
          fprintf(file, "\t    strncpy(%s, PrMargv[PrMarg], %d-1);\n", name,
                  variable->description->length);
          fprintf(file, "\t    if (strlen(PrMargv[PrMarg])>%d-1) {\n",
                  variable->description->length);
        } else {
          fprintf(file, "\tif (strlen(PrMargv[PrMarg])==0) %s = PrMnewstr(\"\");\n", name);
          fprintf(file, "\telse {\n");
          fprintf(file, "\t    %s = (char*)realloc(%s, strlen(PrMargv[PrMarg])+1);\n", name, name);
          fprintf(file, "\t    if (NULL==strcpy(%s, PrMargv[PrMarg])) {\n", name);
        }
        break;
      default:
        fprintf(file, "\tif (1!=sscanf(PrMargv[PrMarg], %s, &%s)) {\n", Type_Formats[index], name);
        break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "\t    fprintf(stderr,\n");
    fprintf(file, "\t     \"\\n*** Error reading value %%s for %%s\\n\\n\", \n");
    fprintf(file, "\t     PrMargv[PrMarg], PrMargv[PrMarg-1]);\n");
    fprintf(file, "\t    return(PrMFATAL);\n");
    fprintf(file, "\t}\n");
  }
  switch (variable->description->type->type) {
    case RDIR:
    case RWDIR:
    case WDIR:
    case RFILE:
    case RWFILE:
    case WFILE:
    case STRING:
    case EXPR: fprintf(file, "\t}\n"); break;
  } /* switch (variable->description->type->type) */
  if (variable->description->storage == SCALAR)
    fprintf(file, "\t%s = PrMTRUE;\n", variable->boolean);
  else
    fprintf(file, "\t%s[PrMelem%d] = PrMTRUE;\n", variable->boolean, a_level);
  /*
      Code to realloc dependents variables
  */
  dep_number   = 0;
  dep_variable = variable->dependents;
  while (dep_variable) {
    realloc_code(file, dep_variable, "\t", dep_number);
    dep_number++;
    dep_variable = dep_variable->next;
  }
  /*
      Code to reinitialize default_ctl variables
  */
  dep_variable = variable->default_ctl;
  while (dep_variable) {
    reinitial_code(file, dep_variable, "\t");
    dep_variable = dep_variable->next;
  }

  fprintf(file, "    }\n");

  /*
      Code to read an array
  */
  if (variable->description->storage != SCALAR) {
    match = substitute_all(variable->format, "%d", "%*d");
    strcpy(format, match);
    if (match != variable->format) free(match);
    strcat(format, "%[ ]");
    match = format;
    if (a_level > 1) {
      fprintf(file, "    else if (1==sscanf(PrMargument,\n");
      fprintf(file, "     \"-%s\", PrMspace)) {\n", match);
      fprintf(file, "\tsscanf(PrMargv[PrMarg], \"%s\"\n\t ", variable->format);
      for (level = 1; level < a_level; level++) fprintf(file, ", &PrMelem%d", level);
      fprintf(file, ");\n");
      for (level = 1; level < a_level; level++) {
        bound = variable->bounds;
        for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
        fprintf(file, "\tif (PrMelem%d>=%s || PrMelem%d<0) {\n", level, bound->data, level);
        fprintf(file, "\t    fprintf(stderr,\n");
        fprintf(file, "\t     \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n",
                level);
        fprintf(file, "\t     PrMelem%d, PrMargv[PrMarg], %s-1);\n", level, bound->data);
        fprintf(file, "\t    return(PrMFATAL);\n");
        fprintf(file, "\t}\n");
      }
    } else {
      fprintf(file, "    else if (0==strcmp(PrMargument, \"-%s \")) {\n", vname);
    }
    fprintf(file, "\tif (PrMargc <= ++PrMarg) {\n");
    fprintf(file, "\t    fprintf(stderr,\n");
    fprintf(file, "\t     \"\\n*** Missing argument after -%s\\n\\n\");\n", vname);
    fprintf(file, "\t    return(PrMFATAL);\n");
    fprintf(file, "\t}\n");

    dep_number   = 0;
    dep_variable = variable->dependents;
    while (dep_variable) {
      fprintf(file, "\tPrMold_size%d = %s;\n", dep_number, dep_variable->bottom_bound->data);
      dep_number++;
      dep_variable = dep_variable->next;
    }

    /*
  Code to read different values for each array element
    */
    fprintf(file, "\tif (strchr(PrMargv[PrMarg], '{')) {\n");
    fprintf(file, "\t    PrMword = strtok(PrMargv[PrMarg], \" \\t{,}\");\n");
    fprintf(file, "\t    for (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", a_level, a_level,
            variable->bounds->data, a_level);
    fprintf(file, "\t\tif (NULL == PrMword) {\n");
    fprintf(file, "\t\t    fprintf(stderr,\n");
    fprintf(file, "\t\t     \"\\n*** %%d argument(s) missing after %s\\n\\n\",\n", vname);
    fprintf(file, "\t\t     %s-PrMelem%d);\n", variable->bounds->data, a_level);
    fprintf(file, "\t\t    return(PrMFATAL);\n");
    fprintf(file, "\t\t}\n");
    switch (variable->description->type->type) {
      case FLAG:
      case BOOLEAN:
        fprintf(file, "\t\tPrMlowercase(PrMword);\n");
        fprintf(file, "\t\tif (0==strcmp(PrMword, \"true\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMTRUE;\n", vname, a_level);
        fprintf(file, "\t\telse if (0==strcmp(PrMword, \"vrai\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMTRUE;\n", vname, a_level);
        fprintf(file, "\t\telse if (0==strcmp(PrMword, \"1\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMTRUE;\n", vname, a_level);
        fprintf(file, "\t\telse if (0==strcmp(PrMword, \"false\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMFALSE;\n", vname, a_level);
        fprintf(file, "\t\telse if (0==strcmp(PrMword, \"faux\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMFALSE;\n", vname, a_level);
        fprintf(file, "\t\telse if (0==strcmp(PrMword, \"0\"))\n");
        fprintf(file, "\t\t    %s[PrMelem%d] = PrMFALSE;\n", vname, a_level);
        fprintf(file, "\t\telse {\n");
        break;
      case BYTE:
      case CHAR:
      case SHORT:
        fprintf(file, "\t\tif (1==sscanf(PrMword, \"%%d\", &PrMint_arg)) {\n");
        fprintf(file, "\t\t    if (PrMint_arg<%d || PrMint_arg>%d) {\n", min, max);
        fprintf(file, "\t\t\tfprintf(stderr,\n");
        fprintf(file, "\t\t\t \"\\n*** Invalid %s value %%d for %s[PrMelem%d]\\n\\n\",\n",
                Type_Names[index], vname, a_level);
        fprintf(file, "\t\t\t PrMint_arg);\n");
        fprintf(file, "\t\t\treturn(PrMFATAL);\n");
        fprintf(file, "\t\t    }\n");
        fprintf(file, "\t\t    else {\n");
        fprintf(file, "\t\t\t%s[PrMelem%d] = PrMint_arg;\n", vname, a_level);
        fprintf(file, "\t\t    }\n");
        fprintf(file, "\t\t}\n");
        fprintf(file, "\t\telse {\n");
        break;
      case COMPLEX:
      case D_COMPLEX:
        fprintf(file, "\t\tif (3!=sscanf(PrMword, %s,\n", Type_Formats[index]);
        fprintf(file, "\t\t &%s[PrMelem%d].real, &%s[PrMelem%d].imag, junk)) {\n", vname, a_level,
                vname, a_level);
        break;
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->length) {
          fprintf(file, "\t\t{\n");
          fprintf(file, "\t\t    strncpy(%s[PrMelem%d], PrMargv[PrMarg], %d-1);\n", vname, a_level,
                  variable->description->length);
          fprintf(file, "\t\t    if (strlen(PrMargv[PrMarg])>%d-1) {\n",
                  variable->description->length);
        } else {
          fprintf(file, "\t\tif (strlen(PrMargv[PrMarg])==0) %s[PrMelem%d] = PrMnewstr(\"\");\n",
                  vname, a_level);
          fprintf(file, "\t\telse {\n");
          fprintf(file, "\t\t    %s[PrMelem%d] = \n", vname, a_level);
          fprintf(file, "\t\t     (char*)realloc(%s[PrMelem%d], strlen(PrMword)+1);\n", vname,
                  a_level);
          fprintf(file, "\t\t    if (NULL==strcpy(%s[PrMelem%d], PrMword)) {\n", vname, a_level);
        }
        break;
      default:
        fprintf(file, "\t\tif (1!=sscanf(PrMword, %s, &%s[PrMelem%d])) {\n", Type_Formats[index],
                vname, a_level);
        break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "\t\t    fprintf(stderr,\n");
    fprintf(file, "\t\t     \"\\n*** Error reading value %%s for %%s\\n\\n\",\n");
    fprintf(file, "\t\t     PrMargv[PrMarg], PrMargv[PrMarg-1]);\n");
    fprintf(file, "\t\t    return(PrMFATAL);\n");
    fprintf(file, "\t\t}\n");
    switch (variable->description->type->type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR: fprintf(file, "\t\t}\n"); break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "\t\t%s[PrMelem%d] = PrMTRUE;\n", variable->boolean, a_level);
    fprintf(file, "\t\tPrMword = strtok(NULL, \" \\t{,}\");\n");
    fprintf(file, "\t    }\n");
    fprintf(file, "\t}\n");
    /*
  Code to read a single value and assign it to the entire array
    */
    fprintf(file, "\telse {\n");
    switch (variable->description->type->type) {
      case FLAG:
      case BOOLEAN:
        fprintf(file, "\t    PrMlowercase(PrMargv[PrMarg]);\n");
        fprintf(file, "\t    if (0==strcmp(PrMargv[PrMarg], \"true\"))\n");
        fprintf(file, "\t\t%s[0] = PrMTRUE;\n", vname);
        fprintf(file, "\t    else if (0==strcmp(PrMargv[PrMarg], \"vrai\"))\n");
        fprintf(file, "\t\t%s[0] = PrMTRUE;\n", vname);
        fprintf(file, "\t    else if (0==strcmp(PrMargv[PrMarg], \"1\"))\n");
        fprintf(file, "\t\t%s[0] = PrMTRUE;\n", vname);
        fprintf(file, "\t    else if (0==strcmp(PrMargv[PrMarg], \"false\"))\n");
        fprintf(file, "\t\t%s[0] = PrMFALSE;\n", vname);
        fprintf(file, "\t    else if (0==strcmp(PrMargv[PrMarg], \"faux\"))\n");
        fprintf(file, "\t\t%s[0] = PrMFALSE;\n", vname);
        fprintf(file, "\t    else if (0==strcmp(PrMargv[PrMarg], \"0\"))\n");
        fprintf(file, "\t\t%s[0] = PrMFALSE;\n", vname);
        fprintf(file, "\t    else {\n");
        break;
      case BYTE:
      case CHAR:
      case SHORT:
        fprintf(file, "\t    if (1==sscanf(PrMargv[PrMarg], \"%%d\", &PrMint_arg)) {\n");
        fprintf(file, "\t\tif (PrMint_arg<%d || PrMint_arg>%d) {\n", min, max);
        fprintf(file, "\t\t    fprintf(stderr,\n");
        fprintf(file, "\t\t     \"\\n*** Invalid %s value %%d for %s\\n\\n\",\n", Type_Names[index],
                vname);
        fprintf(file, "\t\t     PrMint_arg);\n");
        fprintf(file, "\t\t    return(PrMFATAL);\n");
        fprintf(file, "\t\t}\n");
        fprintf(file, "\t\telse {\n");
        fprintf(file, "\t\t    %s[0] = PrMint_arg;\n", vname);
        fprintf(file, "\t\t}\n");
        fprintf(file, "\t    }\n");
        fprintf(file, "\t    else {\n");
        break;
      case COMPLEX:
      case D_COMPLEX:
        fprintf(file, "\t    if (3!=sscanf(PrMargv[PrMarg], %s,\n", Type_Formats[index]);
        fprintf(file, "\t     &%s[0].real, &%s[0].imag, junk)) {\n", vname, vname);
        break;
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->length) {
          fprintf(file, "\t    {\n");
          fprintf(file, "\t\tstrncpy(%s[0], PrMargv[PrMarg], %d-1);\n", vname,
                  variable->description->length);
          fprintf(file, "\t\tif (strlen(PrMargv[PrMarg])>%d-1) {\n", variable->description->length);
        } else {
          fprintf(file, "\t    if (strlen(PrMargv[PrMarg])==0) %s[0] = PrMnewstr(\"\");\n", vname);
          fprintf(file, "\t    else {\n");
          fprintf(file, "\t\t%s[0] = (char*)realloc(%s[0], strlen(PrMargv[PrMarg])+1);\n", vname,
                  vname);
          fprintf(file, "\t\tif (NULL==strcpy(%s[0], PrMargv[PrMarg])) {\n", vname);
        }
        break;
      default:
        fprintf(file, "\t    if (1!=sscanf(PrMargv[PrMarg], %s, &%s[0])) {\n", Type_Formats[index],
                vname);
        break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "\t\tfprintf(stderr,\n");
    fprintf(file, "\t\t \"\\n*** Error reading value %%s for %%s\\n\\n\",\n");
    fprintf(file, "\t\t PrMargv[PrMarg], PrMargv[PrMarg-1]);\n");
    fprintf(file, "\t\treturn(PrMFATAL);\n");
    fprintf(file, "\t    }\n");
    switch (variable->description->type->type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR: fprintf(file, "\t    }\n"); break;
    } /* switch (variable->description->type->type) */
    fprintf(file, "\t    %s[0] = PrMTRUE;\n", variable->boolean);
    fprintf(file, "\t    for (PrMelem%d=1; PrMelem%d<%s; PrMelem%d++) {\n", a_level, a_level,
            variable->bounds->data, a_level);
    switch (variable->description->type->type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->length) {
          fprintf(file, "\t\tstrncpy(%s[PrMelem%d], %s[0], %d-1);\n", vname, a_level, vname,
                  variable->description->length);
        } else {
          fprintf(file, "\t\tfree(%s[PrMelem%d]);\n", vname, a_level);
          fprintf(file, "\t\t%s[PrMelem%d] = PrMnewstr(%s[0]);\n", vname, a_level, vname);
        }
        break;
      default: fprintf(file, "\t\t%s[PrMelem%d] = %s[0];\n", vname, a_level, vname); break;
    }
    fprintf(file, "\t\t%s[PrMelem%d] = PrMTRUE;\n", variable->boolean, a_level);
    fprintf(file, "\t    }\n");
    fprintf(file, "\t}\n");
    /*
  Code to realloc dependents variables
    */
    dep_number   = 0;
    dep_variable = variable->dependents;
    while (dep_variable) {
      realloc_code(file, dep_variable, "\t", dep_number);
      dep_number++;
      dep_variable = dep_variable->next;
    }
    /*
  Code to reinitialize default_ctl variables
    */
    dep_variable = variable->default_ctl;
    while (dep_variable) {
      reinitial_code(file, dep_variable, "\t");
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    }\n");
  } /* if (variable->description->storage!=SCALAR) */
}
