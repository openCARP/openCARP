// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    param.c

    Author:	Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:	Febuary 10, 1999

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>

#include "param.h"
#include "y.tab.h"

#if (SYSTEME == WINNT)
#define SLASH '\\'
#else
#define SLASH   '/'
#define ENGLISH 1
int copyright();
#endif

int yyparse();

/* main()		*****************************************************/

int main(int argc, char* argv[])
{
  extern Parameters parameters;
  extern int        c_dialect, interface, language, lineno;
  extern Boolean    parser_error;
  extern char*      pdffile;
  char *            autosave = NULL, *command = NULL, *dummy_name = NULL, *exec_name,
       *extension = (char*)".prm", file_name[257], *func_name = (char*)"param",
       *macro_name = (char*)"PARAMETERS", *mkf_name = NULL, *prog_name = NULL, *root_name = NULL,
       *slash;
  int     arg = 1, verbose = 0;
  Boolean Stdin = FALSE, loop = FALSE;

  language  = C;
  interface = NONE;
#if (SYSTEME == SUNOS)
  c_dialect = KR;
#else
  c_dialect = ANSI;
#endif

  /*
      Read parameters from the command line
  */
  while (argc > arg) {
    if (strcmp(argv[arg], "-ansi") == 0) {
      c_dialect = ANSI;
      arg++;
    } else if (strcmp(argv[arg], "-command") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -command\n\n");
        exit(-1);
      }
      command = argv[arg++];
    } else if (strcmp(argv[arg], "-dummy") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -dummy\n\n");
        exit(-1);
      }
      dummy_name = argv[arg++];
    } else if (strcmp(argv[arg], "-ext") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -ext\n\n");
        exit(-1);
      }
      extension = argv[arg++];
    } else if (strcmp(argv[arg], "-fortran") == 0) {
      language = FORTRAN;
      arg++;
    } else if (strcmp(argv[arg], "-func") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -func\n\n");
        exit(-1);
      }
      func_name = argv[arg++];
    } else if (strcmp(argv[arg], "-kr") == 0) {
      c_dialect = KR;
      arg++;
    } else if (strcmp(argv[arg], "-loop") == 0) {
      loop = TRUE;
      arg++;
    } else if (strcmp(argv[arg], "-macro") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -macro\n\n");
        exit(-1);
      }
      macro_name = argv[arg++];
    } else if (strcmp(argv[arg], "-matlab") == 0) {
      language = MATLAB;
      arg++;
    } else if (strcmp(argv[arg], "-mkf") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -mkf\n\n");
        exit(-1);
      }
      mkf_name = argv[arg++];
    } else if (strcmp(argv[arg], "-motif") == 0) {
      interface = MOTIF;
      arg++;
    } else if (strcmp(argv[arg], "-prog") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -prog\n\n");
        exit(-1);
      }
      prog_name = argv[arg++];
    } else if (strcmp(argv[arg], "-save") == 0) {
      if (argc == ++arg) {
        fprintf(stderr, "\n*** Missing argument after -save\n\n");
        exit(-1);
      }
      autosave = argv[arg++];
    } else if (strcmp(argv[arg], "-v") == 0) {
      verbose++;
      arg++;
    } else if (strcmp(argv[arg], "-") == 0) {
      Stdin = TRUE;
      arg++;
    } else {
      if (root_name || argv[arg][0] == '-') {
        fprintf(stderr, "\n*** Unknown argument '%s' while processing file %s.prm\n\n", argv[arg],
                root_name);
        exit(-1);
      } else
        root_name = argv[arg++];
    }
  }

  if (root_name == NULL) root_name = (char*)"param";
  if (autosave == NULL) autosave = (char*)"";
  if (mkf_name == NULL) mkf_name = (char*)"";

#if (SYSTEME != SGI4D)
  if (language == FORTRAN) {
    fprintf(stderr, "\n*** FORTRAN is supported only on SGI 4D computers\n\n");
    exit(-1);
  }
#endif
#ifdef NOMOTIF
  if (interface == MOTIF) {
    fprintf(stderr, "\n*** Motif is not supported on Sun computers\n\n");
    exit(-1);
  }
#endif

  if (!Stdin) {
    strcpy(file_name, root_name);
    strcat(file_name, extension);
    if (freopen(file_name, "r", stdin) == NULL) {
      extension = (char*)".pdf";
      strcpy(file_name, root_name);
      strcat(file_name, extension);
      free(pdffile);
      if (freopen(file_name, "r", stdin) == NULL) {
        fprintf(stderr, "\n*** Unable to open %s\n\n", file_name);
        exit(-1);
      }
    }
    pdffile = newstr(file_name);
  }

  /* Remove directory from name */
  slash = strrchr(root_name, SLASH);
  if (slash) {
    root_name = slash;
    root_name++;
  }

  initialize(&parameters);
  if (yyparse() || parser_error) exit(1);
  if (verbose > 0) display(&parameters);
  restructure(&parameters, verbose);
  if (dummy_name)
    exec_name = dummy_name;
  else
    exec_name = root_name;
  build_c(root_name, &parameters, autosave, exec_name, func_name, macro_name, extension);
#if (SYSTEME == SGI4D)
  if (language == FORTRAN) build_f(root_name, func_name, macro_name, &parameters, extension);
#endif
  if (language == MATLAB) build_mex(root_name, func_name, macro_name, &parameters, extension);
  if (parameters.n_compiled > 0) {
    if (prog_name == NULL) prog_name = root_name;
    if (dummy_name == NULL) dummy_name = root_name;
    build_dummy(root_name, &parameters, mkf_name, prog_name, dummy_name, command, loop, func_name,
                macro_name, extension);
    build_make(root_name, mkf_name, prog_name, dummy_name, autosave, command, loop, extension);
  } else if (dummy_name) {
    if (prog_name == NULL) prog_name = (char*)"";
    build_dummy(root_name, &parameters, mkf_name, prog_name, dummy_name, command, loop, func_name,
                macro_name, extension);
    build_make(root_name, mkf_name, prog_name, dummy_name, autosave, command, loop, extension);
  }

  return (0);
}
