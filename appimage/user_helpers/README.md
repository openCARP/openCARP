# openCARP AppImage

The openCARP AppImage contains the openCARP simulator and meshtool.

carputils and the experiments must be installed separately.

## Get the openCARP executables

In order to extract all the openCARP executables, run the script `extract_appimage.sh` with the command `bash extract_appimage.sh`.
A directory named `openCARP/bin` will be created, containing the openCARP binaries.

For example, you can run bench using `./openCARP/bin/bench`.

### Update openCARP

If you want to use a new version of openCARP, the only thing to do is to replace this folder by the new AppImage package of openCARP and to run the script `./extract_appimage.sh` again.

### Uninstall openCARP

In order to remove openCARP from your system, you only have to delete this directory.


## Install carputils

Additionally, you can use the script `install_carputils.sh` in order to install the Python package carputils.

### Requirements

The following software must be installed on your system before running this script:
- python3
- pip
- git
- gcc

On Ubuntu/Debian, you can install it using `sudo apt install python3 python3-pip git gcc`.
On CentOS/Red Hat/Fedora, you can install it using `sudo yum python3 python3-devel git gcc`.

### Install carputils

Once you ensured to have the prerequirements installed, you can run `bash install_carputils.sh` in order to download and install carputils via pip.

### Update carputils

In order to update carputils, you can run the script `bash install_carputils.sh` again.
carputils will be uninstalled and replaced by its latest version.

**Note :** a new settings file will be generated in `$HOME/.config/carputils/settings.yaml`. If a settings file already exist at this location, it will backed up in the same directory. If you are not using the default configuration file, you may want to update the new file with your personal settings.

### Uninstall carputils

You can uninstall carputils by running the following command: `python3 -m pip uninstall carputils`.
