#!/bin/bash

# This script downloads and installs carputils using pip.

CUR_DIR=$PWD
SCRIPT_DIR=`dirname "$(readlink -f "$0")"`

# Check if git is installed
if ! command -v git &> /dev/null; then
    echo "git command can't be found. Please install git to run this script."
    echo "On Debian/Ubuntu, you can run 'sudo apt install git'"
    echo "On Red Hat/CentOS, you can run 'sudo yum install git'"
    exit
fi

# Check if python is installed
if ! command -v python3 &> /dev/null; then
    echo "python3 command can not be found. Please install python3 to run this script."
    echo "On Debian/Ubuntu, you can run 'sudo apt install python3'"
    echo "On Red Hat/CentOS, you can run 'sudo yum install python3 python3-devel'"
    exit
fi

# Check if pip3 is installed
if ! command -v pip3 &> /dev/null; then
    echo "pip command can not be found. Please install pip to run this script."
    echo "On Debian/Ubuntu, you can run 'sudo apt install python3-pip'"
    echo "On Red Hat/CentOS/Fedora, you can run 'sudo yum install python3-pip'"
    exit
fi

# Check if gcc is installed
if ! command -v gcc &> /dev/null; then
    echo "gcc command can not be found. Please install gcc to run this script."
    echo "On Debian/Ubuntu, you can run 'sudo apt install gcc'"
    echo "On Red Hat/CentOS/Fedora, you can run 'sudo yum install gcc'"
    exit
fi

echo Cloning carputils...
git clone https://git.opencarp.org/openCARP/carputils.git

echo Installing carputils...
cd carputils
pip3 install --upgrade pip
pip3 install --upgrade setuptools
pip3 install .

echo Creating carputils settings file...
# Get home directory of the user, even if script is run with sudo.
USER_HOME=$HOME
if [ "$(uname -s)" == "Linux" ]; then
    CANDIDATE_HOME=$(getent passwd "$SUDO_USER" | cut -d: -f6)
    if ! [ -z "${CANDIDATE_HOME}" ]; then
        USER_HOME="${CANDIDATE_HOME}"
    fi
fi
SETTINGS_FILE="${USER_HOME}/.config/carputils/settings.yaml"
if [ -f "$SETTINGS_FILE" ]; then
    echo "Backup the existing settings.yaml..."
    mv $SETTINGS_FILE $SETTINGS_FILE.backup.$(date +%F-%T)
fi
# Add the paths to the executables obtained from the AppImage to the settings file
python3 ./bin/cusettings --mpiexec ${SCRIPT_DIR}/openCARP/bin/mpiexec --software-root ${SCRIPT_DIR}/openCARP/bin $SETTINGS_FILE

cd ..

echo Removing carputils sources...
rm -rf carputils
