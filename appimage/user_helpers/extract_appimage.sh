#!/bin/bash

# This script extracts the openCARP executables contained in the AppImage
# into {APPIMAGE_DIRECTORY}/openCARP/bin

# Get the directory containing the script and the Appimage
SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
OPENCARP_BIN=$SCRIPT_DIR/openCARP/bin
echo Creating directory $OPENCARP_BIN
mkdir -p $OPENCARP_BIN

# Extract the AppImage to get the list of executables it contains
chmod +x $SCRIPT_DIR/openCARP-*.AppImage
$SCRIPT_DIR/openCARP-*.AppImage --appimage-extract

# Extract the executables in $SCRIPT_DIR/openCARP/bin
CUR_DIR=$PWD
cd squashfs-root/usr/bin
for executable in *
do
        echo Extracting $executable in $OPENCARP_BIN
        ln -s $SCRIPT_DIR/openCARP-*.AppImage $OPENCARP_BIN/$executable
done
cd $CUR_DIR
# Remove the directory containing the extracted AppImage
rm -rf squashfs-root

echo 
echo openCARP executables were extracted in $OPENCARP_BIN
echo To add this directory to your path, run
echo export PATH=$OPENCARP_BIN:\$PATH

echo
echo You could now want to install carputils.
echo To do so, run the script ./install_carputils.sh
