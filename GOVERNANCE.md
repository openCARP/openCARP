# Governance of the openCARP project

## Overview
openCARP is a meritocratic, consensus-based community project. Anyone with an interest in the project can join the community, contribute to the project design and participate in the decision making process. This document describes how that participation takes place and how to set about earning merit within the project community.

## Roles And Responsibilities
### Users
Users are community members who have a need for the project. They are the most important members of the community and without them the openCARP project would have no purpose. Anyone can be a user; there are no special requirements.

The openCARP project asks its users to participate in the project and community as much as possible. User contributions enable the project team to ensure that they are satisfying the needs of those users. Common user contributions include (but are not limited to):
* evangelising about the project (e.g. a link on a website and word-of-mouth awareness raising)
* informing developers of strengths and weaknesses from a new user perspective
* providing moral support (a ‘thank you’ goes a long way)
* potentially providing financial support (the software is open, but its development and maintenance needs to be funded)

Users who continue to engage with the project and its community will often become more and more involved. Such users may find themselves becoming contributors, as described in the next section.

### Contributors
Contributors are community members who contribute in concrete ways to the openCARP project. Anyone can become a contributor, and contributions can take many forms, as detailed in a [separate document](https://opencarp.org/community/contribute). There is no expectation of commitment to the project, no specific skill requirements and no selection process.

In addition to their actions as users, contributors may also find themselves doing one or more of the following:
* supporting new users (existing users are often the best people to support new users)
* [reporting bugs](https://git.opencarp.org/groups/openCARP/-/issues)
* participate in discussion on [issues](https://git.opencarp.org/groups/openCARP/-/issues) and [merge requests](https://git.opencarp.org/openCARP/openCARP/merge_requests)
* identifying requirements
* providing graphics and web design
* programming
* assisting with project infrastructure
* writing documentation
* [fixing bugs](https://git.opencarp.org/groups/openCARP/-/issues)
* adding features

Contributors engage with the project through the [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues), [question and answer system](https://www.opencarp.org/q2a), or by writing or editing documentation. They submit changes to the project itself via commits to non-protected branches and subsequent merge requests to protected branches, which will be considered for inclusion in the project by existing Maintainers (see next section). The [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues) is the most appropriate place to ask for help when making that first contribution.

As contributors gain experience and familiarity with the project, their profile within, and commitment to, the community will increase. At some stage, they may consider applying for or being nominated for Maintainership.

### Maintainers
Maintainers are community members who have shown that they are committed to the continued development of the project through ongoing engagement with the community. Maintainership gives decision making power. That is, Maintainers can change project outputs by accepting merge requests to protected branches after having reviewed the code contributions. This does not mean that a Maintainer is free to do what they want. While Maintainership indicates a valued member of the community who has demonstrated a healthy respect for the project’s aims and objectives, their own contributions continue to be reviewed by other members of the community before acceptance in an official release. Maintainership can be dedicated to a specific part of the project. The current list of Maintainer is available on the [project website](https://opencarp.org/about/people#maintainers).
In addition to the role of a contributor, Maintainers are expected to support the project by:
* reviewing incoming merge requests
* triaging issues
* proactively fixing bugs 
* generally performing maintenance tasks

Anyone can become a Maintainer; there are no special requirements, other than to have shown a willingness and ability to participate in the project as a team player. Typically, a potential Maintainer will need to show that they have an understanding of the project, its objectives and its strategy. They will also have provided valuable contributions to the project over a period of time. Maintainership can be dedicated to a specific part of the project.

New Maintainers can be nominated by any existing Maintainer. Once they have been nominated, there will be a vote by the Steering Committee (see below). Maintainer voting is one of the few activities that takes place on the project’s private management infrastructure. This is to allow steering committe members to freely express their opinions about a nominee without causing embarrassment. Once the vote has been held, the aggregated voting results are published. The nominee is entitled to request an explanation of any ‘no’ votes against them, regardless of the outcome of the vote. This explanation will be provided by the Steering Committee and will be anonymous and constructive in nature.

It is important to recognise that Maintainership is a privilege, not a right. That privilege must be earned and once earned it can be removed by the Steering Committee in extreme circumstances. However, under normal circumstances Maintainership exists for as long as the Maintainer wishes to continue engaging with the project.

A Maintainer who shows an above-average level of contribution to the project, particularly with respect to its strategic direction and long-term health, may be nominated to become a member of the Steering Committee. This role is described below.

### Steering Committee
The Steering Committee consists of those individuals identified as such on the [project website](https://opencarp.org/about/people#steering-committee). The Steering Committee has additional responsibilities over and above those of a Maintainer. These responsibilities ensure the smooth running of the project. Steering Committee members are expected to participate in strategic planning, approve changes to the governance model and manage the copyrights within the project outputs. If you want to bring an issue to the Steering Committee's attention, open an issue with the label "Steering Committe" in the [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues).

Members of the Steering Committee do not have significant authority over other members of the community, although it is the Steering Committee that votes on new Maintainers. It also makes decisions when community consensus cannot be reached. In addition, the Steering Committee has access to the project’s private infrastructure and its archives. This is used for sensitive issues, such as votes for new Maintainers and legal matters that cannot be discussed in public. It is never used for project management or planning.

Membership of the Steering Committee is by invitation from the existing Steering Committee members. A nomination will result in discussion and then a vote by the existing Steering Committee members. Steering Committee membership votes are subject to consensus approval of the current Steering Committee members. Members of the Steering Committee should not share main affiliations.

## Support
All participants in the community are encouraged to provide support for new users within the project management infrastructure. This support is provided as a way of growing the community. Those seeking support should recognise that all support activity within the project is voluntary and is therefore provided as and when time allows. A user requiring guaranteed response times or results should therefore seek to purchase a support contract from the openCARP project or a community member. However, for those willing to engage with the project on its own terms, and willing to help support other users, the community support channels, in particular the [question and answer system](https://www.opencarp.org/q2a), are ideal.

## Contribution Process
Anyone can contribute to the project, regardless of their skills, as there are many ways to contribute. For instance, a contributor might be active on the [question and answer system](https://www.opencarp.org/q2a) and [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues), or might commit code and open merge requests. The various ways of contributing are described in more detail in the [CONTRIBUTING](https://opencarp.org/community/contribute) document.

The [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues) is the most appropriate place for a contributor to ask for help when making their first contribution.

## Decision Making Process
Decisions about all issues, including the future of the project, are made through discussion with all members of the community, from the newest user to the most experienced Steering Committee member. All discussion takes place on the [issue tracker](https://git.opencarp.org/groups/openCARP/-/issues). If discussion takes place elsewhere (in person, video calls, etc.), the main arguments and conclusions are documented in the respective issues.

In order to ensure that the project is not bogged down by endless discussion and continual voting, the project operates a policy of lazy consensus. This allows the majority of decisions to be made without resorting to a formal vote.

### Lazy consensus
Decision making typically involves the following steps:
1. Proposal
2. Discussion
3. Vote (if consensus is not reached through discussion)
4. Decision

Any community member can make a proposal for consideration by the community. In order to initiate a discussion about a new idea, they should open an [issue](https://git.opencarp.org/groups/openCARP/-/issues) using the approriate [labels](https://git.opencarp.org/openCARP/openCARP/-/labels) and (if applicable) implement the idea in a non-protected branch. This will prompt a review and, if necessary, a discussion of the idea. The goal of this review and discussion is to gain approval for the contribution. Since most people in the openCARP community have a shared vision, there is often little need for discussion in order to reach consensus.

In general, as long as nobody explicitly opposes a proposal or patch, it is recognised as having the support of the community. This is called lazy consensus - that is, those who have not stated their opinion explicitly have implicitly agreed to the implementation of the proposal.

Lazy consensus is a very important concept within the project. It is this process that allows a large group of people to efficiently reach consensus, as someone with no objections to a proposal need not spend time stating their position, and others need not spend time reading such mails.

For lazy consensus to be effective, it is necessary to allow at least 1 week before assuming that there are no objections to the proposal. Contributors can ask for a prolongation of another week without further reasonsing once for each issue. This requirement ensures that everyone is given enough time to read, digest and respond to the proposal. This time period is chosen so as to be as inclusive as possible of all participants, regardless of their location and time commitments.

### Voting
Not all decisions can be made using lazy consensus. Issues such as those affecting the strategic direction or legal standing of the project must gain explicit approval in the form of a vote. Every member of the community is encouraged to express their opinions in all discussion and all votes. However, only project Steering Committee members (as defined above) have binding votes for the purposes of decision making.

## Attribution
The openCARP governance model is adapted from the [OSS Watch](http://oss-watch.ac.uk) meritocratic governance model template,
available at <http://oss-watch.ac.uk/resources/meritocraticgovernancemodel#template-for-a-meritocratic-governance-document>
