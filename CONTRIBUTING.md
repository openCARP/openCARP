# Contributing to openCARP

## Introduction and Ground Rules
First off, thank you for considering contributing to openCARP. It's people like you that make openCARP such a great tool.

Following these guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your merge requests.

! openCARP is an open project and we love to receive contributions from our community — you! There are many ways to contribute, from [writing tutorials](https://git.opencarp.org/openCARP/experiments/-/blob/master/TUTORIALS.md) or blog posts, improving the documentation, submitting bug reports and feature requests, [sharing in silico experiments](https://opencarp.org/community/upload-experiment), writing tests, or writing code which can be incorporated into openCARP itself.

Please, don't use the issue tracker for support questions. Check whether the [question and answer system](https://www.opencarp.org/q2a/) can help with your issue. If your problem is not strictly openCARP specific but e.g. Python-related, Stack Overflow is also worth considering.

Please note that the openCARP project is released with a [Contributor Code of Conduct](https://opencarp.org/community/code-of-conduct). By participating in this project you agree to abide by its terms.

## How to Report a Bug
We believe that openCARP should not introduce and security relevant issues that do not stem from the underyling libraries or operating system. In the unlikely event that you find a security vulnerability, do NOT open an issue. Email <security@opencarp.org> instead.

When filing an [issue](https://git.opencarp.org/groups/openCARP/-/issues), make sure to follow the template and answer these five questions:
* What version of openCARP are you using?
* What operating system and processor architecture are you using?
* What did you do?
* What did you expect to see?
* What did you see instead?

General questions should go to the [question and answer system](https://www.opencarp.org/q2a/) instead of the issue tracker. The users there will answer or ask you to file an issue if you've tripped over a bug.

## Your First Contribution
! Unsure where to begin contributing to openCARP? Whether it's improvements to the documentation or the code, you can start by looking through these [beginner](https://git.opencarp.org/groups/openCARP/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Good%20first%20issue) and [help-wanted](https://git.opencarp.org/groups/openCARP/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Help%20wanted) issues.

At this point, you're ready to make your changes! Feel free to ask for help; everyone is a beginner at first! [First Timers Only](https://www.firsttimersonly.com) lists some good resources for getting started with contributing to open source projects.

The process to get your changes into the openCARP repositories is the following:
1. Click on *Request access* to be granted developer permission for the [openCARP group of projects](https://git.opencarp.org/openCARP).
2. Fork off your own branch of the repository
3. Do the changes in your branch

If you like the change and think the project could use it:
* Be sure you have followed the [conventions](#conventions) for the project
* Note our [Code of Conduct](https://opencarp.org/community/code-of-conduct)
* Sign our [Contributor Agreement](https://opencarp.org/cla)
* Include a description of your changes in the [CHANGELOG](https://git.opencarp.org/openCARP/openCARP/-/blob/master/CHANGELOG.md)
* Open a merge request

The maintainer team looks at merge requests on a regular basis. After feedback has been given, we expect responses within a month. After that time, we may close the merge request if it isn't showing any activity. Merge requests can be merged after positive review of at least one maintainer responsible for the respective part of the project.

Responsibilities:
* Ensure cross-platform compatibility for every change that's accepted.
* Create [issues](https://git.opencarp.org/groups/openCARP/-/issues) for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback. See our [governance model](https://opencarp.org/about/governance) for more details.
* Keep commits small, preferably one new feature / fixed issue per commit.
* Be welcoming to newcomers and encourage diverse new contributors from all backgrounds.

If a maintainer asks you to "rebase" your merge request, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge.

## Conventions
This section is supposed to expose some conventions that may not be obvious
to a new user or contributor. This list is neither complete nor final.

### Coding conventions
* Be consistent with existing code and documents. We do not provide a strict coding style guide, but you could optionally format your code by `clang-format` within our [openCARP developer docker image](https://git.opencarp.org/openCARP/openCARP/tree/master/docker#for-developers).
* Use spaces for indentation, do not use tabs.
* Name functions and variables in a way they are self explanatory.
* We try to keep the naming scheme to snake case (e.g. `time_trace`), as opposed
  to camel-case (e.g. `timeTrace`). Since we tend to have large function names, this
  increases readability. Since we move many code parts over from an existing code-base,
  the renaming may not be enforced with the first commits, but should be enforced over
  time.
* User-defined globals should be in the namespace `user_globals`. The globals defined
  automatically by param are in the namespace `param_globals`.
* Each module (e.g. `simulator`, `numerics`) has a header file of the same name that
  exposes the relevant definitions of that particular module. As such, including the
  capabilities of a module is streamlined.
* We have individual namespaces for the main code parts:
  * openCARP simulator: `opencarp`
  * limpet single cell models: `limpet`
  * slimfem mesh management and FEM framework: `SF`

### Design conventions
* The simulated physics (i.e. physical models such as cell and tissue electrics) are
  encapsulated into individual classes.
  All which derive from a common interface `Basic_physic`. That interface can then be
  used to trigger initialization, computation and output events without having to know
  the physics instance.
* Meshes, mappings, physics and state vectors which are needed for the interoperability
  of different physics are registered into global scope registries (basically a std::map).
  As such, different physics can interoperate without the need of an API.


## How to Suggest a Feature or Enhancement
If you find yourself wishing for a feature that doesn't exist in openCARP, you are probably not alone. There are bound to be others out there with similar needs. Many of the features that openCARP has today have been added because our users saw the need. Open an issue on our [issues list](https://git.opencarp.org/groups/openCARP/-/issues) which describes the feature you would like to see, why you need it, and how it should work. Don't forget to use the "Feature Request" label and any additional, specific [labels](https://git.opencarp.org/openCARP/openCARP/-/labels).
Feature requests are welcome. But take a moment to find out whether your idea fits with the scope and aims of the openCARP project. It's up to you to make a strong case to convince the project's developers of the merits of this feature. Understand that ressources of all people involved in the project are limited and that everyone is welcome to contribute.

## Attribution
The openCARP contrubution guide is adapted from [nayafia's](https://github.com/nayafia) CONTRIBUTING template, available at
<https://github.com/nayafia/contributing-template/blob/master/CONTRIBUTING-template.md>

