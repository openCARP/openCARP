include ../switches.def
include ../make.def


NUM_SRC = \
timers.cc \
basics.cc \
timer_utils.cc \
short_float.cc \
IGBheader.cc


ifdef ENABLE_PETSC
	NUM_SRC += \
petsc/SF_petsc_vector.cc \
petsc/SF_petsc_matrix.cc \
petsc/SF_petsc_solver.cc \
petsc/petsc_utils.cc
endif

ifdef ENABLE_GINKGO
	NUM_SRC += \
ginkgo_utils.cc \
ginkgo/SF_ginkgo_vector.cc \
gingko/SF_ginkgo_matrix.cc \
gingko/SF_ginkgo_solver.cc
endif

BUILD_DIR = build
$(shell mkdir -p $(BUILD_DIR) >/dev/null)

NUM_OBJ = $(addprefix $(BUILD_DIR)/, $(notdir $(NUM_SRC:%.cc=%.o)))
NUM_DEP = $(NUM_OBJ:%.o=%.d)
CHECKS  = $(NUM_SRC:%.cc=%_chk)

TARGET = libnumerics$(FLV).a

$(TARGET): $(NUM_OBJ)
	@echo " * creating archive: " $@
	@ar -r -c $@ $(NUM_OBJ)
	@ranlib $@

$(BUILD_DIR)/%.o: %.cc
	@echo " * NUMERICS: compiling" $<
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(BUILD_DIR)/%.o: ginkgo/%.cc
	@echo " * NUMERICS: compiling" $<
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(BUILD_DIR)/%.o: petsc/%.cc
	@echo " * NUMERICS: compiling" $<
	$(CXX) -c $(CXXFLAGS) $< -o $@

%_chk: %.cc
	$(CHECKER) -analyze $< -- $(CHECKER_FLAGS)

clean:
	rm -rf $(BUILD_DIR)
	rm -f $(TARGET)

check: $(CHECKS)
	find . -name \*.plist -exec rm \{\} \;

format:
	$(FORMATTER) $(FORMATTER_FLAGS) *.cc *.h

all: $(TARGET)

-include $(NUM_DEP)
