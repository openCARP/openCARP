#ifndef _SF_PETSC_MATRIX_H
#define _SF_PETSC_MATRIX_H

#include "SF_abstract_matrix.h"

#ifdef WITH_PETSC

#include <cassert>

#include <petscmat.h>

#include "SF_globals.h"
#include "SF_petsc_vector.h"
#include "petsc_compat.h"


namespace SF {

/**
 * @brief A class encapsulating a Ginkgo matrix.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_matrix base type and
 *       use the init_matrix() function from numerics/SF_init.h.
 *
 * @see abstract_matrix
 * @see numerics/SF_init.h
 */
class petsc_matrix : public abstract_matrix<SF_int,SF_real>
{
  public:
  /// the petsc Mat object
  mutable Mat data;

  /** Default constructor */
  petsc_matrix() : abstract_matrix<SF_int,SF_real>::abstract_matrix(), data(NULL)
  {}

  ~petsc_matrix() override
  {
    if(data) MatDestroy(&data);
  }

  inline void init(SF_int iNRows, SF_int iNCols, SF_int ilrows, SF_int ilcols,
                   SF_int loc_offset, SF_int mxent) override
  {
    // init member vars
    abstract_matrix<SF_int,SF_real>::init(iNRows, iNCols, ilrows, ilcols, loc_offset, mxent);

    MatCreateAIJ(PETSC_COMM_WORLD, ilrows, ilcols, this->NRows, this->NCols, mxent, COMPAT_PETSC_NULLPTR, mxent, COMPAT_PETSC_NULLPTR, &data);
    MatSetFromOptions(data);
    MatMPIAIJSetPreallocation(data, mxent, COMPAT_PETSC_NULLPTR, mxent, COMPAT_PETSC_NULLPTR);

    MatSetOption(data, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    this->zero();
  }

  inline void init(const meshdata<mesh_int_t, mesh_real_t> & imesh,
                   const SF_int irow_dpn,
                   const SF_int icol_dpn,
                   const SF_int max_edges)
  {
    abstract_matrix<SF_int,SF_real>::init(imesh, irow_dpn, icol_dpn, max_edges);
  }

  inline void zero() override
  {
    MatZeroEntries(data);
  }

  inline void mult(const abstract_vector<SF_int,SF_real> & x_, abstract_vector<SF_int,SF_real> & b_) const override
  {
    auto &x = dynamic_cast<const petsc_vector &>(x_);
    auto &b = dynamic_cast<petsc_vector &>(b_);
    MatMult(data, x.data, b.data);
  }

  inline void mult_LR(const abstract_vector<SF_int, SF_real>& L_, const abstract_vector<SF_int, SF_real>& R_) override
  {
    auto &L = dynamic_cast<const petsc_vector&>(L_);
    auto &R = dynamic_cast<const petsc_vector&>(R_);
    MatDiagonalScale(data, L.data, R.data);
  }

  inline void diag_add(const abstract_vector<SF_int, SF_real>& diag_) override
  {
    auto &diag = dynamic_cast<const petsc_vector&>(diag_);
    MatDiagonalSet(data, diag.data, ADD_VALUES);
  }

  inline void get_diagonal(abstract_vector<SF_int, SF_real>& vec_) const override
  {
    auto &vec = dynamic_cast<petsc_vector&>(vec_);
    MatGetDiagonal(data, vec.data);
  }

  inline void finish_assembly() override
  {
    MatAssemblyBegin(data, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(data, MAT_FINAL_ASSEMBLY);
  }

  inline void scale(SF_real s) override
  {
    MatScale(data, s);
  }

  inline void add_scaled_matrix(const abstract_matrix<SF_int,SF_real>& A_, const SF_real s,
                                const bool same_nnz) override
  {
    auto &A = dynamic_cast<const petsc_matrix&>(A_);

    MatStructure NNZ = same_nnz ? SAME_NONZERO_PATTERN : DIFFERENT_NONZERO_PATTERN;
    MatAXPY(data, s, A.data, NNZ);
  }

  inline void duplicate(const abstract_matrix<SF_int,SF_real>& M_) override
  {
    auto &M = dynamic_cast<const petsc_matrix&>(M_);

    this->NRows   = M.NRows;
    this->NCols   = M.NCols;
    this->row_dpn = M.row_dpn;
    this->col_dpn = M.col_dpn;
    this->lsize   = M.lsize;
    this->start   = M.start;
    this->stop    = M.stop;
    this->mesh    = M.mesh;

    if(data) MatDestroy(&data);
    MatDuplicate(M.data, MAT_COPY_VALUES, &data);
  }

  inline void set_values(const vector<SF_int> & row_idx, const vector<SF_int> & col_idx,
                         const vector<SF_real> & vals, bool add) override
  {
    assert(row_idx.size() == col_idx.size() && row_idx.size() == vals.size());

    // add values into system matrix
    MatSetValues(data, row_idx.size(), row_idx.data(), col_idx.size(), col_idx.data(),
                 vals.data(), add ? ADD_VALUES : INSERT_VALUES);
  }

  inline void set_values(const vector<SF_int> & row_idx, const vector<SF_int> & col_idx,
                         const SF_real* vals, bool add) override
  {
    assert(row_idx.size() == col_idx.size());

    // add values into system matrix
    MatSetValues(data, row_idx.size(), row_idx.data(), col_idx.size(), col_idx.data(),
                 vals, add ? ADD_VALUES : INSERT_VALUES);
  }

  inline void set_value(SF_int row_idx, SF_int col_idx, SF_real val, bool add) override
  {
    MatSetValue(data, row_idx, col_idx, val, add ? ADD_VALUES : INSERT_VALUES);
  }

  inline SF_real get_value(SF_int row_idx, SF_int col_idx) const override
  {
    SF_real val;
    MatGetValues(data, 1, &row_idx, 1, &col_idx, &val);
    return val;
  }

  inline void write(const char* filename) const override
  {
    PetscViewer viewer;
    PetscViewerBinaryOpen(this->mesh ? this->mesh->comm : PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &viewer);
    MatView(this->data, viewer);
    PetscViewerDestroy(&viewer);
  }
};


void init_matrix_petsc(abstract_matrix<SF_int,SF_real>** mat);


} // namespace SF

#endif // WITH_PETSC
#endif // _SF_PETSC_MATRIX_H
