#ifndef _SF_PETSC_VECTOR_H
#define _SF_PETSC_VECTOR_H

#include "SF_abstract_vector.h"

#ifdef WITH_PETSC

#include <petscvec.h>

#include "SF_globals.h"


namespace SF {


// A few helpers
inline void petsc_forward(Vec in, Vec out, scattering &sc, bool add = false)
{
  InsertMode imode = add ? ADD_VALUES : INSERT_VALUES;

  VecScatterBegin(sc.vec_sc, in, out, imode, SCATTER_FORWARD);
  VecScatterEnd  (sc.vec_sc, in, out, imode, SCATTER_FORWARD);
}

inline void petsc_backward(Vec in, Vec out, scattering &sc, bool add = false)
{
  InsertMode imode = add ? ADD_VALUES : INSERT_VALUES;

  VecScatterBegin(sc.vec_sc, in, out, imode, SCATTER_REVERSE);
  VecScatterEnd  (sc.vec_sc, in, out, imode, SCATTER_REVERSE);
}

/**
 * @brief A class encapsulating a PETSc vector.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_vector base type and
 *       use the init_vector() functions from numerics/SF_init.h.
 *
 * @see abstract_vector
 * @see numerics/SF_init.h
 */
class petsc_vector: public abstract_vector<SF_int,SF_real> {

  public:
  Vec                   data   = NULL;    ///< the PETSc vector pointer

  petsc_vector()
  {}

  ~petsc_vector() override
  {
    if(data) VecDestroy(&data);
  }

  petsc_vector(const meshdata<mesh_int_t,mesh_real_t> & imesh, int idpn, ltype inp_layout)
  {
    init(imesh, idpn, inp_layout);
  }

  petsc_vector(int igsize, int ilsize, int idpn, ltype ilayout) {
    init(igsize, ilsize, idpn, ilayout);
  }

  petsc_vector(const petsc_vector & vec) {
    init(vec);
  }

  inline void init(const meshdata<mesh_int_t,mesh_real_t> & imesh, int idpn, ltype inp_layout) override
  {
    int N = 0, n = 0;
    std::tie(N, n) = abstract_vector<SF_int,SF_real>::init_common(imesh, idpn, inp_layout);
    if(data) VecDestroy(&data);
    VecCreateMPI(PETSC_COMM_WORLD, n, N, &data);
  }

  inline void init(int igsize, int ilsize, int idpn = 1, ltype ilayout = abstract_vector<SF_int,SF_real>::unset) override {
    if(data) VecDestroy(&data);

    int ierr;
    if (ilsize == igsize && igsize >= 0) {
      ierr = VecCreateSeq(PETSC_COMM_SELF, igsize, &data);
    }
    else if (ilsize >= 0) {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, ilsize, abs(igsize), &data);
    }
    else {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, abs(igsize), &data);
    }
    // ierr = VecSetFromOptions (v);
    // ierr = VecSetBlockSize(v, bs);

    this->mesh = NULL;
    this->dpn = idpn;
    this->layout = ilayout;
    set(0.0);
  }

  inline void init(const abstract_vector<SF_int,SF_real> & vec_) override {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    if(data) VecDestroy(&data);

    VecDuplicate(vec.data, &data);

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
    set(0.0);
  }

  inline void set(const vector<SF_int>& idx, const vector<SF_real>& vals, const bool additive = false) override
  {
    InsertMode mode = additive ? ADD_VALUES : INSERT_VALUES;

    VecSetValues(data, idx.size(), idx.data(), vals.data(), mode);

    VecAssemblyBegin(data);
    VecAssemblyEnd(data);
  }

  inline void set(const vector<SF_int> & idx, const SF_real val) override
  {
    vector<PetscInt>  iidx(idx);
    vector<PetscReal> vals(idx.size(), val);

    set(idx, vals);
  }

  inline void set(const SF_real val) override
  {
    VecSet(data, val);
  }

  // TODO: This is very slow and should not be used
  inline void set(const SF_int idx, const SF_real val) override
  {
    VecSetValue(data, idx, val, INSERT_VALUES);

    VecAssemblyBegin(data);
    VecAssemblyEnd(data);
  }

  // TODO: This is very slow and should be removed.
  // According to docs, out is only written on the processes holding the respecitve indices.
  inline void get(const vector<SF_int> & idx, SF_real *out) override
  {
    vector<PetscInt>  iidx(idx);
    vector<PetscReal> iout(iidx.size());

    VecGetValues(data, iidx.size(), iidx.data(), iout.data());

    for(size_t i=0; i<iidx.size(); i++) out[i] = iout[i];
  }

  // TODO: We dont want to use this as this is slow..
  SF_real get(const SF_int idx) override
  {
    vector<PetscInt>  iidx(1, idx);
    vector<PetscReal> iout(iidx.size());

    VecGetValues(data, iidx.size(), iidx.data(), iout.data());

    return iout[0];
  }

  inline void operator *= (const SF_real sca) override
  {
    // VecScale(data, sca);
    PetscReal* w = this->ptr();

    for(PetscInt i=0; i<this->lsize(); i++) w[i] *= sca;
    this->release_ptr(w);
  }

  inline void operator /= (const SF_real sca) override
  {
    (*this) *= 1.0 / sca;
  }

  inline void operator *= (const abstract_vector<SF_int,SF_real> & vec_) override
  {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    SF_int mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr();
    const PetscReal* v = vec.const_ptr();

    for(SF_int i=0; i < mysize; i++) w[i] *= v[i];

    this->release_ptr(w);
    vec.const_release_ptr(v);
  }

  void add_scaled(const abstract_vector<SF_int,SF_real> & vec_, SF_real k) override
  {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    SF_int mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr();
    const PetscReal* r = vec.const_ptr();

    for (SF_int i=0; i<mysize; i++)
      w[i] += r[i] * k;

    this->release_ptr(w);
    vec.const_release_ptr(r);
  }

  inline void operator += (const abstract_vector<SF_int,SF_real> & vec_) override
  {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    this->add_scaled(vec, 1.0);
  }

  inline void operator -= (const abstract_vector<SF_int,SF_real> & vec_) override
  {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    this->add_scaled(vec, -1.0);
  }

  inline void operator += (SF_real c) override
  {
    PetscReal* w = this->ptr();

    for(SF_int i=0; i<this->lsize(); i++) w[i] += c;

    this->release_ptr(w);
  }

  inline void operator= (const vector<SF_real> & rhs) override
  {
    assert(size_t(this->lsize()) == rhs.size());

    PetscReal *w = this->ptr();
    const SF_real *r = rhs.data();

    for(size_t i=0; i<rhs.size(); i++)
      w[i] = r[i];

    this->release_ptr(w);
  }

  inline void operator=(const abstract_vector<SF_int,SF_real>& rhs) override
  {
    int  rhs_gsize = rhs.gsize();

    if (rhs_gsize) {
      int my_lsize = this->lsize(), rhs_lsize = rhs.lsize();
      assert(my_lsize == rhs_lsize);

            PetscReal *w = this->ptr();
      const PetscReal *r = rhs.const_ptr();

      for (SF_int i = 0; i < my_lsize; i++) w[i] = r[i];

      this->release_ptr(w);
      rhs.const_release_ptr(r);
    }
  }

  inline void shallow_copy(const abstract_vector<SF_int,SF_real>& v_) override
  {
    auto &v = dynamic_cast<const petsc_vector &>(v_);
    this->data   = v.data;
    this->mesh   = v.mesh;
    this->dpn    = v.dpn;
    this->layout = v.layout;
  }

  inline void deep_copy(const abstract_vector<SF_int,SF_real> & vec_) override
  {
    auto &vec = dynamic_cast<const petsc_vector &>(vec_);
    if(data) VecDestroy(&data);

    VecDuplicate(vec.data, &data);
    VecCopy(vec.data, data);

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
  }

  inline void overshadow(const abstract_vector<SF_int,SF_real> & sub_, bool member, int offset, int sz, bool share) override
  {
    auto &sub = dynamic_cast<const petsc_vector &>(sub_);
    // RVector super;
    int        loc_size = 0;
    const PetscReal* databuff = NULL;

    if(member) {
      SF_int start, stop;
      sub.get_ownership_range(start, stop);

      if(sz == -1)
        sz = sub.gsize();

      SF_int end    = offset + sz;
      SF_int lstart = stop - start;

      if(start >= offset)
        lstart = 0;
      else if(offset < end)
        lstart = offset - start;

      int lstop  = 0;
      if(end >= stop)
        lstop = stop - start;
      else if(end >= start)
        lstop = end-start;

      loc_size = lstop - lstart;

      if(loc_size < 0)
        loc_size = 0;

      databuff = sub.const_ptr() + lstart;
    }

    if(share)
      VecCreateMPIWithArray(PETSC_COMM_WORLD, 1, loc_size, PETSC_DECIDE, databuff, &data);
    else
      VecCreateMPI(PETSC_COMM_WORLD, loc_size, PETSC_DECIDE, &data);

    if(member)
      sub.const_release_ptr(databuff);
  }

  inline SF_int lsize() const override
  {
    PetscInt loc_size;
    VecGetLocalSize(data, &loc_size);
    return SF_int(loc_size);
  }

  inline SF_int gsize() const override
  {
    PetscInt glb_size;
    VecGetSize(data, &glb_size);
    return SF_int(glb_size);
  }

  inline void get_ownership_range(SF_int & start, SF_int & stop) const override
  {
    PetscInt iS, iE;
    VecGetOwnershipRange(data, &iS, &iE);
    start = iS, stop = iE;
  }

  inline PetscReal* ptr() override
  {
    PetscReal* p;
    // get pointer to local data
    VecGetArray(data, &p);
    return p;
  }

  inline const PetscReal* const_ptr() const override
  {
    const PetscReal* p;
    // get read pointer to local data
    VecGetArrayRead(data, &p);
    return p;
  }

  inline void release_ptr(PetscReal* & p) override
  {
    // release local data
    VecRestoreArray(data, &p);
  }

  inline void const_release_ptr(const PetscReal* & p) const override
  {
    // release local read data
    VecRestoreArrayRead(data, &p);
  }

  inline SF_real mag() const override
  {
    PetscReal ret;
    VecNorm(data, NORM_2, &ret);

    return SF_real(ret);
  }

  inline SF_real sum() const override
  {
    PetscReal ret;
    VecSum(data, &ret);
    return ret;
  }

  inline SF_real dot(const abstract_vector<SF_int,SF_real> & v_) const override
  {
    auto &v = dynamic_cast<const petsc_vector &>(v_);

    PetscReal ret;
    VecDot(this->data, v.data, &ret);
    return ret;
  }

  inline SF_real min() const override
  {
    PetscReal min;
    VecMin(this->data, NULL, &min);

    return min;
  }

  bool is_init() const override
  {
    bool ret = this->data != NULL;
    return ret;
  }

  inline std::string to_string() const override
  {
    std::string result = "petsc_vector (" + std::to_string(lsize()) + ") [";

    auto p = const_ptr();

    for (auto i = 0; i < lsize(); i++) {
      if (i > 0) result += ", ";
      result += std::to_string(p[i]);
    }
    result += "]";
    const_release_ptr(p);

    return result;
  }

  inline bool equals(const abstract_vector<SF_int,SF_real> & rhs_) const override
  {
    auto &rhs = dynamic_cast<const petsc_vector &>(rhs_);
    int mylsize = this->lsize();

    if (mylsize != rhs.lsize())
      return false;

    auto w = this->const_ptr();
    auto r = rhs.const_ptr();

    bool equal = true;
    for(int i = 0; i < mylsize; i++)
      if (fabs(w[i] - r[i]) > 0.0001)
        equal = false;

    this->const_release_ptr(w);
    rhs.const_release_ptr(r);

    return equal;
  }

  inline void finish_assembly() override
  {
    VecAssemblyBegin(this->data);
    VecAssemblyEnd  (this->data);
  }

  inline void forward(abstract_vector<SF_int,SF_real> & out_, scattering &sc, bool add = false) override {
    auto &out = dynamic_cast<petsc_vector &>(out_);
    petsc_forward(this->data, out.data, sc, add);
  }

  inline void backward(abstract_vector<SF_int,SF_real> & out_, scattering &sc, bool add = false) override {
    auto &out = dynamic_cast<petsc_vector &>(out_);
    petsc_backward(this->data, out.data, sc, add);
  }

  inline void apply_scattering(scattering &sc, bool fwd) override {
    Vec v = this->data;
    if(fwd) {
      // in the case of forward mapping, the values are permuted from v into b_buff.
      // We need to copy them out afterwards to guarantee consistency
      petsc_forward(v, sc.b_buff, sc);
      VecCopy(sc.b_buff, v);
    }
    else {
      // in the case of reverse mapping, the values are permuted from sc.b_buff to v
      // We need to copy the values of v to pVec beforehand.
      VecCopy(v, sc.b_buff);
      petsc_backward(sc.b_buff, v, sc);
    }
  }
};


void init_vector_petsc(abstract_vector<SF_int,SF_real>** vec);


} // namespace SF

#endif // WITH_PETSC
#endif // _SF_PETSC_VECTOR_H
