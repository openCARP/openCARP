#include "SF_petsc_matrix.h"
#include "basics.h"


namespace SF {

void init_matrix_petsc(abstract_matrix<SF_int,SF_real>** mat)
{
  *mat = new petsc_matrix();
}

} // namespace SF
