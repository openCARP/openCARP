// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file petsc_utils.h
* @brief Basic PETSc utilities.
* @author Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/


#ifndef _PETSC_UTILS_H
#define _PETSC_UTILS_H

#include "petsc_compat.h"

namespace opencarp {

#define DELIMIT_OPTS      "+"
#define PRINTF            PetscPrintf
#define PRINTF_SYNC       PetscSynchronizedPrintf
#define FPRINTF_SYNC      PetscSynchronizedFPrintf
#define FPRINTF           PetscFPrintf
#define PRINTF_FLUSH(A,B) COMPAT_PetscSynchronizedFlush(A,B)
#define SELF              PETSC_COMM_SELF,
#define WORLD             PETSC_COMM_WORLD,
#define COMM_W            PETSC_COMM_WORLD
#define COMM_S            PETSC_COMM_SELF
#define EXIT(A)           do {PetscFinalize(); exit(A);} while (0)

/** Initialize Petsc and MPI and determine the number of nodes and
    the rank of the node we are running on

   \note argc and argv are copied since it is not known what PETSc will do to them

   \pre Prm options are separated from PETSc options by the argument DELIMIT_OPTS

   \post Alter argc so that it includes all options up to but not including
         DELIMIT_OPTS
 */
void initialize_PETSc(int *argc, char **argv, char *rc, char *help_msg);

/** remove all options from Petsc database included in a given file
 *
 *  This is to keep options restricted to the one particular linear system
 *  for which we set the options previously using the same file,
 *  otherwise these options would have a global effect.
 *
 *  \param [in] comm     communicator
 *  \param [in] file     file holding petsc options
 *
 *  \post All petsc options in file are removed from database.
 *
 *  \note This should be avoided in future implementations. Rather, we
 *        should use the prepend tags to make options physics/problem specific.
 *
 */
PetscErrorCode PetscOptionsClearFromFile(MPI_Comm comm, const char* file);
PetscErrorCode PetscOptionsClearFromString(const char in_str[]);

const char* petsc_get_converged_reason_str(int reason);
}  // namespace opencarp

#endif
