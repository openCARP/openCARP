// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file petsc_compat.h
* @brief This file helps compile against multiple different versions of PETSc.
* @author Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/




#ifndef __PETSC_COMPAT_H__
#define __PETSC_COMPAT_H__
//// HEADER GUARD ///////////////////////////
// -*- c++ -*-
/** 
This file helps compile against multiple different versions of PETSc.
If you have recently upgraded PETSc and some of the function
signatures have changed, here is what I recommend you do:

\li Change CARP so that it compiles for the newer version of PETSc
\li Add COMPAT_ in front of the PETSc call.
\li Write a macro here that transforms the new code to what the older versions of PETSc expect.

This way, you will always feel like you are coding against the newest
version of PETSc, and you can use the documentation availiable on the
PETSc website.  Furthermore, if compatibility against an ancient
version of PETSc is no longer important, you can remove the COMPAT_
prefix and the code should still compile for all your installations of
PETSc.

*/

#include "petsc.h"
#include "petscversion.h"
#include "petscvec.h"
#include "petscksp.h"
#include "petscsys.h"
#define PETSC_VERSION (PETSC_VERSION_MAJOR*100000 + PETSC_VERSION_MINOR*1000 + PETSC_VERSION_SUBMINOR*100 + PETSC_VERSION_PATCH)


#define COMPAT_PCFactorSetFill(a,b) PCILUSetFill(a,b)
#define COMPAT_VecSetOption(_vec, VEC_IGNORE_NEGATIVE_INDICES, VALUE)
#define COMPAT_VecScatterBegin(a,b,c,d,e) VecScatterBegin(b,c,d,e,a)
#define COMPAT_VecScatterEnd(a,b,c,d,e)   VecScatterEnd  (b,c,d,e,a)
#define COMPAT_KSP_NORM_UNPRECONDITIONED KSP_UNPRECONDITIONED_NORM
#define COMPAT_KSP_NORM_PRECONDITIONED KSP_PRECONDITIONED_NORM
#define COMPAT_MatTranspose(mat, reuse, pmat) ((&(mat) == (pmat)) ? MatTranspose((mat), PETSC_NULL) : MatTranspose((mat), (pmat)))
#define COMPAT_VecDestroy(v) VecDestroy(v)
#define COMPAT_KSPDestroy(ksp) KSPDestroy(ksp)
#define COMPAT_VecScatterDestroy(v) VecScatterDestroy(v)
#define COMPAT_ISDestroy(is) ISDestroy(is)
#define COMPAT_PetscViewerDestroy(v) PetscViewerDestroy(v)
#define COMPAT_MatDestroy(m) MatDestroy(m)
#define COMPAT_PetscTruth PetscTruth
#define COMPAT_PETSC_TRUTH PETSC_TRUTH
#define COMPAT_MatZeroRows(mat,numRows,rows,diag,x,b) MatZeroRows(mat,numRows,rows,diag)
#define COMPAT_SETERRQ(comm,code,message) SETERRQ(code,message)
#define COMPAT_SETERRQ1(comm,code,message,arg) SETERRQ1(code,message,arg)
#define COMPAT_MatLoad(viewer,type,mat) MatLoad(viewer,type,&mat)
#define COMPAT_VecLoad(viewer,type,vec) VecLoad(viewer,type,&vec)
#define COMPAT_ISCreateGeneral(comm,n,idx,mode,is) ISCreateGeneral(comm,n,idx,is)
#define COMPAT_ISCreateGeneralWithArray(comm,n,idx,mode,is) ISCreateGeneralWithArray(comm,n,idx,is)
#define COMPAT_ISCreateGeneralNC(comm,n,idx,mode,is) ISCreateGeneralNC(comm,n,idx,is)
#define COMPAT_MatCreateAIJ MatCreateMPIAIJ
#define COMPAT_PetscTokenDestroy(A) PetscTokenDestroy(A)
#define COMPAT_MetisIndexType idxtype
#define COMPAT_MetisRealType float
#define COMPAT_MatNewNZAllocErr(A,B)
#define COMPAT_VecCreateMPIWithArray(C,B,n,N,a,V) VecCreateMPIWithArray(C,n,N,a,V)
#define COMPAT_PetscGetTime PetscGetTime
#define COMPAT_PetscMPIIntCast(a,b) (b=PetscMPIIntCast(a))
#define COMPAT_MatMatMult(A,B,scall,fill,C) assert(0)
#define COMPAT_MatMatMatMult(A,B,C,scall,fill,D) assert(0)
#define COMPAT_MatMumpsSetCntl(A,icntl,val) assert(0)
#define COMPAT_MatMumpsSetIcntl(A,icntl,val) assert(0)
#define COMPAT_KSPCreate(comm,inksp) KSPCreate(comm,inksp)
#define COMPAT_KSPSetType(ksp,type) KSPSetType(ksp,type)
#define COMPAT_KSPSetOperators(ksp,A,B,mode) KSPSetOperators(ksp,A,B,mode)
#define COMPAT_KSPSetTolerances(ksp, rtol, abstol, dtol, maxits) KSPSetTolerances(ksp, rtol, abstol, dtol, maxits)
#define COMPAT_KSPSetReusePreconditioner(ksp,flag)
#define COMPAT_KSPGetPC(ksp,pc) KSPGetPC(ksp,pc)
#define COMPAT_PCFieldSplitSetSchurPre(pc,ptype,pre) PCFieldSplitSchurPrecondition(pc,ptype,pre)
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELFP -1
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELF  -1
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_A11   -1
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_USER  -1
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_FULL  -1
#define COMPAT_PetscSynchronizedFlush(A,B) PetscSynchronizedFlush(A)
#define COMPAT_SNESCreate(comm,snes) SNESCreate (comm,snes)
#define COMPAT_SNESSetFunction(snes,r,f,ctx) SNESSetFunction (snes,r,f,ctx)
#define COMPAT_SNESSetJacobian(snes,Amat,Pmat,J,ctx) SNESSetJacobian(snes,Amat,Pmat,J,ctx)
#define COMPAT_SNESSetFromOptions(snes) SNESSetFromOptions(snes)
#define COMPAT_SNESSolve(snes,b,x) SNESSolve(snes,b,x)
#define COMPAT_SNESGetIterationNumber(snes,iter) SNESGetIterationNumber(snes,iter)
#define COMPAT_PetscStackCallHDF5Return(ret,func,args)
#define COMPAT_PetscStackCallHDF5(func,args)
#define COMPAT_PetscOptionsInsertFile(a,b,c) PetscOptionsInsertFile(a,b,c)
#define COMPAT_PetscOptionsGetInt(a,b,c,d) PetscOptionsGetInt(a,b,c,d)
#define COMPAT_PetscOptionsClearValue(a) PetscOptionsClearValue(a)
#define COMPAT_PetscOptionsSetAlias(a,b) PetscOptionsSetAlias(a,b)
#define COMPAT_PetscOptionsInsertString(a) PetscOptionsInsertString(a)
#define COMPAT_PETSC_NULLPTR PETSC_NULL

#if PETSC_VERSION >= 203200
#undef COMPAT_PCFactorSetFill
#define COMPAT_PCFactorSetFill(a,b) PCFactorSetFill(a,b)
#endif

#if PETSC_VERSION >= 203300
#undef  COMPAT_VecScatterBegin
#define COMPAT_VecScatterBegin(a,b,c,d,e) VecScatterBegin(a, b, c, d, e)
#undef  COMPAT_VecScatterEnd
#define COMPAT_VecScatterEnd(a,b,c,d,e)   VecScatterEnd  (a, b, c, d, e)
#undef  COMPAT_VecSetOption
#define COMPAT_VecSetOption(_vec, VEC_IGNORE_NEGATIVE_INDICES, VALUE) VecSetOption(_vec, VEC_IGNORE_NEGATIVE_INDICES)
#undef  COMPAT_KSP_NORM_UNPRECONDITIONED
#define COMPAT_KSP_NORM_UNPRECONDITIONED KSP_NORM_UNPRECONDITIONED
#undef  COMPAT_KSP_NORM_PRECONDITIONED
#define COMPAT_KSP_NORM_PRECONDITIONED KSP_NORM_PRECONDITIONED

#endif

#if PETSC_VERSION >= 300000
#undef  COMPAT_VecSetOption
#define COMPAT_VecSetOption(_vec, VEC_IGNORE_NEGATIVE_INDICES, VALUE) VecSetOption(_vec, VEC_IGNORE_NEGATIVE_INDICES, VALUE)
#undef  COMPAT_MatTranspose
#define COMPAT_MatTranspose(mat, reuse, pmat) MatTranspose(mat, reuse, pmat)
#undef  COMPAT_MAT_SET_OPT_KEEP_ZEROES
#define COMPAT_MAT_SET_OPT_KEEP_ZEROES(mat,flg) MatSetOption(mat,MAT_KEEP_ZEROED_ROWS,flg)
#endif

#if PETSC_VERSION >= 301000
#undef  COMPAT_MAT_SET_OPT_KEEP_ZEROES
#define COMPAT_MAT_SET_OPT_KEEP_ZEROES(mat,flg) MatSetOption(mat,MAT_KEEP_NONZERO_PATTERN,flg)
#endif

#if PETSC_VERSION >= 302000
#undef  COMPAT_VecDestroy
#define COMPAT_VecDestroy(v) VecDestroy(&v)
#undef  COMPAT_KSPDestroy
#define COMPAT_KSPDestroy(ksp) KSPDestroy(&ksp)
#undef  COMPAT_VecScatterDestroy
#define COMPAT_VecScatterDestroy(v) VecScatterDestroy(&v)
#undef  COMPAT_ISDestroy
#define COMPAT_ISDestroy(is) ISDestroy(&is)
#undef  COMPAT_PetscViewerDestroy
#define COMPAT_PetscViewerDestroy(v) PetscViewerDestroy(&v)
#undef  COMPAT_MatDestroy
#define COMPAT_MatDestroy(m) MatDestroy(&m)
#undef  COMPAT_PetscTruth
#define COMPAT_PetscTruth PetscBool
#undef  COMPAT_PETSC_TRUTH
#define COMPAT_PETSC_TRUTH PETSC_BOOL
#undef  COMPAT_MatZeroRows
#define COMPAT_MatZeroRows(mat,numRows,rows,diag,x,b) MatZeroRows(mat,numRows,rows,diag,x,b)
#undef  COMPAT_SETERRQ
#define COMPAT_SETERRQ(comm,code,message) SETERRQ(comm,code,message)
#undef  COMPAT_SETERRQ1
#define COMPAT_SETERRQ1(comm,code,message,arg) SETERRQ1(comm,code,message,arg)
#undef  COMPAT_MatLoad
#define COMPAT_MatLoad(viewer,type,mat) MatLoad(mat,viewer)
#undef  COMPAT_VecLoad
#define COMPAT_VecLoad(viewer,type,vec) VecLoad(vec,viewer)
#undef  COMPAT_ISCreateGeneral
#define COMPAT_ISCreateGeneral(comm,n,idx,mode,is) ISCreateGeneral(comm,n,idx,mode,is)
#undef  COMPAT_ISCreateGeneralWithArray
#define COMPAT_ISCreateGeneralWithArray(comm,n,idx,mode,is) ISCreateGeneral(comm,n,idx,mode,is)
#undef  COMPAT_ISCreateGeneralNC
#define COMPAT_ISCreateGeneralNC(comm,n,idx,mode,is) ISCreateGeneral(comm,n,idx,mode,is)
#endif

#if PETSC_VERSION >= 303000
#undef  COMPAT_MatCreateAIJ
#define COMPAT_MatCreateAIJ MatCreateAIJ
#undef  COMPAT_MatNewNZAllocErr
#define COMPAT_MatNewNZAllocErr(A,B) MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,B)
#undef  COMPAT_PetscTokenDestroy
#define COMPAT_PetscTokenDestroy(A) PetscTokenDestroy(&A)
#undef  COMPAT_MetisIndexType
#define COMPAT_MetisIndexType idx_t
#undef  COMPAT_MetisRealType
#define COMPAT_MetisRealType real_t
#undef  COMPAT_VecCreateMPIWithArray
#define COMPAT_VecCreateMPIWithArray VecCreateMPIWithArray
#endif

#if PETSC_VERSION >= 304000
#undef  COMPAT_PetscGetTime
#define COMPAT_PetscGetTime PetscTime
#undef  COMPAT_PetscMPIIntCast
#define COMPAT_PetscMPIIntCast(a,b) PetscMPIIntCast(a,&b)
#undef  COMPAT_MatMatMult
#define COMPAT_MatMatMult(A,B,scall,fill,C) MatMatMult(A,B,scall,fill,C)
#undef  COMPAT_MatMatMatMult
#define COMPAT_MatMatMatMult(A,B,C,scall,fill,D) MatMatMatMult(A,B,C,scall,fill,D)
#undef  COMPAT_MatMumpsSetCntl
#define COMPAT_MatMumpsSetCntl(A,icntl,val) MatMumpsSetCntl(A,icntl,val)
#undef  COMPAT_MatMumpsSetIcntl
#define COMPAT_MatMumpsSetIcntl(A,icntl,val) MatMumpsSetIcntl(A,icntl,val)
#endif

#if PETSC_VERSION >= 305000
#undef  COMPAT_KSPSetReusePreconditioner
#define COMPAT_KSPSetReusePreconditioner(ksp,flag) KSPSetReusePreconditioner (ksp,flag)
#undef  COMPAT_KSPSetOperators
#define COMPAT_KSPSetOperators(ksp,A,B,mode) KSPSetOperators(ksp,A,B)
#undef  COMPAT_PetscSynchronizedFlush
#define COMPAT_PetscSynchronizedFlush(A,B) PetscSynchronizedFlush(A,B)
#undef  COMPAT_PCFieldSplitSetSchurPre
#define COMPAT_PCFieldSplitSetSchurPre(pc,ptype,pre) PCFieldSplitSetSchurPre(pc,ptype,pre)
#undef  COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELFP
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELFP PC_FIELDSPLIT_SCHUR_PRE_SELFP
#undef  COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELF
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_SELF PC_FIELDSPLIT_SCHUR_PRE_SELF
#undef  COMPAT_PC_FIELDSPLIT_SCHUR_PRE_A11
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_A11 PC_FIELDSPLIT_SCHUR_PRE_A11
#undef  COMPAT_PC_FIELDSPLIT_SCHUR_PRE_USER
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_USER PC_FIELDSPLIT_SCHUR_PRE_USER
#undef  COMPAT_PC_FIELDSPLIT_SCHUR_PRE_FULL
#define COMPAT_PC_FIELDSPLIT_SCHUR_PRE_FULL PC_FIELDSPLIT_SCHUR_PRE_FULL
#undef  COMPAT_PetscLogPrintSummary
#define COMPAT_PetscLogPrintSummary PetscLogPrintSummary
#endif

#if PETSC_VERSION >= 306000
#undef COMPAT_PetscStackCallHDF5Return
#define COMPAT_PetscStackCallHDF5Return(ret,func,args) PetscStackCallHDF5Return(ret,func,args)
#undef COMPAT_PetscStackCallHDF5
#define COMPAT_PetscStackCallHDF5(func,args) PetscStackCallHDF5(func,args)
#undef COMPAT_MatGetDiagonal_Nest
#define COMPAT_MatGetDiagonal_Nest(mat, vec) MatGetDiagonal_Nest(mat, vec)
#endif

#if PETSC_VERSION >= 307000
#undef COMPAT_PetscOptionsInsertFile
#define COMPAT_PetscOptionsInsertFile(a,b,c) PetscOptionsInsertFile(a,NULL,b,c)
#undef COMPAT_PetscOptionsGetInt
#define COMPAT_PetscOptionsGetInt(a,b,c,d) PetscOptionsGetInt(NULL,a,b,c,d)
#undef COMPAT_PetscOptionsClearValue
#define COMPAT_PetscOptionsClearValue(a) PetscOptionsClearValue(NULL,a)
#undef COMPAT_PetscOptionsSetAlias
#define COMPAT_PetscOptionsSetAlias(a,b) PetscOptionsSetAlias(NULL, a, b)
#undef COMPAT_PetscOptionsInsertString
#define COMPAT_PetscOptionsInsertString(a) PetscOptionsInsertString(NULL,a)
#endif

#if PETSC_VERSION >= 317000
#undef  COMPAT_SETERRQ1
#define COMPAT_SETERRQ1(comm,code,message,arg) SETERRQ(comm,code,message,arg)
#undef COMPAT_PETSC_NULLPTR 
#define COMPAT_PETSC_NULLPTR PETSC_NULLPTR
#endif

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
