#include "SF_petsc_vector.h"
#include "basics.h"

namespace SF {

void init_vector_petsc(abstract_vector<SF_int,SF_real>** vec)
{
  *vec = new petsc_vector();
}

} // namespace SF
