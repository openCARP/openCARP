#ifndef _SF_INIT_H_
#define _SF_INIT_H_

#include "SF_abstract_vector.h"
#include "SF_abstract_matrix.h"
#include "SF_abstract_lin_solver.h"

#include "SF_ginkgo_vector.h"
#include "SF_ginkgo_matrix.h"
#include "SF_ginkgo_solver.h"

#include "SF_petsc_vector.h"
#include "SF_petsc_matrix.h"
#include "SF_petsc_solver.h"

namespace SF {
namespace detail {

  /** Simple helper function which initializes a Ginkgo vector or throws */
  template <typename T, typename S>
  inline void init_or_throw_ginkgo(SF::abstract_vector<T, S>** object)
  {
#ifndef WITH_GINKGO
    throw std::runtime_error("The Ginkgo backend was not enabled");
#else
    init_vector_ginkgo(object);
#endif
  }

  /** Simple helper function which initializes a Ginkgo matrix or throws */
  template <typename T, typename S>
  inline void init_or_throw_ginkgo(SF::abstract_matrix<T, S>** object)
  {
#ifndef WITH_GINKGO
    throw std::runtime_error("The Ginkgo backend was not enabled");
#else
    init_matrix_ginkgo(object);
#endif
  }

  /** Simple helper function which initializes a Ginkgo solver or throws */
  template <typename T, typename S>
  inline void init_or_throw_ginkgo(SF::abstract_linear_solver<T, S>** object)
  {
#ifndef WITH_GINKGO
    throw std::runtime_error("The Ginkgo backend was not enabled");
#else
    init_solver_ginkgo(object);
#endif
  }

  /** Simple helper function which initializes a PETSc vector or throws */
  inline void init_or_throw_petsc(SF::abstract_vector<SF_int,SF_real>** object)
  {
#ifndef WITH_PETSC
    throw std::runtime_error("The PETSc backend was not enabled");
#else
    init_vector_petsc(object);
#endif
  }

  /** Simple helper function which initializes a PETSc matrix or throws */
  template <typename T, typename S>
  inline void init_or_throw_petsc(SF::abstract_matrix<T, S>** object)
  {
#ifndef WITH_PETSC
    throw std::runtime_error("The PETSc backend was not enabled");
#else
    init_matrix_petsc(object);
#endif
  }

  /** Simple helper function which initializes a PETSc solver or throws */
  template <typename T, typename S>
  inline void init_or_throw_petsc(SF::abstract_linear_solver<T, S>** object)
  {
#ifndef WITH_PETSC
    throw std::runtime_error("The PETSc backend was not enabled");
#else
    init_solver_petsc(object);
#endif
  }

}  // namespace detail


/**
 * Initialize a default SF::abstract_vector based on the param_globals::flavor
 * setting.
 *
 * @param vec   The vector to initialize.j
 *
 * @tparam T    Integer type (indices).
 * @tparam S    Floating point type (values).
 * 
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_vector(SF::abstract_vector<T, S>** vec)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(vec);
  } else {
    detail::init_or_throw_petsc(vec);
  }
}

/**
 * Initialize an SF::abstract_vector with specified sizes based on the
 * param_globals::flavor setting.
 *
 * @param vec     The vector to initialize
 * @param igsize  Global size
 * @param ilsize  Local size
 * @param idpn    Number of d.o.f. used per mesh node
 * @param ilayout Vector layout w.r.t. used mesh.
 *
 * @tparam T      Integer type (indices).
 * @tparam S      Floating point type (values).
 *
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_vector(SF::abstract_vector<T, S>** vec, int igsize,
                        int ilsize, int idpn = 1,
                        typename abstract_vector<T, S>::ltype ilayout = abstract_vector<T, S>::unset)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(vec);
  } else {
    detail::init_or_throw_petsc(vec);
  }
  (*vec)->init(igsize, ilsize, idpn, ilayout);
}

/**
 * Initialize an SF::abstract_vector from a mesh based on the
 * param_globals::flavor setting.
 *
 * @param vec     The vector to initialize
 * @param mesh    The mesh defining the parallel layout of the vector.
 * @param i       The number of d.o.f. per mesh vertex.
 * @param ltype   The vector layout.
 *
 * @tparam T      Integer type (indices).
 * @tparam S      Floating point type (values).
 *
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_vector(SF::abstract_vector<T, S>** vec,
                        const SF::meshdata<mesh_int_t, mesh_real_t>& mesh,
                        int i, typename SF::abstract_vector<T, S>::ltype ltype)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(vec);
  } else {
    detail::init_or_throw_petsc(vec);
  }
  (*vec)->init(mesh, i, ltype);
}

/**
 * Initialize an SF::abstract_vector from another vector based on the
 * param_globals::flavor setting.
 *
 * @param vec    The vector to initialize
 * @param in     The vector to initialize from
 *
 * @tparam T     Integer type (indices).
 * @tparam S     Floating point type (values).
 *
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_vector(SF::abstract_vector<T, S>** vec,
                        SF::abstract_vector<T, S>* in)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(vec);
  } else {
    detail::init_or_throw_petsc(vec);
  }
  (*vec)->init(*in);
}

/**
 * Initialize a default SF::abstract_matrix based on the param_globals::flavor
 * setting.
 *
 * @param mat  The matrix to initialize
 *
 * @tparam T   Integer type (indices).
 * @tparam S   Floating point type (values).
 *
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_matrix(SF::abstract_matrix<T, S>** mat)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(mat);
  } else {
    detail::init_or_throw_petsc(mat);
  }
}

/**
 * Initialize a default SF::abstract_linear_solver based on the
 * param_globals::flavor setting.
 *
 * @param sol         The solver to initialize
 *
 * @tparam T  Integer type (indices).
 * @tparam S  Floating point type (values).
 *
 * @throws std::error   if the user requests an uncompiled flavor
 */
template <class T, class S>
inline void init_solver(SF::abstract_linear_solver<T, S>** sol)
{
  if ((param_globals::flavor != nullptr) && (std::string{param_globals::flavor} == "ginkgo")) {
    detail::init_or_throw_ginkgo(sol);
  } else {
    detail::init_or_throw_petsc(sol);
  }
}


}  // namespace SF

#endif  // _SF_INIT_H_
