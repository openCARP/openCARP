// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file timer_utils.h
* @brief Timers and timer manager.
* @author Gernot Plank, Edward Vigmond, Aurel Neic
* @version 
* @date 2019-10-25
*/

#ifndef TIMER_H
#define TIMER_H

#include <cstdio>
#include <cstdlib>
#include <cstdbool>
#include <cmath>

#include <string>
#include <vector>


namespace opencarp {

/**
* @brief The timers we always need are indexed in this enum
*/
enum io_time {iotm_console = 0, iotm_state_var, iotm_spacedt, iotm_chkpt_list, iotm_chkpt_intv, iotm_trace, iotm_num_timers};

/**
* @brief We distinguish between equidistant and non-equidistant timers with an enum
*/
enum t_timer {EQUDIST, NONEQUDIST};

struct base_timer {
  t_timer       type;          //!< type of timer, continuous equidistant, or non-equidistant
  const char*   name;          //!< timer name
  bool          triggered;     //!< flag indicating trigger at current time step
  int           trigger_count; //!< count number of triggered IO events
  int           numIOs;        //!< total number of triggers during simulation
  double        trigger_dur;   //!< triggered event duration (e.g.,stim duration)
  long          d_trigger_dur; //!< discrete duration
  int           active;        //!< count down to end of a trigger with duration
  std::string   pool;          //!< label of pool (IO || TS || STIM || ...) to which timer belongs

  virtual void update() = 0;
  virtual void reset() = 0;

  inline void assign_pool(const char* poolname)
  {
    pool = poolname;
  }
};


/// centralize time managment and output triggering
class timer_manager {
public:

  double time;       //!< current time
  long   d_time;     //!< current time instance index
  double time_step;  //!< global reference time step
  double start;      //!< initial time (nonzero when restarting)
  long   d_start;    //!< initial index in multiples of dt
  double end;        //!< final time
  long   d_end;      //!< final index in multiples of dt

  std::vector<base_timer*> timers; //!< vector containing individual timers

  /** Main constructor
   *
   * See setup() for parameter description.
   *
   */
  timer_manager(double inp_dt, double inp_start, double inp_end)
  {
    setup(inp_dt, inp_start, inp_end);
    timers.assign(iotm_num_timers, nullptr);
  }

  /** Update all timers and IO triggers
   *
   *  \note triggered within this function means the leading edge, while
   *        the triggered field outside the function means that the
   *        trigger is on
   */
  inline void update_timers()
  {
    d_time++;
    time = d_time * time_step;

    for (base_timer* t : timers)
      if(t != nullptr) t->update();
  }

  /**
  * @brief Reset time in timer_manager and then reset registered timers
  */
  inline void reset_timers() {
    time = start;
    d_time = d_start;

    for (base_timer* t : timers)
      if(t != nullptr) t->reset();
  }

  /**
  * @brief Add a equidistant step timer to the array of timers
  *
  * @param istart      Timing interval start.
  * @param iend        Timing interval end.
  * @param ntrig       Number of triggered events in the timer.
  * @param iintv       Interval between trigger events.
  * @param idur        Duration of a trigger event.
  * @param iname       Timer name.
  * @param poolname    Optional timer pool name.
  *
  * @return 
  */
  int add_eq_timer(double istart, double iend, int ntrig, double iintv, double idur,
                   const char *iname, const char *poolname = nullptr);

  int add_neq_timer(const std::vector<double> & itrig, double idur, const char *iname,
                    const char *poolname = nullptr);

  inline int
  add_singlestep_timer(double tg, double idur, const char *iname,
                       const char *poolname = nullptr)
  {
    return add_eq_timer(tg, tg, 0, 0, idur, iname, poolname);
  }

  void initialize_eq_timer(double istart, double iend, int ntrig, double iintv,
                           double idur, int ID, const char *iname, const char* poolname = nullptr);

  void initialize_neq_timer(const std::vector<double> & itrig, double idur, int ID,
                            const char *iname, const char* poolname = nullptr);

  inline void
  initialize_singlestep_timer(double tg, double idur, int ID, const char *iname,
                              const char *poolname = nullptr)
  {
    initialize_eq_timer(tg, tg, 0, 0, idur, ID, iname, poolname);
  }

  inline size_t num_timers()           const {return timers.size();}
  inline double timer_duration(int ID) const {return ID > -1 ? timers[ID]->trigger_dur : 0.0;}
  inline bool   elapsed()              const {return d_time > d_end;}

  inline bool trigger(int ID) const
  {
    return ID > -1 && timers[ID] != nullptr && timers[ID]->triggered;
  }

  inline bool triggered_now(int ID) const
  {
    return ID > -1 &&
           timers[ID] != nullptr &&
           ((timers[ID]->d_trigger_dur) - (timers[ID]->active)) == 1;
  }

  inline bool trigger_end(int ID) const
  {
    return ID > -1 &&
           timers[ID] != nullptr &&
           timers[ID]->triggered &&
           !timers[ID]->active;
  }

  inline int  trigger_elapse(int ID) const
  {
    if(ID > -1 && timers[ID] != nullptr)
      return timers[ID]->d_trigger_dur - timers[ID]->active - 1;
    else
      return 0;
  }

  /// return number of ticks elapsed after trigger
  inline int active_ticks(int ID) const
  {
    if(ID > -1 && timers[ID] != nullptr)
      return timers[ID]->triggered ? timers[ID]->d_trigger_dur - timers[ID]->active : 0;
    else
      return 0;
  }

  /**
  * @brief Initialize the timer_manager.
  *
  * The initialization code that is called in the constructor and for reinitialization
  *
  * @param inp_dt     Basic time step.
  * @param inp_start  Simulation time start.
  * @param inp_end    Simulation time end.
  */
  void setup(double inp_dt, double inp_start, double inp_end);
};


struct timer_eq : base_timer
{
  const timer_manager* mng;

  double        start;      //!< Time when we start I/O in mode _EQUDIST
  long          d_start;    //!< discrete start in multiples of dt
  double        end;        //!< Time when we stop I/O in mode _EQUDIST
  long          d_end;      //!< discrete stop index in multiples of dt
  double        intv;       //!< io interval in ms
  long          d_intv;     //!< discrete io interval in multiples of dt
  double        nxt;        //!< next output time in ms
  int           d_nxt;      //!< next output index

  void initialize(const timer_manager* t, double istart, double iend, int ntrig, double iintv,
                  double idur, const char *iname);
  void update();
  void reset();
};

struct timer_neq : base_timer
{
  const timer_manager* mng;

  std::vector<double> trig;    //!< the times that the timer will trigger
  std::vector<long>   d_trig;  //!< the discrete times associated to trig

  void initialize(const timer_manager* t, const std::vector<double> & itrig,
                  const double idur, const char *iname);
  void update();
  void reset();
};

}  // namespace opencarp

#endif

