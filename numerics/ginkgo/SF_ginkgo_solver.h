// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GINKGO_LIN_SOLVER_H
#define _SF_GINKGO_LIN_SOLVER_H

#include <chrono>
#include "SF_abstract_lin_solver.h"

#ifdef WITH_GINKGO
#include "basics.h"

#include <ginkgo/ginkgo.hpp>
#include "ginkgo_utils.h"

#include "SF_globals.h"

#include "SF_ginkgo_vector.h"
#include "SF_ginkgo_matrix.h"


namespace SF {

/**
 * @brief A class encapsulating a Ginkgo solver.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_linera_solver base
 *       type and use the init_solver() function from numerics/SF_init.h.
 *
 * @see abstract_linear_solver
 * @see numerics/SF_init.h
 */
template <class T, class S>
struct ginkgo_solver : abstract_linear_solver<T, S> {
  using gko_vec = gko::experimental::distributed::Vector<S>;
  using local_vec = gko::matrix::Dense<S>;
  using norm_t = typename abstract_linear_solver<T,S>::norm_t;

  ginkgo_matrix<T, S>* matrix       = NULL;

  std::shared_ptr<gko::LinOp>                     solver           = NULL;
  std::shared_ptr<const gko::log::Convergence<S>> logger           = NULL;
  std::shared_ptr<gko::stop::Combined::Factory>   stop             = NULL;
  std::shared_ptr<gko::LinOp>                     precond          = NULL;

  bool                                            has_nullspace    = false;
  bool                                            check_start_norm = false;

  void operator()(abstract_vector<T, S>& x_, const abstract_vector<T, S>& b_) override
  {
    auto &x = dynamic_cast<ginkgo_vector<T, S>&>(x_);
    auto &b = dynamic_cast<const ginkgo_vector<T, S>&>(b_);
    assert(x.data != b.data);
    assert(solver != NULL);

    const double solve_zero = 1e-16;
    const double NORMB = check_start_norm ? b.mag() : 1.0;

    if (NORMB > solve_zero) {
      solver->apply(b.data.get(), x.data.get());

      // In case there is a constant nullspace, we have to remove its
      // contribution to the solution. For now this is not an option in ginkgo,
      // so we do it after the linear solve.
      if (has_nullspace) {
        auto exec   = get_global_exec();
        auto nullsp = gko_vec::create_with_config_of(b.data.get());
        nullsp->fill(1.);
        auto scal = local_vec::create(exec, gko::dim<2>{1, b.data->get_size()[1]});
        auto n_op = gko::initialize<local_vec>({-(double)b.data->get_size()[0]}, exec);
        x.data->compute_dot(nullsp.get(), scal.get());
        scal->inv_scale(n_op.get());
        x.data->add_scaled(scal.get(), nullsp.get());
      }
    } else {
      abstract_linear_solver<T, S>::niter          = 0;
      abstract_linear_solver<T, S>::final_residual = NORMB;
    }
  }

  void setup_solver(abstract_matrix<T, S>& mat, double tol, int max_it, short norm,
                    std::string name, bool has_nullspace, void* logger,
                    const char* solver_opts_file, const char* default_opts) override;

protected:
  void set_stopping_criterion(norm_t normtype, double tol, int max_it, bool verbose, void* logger) override;
};

template <class T, class S>
void init_solver_ginkgo(abstract_linear_solver<T, S>** sol);


}  // namespace SF

#endif  // WITH_GINKGO
#endif  // _SF_GINKGO_LIN_SOLVER_H
