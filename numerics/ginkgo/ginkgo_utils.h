#ifndef OPENCARP_GINKGO_UTILS_H_
#define OPENCARP_GINKGO_UTILS_H_

#ifdef WITH_GINKGO
#include <ginkgo/ginkgo.hpp>

#include "SF_abstract_vector.h"

std::shared_ptr<const gko::Executor>& get_global_exec();
gko::experimental::mpi::communicator get_global_comm();

std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> get_global_partition(int local_rows);

std::shared_ptr<gko::experimental::distributed::Matrix<double, int, int>> get_scattering_matrix(SF::scattering& sc, int in_size, int out_size, std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> in_part, std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> out_part, bool forward);

#endif // WITH_GINKGO

#endif // OPENCARP_GINKGO_UTILS_H_
