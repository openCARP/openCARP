// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GINKGO_VECTOR_H
#define _SF_GINKGO_VECTOR_H

#include "SF_abstract_vector.h"

#ifdef WITH_GINKGO

#include <memory>

#include <ginkgo/ginkgo.hpp>
#include "ginkgo_utils.h"

#include "SF_globals.h"


namespace SF {


/**
* @brief A class encapsulating a Ginkgo vector.
*
* @note Objects of this class should not be used or initialized directly.
*       Instead, use virtual functions on the abstract_vector base type and
*       use the init_vector() functions from numerics/SF_init.h.
*
* @see abstract_vector
* @see numerics/SF_init.h
*/
template<class T, class S>
class ginkgo_vector: public abstract_vector<T,S>
{
  using typename abstract_vector<T,S>::ltype;

  public:

  using gko_vec = gko::experimental::distributed::Vector<S>;
  using local_vec = gko::matrix::Dense<S>;
  std::shared_ptr<gko_vec> data   = NULL;    ///< the Ginkgo vector pointer
  std::shared_ptr<gko_vec> buffer   = NULL;  ///< a buffer for scatterings
  mutable std::shared_ptr<gko_vec> host_data = NULL; ///< a pointer guaranteed to be on CPU
  T global_start;
  T global_stop;
  mutable std::shared_ptr<const gko::experimental::distributed::Partition<T, T>> partition;
  bool asked_for_range = false;

  ginkgo_vector()
  {}

  ~ginkgo_vector() override
  {}

  ginkgo_vector(const meshdata<mesh_int_t,mesh_real_t> & imesh,
                int idpn, ltype inp_layout) {
    init(imesh, idpn, inp_layout);
  }

  ginkgo_vector(int igsize, int ilsize, int idpn, ltype ilayout) {
    init(igsize, ilsize, idpn, ilayout);
  }

  ginkgo_vector(const ginkgo_vector<T,S> & vec) {
    init(vec);
  }

  void build_partition() {
    const auto comm = get_global_comm();
    const auto exec = get_global_exec();
    auto ls = this->lsize();
    std::vector<T> sizes(comm.size());
    comm.all_gather(exec, &ls, 1, sizes.data(), 1);
    auto rank = comm.rank();

    gko::array<T> ranges_array{
      exec->get_master(), static_cast<gko::size_type>(comm.size() + 1)};
    ranges_array.get_data()[0] = 0;
    for (int i = 0; i < comm.size(); i++) {
        ranges_array.get_data()[i + 1] = ranges_array.get_data()[i] + sizes[i];
    }

    global_start = ranges_array.get_data()[rank];
    global_stop = ranges_array.get_data()[rank + 1];
    ranges_array.set_executor(exec);
    partition = gko::share(gko::experimental::distributed::Partition<T, T>::build_from_contiguous(exec, ranges_array));
  }

  /**
   * Default intiialization to empty vectors
   */
  inline void init()
  {
    auto exec = get_global_exec();
    auto comm = get_global_comm();
    data = gko_vec::create(exec, comm);
    host_data = gko_vec::create(exec->get_master(), comm);
    build_partition();
  }

  inline void init(const meshdata<mesh_int_t,mesh_real_t> & imesh,
                   int idpn, ltype inp_layout) override
  {
    gko::size_type N = 0, n = 0;
    const auto exec = get_global_exec();
    const auto comm = get_global_comm();
    std::tie(N, n) = abstract_vector<T,S>::init_common(imesh, idpn, inp_layout);
    data = gko_vec::create(exec, comm, gko::dim<2>{N, 1}, gko::dim<2> {n, 1});
    host_data = gko_vec::create(exec->get_master(), comm, gko::dim<2>{N, 1}, gko::dim<2>{n, 1});
    set(0.0);
    build_partition();
  }

  inline void init(int igsize, int ilsize, int idpn = 1, ltype ilayout = abstract_vector<T,S>::unset) override
  {
    const auto exec = get_global_exec();
    const auto comm = get_global_comm();
    gko::size_type ls = ilsize;
    gko::size_type gs = igsize;
    data = gko_vec::create(exec, comm, gko::dim<2>{gs, 1}, gko::dim<2>{ls, 1});
    host_data = gko_vec::create(exec->get_master(), comm, gko::dim<2>{gs, 1}, gko::dim<2>{ls, 1});

    this->mesh = NULL;
    this->dpn = idpn;
    this->layout = ilayout;
    set(0.0);
    build_partition();
  }

  inline void init(const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    data = gko_vec::create_with_config_of(vec.data);
    host_data = gko_vec::create_with_config_of(vec.host_data);

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
    set(0.0);
    build_partition();
  }

  inline void set(const vector<T>& idx, const vector<S>& vals, const bool additive = false) override
  {
    const auto rank = get_global_comm().rank();
    T start, stop;
    this->get_ownership_range(start, stop);
    auto local_data = this->ptr();
    for (auto i = 0; i < idx.size(); i++) {
      if (idx.data()[i] >= start && idx.data()[i] < stop) {
        local_data[idx.data()[i] - start] = additive ? local_data[idx.data()[i] - start] + vals.data()[i] : vals.data()[i];
      }
    }
    this->release_ptr(local_data);
  }

  inline void set(const vector<T>& idx, const S val) override
  {
    vector<S> vals(idx.size(), val);
    set(idx, vals);
  }

  inline void set(const S val) override
  {
    data->fill(val);
  }

  inline void set(const T idx, const S val) override
  {
    auto p = this->ptr();
    p[idx] = val;
    this->release_ptr(p);
  }

  inline void get(const vector<T> & idx, S *out) override
  {
    const auto vals = this->const_ptr();
    for (T i = 0; i < idx.size(); i++) {
      out[i] = vals[idx[i]];
    }
  }

  S get(const T idx) override
  {
    return data->at_local(idx);
  }

  inline void operator *= (const S sca) override
  {
    const auto scale_op = gko::initialize<local_vec>({sca}, get_global_exec());
    data->scale(scale_op.get());
  }

  inline void operator /= (const S sca) override
  {
    (*this) *= 1.0 / sca;
  }

  inline void operator *= (const abstract_vector<T,S> & vec_) override
  {
    auto exec = get_global_exec();
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    gko::size_type mysize = this->lsize();
    assert(this->lsize() == vec.lsize());

    auto diag_array = gko::make_const_array_view(exec, mysize, vec.data->get_const_local_values());
    auto diag = gko::matrix::Diagonal<S>::create_const(exec, mysize, std::move(diag_array));
    auto local_array = gko::make_array_view(exec, mysize, data->get_local_values());
    auto local = local_vec::create(exec, gko::dim<2>{mysize, 1}, std::move(local_array), 1);
    diag->apply(local.get(), local.get());
  }

  void add_scaled(const abstract_vector<T,S> & vec_, S k) override
  {
    auto exec = get_global_exec();
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    T mysize = this->lsize();

    assert(mysize == vec.lsize());

    const auto scale_op = gko::initialize<local_vec>({k}, exec);
    data->add_scaled(scale_op.get(), vec.data.get());
  }

  inline void operator += (const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    this->add_scaled(vec, 1.0);
  }

  inline void operator -= (const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    this->add_scaled(vec, -1.0);
  }

  /// scalar add, equivalent to VecShift
  inline void operator += (S c) override
  {
    auto add_op = gko_vec::create_with_config_of(data.get());
    add_op->fill(c);
    const auto scale_op = gko::initialize<local_vec>({1.0}, get_global_exec());
    data->add_scaled(scale_op.get(), add_op.get());
  }

  /// deep copy vector
  inline void operator= (const vector<S> & rhs) override
  {
    assert(this->lsize() == rhs.size());

    S *v = this->ptr();
    const S *r = rhs.data();

    for(size_t i=0; i<rhs.size(); i++) {
      v[i] = r[i];
    }
    this->release_ptr(v);
  }

  inline void operator=(const abstract_vector<T, S>& rhs_) override
  {
    auto &rhs = dynamic_cast<const ginkgo_vector<T,S> &>(rhs_);
    this->data->copy_from(rhs.data);
    this->host_data->copy_from(rhs.host_data);
  }

  inline void shallow_copy(const abstract_vector<T,S> & v_) override
  {
    auto &v = dynamic_cast<const ginkgo_vector<T,S> &>(v_);
    if (!data) init();
    this->data   = v.data;
    this->host_data = v.host_data;
    this->mesh   = v.mesh;
    this->dpn    = v.dpn;
    this->layout = v.layout;
  }

  inline void deep_copy(const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
    this->data = gko::clone(vec.data);
    this->host_data = gko::clone(vec.host_data);
  }


  inline void overshadow(const abstract_vector<T,S> & sub, bool member, int offset, int sz, bool share) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(sub);
    std::runtime_error("The overshadow feature is not implemented with the Ginkgo backend.");
    // TODO: not implemented yet
  }

  inline T lsize() const override
  {
    return data != NULL ? data->get_local_vector()->get_size()[0] : 0;
  }

  inline T gsize() const override
  {
    return data != NULL ? data->get_size()[0] : 0;
  }

  inline void get_ownership_range(T & start, T & stop) const override
  {
    start = global_start;
    stop = global_stop;
  }

  inline S* ptr() override
  {
    S *p;
    if (data != NULL) {
      host_data->copy_from(data.get());
      p = host_data->get_local_values();
    }
    return p;
  }

  inline const S* const_ptr() const override
  {
    const S* p;
    if (data != NULL) {
      host_data->copy_from(data.get());
      p = host_data->get_const_local_values();
    }
    return p;
  }

  inline void release_ptr(S*& p) override
  {
    if (!data) {
      auto exec = get_global_exec();
      auto comm = get_global_comm();
      data = gko_vec::create(exec, comm);
    }
    data->copy_from(host_data.get());
  }

  inline void const_release_ptr(const S*& p) const override {}

  inline S mag() const override
  {
    auto exec = get_global_exec();
    S mag = 0.;
    if (this->gsize() > 0) {
      auto result = local_vec::create(exec, gko::dim<2>{1, 1});
      
      data->compute_norm2(result.get());
      auto host_result = gko::clone(exec->get_master(), result);
      mag = host_result->at(0,0);
    }
    return mag;
  }

  inline S sum() const override
  {
    auto exec = get_global_exec();
    auto comm = get_global_comm();
    S sum = 0.;
    auto one_vec = gko_vec::create(exec, comm, 
      gko::dim<2>{static_cast<gko::size_type>(this->gsize()), 1}, 
      gko::dim<2>{static_cast<gko::size_type>(this->lsize()), 1});
    one_vec->fill(1.0);
    auto result = gko::initialize<local_vec>({0.0}, exec);
    data->compute_dot(one_vec.get(), result.get());
    auto host_result = gko::clone(exec->get_master(), result);
    sum = host_result->at(0,0);
    return sum;
  }

  inline S dot(const abstract_vector<T,S> & v_) const override
  {
    auto &v = dynamic_cast<const ginkgo_vector<T,S> &>(v_);
    auto result = gko::initialize<local_vec>({0.0}, get_global_exec());
    data->compute_dot(v.data.get(), result.get());
    auto host_result = local_vec::create(get_global_exec()->get_master());
    host_result->copy_from(result.get());
    return host_result->at(0,0);
  }

  inline S min() const override
  {
    auto comm = get_global_comm();
    auto exec = get_global_exec();
    auto vals = this->const_ptr();
    auto local_min = std::min_element(vals, vals + this->lsize());

    S min = *local_min;
    comm.all_reduce(exec->get_master(), &min, 1, MPI_MIN);
    return min;
  }

  bool is_init() const override
  {
    auto ret = this->data != NULL;
    return ret;
  }

  inline std::string to_string() const override
  {
    std::string result = "ginkgo_vector (" + std::to_string(lsize()) + ") [";

    const S* p = const_ptr();

    for (auto i = 0; i < lsize(); i++) {
      if (i > 0) result += ", ";
      result += std::to_string(p[i]);
    }
    result += "]";

    return result;
  }

  inline bool equals(const abstract_vector<T,S> & rhs_) const override
  {
    auto exec = get_global_exec();
    auto comm = get_global_comm();
    auto &rhs = dynamic_cast<const ginkgo_vector<T,S> &>(rhs_);
    auto tmp = gko_vec::create(exec, comm);
    tmp->copy_from(rhs.data.get());
    auto scalar = gko::initialize<local_vec>({-1.0}, exec);
    tmp->add_scaled(scalar.get(), data.get());
    tmp->compute_norm2(scalar.get());
    auto host_result = gko::clone(exec->get_master(), scalar);

    return fabs(host_result->at(0,0)) < 0.0001;
  }

  inline void finish_assembly() override
  {
    data->copy_from(host_data.get());
  }

  inline void forward(abstract_vector<T, S>& out, scattering& sc, bool add = false) override
  {
    auto out_ginkgo = dynamic_cast<ginkgo_vector<T,S>*>(&out);
    auto scatter_mat = get_scattering_matrix(sc, data->get_size()[0], out_ginkgo->data->get_size()[0], partition, out_ginkgo->partition, true);
    const auto scale_op = add ? gko::initialize<local_vec>({1.}, get_global_exec()) : gko::initialize<local_vec>({0.}, get_global_exec());
    const auto one_op = gko::initialize<local_vec>({1.}, get_global_exec());
    scatter_mat->apply(one_op, this->data, scale_op, out_ginkgo->data);
  }

  inline void backward(abstract_vector<T, S>& out, scattering& sc, bool add = false) override
  {
    auto out_ginkgo = dynamic_cast<ginkgo_vector<T,S>*>(&out);
    auto scatter_mat = get_scattering_matrix(sc, data->get_size()[0], out_ginkgo->data->get_size()[0], partition, out_ginkgo->partition, false);
    const auto scale_op = add ? gko::initialize<local_vec>({1.}, get_global_exec()) : gko::initialize<local_vec>({0.}, get_global_exec());
    const auto one_op = gko::initialize<local_vec>({1.}, get_global_exec());
    scatter_mat->apply(one_op, this->data, scale_op, out_ginkgo->data);
  }

  inline void apply_scattering(scattering &sc, bool fwd) override 
  {
    if (this->buffer == NULL) {
      this->buffer = this->data->clone();
    }
    if (fwd) {
      auto scatter_mat = get_scattering_matrix(sc, data->get_size()[0], this->buffer->get_size()[0], this->partition, this->partition, true);
      scatter_mat->apply(this->data, this->buffer);
      this->data->copy_from(this->buffer);
    } else {
      this->buffer->copy_from(this->data);
      auto scatter_mat = get_scattering_matrix(sc, data->get_size()[0], this->data->get_size()[0], this->partition, this->partition, false);
      scatter_mat->apply(this->buffer, this->data);
    }
  }

};


template<class T, class S>
void init_vector_ginkgo(abstract_vector<T,S>** vec);


} // namespace SF

#endif // WITH_GINKGO
#endif // _SF_GINKGO_VECTOR_H
