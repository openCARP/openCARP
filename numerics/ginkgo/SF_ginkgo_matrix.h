// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GINKGO_MATRIX_H
#define _SF_GINKGO_MATRIX_H

#include <stdexcept>
#include "SF_abstract_matrix.h"

#ifdef WITH_GINKGO

#include <cassert>

#include <ginkgo/ginkgo.hpp>
#include "ginkgo_utils.h"

#include "SF_globals.h"
#include "SF_ginkgo_vector.h"


namespace SF {

/**
 * @brief A class encapsulating a Ginkgo matrix.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_matrix base type and
 *       use the init_matrix() function from numerics/SF_init.h.
 *
 * @see abstract_matrix
 * @see numerics/SF_init.h
 */
template<class T, class S>
class ginkgo_matrix : public abstract_matrix<T,S>
{
  public:

  using mtx = gko::experimental::distributed::Matrix<S, T, T>;
  using gko_vec = gko::experimental::distributed::Vector<S>;
  using local_mtx = gko::matrix::Csr<S, T>;
  using local_vec = gko::matrix::Dense<S>;

  gko::matrix_assembly_data<S, T> raw_data;
  T local_rows;
  gko::size_type global_rows, global_cols;

  /// the ginkgo matrix object
  mutable std::shared_ptr<mtx> data;
  mutable std::shared_ptr<const gko::experimental::distributed::Partition<T, T>> row_partition;
  mutable std::shared_ptr<const gko::experimental::distributed::Partition<T, T>> col_partition;

  /** Default constructor */
  ginkgo_matrix() : abstract_matrix<T,S>::abstract_matrix(), data{}, raw_data{{0,0}}, row_partition{}, col_partition{}
  {}

  // destructor
  ~ginkgo_matrix() override
  {}

  inline void init(T iNRows, T iNCols, T ilrows, T ilcols,
                   T loc_offset, T mxent) override
  {
    // init member vars
    abstract_matrix<T, S>::init(iNRows, iNCols, ilrows, ilcols, loc_offset, mxent);

    data = mtx::create(get_global_exec(), get_global_comm());
    row_partition = get_global_partition(ilrows);
    col_partition = get_global_partition(ilcols);

    local_rows = ilrows;
    global_rows = iNRows;
    global_cols = iNCols;
    raw_data = gko::matrix_assembly_data<S, T>(gko::dim<2>(global_rows, global_cols));
  }

  inline void zero() override
  {
    auto exec = get_global_exec();
    auto zero_op = gko::initialize<local_vec>({gko::zero<S>()}, exec);
    const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_local_matrix().get()))->scale(zero_op.get());
    const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_non_local_matrix().get()))->scale(zero_op.get());
    raw_data = gko::matrix_assembly_data<S, T>(gko::dim<2>(global_rows, global_cols));
  }

  inline void mult(const abstract_vector<T,S> & x_, abstract_vector<T,S> & b_) const override
  {
    auto &x = dynamic_cast<const ginkgo_vector<T,S> &>(x_);
    auto &b = dynamic_cast<ginkgo_vector<T,S> &>(b_);
    data->apply(x.data.get(), b.data.get());
  }

  inline void mult_LR(const abstract_vector<T,S> & L_, const abstract_vector<T,S> & R_) override
  {
    auto exec = get_global_exec();
    auto &L = dynamic_cast<const ginkgo_vector<T,S> &>(L_);
    auto &R = dynamic_cast<const ginkgo_vector<T,S> &>(R_);
    if (L.is_init()) {
      if (L.gsize() == data->get_size()[0]) {
          data->row_scale(L.data);
      }
    }
    if (R.is_init()) {
      if (R.gsize() == data->get_size()[1]) {
          data->col_scale(R.data);
      }
    }
  }

  inline void diag_add(const abstract_vector<T,S> & diag_) override
  {
    auto exec = get_global_exec();
    auto &diag = dynamic_cast<const ginkgo_vector<T,S> &>(diag_);
    auto diag_array = gko::make_const_array_view(exec, diag.lsize(), diag.data->get_const_local_values());
    auto diag_matrix = gko::matrix::Diagonal<S>::create_const(exec, diag.lsize(), std::move(diag_array));
    auto I = gko::matrix::Identity<S>::create(exec, diag.lsize());
    auto one_op = gko::initialize<gko::matrix::Dense<S>>({gko::one<S>()}, exec);
    auto local_diag = local_mtx::create(exec);
    diag_matrix->convert_to(local_diag.get());
    auto local = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_local_matrix().get()));

    local_diag->apply(one_op.get(), I.get(), one_op.get(), local);
  }

  inline void get_diagonal(abstract_vector<T,S> & vec_) const override
  {
    auto exec = get_global_exec();
    auto &vec = dynamic_cast<ginkgo_vector<T,S> &>(vec_);
    gko::size_type size = data->get_local_matrix()->get_size()[0];
    auto diag = dynamic_cast<const local_mtx *>(data->get_local_matrix().get())->extract_diagonal();
    auto tmp_array = gko::array<S>::view(exec, size, diag->get_values());
    auto tmp = local_vec::create(exec, gko::dim<2>{size, 1}, tmp_array, 1);

    auto local_array = gko::array<S>::view(exec, size, vec.data->get_local_values());
    auto local = local_vec::create(exec, gko::dim<2>{size, 1}, local_array, 1);
    local->copy_from(tmp.get());
  }

  inline void finish_assembly() override
  {
    data->read_distributed(raw_data.get_ordered_data(), row_partition, col_partition, gko::experimental::distributed::assembly_mode::communicate);
  }

  inline void scale(S s) override
  {
    const auto s_op = gko::initialize<local_vec>({s}, get_global_exec());
    auto local_data = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_local_matrix().get()));
    auto non_local_data = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_non_local_matrix().get()));
    local_data->scale(s_op.get());
    non_local_data->scale(s_op.get());
  }

  inline void add_scaled_matrix(const abstract_matrix<T,S> & A_, const S s,
                                const bool same_nnz) override
  {
    auto exec = get_global_exec();
    auto &A = dynamic_cast<const ginkgo_matrix<T,S> &>(A_);
    auto s_op = gko::initialize<local_vec>({s}, exec);
    if (same_nnz) {
        gko::size_type local_nnz = dynamic_cast<const local_mtx *>(data->get_local_matrix().get())->get_num_stored_elements();
        gko::size_type non_local_nnz = dynamic_cast<const local_mtx *>(data->get_non_local_matrix().get())->get_num_stored_elements();
        
        auto local = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_local_matrix().get()));
        auto local_array = gko::make_array_view(exec, local_nnz, const_cast<S*>(local->get_values()));
        auto local_dense = local_vec::create(exec, gko::dim<2>{local_nnz, 1}, std::move(local_array), 1);
        auto non_local = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_non_local_matrix().get()));
        auto non_local_array = gko::make_array_view(exec, non_local_nnz, const_cast<S*>(non_local->get_values()));
        auto non_local_dense = local_vec::create(exec, gko::dim<2>{non_local_nnz, 1}, std::move(non_local_array), 1);

        auto A_local = dynamic_cast<const local_mtx *>(A.data->get_local_matrix().get());
        auto A_local_array = gko::make_const_array_view(exec, local_nnz, A_local->get_const_values());
        auto A_local_dense = local_vec::create_const(exec, gko::dim<2>{local_nnz, 1}, std::move(A_local_array), 1);
        auto A_non_local = dynamic_cast<const local_mtx *>(A.data->get_non_local_matrix().get());
        auto A_non_local_array = gko::make_const_array_view(exec, non_local_nnz, A_non_local->get_const_values());
        auto A_non_local_dense = local_vec::create_const(exec, gko::dim<2>{non_local_nnz, 1}, std::move(A_non_local_array), 1);

        local_dense->add_scaled(s_op.get(), A_local_dense.get());
        non_local_dense->add_scaled(s_op.get(), A_non_local_dense.get());
    } else {
        // We assume only locally owned data is added here.
        auto I = gko::matrix::Identity<S>::create(exec, data->get_local_matrix()->get_size()[0]);
        auto one_op = gko::initialize<local_vec>({gko::one<S>()}, exec);
        auto local = const_cast<local_mtx *>(dynamic_cast<const local_mtx *>(data->get_local_matrix().get()));

        A.data->get_local_matrix()->apply(s_op.get(), I.get(), one_op.get(), local);
    }
  }

  inline void duplicate (const abstract_matrix<T,S>& M_) override
  {
    auto &M = dynamic_cast<const ginkgo_matrix<T,S> &>(M_);

    this->NRows   = M.NRows;
    this->NCols   = M.NCols;
    this->row_dpn = M.row_dpn;
    this->col_dpn = M.col_dpn;
    this->lsize   = M.lsize;
    this->start   = M.start;
    this->stop    = M.stop;
    this->mesh    = M.mesh;

    data = gko::clone(M.data);
  }

  // TODO: this is slow!
  inline void set_values(const vector<T> & row_idx, const vector<T> & col_idx,
                         const vector<S> & vals, bool add) override
  {
    assert(row_idx.size() == col_idx.size() == vals.size());

    for (auto i = 0; i < row_idx.size(); i++) {
      if (add) {
        raw_data.add_value(row_idx.data()[i], col_idx.data()[i], vals.data()[i]);
      } else {
        raw_data.set_value(row_idx.data()[i], col_idx.data()[i], vals.data()[i]);
      }
    }
  }

  // TODO: this is slow!
  inline void set_values(const vector<T>& row_idx, const vector<T>& col_idx,
                         const S* vals, bool add) override
  {
    const auto n = row_idx.size();
    const auto m = col_idx.size();
    assert(n == m);

    for (auto i = 0; i < n; i++) {
      for (auto j = 0; j < m; j++) {
        if (add) {
          raw_data.add_value(row_idx.data()[i], col_idx.data()[j], vals[i * m + j]);
        } else {
          raw_data.set_value(row_idx.data()[i], col_idx.data()[j], vals[i * m + j]);
        }
      }
    }
  }

  inline void set_value(T row_idx, T col_idx, S val, bool add) override
  {
    if (add) {
      raw_data.add_value(row_idx, col_idx, val);
    } else {
      raw_data.set_value(row_idx, col_idx, val);
    }
  }

  // TODO: this is slow!
  inline S get_value(T row_idx, T col_idx) const override
  {
    return 0;
  }

  inline void write(const char* filename) const override {}
};


template<class T, class S>
void init_matrix_ginkgo(abstract_matrix<T,S>** mat);


} // namespace SF

#endif // WITH_GINKGO
#endif // _SF_GINKGO_MATRIX_H
