#include "SF_ginkgo_solver.h"
#include "basics.h"


#include <ginkgo/core/config/registry.hpp>
#include <ginkgo/core/config/type_descriptor.hpp>
#include <ginkgo/extensions/config/json_config.hpp>
#include <nlohmann/json.hpp>


namespace SF {


template <typename T, typename S>
void ginkgo_solver<T, S>::set_stopping_criterion(norm_t normtype, double tol, int max_it, bool verbose, void* void_logger)
{
  this->norm   = normtype;
  this->max_it = max_it;
  auto logger  = reinterpret_cast<opencarp::FILE_SPEC>(void_logger);

  bool abs = true;

  switch (normtype) {
    case norm_t::absPreResidual:
      if (verbose)
        opencarp::log_msg(logger, 0, 0,"Solving %s system using absolute tolerance (%g) of preconditioned residual\n"
                "as stopping criterion", this->name.c_str(), tol);
      break;

    case norm_t::absUnpreResidual:
      // TODO: implement in Ginkgo?
      // this is not possible with all iterative methods, ideally we would issue
      // an error message here to inform the user
      if (verbose)
        opencarp::log_msg(logger, 0, 0,"Solving %s system using absolute tolerance (%g) of unpreconditioned residual\n"
                "as stopping criterion", this->name.c_str(), tol);
      throw std::runtime_error("absUnpreResidual scheme is not available with Ginkgo");
      break;

    case norm_t::relResidual:
      abs = false;
      if (verbose)
        opencarp::log_msg(logger, 0, 0,"Solving %s system using relative tolerance (%g)\n"
                "as stopping criterion", this->name.c_str(), tol);
      break;

    case norm_t::absPreRelResidual:
      abs = false;
      if(verbose)
        opencarp::log_msg(logger, 0, 0,"Solving %s system using combined relative and absolute tolerance (%g)\n"
                "as stopping criterion (preconditioned L2 is used)", this->name.c_str(), tol);
      break;

    default:
      opencarp::log_msg(logger, 2, 0,"Chosen stopping criterion invalid, defaulting to absolute tolerance of preconditioned residual");
  }

  this->stop   = gko::stop::Combined::build()
                    .with_criteria(
                          gko::stop::Iteration::build()
                              .with_max_iters(static_cast<gko::size_type>(max_it))
                              .on(get_global_exec()),
                          gko::stop::ImplicitResidualNorm<S>::build()
                              .with_reduction_factor(tol)
                              .with_baseline(abs ? gko::stop::mode::absolute : gko::stop::mode::rhs_norm)
                              .on(get_global_exec()))
                    .on(get_global_exec());
  this->logger = gko::log::Convergence<S>::create();
  this->stop->add_logger(this->logger);

  auto iterative_solver = dynamic_cast<gko::solver::IterativeBase*>(this->solver.get());
  iterative_solver->set_stop_criterion_factory(this->stop);
}

template <typename T, typename S>
void ginkgo_solver<T, S>::setup_solver(abstract_matrix<T, S>& mat, double tol, int max_it, short norm,
                    std::string name, bool has_nullspace, void* void_logger,
                    const char* solver_opts_file, const char* default_opts)
{
  auto &gko_mat = dynamic_cast<ginkgo_matrix<T,S>&>(mat);
  auto exec = gko_mat.data->get_executor();
  auto non_const_exec = std::const_pointer_cast<gko::Executor>(exec);
  auto str_name = name + " (Ginkgo)";
  this->name = str_name;
  this->options_file = solver_opts_file;
  auto logger = reinterpret_cast<opencarp::FILE_SPEC>(void_logger);

  bool verbose = true;

  auto normtype = this->convert_param_norm_type(norm);

  this->has_nullspace = has_nullspace;

  if (verbose)
    opencarp::log_msg(logger, 0, 0, "%s solver: using configuration settings given in \n %s",
            this->name.c_str(), this->options_file);
  auto ptree = gko::ext::config::parse_json_file(this->options_file);
  auto reg = gko::config::registry();
  auto td = gko::config::make_type_descriptor<S, T, T>();

  auto solver_factory = gko::config::parse(ptree, reg, td).on(exec);
  this->solver = solver_factory->generate(gko_mat.data);
  this->set_stopping_criterion(normtype, tol, max_it, verbose, logger);
}

#define OPENCARP_DECLARE_GINKGO_SOLVER(IndexType, ValueType) \
class ginkgo_solver<IndexType, ValueType>
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_GINKGO_SOLVER);


template <class T, class S>
void init_solver_ginkgo(abstract_linear_solver<T, S>** sol)
{
  *sol = new ginkgo_solver<T, S>();
}

#define OPENCARP_DECLARE_INIT_SOLVER(IndexType, ValueType) \
void init_solver_ginkgo<IndexType, ValueType>(abstract_linear_solver<IndexType,ValueType>**)
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_INIT_SOLVER);


}  // namespace SF
