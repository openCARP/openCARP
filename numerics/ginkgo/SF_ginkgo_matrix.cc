#include "SF_ginkgo_matrix.h"
#include "basics.h"

namespace SF {


#define OPENCARP_DECLARE_GINKGO_MATRIX(IndexType, ValueType) \
class ginkgo_matrix<IndexType, ValueType>
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_GINKGO_MATRIX);


template<class T, class S>
void init_matrix_ginkgo(abstract_matrix<T,S>** mat)
{
  *mat = new ginkgo_matrix<T,S>();
}

#define OPENCARP_DECLARE_INIT_MATRIX(IndexType, ValueType) \
void init_matrix_ginkgo<IndexType, ValueType>(abstract_matrix<IndexType,ValueType>**)
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_INIT_MATRIX);


} // namespace SF
