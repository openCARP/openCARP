#include "ginkgo_utils.h"
#include "basics.h"

#include <map>

using shared_exec = std::shared_ptr<const gko::Executor>;

shared_exec read_exec()
{
  std::map<std::string, std::function<shared_exec()>>
      exec_map{
          {"ref", [] { return gko::ReferenceExecutor::create(); }},
          {"omp", [] { return gko::OmpExecutor::create(); }},
          {"cuda", [] { return gko::CudaExecutor::create(
                                   gko::experimental::mpi::map_rank_to_device_id(
                                       SF_COMM,
                                       gko::CudaExecutor::get_num_devices()),
                                   gko::OmpExecutor::create(), true,
                                   gko::allocation_mode::device);
              }},
          {"hip", [] { return gko::HipExecutor::create(
                           gko::experimental::mpi::map_rank_to_device_id(
                               SF_COMM,
                               gko::HipExecutor::get_num_devices()),                      
                           gko::OmpExecutor::create(), true);
              }}};
  return exec_map.at(param_globals::ginkgo_exec)();
}

shared_exec& get_global_exec()
{
  static shared_exec global_exec = read_exec();
  return global_exec;
}

gko::experimental::mpi::communicator get_global_comm()
{
    static gko::experimental::mpi::communicator comm{SF_COMM};
    return comm;
}

template <typename T>
std::shared_ptr<const gko::experimental::distributed::Partition<T, T>> create_partition(T local_rows)
{
    auto exec = get_global_exec();
    auto comm = get_global_comm();

    auto sizes = new T[comm.size()];
    comm.all_gather(exec, &local_rows, 1, sizes, 1);

    gko::array<T> ranges_array{
      exec->get_master(), static_cast<gko::size_type>(comm.size() + 1)};
    ranges_array.get_data()[0] = 0;
    for (int i = 0; i < comm.size(); i++) {
        ranges_array.get_data()[i+1] = ranges_array.get_data()[i] + sizes[i];
    }
    ranges_array.set_executor(exec);

    auto partition = gko::share(gko::experimental::distributed::Partition<T, T>::build_from_contiguous(exec, ranges_array));
    return partition;
}

std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> get_global_partition(int local_rows)
{
    auto global_partition = create_partition(local_rows);
    return global_partition;
}

std::shared_ptr<gko::experimental::distributed::Matrix<double, int, int>> get_scattering_matrix(SF::scattering& sc, int in_size, int out_size, std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> in_part, std::shared_ptr<const gko::experimental::distributed::Partition<int, int>> out_part, bool forward)
{
    using mat = gko::experimental::distributed::Matrix<double, int, int>;
    static std::map<SF::scattering*, std::pair<std::shared_ptr<mat>, std::shared_ptr<mat>>> scatter_map{};

    if (auto search = scatter_map.find(&sc); search == scatter_map.end()) {
        gko::size_type out_s = static_cast<gko::size_type>(out_size);
        gko::size_type in_s = static_cast<gko::size_type>(in_size);
        gko::matrix_data<double, int> forward_data{gko::dim<2>{out_s, in_s}};
        gko::matrix_data<double, int> backward_data{gko::dim<2>{in_s, out_s}};
        for (int i = 0; i < sc.idx_a.size(); i++) {
            forward_data.nonzeros.emplace_back(sc.idx_b[i], sc.idx_a[i], 1);
            backward_data.nonzeros.emplace_back(sc.idx_a[i], sc.idx_b[i], 1);
        }
        forward_data.sort_row_major();
        backward_data.sort_row_major();

        auto forward_mat = gko::share(mat::create(get_global_exec(), get_global_comm()));
        forward_mat->read_distributed(forward_data, out_part, in_part, gko::experimental::distributed::assembly_mode::communicate);
        auto backward_mat = gko::share(mat::create(get_global_exec(), get_global_comm()));
        backward_mat->read_distributed(backward_data, in_part, out_part);
        scatter_map.emplace(std::make_pair(&sc, std::make_pair(forward_mat, backward_mat)));
    }
    return forward ? scatter_map.at(&sc).first : scatter_map.at(&sc).second;
}
