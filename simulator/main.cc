// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file main.cc
* @brief openCARP main function
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#include <cstdio>
#include <csignal>

//#include <omp.h>
// we set PrMGLOBAL ONLY ONCE. this will initialize the global variables
// in only one compilation unit
#define PrMGLOBAL
#include "simulator.h"
#include "async_io.h"
#include "numerics.h"
#include "fem.h"


// globals
namespace opencarp {
namespace user_globals {
  /// Registry for the different scatter objects
  SF::scatter_registry scatter_reg;
  /// Registry for the different meshes used in a multi-physics simulation
  std::map<mesh_t, sf_mesh> mesh_reg;
  /// Registriy for the inter domain mappings
  std::map<SF::quadruple<int>, SF::index_mapping<int> > map_reg;
  /// the physics
  std::map<physic_t, Basic_physic*> physics_reg;
  /// a manager for the various physics timers
  timer_manager* tm_manager;
  /// important solution vectors from different physics
  std::map<datavec_t, sf_vec*> datavec_reg;
  /// file descriptor for petsc error output
  FILE* petsc_error_fd = NULL;
  /// flag storing whether legacy stimuli are used
  bool using_legacy_stimuli = false;
  /// Communicator between IO and compute worlds
  MPI_Comm IO_Intercomm;
}  // namespace user_globals
}  // namespace opencarp

using namespace opencarp;

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char** argv)
{
// #ifdef __osf__
//   signal( SIGFPE, SIG_IGN );
// #endif
//   signal( SIGHUP,  savequit );
//   signal( SIGINT,  savequit );
//   signal( SIGQUIT, savequit );
//   signal( SIGIO,   savequit );

  //omp_set_num_threads(20);

  parse_params_cpy(argc, argv);

  // This is called intentionally before MPI_Init(), which might be helpful
  // in an HPC context, where a script might call this without mpirun on the master.
  // Still, in a parallel run we dont want every rank to print the build info,
  // as this is quite spammy. Therefore, // we call show_build_info() a second time
  // after MPI_Init, but only on rank 0.
  if(param_globals::buildinfo) {
    show_build_info();
    exit(EXIT_SUCCESS);
  }

  char *DBfile   = NULL;  // could be used to pass in ~/.petscrc
  char *help_msg = NULL;
  initialize_PETSc(&argc, argv, DBfile, help_msg);

  if(get_rank() == 0)
    show_build_info();

  bool io_rank = setup_IO(argc, argv);

  if(!io_rank) {
    setup_petsc_err_log();

    // accumulate check and conversions of param globals in here, so that
    // the rest of the codebase can remain stateless
    check_and_convert_params();

    // set up meshes
    parse_mesh_types();
    setup_meshes();
    output_meshes();

    // for experiment EXP_SETUP_MESH we quit after the mesh output step
    if(param_globals::experiment == EXP_SETUP_MESH)
      cleanup_and_exit();

    // convert input to internal units
    basic_timer_setup();

    // register and initialize physics
    register_physics();
    initialize_physics();

    // for experiment EXP_OUTPUT_FEM we quit after the electrics initialization
    if(param_globals::experiment == EXP_OUTPUT_FEM)
      cleanup_and_exit();

    // simulate
    if(param_globals::experiment == EXP_NORMAL)
      simulate();

    post_process();

    if(param_globals::num_io_nodes > 0)
      async::COMPUTE_send_exit_flag();

    cleanup_and_exit();
  } else {
    async::async_IO_queue queue;
    async::IO_poll_for_output(queue);

    PetscFinalize();
    exit(EXIT_SUCCESS);
  }
}

